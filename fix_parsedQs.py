import shutil

input = './node_modules/@types/qs/index.d.ts'
output = './node_modules/@types/qs/temp.ts'
# input = 'index.d copy.ts'
# output = 'test.ts'

string1 = 'string | string[] | ParsedQs | ParsedQs[]'
string2 = 'any'

with open(input) as f1, open(output, "w") as f3:
    f1_lines = f1.readlines()
    for line in f1_lines:
        if string1 in line:
            line = line.replace(string1, string2)
        f3.write(line)

shutil.move(output, input)
