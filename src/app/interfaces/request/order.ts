import { ErrorModel } from 'app/models'
import {
	OrderRequestInterface, VariantInterface, ProductInterface, BrandInterface, VariantAssetInterface, CategoryInterface, SizeInterface, ColorInterface, ServiceInterface, ServiceAssetInterface, TopupInterface, MatchboxInterface, MatchboxAssetInterface, OrderDetailInterface, StylecardVariantInterface, StylecardInterface, StyleboardInterface, SchedulerPreOrderInterface,
} from 'energie/app/interfaces'

import InterfaceAddressModel from 'energie/app/models/interface.address'
import { OrderRequestVoucherMetadataInterface, OrderRequestVariantMetadataInterface, OrderRequestMatchboxMetadataInterface, OrderRequestServiceMetadataInterface, OrderRequestTopupMetadataInterface, OrderRequestStyleboardMetadataInterface } from 'energie/app/interfaces/order.request'

import { Parameter, Without } from 'types/common'

import { ORDER_REQUESTS, ORDER_DETAILS } from 'energie/utils/constants/enum'


export type RequestOrderInterface<as_voucher extends boolean = false, type extends ORDER_REQUESTS = ORDER_REQUESTS> = Without<OrderRequestInterface, 'id' | 'ref_id' | 'order_id' | 'order_address_id'> & {
	id: string | number,
	metadata: (as_voucher extends true ? OrderRequestVoucherMetadataInterface
		: type extends ORDER_REQUESTS.VARIANT ? OrderRequestVariantMetadataInterface
		: type extends ORDER_REQUESTS.MATCHBOX ? OrderRequestMatchboxMetadataInterface
		: type extends ORDER_REQUESTS.SERVICE ? OrderRequestServiceMetadataInterface
		: type extends ORDER_REQUESTS.TOPUP ? OrderRequestTopupMetadataInterface
		: type extends ORDER_REQUESTS.STYLEBOARD ? OrderRequestStyleboardMetadataInterface
		: {}) & {
		address?: Parameter<InterfaceAddressModel> & { id?: number },
	},
}

export type OrderDetailDynamicInterface<T extends ORDER_DETAILS, R extends ORDER_REQUESTS> = OrderDetailInterface & {
	type: T,
	quantity: number,
	request: OrderRequestInterface,
	error?: ErrorModel,
	item: T extends ORDER_DETAILS.INVENTORY ? (VariantInterface & {
		product: ProductInterface,
		brand: BrandInterface,
		asset: VariantAssetInterface,
		category: CategoryInterface,
		size: SizeInterface,
		colors: ColorInterface[],
		stylecard?: StylecardInterface,
		styleboard?: StyleboardInterface,
		po?: SchedulerPreOrderInterface,
	}) : T extends ORDER_DETAILS.STYLEBOARD ? (ServiceInterface & {
		brand: BrandInterface,
		asset: ServiceAssetInterface,
	}) : T extends ORDER_DETAILS.WALLET_TRANSACTION ? (TopupInterface & {
		amount: number,
		asset: string,
	}) : T extends ORDER_DETAILS.VOUCHER ? (R extends ORDER_REQUESTS.MATCHBOX
		? (MatchboxInterface & {
			brand: BrandInterface,
			asset: MatchboxAssetInterface,
		}) : R extends ORDER_REQUESTS.SERVICE ? (ServiceInterface & {
			brand: BrandInterface,
			asset: ServiceAssetInterface,
		}) : R extends ORDER_REQUESTS.STYLEBOARD ? (ServiceInterface & {
			brand: BrandInterface,
			asset: ServiceAssetInterface,
		}) : R extends ORDER_REQUESTS.TOPUP ? (TopupInterface & {
			amount: number,
			asset: string,
		}) : R extends ORDER_REQUESTS.VOUCHER ? never : (VariantInterface & {
			product: ProductInterface,
			brand: BrandInterface,
			asset: VariantAssetInterface,
			category: CategoryInterface,
			size: SizeInterface,
			colors: ColorInterface[],
		})
	) : (MatchboxInterface & {
		brand: BrandInterface,
		asset: MatchboxAssetInterface,
		order_detail_id?: number,
		expired_at?: Date,
		variants: Array<StylecardVariantInterface & {
			variant: VariantInterface,
			product: ProductInterface,
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset: VariantAssetInterface,
			po?: SchedulerPreOrderInterface,
		}>,
	}),
}
