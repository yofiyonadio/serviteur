import { STYLESHEETS, STYLESHEET_STATUSES } from 'energie/utils/constants/enum'

export interface ProductFilterInterface {
	brand_ids?: string,
	category_ids?: string,
	size_ids?: string,
	color_ids?: string,
	price?: string,
	search?: string,
}

export interface DataPaginateInterface {
	total: number,
	data: [],
}

export interface InventoryParameterInterface {
	brand?: string,
	category?: string,
	size?: string,
	color?: string,
}

export interface StylistInventoryFilterInterface {
	brand?: string,
	price?: number,
	size?: string,
	color?: string,
}

export interface StylesheetFilterInterface {
	type?: STYLESHEETS,
	search?: string,
	status?: STYLESHEET_STATUSES,
	user_id?: number,
}
