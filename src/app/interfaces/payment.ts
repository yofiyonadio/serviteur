import { EWALLET, RETAIL_OUTLET, VA_BANK } from 'app/handlers/xendit'
import { PAYMENT_AGENTS, PAYMENT_METHOD } from 'energie/utils/constants/enum'

export interface PaymentRequestInterface {
	method: PAYMENT_METHOD
	token?: string
	masked_card?: string
	auth_id?: string
	va_bank?: VA_BANK
	ewallet?: EWALLET
	phone?: string
	retail_outlet?: RETAIL_OUTLET
	agent?: PAYMENT_AGENTS
}
