type payment_types = 'credit_card' | 'bca_klikpay' | 'bca_klikbca' | 'bri_epay' | 'mandiri_clickpay' | 'telkomsel_cash' | 'bank_transfer' | 'echannel' | 'indosat_dompetku' | 'mandiri_ecash' | 'cstore'
type banks = 'bca' | 'bni' | 'mandiri' | 'cimb' | 'bri' | 'danamon' | 'maybank' | 'mega'
type fraud_statuses = 'accept' | 'challenge' | 'deny'
type status_codes = '200' | '201' | '202' | '400' | '404' | '406' | '500'


export interface TransactionDetailInterface {
	order_id: string
	gross_amount: number
}

export interface ItemDetailInterface {
	id: string | number
	price: number
	quantity: number
	name?: string
	brand?: string
	category?: string
	merchant_name?: string
}

export interface AddressInterface {
	first_name?: string
	last_name?: string
	email?: string
	phone?: string
	address?: string
	city?: string
	postal_code?: string
	country_code?: string
}

export interface CustomerDetailInterface {
	first_name: string
	last_name: string
	email: string
	phone?: string
	billing_address?: AddressInterface
	shipping_address?: AddressInterface
}

export interface InstallmentInterface {
	required?: boolean
	terms?: {
		[k in banks]: number[]
	}
}

export interface CreditCardInterface {
	secure?: boolean
	bank?: banks
	channel?: 'migs'
	type?: 'authorize_capture' | 'authorize'
	whitelist_bins?: Array<'bni' | 'bca' | 'mandiri' | 'cimb' | 'bri' | 'mega' | 'maybank'>
	installment?: InstallmentInterface
}

export interface FreeTextItemInterface {
	en?: string
	id?: string
}

export interface FreeTextInterface {
	inquiry?: FreeTextItemInterface[]
	payment?: FreeTextItemInterface[]
}

export interface BCAVAInterface {
	va_number?: string
	sub_company_code?: string
	free_text?: FreeTextInterface
}

export interface BNIVAInterface {
	va_number?: string
}

export interface PermataVAInterface {
	va_number?: string
	recipient_name?: string
}

export interface CallbackInterface {
	finish: string
	unfinish?: string
	error?: string
}

export interface ExpiryInterface {
	start_time?: string,
	unit: 'day' | 'hour' | 'minute',
	duration: number
}

export interface TransactionParameterInterface {
	transaction_details: TransactionDetailInterface
	item_details?: ItemDetailInterface[]
	customer_details?: CustomerDetailInterface
	enabled_payments?: Array<'credit_card' | 'mandiri_clickpay' | 'cimb_clicks' | 'bca_klikbca' | 'bca_klikpay' | 'bri_epay' | 'telkomsel_cash' | 'echannel' | 'mandiri_ecash' | 'permata_va' | 'other_va' | 'bca_va' | 'bni_va' | 'indomaret' | 'danamon_online' | 'akulaku'>
	credit_card?: CreditCardInterface

	// VAs
	bca_va?: BCAVAInterface
	bni_va?: BNIVAInterface
	permata_va?: PermataVAInterface

	callbacks: CallbackInterface
	expiry?: ExpiryInterface
	custom_field1?: string
	custom_field2?: string
	custom_field3?: string
}

export interface NotificationParameterInterface {
	status_code: status_codes
	status_message: string
	order_id: string
	gross_amount: string
	payment_type: payment_types
	transaction_status: 'capture' | 'settlement' | 'pending' | 'cancel' | 'expire' | 'authorize'
	transaction_time: string
	fraud_status: fraud_statuses
	approval_code: string
	masked_card?: string
	bank: string
	currency: string
	transaction_id: string
	signature_key: string
}

export interface RefundInterface {
	refund_chargeback_id: string,
	refund_amount: string,
	created_at: string,
	reason: string,
	refund_key: string,
	refund_method: string
}

export interface TransactionStatusRespondInterface {
	status_code: status_codes
	transaction_id: string
	order_id: string
	gross_amount: string
	payment_type: payment_types
	transaction_time: string
	transaction_status: 'capture' | 'deny' | 'authorize' | 'settlement' | 'pending' | 'cancel' | 'expire'
	fraud_status: fraud_statuses
	masked_card: string
	bank: banks
	status_message: string
	signature_key: string
	approval_code: string
	channel_response_code: string
	channel_response_message: string
	card_type: 'credit' | 'debit'
	refund_amount: string
	refunds: RefundInterface[]
}

export interface TransactionActionRespondInterface {
	transaction_id: string
	order_id: string
	gross_amount: string
	payment_type: payment_types
	transaction_time: string
	transaction_status: 'capture' | 'deny' | 'authorize'
	fraud_status: fraud_statuses
	masked_card: string
	status_code: status_codes
	bank: banks
	status_message: string
}

export interface TransactionExpireRespondInterface {
	transaction_id: string
	order_id: string
	gross_amount: string
	payment_type: payment_types
	transaction_time: string
	transaction_status: 'capture' | 'deny' | 'authorize'
	status_code: status_codes
	status_message: string
}

export interface TransactionRefundRespondInterface {
	transaction_id: string
	order_id: string
	gross_amount: string
	payment_type: payment_types
	transaction_time: string
	transaction_status: 'refund' | 'partial_refund'
	status_code: status_codes
	status_message: string
	refund_chargeback_id: string
	refund_amount: string
	refund_key: string
}
