export interface CreditCardResponseInterface {
	id: string,
	created: string,
	charge_type: 'SINGLE_USE_TOKEN' | 'MULTIPLE_USE_TOKEN' | 'RECURRING',
	business_id: string,
	authorized_amount: number,
	external_id: string,
	card_type: string,
	merchant_id: string,
	card_brand: string,
	status: 'CAPTURED' | 'AUTHORIZED' | 'REVERSED' | 'FAILED',
	bank_reconciliation_id: string,
	ecl: eci_code,
	capture_amount: number,
	merchant_reference_code: string,
	failure_reason?: 'EXPIRED_CARD' | 'CARD_DECLINED' | 'INSUFFICIENT_BALANCE' | 'STOLEN_CARD' | 'INACTIVE_CARD' | 'INVALID_CVN' | 'PROCESSOR_ERROR' | 'BIN_BLOCK',
	approval_code?: string,
	descriptor?: string,
	currency?: string,
	mid_label?: string,
	promotion?: {
		reference_id?: string,
		original_amount?: number,
	},
	installment?: {
		count?: number,
		interval?: string,
	}
}

type eci_code = '00' // Unable to Authenticate (MasterCard)
	| '01'           // Authentication attempted (MasterCard)
	| '02'           // Successful authentication (MasterCard)
	| '03'           // Successful authentication (Visa, AMEX, JCB)
	| '05'           // Successful authentication (Visa, AMEX, JCB)
	| '06'           // Authentication attempted (Visa, AMEX, JCB)
	| '07'           // Unable to Authenticate (Visa, AMEX, JCB)

export interface VirtualAccountResponseInterface {
	id: string,
	owner_id: string,
	external_id: string,
	bank_code: string,
	merchant_code: string,
	name: string,
	account_number: string,
	is_closed: boolean,
	is_single_use: boolean,
	status: va_status,
	suggested_amount?: string,
	expected_amount?: number,
	expiration_date?: Date,
	description?: string,   // Only supported for BRI virtual accounts.
}

type va_status = 'PENDING'  // virtual account creation request has been sent and request is being processed by the bank.
	| 'INACTIVE'            // either the single use virtual account has been paid or already expired.
	| 'ACTIVE'              // the virtual account is ready to be used by the end user.

export interface RetailOutletResponseInterface {
	id: string,
	owner_id: string,
	external_id: string,
	retail_outlet_name: 'ALFAMART' | 'INDOMARET',
	prefix: string,
	name: string,
	payment_code: string,
	expected_amount: number,
	is_single_use: boolean,
	expiration_date: Date
}

export interface OVOResponseInterface {
	ewallet_type: 'OVO',
	business_id: string,
	external_id: string,
	amount: number,
	phone: string,
	status: string,
	created: string,
}

export interface DANAResponseInterface {
	ewallet_type: 'DANA',
	external_id: string,
	amount: number,
	checkout_url: string,
}

export interface LINKAJAResponseInterface {
	ewallet_type: 'LINKAJA',
	external_id: string,
	amount: number,
	checkout_url: string,
	transaction_date: string,
}

export interface VirtualAccountNotification {
	method: 'va',
	id: string,
	payment_id: string,
	callback_virtual_account_id: string,
	owner_id: string,
	external_id: string,
	account_number: string,
	bank_code: string,
	amount: number,
	merchant_code: string,
	transaction_timestamp: string,
	sender_name: string,
}

export interface RetailOutletNotification {
	method: 'retail-outlet',
	fixed_payment_code_payment_id: string,
	owner_id: string,
	fixed_payment_code_id: string,
	payment_id: string,
	external_id: string,
	payment_code: string,
	prefix: string,
	retail_outlet_name: string,
	amount: number,
	name: string,
	transaction_timestamp: string,
}

export interface OVONotification {
	method: 'ewallet',
	ewallet_type: 'OVO',
	id: string,
	event: string,
	external_id: string,
	business_id: string,
	phone: string,
	amount: number,
	status: 'COMPLETED' | 'FAILED',
	created: Date,
	failure_code: string,
}

export interface DANANotification {
	method: 'ewallet',
	ewallet_type: 'DANA',
	callback_authentication_token: string,
	external_id: string,
	amount: number,
	business_id: string,
	payment_status: 'EXPIRED' | 'PAID',
	transaction_date: string,
}

export interface LINKAJANotification {
	method: 'ewallet',
	ewallet_type: 'LINKAJA',
	callback_authentication_token: string,
	external_id: string,
	amount: number,
	items: Array<{
		id: string,
		name: string,
		price: number,
		quantity: number,
	}>,
	status: 'FAILED' | 'SUCCESS_COMPLETED',
}

export interface CardInterface {
	id: string,
	created_at: string,
	order_number: string,
	card_type: string,
	card_brand: string,
	status: 'CAPTURED' | 'AUTHORIZED' | 'REVERSED' | 'FAILED',
}

export interface VirtualAccountInterface {
	id: string,
	bank: string,
	order_number: string,
	prefix: string,
	payment_code: string,
	name: string,
	amount: number,
	expired_at: Date,
}

export interface RetailOutletInterface {
	id: string,
	outlet_name: string,
	order_number: string,
	prefix: string,
	payment_code: string,
	name: string,
	amount: number,
	expired_at: Date,
}

export interface EwalletInterface {
	ewallet_type: string,
	order_number: string,
	checkout_url: string,
	amount: number,
	metadata: object,
}
