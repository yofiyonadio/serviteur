import { FeedbackInterface } from 'energie/app/interfaces'
export interface ExchangeRequestInterface {
	order_detail_id: number,
	note: string,
	inventory: FeedbackInterface[],
}
