import { BrandAddressInterface, BrandInterface, InventoryInterface, VariantInterface, VariantColorInterface, ProductInterface, SizeInterface, ColorInterface, VariantAssetInterface } from 'energie/app/interfaces'


export interface InventoryDeepInterface extends InventoryInterface {
	variant: VariantInterface & {
		product: ProductInterface & {
			brand: BrandInterface,
		},
		variantColors: Array<VariantColorInterface & {
			color: ColorInterface,
		}>,
		size: SizeInterface,
		assets: VariantAssetInterface[],
	},
	brandAddress: BrandAddressInterface,
}
