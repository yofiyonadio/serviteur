	// private getConfigFromFile(file: any, metadata = {}, nonce: string) {
	// 		return {
	// 			url: `${file.public_id}.${file.format}`,
	// 			type: file.resource_type,
	// 			width: file.width,
	// 			height: file.height,
	// 			nonce: nonce,
	// 			metadata: {
	// 				version: file.version,
	// 				tags: file.tags,
	// 				type: file.type,
	// 				etag: file.etag,
	// 				placeholder: file.placeholder,
	// 				url: file.url,
	// 				secureUrl: file.secure_url,
	// 				format: file.format,
	// 				publicId: file.public_id,
	// 				...metadata,
	// 			},
	// 		}
	// 	}

export enum FormatType {
	jpg = 'jpg',
	png = 'png',
	gif = 'gif',
	mp4 = 'mp4',
}

export enum CategoryType {
	google_tagging = 'google_tagging',
	google_video_tagging = 'google_video_tagging',
	imagga_tagging = 'imagga_tagging',
	aws_rek_tagging = 'aws_rek_tagging',
}

export enum AccessType {
	public = 'public',
	authenticated = 'authenticated',
}

export enum UploadType {
	upload = 'upload',
	private = 'private',
	authenticated = 'authenticated',
}

export enum ResourceType {
	image = 'image',
	raw = 'raw',
	video = 'video',
	auto = 'auto',
}

export interface AssetResponse {
	access_mode?: AccessType
	backup?: boolean
	bytes?: number
	colors?: object[]
	context?: object
	coordinates?: object
	created_at: Date
	// data
	delete_token?: string
	// deleted
	// derived
	// eager
	etag: string
	// faces
	folders?: []
	format: FormatType
	grayscale?: boolean
	height: number
	illustration_score?: number
	image_metadata?: object
	info?: object
	last_updated?: Date
	// mappings
	// message
	// moderation
	// next_cursor
	original_filename: string
	pages?: number
	partial?: boolean
	phash?: string
	pixels: number
	placeholder?: string
	// plan?: string
	// presets
	predominant?: object
	public_id: string
	// resources
	resource_type: ResourceType
	secure_url: string
	semi_transparent?: boolean
	signature: string
	transformations: object[]
	tags: string[]
	time: number
	type: UploadType
	url: string
	version: number
	width: number
	// use_filename: boolean
	// unique_filename: boolean
}

// https://cloudinary.com/documentation/image_upload_api_reference#upload_method
export interface AssetUploadConfig {
	public_id?: string
	folder?: string
	use_filename?: boolean // default false
	unique_filename?: boolean // default true
	resource_type?: ResourceType // default ResourceType.image
	type?: UploadType // default to "upload"
	// access_control
	access_mode?: AccessType // default to "public"
	discard_original_filename?: boolean // default false
	overwrite?: boolean // default true
	tags?: string[] // default []
	context?: object // default {}
	colors?: boolean // default false
	faces?: boolean // default false
	quality_analysis?: boolean // default false
	image_metadata?: boolean // default false
	phash?: boolean // default false
	auto_tagging?: number // between 0.0 to 1.0
	categorization?: CategoryType // or comma separated
	// detection
	// ocr
	// eager
	// eager_async
	// eager_notification_url
	// transformation
	format?: FormatType
	custom_coordinates?: string // x,y,w,h
	face_coordinates?: string[] // x,y,w,h
	// background_removal
	// raw_convert
	allowed_formats?: FormatType[]
	async?: boolean // default false
	backup?: boolean
	// headers
	invalidate?: boolean // default false
	// moderation
	// notification_url // webhook that is triggered if asnyc
	return_delete_token?: boolean
}
