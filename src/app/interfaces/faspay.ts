type true_false = 0 | 1
type yes_no = 'Yes' | 'No' | 'NA'

export type status_code = 0 | '0' 	// belum di proses
	| 1 | '1'					// dalam proses
	| 2 | '2'					// payment sukses
	| 3 | '3'					// payment gagal
	| 4 | '4'					// payment reversal
	| 7 | '7'					// payment expired
	| 8 | '8'					// payment cancelled
	| 9 | '9'					// unknown

type response_code = '00'	// Sukses
	| '03'					// Invalid Merchant
	| '13'					// Invalid Amount
	| '14'					// Invalid Order
	| '17'					// Order Cancelled by Merchant / Customer
	| '18'					// Invalid Customer or MSISDN is not found
	| '21'					// Subscription is Expired
	| '30'					// Format Error
	| '40'					// Requested Function not Supported
	| '54'					// Order is Expired
	| '55'					// Incorrect User / Password
	| '63'					// Security Violation(from unknown IP - Address)
	| '56'					// Not Active / Suspended
	| '66'					// internal Error
	| '80'					// Payment Was Reversal
	| '81'					// Already Been Paid
	| '82'					// Unregistered Entity
	| '83'					// Parameter is mandatory
	| '84'					// Unregistered Parameters
	| '85'					// Insufficient Paramaters
	| '96'					// System Malfunction

type payment_method = 1 | '1'		// Credit Card / Debit Card
	| 2 | '2'						// Maybank2u
	| 3 | '3'						// FPX
	| 4 | '4'						// CUP
	| 5 | '5'						// Kiosk(reserved for future)
	| 6 | '6'						// Cash(reserved for future)
	| 7 | '7'						// 7-eleven(reserved for future)
	| 8 | '8'						// CIMB Clicks
	| 9 | '9'						// ENets
	| 10 | '10'						// PBB Payment Agent
	| 11 | '11'						// MIDAZZ Prepaid
	| 12 | '12'						// KlikBCA
	| 13 | '13'						// Paypal Express Checkout
	| 14 | '14'						// RHB Payment Gateway
	| 15 | '15'						// AliPay

type error_code = '0'
	| '5500' | '5501' | '5502' | '5503' | '5504' | '5505' | '5506' | '5507' | '5508' | '5509' | '5510' | '5511' | '5512' | '5513' | '5514' | '5515' | '5516' | '5517' | '5518' | '5519'
	| '5520' | '5521' | '5522' | '5523' | '5524' | '5525' | '5526' | '5527' | '5528' | '5529' | '5530' | '5531' | '5532' | '5533' | '5534' | '5535' | '5536' | '5537' | '5538'
	| '5540' | '5541' | '5542' | '5543' | '5544' | '5545' | '5546' | '5547' | '5548' | '5549' | '5550' | '5551' | '5552' | '5553' | '5554' | '5555' | '5557'
	| '5560' | '5562' | '5566' | '5567' | '5568' | '5569' | '5574' | '5575' | '5576' | '5577' | '5578' | '5579'
	| '5582' | '5583' | '5584' | '5585' | '5588' | '5590' | '5594' | '5595' | '5596' | '5597' | '5598' | '5599'
	| '5600' | '5601' | '5602' | '5603' | '5604' | '5605' | '5606' | '5607' | '5608' | '5609' | '5610' | '5611' | '5612' | '5613' | '5614' | '5615' | '5616' | '5617' | '5618' | '5619'
	| '5620' | '5621' | '5622' | '5623' | '5624' | '5625' | '5626' | '5627' | '5628' | '5629' | '5630' | '5631' | '5632' | '5633' | '5634'
	| '6000' | '6001' | '6002'
	| '6100' | '6101' | '6102' | '6103' | '6104' | '6105' | '6106' | '6107' | '6108' | '6109' | '6110' | '6111' | '6112' | '6117' | '6118' | '6119' | '6120' | '6121'
	| '6200' | '6201' | '6202'
	| '6300' | '6301' | '6302'
	| '7000' | '7001' | '7002' | '7003' | '7004' | '7005' | '7006' | '7007' | '7008'
	| '9992' | '9993' | '9994' | '9995' | '9996' | '9997' | '9998' | '9999'

type transaction_status = 'N'			// Pending
	| 'A'								// Successfully authorized
	| 'E'								// Errored
	| 'C'								// Successfully captured
	| 'CF'								// Capture failed
	| 'S'								// Successfully approved for sales in bank
	| 'F'								// Failed, declined by bank
	| 'RC'								// Need to reconfirm
	| 'CB'								// Chargeback, reversal initiated by bank
	| 'V'								// Void
	| 'B'								// Blocked
	| 'CR'								// Successfully capture a reversal
	| 'CRF'								// Capture reversal failed
	| 'SR'								// Sale reversed
	| 'SRF'								// Sale reversal failed
	| 'PCR'								// Capture reversal in progress
	| 'PSR'								// Sales reversal in progress
	| 'PCB'								// Chargeback in progress
	| 'I'								// Incomplete

type user_code = '101'					// Transaction approved
	| '102'								// Transaction declined by bank
	| '103'								// Transaction declined by faspay
	| '104'								// Transaction invalid
	| '105'								// Transaction declined because of dispute / cancellation on card holder
	| '106'								// Transaction declined - unknown
	| '107'								// Transaction declined - e-wallet problem
	| '108'								// Transaction pending

type currency_code = 'AED'
	| 'AUD'
	| 'BND'
	| 'CHF'
	| 'CNY'
	| 'EGP'
	| 'EUR'
	| 'GBP'
	| 'HKD'
	| 'IDR'
	| 'INR'
	| 'JPY'
	| 'KRW'
	| 'LKR'
	| 'MYR'
	| 'NZD'
	| 'PHP'
	| 'PKR'
	| 'SAR'
	| 'SEK'
	| 'SGD'
	| 'THB'
	| 'TWD'
	| 'USD'
	| 'ZA'

type user_indicator = 'SUC'				// Success
	| 'RTY' 							// Retry, please re-order
	| 'ALT'								// Alert, need action
	| 'NA'								// Not applicable

type fraud_level = -1 | '-1'			// Not used
	| 0 | '0'							// Very low
	| 1 | '1'							// Low
	| 2 | '2'							// Medium
	| 3 | '3'							// High
	| 4 | '4'							// Very high

type card_type = 'M'					// Mastercard
	| 'V'								// Visa
	| 'J'								// JCB
	| 'A'								// Amex

type bank_type = '001'					// Maybank Malaysia
	| '002'								// Bank Islam Malaysia
	| '003'								// Citibank Malaysia
	| '004'								// Public Bank Malaysia
	| '005'								// CIMB Bank Malaysia(formerly known as Southern Bank Malaysia)
	| '006'								// UOB Singapore
	| '007'								// DBS Singapore
	| '008'								// Citibank Singapore
	| '009'								// RHB Malaysia
	| '010'								// HSBC
	| '011'								// Bank Internasional Indonesia
	| '012'								// Standard Chartered Bank
	| '013'								// American Express
	| '014'								// Alliance Bank Malaysia
	| '015'								// Bank Central Asia Indonesia
	| '997'								// Simulator
	| '998'								// Others
	| '999'								// Not Available

export interface ItemDetailInterface {
	product: string
	amount: number
	qty: number
}

export interface TransactionParameterInterface {
	merchant_id: number
	merchant_name: string
	order_id: string
	order_reff?: string
	bill_date: string 		// YYYY-MM-DD HH:mm:ss
	bill_expired: string	// YYYY-MM-DD HH:mm:ss
	bill_desc: string		// Description
	bill_gross?: number
	bill_miscfee?: number
	bill_total: number
	display_cust: boolean
	cust_no?: string 		// Cust ID
	custName?: string		// Cust Name
	custPhone?: number
	custEmail: string
	billingAddress?: string
	billingCity?: string
	billingRegion?: string
	billingState?: string
	billingPostcode?: string
	billingCountryCode?: string
	receiver_name?: string
	shippingAddress?: string
	shippingCity?: string
	shippingRegion?: string
	shippingState?: string
	shippingPostCode?: string

	klik_pay_code?: string
	clear_key?: string
	mixed?: true_false

	mid_full?: number
	mid_tiga_bulan?: number
	mid_enam_bulan?: number
	mid_duabelas_bulan?: number
	mid_duaempat_bulan?: number

	cicilan_enam_bulan?: true_false
	cicilan_tiga_bulan?: true_false
	cicilan_duabelas_bulan?: true_false
	cicilan_duaempat_bulan?: true_false

	products: ItemDetailInterface[]
	signature: string
	term_condition: 0 | 1
	return_url: string
}

export interface InquiryParameterInterface {
	trx_id: number
	merchant_id: number
	// merchant?: string
	request?: string
	bill_no: string
	signature: string
}

export interface InquiryResponseInterface {
	trx_id: number
	merchant_id: number
	merchant?: string
	response?: string
	bill_no: string
	payment_reff?: string
	payment_date?: string
	payment_status_code: status_code
	payment_status_desc: string
	response_code: response_code
	response_desc: string
}

export interface PaymentNotificationInterface {
	trx_id: number
	merchant_id: number
	merchant: string
	bill_no: string
	payment_reff?: string
	payment_date: string
	payment_status_code: status_code
	payment_status_desc: string
	signature: string
	amount: number
	request?: string
}

export interface PaymentNotificationResponseInterface {
	response: string
	trx_id: number
	merchant_id: number
	merchant: string
	bill_no: string
	response_code: response_code
	response_desc: string
	response_date: string
}

export interface CreditReturnResponseInterface {
	PAYMENT_METHOD: payment_method
	MERCHANTID: string
	MERCHANT_TRANID: string
	ERR_CODE: error_code
	ERR_DESC: string
	USR_CODE?: user_code
	USR_MSG?: string
	TXN_STATUS: transaction_status
	DESCRIPTION?: string
	CURRENCYCODE: currency_code
	AMOUNT: string
	SIGNATURE: string
	EUI?: user_indicator
	CUSTNAME: string
	TRANSACTIONID: string
	TRANSACTIONTYPE: string
	TRANDATE?: string
	IS_BLACKLISTED?: yes_no
	FRAUDRISKLEVEL?: fraud_level
	FRAUDRISKSCORE?: number | string
	EXCEED_HIGH_RISK?: yes_no
	CARDNAME?: string
	CARDTYPE?: card_type
	CARD_NO_PARTIAL?: string
	ACQUIRER_ID?: string
	ACQUIRER_BANK?: bank_type
	BANK_RES_CODE?: string
	BANK_RES_MSG?: string
	AUTH_ID?: string
	WHITELIST_CARD?: yes_no
	is_on_us?: yes_no
	BANK_REFERENCE?: string
}

export interface CreditInquiryParameterInterface {
	PAYMENT_METHOD: payment_method
	TRANSACTIONTYPE: '4'
	MERCHANTID: string
	MERCHANT_TRANID: string
	AMOUNT: string 				// Please provide float value fixed(2)
	RESPONSE_TYPE: '3'
	TRANSACTIONID: string
	SIGNATURE: string,
}

export interface CreditInquiryResponseInterface {
	PAYMENT_METHOD?: payment_method
	MERCHANTID: string
	MERCHANT_TRANID: string
	TRANSACTIONID: string // harus disimpan buat later references
	ERR_CODE: error_code
	ERR_DESC: string
	TXN_STATUS: transaction_status
	AMOUNT: string 				// Please provide float value fixed(2)
	SIGNATURE: string
	TRANSACTIONTYPE: '4'
	TOTAL_REFUND_AMOUNT: number | string
	TRANDATE?: string
	CUSTNAME?: string
	DESCRIPTION?: string
	IS_BLACKLISTED?: yes_no
	FRAUDRISKLEVEL?: fraud_level
	FRAUDRISKSCORE?: number | string
	EUI?: user_indicator
	EXCEED_HIGH_RISK?: yes_no
	CURRENCYCODE?: currency_code
	ACQUIRER_ID?: string
	CARDNAME?: string
	CARDTYPE?: card_type
	CARD_NO_PARTIAL?: string
	ACQUIRER_BANK?: bank_type
	BANK_RES_CODE?: string
	BANK_RES_MSG?: string
	AUTH_ID?: string
	BANK_REFERENCE?: string
	is_on_us?: yes_no
	CAPTURE_DATE?: string
	REFUND_DATE?: string
	WHITELIST_CARD?: yes_no
	PYMT_TOKEN?: string // harus disimpan
}

// Only if TXN_STATUS = "A"
export interface CreditCaptureParameterInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '2'
	MERCHANTID: string
	MERCHANT_TRANID: string
	TRANSACTIONID: string
	AMOUNT: string
	RESPONSE_TYPE: '3'
	SIGNATURE: string
	RETURN_URL: string
	CUSTNAME?: string
	CUSTEMAIL?: string
	DESCRIPTION?: string
	MREF1?: string
	MREF2?: string
	MREF3?: string
	MREF4?: string
	MREF5?: string
	MREF6?: string
	MREF7?: string
	MREF8?: string
	MREF9?: string
	MREF10?: string
}

export interface CreditCaptureResponseInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '2'
	MERCHANTID: string
	MERCHANT_TRANID: string
	ERR_CODE: error_code
	ERR_DESC: string
	TXN_STATUS: 'C' | 'CF'
	AMOUNT: string
	TRANSACTIONID: number | string
	SIGNATURE: string
	USR_CODE?: user_code
	USR_MSG?: string
	ACQUIRER_ID?: string
	is_on_us?: yes_no
	BANK_RES_MSG?: string
	CAPTURE_DATE?: string
}

// Only if TXN_STATUS = "N" "A" "RC"
export interface CreditVoidParameterInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '10'
	MERCHANTID: string
	MERCHANT_TRANID: string
	TRANSACTIONID: number | string
	AMOUNT: string
	RESPONSE_TYPE: '3'
	SIGNATURE: string
	RETURN_URL: string
	CUSTNAME?: string
	CUSTEMAIL?: string
	DESCRIPTION?: string
}

export interface CreditVoidResponseInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '10'
	MERCHANTID: string
	MERCHANT_TRANID: string
	ERR_CODE: error_code
	ERR_DESC: string
	TXN_STATUS: 'V' | 'E'
	AMOUNT: string
	TRANSACTIONID: number | string
	SIGNATURE: string
	USR_CODE?: user_code
	USR_MSG?: string
	is_on_us?: yes_no
}

// Only if TXN_STATUS = "C" "S"
export interface CreditRefundParameterInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '11'
	MERCHANTID: string
	MERCHANT_TRANID: string
	TRANSACTIONID: number | string
	AMOUNT: string
	REFUND_AMOUNT: string
	RESPONSE_TYPE: '3'
	SIGNATURE: string
	RETURN_URL: string
	CUSTNAME?: string
	CUSTEMAIL?: string
	DESCRIPTION?: string
}

export interface CreditRefundResponseInterface {
	PAYMENT_METHOD: '1'
	TRANSACTIONTYPE: '11'
	MERCHANTID: string
	MERCHANT_TRANID: string
	ERR_CODE: error_code
	ERR_DESC: string
	TXN_STATUS: 'C' | 'S'
	AMOUNT: string
	TRANSACTIONID: number | string
	SIGNATURE: string
	USR_CODE?: user_code
	USR_MSG?: string
	ACQUIRER_ID?: string
	REFUND_DATE?: string
	REFUND_AMOUNT?: string
	TOTAL_REFUND_AMOUNT?: string
	BANK_RES_CODE?: string
	BANK_RES_MSG?: string
}
