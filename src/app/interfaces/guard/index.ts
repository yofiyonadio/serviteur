import moment from 'moment'
export * from './typechecker'


type Primitives = 'bigint'
	| 'boolean'
	| 'function'
	| 'number'
	| 'string'
	| 'symbol'
	| 'undefined'

type OptionalPrimitives = 'bigint?'
	| 'boolean?'
	| 'function?'
	| 'number?'
	| 'string?'
	| 'symbol?'
	| 'undefined?'

type Customs = 'date'

type OptionalCustoms = 'date?'

type Objects = 'object'

type OptionalObjects = 'object?'

type Enums = 'enum'

type OptionalEnums = 'enum?'

type Ors = 'or'

type OptionalOrs = 'or?'

type Arrays = 'array'

type OptionalArrays = 'array?'

type Types = Primitives | Customs // | Objects | Enums | Ors

type OptionalTypes = OptionalPrimitives | OptionalCustoms // | OptionalObjects | OptionalEnums | OptionalOrs

type ConfigurableTypes = [Objects, AllTypesShape]
	| [Enums, string[]]
	| [Ors, AllTypes[]]
	| [Arrays, AllTypes]

type OptionalConfigurableTypes = [OptionalObjects, AllTypesShape]
	| [OptionalEnums, string[]]
	| [OptionalOrs, AllTypes[]]
	| [OptionalArrays, AllTypes]

type AllTypes = Types | OptionalTypes | ConfigurableTypes | OptionalConfigurableTypes

interface AllTypesShape {
	[key: string]: AllTypes
}

export function checker(value: any): [any, any] {
	return value
}

export function check(value: any, type: AllTypes): boolean {
	let _type: Types | OptionalTypes | ConfigurableTypes[0] | OptionalConfigurableTypes[0]
	let _config: ConfigurableTypes[1] | OptionalConfigurableTypes[1] | null

	if (Array.isArray(type)) {
		// Configurables
		_type = type[0]
		_config = type[1]
	} else {
		// Non Configurables
		_type = type
		_config = null
	}

	const len = _type.length - 1
	const isOptional = _type[len] === '?'

	if (isOptional) {
		return optional(value, _type.substr(0, len) as Types | ConfigurableTypes[0], _config)
	} else {
		return required(value, _type as Types | ConfigurableTypes[0], _config)
	}
}

function optional(value: any, type: Types | ConfigurableTypes[0], config: ConfigurableTypes[1] | null): boolean {
	return value === undefined || value === null
		? true
		: required(value, type, config)
}

function required(value: any, type: Types | ConfigurableTypes[0], config: ConfigurableTypes[1] | null): boolean {
	if (config !== null) {
		// Configurable Types
		switch (type) {
		case 'object':
			return isShape(value, config as AllTypesShape)
		case 'array':
			return isArray(value, config as AllTypes)
		case 'enum':
			return isEnum(value, config as string[])
		case 'or':
			return isOr(value, config as AllTypes[])
		default:
			throw new Error(`Non existent configurable types "${type}"!`)
		}
	} else {
		// Non Configurable Types
		switch (type) {
		case 'date':
			return isDate(value)
		case 'number':
			return typeof value === type && !isNaN(value)
		default:
			return typeof value === type
		}
	}
}

function isArray(values: any, type: AllTypes): boolean {
	return Array.isArray(values) && values.findIndex(value => {
		return !check(value, type)
	}) === -1
}

function isTruthy(value: any) {
	return !!value || value === 0
}

function isDate(value: any): boolean {
	return moment(value).isValid()
}

function isShape(value: { [k: string]: any }, config: AllTypesShape): boolean {
	if (isTruthy(value)) {
		const keys = Object.keys(config) as Array<keyof typeof config>

		return keys.findIndex(key => {
			return !check(value[key], config[key])
		}) === -1
	} else {
		return false
	}
}

function isEnum(value: any, config: string[]): boolean {
	return config.indexOf(value) > -1
}

function isOr(value: any, config: AllTypes[]): boolean {
	return config.findIndex(type => {
		return check(value, type)
	}) > -1
}

