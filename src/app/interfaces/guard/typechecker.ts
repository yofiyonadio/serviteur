import { check } from './index'
import { ORDER_REQUESTS, PAYMENT_AGENTS, PAYMENT_METHOD } from 'energie/utils/constants/enum'
import { VA_BANK, EWALLET, RETAIL_OUTLET } from 'app/handlers/xendit'

export function isProductRequest(data: any) {
	return check(data, ['array', ['object', {
		id: ['or', ['number', 'string']],
		type: ['enum', Object.keys(ORDER_REQUESTS)],
		as_voucher: 'boolean?',
		quantity: 'number?',
		metadata: ['or', [['object', {
			code: 'string',
			name: 'string',
			email: 'string',
			note: 'string',
			is_physical: 'boolean?',
			merchant: 'string?',
		}], ['object', {
			outfit: 'string?',
			attire: 'string?',
			product: 'string?',
			note: 'string?',
			asset_ids: ['array?', 'number'],
			merchant: 'string?',
		}], ['object', {
			note: 'string',
			lineup: 'string?',
			asset_ids: ['array?', 'number'],
			merchant: 'string?',
		}], ['object', {
			stylecard_variant_ids: ['array', 'number'],
			merchant: 'string?',
		}], ['object', {
			amount: 'number',
		}], ['object', {
			styleboard_id: 'number?',
			stylecard_id: 'number?',
			note: 'string?',
			merchant: 'string?',
		}]]],
	}]])
}

export function checkPayment(data: any) {
	return check(data, ['or', [['object', {
		method: ['enum', [PAYMENT_METHOD.CARD]],
		token: 'string',
		masked_card: 'string?',
		auth_id: 'string?',
	}], ['object', {
		method: ['enum', [PAYMENT_METHOD.VIRTUAL_ACCOUNT]],
		va_bank: ['enum', Object.keys(VA_BANK)],
	}], ['object', {
		method: ['enum', [PAYMENT_METHOD.EWALLET]],
		ewallet: ['enum', [EWALLET.OVO, EWALLET.LINKAJA]],
		phone: 'string',
	}], ['object', {
		method: ['enum', [PAYMENT_METHOD.EWALLET]],
		ewallet: ['enum', [EWALLET.DANA]],
	}], ['object', {
		method: ['enum', [PAYMENT_METHOD.RETAIL_OUTLET]],
		retail_outlet: ['enum', Object.keys(RETAIL_OUTLET)],
	}], ['object', {
		method: ['enum', [PAYMENT_METHOD.PAYLATER]],
		agent: ['enum', [PAYMENT_AGENTS.INDODANA]],
	}]]])
}

export function checkBrandAssetConfig(data: any) {
	return check(data, ['object?', {
		public_id: 'string?',
		folder: 'string?',
		use_filename: 'boolean?',
		unique_filename: 'boolean?',
		resource_type: ['enum?', ['image', 'raw', 'video', 'auto']],
		type: ['enum?', ['upload', 'private', 'authenticated']],
		access_mode: ['enum?', ['public', 'authenticated']],
		discard_original_filename: 'boolean?',
		overwrite: 'boolean?',
		tags: ['array?', 'string'],
		context: ['object?', {}],
		colors: 'boolean?',
		faces: 'boolean?',
		quality_analysis: 'boolean?',
		image_metadata: 'boolean?',
		phash: 'boolean?',
		auto_tagging: 'number?',
		categorization: ['enum?', ['google_tagging', 'google_video_tagging', 'imagga_tagging', 'aws_rek_tagging']], // or comma separated
		format: ['enum?', ['jpg', 'png', 'gif', 'mp4']],
		custom_coordinates: 'string?',
		face_coordinates: ['array?', 'string'],
		allowed_formats: ['array?', ['enum?', ['jpg', 'png', 'gif', 'mp4']]],
		async: 'boolean?',
		backup: 'boolean?',
		invalidate: 'boolean?',
		return_delete_token: 'boolean?',
	}])
}

export function checkBrandAssetMetadata(data: any) {
	return check(data, ['object?', {
		access_mode: ['enum?', ['public', 'authenticated']],
		backup: 'boolean?',
		bytes: 'number?',
		colors: ['array?', ['object', {}]],
		context: ['object?', {}],
		coordinates: ['object?', {}],
		created_at: 'date',
		delete_token: 'string?',
		etag: 'string',
		folders: ['array?', 'string'],
		format: ['enum?', ['jpg', 'png', 'gif', 'mp4']],
		grayscale: 'boolean?',
		height: 'number',
		illustration_score: 'number?',
		image_metadata: ['object?', {}],
		info: ['object?', {}],
		last_updated: 'date?',
		original_filename: 'string',
		pages: 'number?',
		partial: 'boolean?',
		phash: 'string?',
		pixels: 'number',
		placeholder: 'string?',
		predominant: ['object?', {}],
		public_id: 'string',
		resource_type: ['enum?', ['image', 'raw', 'video', 'auto']],
		secure_url: 'string',
		semi_transparent: 'boolean?',
		signature: 'string',
		transformations: ['array?', ['object', {}]],
		time: 'number',
		type: ['enum?', ['upload', 'private', 'authenticated']],
		url: 'string',
		version: 'number',
		width: 'number',
	}])
}

export function createOrUpdateShipmentChecker(
	method: any,
	packet: any,
	// order_detail_ids: any,
	// destination: any,
	type: any,
	amount: any,
	prices: any,
	awb: any,
	url: any,
	note: any,
	metadata: any,
	created_at: any,
	courier: any,
	service: any,
	status: any,
) {
	return check(method, ['enum?', ['TIKI', 'MANUAL', 'NINJA']])
		&& check(packet, ['object?', {
			weight: 'number',
			length: 'number',
			width: 'number',
			height: 'number',
			volume: 'number?'}])
		// && check(req.body.order_detail_ids, '') /// OR SUPPORT!!!!!!!!!
		// && check(destination, ['array?', ['or?', [['object?', {}], 'number?']]])
		&& check(type, ['enum?', ['TIKI', 'MANUAL', 'NINJA']])
		&& check(amount, 'number')
		&& check(prices, ['object', {}])
		&& check(awb, 'string')
		&& check(url, 'string')
		&& check(note, 'string')
		&& check(metadata, ['object', {
			max_delivery_day: 'number?',
			min_delivery_day: 'number?',
		}])
		&& check(created_at, 'date')
		&& check(courier, 'string')
		&& check(service, 'string')
		&& check(status, ['enum', [
			'PENDING',
			'PROCESSING',
			'ON_COURIER',
			'FAILED',
			'DELIVERED',
			'DELIVERY_CONFIRMED',
			'EXCEPTION',
		]]) ? true : false
}
