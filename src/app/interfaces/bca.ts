type channel_type =
  '6010'    // Teller/Branch
  | '6011'  // ATM
  | '6012'  // POS/EDC
  | '6013'  // AutoDebit
  | '6014'  // Internet Banking
  | '6015'  // Kiosk
  | '6016'  // Phone Banking
  | '6017'  // Mobile Banking
  | '6018'  // LLG/Kiriman Uang

type current_code =
  'IDR'
  | 'USD'
  | 'SGD'

type payment_flag_status =
  '00'    // Success
  | '01'  // Reject
  | '02'  // Payment Timeout

export interface InquiryListofBillsRequestInterface {
  CompanyCode: string
  CustomerNumber: string
  RequestID: string
  ChannelType?: channel_type
  TransactionDate?: string
  AdditionalData?: string
}

export interface InquiryListofBillsResponseInterface {
  CompanyCode: string
  CustomerNumber: string
  RequestID: string
  InquiryStatus: payment_flag_status
  InquiryReason: {
	  Indonesian: string
	  English: string,
  },
  CustomerName: string,
  CurrencyCode: current_code
  TotalAmount: string
  SubCompany: string
  DetailBills: Array<{
	  BillDescription: {
		  Indonesian: string
		  English: string,
	},
	  BillAmount: string
	  BillNumber: string
	  BillSubCompany: string,
  }>
  FreeTexts: Array<{
  Indonesian: string
	English: string,
  }>
  AdditionalData: string
}

export interface PaymentFlagRequestInterface {
  CompanyCode: string
  CustomerNumber: string
  RequestID: string
  ChannelType?: channel_type
  CustomerName?: string
  CurrencyCode?: current_code
  PaidAmount?: string
  TotalAmount?: string
  SubCompany?: string
  TransactionDate?: string
  Reference?: string
  DetailBills?: Array<{
	  BillAmount: number
	  BillNumber: string
	  BillSubCompany: string
	  BillReference: number,
  }>
  FlagAdvice?: string
  AdditionalData?: string
}

export interface PaymentFlagResponseInterface {
  CompanyCode: string
  CustomerNumber: string
  RequestID: string
  PaymentFlagStatus: payment_flag_status
  PaymentFlagReason: {
	  Indonesian: string
	  English: string,
  }
  CustomerName: string
  CurrencyCode: current_code
  PaidAmount: string
  TotalAmount: string
  TransactionDate: string
  DetailBills: Array<{
	  BillNumber: string
	  Status: string
	  Reason: {
		  Indonesian: string
		  English: string,
	  },
  }>
  FreeTexts: Array<{
	  Indonesian: string
	  English: string,
  }>
  AdditionalData: string
}
