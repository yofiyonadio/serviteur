import { PacketInterface } from 'energie/app/interfaces'
import { Parameter } from 'types/common'

type delivery_start_time  = '09:00' | '12:00' | '15:00' | '18:00'
type delivery_end_time  = '12:00' | '18:00' | '22:00' | '15:00' | '18:00' | '22:00'

export enum NINJA_EVENT_STATUS {
	'Staging', // A Order has been created and is in Staging phase
	'Pending Pickup', // Order has been confirmed and is pending pickup
	'Van en-route to pickup', // A van has been dispatched to pick up Order
	'En-route to Sorting Hub', // Order has been picked up and is en-route to Sorting Hub
	'Arrived at Sorting Hub', // Order has arrived at Sorting Hub and has been processed successfully
	'Arrived at Origin Hub', // Order has arrived at Origin Hub and has been processed successfully
	'On Vehicle for Delivery', // Order is on van, en-route to delivery
	'Completed', // Delivery has been successfully completed
	'Pending Reschedule', // Delivery has failed and the Order is pending re-schedule
	'Pickup fail', // Pickup has failed and the Order is awaiting re-schedule
	'Cancelled', // Order has been cancelled
	'Returned to Sender', // Delivery of Order has failed repeatedly, sending back to Sender
	'Parcel Size', // The parcel size of an Order has been changed
	'Arrived at Distribution Point', // The parcel has been placed at the Distribution Point for customer collection
	'Successful Delivery', // Proof of delivery is now ready
	'Successful Pickup', // Proof of pickup is now ready
	'Parcel Weight', // The parcel weight of an Order has been changed
	'Cross Border Transit', // Order is in cross border leg or is pending tax payment from consignee if required
	'Customs Cleared', // Order is ready for pickup from customs warehouse
	'Customs Held', // Order is in customs clearance exception
	'Return to Sender Triggered', // Return to Sender request has been triggered,
	'Pending Pickup at Distribution Point', // Order has been received at Distribution Point and is pending pickup
	'Parcel Measurements Update', // The parcel size, or parcel weight, or parcel dimensions of an Order has been changed
}

export enum COUNTRY {
	SG = 'Singapore',
	ID = 'Indonesia',
	MY = 'Malaysia',
	TH = 'Thailand',
	VN = 'Vietnam',
	PH = 'Philliphine',
	MM = 'Myanmar',
}

export enum TIMEZONE {
	ASIA_SINGAPORE = 'Asia/Singapore',
	ASIA_KUALA_LUMPUR = 'Asia/Kuala Lumpur',
	ASIA_JAKATA = 'Asia/Jakarta',
}

export enum SERVICE_TYPE {
	PARCEL = 'Parcel',
	RETURN = 'Return',
	MARKETPLACE = 'Marketplace',
	BULKY = 'Bulky',
	INTERNATIONAL = 'International',
}

export enum SERVICE_LEVEL {
	STANDARD = 'Standard',
	EXSPRESS = 'Express',
	SAMEDAY = 'Sameday',
	NEXTDAY = 'Nextday',
}

export enum PORTATION {
	IMPORT = 'Import',
	EXPORT = 'Export',
}

export interface NinjaInterfaceAddress  {
	postcode: string,
	country: COUNTRY,
	address1: string,
	address2?: string,
	latitude?: string,
	longitude?: string,
}

export interface IndonesiaInterfaceAddressModel extends NinjaInterfaceAddress {
	kelurahan?: string,
	kecamatan?: string,
	city?: string,
	province?: string,
}

export interface MalaysiaInterfaceAddressModel extends NinjaInterfaceAddress {
	area?: string,
	city?: string,
	state?: string,
}

export interface ThailandInterfaceAddressModel extends NinjaInterfaceAddress {
	sub_district?: string,
	district?: string,
	province?: string,
}

export interface PhilippinesInterfaceAddressModel extends NinjaInterfaceAddress {
	subdivision?: string,
	district?: string,
	city?: string,
	province?: string,
}

export interface VietnamInterfaceAddressModel extends NinjaInterfaceAddress {
	district?: string,
	city?: string,
	ward?: string,
}

export interface MyanmarInterfaceAddressModel extends NinjaInterfaceAddress {
	district?: string,
	state?: string,
	township?: string,
}

export interface OtherInterfaceAddressModel extends NinjaInterfaceAddress {
	city?: string,
	state?: string,
}

export interface NinjaShipmentAddressInterface {
	name: string,
	phone_number: string,
	email: string,
	address: IndonesiaInterfaceAddressModel | VietnamInterfaceAddressModel | NinjaInterfaceAddress | MalaysiaInterfaceAddressModel | PhilippinesInterfaceAddressModel | OtherInterfaceAddressModel | ThailandInterfaceAddressModel
}

export interface MarketplaceInterface {
	seller_id: string,  // External reference for this marketplace seller. If your platform is a marketplace, and you are creating an order for a seller on your platform, do pass in the unique seller ID in this field.
	seller_company_name: string,  // Company name of this marketplace seller.
}

export interface ParcelJobInterface {
	dimensions: Parameter<PacketInterface>,
	is_pickup_required?: boolean,
	delivery_start_date?: Date,
	delivery_timeslot?: {
		start_time: delivery_start_time
		end_time: delivery_end_time
		timezone: TIMEZONE,
	},
	delivery_instructions?: string, // Instruction to driver for delivery attempts.
	allow_weekend_delivery?: boolean // default true
	cash_on_delivery?: number // Specifies the cash that should be picked up from recipient of the parcel.
	insured_value?: number // Specifies the desired insured value of the parcel.
	pickup_address?: NinjaInterfaceAddress,
	pickup_service_type?: SERVICE_TYPE,
	pickup_service_level?: SERVICE_LEVEL,
	allow_self_collection?: boolean // Boolean flag that indicates whether or not customer is allowed to self collect the parcels
	pickup_instructions?: string // Instruction to driver for pickup attempts.
}

export interface NotificationNinjaParameterInterface {
	shipper_id: string,
	status: NINJA_EVENT_STATUS,
	timestamp: Date,
	tracking_id: string,
	id?: string,
	order_id?: string,
	shipper_ref_no?: string,
	tracking_ref_no?: string,
	shipper_order_ref_no?: string,
	previous_status?: string,
	comments?: string,
	previous_size?: string, // Size change
	new_size?: string,		// New Size
	previous_weight?: string, // Weight change
	new_weight?: string,     // New weight
	previous_measurements?: { // Parcel Measurements Update
		size?: string,
		measured_weight?: string,
	},
	new_measurements?: {     // New Parcel Measurements Update
		width?: number,
		height?: number,
		length?: number,
		size?: string,
		measured_weight?: number,
		volumetric_weight?: number,
	},
	pod?: { 				// Successful Delivery
		type?: string,
		name?: string,
		identity_number?: string,
		contact?: string,
		uri?: string,
		left_in_safe_place?: boolean,
	}
}



