import bodyParser from 'body-parser'
import Express from 'express'
import { Connection } from 'typeorm'
import Router from './router'
import Middleware from './middleware'
import LoggerHelper from 'energie/utils/helpers/logger'
import Defaults from 'utils/constants/default'

const app = Express()
const port = Number(process.env.PORT) + Number(process.env.NODE_APP_INSTANCE || 0)

Defaults.setIsMain(port === Number(process.env.PORT))


export default {
	init(connection: Connection) {
		app.use(bodyParser.raw({ limit: '100kb', type: 'text/csv' })) // for csv uploading file
		app.use(bodyParser.json({ limit: '40mb' })) // for parsing application/json
		app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

		Middleware.init(app, connection)
		Router.init(app, connection)

		app.use('/', (req, res) => {
			res.json(
				{
					status: 'success',
					data: 'Serviteur Server is Running, Enjoy :)'
				}
			)
		})

		app.listen(port, () => {
			LoggerHelper.log('Yuna Serviteur is running on port ' + port + '!', 'server')
			LoggerHelper.log('---------------------------------------------', 'server')
		})
	},
}
