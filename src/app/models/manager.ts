import {
	Connection, EntityManager,
} from 'typeorm'
import BaseModel from 'energie/app/models/_base'
import RepositoryModel from './repository'

import { boundMethod } from 'autobind-decorator'


class ManagerModel extends BaseModel {

	public static bound = boundMethod

	protected static __type = 'manager'

	protected connection: Connection
	protected initialized: Promise<any>

	async initialize<Repositories extends new(...args: any[]) => RepositoryModel<{}>>(connection: Connection, repositories: Repositories[] = [], initialization: () => any = () => true): Promise<any> {
		if(!this.initialized) {
			this.connection = connection

			repositories.forEach(repository => {
				this[(repository as any).__displayName] = connection.getCustomRepository(repository)
			})

			this.initialized = Promise.resolve().then(initialization)
		}

		return this.initialized
	}

	async transaction<Fn extends (transaction: EntityManager) => any>(fn: Fn): Promise<ReturnType<Fn>> {
		const qR = this.connection.createQueryRunner()

		await qR.connect()

		await qR.startTransaction()

		try {
			const result = await fn(qR.manager)

			await qR.commitTransaction()
			// if(qR.isReleased) {
				await qR.release()
			// }

			return result
		} catch (err) {

			await qR.rollbackTransaction()
			await qR.release()

			throw err
		}
	}

	// tslint:disable-next-line:ban-types
	protected async withTransaction<T extends (trx: EntityManager) => any>(transaction: EntityManager | undefined, fn: T): Promise<ReturnType<T>> {
		if(transaction === undefined) {
			return this.transaction((trx: EntityManager) => {
				return fn(trx)
			})
		} else {
			return fn(transaction)
		}
	}
}


export default ManagerModel
