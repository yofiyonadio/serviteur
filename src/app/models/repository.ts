import {
	EntityManager,
	IsNull,
	SaveOptions,
	FindOperator,
} from 'typeorm'

import BaseModel from 'energie/app/models/_base'
import PlainRecordModel from 'energie/app/models/plain.record'

import ErrorModel, { ERRORS, ERROR_CODES } from './error'

import { boundMethod } from 'autobind-decorator'

import {
	memoize,
	isEqual,
} from 'lodash'

const RECURSIZE = 2048

const haveDeletedColumn = memoize(record => {
	if (Object.keys(record).indexOf('deleted_at') > -1) {
		return [true, 'deleted_at']
	} else if (Object.keys(record).indexOf('expired_at') > -1) {
		return [true, 'expired_at']
	} else if (Object.keys(record).indexOf('cancelled_at') > -1) {
		return [true, 'cancelled_at']
	}

	return false
})

type Record<T, U extends keyof T> = InstanceType<new (U: string) => T[U]>
type NoRecordKeys<T> = { [t in keyof T]: T[t] extends PlainRecordModel | PlainRecordModel[] ? never : t }[keyof T]
type NoFunctionKeys<T> = { [t in keyof T]: T[t] extends ((...args: any) => any) ? never : t}[keyof T]
type NoReadonlyKeys<T> = { [t in keyof T]: 'readonly' extends keyof T[t] ? never : t }[keyof T]
type Bite<T, U, V = Pick < T, NoReadonlyKeys< Pick< T, NoFunctionKeys< Pick< T, NoRecordKeys<T>>>>>>> = Pick < V, {
	[Key in keyof V]: V[Key] extends U ? Key : never
}[keyof V]>
export type Keys<T> = Partial<T>
export type KeyOrKeys<T> = Keys<T> | Array<Keys<T>>
export type Where<T, U = Keys<T>> = {
	[u in keyof U]?: U[u] | FindOperator<any>
}
export type Order<T, U = Keys<T>, V = {
	[k in keyof U]?: 'ASC' | 'DESC'
}> = Partial<V>
export type Relations<T, U = Partial< Bite< T, PlainRecordModel | PlainRecordModel[], Pick<T, NoReadonlyKeys<Pick<T, NoFunctionKeys< T>>>>>>> = Array<keyof U>
export type Selections<T> = Array<keyof Keys<T>>



// type Json = SubType<{
// 	id: string,
// 	firstName: string,
// 	cool: Date,
// }, string>


export default class RepositoryModel<Records extends {
	[k: string]: PlainRecordModel,
} = {}> extends BaseModel {

	public static bound = boundMethod

	protected static __type = 'repository'

	protected records: {
		[k in keyof Records]: typeof PlainRecordModel
	}

	protected manager: EntityManager

	constructor(
		manager: EntityManager,
		autobind: string[] = [],
	) {
		super([
			'getOne',
			'getMany',
			'getManyAndCount',
			'parser',
			'query',
			'save',
			...autobind,
		])

		this.manager = manager
		return this
	}

	// async bump<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, id: number, transaction: EntityManager): Promise<boolean> {
	// 	return await this.update(RecordName, {
	// 		id,
	// 	} as any, {}, {
	// 		transaction,
	// 	})
	// }

	async saveOrUpdate<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data: Array<Keys<RecordType>>, transaction?: EntityManager): Promise<boolean> {
		if (data.length) {
			const _data = data.shift()
			const id = (_data as any).id
			const manager = transaction || this.manager

			// tslint:disable-next-line: no-string-literal
			delete _data['id']

			if (!id) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Must supply id.')
			}

			const Model = this.records[Key]
			const keys = Object.keys(_data)
			const isExist = await this.getOne(Key, {
				id,
			} as any, {
				select: ['id' as any],
				transaction: manager,
			}).then(() => true).catch(err => false)

			if (isExist) {
				await manager.query(`
					UPDATE "${Model.table.schema}"."${Model.table.name}"
					SET ${ keys.map(key => {
					return `"${key}" = ${typeof _data[key] === 'number' ? _data[key] : _data[key] === null ? 'null' : typeof _data[key] === 'string' ? `'${_data[key].replace(/'/gi, '\'\'')}'` : `${_data[key]}`}`
				}).join(', ')}
					WHERE id = ${id}
				`)
			} else {
				await manager.query(`
					INSERT INTO "${Model.table.schema}"."${Model.table.name}"(
						"id", ${ keys.map(key => `"${key}"`).join(', ') }
					) VALUES (${id}, ${ keys.map(key => typeof _data[key] === 'number' ? _data[key] : _data[key] === null ? 'null' : typeof _data[key] === 'string' ? `'${_data[key].replace(/'/gi, '\'\'')}'` : `${_data[key]}`) })
				`)
			}

			this.log(`${Model.table.name} with id = ${id} done migrated`)

			return this.saveOrUpdate(Key, data, transaction)
		} else {
			// habis
			return true
		}
	}

	async insertOrUpdate<T>(table: string, values: object[], conflictTarget: string[] | undefined, transaction: EntityManager): Promise<boolean> {
		const manager = transaction || this.manager
		const chunk = values.splice(0, RECURSIZE)
		const column = Object.keys(chunk[0] as any)

		manager.createQueryBuilder()
			.insert()
			.into<T>(table as any, column)
			.values(chunk)
			.orUpdate({conflict_target: conflictTarget ? conflictTarget : [`${column[0]}`], overwrite: column.map(c => `${c}`)})
			.returning(`${table}.${column[0]}`)
			.execute()
			.then(q => {
				q.raw.map(log => {
					this.log(`Insert or Update ${table} ${column[0]} ${log[Object.keys(log)[0]]}`)
				})
			})

		return values.length ? this.insertOrUpdate(table, values, conflictTarget, transaction) : true
	}

	// async save<T>(RecordModel: T, SaveOption?: SaveOptions): Promise<T>
	protected create<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data?: Keys<RecordType>): RecordType
	protected create<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data?: Array<Keys<RecordType>>): RecordType[]
	protected create<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data?: KeyOrKeys<RecordType>): RecordType | RecordType[] {

		const Model = this.records[Key]
		const isArray = Array.isArray(data)
		const datas = isArray ? data as Array<Keys<RecordType>> : null
		const dataa = isArray ? null : data as Keys<RecordType>

		let instances: PlainRecordModel | PlainRecordModel[]

		if (isArray && datas.length) {
			instances = datas.map((d: Keys<RecordType>) => {
				return new Model().update(d)
			})
		} else if (!isArray && !isEqual(dataa, {})) {
			instances = new Model().update(dataa)
		} else {
			instances = new Model()
		}

		if(isArray && datas.length) {
			return instances as RecordType[]
		} else {
			return instances as RecordType
		}

	}

	protected async getOne<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, where?: Where<RecordType>, getOptions?: {
		excludeDeleted?: boolean,
		relations?: Relations<RecordType>,
		select?: Selections<RecordType>,
		order?: Order<RecordType>,
		transaction: EntityManager,
	}): Promise<RecordType> {
		return this.__get(RecordName, where, getOptions)
	}

	protected async getMany<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, where?: Where<RecordType>, getOptions?: {
		excludeDeleted?: boolean,
		relations?: Relations<RecordType>,
		select?: Selections<RecordType>,
		order?: Order<RecordType>,
		skip?: number,
		take?: number,
		transaction: EntityManager,
	}): Promise<RecordType[]> {
		return this.__get(RecordName, where, {
			...getOptions,
			many: true,
		})
	}

	protected async getManyAndCount<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, where?: Where<RecordType>, getOptions?: {
		excludeDeleted?: boolean,
		relations?: Relations<RecordType>,
		select?: Selections<RecordType>,
		order?: Order<RecordType>
		skip?: number,
		take?: number,
		transaction?: EntityManager,
	}): Promise<RecordType[]> {
		return this.__get(RecordName, where, {
			...getOptions,
			many: true,
			count: true,
		})
	}

	protected parseCount<T>(queryResult: [T, number]): {
		data: T,
		count: number,
	} {
		return {
			data: queryResult[0],
			count: queryResult[1],
		}
	}

	protected parser<T>(queryResult: T): T {
		if (queryResult === undefined || Array.isArray(queryResult) && !queryResult.length) {
			// tslint:disable-next-line: no-arg
			throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, `Repository: ${ this.constructor.name }`)
		}

		return queryResult
	}

	// protected parser(detail: string) {
	// 	return <T>(queryResult: T): T => {
	// 		if (queryResult === undefined || Array.isArray(queryResult) && !queryResult.length) {
	// 			throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, detail)
	// 		}

	// 		return queryResult
	// 	}
	// }

	protected query(table: string, alias?: string, transaction?: EntityManager) {
		const manager = transaction || this.manager
		const query = manager.createQueryBuilder(table, alias || table)

		return query

		// if(transaction) {
		// 	// return query
		// 	return query.setLock('pessimistic_read')
		// } else {
		// 	return query
		// }
	}

	protected queryCustom<T>(query: string, parameters?: any[], transaction?: EntityManager): Promise<T> {
		const manager = transaction || this.manager

		return manager.query(query, parameters)
	}

	protected queryInsert<T>(table: string, transaction?: EntityManager) {
		const manager = transaction || this.manager
		const query = manager.createQueryBuilder().insert().into<T>(table as any)

		return query
	}

	protected queryUpdate<T>(table: string, transaction?: EntityManager) {
		const manager = transaction || this.manager
		const query = manager.createQueryBuilder().update<T>(table as any)

		return query
	}

	protected queryDelete<T>(table: string, transaction?: EntityManager) {
		const manager = transaction || this.manager
		const query = manager.createQueryBuilder().delete().from<T>(table as any)

		return query
	}

	protected async save<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data: Keys<RecordType>, option?: SaveOptions | EntityManager): Promise<RecordType>
	protected async save<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data: Array<Keys<RecordType>>, option?: SaveOptions | EntityManager): Promise<RecordType[]>
	protected async save<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data: KeyOrKeys<RecordType>, option?: SaveOptions | EntityManager): Promise<RecordType | RecordType[]> {

		let instances
		const manager = option instanceof EntityManager ? option : this.manager
		const saveOption = option instanceof EntityManager ? undefined : option

		if(Array.isArray(data)) {
			instances = this.create(Key, data as Array<Keys<RecordType>>)
		} else {
			instances = this.create(Key, data as Keys<RecordType>)
		}

		await manager.save(instances, saveOption)

		return instances

	}

	protected async saveRecursive<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(Key: RecordKey, data: Array<Keys<RecordType>>, option?: SaveOptions | EntityManager, saved: RecordType[] = []): Promise<RecordType[]> {
		let instances
		const manager = option instanceof EntityManager ? option : this.manager
		const saveOption = option instanceof EntityManager ? undefined : option

		instances = this.create(Key, data.splice(0, RECURSIZE) as Array<Keys<RecordType>>)

		await manager.save(instances, saveOption)

		if(data.length) {
			return this.saveRecursive(Key, data, option, saved.concat(instances))
		}

		return saved.concat(instances)
	}

	protected async saveBig<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(schema: string, table: string, data: Array<Keys<RecordType>>, option?: SaveOptions | EntityManager): Promise<boolean> {
		// const record = this.records[Key]
		const manager = option instanceof EntityManager ? option : this.manager
		// const saveOption = option instanceof EntityManager ? undefined : option

		const datas = data.splice(0, RECURSIZE)
		const keys = Object.keys(datas[0])

		await manager.query(`
			INSERT INTO "${schema}"."${table}"(
				${keys.map(key => `"${key}"`).join(', ')}
			) VALUES ${datas.map((d, i) => `(${Object.keys(d).map((k, n, o) => `$${(n + 1) + (i * o.length)}`).join(', ')})`).join(', ')}
		`, datas.map(d => keys.map(k => d[k])).reduce((sum, ds) => sum.concat(ds), []))

		if(data.length) {
			return this.saveBig(schema, table, data, option)
		}

		await manager.query(`
			SELECT setval('${schema}.${table}_id_seq', (SELECT MAX(id) FROM "${schema}"."${table}"));
		`)

		return true
	}

	protected async renew<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, where: number | string, data: Keys<RecordType>, {
		excludeDeleted,
		transaction,
		parameters,
	}: {
		excludeDeleted?: boolean,
		transaction ?: EntityManager,
		saveOptions?: Partial<SaveOptions>,
		parameters?: object,
	} = {}): Promise<boolean> {
		const _record = this.records[RecordName]
		const _excludeDeleted = excludeDeleted === undefined ? true : excludeDeleted
		const haveDeleted = _excludeDeleted ? haveDeletedColumn(_record) : false
		const manager = transaction || this.manager

		if((data as any).updated_at) {
			this.warn('Cannot set updated at!')
		}

		return manager.createQueryBuilder()
			.update(_record)
			.set(data)
			.where(typeof where === 'number' ? `id = :id${ haveDeleted ? ` AND ${ haveDeleted[1] } IS NULL` : ''}` : where, {
				id: typeof where === 'number' ? where : null,
				...(parameters || {}),
			})
			.returning(['id'])
			.execute().then(d => {
				return !!d.raw.length
			}).catch(err => {
				this.warn(err)

				return false
			})

		// return await manager.update(_record, haveDeleted ? {
		// 	...where,
		// 	[haveDeleted[1] as string]: IsNull(),
		// } : where, data as any).then(d => {
		// 	this.log(d, d.raw.affectedRows)
		// 	return true
		// }).catch(err => {
		// 	this.warn(err)

		// 	return false
		// })
	}

	private async __get<RecordKey extends keyof Records, RecordType extends Record<Records, RecordKey>>(RecordName: RecordKey, where: Where<RecordType>, getOptions: {
		excludeDeleted?: boolean,
		relations?: Relations<RecordType>,
		select?: Selections<RecordType>, // todo
		order?: Order<RecordType>,
		skip?: number,
		take?: number,
		many?: boolean,
		count?: boolean,
		transaction?: EntityManager,
	} = {}): Promise<any> {
		const _record = this.records[RecordName]
		const _where = where || {}
		const excludeDeleted = getOptions.excludeDeleted === undefined ? true : getOptions.excludeDeleted
		const haveDeleted = excludeDeleted ? haveDeletedColumn(_record) : false
		const manager = getOptions.transaction ? getOptions.transaction : this.manager
		// const lock = getOptions.transaction ? { lock: { mode: 'pessimistic_read' } } : {}
		const whereClause = haveDeleted ? {
			where: {
				..._where,
				[haveDeleted[1] as string]: IsNull(),
			},
		} : where ? { where } : {}

		if(getOptions.many && getOptions.count) {
			return manager.findAndCount(_record as any, {
				...whereClause,
				// ...lock,
				select: getOptions.select,
				relations: getOptions.relations as string[],
				order: getOptions.order as any,
				skip: getOptions.skip,
				take: getOptions.take,
			}).then(this.parser)
		} else if(getOptions.many) {
			return manager.find(_record as any, {
				...whereClause,
				// ...lock,
				select: getOptions.select,
				relations: getOptions.relations as string[],
				order: getOptions.order as any,
				skip: getOptions.skip,
				take: getOptions.take,
			}).then(this.parser)
		} else {
			return manager.findOne(_record as any, whereClause.where || {}, {
				// ...lock as any,
				select: getOptions.select,
				relations: getOptions.relations as string[],
				order: getOptions.order,
			}).then(this.parser)
		}
	}

}
