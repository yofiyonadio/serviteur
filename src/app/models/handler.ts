import BaseModel from 'energie/app/models/_base'
import Immutable from 'immutable'


export default class HandlerModel<T extends {
	[k: string]: any,
}> extends BaseModel {

	protected static __type = 'handler'
	private _init: boolean
	private _config: any
	private _record: any
	private _model: any

	constructor(model: T, autobind: string[] = []) {
		super([
			'init',
			'set',
			'get',
			...autobind,
		])

		this._init = false
		this._config = model
		this._record = Immutable.Record(model, this.__displayName)
		this._model = new this._record()

		this.init()
	}

	get config() {
		return this._config
	}

	get model() {
		return this._model
	}

	init() {
		if (this._init) {
			this._init = true
		}
	}

	set(key: keyof T, value: any) {
		this.model.set(key, value)

		return this
	}

	get(key: keyof T) {
		return this.model.get(key)
	}
}
