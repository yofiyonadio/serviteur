import BaseModel from 'energie/app/models/_base'
import ErrorModel, { ERRORS, ERROR_CODES } from './error'
import code from 'utils/constants/code'

import {
	isObject,
	pick,
} from 'lodash'
import {
	Response,
} from 'express'
import { Connection, EntityManager } from 'typeorm'
import { Await } from 'types/common'


class ControllerModel extends BaseModel {

	protected static __type = 'controller'

	protected connection: Connection = null

	responder(response: Response, {
		whitelist,
		picker = pick,
	}: {
		whitelist?: string[] | boolean,
		picker?: (data: any, whitelist: string[] | boolean) => {},
	} = {}, iteration: number = 0) {
		return (data: any) => {
			if(iteration === 0) {
				return response.json(this.responder(response, { whitelist, picker }, iteration + 1)(data))
			} else {
				if (Array.isArray(data)) {
					// AN ARRAY
					return data.map(d => {
						return this.responder(response, { whitelist, picker }, iteration + 1)(d)
					})
				} else {
					// NOT AN ARRAY
					if (isObject(data)) {
						if(whitelist) {
							if(whitelist === true) {
								return data
							} else {
								return picker(data, whitelist)
							}
						} else if((data as any).columns) {
							return picker(data, (data as any).columns)
						} else {
							return data
						}
					} else {
						return data
					}
				}
			}
		}
	}

	catcher(response: Response) {
		return (err: Error | ErrorModel) => {
			this.warn(err)
			if(err instanceof ErrorModel) {
				return response.status(this.getStatus(err.type)).json(err)
			} else {
				return response.status(code.httpstatus.internalerror).json(new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, {
					name: err.name,
					message: err.message,
				}))
			}
		}
	}

	route(): object {
		return {}
	}

	setConnection(connection: Connection) {
		this.connection = connection

		return true
	}

	async transaction<T extends (trx: EntityManager) => Promise<any>>(fn: T): Promise<Await<ReturnType<T>>> {
		const qR = this.connection.createQueryRunner()

		await qR.connect()

		await qR.startTransaction()

		try {
			const result = await fn(qR.manager)

			await qR.commitTransaction()
			// if(qR.isReleased) {
			await qR.release()
			// }

			return result
		} catch (err) {

			await qR.rollbackTransaction()
			await qR.release()

			throw err
		}
	}

	private getStatus(type: ERRORS) {
		switch (type) {
		case ERRORS.UNKNOWN:
		default:
			return code.httpstatus.internalerror

		case ERRORS.NODATA:
			return code.httpstatus.notfound

		case ERRORS.NOT_AUTHORIZED:
			return code.httpstatus.unauthorized

		case ERRORS.NOT_FOUND:
			return code.httpstatus.notfound

		case ERRORS.NOT_SATISFIED:
			return code.httpstatus.badrequest

		case ERRORS.NOT_VALID:
			return code.httpstatus.badrequest

		case ERRORS.NOT_VERIFIED:
			return code.httpstatus.forbidden
		}
	}
}


export default ControllerModel
