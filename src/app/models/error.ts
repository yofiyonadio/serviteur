import BaseModel from 'energie/app/models/_base'

// import _ from 'lodash'

export enum ERRORS {
	'UNKNOWN' = 'UNKNOWN',
	'NODATA' = 'NODATA',
	'NOT_VALID' = 'NOT_VALID',
	'NOT_FOUND' = 'NOT_FOUND',
	'NOT_VERIFIED' = 'NOT_VERIFIED',
	'NOT_AUTHORIZED' = 'NOT_AUTHORIZED',
	'NOT_SATISFIED' = 'NOT_SATISFIED',
}

export enum ERROR_CODES {
	// 'ERR_000' = 'ERR_000',
	// 'ERR_001' = 'ERR_001',
	// 'ERR_002' = 'ERR_002',
	// 'ERR_003' = 'ERR_003',
	// 'ERR_004' = 'ERR_004',
	// 'ERR_005' = 'ERR_005',
	// 'ERR_006' = 'ERR_006',
	// 'ERR_007' = 'ERR_007',
	// 'ERR_008' = 'ERR_008',
	// 'ERR_009' = 'ERR_009',
	'ERR_010' = 'ERR_010',
	// 'ERR_011' = 'ERR_011',
	// 'ERR_012' = 'ERR_012',
	// 'ERR_013' = 'ERR_013',
	// 'ERR_014' = 'ERR_014',
	// 'ERR_015' = 'ERR_015',
	// 'ERR_016' = 'ERR_016',
	'ERR_017' = 'ERR_017',
	'ERR_018' = 'ERR_018',
	'ERR_019' = 'ERR_019',
	// 'ERR_020' = 'ERR_020',
	// 'ERR_021' = 'ERR_021',
	// 'ERR_022' = 'ERR_022',
	// 'ERR_023' = 'ERR_023',
	// 'ERR_024' = 'ERR_024',
	// 'ERR_025' = 'ERR_025',
	// 'ERR_026' = 'ERR_026',
	// 'ERR_027' = 'ERR_027',
	// 'ERR_028' = 'ERR_028',
	'ERR_029' = 'ERR_029',
	// 'ERR_030' = 'ERR_030',
	// 'ERR_031' = 'ERR_031',
	// 'ERR_032' = 'ERR_032',
	// 'ERR_033' = 'ERR_033',
	// 'ERR_034' = 'ERR_034',
	// 'ERR_035' = 'ERR_035',
	// 'ERR_036' = 'ERR_036',
	'ERR_037' = 'ERR_037',
	// 'ERR_038' = 'ERR_038',
	// 'ERR_039' = 'ERR_039',
	// 'ERR_040' = 'ERR_040',
	// 'ERR_041' = 'ERR_041',
	// 'ERR_042' = 'ERR_042',
	// 'ERR_043' = 'ERR_043',
	'ERR_099' = 'ERR_099|Unhandled error occured',
	'ERR_100' = 'ERR_100|Parameter not satisfied',
	'ERR_101' = 'ERR_101|NULL',
	'ERR_102' = 'ERR_102|Need additional privilege',
	'ERR_103' = 'ERR_103|Parameter invalid',
	'ERR_104' = 'ERR_104|One of item(s) is unavailable',
	'ERR_105' = 'ERR_105|Result invalid',
	'ERR_106' = 'ERR_106|Try again later',
	'ERR_107' = 'ERR_107|Multiple results found',
	'ERR_108' = 'ERR_108|Duplicate value',
	'ERR_109' = 'ERR_109|Need consent',
}


class ErrorModel extends BaseModel {

	protected static __type = 'error'

	type: ERRORS
	code: string
	message: string
	detail: any
	is: (...args: ERROR_CODES[]) => boolean

	constructor(type: ERRORS, code: ERROR_CODES, detail?: any) {
		super()
		const messages = code.split('|')

		this.type = type
		this.code = messages[0]
		this.message = messages[1]
		this.detail = detail

		this.is = (...codes: ERROR_CODES[]): boolean => {
			return codes.findIndex(c => c === code) > -1
		}
	}
}


export default ErrorModel
