
import { Axios } from 'utils/helpers'

// import LoggerHelper from 'energie/utils/helpers/logger'
import { PoolClient } from 'pg'

import { ListenerManager } from './managers'
import ListenerInterface from '../utils/constants/listener'
class Listener {

    notify(client: PoolClient) {
        client.query('LISTEN event_global_listener')
        client.query('LISTEN event_mv_listener')
        client.on('notification', async res => {
            const payload = JSON.parse(res.payload) as ListenerInterface

            if ((JSON.stringify(payload.new) !== JSON.stringify(payload.old) && payload.type === 'UPDATE') || (payload.type !== 'UPDATE')) {

                if (res.channel === 'event_mv_listener') {
                    const data = JSON.parse(res.payload)
                    // LoggerHelper.log(`REFRESH MATERIALIZED VIEW CONCURRENTLY ${data.mview}...`, 'server')
                    client.query(`REFRESH MATERIALIZED VIEW CONCURRENTLY ${data.mview};`)
                        .then(() => {
                            // LoggerHelper.log(`REFRESH MATERIALIZED VIEW ${data.mview} Success!`, 'server')
                        })
                        .catch(err => err)
                }

                if (res.channel === 'event_global_listener' && payload.exec_type === 'internal') {
                    ListenerManager.executor(payload)
                }

                if (res.channel === 'event_global_listener' && payload.exec_type === 'external') {
                    Promise.allSettled(payload.target_uri.map(async uri => {
                        return new Promise(async (resolve, reject) => {
                            Axios.method(payload.method, uri, '')
                                .then((res: any) => res.status === 200 ? resolve(uri) : reject(uri))
                                .catch(err => reject(uri))
                        })
                    })).then(result => {
                        result.forEach((res: any) => {
                            if (res.status === 'fulfilled') {
                                // success
                            } else {
                                // failed
                            }
                        })
                    }).catch(error => {
                        // failed
                    })
                }
            }

            // ----------------------------------------------


        })
    }

}

export default new Listener()