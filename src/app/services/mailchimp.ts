import ServiceModel from 'app/models/service'

// import TimeHelper from 'utils/helpers/time';


class MailchimpService extends ServiceModel {
	constructor() {
		super(process.env.MAILCHIMP_API, 0)
	}

	subscribe(email: string, tags: string[] = [], config: {
		list_id: string,
		token: string,
	}) {
		return this.post(`lists/${config.list_id}/members/`, {
			data: {
				email_address: email,
				status: 'subscribed',
				tags,
			},
			headers: {
				Authorization: `user ${config.token}`,
			},
		})
	}
}

export default new MailchimpService()
