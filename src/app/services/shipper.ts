import ServiceModel from 'app/models/service'

// import TimeHelper from 'utils/helpers/time';


class ShipperService extends ServiceModel {
	constructor() {
		super(process.env.SHIPPER_API + 'public/v1/', 0)
	}

	async getProvinces(): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				id: number,
				name: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('provinces', {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getAllCities(): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				id: number,
				name: string,
				province_id: number,
				province_name: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('cities', {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
				origin: 'all',
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getCities(province_id: number): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				id: number,
				name: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('cities?province=' + province_id, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getSuburbs(city_id: number): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				id: number,
				name: string,
				alias: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('suburbs?city=' + city_id, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getAreas(suburb_id: number): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				id: number,
				name: string,
				alias: string,
				postcode: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('areas?suburb=' + suburb_id, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getAreasFromPostal(postal: string): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rows: Array<{
				area_id: number,
				area_name: string,
				area_alias: string,
				suburb_id: number,
				suburb_name: string,
				suburb_alias: string,
				city_id: number,
				city_name: string,
				province_id: number,
				province_name: string,
			}>,
			statusCode: number,
		},
	}> {
		return this.get('details/' + postal, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async checkPrice<T extends {
		logo_url: string,
		rate_id: number,
		rate_name: string,
		stop_origin: number,					// origin area id
		stop_destination: number,				// destination area id
		weight: number,
		volumeWeight: number,
		finalWeight: number,
		itemPrice: number,
		name: string,
		finalRate: number,
		insuranceRate: number,
		compulsory_insurance: number,
		liability: number,
		discount: number,
		min_day: number,
		max_day: number,
	}>(
		originAreaId: number,
		targetAreaId: number,
		weight: number,
		length: number,
		width: number,
		height: number,
		price: number,
	): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			rule: string,
			originArea: string,
			destinationArea: string,
			rates: {
				logistic: {
					regular: T[],
					express: T[],
				},
			},
			statusCode: number,
		},
	}> {
		return this.get('domesticRates', {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
				o: originAreaId,
				d: targetAreaId,
				wt: weight,
				l: length,
				w: width,
				h: height,
				v: price,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async createDomesticOrder(
		originAreaId: number,
		targetAreaId: number,
		weight: number,
		length: number,
		width: number,
		height: number,
		price: number,

		rateID: number,

		consignerName: string,
		consignerPhoneNumber: string,
		originAddress: string,
		originDirection: string,

		consigneeName: string,
		consigneePhoneNumber: string,
		destinationAddress: string,
		destinationDirection: string,

		itemName: string,
		contents: string,

		useInsurance: 0 | 1,
		packageType: 1 | 2 | 3,
		externalID: string,
		paymentType: 'cash' | 'postpay',
		cod: 0 | 1,
	): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			statusCode: number,
			id: string,
		},
	}> {
		return this.post('orders/domestics', {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			data: {
				o: originAreaId,
				d: targetAreaId,
				wt: weight,
				l: length,
				w: width,
				h: height,
				v: price,

				rateID,

				consignerName,
				consignerPhoneNumber,
				originAddress,
				originDirection,

				consigneeName,
				consigneePhoneNumber,
				destinationAddress,
				destinationDirection,

				itemName,
				contents,

				useInsurance,
				packageType,
				externalID,
				paymentType,
				cod,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
				'Content-Type': 'application/json',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async orderActivation(order_id: string, active: boolean): Promise<{
		status: string,
		data: {
			message: string,
			statusCode: number,
		},
	}> {
		return this.put('activations/' + order_id, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
			},
			data: {
				active,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
				'Content-Type': 'application/json',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getTrackingOrderId(id: string): Promise<{
		status: string,
		data: {
			id: string,
			title: string,
			content: string,
			statusCode: number,
		},
	}> {
		return this.get('orders', {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
				id,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	async getOrderDetail(orderID: string): Promise<{
		status: string,
		data: {
			title: string,
			content: string,
			order: {
				tracking: [{
					_id: string,
					trackStatus: {
						name: string,
						description: string,
					},
					logisticStatus: [{
						name: string,
						description: string,
					}],
					createdBy: string,
					createdDate: string,
				}],
				detail: {
					id: string,
					externalID: string,
					labelChecksum: string,
					consigner: {
						id: string,
						name: string,
						phoneNumber: string,
					},
					consignee: {
						id: string,
						name: string,
						phoneNumber: string,
					},
					awbNumber: string,
					package: {
						type: string,
						itemName: string,
						contents: string,
						price: {
							value: number,
							UoM: string,
						},
						dimension: {
							length: {
								value: number,
								UoM: 'cm',
							},
							height: {
								value: number,
								UoM: 'cm',
							},
							width: {
								value: number,
								UoM: 'cm',
							},
							weight: {
								value: number,
								UoM: 'kg',
							},
							volumeWeight: {
								value: number,
								UoM: 'kg',
							},
							pictureURL: string,
							isConfirmed: 0 | 1,
						},
					},
					origin: {
						address: string,
						direction: string,
						cityID: number,
						cityName: string,
						provinceID: number,
						provinceName: string,
					},
					destination: {
						address: string,
						direction: string,
						cityID: number,
						cityName: string,
						provinceID: number,
						provinceName: string,
					},
					driver: {
						name: string,
						phoneNumber: string,
						vehicleType: string,
						vehicleNumber: string,
					},
					courier: {
						rate_id: number,
						rate_name: string,
						name: string,
						shipmentType: string,
						min_day: number,
						max_day: number,
						rate: {
							value: number,
							UoM: 'IDR',
						},
					},
					creationDate: string,
					lastUpdatedDate: string,
					useInsurance: 0 | 1,
				},
			},
			statusCode: number,
		},
	}> {
		return this.get('orders/' + orderID, {
			query: {
				apiKey: process.env.SHIPPER_TOKEN,
				orderID,
			},
			headers: {
				'Accept': 'application/json',
				'User-Agent': 'Mozilla',
			},
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new ShipperService()
