import ServiceModel from 'app/models/service'


class JenkinsService extends ServiceModel {
	constructor() {
		super('https://jenkins.helloyuna.io/job/send_whatsapp_message/buildWithParameters', 0)
	}

	notifyByNewStyleboard(config: {
		token: string,
		phone: string,
		msg: string,
	}) {
		return this.get('', {
			query: { ...config },
			auth: {
				username: 'yuna_build',
				password: '114d855c91da5acd2e29f9d165cadb70e6',
			}
		})
	}
}

export default new JenkinsService()
