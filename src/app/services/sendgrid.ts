import {
  // ErrorModel,
  ServiceModel,
} from 'app/models'

class SendgridService extends ServiceModel {
  constructor() {
    super('', 0)
  }

  async send() {
    return this.post(`${process.env.SENDGRID_URL}`, {
      headers: {
        'Authorization': `Bearer ${process.env.SENDGRID_KEY}`,
      },
      data: {
        personalizations: [{
          to: [{
            email: "rheza@helloyuna.io",
            name: "Rheza As"
          }],
          subject: "Testingg"
        }],
        from: {
          email: "devs@helloyuna.com",
          name: "Dev"
        },
        content: [{
          type: "text/plain",
          value: "Attemp 1 test"
        }]

      }
    }).then(res => {
      return res
    })
  }

}

export default new SendgridService()
