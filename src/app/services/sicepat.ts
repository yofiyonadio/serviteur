import {
	// ErrorModel,
	ServiceModel,
} from 'app/models'

class SicepatService extends ServiceModel {

	// private token: string

	constructor() {
		super('', 0)

		// this.token = undefined
		// this.authentication = this.authentication.bind(this)
	}

	async getOriginArea(): Promise<Array<{
		origin_name: string,
		origin_code: string,
	}>> {
		return this.get(`${ process.env.SICEPAT_API }/customer/origin`, {
			headers: {
				'api-key': process.env.API_KEY,
			},
		}).then(res => {
			return res.sicepat.results
		})
	}

	async getDestinationArea(): Promise<Array<{
		destination_code: string,
		subdistrict: string,
		city: string,
		province: string,
	}>> {
		return this.get(`${ process.env.SICEPAT_API }/customer/destination`, {
			headers: {
				'api-key': process.env.API_KEY,
			},
		}).then(res => {
			return res.sicepat.results
		})
	}

	async getService(
		origin: string,
		destination: string,
		weight: number,
	): Promise<Array<{
		service: string,
		description: string,
		tariff: number,
		etd: string,
	}>> {
		return this.get(`${ process.env.SICEPAT_API }/customer/tariff?origin=${ origin }&destination=${ destination }&weight=${ weight }`, {
			headers: {
				'api-key': process.env.API_KEY,
			},
		}).then(res => {
			return res.sicepat.results
		})
	}

	async trackPacket(waybill: string): Promise<{
		waybill_number: string,
		kodeasal: string,
		kodetujuan: string,
		service: string,
		estimasi: string,
		weight: string,
		resi_JNE: string,
		sender: string,
		sender_address: string,
		receiver_address: string,
		receiver_name: string,
		realprice: string,
		POD_receiver: string,
		POD_receiver_time: string,
		send_date: string
		track_history: Array<{
			date_time: string,
			status: string,
			city: string,
			receiver_name: string,
		}>,
		last_status: {
			date_time: string,
			status: string,
			city: string,
			receiver_name: string,
		},
		perwakilan: string,
	}> {
		return this.get(`${ process.env.SICEPAT_API }/customer/waybill?waybill=${ waybill }`, {
			headers: {
				'api-key': process.env.API_KEY,
			},
		}).then(res => {
			return res.sicepat.result
		})
	}

	async createShipment(
		requestDate: string,
		originCode: string,
		referenceNumber: string,
		receiptNumber: string,
		price: number,
		type: string,
		merchant: {
			title: string,
			receiver: string,
			phone: string,
			address: string,
			district: string,
			city: string,
			province: string,
			postal: string,
			code: string,
		},
		receiver: {
			title: string,
			receiver: string,
			phone: string,
			address: string,
			district: string,
			city: string,
			province: string,
			postal: string,
			code: string,
		},
		packet: {
			weight: number,
			length: number,
			width: number,
			height: number,
		},
	): Promise<{
		request_number: string,
		receipt_datetime: string,
		datas: Array<{
			cust_package_id: string,
			receipt_number: string,
		}>,
	}> {
		return this.post(`${ process.env.SICEPAT_API_PICKUP }/api/partner/requestpickuppackage`, {
			headers: {
				'Content-Type': 'application/json',
			},
			data: {
				auth_key: process.env.AUTH_KEY,
				reference_number: referenceNumber,
				pickup_request_date: requestDate,
				pickup_merchant_name: merchant.title,
				pickup_address: merchant.address,
				pickup_city: merchant.city,
				pickup_merchant_phone: merchant.phone,
				pickup_merchant_email : 'devs@helloyuna.io',
				PackageList: [{
					receipt_number: receiptNumber,
					origin_code: originCode,
					delivery_type: type,
					parcel_category: 'Normal',
					parcel_content: 'Matchbox',
					parcel_qty: 1,
					parcel_uom: 'Pcs',
					parcel_value: price,
					total_weight: packet.weight,
					shipper_name: merchant.receiver,
					shipper_address: merchant.address,
					shipper_phone: merchant.phone,
					shipper_district: merchant.district,
					shipper_city: merchant.city,
					shipper_province: merchant.province,
					shipper_zip: merchant.postal,
					recipient_title: 'Mrs',
					recipient_name: receiver.receiver,
					recipient_address: receiver.address,
					recipient_district: receiver.district,
					recipient_city: receiver.city,
					recipient_province: receiver.district,
					recipient_phone: receiver.phone,
					destination_code: receiver.code,
				}],
			},
		}).then(res => {
			return res
		})
	}

}

export default new SicepatService()
