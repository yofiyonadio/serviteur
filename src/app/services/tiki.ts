import {
	ErrorModel,
	ServiceModel,
} from 'app/models'

import InterfaceAddressModel from 'energie/app/models/interface.address'

import { CommonHelper } from 'utils/helpers'

import { Parameter } from 'types/common'

import { ERRORS, ERROR_CODES } from 'app/models/error'
import { CONNOTE_STATUS } from 'app/interfaces/tiki'


class TikiService extends ServiceModel {

	private token: string

	constructor() {
		super(process.env.TIKI_API, 0)

		this.token = undefined
		this.authentication = this.authentication.bind(this)
		// this.authentiucation()
	}

	async getServices(
		origin_tariff_code: string,
		destination_tariff_code: string,
		weight: number,
	): Promise<Array<{
		SERVICE: string,						// PRODUCT SERVICE
		DESCRIPTION: string,					// DESCRIPTION
		TARIFF: string,							// PRICE
		EST_DAY: string,						// ESTIMATED SHIPMENT DAY
	}>> {
		return this.post('tariff/product', {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': this.token,
			},
			data: {
				orig: origin_tariff_code,
				dest: destination_tariff_code,
				weight,
			},
		}).then(res => {
			if(res.status === 200) {
				return res.response
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, res)
			}
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.getServices(origin_tariff_code, destination_tariff_code, weight)
			})
		})
	}

	async getAreaInfo(): Promise<Array<{
		id: number,							// TIKI AREA ID
		city_id: number,					// CODE CITY
		province_id: number,				// CODE PROVINCE
		citycounty_id: number,				// CODE COUNTRY
		sub_dist: string,					// SUB DISTICT NAME
		dist: string,						// DISTRICT NAME
		city_county_type: null | string,	// TYPE OF COUNTRY
		city_county: string,				// COUNTRY NAME
		province: string,					// PROVINCE NAME
		zip_code: number,					// ZIP CODE
		tariff_code: string,				// TARIFF CODE
	}>> {
		return this.post('tariff/areainfo', {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': this.token,
			},
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.getAreaInfo()
			})
		})
	}

	// =================================================
	// Call this method to create tiki shipment
	// =================================================
	async createShipment(
		id: string, 									// custom Yuna shipment ID
		description: string,							// packet content description
		value: number,									// packet content value, 0 to remove insurance
		service: string,								// courier service
		consignor: Parameter<InterfaceAddressModel>,	// origin
		consignee: Parameter<InterfaceAddressModel>,	// destination
		is_cod: 0 | 1 = 0,								// 1 means COD
		awb?: string,									// custom awb number
	): Promise<{
		status: 200,
		msg: string,
		response: {
			paket_id: string,
			paket_awb: string,
		},
	}> {
		 return this.post('/mde/manifestorder', {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': this.token,
			},
			data: CommonHelper.stripUndefined({
				accnum: process.env.TIKI_ACC_NUM,
				paket_awb: awb,
				paket_id: id,
				paket_content: description,
				paket_value: value,
				paket_cod: is_cod,
				paket_service: service,
				consignor_name: consignor.receiver,
				consignor_company: consignor.title,
				consignor_address1: consignor.address,
				consignor_address2: undefined,
				consignor_zipcode: consignor.postal,
				consignor_phone: consignor.phone,
				consignee_name: consignee.receiver,
				consignee_company: consignee.title,
				consignee_address1: consignee.address,
				consignee_address2: undefined,
				consignee_zipcode: consignee.postal,
				consignee_phone: consignee.phone,
			}),
		 }).catch(err => {
			 return this.authentication(err).then(() => {
				 return this.createShipment(id, description, value, service, consignor, consignee, is_cod)
			 })
		 })
	}

	async getShipmentInfo(packet_id: string): Promise<{
		cnno: string,																	// CONNOTE TIKI
		seq_no: number,																	// SEQ NUMBER (for multiple part shipment)
		pieces_no: number,																// TOTAL SEQ NUMBER
		sender_reference: string,														// CUSTOMER PACKAGE-ID
		origin_tariff: string,															// ORIGIN TARIFF CODE
		destination_tariff_code: string,												// DESTINATION TARIFF CODE
		destination_city_name: string,                                            		// KOTA TUJUAN
		product: string,																// SERVICE
		sys_created_on: string,															// DATE OF MANIFEST
		weight: number,																	// CHARGEABLE WEIGHT
		consignor_name: string,															// CONSIGNOR NAME
		consignor_address: string,														// CONSIGNOR ADDRESS
		consignee_name: string,															// CONSIGNEE NAME
		consignee_address: string,														// CONSIGNEE ADDRESS
		est_day: number,																// ESTIMATED SHIPMENT DAYS
		est_date: string,																// ESTIMATED SHIPMENT DATES
	}> {
		return this.post('/connote/info', {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': this.token,
			},
			data: {
				cnno: packet_id,
			},
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.getShipmentInfo(packet_id)
			})
		})
	}

	async getShipmentHistory(awb: string): Promise<Array<{
		seq_no: number,			// SEQ NUMBER (for multiple part shipment)
		entry_date: string,		// TANGGAL STATUS
		status: CONNOTE_STATUS, // KODE STATUS
		entry_name: string,		// NAMA
		entry_place: string,	// LOKASI
		noted: string,			// KETERANGAN
		// description: CONNOTE_STATUS_DESCRIPTION,
	}>> {
		return this.post('/connote/mpds/history', {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': this.token,
			},
			data: {
				cnno: awb,
			},
		}).then(res => {
			if (res.status === 200) {
				return res.response[0]?.history?.map((h: any) => {
					return {
						...h,
						status: CONNOTE_STATUS[h.status],
					}
				}) ?? []
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, res)
			}
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.getShipmentHistory(awb)
			})
		})
	}

	private authentication(err?: any) {
		// SELIDIKI WHAT KIND OF RESPONSE DO I GET IF THE TOKEN EXPIRED
		if (!this.token || !err || (err && err.status === 403 && err.msg === 'Failed to authenticate token.')) {
			return this.post('user/auth', {
				headers: {
					'Content-Type': 'application/json',
				},
				data: {
					username: process.env.TIKI_MERCHANT_USERNAME,
					password: process.env.TIKI_MERCHANT_PASSWORD,
				},
			}).then(token => {
				return this.token = token.response.token
			})
		}

		throw err
	}


}

export default new TikiService()
