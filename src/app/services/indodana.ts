import {
	ErrorModel,
	ServiceModel,
} from 'app/models'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import crypto from 'crypto'

import {
	BrandAddressInterface,
	ExchangeDetailInterface,
	OrderAddressInterface,
	OrderDetailInterface,
	OrderInterface,
	OrderPaymentInterface,
	UserInterface,
	UserProfileInterface,
} from 'energie/app/interfaces'
import { TimeHelper } from 'utils/helpers'

class IndodanaService extends ServiceModel {
	constructor() {
		super('', 0)
	}

	async createPayment(
		order: OrderInterface & {
			details: OrderDetailInterface[],
			address: OrderAddressInterface,
			price: {
				subtotal: number,
				handling: number,
				shipping: number,
				discount: number,
				adjustments: number,
				wallet: number,
				roundup: number,
				total: number,
			},
		},
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		yunaAddress: BrandAddressInterface,
	): Promise<{
		redirectUrl: string,
		transactionId: string,
	}> {
		const items = [...order.details.map(detail => {
			return {
				id: detail.id.toString(),
				name: detail.title,
				price: detail.price,
				category: 'clothing',
				parentId: 'Yuna&Co.',
				quantity: 1,
			}
		}), ...Object.keys(order.price).reduce((init, key) => {
			if (key === 'handling' || key === 'shipping') {
				return init.concat({
					id: key === 'shipping' ? 'shippingfee' : key,
					name: key === 'shipping' ? 'Shipping' : 'Styling Fee',
					price: order.price[key],
					category: 'services',
					parentId: 'Yuna&Co.',
					quantity: 1,
				})
			} else if (key === 'discount' || key === 'wallet') {
				const index = init.findIndex(i => i.id === 'discount')

				if (index > -1) {
					init[index].price = init[index].price + order.price[key]

					return init
				} else {
					return init.concat({
						id: 'discount',
						name: 'discount',
						price: order.price[key],
						category: 'services',
						parentId: 'Yuna&Co.',
						quantity: 1,
					})
				}
			} else {
				return init
			}
		}, [])]

		return this.post(`${process.env.INDODANA_API}/v2/checkout_url`, {
			headers: {
				Authorization: this.auth(),
			},
			data: {
				transactionDetails: {
					merchantOrderId: order.number,
					amount: order.price.total,
					items,
				},
				customerDetails: {
					firstName: user.profile.first_name,
					lastName: user.profile.last_name,
					email: user.email,
					phone: !!user.profile.phone ? user.profile.phone : order.address.phone,
				},
				sellers: [{
					id: 'Yuna&Co.',
					name: 'Yuna & Co.',
					email: 'noreply@helloyuna.io',
					url: process.env.MAIN_WEB_URL,
					address: {
						firstName: yunaAddress.receiver,
						lastName: '',
						address: yunaAddress.address,
						city: yunaAddress.district,
						postalCode: yunaAddress.postal,
						phone: yunaAddress.phone,
						countryCode: 'ID',
					},
				}],
				billingAddress: {
					firstName: order.address.receiver,
					lastName: '',
					address: order.address.address,
					city: order.address.district,
					postalCode: order.address.postal,
					phone: order.address.phone,
					countryCode: 'ID',
				},
				shippingAddress: {
					firstName: order.address.receiver,
					lastName: '',
					address: order.address.address,
					city: order.address.district,
					postalCode: order.address.postal,
					phone: order.address.phone,
					countryCode: 'ID',
				},
				approvedNotificationUrl: `${process.env.CONNECTER_URL}third-party/indodana`,
				backToStoreUrl: `${process.env.MAIN_WEB_URL}checkout/${order.number}/SUCCESS`,
				expirationAt: TimeHelper.moment(order.created_at).add(7, 'days').format('YYYY-MM-DDThh:mm:ssZ'),
			},
		}).catch(err => {
			this.warn(err.error.message)

			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, err.error.message)
		})
	}

	async createRefund(
		order: OrderInterface & {
			payment?: OrderPaymentInterface,
			exchangeDetail?: ExchangeDetailInterface,
		},
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		amount: number,
		reason: 'Expired' | 'Out of Stock' | 'Items Returned' | 'Others',
	): Promise<{
		status: string,
		fraudStatus: string,
		merchantOrderId: string,
		originalTransactionAmount: number,
		cancellationAmount: number,
		afterCancellationTransactionAmount: number,
		transactionStatus: string,
		transactionTime: string,
		paymentType: string,
	}> {
		return this.post(`${process.env.INDODANA_API}/v2/order_cancellation`, {
			headers: {
				Authorization: this.auth(),
			},
			data: {
				refundId: !!order.exchangeDetail?.id ? `${order.exchangeDetail.id}` : `${order.payment.id}`,
				merchantOrderId: order.number,
				cancellationAmount: amount,
				cancellationReason: reason,
				cancelledBy: `${user.profile.first_name} ${user.profile.last_name}`,
				cancellationDate: TimeHelper.moment().format('YYYY-MM-DDThh:mm:ssZ'),
			},
		}).catch(err => {
			this.warn(err.error.message)

			throw err
		})
	}

	auth(): string {
		const apiKey: string = process.env.INDODANA_API_KEY
		const apiSecret: string = process.env.INDODANA_API_SECRET
		const nonce: number = Math.floor(Date.now() / 1000)
		const signature = crypto.createHmac('sha256', apiSecret).update(`${apiKey}:${nonce}`).digest('hex')

		return `Bearer ${apiKey}:${nonce}:${signature}`
	}
}

export default new IndodanaService()
