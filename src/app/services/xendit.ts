import {
	ServiceModel,
} from 'app/models'

class XenditService extends ServiceModel {
	constructor() {
		super('', 0)
	}

	async getPaymentMethod(): Promise<Array<{
		business_id: string,
		is_livemode: boolean,
		channel_code: string,
		name: string,
		currency: string,
		channel_category: string,
		is_enabled: boolean,
	}>> {
		return this.get(`${ process.env.XENDIT_API }/payment_channels`, {
			headers: {
				authorization: `Basic ${Buffer.from(process.env.XENDIT_KEY + ':').toString('base64')}`,
			},
		}).then(res => {
			return res
		})
	}
}

export default new XenditService()
