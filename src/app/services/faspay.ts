import ServiceModel from 'app/models/service'
import { InquiryParameterInterface, InquiryResponseInterface } from 'app/interfaces/faspay'

import FormData from 'form-data'
import xml2js from 'xml2js'
import js2xml from 'jsontoxml'
import { CommonHelper } from 'utils/helpers'


class FaspayService extends ServiceModel {
	constructor() {
		super('', 0)
	}

	async checkStatus(trx_id: number, data: InquiryParameterInterface): Promise<InquiryResponseInterface> {
		return this.post(process.env.FASPAY_INQUIRY_API, {
			headers: { 'Content-Type': 'text/xml' },
			data: js2xml({
				faspay: data,
			}, {
				xmlHeader: true,
			}),
		}).then(_data => {
			return new Promise((res, rej) => {
				xml2js.parseString(_data, (err: any, result: any) => {
					if(err) {
						rej(err)
					} else {
						res({
							response: result.faspay.response?.shift(),
							trx_id: parseInt(result.faspay.trx_id?.shift(), 10),
							merchant_id: parseInt(result.faspay.merchant_id?.shift(), 10),
							merchant: result.faspay.merchant?.shift(),
							bill_no: result.faspay.bill_no?.shift(),
							payment_reff: result.faspay.payment_reff?.shift(),
							payment_date: result.faspay.payment_date?.shift(),
							payment_status_desc: result.faspay.payment_status_desc?.shift(),
							payment_status_code: result.faspay.payment_status_code?.shift(),
							payment_total: parseFloat(result.faspay.payment_total?.shift() ?? 0),
							response_code: result.faspay.response_code?.shift(),
							response_desc: result.faspay.response_desc?.shift(),
						})
					}
				})
			})
		})
	}

	async checkCreditStatus<P, R>(parameter: P): Promise<R> {
		const data = new FormData()

		Object.keys(CommonHelper.stripUndefined(parameter)).forEach(key => {
			data.append(key, parameter[key])
		})

		return this.service.post(process.env.FASPAY_CREDIT_INQUIRY_API, data, {
			headers: data.getHeaders(),
		}).then(res => {
			return res.data.split(';').map(keyAndValue => {
				const keyValues = keyAndValue.split('=')

				return {
					[keyValues.shift()]: keyValues.join('='),
				}
			}).reduce(CommonHelper.sumObject, {})
		})
	}
}

export default new FaspayService()
