import {
	ServiceModel,
} from 'app/models'
import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'

import crypto from 'crypto'
import { TimeHelper } from 'utils/helpers'

class BCAService extends ServiceModel {
	constructor() {
		super('', 0)
	}

	async getAccessToken(): Promise<string> {
		return this.post(`${ process.env.BCA_API }/api/oauth/token`, {
			headers: {
				'authorization': `Basic ${Buffer.from(process.env._BCA_CLIENT_ID + ':' + process.env._BCA_CLIENT_SECRET).toString('base64')}`,
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			data: 'grant_type=client_credentials',
		}).then(res => {
			return res.access_token
		})
	}

	async inquryVA(
		request_id?: string,
		customer_number?: string,
	): Promise<{
		TransactionData: Array<{
			DetailBills: Array<{
				BillReference: string,
				BillNumber: string,
			}>,
			PaymentFlagStatus: string,
			RequestID: string,
			Reference: string,
			TotalAmount: string,
			TransactionDate: string,
			PaidAmount: string,
		}>,
	}> {
		const timestamp = TimeHelper.moment().format('YYYY-MM-DDTHH:mm:ss.000Z').toString()
		const token = await this.getAccessToken()
		const query = {
			CompanyCode: process.env.COMPANY_CODE,
			...(request_id ? {RequestID: request_id} : customer_number ? {CustomerNumber: customer_number} : {}),
		}
		const headers = {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json',
			'X-BCA-KEY': process.env._BCA_API_KEY,
			'X-BCA-Timestamp': timestamp,
			'X-BCA-Signature': this.signatureHandler(
				'GET',
				`/va/payments?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`,
				token,
				process.env._BCA_API_SECRET,
				timestamp,
				'',
			),
		}

		return this.get(`${ process.env.BCA_API }/va/payments`, {
			headers,
			query,
		}).then(res => {
			return res
		})
		.catch(err => {
			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, err)
		})
	}

	signatureHandler(
		method: string,
		url: string,
		token: string,
		secret: string,
		timestamp: string,
		body: object | string,
	) {
		const stringifyBody = typeof body === 'string' ? body :  JSON.stringify(body).replace(/ /g, '')
		const hashBody = crypto.createHash('sha256').update(`${stringifyBody}`).digest('hex')
		const stringToSign = method + ':' + url + ':' + token + ':' + hashBody + ':' + timestamp
		const signature = crypto.createHmac('sha256', secret).update(stringToSign).digest('hex')

		return signature
	}
}

export default new BCAService()
