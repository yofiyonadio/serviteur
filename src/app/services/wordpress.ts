import {
	// ErrorModel,
	ServiceModel,
} from 'app/models'

import { PostsInterface, MediaInterface, CategoryInterface } from '../interfaces/wordpress'

class WordpressService extends ServiceModel {

	// private token: string

	constructor() {
		super(process.env.JOURNAL_URL, 0)

		// this.token = undefined
		// this.authentication = this.authentication.bind(this)
	}

	async getPosts(
		options: {
			page?: number,
			per_page?: number,
			search?: string,
			order?: string,
		},
	): Promise<PostsInterface[]> {
		return this.get(`/wp-json/wp/v2/posts`, {
			query: options,
		}).then(res => {
			return res
		})
	}

	async getMedia(featured_media: number): Promise<MediaInterface> {
		return this.get(`/wp-json/wp/v2/media/${featured_media}`)
			.then(res => {
				return res
			})
	}

	async getCategory(id: number): Promise<CategoryInterface> {
		return this.get(`/wp-json/wp/v2/categories/${id}`)
			.then(res => {
				return res
			})
	}
}

export default new WordpressService()
