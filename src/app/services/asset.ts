import ServiceModel from 'app/models/service'
import {
	AssetResponse,
	AssetUploadConfig,
	ResourceType,
} from 'app/interfaces/file'

import InterfaceAssetModel from 'energie/app/models/interface.asset'

import {
	ASSETS,
} from 'energie/utils/constants/enum'


class AssetService extends ServiceModel {

	constructor() {
		super(process.env.PEINTURE_URL, 0)
	}

	async upload({
		image,
		options,
		// nonce,
		metadata,
		nonce,
	}: {
		image: string,
		options?: AssetUploadConfig,
		metadata?: object,
		nonce?: string,
	}): Promise<InterfaceAssetModel> {

		return this.post('upload', {
			data: {
				image,
				options,
			},
		}).then((data: AssetResponse) => {
			return this.getConfigFromFile(data, nonce, metadata)
		})
	}

	getConfigFromFile({
		public_id,
		resource_type,
		format,
		width,
		height,
		version,
		tags,
		url,
		secure_url,
		...file
	}: AssetResponse, nonce?: string, metadata?: object): InterfaceAssetModel {
		let type: ASSETS

		switch (resource_type) {
		case ResourceType.image:
			type = ASSETS.IMAGE
			break
		case ResourceType.video:
			type = ASSETS.VIDEO
			break
		case ResourceType.raw:
			type = ASSETS.RAW
			break
		case ResourceType.auto:
			type = ASSETS.FILE
			break
		}

		return {
			url: `${public_id}.${format}`,
			type,
			width,
			height,
			nonce,
			metadata: {
				publicId: public_id,
				version,
				url,
				secureUrl: secure_url,
				tags,
				...file,
				...metadata,
			},
		} as InterfaceAssetModel
	}

}

export default new AssetService()
