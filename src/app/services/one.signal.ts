import ServiceModel from 'app/models/service'

type Languages = 'en' | 'id'
type Config = {
	[key in Languages]: string
}
type MultipleConfig = {
	[key in Languages]: {
		title: string,
		subtitle: string,
		message: string,
	}
} & {
	en: {
		title: string,
		subtitle: string,
		message: string,
	},
}


class OneSignalService extends ServiceModel {
	constructor() {
		super(process.env.ONESIGNAL_API, 0)
	}

	notify(type: 'user' | 'segment', key: number | string[], url: string, config: MultipleConfig['en'] | MultipleConfig, data: {}, wake: boolean = false): Promise<{
		id: string,
		recipients: number,
		errors?: string[] | {
			[key: string]: string[],
		},
	}> {

		const headings: Config = {} as Config
		const subtitle: Config = {} as Config
		const contents: Config = {} as Config

		if ((config as MultipleConfig['en']).title) {
			headings.en = (config as MultipleConfig['en']).title
			subtitle.en = (config as MultipleConfig['en']).subtitle
			contents.en = (config as MultipleConfig['en']).message
		} else {
			const c = config as MultipleConfig
			Object.keys(c).forEach(lang => {
				headings[lang] = c[lang].title
				subtitle[lang] = c[lang].subtitle
				contents[lang] = c[lang].message
			})
		}

		return this.post('notifications', {
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Authorization': `Basic ${ process.env.ONESIGNAL_KEY }`,
			},
			data: {
				app_id: process.env.ONESIGNAL_APP_ID,
				headings,
				subtitle,
				contents,
				url,
				data,
				content_available: wake,
				...( type === 'user' ? {
					include_external_user_ids: [key + ''],
				} : {
					included_segments: key,
				}),
			},
		}) as any
	}
}

export default new OneSignalService()
