import ServiceModel from 'app/models/service'

import {
	SERVICE_LEVEL,
	SERVICE_TYPE,
	NinjaShipmentAddressInterface,
	MarketplaceInterface,
	ParcelJobInterface,
	PORTATION,
} from 'app/interfaces/ninja'

// import { CommonHelper } from 'utils/helpers'


class NinjaService extends ServiceModel {

	private token: string

	constructor() {
		super(process.env.NINJA_API, 0)

		this.token = undefined
		this.authentication = this.authentication.bind(this)
		// this.authentication()
	}


	// =================================================
	// Call this method to create tiki shipment
	// =================================================
	async createShipment(
		id: string, 									// custom Yuna shipment ID
		service_type: SERVICE_TYPE,							// tariff_code from database
		service_level: SERVICE_LEVEL,							// packet content description
		from: NinjaShipmentAddressInterface,
		to: NinjaShipmentAddressInterface,
		parcel_job: Partial<ParcelJobInterface>,
		marketplace?: MarketplaceInterface,
		international?: {
			portation: PORTATION,
		},
	): Promise<{
		tracking_number: string,
		service_type: SERVICE_TYPE,
		service_level: SERVICE_LEVEL,
	}> {
		 return this.post(`${process.env.NINJA_SERVER_ID}/${process.env.NINJA_API_VERSION}/orders`, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer y1XH1bkDt4LzL5fKyXIgnTSEiHofNr38oLXVT2TO`,
			},
			data: {
				service_type,
				service_level,
				// requested_tracking_number: 'NV-123456',
				from,
				to,
				parcel_job,
				marketplace,
				international,
			},
		 }).catch(err => {
			 return this.authentication(err).then(() => {
				 return this.createShipment(id, service_type, service_level, from, to, parcel_job, marketplace, international)
			 })
		 })
	}
	// Waybill successfully generated. A PDF file will be returned in the response.
	// NOTE: Waybill can only be generated for orders that have been successfully processed.
	async generateWaybill(tracking_number: string): Promise<number> {
		return this.get(`${process.env.NINJA_SERVER_ID}/${process.env.NINJA_API_VERSION}/reports/waybill?tids=${tracking_number}`, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${this.token}`,
			},
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.generateWaybill(tracking_number)
			})
		})
	}
	// NOTE: For cancellation of orders, please note that only orders that are Pending Pickup can be cancelled.
	async cancelOrder(tracking_number: string): Promise<number> {
		return this.delete(`${process.env.NINJA_SERVER_ID}/${process.env.NINJA_API_VERSION}/orders/${tracking_number}`, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer y1XH1bkDt4LzL5fKyXIgnTSEiHofNr38oLXVT2TO`,
			},
		}).catch(err => {
			return this.authentication(err).then(() => {
				return this.generateWaybill(tracking_number)
			})
		})
	}
	private authentication(err?: any) {
	// 	Please take note of the following points:

	// We require shippers to cache the authorization access tokens for their API integration.
	// Shippers are encouraged to retry order creation requests IF a HTTP 5xx error is returned from our API.
	// If a HTTP 4xx error is returned, please log the response for your debugging purposes.
	// DO NOT retry the same order creation request without fixing the error specified in the response.

		if (!err || (err && err.status === 403 && err.msg === 'Failed to authenticate token.')) {
			return this.post(`${process.env.NINJA_SERVER_ID}/2.0/oauth/access_token`, {
				headers: {
					'Content-Type': 'application/json',
				},
				data: {
					client_id: process.env.NINJA_CLIENT_ID,
					client_secret: process.env.NINJA_CLIENT_KEY,
					grant_type: 'client_credentials',
				},
			}).then(token => {
				return  this.token =  token.access_token
			})
		}

		throw err
	}


}

export default new NinjaService()
