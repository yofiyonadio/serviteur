import AdminMiddleware from 'middlewares/admin'
import AllowOriginMiddleware from 'middlewares/allow.origin'
import AuthenticationMiddleware from 'middlewares/authentication'
import ModifierMiddleware from 'middlewares/modifier'


import { Application } from 'express'
import { Connection } from 'typeorm'

export default {
	init(app: Application, connection: Connection) {
		app.use('/', ModifierMiddleware(connection))
		app.use('/', AllowOriginMiddleware(connection))

		app.use('/admin', AuthenticationMiddleware(connection))
		app.use('/admin', AdminMiddleware('admin', connection))
		app.use('/admin/wallet', AdminMiddleware(['superadmin', 'finance'], connection))
		app.use('/admin/migration', AdminMiddleware(['superadmin'], connection))
		app.use('/admin/functions', AdminMiddleware(['superadmin'], connection))

	},
}
