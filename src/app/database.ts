import {
	createConnection,
} from 'typeorm'

import { Pool } from 'pg'

import * as Managers from 'app/managers'
import * as Records from 'energie/app/records'

import Defaults from 'utils/constants/default'
import EntityHelper from 'energie/utils/helpers/entity'
import LoggerHelper from 'energie/utils/helpers/logger'

import Listener from './listener'

export default {
	async init() {
		return await createConnection({
			name: 'defaultConnection',
			type: 'postgres',
			ssl: process.env.DB_HOST !== 'localhost' ? { rejectUnauthorized: false } : false,
			host: process.env.DB_HOST,
			port: parseInt(process.env.DB_PORT, 10),
			database: process.env.DB_NAME,
			username: process.env.DB_USER,
			password: process.env.DB_PASS,
			logging: Defaults.DEBUG ? ['error', 'warn', 'info', 'log'] : false,
			synchronize: false,
			entities: [...Object.values(Records).map(record => EntityHelper.create(record))],
		})
			.then(async connection => {
				const qR = connection.createQueryRunner()
				await qR.connect()

				const conn = new Pool({
					user: process.env.DB_USER,
					host: process.env.DB_HOST,
					database: process.env.DB_NAME,
					password: process.env.DB_PASS,
					port: parseInt(process.env.DB_PORT, 10),
				})

				conn.connect(async (err, client) => {
					LoggerHelper.log('Connection Pool success!', 'server')
					Listener.notify(client)
				})

				// ==================================================================
				// Refresh materialized view
				// ==================================================================
				/*
				await Promise.all(EntityHelper.refreshMaterializedView().map(async query => {
					return await qR.query(query)
				}))
				*/

				// ==================================================================
				// Refresh materialized view
				// ==================================================================

				if (Defaults.DEBUG) {
					LoggerHelper.log('Connection has been established successfully!', 'server')
				}

				await Promise.all(Object.values(Managers).map(manager => {
					return manager.initialize(connection)
				}))

				await qR.release()

				return connection
			})
	},
}
