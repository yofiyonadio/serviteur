import {
	config,
} from 'dotenv'
import 'reflect-metadata'

config()

import Defaults from 'utils/constants/default'
import LoggerHelper from 'energie/utils/helpers/logger'

Defaults.setDebugLevel(process.env.DEBUG === 'true')
Defaults.setSync(process.env.SYNC === 'true')
LoggerHelper.setType('service', '\x1b[1m\x1b[38;5;155m')
LoggerHelper.setType('controller', '\x1b[1m\x1b[38;5;51m')
LoggerHelper.setMaxLength(21)


import Database from './database'
import Server from './server'

Database
	.init()
	.then(Server.init)
	.catch(console.warn)
