import HandlerModel from 'app/models/handler'
import { OAuth2Client } from 'google-auth-library'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'


class GoogleHandler extends HandlerModel<{
	clientId: string,
	clientSecret: string,
}> {
	protected static __displayName = 'Google Handler'

	private web: OAuth2Client
	// private ios: OAuth2Client
	// private android: OAuth2Client

	constructor() {
		super({
			clientId: process.env.GOOGLE_CLIENT_ID,
			clientSecret: process.env.GOOGLE_CLIENT_SECRET,
		})

		this.web = new OAuth2Client({
			clientId: this.get('clientId'),
			clientSecret: this.get('clientSecret'),
		})
	}

	async authenticate(accessToken: string, platform?: 'ios' | 'android' | 'web') {

		let client: OAuth2Client

		client = this.web

		return await client.verifyIdToken({
			idToken: accessToken,
			audience: this.get('clientId'),
		})
		.then(ticket => {
			return ticket.getPayload()
		})
		.catch(err => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `${err}`)
		})
	}
}

export default new GoogleHandler()
