import FacebookHandler from './facebook'
import FaspayHandler from './faspay'
import GoogleHandler from './google'
import MailchimpHandler from './mailchimp'
import MandrillHandler from './mandrill'
import MidtransHandler from './midtrans'
import NinjaHandler from './ninja'
import OneSignalHandler from './one.signal'
import SegmentHandler from './segment'
import SendGridHandler from './send.grid'
import TwilioHandler from './twilio'
import XenditHandler from './xendit'
import JenkinsHandler from './jenkins'

export {
	FacebookHandler,
	FaspayHandler,
	GoogleHandler,
	MailchimpHandler,
	MandrillHandler,
	MidtransHandler,
	NinjaHandler,
	OneSignalHandler,
	SegmentHandler,
	SendGridHandler,
	TwilioHandler,
	XenditHandler,
	JenkinsHandler,
}
