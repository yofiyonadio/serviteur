import HandlerModel from 'app/models/handler'
import Xendit from 'xendit-node'
import XenditService from '../services/xendit'
import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import {
	CreditCardResponseInterface,
	VirtualAccountResponseInterface,
	RetailOutletResponseInterface,
	// OVOResponseInterface,
	// DANAResponseInterface,
	// LINKAJAResponseInterface,
	VirtualAccountNotification,
	RetailOutletNotification,
	OVONotification,
	DANANotification,
	LINKAJANotification,
	CardInterface,
	VirtualAccountInterface,
	RetailOutletInterface,
	EwalletInterface,
} from 'app/interfaces/xendit'

import { PAYMENTS, PAYMENT_METHOD } from 'energie/utils/constants/enum'
import { PaymentMetadataInterface } from 'energie/app/interfaces/order.payment'

export enum VA_BANK {
	MANDIRI = 'MANDIRI',
	BNI = 'BNI',
	BRI = 'BRI',
	BCA = 'BCA',
	PERMATA = 'PERMATA',
}

export enum EWALLET {
	OVO = 'ID_OVO',
	DANA = 'ID_DANA',
	LINKAJA = 'ID_LINKAJA',
}

export enum RETAIL_OUTLET {
	ALFAMART = 'ALFAMART',
	INDOMARET = 'INDOMARET',
}

// type ewallet_response = OVOResponseInterface | DANAResponseInterface | LINKAJAResponseInterface
type ewallet_notification = OVONotification | DANANotification | LINKAJANotification
type notification = VirtualAccountNotification | RetailOutletNotification | ewallet_notification

class XenditHandler extends HandlerModel<{
	key: string,
	token: string,
}> {
	protected static __displayName = 'Xendit Handler'
	private xendit: Xendit = null

	constructor() {
		super({
			key: process.env.XENDIT_KEY,
			token: process.env.XENDIT_TOKEN,
		})

		this.xendit = new Xendit({secretKey: this.get('key')})
	}

	async getPaymentMethod(): Promise<Array<{
		method: PAYMENT_METHOD,
		code: string,
		name: string,
		is_active: boolean,
	}>> {
		return await XenditService.getPaymentMethod()
		.then(channels => {
			return channels.reduce((init, channel) => {
				let method: PAYMENT_METHOD

				switch(channel.channel_category) {
				case 'CREDIT_CARD':
					method = PAYMENT_METHOD.CARD
					break
				case 'VIRTUAL_ACCOUNT':
					method = PAYMENT_METHOD.VIRTUAL_ACCOUNT
					break
				case 'RETAIL_OUTLET':
					method = PAYMENT_METHOD.RETAIL_OUTLET
					break
				case 'EWALLET':
					method = PAYMENT_METHOD.EWALLET
					break
				}

				return init.concat({
					method,
					code: channel.channel_code,
					name: channel.name,
					is_active: channel.is_enabled,
				})

			}, [] as Array<{
				method: PAYMENT_METHOD,
				code: string,
				name: string,
				is_active: boolean,
			}>)
		})
	}

	async transaction(
		method: PAYMENT_METHOD,
		token?: string,
		auth_id?: string,
		va_bank?: VA_BANK,
		ewallet?: EWALLET,
		retail_outlet?: RETAIL_OUTLET,
		name?: string,
		phone?: string,
		order_number?: string,
		amount?: number,
		expired_at?: Date,
	) {
		switch(method) {
		case PAYMENT_METHOD.CARD:
			return await this.withCard(token, auth_id, order_number, amount)
			.then(card => {
				return {
					method,
					...card,
				}
			})
		case PAYMENT_METHOD.VIRTUAL_ACCOUNT:
			return await this.withVirtualAccount(va_bank, name, order_number, amount, expired_at)
			.then(va => {
				return {
					method,
					...va,
				}
			})
		case PAYMENT_METHOD.EWALLET:
			return await this.withEWallet(ewallet, phone, order_number, amount, expired_at)
			.then(ew => {
				return {
					method,
					...ew,
				}
			})
		case PAYMENT_METHOD.RETAIL_OUTLET:
			return await this.withRetailOutlet(retail_outlet, name, order_number, amount, expired_at)
			.then(ro => {
				return {
					method,
					...ro,
				}
			})
		}
	}

	async setExpired(
		method: PAYMENT_METHOD,
		method_id: string,
		amount: number,
		expired_at: Date,
	) {
		switch(method) {
		case PAYMENT_METHOD.VIRTUAL_ACCOUNT:
			return await this.setExpiredVirtualAccount(method_id, amount, expired_at)
			.then(va => {
				return {
					method,
					...va,
				}
			})
		case PAYMENT_METHOD.RETAIL_OUTLET:
			return await this.setExpiredRetailOutlet(method_id, amount, expired_at)
			.then(ro => {
				return {
					method,
					...ro,
				}
			})
		case PAYMENT_METHOD.EWALLET:
			throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, '')
		}
	}

	async notification(
		token: string,
		data: notification,
	): Promise<{
		order_number: string,
		status: PAYMENTS,
		metadata: PaymentMetadataInterface,
	}> {
		if(token !== this.get('token')) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Validation error!')
		}

		switch(data.method) {
		case 'va':
			return {
				order_number: data.external_id,
				status: PAYMENTS.SUCCESS,
				metadata: {
					transaction_id: data.id,
					transaction_date: data.transaction_timestamp,
					amount: data.amount,
					order_number: data.external_id,
					...(data as Omit<VirtualAccountNotification, 'id' | 'transaction_timestamp' | 'amount' | 'external_id'>),
				},
			}
		case 'retail-outlet':
			return {
				order_number: data.external_id,
				status: PAYMENTS.SUCCESS,
				metadata: {
					transaction_id: data.payment_id,
					transaction_date: data.transaction_timestamp,
					amount: data.amount,
					order_number: data.external_id,
					...(data as Omit<RetailOutletNotification, 'payment_id' | 'transaction_timestamp' | 'amount' | 'external_id'>),
				},
			}
		case 'ewallet':
			switch(data.ewallet_type) {
			case 'OVO':
				return {
					order_number: data.external_id,
					status: data.status === 'COMPLETED' ? PAYMENTS.SUCCESS : PAYMENTS.FAILED,
					metadata: {
						...data,
					} as any,
				}
			case 'DANA':
				return {
					order_number: data.external_id,
					status: data.payment_status === 'PAID' ? PAYMENTS.SUCCESS : PAYMENTS.FAILED,
					metadata: {
						...data,
					} as any,
				}
			case 'LINKAJA':
				return {
					order_number: data.external_id,
					status: data.status === 'SUCCESS_COMPLETED' ? PAYMENTS.SUCCESS : PAYMENTS.FAILED,
					metadata: {
						...data,
					} as any,
				}
			}
		}
	}

	// credit card
	private async withCard(
		token: string,
		auth_id: string | undefined,
		order_number: string,
		amount: number,
	): Promise<CardInterface> {
		const Card = this.xendit.Card
		const card = new Card({})

		return await card.createCharge({
			tokenID: token,
			authID: auth_id,
			externalID: order_number,
			amount,
		})
		.then((result: CreditCardResponseInterface) => {
			if(result.status === 'FAILED') {
				let reason: string

				switch(result.failure_reason) {
				case 'CARD_DECLINED':
					reason = 'Kartu anda ditolak oleh pihak bank'
					break
				case 'INACTIVE_CARD':
					reason = 'Kartu anda tidak aktif'
					break
				case 'INSUFFICIENT_BALANCE':
					reason = 'Kartu anda tidak mempunyai dana yang cukup'
					break
				case 'EXPIRED_CARD':
					reason = 'Kartu anda sudah kadaluarsa'
					break
				case 'INVALID_CVN':
					reason = 'CVN/CVV yang anda masukan salah'
					break
				case 'STOLEN_CARD':
					reason = 'Kartu yang anda gunakan ditandai telah dicuri'
					break
				case 'PROCESSOR_ERROR':
					reason = 'Proses integrasi kartu bermasalah, Harap kontak kami'
					break
				case 'BIN_BLOCK':
					reason = 'Kartu anda telah diblokir pihak bank'
					break
				}

				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, reason)
			}

			return {
				id: result.id,
				created_at: result.created,
				order_number: result.external_id,
				card_type: result.card_type,
				card_brand: result.card_brand,
				status: result.status,
			}
		})
		.catch(error => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		})
	}

	// virtual account
	private async withVirtualAccount(
		va_bank: VA_BANK,
		name: string,
		order_number: string,
		amount: number,
		expired_at: Date,
	): Promise<VirtualAccountInterface> {
		const VA = this.xendit.VirtualAcc
		const va = new VA({})

		return await va.createFixedVA({
			externalID: order_number,
  			bankCode: va_bank,
			name,
			expirationDate: expired_at,
			expectedAmt: amount,
			isClosed: true, // diset true biar user gabisa masukin sembarang amount
			isSingleUse: true, // diset true biar kalo udah bayar payment codenya gabisa dipake lagi
		})
		.then((result: VirtualAccountResponseInterface) => {
			return {
				id: result.id,
				bank: result.bank_code,
				order_number: result.external_id,
				prefix: result.merchant_code,
				payment_code: result.account_number,
				name: result.name,
				amount: result.expected_amount,
				expired_at: result.expiration_date,
			}
		})
		.catch(error => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		})
	}

	private async setExpiredVirtualAccount(
		id: string,
		amount: number,
		expired_at: Date,
	): Promise<VirtualAccountInterface> {
		const VA = this.xendit.VirtualAcc
		const va = new VA({})

		return await va.updateFixedVA({
			id,
			expectedAmt: amount,
			expirationDate: expired_at,
		})
		.then((result: VirtualAccountResponseInterface) => {
			return {
				id: result.id,
				bank: result.bank_code,
				order_number: result.external_id,
				prefix: result.merchant_code,
				payment_code: result.account_number,
				name: result.name,
				amount: result.expected_amount,
				expired_at: result.expiration_date,
			}
		})
		.catch(error => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		})
	}

	// retail outlet
	private async withRetailOutlet(
		outlet_name: RETAIL_OUTLET,
		name: string,
		order_number: string,
		amount: number,
		expired_at: Date,
	): Promise<RetailOutletInterface> {
		const RO = this.xendit.RetailOutlet
		const ro = new RO({})

		if(amount < 11000 || amount > 5000000) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Minimum amount is Rp.11000 and Maximum amount is Rp.5000000')
		}

		return await ro.createFixedPaymentCode({
			retailOutletName: outlet_name,
			externalID: order_number,
			name,
			expirationDate: expired_at,
			expectedAmt: amount,
			isSingleUse: true,
		})
		.then((result: RetailOutletResponseInterface) => {
			return {
				id: result.id,
				outlet_name: result.retail_outlet_name,
				order_number: result.external_id,
				payment_code: result.payment_code,
				name: result.name,
				prefix: result.prefix,
				amount: result.expected_amount,
				expired_at: result.expiration_date,
			}
		})
		.catch(error => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		})
	}

	private async setExpiredRetailOutlet(
		id: string,
		amount: number,
		expired_at: Date,
	): Promise<RetailOutletInterface> {
		const RO = this.xendit.RetailOutlet
		const ro = new RO({})

		return await ro.updateFixedPaymentCode({
			id,
			expectedAmt: amount,
			expirationDate: expired_at,
		}).then((result: RetailOutletResponseInterface) => {
			return {
				id: result.id,
				outlet_name: result.retail_outlet_name,
				order_number: result.external_id,
				payment_code: result.payment_code,
				name: result.name,
				prefix: result.prefix,
				amount: result.expected_amount,
				expired_at: result.expiration_date,
			}
		})
		.catch(error => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		})
	}

	// ewallet
	private async withEWallet(
		type: EWALLET,
		phone: string,
		order_number: string,
		amount: number,
		expired_at: Date,
	): Promise<EwalletInterface> {
		// const EWallet = this.xendit.EWallet
		// const ewallet = new EWallet({})

		// if(amount < 100 || amount > 10000000) {
		// 	throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Minimum amount is Rp.100 and Maximum amount is Rp.10000000')
		// }

		// return Promise.resolve()
		// .then(async () => {
		// 	switch(type) {
		// 	case EWALLET.OVO:
		// 		return await ewallet.createPayment({
		// 			ewalletType: type,
		// 			phone,
		// 			externalID: order_number,
		// 			expirationDate: expired_at,
		// 			amount,
		// 		})
		// 	case EWALLET.DANA:
		// 		return await ewallet.createPayment({
		// 			ewalletType: type,
		// 			externalID: order_number,
		// 			expirationDate: expired_at,
		// 			amount,
		// 			callbackURL: `${process.env.CONNECTER_URL}third-party/xendit/ewallet`,
		// 			redirectURL: `${process.env.MAIN_WEB_URL}`,
		// 		})
		// 	case EWALLET.LINKAJA:
		// 		return await ewallet.createPayment({
		// 			ewalletType: type,
		// 			phone,
		// 			externalID: order_number,
		// 			expirationDate: expired_at,
		// 			amount,
		// 			item: {},
		// 			callbackURL: `${process.env.CONNECTER_URL}third-party/xendit/ewallet`,
		// 			redirectURL: `${process.env.MAIN_WEB_URL}`,
		// 		})
		// 	}
		// })
		// .then((result: ewallet_response) => {
		// 	return {
		// 		ewallet_type: result.ewallet_type,
		// 		order_number: result.external_id,
		// 		amount: result.amount,
		// 		checkout_url: (result as DANAResponseInterface | LINKAJAResponseInterface).checkout_url,
		// 		metadata: {
		// 			...result,
		// 		},
		// 	}
		// })
		// .catch(error => {
		// 	throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, error)
		// })
		throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_106, 'Please Use Another Payment Method')
	}
}

export default new XenditHandler()
