import HandlerModel from 'app/models/handler'
import FaspayService from 'app/services/faspay'

// import * as FormData from 'form-data'
import sha1 = require('sha1')
import md5 = require('md5')


import {
	TransactionParameterInterface,
	InquiryParameterInterface,
	InquiryResponseInterface,
	PaymentNotificationInterface,
	CreditReturnResponseInterface,
	CreditInquiryParameterInterface,
	CreditInquiryResponseInterface,
	CreditCaptureResponseInterface,
	CreditVoidResponseInterface,
	CreditRefundResponseInterface,
	CreditCaptureParameterInterface,
} from 'app/interfaces/faspay'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import { PAYMENTS } from 'energie/utils/constants/enum'
import { Without } from 'types/common'


class FaspayHandler extends HandlerModel<{
	merchantId: number,
	merchantName: string,
	userId: string,
	password: string,
	creditMerchantId: string,
	creditMerchantPassword: string,
}> {
	protected static __displayName = 'Faspay Handler'

	constructor() {
		super({
			merchantId: parseInt(process.env.FASPAY_MERCHANT_ID, 10),
			merchantName: process.env.FASPAY_MERCHANT_NAME,
			userId: process.env.FASPAY_USER_ID,
			password: process.env.FASPAY_PASSWORD,
			creditMerchantId: process.env.FASPAY_CREDIT_MERCHANT_ID,
			creditMerchantPassword: process.env.FASPAY_CREDIT_MERCHANT_PASSWORD,
		})
	}

	async transaction(parameter: Without<TransactionParameterInterface, 'signature' | 'merchant_id' | 'merchant_name'>) {
		const data = {} as any

		Object.keys(parameter).forEach(key => {
			if(Array.isArray(parameter[key])) {
				data[key] = this.serialize(parameter[key])
			} else {
				data[key] = parameter[key]
			}
		})

		data.merchant_id = this.get('merchantId')
		data.merchant_name = this.get('merchantName')
		data.signature = sha1(md5(`${ this.get('userId') }${ this.get('password') }${ parameter.order_id }${ parameter.bill_total }`))

		return this.generateHtml(process.env.FASPAY_API, parameter.order_id, JSON.stringify(data))
	}

	async handleNotification(notification: PaymentNotificationInterface) {
		this.log(`Transaction notification received. Order ID: ${notification.bill_no}. Transaction status: ${notification.payment_status_code}.`)

		// =================================
		// CHECK SIGNATURE HASH
		// =================================

		if (sha1(md5(`${ this.get('userId') }${ this.get('password') }${ notification.bill_no }${ notification.payment_status_code }`)) !== notification.signature) {
			throw new ErrorModel(ERRORS.NOT_VERIFIED, ERROR_CODES.ERR_103, 'Signature key invalid')
		}

		return this.processNotification(await this.checkStatus('DEBIT', {
			trx_id: notification.trx_id,
			merchant_id: notification.merchant_id,
			request: notification.request,
			bill_no: notification.bill_no,
		}), notification)
	}

	async handleCreditTransaction(response: CreditReturnResponseInterface) {
		this.log(`Transaction return url received. Order ID: ${response.MERCHANT_TRANID}. Transaction status: ${response.TXN_STATUS}.`)

		// =================================
		// CHECK SIGNATURE HASH
		// =================================

		if (`${sha1(`##${ this.get('creditMerchantId') }##${ this.get('creditMerchantPassword') }##${ response.MERCHANT_TRANID }##${ response.AMOUNT }##${ response.TXN_STATUS }##`.toUpperCase())}`.toUpperCase() !== response.SIGNATURE) {
			throw new ErrorModel(ERRORS.NOT_VERIFIED, ERROR_CODES.ERR_103, 'Signature key invalid')
		}

		return this.processCreditNotification('4', await this.checkStatus('CREDIT', {
			PAYMENT_METHOD: '1',
			TRANSACTIONID: response.TRANSACTIONID,
			TRANSACTIONTYPE: '4',
			MERCHANTID: response.MERCHANTID,
			MERCHANT_TRANID: response.MERCHANT_TRANID,
			AMOUNT: response.AMOUNT,
			RESPONSE_TYPE: '3',
		}), response)
	}

	async getStatus(type: 'DEBIT', parameter: Without<InquiryParameterInterface, 'signature'>)
	async getStatus(type: 'CREDIT', parameter: Without<CreditInquiryParameterInterface, 'SIGNATURE'>)
	async getStatus(type: 'DEBIT' | 'CREDIT', parameter: Without<InquiryParameterInterface, 'signature'> | Without<CreditInquiryParameterInterface, 'SIGNATURE'>): Promise<PAYMENTS> {
		if(type === 'DEBIT') {
			const response: InquiryResponseInterface = await this.checkStatus('DEBIT', parameter as InquiryParameterInterface)

			return (await this.processNotification(response)).code
		} else if(type === 'CREDIT') {
			const response: CreditInquiryResponseInterface = await this.checkStatus('CREDIT', parameter as CreditInquiryParameterInterface)

			return (await this.processCreditNotification('4', response)).code
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Type not valid.')
		}
	}

	private async checkStatus(type: 'DEBIT', parameter: Without<InquiryParameterInterface, 'signature'>): Promise<InquiryResponseInterface>
	private async checkStatus(type: 'CREDIT', parameter: Without<CreditInquiryParameterInterface, 'SIGNATURE'>): Promise<CreditInquiryResponseInterface>
	private async checkStatus(type: 'DEBIT' | 'CREDIT', parameter: Without<InquiryParameterInterface, 'signature'> | Without<CreditInquiryParameterInterface, 'SIGNATURE'>): Promise<any> {
		if (type === 'DEBIT') {
			return FaspayService.checkStatus((parameter as InquiryParameterInterface).trx_id, {
				...(parameter as InquiryParameterInterface),
				signature: sha1(md5(`${this.get('userId')}${this.get('password')}${(parameter as InquiryParameterInterface).bill_no}`)),
			})
		} else if (type === 'CREDIT') {
			return FaspayService.checkCreditStatus<CreditInquiryParameterInterface, CreditInquiryResponseInterface>({
				...parameter as CreditInquiryParameterInterface,
				SIGNATURE: `${sha1(`##${this.get('creditMerchantId')}##${this.get('creditMerchantPassword')}##${(parameter as CreditInquiryParameterInterface).MERCHANT_TRANID}##${(parameter as CreditInquiryParameterInterface).AMOUNT}##${(parameter as CreditInquiryParameterInterface).TRANSACTIONID || 0}##`.toUpperCase())}`.toUpperCase(),
			})
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Type must be specified as either CREDIT or DEBIT.')
		}
	}

	private async processNotification(response: InquiryResponseInterface, notification?: PaymentNotificationInterface) {

		// ===================================================
		// CHECK STATUS CODE, FRAUD STATUS, TRANSACTION STATUS
		// ===================================================

		this.log('Processing Notification')
		this.log(response, notification)

		let code: PAYMENTS
		let note: string

		switch(response.payment_status_code) {
		case 0:
		case '0':
			code = PAYMENTS.PENDING
			note = 'Payment isn\'t processed yet.'
			break
		case 1:
		case '1':
			code = PAYMENTS.PENDING
			note = 'Payment is in process.'
			break
		case 2:
		case '2':
			code = PAYMENTS.SUCCESS
			note = 'Payment success, status code 2.'
			break
		case 3:
		case '3':
			code = PAYMENTS.FAILED
			note = 'Payment failed, status code 3.'
			break
		case 4:
		case '4':
			code = PAYMENTS.EXCEPTION
			note = 'Payment reversal, status code 4'
			break
		case 7:
		case '7':
			code = PAYMENTS.FAILED
			note = 'Payment expired, status code 7'
			break
		case 8:
		case '8':
			code = PAYMENTS.FAILED
			note = 'Payment cancelled, status code 8'
			break
		case 9:
		case '9':
			code = PAYMENTS.PENDING
			note = 'Payment pending (unknown), status code 9'
			break
		default:
			code = PAYMENTS.EXCEPTION
			note = `Payment exception, status code ${response.payment_status_code}`
			break
		}

		return {
			order_number: response.bill_no,
			code,
			note,
			metadata: {
				notification,
				transaction: response,
			},
		}
	}

	private async processCreditNotification(type: '4', response: CreditInquiryResponseInterface, notification?: CreditReturnResponseInterface): Promise<{ order_number: string, code: PAYMENTS, note: string, metadata: { transaction: typeof response, notification?: typeof notification } }>
	private async processCreditNotification(type: '2', response: CreditCaptureResponseInterface, notification?: CreditReturnResponseInterface): Promise<{ order_number: string, code: PAYMENTS, note: string, metadata: { transaction: typeof response, notification?: typeof notification } }>
	private async processCreditNotification(type: '10', response: CreditVoidResponseInterface, notification?: CreditReturnResponseInterface): Promise<{ order_number: string, code: PAYMENTS, note: string, metadata: { transaction: typeof response, notification?: typeof notification } }>
	private async processCreditNotification(type: '11', response: CreditRefundResponseInterface, notification?: CreditReturnResponseInterface): Promise<{ order_number: string, code: PAYMENTS, note: string, metadata: { transaction: typeof response, notification?: typeof notification } }>
	private async processCreditNotification(type: '4' | '2' | '10' | '11', response: CreditInquiryResponseInterface | CreditCaptureResponseInterface | CreditVoidResponseInterface | CreditRefundResponseInterface, notification?: CreditReturnResponseInterface): Promise<{
		order_number: string,
		code: PAYMENTS,
		note: string,
		metadata: {
			transaction: typeof response,
			notification?: typeof notification,
		},
	}> {

		// ===================================================
		// CHECK STATUS CODE, FRAUD STATUS, TRANSACTION STATUS
		// ===================================================

		this.log('Processing Credit Notification')
		this.log(response)

		let code: PAYMENTS
		let note: string

		switch (response.TXN_STATUS) {
		case 'N':
			code = PAYMENTS.PENDING
			note = 'Pending process'
			break
		case 'A':
			await this.approveTransaction({
				MERCHANTID: response.MERCHANTID,
				MERCHANT_TRANID: response.MERCHANT_TRANID,
				TRANSACTIONID: response.TRANSACTIONID,
				AMOUNT: response.AMOUNT,
			}).then(() => {
				code = PAYMENTS.SUCCESS
				note = 'Credit authorized. Captured by Yuna.'
			}).catch(err => {
				code = PAYMENTS.FAILED
				note = err.message
			})
			break
		case 'E':
		default:
			code = PAYMENTS.FAILED
			note = `Payment errored (${response.ERR_CODE}).${ (response as any).USR_MSG || response.ERR_DESC ? ` ${(response as any).USR_MSG || response.ERR_DESC}` : ''}`
			break
		case 'C':
			code = PAYMENTS.SUCCESS
			note = 'Capture success.'
			break
		case 'CF':
			code = PAYMENTS.FAILED
			note = 'Capture failed.'
			break
		case 'S':
			code = PAYMENTS.SUCCESS
			note = 'Approved sales.'
			break
		case 'F':
			code = PAYMENTS.FAILED
			note = 'Declined by bank.'
			break
		case 'RC':
			code = PAYMENTS.PENDING
			note = 'RC. Need to reconfirm.'
			break
		case 'CB':
			code = PAYMENTS.EXCEPTION
			note = 'Chargeback by bank.'
			break
		case 'V':
			code = PAYMENTS.FAILED
			note = 'Payment void.'
			break
		case 'B':
			code = PAYMENTS.FAILED
			note = 'Blocked.'
			break
		case 'CR':
			code = PAYMENTS.EXCEPTION
			note = 'CR. Capture reversal success.'
			break
		case 'CRF':
			code = PAYMENTS.EXCEPTION
			note = 'CRF. Capture reversal failed.'
			break
		case 'SR':
			code = PAYMENTS.EXCEPTION
			note = 'SR. Sales reversed.'
			break
		case 'SRF':
			code = PAYMENTS.EXCEPTION
			note = 'SRF. Sales reversal failed.'
			break
		case 'PCR':
			code = PAYMENTS.EXCEPTION
			note = 'PCR. Capture reversal in progress.'
			break
		case 'PSR':
			code = PAYMENTS.EXCEPTION
			note = 'PSR. Sales reversal in progress.'
			break
		case 'PCB':
			code = PAYMENTS.EXCEPTION
			note = 'PCB. Chargeback in progress.'
			break
		case 'I':
			code = PAYMENTS.FAILED
			note = 'Incomplete data.'
			break
		}

		return {
			order_number: response.MERCHANT_TRANID,
			code,
			note,
			metadata: {
				transaction: response,
				notification,
			},
		}
	}

	private async approveTransaction(data: Without<CreditCaptureParameterInterface, 'PAYMENT_METHOD' | 'TRANSACTIONTYPE' | 'RESPONSE_TYPE' | 'SIGNATURE' | 'RETURN_URL'>) {
		return FaspayService.checkCreditStatus<CreditCaptureParameterInterface, CreditCaptureResponseInterface>({
			PAYMENT_METHOD: '1',
			TRANSACTIONTYPE: '2',
			RESPONSE_TYPE: '3',
			SIGNATURE: `${sha1(`##${this.get('creditMerchantId')}##${this.get('creditMerchantPassword')}##${data.MERCHANT_TRANID}##${data.AMOUNT}##${data.TRANSACTIONID}##`.toUpperCase())}`.toUpperCase(),
			RETURN_URL: process.env.FASPAY_CREDIT_RETURN_URL,
			...data,
		})
	}

	// private async voidTransaction(order_number: string) {
	// 	return this.snapper.transaction.cancel(order_number)
	// }

	// private async refundTransaction(order_number: string, amount: number, refund_id?: string, reason?: string) {
	// 	return this.snapper.transaction.refund(order_number, {
	// 		amount,
	// 		refund_key: refund_id,
	// 		reason,
	// 	})
	// }

	private generateHtml(link: string, number: string, data: string) {
		return `
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html lang="en">
				<head>
					<title>Yuna & Co. — Payment of ${ number }</title>
					<meta charset="utf-8">
					<meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
					<!-- Mobile viewport optimized: h5bp.com/viewport -->
					<meta name="viewport" content="width=800">
					<link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<style type="text/css">p{margin: 0}</style>
				</head>
				<body style="margin:0;">
					<div style="flex: 1; display: flex; height: 100vh; width: 100vw; align-items: center; justify-content: center; flex-direction: column;">
						<p style="font-family: work sans; color: #27272B; font-size: 20px; font-weight: 500; margin-bottom: 24px;">Processing Your Payment</p>
						<img src="data:image/gif;base64,R0lGODlhMAAwAPYAAGNjY2RkZFhYWHx8fEFBQW9vb4eHh+fn6JOTk6CgoKurq7W2tllZWUBAQMLCws3OzjU1NXt7e3BwcEtLS5+fn87OztjY2MHCwj8/P01NTba2tqqrq05OTpKTk3t7fNvb3NjY2Z6fn+Pk5IeHiOTl5oGBgUxMTG5ubnx8fTQ0NLW2t8HBws3Oz+bn6J+goLe3t56ens3NzaurrFBQUMPExNna28HCw87Oz7a2t97f3+jp6sHBweLi483NzoyMjMPDxHJyc4aHh52entDQ0NbW1qqqqpKSk7W1teTl5eXm58LDw7W1ttbX2NjZ2VdXV1paWmJiYn19fYCAgdTV1kNDQ5OUlIiIiMPDw9fX13Bwca2tra6urq+vr6WlpZKSkkpKSpSUldfY2KCgoZSUlObn57a3t+bm5zY2NltbW1xcXGBgYGFhYWdnZ2lpaXd3d3h4eIaGhomJiZycnZ2dnqGhobCwsLm5ubq6usLCw9PU1N3d3uHi4gAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJAwAAACH+GU9wdGltaXplZCB1c2luZyBlemdpZi5jb20ALAAAAAAwADAAAAfLgACCg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjpKWmp6ipqo53b2ltMIt5cWtrcRaLc21pb3eJcBDBwQx6iHXCwnWIegzIEAaHYs7BbId508G4hmzYdIYz2BB4hnHhcYZ44TOFe+EQctvha4Zy7nuE7eHwhWry9PaFwGEbV6gctnOF0mFbV0jatGqGrmHLc4jbNDGHgCEjZmyaMmvNkMFJZMcNGliyaKm5lWtXr1UwY8qcSbOmzZs4c+rcybOnz582AwEAIfkECQMAAwAsBgATACYACgAABoXAgXDAMgQkiaFSSUIEAgjScqqkQK5XhpTqIGAhBMfUQWMpNV+shErypglbYkY9qCVmaax4ichfEUMFaQoRflgUU4J+AEN5CIWGEIhLinmMQo4kCnh+e0p9foBClVcKQmh5BWwNeQ1xLBN0Q1ZfAnFLXV8NnkNkZktFABGTVEJNAABRxUpBACH5BAkDAAMALAUAEwAmAAoAAAaEwIFwOLgZApIEcSnUJSQBRI5JFSogWCxjSr1lspCGorq0gbMMqu57bnCNgYRuUIAwboBzFsek6LEeAzoNWR43hYR/CEwGf3YDOGeHgIl6i0uNfwIDZmB0dniOfEt+f4GDhUuTZwBqbGBuQnAUc0tXYAJcTF6wY2SqCAAFFL+CFAUAUmRBACH5BAkDAAYALAUAEwAmAAoAAAaAQINwONwMAqMdcSncjQIDCnMq5AUgWOyIakBksQHedIOgiA2Fb3YxXaixkuFD6MUKDI83NjMV6CEPGwQQGVZfD25/Z0R/EBt+WBsAh4l6i0ONj1lkWXd5eiZ9f4EYhGJkZkJpb2xMlV8FclyGX1tUdVkAl1xCRgBJvAZOABEbVEEAIfkECQMAAQAsBAATACcACgAABoPAgHBIDDhGgMGmyCQhAAAEiUkdjiBYLGBKdTSykIaD2ZM0AK6hBpyVUElfdoM7nIDHATsb22Mi9lg+AwBLDmwIASSAWEtFAIsMWSSGYIgBixAaTI+AkVhTelh4oWx9RX+AgoRCZWcUaoAFb3Fgc1VMV2BbVV61prdFR0mNt04ABVJMQQAh+QQJAwADACwEABMAJgAKAAAHjIADgoOEgkNBARIuhYwkCBIBCESNP5OFQhCZmQyWjEMNmhANKoRBmpyDF6GaAYwDJByropYqqxGDALKZF4yYukAuRB2rHIO6mUKMEccpEAXDoRPGx8mFy8wFtaG3grm6vJfHwJOmm52qsgCusLINna8X74K+p/KDQxOho678hwAF1fi96lAAQAcShAIBACH5BAkDAAUALAQAEwAmAAoAAAZ/wIJwSByuDIFBosgkIQIBBIlIUiASFiYCwuUGpszCitCFEFZCi6msIKrKXUmYRIYTpgM7UQDvVrR9XCUFAH1ZQoFcbUWFiQqNZYcFiRCLRJB9CnllDXuJf0VbgYNqbG6BJ3MNfQ1gVVeSQ6JdAGBMY5xoYWFHAAOWu04AAFJDQQAh+QQJAwADACwEABMAJgAKAAAGesCBcEgkalABlKbItCACkgRpeDFAN8zBCcLlSrLCTaMLMVUGGjIkUESouYhs5Q1hDEzv85DOzW7paW9YQhd8EBdMeHQldIMDhXyIRYpvJZRdekKGfnwagV0nbXRxTXQChFYnjkR/oGADYmQCma9DRwBKtQNOACcUUwNBACH5BAkDAAMALAMAEwAoAAoAAAeIgAOCg4SFAzAFASM3hoVHKAEoR4VGARAmKCCGIAwQnp4wjYIRn54Fg6SlBEiFAKWfNo1Gr54jA0i0EKGDNrkQAI2+EEi9tLaDs7kEm8I2xa/Hgsm5zL42uLS7gs+vwIbCrCO0rISutLGGMLlGyK6YmoWcr0WiA6mfp/WiiABGjPpHRgCIMElQIAAh+QQJAwAAACwDABMAKAAKAAAHlIAAggAWg4aGFh0BEi5Jh4dJLhIBHYWDHQQQEAGOjxuZmhAZMY+CMRmhEAQbggapop2DMa+aDKUAArQQMbO0HYcFuhCshxrCBRu6AYcmwr+HHcIZybTLhs26z4bRuia9r9qCwbrEhsa6BQCuqSaxproCt7m0pACYmgXug5/s9Y+nqRqUE2TpFiFFBULogxSiACVLgQAAIfkECQMAAQAsAwATACkACgAAB42AAYJMSiyCh4iCCgMABheJkCwGAAMJiC0SEJocj5AtAJqhBpCHCKGaDC2CoKcETJARp6EUpEuymhIBCrcQBYlMvBAcpALBFwa8BIm2waqJwRAUprfKiMy8zojQFLu3vojAycTGAayhrpDItwq1vN+Y552Jn7KjpAEUsgLZAS0XhvcURWgk754kSrQEBQIAIfkECQMAAgAsAwATACkACgAAB46AAoI/CS9NgoiJAk0IARIJJIqKJAkSAQiHg08QnAQKkoIKBJycGT2gAj0ZpBCeAiSrrBA/kj2ynE+oTrezIby5igW8EJ+KCsMFwrySJsMIkgjDGcq3zM7Q0r63TpLUsguSx7wFsLe0irbburynqqQNxeENrCanoO7v8akhP5mojAAKhIiE6lWIAgAwCQoEACH5BAkDAAcALAMAEwAqAAoAAAaHwMNB9EK4QMKkUhiSBAyVpVQzCAw0yksGwoUgpEIQo8sNgYURMkQSJqghZingDbmAO3TDIf0mSC90EABggRAHc3RRSnh0fksVhReIb4pJjH1SkIEXfG9/gYNShQcgGG9xS5NkdlIhdB1CWmSwYGJqCmd7agVKRB0hSLkHTQAdlWcaBgARWEJBACH5BAkDAAgALAMAEwAqAAoAAAV5IIJ8ydA5YqqK2hAM2ip/XSAlR6oQUA8ZslTB15MERTti5oF4EH2dYOfZi8qcVAZiSIUEu70gl5rsolRYs2wC7rDVaDDkrHpTO+PnFywGK9JEVitTd0GAPgJIGERAR3kQBUcIZT0TTCIkBieSLB4AHjGcNAAFFDkIIQAh+QQJAwAAACwDABMAKgAKAAAHkoAAABUIAQYagomKghUGARIJi5IHCBIBCBaJChCcnBIHkoIhnZwMmaEVJqQECgAXpJ0Goa+wEAGhB6qwBBZQtZynir6/F5Kjvwa/nIiLyhAIkhHKw7/Mis7Qi9K/UNSwwYnepMWLx7UGtLCykumkULi63wCbpAWgoeal4Iqpq9aEBQ6F4ueoQIiBiQ6EKAAFk6BAACH5BAUDAAwALAMAEwAqAAoAAAV5IMNUSDAoYqqKViklxypTUmBUKQXtexDLDAWBB8nggBYBEUIJLncS5HCZ+a0AT8hFmT2qIlmm7BLGhlHXcESGCE+zaJX5uV61y2FvCpxtrshZAAtZBVJPE1ZyWRcMOkQAiSpCRBN6KklLfiMucUCbAAUUkTMFADciIQAh+QQJAwASACwDABMAKgAKAAAFWaAkOYaYiGgqHmKAqLA4iA5axnGdEjp8MKoXDseCEXCF2GL4gAmdsGYMOKyqkioFDmLtirApbYyLk6aewShO4EUdGkakUoK2OuCoRk/1S9NuXUUAdTgRIlIhACH5BAkDAAMALAYAEwAmAAoAAAd7gAOCFBIBIz2CiYqCKlGGKouRilMMEJaWFJKCEZeWEpIbhSM2ggCdl6SRHaeWI4sFpyo2rJYAkrSWk6wcUrgEkT24EKkDG7S9tA3AwsTGrLO0tpHCuqcTA6bPkqusHa+xA5SnG5oDnJ0FoBEAh4qEAB2I5QONABGQ84mBACH5BAkDAAMALAYAEwAmAAoAAAeHgAOCgk1VARIJJIOLgyQJEgFVTYyUixsNEJkQGSyVAywZmhANG56MLKKaDJ4CqZmdgh2hAQ4DGwEjEq6ZpYwbuxASgr+aDSSYo8AdlB3AGYIjqQ7IVMrMztDSt7nAGpQawMK2osaDqK4CrLuwA7IQAOzDyJmcnqDkvaaLhQCIiqaOIHWYpC8QACH5BAkDAAIALAUAEwAmAAoAAAeJgAKCg4IOVgEoG4SLgiAIAQUIJIyUglYQmJgBk5QaDZkQDTeVAg6cFKCZKJQgn6kmnDcbk62YFAImqZkgjJe6ELcemQ6omLC/mBqMAMgeAq4QHsUQx8jKi8y/ztAetcC4yLyLvrrBw4WcG79WrNCZsI2zleSanIyeoCajpIuGAB6K+DkCEMneokAAIfkECQMABAAsBQATACYACgAABoFAgnA4JCECAQSJyBRuBgGDo0kVshqQLKQxpZIC2qyhaQwMuiRseLtsFtZZDROgbYAICHgW0WTpIRwgAg0DBGtyb3oATRt/ECVafmGIf4tMGo6QWSx0WXZ4f3xMknATgoQEZRFoamENbUyJh1WjrVtdZJ1aY7RMRgUASr0ETwBSVEEAIfkECQMAAwAsBAATACYACgAABnnAgXBIFMIkgVGsyFShAigVcyoEMSBYrIw6iGSxkqKsgUURAd/satpJYztDUBomXLmxgOkdC0OtVGl5A213DUwxe1kcgF+ChG6GRYiJEBNyX3QDdneCRYl9a2NlZ3drTI9fcFxxAmlbVF5fBatMRwAdS6sqIwARUkRBACH5BAkDAAIALAQAEwAmAAoAAAeHgAKCg4SDIVkBIw+FjBojASgajA4vVySFWAwQm5shjIMenJtZlwIkAZwEi4MAopwOnyGumyOCsqIMgw6zmwCfvJtbJAWzgwjABIwPwLTErsbIyswpI7ecvoK7vNiFBMBaJKepq4Kts7CM1qK1uhoOpYOZrp6fAqGiBfX1hwCK+oKOAHiQNCgQACH5BAkDAAIALAQAEwAmAAoAAAePgAKCg4SDJAkSAQhNhY0KEQEGGoRNCBEIV40VGRCdEAQKjYIkAZ6dBoJXBKaohpymn4yNBrCdIQIFtbICIbWdEY0kvp8Cq7CZgrS+DI1XwxBdxqbIAsq1zIXOw1y5sLu9vsCFwr4EAqqshCSvpgS7hNamtwKVl9SDm+2hoiTdnq2igoUoAGBRwEGPABigFggAIfkECQMAAgAsAwATACcACgAAB4uAAoKDhIUWYAESCSSFhSQJEgFgD4QbAQwBI4yFGwQQnxAZlI0CDwygnxuCXqgQDIaerQybhU6tn5QYtwuEHrepjQu/EAUWv16EtseNrL9fxrfIg8rRzMNfArqtvIO+v6qFwr8FAs2gTrC3TrTJv6MLAPGajZ2ooqSl1MD4pIcABRTYkaJQAICXUYEAACH5BAkDAAYALAMAEwAoAAoAAAeOgAaCg4SFBgsjAQMLhoVhCAEFLiSDJCMEEBABDo0RmZ8FjYJFmJ8ZLIIAn5kEYYUwq58jjSyxmQwGtbYdhbaflIWevkULvgCEur6chQK+ECPFtseDybbLhM2+I9WfvITOEMCEwrZFBqqxroSwtrOG3LeClhiZBdfjsaGipKsTqKKiEAGIwAhgrg4AJAELBAAh+QQJAwADACwDABMAKQAKAAAHj4ADggMrZIOHg2RiEgFjIIiQMlEBIyqHZCMQmgwPkAMPGZqaBDKeA2QSoppRgwWqEASGl6GvBJ2Qma8QMJ+6EBGIMr6/kGTDBANevgyIubrMiCvDECDKz83DApDSwyAPvsCHwuDFx4Kur7KJAr632Lq8p84C7oegr6WeqK8j2+r6MAoA8PLIlCAVEQZaEhQIACH5BAkDAAcALAMAEwApAAoAAAeWgAeCPWUOgoeIgg4GAQMbiZBNCAESCCSHTQEQmwQakIIGm6IBl58aBKIQBD2CAqmbrIkUr5sDn02orxkks7QFkCa0m02QocIUEcIQiSTKEJ6JAMoRycLMztCI0sIRva+/icHCxInGtBQHrq+xiBvCBrcNtCaXmaIN2eWvAKWQp6km2B0gZOjToUUAIjwyKIjEpAKWBAUCACH5BAkDAAIALAMAEwAqAAoAAAeQgAICZkoICliCiYqDCAEBCGaLkgoDAQYXihUZEJwECZKCFQScnZigZgGknAaCZqOqEBqSrrAQBIiSBbWxAga7BJIduxAdkhXDGQIAw5GKursAkhrDEKjMi8+10YvTzL67wcPFi8e/gxi1sou0tc3Yu+qaqhSgAuWqprPLqqyJhB2H6rXqUABAB3f1KAG4lCgQACH5BAkDAAYALAMAEwAqAAoAAAaAQINBpEAgKsKkUqgYBEaOpbQyCgwSygUBwoWMpEJRoMv9gg0IMoQhMoC2agQ4ouaGwIs6RGIo6CFTfxlgAn8OhXpRWX8QbUuMIYh1ikl5f45KkH56gXoThIZvdXJSI3oKeHoFTBhkZlJiaq9SIWoCmERGSGdMEQBQvEJUABF3QkEAIfkECQMAAgAsAwATACoACgAABotAgeCDkCA0wqRS6DAEBoqltIUIBBCtpIIA6UIkWanA4O0GwlIH10twCB5lr0GsiXcl4ta6TGgB7F1oSROAEA9SHYWJhW5KLYUQUUt/gAUYjEuPhUiThQWUdoJChICHS4t2HXB2c1J1dgV5l6ECW2UFoklkZQC5TLNepkMdBR2NYkxOHpLIAlQAxWFBACH5BAkDAAoALAMAEwAqAAoAAAV3oKJUSDBsYqqKViklx7oeiRQgVkpBPB/EMsWG0INkKsFRpkhAXYq9QbAC5QmSgiqkAtBuZQUvarXxhr0a2cSLkCG8S7Sa7YZ3tbnVuZpeacxPVRFTWldBWVVIO0UAQDJDTEhTa0VjIyUGlkktAAUUjkE0BQA4IiEAIfkEBQMACQAsAwATACoACgAABYFgklRGMChiqooHEgTIsc6UFBhVikA8H8izxIXQgxByQQujCKEkFkyeJElkcoArQBRyEWwhSJXhi5hdvtovKvst0L5wyFqVjrpXFLh3G06Nt2UrZ1sAUHZBBw1RE1h0WxcJO0UAjSpDRQ19KkpMTiIkABFzQQktAAUxpCkUEQA4IiEAIfkECQMAEAAsAwATACoACgAABUUgBDkilJRoqq7oIJKiwa4wWqdHkCIze6wEVmG1mD1UvB7quGIonyKASgGtNqfWEhOVhG5TgiyrISSKby+flFtCQ9dWWAgAIfkECQMAAwAsBgATACYACgAABoXAgXDAMgQkiaFSSUIEAgjScqqkQK5XhpTqIGAhBMfUQWMpNV+shErypglbYkY9qCVmaax4ichfEUMFaQoRflgUU4J+AEN5CIUphohLinmMQo4kCnh+e0p9foBClVcKQmh5BWwNeQ1xLBN0Q1ZfAnFLXV8NnkNkZktFABGTVEJNAABRxUpBACH5BAkDAAMALAUAEwAmAAoAAAaEwIFwOLgZApIEcSnUJSQBRI5JFSogWCxjSr1lspCGorq0gbMMqu57bnCNgYRuUIAwboBzFsek6LEeAzoNWR43hYR/CEwGf3YDOGeHgIl6i0uNfwIDZmB0dniOfEt+f4GDhUuTZwBqbGBuQnAUc0tXYAJcTF6wY2SqCAAFFL+CFAUAUmRBACH5BAkDAAYALAUAEwAmAAoAAAaAQINwONwMAqMdcSncjQIDCnMq5AUgWOyIakBksQHedIOgiA2Fb3YxXaixkuFD6MUKDI83NjMV6CEPGwQQGVZfD25/Z0R/EBt+WBsAh4l6i0ONj1lkWXd5eiZ9f4EYhGJkZkJpb2xMlV8FclyGX1tUdVkAl1xCRgBJvAZOABEbVEEAIfkECQMAAQAsBAATACcACgAABoPAgHBIDDhGgMGmyCQhAAAEiUkdjiBYLGBKdTSykIaD2ZM0AK6hBpyVUElfdoM7nIDHATsb22Mi9lg+AwBLDmwIASSAWEtFAIsMWSSGYIgBixAaTI+AkVhTelh4oWx9RX+AgoRCZWcUaoAFb3Fgc1VMV2BbVV61prdFR0mNt04ABVJMQQAh+QQJAwADACwEABMAJgAKAAAHjIADgoOEgkNBARIuhYwkCBIBCESNP5OFQhCZmQyWjEMNmhANKoRBmpyDF6GaAYwDJByropYqqxGDALKZF4yYukAuRB2rHIO6mUKMEccpEAXDoRPGx8mFy8wFtaG3grm6vJfHwJOmm52qsgCusLINna8X74K+p/KDQxOho678hwAF1fi96lAAQAcShAIBACH5BAkDAAUALAQAEwAmAAoAAAZ/wIJwSByuDIFBosgkIQIBBIlIUiASFiYCwuUGpszCitCFEFZCi6msIKrKXUmYRIYTpgM7UQDvVrR9XCUFAH1ZQoFcbUWFiQqNZYcFiRCLRJB9CnllDXuJf0VbgYNqbG6BJ3MNfQ1gVVeSQ6JdAGBMY5xoYWFHAAOWu04AAFJDQQAh+QQJAwADACwEABMAJgAKAAAGesCBcEgkalABlKbItCACkgRpeDFAN8zBCcLlSrLCTaMLMVUGGjIkUESouYhs5Q1hDEzv85DOzW7paW9YQhd8EBdMeHQldIMDhXyIRYpvJZRdekKGfnwagV0nbXRxTXQChFYnjkR/oGADYmQCma9DRwBKtQNOACcUUwNBACH5BAkDAAMALAMAEwAoAAoAAAeIgAOCg4SFAzAFASM3hoVHKAEoR4VGARAmKCCGIAwQnp4wjYIRn54Fg6SlBEiFAKWfNo1Gr54jA0i0EKGDNrkQAI2+EEi9tLaDs7kEm8I2xa/Hgsm5zL42uLS7gs+vwIbCrCO0rISutLGGMLlGyK6YmoWcr0WiA6mfp/WiiABGjPpHRgCIMElQIAAh+QQJAwAAACwDABMAKAAKAAAHlIAAggAWg4aGFh0BEi5Jh4dJLhIBHYWDHQQQEAGOjxuZmhAZMY+CMRmhEAQbggapop2DMa+aDKUAArQQMbO0HYcFuhCshxrCBRu6AYcmwr+HHcIZybTLhs26z4bRuia9r9qCwbrEhsa6BQCuqSaxproCt7m0pACYmgXug5/s9Y+nqRqUE2TpFiFFBULogxSiACVLgQAAIfkECQMAAQAsAwATACkACgAAB42AAYJMSiyCh4iCCgMABheJkCwGAAMJiC0SEJocj5AtAJqhBpCHCKGaDC2CoKcETJARp6EUpEuymhIBCrcQBYlMvBAcpALBFwa8BIm2waqJwRAUprfKiMy8zojQFLu3vojAycTGAayhrpDItwq1vN+Y552Jn7KjpAEUsgLZAS0XhvcURWgk754kSrQEBQIAIfkECQMAAgAsAwATACkACgAAB46AAoI/CS9NgoiJAk0IARIJJIqKJAkSAQiHg08QnAQKkoIKBJycGT2gAj0ZpBCeAiSrrBA/kj2ynE+oTrezIby5igW8EJ+KCsMFwrySJsMIkgjDGcq3zM7Q0r63TpLUsguSx7wFsLe0irbburynqqQNxeENrCanoO7v8akhP5mojAAKhIiE6lWIAgAwCQoEACH5BAkDAAcALAMAEwAqAAoAAAaHwMNB9EK4QMKkUhiSBAyVpVQzCAw0yksGwoUgpEIQo8sNgYURMkQSJqghZingDbmAO3TDIf0mSC90EABggRAHc3RRSnh0fksVhReIb4pJjH1SkIEXfG9/gYNShQcgGG9xS5NkdlIhdB1CWmSwYGJqCmd7agVKRB0hSLkHTQAdlWcaBgARWEJBACH5BAkDAAgALAMAEwAqAAoAAAV5IIJ8ydA5YqqK2hAM2ip/XSAlR6oQUA8ZslTB15MERTti5oF4EH2dYOfZi8qcVAZiSIUEu70gl5rsolRYs2wC7rDVaDDkrHpTO+PnFywGK9JEVitTd0GAPgJIGERAR3kQBUcIZT0TTCIkBieSLB4AHjGcNAAFFDkIIQAh+QQJAwAAACwDABMAKgAKAAAHkoAAABUIAQYagomKghUGARIJi5IHCBIBCBaJChCcnBIHkoIhnZwMmaEVJqQECgAXpJ0Goa+wEAGhB6qwBBZQtZynir6/F5Kjvwa/nIiLyhAIkhHKw7/Mis7Qi9K/UNSwwYnepMWLx7UGtLCykumkULi63wCbpAWgoeal4Iqpq9aEBQ6F4ueoQIiBiQ6EKAAFk6BAACH5BAUDAAwALAMAEwAqAAoAAAV5IMNUSDAoYqqKViklxypTUmBUKQXtexDLDAWBB8nggBYBEUIJLncS5HCZ+a0AT8hFmT2qIlmm7BLGhlHXcESGCE+zaJX5uV61y2FvCpxtrshZAAtZBVJPE1ZyWRcMOkQAiSpCRBN6KklLfiMucUCbAAUUkTMFADciIQAh+QQJAwASACwDABMAKgAKAAAFWaAkOYaYiGgqHmKAqLA4iA5axnGdEjp8MKoXDseCEXCF2GL4gAmdsGYMOKyqkioFDmLtirApbYyLk6aewShO4EUdGkakUoK2OuCoRk/1S9NuXUUAdTgRIlIhACH5BAkDAAMALAYAEwAmAAoAAAd7gAOCFBIBIz2CiYqCKlGGKouRilMMEJaWFJKCEZeWEpIbhSM2ggCdl6SRHaeWI4sFpyo2rJYAkrSWk6wcUrgEkT24EKkDG7S9tA3AwsTGrLO0tpHCuqcTA6bPkqusHa+xA5SnG5oDnJ0FoBEAh4qEAB2I5QONABGQ84mBACH5BAkDAAMALAYAEwAmAAoAAAeHgAOCgk1VARIJJIOLgyQJEgFVTYyUixsNEJkQGSyVAywZmhANG56MLKKaDJ4CqZmdgh2hAQ4DGwEjEq6ZpYwbuxASgr+aDSSYo8AdlB3AGYIjqQ7IVMrMztDSt7nAGpQawMK2osaDqK4CrLuwA7IQAOzDyJmcnqDkvaaLhQCIiqaOIHWYpC8QACH5BAkDAAIALAUAEwAmAAoAAAeJgAKCg4IOVgEoG4SLgiAIAQUIJIyUglYQmJgBk5QaDZkQDTeVAg6cFKCZKJQgn6kmnDcbk62YFAImqZkgjJe6ELcemQ6omLC/mBqMAMgeAq4QHsUQx8jKi8y/ztAetcC4yLyLvrrBw4WcG79WrNCZsI2zleSanIyeoCajpIuGAB6K+DkCEMneokAAIfkECQMABAAsBQATACYACgAABoFAgnA4JCECAQSJyBRuBgGDo0kVshqQLKQxpZIC2qyhaQwMuiRseLtsFtZZDROgbYAICHgW0WTpIRwgAg0DBGtyb3oATRt/ECVafmGIf4tMGo6QWSx0WXZ4f3xMknATgoQEZRFoamENbUyJh1WjrVtdZJ1aY7RMRgUASr0ETwBSVEEAIfkECQMAAwAsBAATACYACgAABnnAgXBIFMIkgVGsyFShAigVcyoEMSBYrIw6iGSxkqKsgUURAd/satpJYztDUBomXLmxgOkdC0OtVGl5A213DUwxe1kcgF+ChG6GRYiJEBNyX3QDdneCRYl9a2NlZ3drTI9fcFxxAmlbVF5fBatMRwAdS6sqIwARUkRBACH5BAkDAAIALAQAEwAmAAoAAAeHgAKCg4SDIVkBIw+FjBojASgajA4vVySFWAwQm5shjIMenJtZlwIkAZwEi4MAopwOnyGumyOCsqIMgw6zmwCfvJtbJAWzgwjABIwPwLTErsbIyswpI7ecvoK7vNiFBMBaJKepq4Kts7CM1qK1uhoOpYOZrp6fAqGiBfX1hwCK+oKOAHiQNCgQACH5BAkDAAIALAQAEwAmAAoAAAePgAKCg4SDJAkSAQhNhY0KEQEGGoRNCBEIV40VGRCdEAQKjYIkAZ6dBoJXBKaohpymn4yNBrCdIQIFtbICIbWdEY0kvp8Cq7CZgrS+DI1XwxBdxqbIAsq1zIXOw1y5sLu9vsCFwr4EAqqshCSvpgS7hNamtwKVl9SDm+2hoiTdnq2igoUoAGBRwEGPABigFggAIfkECQMAAgAsAwATACcACgAAB4uAAoKDhIUWYAESCSSFhSQJEgFgD4QbAQwBI4yFGwQQnxAZlI0CDwygnxuCXqgQDIaerQybhU6tn5QYtwuEHrepjQu/EAUWv16EtseNrL9fxrfIg8rRzMNfArqtvIO+v6qFwr8FAs2gTrC3TrTJv6MLAPGajZ2ooqSl1MD4pIcABRTYkaJQAICXUYEAACH5BAkDAAYALAMAEwAoAAoAAAeOgAaCg4SFBgsjAQMLhoVhCAEFLiSDJCMEEBABDo0RmZ8FjYJFmJ8ZLIIAn5kEYYUwq58jjSyxmQwGtbYdhbaflIWevkULvgCEur6chQK+ECPFtseDybbLhM2+I9WfvITOEMCEwrZFBqqxroSwtrOG3LeClhiZBdfjsaGipKsTqKKiEAGIwAhgrg4AJAELBAAh+QQJAwADACwDABMAKQAKAAAHj4ADggMrZIOHg2RiEgFjIIiQMlEBIyqHZCMQmgwPkAMPGZqaBDKeA2QSoppRgwWqEASGl6GvBJ2Qma8QMJ+6EBGIMr6/kGTDBANevgyIubrMiCvDECDKz83DApDSwyAPvsCHwuDFx4Kur7KJAr632Lq8p84C7oegr6WeqK8j2+r6MAoA8PLIlCAVEQZaEhQIACH5BAkDAAcALAMAEwApAAoAAAeWgAeCPWUOgoeIgg4GAQMbiZBNCAESCCSHTQEQmwQakIIGm6IBl58aBKIQBD2CAqmbrIkUr5sDn02orxkks7QFkCa0m02QocIUEcIQiSTKEJ6JAMoRycLMztCI0sIRva+/icHCxInGtBQHrq+xiBvCBrcNtCaXmaIN2eWvAKWQp6km2B0gZOjToUUAIjwyKIjEpAKWBAUCACH5BAkDAAIALAMAEwAqAAoAAAeQgAICZkoICliCiYqDCAEBCGaLkgoDAQYXihUZEJwECZKCFQScnZigZgGknAaCZqOqEBqSrrAQBIiSBbWxAga7BJIduxAdkhXDGQIAw5GKursAkhrDEKjMi8+10YvTzL67wcPFi8e/gxi1sou0tc3Yu+qaqhSgAuWqprPLqqyJhB2H6rXqUABAB3f1KAG4lCgQACH5BAkDAAYALAMAEwAqAAoAAAaAQINBpEAgKsKkUqgYBEaOpbQyCgwSygUBwoWMpEJRoMv9gg0IMoQhMoC2agQ4ouaGwIs6RGIo6CFTfxlgAn8OhXpRWX8QbUuMIYh1ikl5f45KkH56gXoThIZvdXJSI3oKeHoFTBhkZlJiaq9SIWoCmERGSGdMEQBQvEJUABF3QkEAIfkECQMAAgAsAwATACoACgAABotAgeCDkCA0wqRS6DAEBoqltIUIBBCtpIIA6UIkWanA4O0GwlIH10twCB5lr0GsiXcl4ta6TGgB7F1oSROAEA9SHYWJhW5KLYUQUUt/gAUYjEuPhUiThQWUdoJChICHS4t2HXB2c1J1dgV5l6ECW2UFoklkZQC5TLNepkMdBR2NYkxOHpLIAlQAxWFBACH5BAkDAAoALAMAEwAqAAoAAAV3oKJUSDBsYqqKViklx7oeiRQgVkpBPB/EMsWG0INkKsFRpkhAXYq9QbAC5QmSgiqkAtBuZQUvarXxhr0a2cSLkCG8S7Sa7YZ3tbnVuZpeacxPVRFTWldBWVVIO0UAQDJDTEhTa0VjIyUGlkktAAUUjkE0BQA4IiEAIfkEBQMACQAsAwATACoACgAABYFgklRGMChiqooHEgTIsc6UFBhVikA8H8izxIXQgxByQQujCKEkFkyeJElkcoArQBRyEWwhSJXhi5hdvtovKvst0L5wyFqVjrpXFLh3G06Nt2UrZ1sAUHZBBw1RE1h0WxcJO0UAjSpDRQ19KkpMTiIkABFzQQktAAUxpCkUEQA4IiEAOw==" alt="loading" style="height: 24px; width: 24px;">
					</div>
					<script>
						var link = "${ link}";
						var data = ${ data };
						var form = document.createElement('form');

						form.target = '_self';
						form.method = 'POST';
						form.action = link;
						form.style.display = 'none';

						for (var key in data) {
							var input = document.createElement('input');
							input.type = 'hidden';
							input.name = key;
							input.value = data[key];
							form.appendChild(input);
						}

						document.body.appendChild(form);
						form.submit();

						setTimeout(function() {
							document.body.removeChild(form);
						}, 100)
					</script>
				</body>
			</html>
		`
	}

	private serialize(mixedValue: any) {
		let val = ''
		let key = ''
		let okey = ''
		let ktype = ''
		let vals = ''
		let count = 0

		const regexp = new RegExp(/(\w+)\(/)
		const _utf8Size = str => {
			// tslint:disable-next-line: no-bitwise
			return ~-encodeURI(str).split(/%..|./).length
		}

		const _getType = inp => {
			let _key: string | number
			let match: string[]
			let cons: string
			let types: string[]
			let _type = typeof inp

			if (_type === 'object' && !inp) {
				return 'null'
			}

			if (_type === 'object') {
				if (!inp.constructor) {
					return 'object'
				}
				cons = inp.constructor.toString()
				match = cons.match(regexp)
				if (match) {
					cons = match[1].toLowerCase()
				}
				types = ['boolean', 'number', 'string', 'array']
				for (_key in types) {
					if (cons === types[_key]) {
						_type = types[_key] as any
						break
					}
				}
			}
			return _type
		}

		const type = _getType(mixedValue) as any

		switch (type) {
		case 'function':
			val = ''
			break
		case 'boolean':
			val = 'b:' + (mixedValue ? '1' : '0')
			break
		case 'number':
			val = (Math.round(mixedValue) === mixedValue ? 'i' : 'd') + ':' + mixedValue
			break
		case 'string':
			val = 's:' + _utf8Size(mixedValue) + ':"' + mixedValue + '"'
			break
		case 'array':
		case 'object':
			val = 'a'

			for (key in mixedValue) {
				if (mixedValue.hasOwnProperty(key)) {
					ktype = _getType(mixedValue[key])
					if (ktype === 'function') {
						continue
					}

					okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key) as any
					vals += this.serialize(okey) + this.serialize(mixedValue[key])
					count++
				}
			}
			val += ':' + count + ':{' + vals + '}'
			break
		case 'undefined':
		default:
			val = 'N'
			break
		}

		if (type !== 'object' && type !== 'array') {
			val += ';'
		}

		return val
	}

}

export default new FaspayHandler()
