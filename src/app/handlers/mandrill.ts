import HandlerModel from 'app/models/handler'

import { Mandrill } from 'mandrill-api'

export enum TEMPLATES {
	'VERIFY' = 'Verification - v1.0',
	// 'RESET_PASSWORD' = 'Reset Password',
	// NOTIFICATION
	'WELCOME' = 'Welcome - v1.0',
	'CHECKOUT' = 'Checkout - v1.1',
	'GIFT' = 'Order eGift Card - v1.0',
	'OUT_OF_STOCK' = 'Order Cancellation - Out Of Stock - v1.1',
	'ORDER_CANCELATION' = 'Order Cancelation - v1.1',
	'ORDER_CONFIRMATION_COMPLETE' = 'Order Confirmation (SPQ Complete) - v1.1',
	'ORDER_CONFIRMATION_INCOMPLETE' = 'Order Confirmation (SPQ Incomplete) - v1.1',
	'POSTPONED' = 'Order Postponed - v1.0',
	'INCOMPLETE_STYLE_PROFILE' = 'Incomplete Style Profile - v1.0',
	'SHIPPING' = 'Shipping Notice - v1.0',
	'DELIVERED' = 'Shipping Delivered - v1.0',
	'EXPIRED' = 'Order Expired - v1.1',
	'FEEDBACK' = 'Feedback - v1.0',
	'MATCHES_READY' = 'Matches ready - v1.1',
	'NOTIFICATION' = 'NOTIFICATION - v1.1',
}

class MandrillHandler extends HandlerModel<{
	token: string,
}> {
	protected static __displayName = 'Mandrill Handler'

	private mandrill: Mandrill = null

	constructor() {
		super({
			token: process.env.MANDRILL_TOKEN,
		})

		this.mandrill = new Mandrill(this.get('token'), process.env.DEBUG === 'true')
	}

	async send(
		template_name: TEMPLATES,
		to: {
			email: string,
			first_name: string,
			last_name?: string,
		},
		{
			vars,
			...message
		}: {
			subject: string,
			important: boolean,
			merge_language: 'mailchimp' | 'handlebars',
			tags: string[],
			vars: {
				[k: string]: any,
			},
		},
		from: {
			from_email: string,
			from_name: string,
		} = {
			from_email: 'noreply@helloyuna.io',
			from_name: 'Yuna & Co.',
		},
	): Promise<any> {
		return new Promise((resolver, rejector) => {
			this.mandrill.messages.sendTemplate({
				template_name,
				template_content: [],
				message: {
					to: [{
						email: to.email,
						name: `${ to.first_name }${ to.last_name ? ' ' + to.last_name : '' }`,
					}],
					...from,
					...message,
					merge_vars: [{
						rcpt: to.email,
						vars: Object.keys(vars).map(key => {
							return {
								name: key,
								content: vars[key],
							}
						}),
					}],
					track_opens: true,
					track_clicks: false,
					preserve_recipients: false,
					merge: true,
				},
				async: true,
			}, resolver, rejector)
		})
	}

}

export default new MandrillHandler()
