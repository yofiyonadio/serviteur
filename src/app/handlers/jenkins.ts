import HandlerModel from 'app/models/handler'
import JenkinsService from 'app/services/jenkins'


class JenkinsHandler extends HandlerModel<{
	token: string,
	msg: string
}> {
	protected static __displayName = 'Jenkins Handler'

	constructor() {
		super({
			token: 'KmKrUUYbxm5BMxeR',
			// title: group name,
			// phone: '089639722246',
			msg: 'No reply! Ada order personal styling masuk, tolong di cek ya di menu styleboard!',
		})
	}

	notifyByNewStyleboard(phone) {
		if(process.env.ENV === 'production') {
			return JenkinsService.notifyByNewStyleboard({
				phone,
				token: this.get('token'),
				msg: this.get('msg'),	
			}).then(() => {
				return true
			}).catch(err => {
				this.warn(err)
	
				return false
			})
		}
		return false
	}
}

export default new JenkinsHandler()
