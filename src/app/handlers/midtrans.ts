import HandlerModel from 'app/models/handler'
import { Snap } from 'midtrans-client'
import encryption from 'js-sha512'


import {
	NotificationParameterInterface,
	TransactionActionRespondInterface,
	TransactionExpireRespondInterface,
	TransactionParameterInterface,
	TransactionRefundRespondInterface,
	TransactionStatusRespondInterface,
} from 'app/interfaces/midtrans'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import { PAYMENTS } from 'energie/utils/constants/enum'


class MidtransHandler extends HandlerModel<{
	isProduction: boolean,
	serverKey: string,
	clientKey: string,
}> {
	protected static __displayName = 'Midtrans Handler'

	private snapper: {
		createTransactionRedirectUrl: (...args: any[]) => Promise<string>,
		transaction: {
			// notification: (notification: NotificationParameterInterface) => any,
			status: (order_number: string) => Promise<TransactionStatusRespondInterface>,
			approve: (order_number: string) => Promise<TransactionActionRespondInterface>,
			deny: (order_number: string) => Promise<TransactionActionRespondInterface>,
			cancel: (order_number: string) => Promise<TransactionActionRespondInterface>,
			expire: (order_number: string) => Promise<TransactionExpireRespondInterface>,
			refund: (order_number: string, config: {
				amount: number,
				refund_key?: string,
				reason?: string,
			}) => TransactionRefundRespondInterface,
		},
	}

	constructor() {
		super({
			isProduction: process.env.ENV === 'production',
			serverKey: process.env.MIDTRANS_SERVER_KEY,
			clientKey: process.env.MIDTRANS_CLIENT_KEY,
		})

		this.snapper = new Snap({
			isProduction: this.get('isProduction'),
			serverKey: this.get('serverKey'),
			clientKey: this.get('clientKey'),
		})
	}

	transaction(parameter: TransactionParameterInterface) {
		return this.snapper
			.createTransactionRedirectUrl(parameter)
			// TODO process status code
	}

	async handleNotification(notification: NotificationParameterInterface) {
		// 1. Always use an HTTPS endpoint.It is secure and there cannot be MITM attacks because we validate the certificates match the hosts.Also do not use self signed certificates.
		// 2. Use standard port(80 / 443) for notification callback url
		// 3. Always implement notification in an idempotent way, in extremely rare cases, we may send multiple notifications for the same transaction event twice.It should not cause double entries in the merchant end, The simple way of achieving this is to use orderid as the key to track the entries.
		// 4. Always check the signature hash of the notification, This will confirm that the notification was actually sent by Midtrans, because we encode the shared secret(server key).Nobody else can build this signature hash.

		this.log(`Transaction notification received. Order ID: ${notification.order_id}. Transaction status: ${notification.transaction_status}. Fraud status: ${notification.fraud_status}`)

		// =================================
		// CHECK SIGNATURE HASH
		// =================================

		if (encryption.sha512(notification.order_id + notification.status_code + notification.gross_amount + this.get('serverKey')) !== notification.signature_key) {
			throw new ErrorModel(ERRORS.NOT_VERIFIED, ERROR_CODES.ERR_103, 'Signature key invalid')
		}

		return this.processNotification(await this.checkStatus(notification.transaction_id), notification)
	}

	private async processNotification(transaction: TransactionStatusRespondInterface, notification?: NotificationParameterInterface) {
		// 5. Always check all the following three fields for confirming success transaction
		// 		status code: Should be 200 for successful transactions
		// 		fraud status: ACCEPT
		// 		transaction status: settlement / capture
		// 6. We strive to send the notification immediately after the transaction has occurred, but in extremely rare cases, it may be delayed because of transaction spikes.If you have not received a notification, please use the Status API to check for current status of the transaction.
		// 7. It is safe to call Status API to get the latest status of the transaction / order on each notification.
		// 8. We set the HTTP timeout to 15 seconds.
		// 9. Please strive to keep the response time of the the HTTP notifications under 5 seconds.
		// 10. In extremely rare cases we may send the HTTP notifications out of order, ie.a "settlement" status for a notification before the notification for "Pending" status.It's important that such later notifications are ignored. Here's the state transition diagram that you could use.But again, use our / status API to confirm the actual status.
		// 11. We send the notification body as JSON, please parse the JSON with a JSON parser.Always expect new fields will be added to the notification body, so parse it in a non strict format, so if the parser sees new fields, it should not throw exception.It should gracefully ignore the new fields.This allows us to extend our notification system for newer use cases without breaking old clients.
		// 12. Always use the right HTTP Status code for responding to the notification, we handle retry for error cases differently based on the status code
		// 		for 2xx: No retries, it is considered success
		// 		for 500: Retry only once
		// 		for 503: Retry 4 times
		// 		for 400 / 404: Retry 2 times
		// 		for 301 / 302 / 303: No retries.We suggest the notificaiton endpoint should be update in setting instead of reply these status code.
		// 		for 307 / 308: Follow the new url with POST method and same notification body.Max redirect is 5 times
		// 		for all other failures: Retry 5 times
		// 13. We do retry at most 5 times with following policy
		// 		Different retry interval from 1st time to 5th time(2m, 10m, 30m, 1.5hour, 3.5hour)
		// 		Put a random time shift for each time retry base on above interval.For example, the first time retry might be 33s(at most 2m) after the job failed.

		// ===================================================
		// CHECK STATUS CODE, FRAUD STATUS, TRANSACTION STATUS
		// ===================================================

		this.log('Processing Notification')
		this.log(transaction, notification)

		if (transaction.status_code !== '200' && transaction.status_code !== '201' && transaction.status_code !== '202') {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, transaction.status_code)
		}

		let code: PAYMENTS
		let note: string

		switch (transaction.fraud_status) {
		case 'accept':
			code = PAYMENTS.SUCCESS
			note = 'Approved by FDS.'
			break
		case 'challenge':
			await this.approveTransaction(transaction.order_id).then(() => {
				code = PAYMENTS.SUCCESS
				note = 'Questioned by FDS. Approved by Yuna.'
			}).catch(err => {
				code = PAYMENTS.FAILED
				note = err.message
			})
			break
		case 'deny':
			code = PAYMENTS.FAILED
			note = 'Denied by FDS.'
			break
		}

		if (code === PAYMENTS.SUCCESS) {
			switch (transaction.transaction_status) {
			case 'authorize':
				note += '\nCredit card is authorized.'
				break
			case 'capture':
				note += '\nTransaction is accepted by the bank and ready for settlement.'
				break
			case 'settlement':
				note += '\nTransaction status is settlement.'
				break
			case 'cancel':
				code = PAYMENTS.FAILED
				note += '\nTransaction status is cancelled.'
				break
			case 'expire':
				code = PAYMENTS.EXCEPTION
				note += '\nTransaction status is expired.'
				break
			case 'pending':
				code = PAYMENTS.PENDING
				note += '\nTransaction status is pending.'
				break
			}
		}

		return {
			order_number: transaction.order_id,
			code,
			note,
			metadata: {
				notification,
				transaction,
			},
		}
	}

	private async checkStatus(order_number: string) {
		return this.snapper.transaction.status(order_number)
	}

	private async approveTransaction(order_number: string) {
		return this.snapper.transaction.approve(order_number)
	}

	// private async denyTransaction(order_number: string) {
	// 	return this.snapper.transaction.deny(order_number)
	// }

	// private async cancelTransaction(order_number: string) {
	// 	return this.snapper.transaction.cancel(order_number)
	// }

	// private async expireTransaction(order_number: string) {
	// 	return this.snapper.transaction.expire(order_number)
	// }

	// private async refundTransaction(order_number: string, amount: number, refund_id?: string, reason?: string) {
	// 	return this.snapper.transaction.refund(order_number, {
	// 		amount,
	// 		refund_key: refund_id,
	// 		reason,
	// 	})
	// }

}

export default new MidtransHandler()
