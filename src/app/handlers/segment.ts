import HandlerModel from 'app/models/handler'
import Analytics from 'analytics-node'

import DEFAULTS from 'utils/constants/default'


class SegmentHandler extends HandlerModel<{
	write_key: string,
}> {
	protected static __displayName = 'Google Handler'

	private segment: Analytics = null

	constructor() {
		super({
			write_key: process.env.SEGMENT_KEY,
		})

		if (!DEFAULTS.DEBUG) {
			this.segment = new Analytics(this.get('write_key'))
		}
	}

	async track(val: {
		user_id: number,
		event: string,
		properties?: object,
	}) {
		if (!DEFAULTS.DEBUG) {
			try {
				this.segment.track({
					userId: val.user_id,
					event: val.event,
					properties: val.properties,
				})
			} catch (err) {
				this.warn(err)
			}
		}
	}
}

export default new SegmentHandler()
