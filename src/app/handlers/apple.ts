import HandlerModel from 'app/models/handler'
import crypto from 'crypto'
import appleSigninAuth from 'apple-signin-auth'

// import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'


class AppleHandler extends HandlerModel<{
}> {
	protected static __displayName = 'Apple Handler'

	constructor() {
		super({}, [])
	}

	async authenticate(accessToken: string, nonce?: string) {
		return appleSigninAuth.verifyIdToken(accessToken, {
			/** sha256 hex hash of raw nonce */
			nonce: nonce ? crypto.createHash('sha256').update(nonce).digest('hex') : undefined,
		})
	}
}

export default new AppleHandler()
