import HandlerModel from 'app/models/handler'
import encryption from 'js-sha512'

import {
	NINJA_EVENT_STATUS,
	NotificationNinjaParameterInterface,
} from 'app/interfaces/ninja'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'

import { SHIPMENT_STATUSES } from 'energie/utils/constants/enum'


class NinjaHandler extends HandlerModel<{
	client_key: string,
}> {
	protected static __displayName = 'Ninja Handler'

	constructor() {
		super({
			client_key: process.env.NINJA_CLIENT_KEY,
		})
	}

	async handleNotification(ninjaSignature: string, notification: NotificationNinjaParameterInterface) {
		this.log(`Transaction notification received. Tracking ID: ${notification.tracking_id}. Transaction status: ${notification.status}`)
		// Verification digital signature
		if (new Buffer(encryption.sha512(process.env.NINJA_CLIENT_KEY)).toString('base64') !== ninjaSignature) {
			throw new ErrorModel(ERRORS.NOT_VERIFIED, ERROR_CODES.ERR_103, 'Signature key invalid')
		}

		return await this.processNotification(notification)
	}

	async processNotification(notification: NotificationNinjaParameterInterface): Promise<{
		order_number: string,
		code: SHIPMENT_STATUSES,
		note: string,
		metadata: {
			notification: NotificationNinjaParameterInterface,
		},
	}> {
		let code: SHIPMENT_STATUSES
		let note: string

		switch(notification.status) {
			case NINJA_EVENT_STATUS['Successful Delivery']:
			// Should have POD
			 code  = SHIPMENT_STATUSES.DELIVERY_CONFIRMED
			 note  = `Matchbox has successfully delivered and received by ${notification.pod.name} at ${notification.timestamp}`
			break
			case NINJA_EVENT_STATUS['Van en-route to pickup']
			|| NINJA_EVENT_STATUS['En-route to Sorting Hub']
			|| NINJA_EVENT_STATUS['Arrived at Origin Hub']
			|| NINJA_EVENT_STATUS['Arrived at Sorting Hub']
			|| NINJA_EVENT_STATUS['On Vehicle for Delivery']:
			 code = SHIPMENT_STATUSES.ON_COURIER
			 note = 'Order on courier'
			break
			case NINJA_EVENT_STATUS.Completed:
			 code = SHIPMENT_STATUSES.DELIVERED
			 note = 'Order has been delivered'
			break
		}

		return {
			order_number: notification.order_id,
			code,
			note,
			metadata: {
				notification,
			},
		}
	}
}

export default new NinjaHandler()
