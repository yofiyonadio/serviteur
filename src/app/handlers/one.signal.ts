import HandlerModel from 'app/models/handler'
import OneSignalService from 'app/services/one.signal'


class OneSignalHandler extends HandlerModel<{
	list_id: string,
	token: string,
}> {
	protected static __displayName = 'One Signal Handler'

	constructor() {
		super({
			list_id: process.env.MAILCHIMP_SUBSCRIBE_LIST_ID,
			token: process.env.MAILCHIMP_TOKEN,
		})
	}

	notify(...params: Parameters<typeof OneSignalService['notify']>) {
		return OneSignalService.notify(...params)
	}
}

export default new OneSignalHandler()
