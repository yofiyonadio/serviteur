import HandlerModel from 'app/models/handler'
import MailchimpService from 'app/services/mailchimp'


class MailchimpHandler extends HandlerModel<{
	list_id: string,
	token: string,
}> {
	protected static __displayName = 'Mailchimp Handler'

	constructor() {
		super({
			list_id: process.env.MAILCHIMP_SUBSCRIBE_LIST_ID,
			token: process.env.MAILCHIMP_TOKEN,
		})
	}

	subscribe(email: string, source = 'subscription_box') {
		return MailchimpService.subscribe(email, [source], {
			list_id: this.get('list_id'),
			token: this.get('token'),
		}).then(() => {
			return true
		}).catch(err => {
			this.warn(err)

			return false
		})
	}
}

export default new MailchimpHandler()
