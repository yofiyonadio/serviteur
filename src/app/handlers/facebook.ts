import HandlerModel from 'app/models/handler'
import { Facebook } from 'fb'


class FacebookHandler extends HandlerModel<{
	appId: string,
	appSecret: string,
	version: string,
}> {
	protected static __displayName = 'Facebook Handler'

	private fb: {
		api: (...args: any[]) => any,
	}

	constructor() {
		super({
			appId: process.env.FB_APP_ID,
			appSecret: process.env.FB_APP_SECRET,
			version: 'v3.0',
		})

		this.fb = new Facebook({
			appId: this.get('appId'),
			appSecret: this.get('appSecret'),
			version: this.get('version'),
		})
	}

	authenticate(accessToken: string) {
		return new Promise((res, rej) => {
			this.fb.api('me', { fields: ['id', 'name', 'email', 'picture.width(450).height(450)'], access_token: accessToken }, (response: any) => {
				if (!response || response.error) {
					rej(response && response.error)
				} else {
					res(response)
				}
			})
		})
	}
}

export default new FacebookHandler()
