import HandlerModel from 'app/models/handler'
import twilio from 'twilio'

class TwilioHandler extends HandlerModel<{
	account_sid: string,
	auth_token: string,
	service_id: string,
}> {
	protected static __displayName = 'Twilio Handler'

	constructor() {
		super({
			account_sid: process.env.ACCOUNT_SID,
			auth_token: process.env.AUTH_TOKEN,
			service_id: process.env.SERVICE_ID,
		})
	}

	async sendSMSToken(
		phone: string,
	) {
		return twilio(this.get('account_sid'), this.get('auth_token'))
			.verify
			.services(this.get('service_id'))
			.verifications
			.create({
				to: phone,
				channel: 'sms',
			})
	}

	async verifySMSToken(
		phone: string,
		code: string,
	) {
		return twilio(this.get('account_sid'), this.get('auth_token'))
			.verify
			.services(this.get('service_id'))
			.verificationChecks
			.create({
				to: phone,
				code,
			})
	}
}

export default new TwilioHandler()
