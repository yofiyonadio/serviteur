import HandlerModel from 'app/models/handler'
import SendGrid from '@sendgrid/mail'
import SendGridClient from '@sendgrid/client'

export enum SENDGRID_TEMPLATES {
	WELCOME = 'd-4eceb7c35a3e4899a6c69c6881edbade', // done
	VERIFY = 'd-b23087a8d328454284479709fd976c85',  // done
	GIFT_CARD = 'd-62d3800afa4141109e761e6834cb38e6', // done
	RECOMMENDATION = 'd-f9bcde0e5f7446a6aca207fc1a5559af', // done
	REFUND = 'd-786380fed8a642b8900feda660addb1b', // done
	PAYMENT_CONFIRMED = 'd-614d8e71d9464bee8273e7d1ffc5a452', // done
	PAYMENT_CONFIRMED_PARTNERSHIP = 'd-6da52f8664d44bf48e030d7a9463ccb0',
	ORDER_CONFIRMED = 'd-bb2ae751a304429da63923a4a5c952e0', // done
	ORDER_CANCELLATION = 'd-bad6c7e2dc304339b3fb33738ec29f10', // done
	ORDER_DELIVERED = 'd-517eb491bf7d40a081a40a43fd911d7a', // done
	ORDER_DISPATCHED = 'd-5fb8140d537445028918a3dff01a2b71', // done
	ORDER_EXPIRED = 'd-80365a7badd84dd986aa6266c4548905', // done
	ORDER_EXPIRING = 'd-f4657b4169d74b6a927c3ee261775ffc', // done
	ORDER_POSTPONED = 'd-021466c772e247c78827cb7ed10121e2', // done
	NOTIFICATION = 'd-eeef88827a8947d98c893fd77a73db0b', // done
	FORGOT_PASSWORD = 'd-537cd0b2c0a44bf3bd92793299f0c916', // done
	SPQ_REMINDER = 'd-477a7de712f64403a7fa689e8a40fb9e', // done
	PREVIEW = 'd-19e3d8b8934e4a089af8e1598fc25e52', // done
	HANDOVER_STYLIST = 'd-0620b9de67d2409babac1fb276dd0cd1', // done
	BAG_ABANDONMENT = 'd-2375f8dec9ac4d0e849c0a9ee8eb054c',
}

class SendGridHandler extends HandlerModel<{
	key: string,
}> {
	protected static __displayName = 'Send Grid Handler'

	constructor() {
		super({
			key: process.env.SENDGRID_KEY,
		})

		SendGrid.setApiKey(this.get('key'))
		SendGridClient.setApiKey(this.get('key'))
	}

	async send(
		template_name: SENDGRID_TEMPLATES,
		to: {
			email: string | string[],
		},
		{
			vars,
			...message
		}: {
			subject?: string,
			important?: boolean,
			tags?: string[],
			vars: {
				[k: string]: any,
			},
		},
		from: {
			email: string,
			name: string,
		} = {
				email: 'noreply@helloyuna.io',
				name: 'Yuna & Co.',
			},
	) {
		if (vars.group_id) {
			return SendGrid.send({
				templateId: template_name,
				to: to.email,
				from,
				dynamicTemplateData: vars,
				asm: {
					groupId: vars.group_id,
				}
			})
		} else {
			return SendGrid.send({
				templateId: template_name,
				to: to.email,
				from,
				dynamicTemplateData: vars,
			})
		}
	}

	async subscribe(email: string) {
		return SendGridClient.request({
			method: 'PUT',
			url: '/v3/marketing/contacts',
			body: {
				list_ids: ['2afb14c8-f3b7-4d2d-b10d-5b5197e1e64f'],
				contacts: [{
					email,
				}],
			},
		}).then(() => {
			return true
		}).catch(err => {
			this.warn(err.response.body.errors)

			return false
		})
	}

	async brandPartnership(
		first_name: string,
		last_name: string,
		brand: string,
		email: string,
		phone: string,
		website: string,
	) {
		return SendGridClient.request({
			method: 'PUT',
			url: '/v3/marketing/contacts',
			body: {
				list_ids: ['3c33dfaf-1204-46f7-a674-8377ac1e5470'],
				contacts: [{
					first_name,
					last_name,
					email,
					phone_number: phone,
					// custom fields gunain id dari masing" custom fields sebagai key -_-
					custom_fields: {
						w1_T: brand,	// id dari custom field brand
						w2_T: website, // id dari custon field website
					},
				}],
			},
		}).then(() => {
			return true
		}).catch(err => {
			this.warn(err.response.body.errors)

			return false
		})
	}

	async sendCustom(email: string, subject: string, text: string) {
		return SendGrid.send({
			personalizations: [{
				to: [{
					email,
				}],
				subject,
			}],
			from: {
				email: 'noreply@helloyuna.io',
				name: 'Yuna & Co.',
			},
			content: [{
				type: 'text/plain',
				value: text,
			}],
		})
	}
}

export default new SendGridHandler()
