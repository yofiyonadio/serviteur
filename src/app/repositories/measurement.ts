import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	MeasurementRecord,
} from 'energie/app/records'

import {
	MeasurementInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class MeasurementRepository extends RepositoryModel<{
	MeasurementRecord: MeasurementRecord,
}> {

	static __displayName = 'MeasurementRepository'

	protected records = {
		MeasurementRecord,
	}

	// ============================= INSERT =============================
@RepositoryModel.bound
	async insert(
		id: number,
		measurement_id: number,
		title: string,
		description: string,
		is_required: boolean,
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.saveOrUpdate('MeasurementRecord', [{
			id,
			measurement_id,
			title,
			description,
			is_required,
		}], transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getIds(transaction: EntityManager): Promise<Array<{
		id: number,
		measurements: MeasurementInterface[],
	}>> {
		return this.query('master.measurement', 'measurement', transaction)
			.select(['measurement.id'])
			.leftJoinAndSelect('measurement.measurements', 'measurements')
			.where('measurement.deleted_at IS NULL')
			.andWhere('measurements.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
