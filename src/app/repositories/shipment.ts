import {
	ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager, IsNull } from 'typeorm'

import {
	// OtherTikiZipcodeRecord,
	HistoryShipmentRecord,
	ShipmentAddressRecord,
	ShipmentOrderDetailRecord,
	ShipmentRecord,
	OrderDetailRecord,
	ShipmentCampaignRecord,
	ShipmentCouponRecord,
} from 'energie/app/records'

import {
	ShipmentAddressInterface,
	ShipmentInterface,
	// PacketInterface,
	OrderDetailInterface,
	LocationInterface,
	PacketInterface,
	ShipmentOrderDetailInterface,
	HistoryShipmentInterface,
	ExchangeInterface,
	ShipmentCampaignInterface,
	ShipmentCouponInterface,
	OrderInterface,
	// OtherTikiZipcodeInterface,
} from 'energie/app/interfaces'
import InterfaceAddressModel from 'energie/app/models/interface.address'

import CommonHelper from 'utils/helpers/common'
import TimeHelper from 'utils/helpers/time'
import { Parameter } from 'types/common'

import { ERROR_CODES } from 'app/models/error'
import { SHIPMENTS, SHIPMENT_STATUSES } from 'energie/utils/constants/enum'

@EntityRepository()
export default class ShipmentRepository extends RepositoryModel<{
	HistoryShipmentRecord: HistoryShipmentRecord,
	ShipmentRecord: ShipmentRecord,
	ShipmentAddressRecord: ShipmentAddressRecord,
	ShipmentCampaignRecord: ShipmentCampaignRecord,
	ShipmentCouponRecord: ShipmentCouponRecord,
	ShipmentOrderDetailRecord: ShipmentOrderDetailRecord,
}> {

	static __displayName = 'ShipmentRepository'

	protected records = {
		HistoryShipmentRecord,
		ShipmentRecord,
		ShipmentAddressRecord,
		ShipmentCampaignRecord,
		ShipmentCouponRecord,
		ShipmentOrderDetailRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		type: SHIPMENTS,
		courier: string,
		service: string,
		amount: number,
		packet_id: number,
		shipment_addresses: [Parameter<ShipmentAddressInterface>, Parameter<ShipmentAddressInterface>],
		order_details: OrderDetailInterface[] | undefined,
		status: SHIPMENT_STATUSES,
		awb: string | undefined,
		url: string | undefined,
		note: string | undefined,
		prices: object | undefined,
		is_facade: boolean | undefined,
		is_return: boolean | undefined,
		created_at: Date = null,
		metadata: object | undefined,
		transaction: EntityManager,
	): Promise<ShipmentInterface & {
		origin: ShipmentAddressInterface,
		destination: ShipmentAddressInterface,
		orderDetails?: OrderDetailInterface[],
	}> {
		const origin = await this.insertAddress(shipment_addresses[0], transaction)
		const destination = await this.insertAddress(shipment_addresses[1], transaction)

		const shipment = await this.save('ShipmentRecord', {
			type,
			courier,
			service,
			amount,
			packet_id,
			origin_id: origin.id,
			destination_id: destination.id,
			status,
			awb,
			url,
			note,
			prices,
			is_facade,
			is_return,
			created_at,
			metadata,
		}, transaction)

		if (!!order_details) {
			await this.save('ShipmentOrderDetailRecord', order_details.map(orderDetail => {
				return {
					shipment_id: shipment.id,
					order_detail_id: orderDetail.id,
				}
			}), transaction)

			return {
				...shipment,
				origin,
				destination,
				orderDetails: order_details,
			}
		} else {
			return {
				...shipment,
				origin,
				destination,
			}
		}
	}

	@RepositoryModel.bound
	async insertAddress(
		address: Partial<ShipmentAddressInterface>,
		transaction: EntityManager,
	): Promise<ShipmentAddressInterface> {
		return this.getOne('ShipmentAddressRecord', {
			location_id: address.location_id || IsNull(),
			title: CommonHelper.default(address.title, ''),
			receiver: address.receiver || IsNull(),
			phone: address.phone || IsNull(),
			address: address.address || IsNull(),
			district: address.district || IsNull(),
			postal: address.postal || IsNull(),
		}, {
			transaction,
		}).catch(async err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.save('ShipmentAddressRecord', {
					location_id: address.location_id,
					title: address.title || '',
					receiver: address.receiver,
					phone: address.phone,
					address: address.address,
					district: address.district,
					postal: address.postal,
					metadata: address.metadata,
				}, transaction)
			}

			throw err
		})
	}

	insertCampaign(campaign: Parameter<ShipmentCampaignInterface>, transaction: EntityManager): Promise<ShipmentCampaignInterface>
	insertCampaign(campaigns: Array<Parameter<ShipmentCampaignInterface>>, transaction: EntityManager): Promise<ShipmentCampaignInterface[]>

	@RepositoryModel.bound
	async insertCampaign(
		campaign: Parameter<ShipmentCampaignInterface> | Array<Parameter<ShipmentCampaignInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('ShipmentCampaignRecord', campaign as any, transaction)
	}

	insertCoupon(coupon: Parameter<ShipmentCouponInterface>, transaction: EntityManager): Promise<ShipmentCouponInterface>
	insertCoupon(coupons: Array<Parameter<ShipmentCouponInterface>>, transaction: EntityManager): Promise<ShipmentCouponInterface[]>

	@RepositoryModel.bound
	async insertCoupon(
		coupon: Parameter<ShipmentCouponInterface> | Array<Parameter<ShipmentCouponInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('ShipmentCouponRecord', coupon as any, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		shipment_id: number,
		update: Partial<Parameter<ShipmentInterface, 'packet_id'>>,
		updated_at: Date | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('ShipmentRecord', shipment_id, CommonHelper.stripKey(update as ShipmentInterface, 'packet_id'), {
			transaction,
		}).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryShipmentRecord', {
					changer_user_id,
					shipment_id,
					status: update.status,
					note: update.note,
					courier: update.courier,
					service: update.service,
					awb: update.awb,
					url: update.url,
					amount: update.amount,
					prices: update.prices,
					cancelled_at: update.cancelled_at,
					created_at: updated_at,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updateAddress(
		shipment_address_id: number,
		data: Partial<Parameter<InterfaceAddressModel>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('ShipmentAddressRecord', shipment_address_id, data, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updatePacketId(shipment_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<ShipmentInterface>('app.shipment', transaction)
			.set({ packet_id })
			.where(Array.isArray(shipment_id_or_ids) ? 'id IN (:...shipment_id_or_ids)' : 'id = :shipment_id_or_ids', { shipment_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 12,
		filter: {
			type?: SHIPMENTS,
			status?: SHIPMENT_STATUSES,
			sort?: 'ASC' | 'DESC',
			search?: string,
			with_order?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<ShipmentInterface & {
			destination: ShipmentAddressInterface,
			packet: PacketInterface,
			orderDetails?: Array<OrderDetailInterface & {
				order: OrderInterface,
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.shipment', 'shipment', transaction)
			.leftJoinAndSelect('shipment.destination', 'destination')
			.leftJoinAndSelect('shipment.packet', 'packet')
			.where('TRUE')

		if(filter.with_order) {
			Q = Q.leftJoin('shipment.shipmentOrderDetails', 'shipmentOrderDetails')
			.leftJoinAndMapMany('shipment.orderDetails', OrderDetailRecord, 'orderDetails', 'orderDetails.id = shipmentOrderDetails.order_detail_id')
			.leftJoinAndSelect('orderDetails.order', 'order')
		}

		if(filter.search) {
			if(filter.with_order) {
				Q = Q.andWhere('order.number ILIKE :search', {search: `%${filter.search}%`})
			} else {
				Q = Q.leftJoin('shipment.shipmentOrderDetails', 'shipmentOrderDetails')
				.leftJoin('shipmentOrderDetails.orderDetail', 'orderDetail')
				.leftJoin('orderDetail.order', 'order')
				.andWhere('order.number ILIKE :search', {search: `%${filter.search}%`})
			}
		}

		if(filter.type) {
			Q = Q.andWhere('shipment.type = :type', {type: filter.type})
		}

		if(filter.status) {
			Q = Q.andWhere('shipment.status = :status', {status: filter.status})
		}

		if(filter.sort) {
			Q = Q.orderBy('shipment.id', filter.sort)
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount) as any
	}

@RepositoryModel.bound
	async getAddressDeep(address_id: number, transaction: EntityManager): Promise<ShipmentAddressInterface & {
		location: LocationInterface,
	}> {
		return this.query('app.shipment_address', 'address', transaction)
			.leftJoinAndSelect('address.location', 'location')
			.where('address.id = :address_id', { address_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByIds(
		shipment_ids: number[],
		transaction: EntityManager,
	): Promise<Array<ShipmentInterface & {
		destination: ShipmentAddressInterface,
		packet: PacketInterface,
		orderDetails: OrderDetailInterface[],
	}>> {
		return this.query('app.shipment', 'shipment', transaction)
			.leftJoinAndSelect('shipment.destination', 'destination')
			.leftJoinAndSelect('shipment.packet', 'packet')
			.leftJoin('shipment.shipmentOrderDetails', 'shipmentOrderDetails')
			.leftJoinAndMapMany('shipment.orderDetails', OrderDetailRecord, 'orderDetails', 'orderDetails.id = shipmentOrderDetails.order_detail_id')
			.where('shipment.id IN (:...ids)', { ids: shipment_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBelong(
		user_id: number,
		shipment_id: number,
		transaction: EntityManager,
	): Promise<ShipmentInterface> {
		return this.query('app.shipment', 'shipment', transaction)
			.innerJoin('shipment.shipmentOrderDetails', 'sO')
			.innerJoin('sO.orderDetail', 'oD')
			.innerJoin('oD.order', 'order', 'order.user_id = :user_id', { user_id })
			.where('shipment.id = :shipment_id', { shipment_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByOrderDetailId(
		order_detail_id: number,
		is_facade: boolean | undefined,
		is_return: boolean | undefined,
		transaction: EntityManager,
	): Promise<ShipmentInterface & {
		origin: ShipmentAddressInterface,
		destination: ShipmentAddressInterface,
		packet: PacketInterface,
		count: number,
	}> {
		let Q = this.query('app.shipment', 'shipment', transaction)
			.addSelect('shipmentOrderDetails.order_detail_id')
			.leftJoinAndSelect('shipment.origin', 'origin')
			.leftJoinAndSelect('shipment.destination', 'destination')
			.leftJoinAndSelect('shipment.packet', 'packet')
			.innerJoin('shipment.shipmentOrderDetails', 'shipmentOrderDetails', 'shipmentOrderDetails.order_detail_id = :order_detail_id', { order_detail_id })
			.orderBy('shipment.created_at', 'DESC')

		if (is_facade === true || is_facade === false) {
			Q = Q.where('shipment.is_facade = :is_facade', { is_facade })
		}

		if (is_return === true || is_return === false) {
			Q = Q.where('shipment.is_return = :is_return', { is_return })
		}

		return Q.getOne()
			.then(this.parser)
			.then((shipment: ShipmentInterface & { shipmentOrderDetails: ShipmentOrderDetailInterface[] }) => {
				return CommonHelper.stripUndefined({
					...shipment,
					shipmentOrderDetails: undefined,
					count: shipment.shipmentOrderDetails.length,
				})
			}) as any
	}

	@RepositoryModel.bound
	async getCountInCurrentMinute(
		transaction: EntityManager,
		startMinute: Date = TimeHelper.moment().startOf('m').toDate(),
		endMinute: Date = TimeHelper.moment().startOf('m').toDate(),
	): Promise<number> {
		return this.query('app.shipment', 'shipment', transaction)
			.select('COUNT(DISTINCT(id))')
			.where(`shipment.created_at BETWEEN '${startMinute.toISOString()}' AND '${endMinute.toISOString()}'`)
			.getCount()
			.then(this.parser)
	}

	@RepositoryModel.bound
	async getActive(
		offset: number = 0,
		limit: number = 32,
		transaction: EntityManager,
	): Promise<{
		data: ShipmentInterface[],
		count: number,
	}> {
		return this.query('app.shipment', 'shipment', transaction)
			.where('shipment.status IN (:...status)', { status: [SHIPMENT_STATUSES.PENDING, SHIPMENT_STATUSES.PROCESSING, SHIPMENT_STATUSES.ON_COURIER] })
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			})
			.then(this.parser) as any
	}

@RepositoryModel.bound
	async getOrderDetails(
		shipment_ids: number[],
		include_exchange: boolean,
		include_replacing: boolean,
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		replacing?: ExchangeInterface,
		exchange?: ExchangeInterface,
	}>> {
		let Q = this.query('app.order_detail', 'detail', transaction)
			.innerJoin('detail.orderDetailShipments', 'oDS', 'oDS.shipment_id IN (:...shipment_ids)', { shipment_ids })

		if(include_exchange) {
			Q = Q.leftJoinAndSelect('detail.exchange', 'exchange')
		}

		if(include_replacing) {
			Q = Q.leftJoinAndSelect('detail.replacing', 'replacing')
		}

		return Q
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDelivered(
		date: Date,
		transaction: EntityManager,
	): Promise<Array<ShipmentInterface & {
		history: HistoryShipmentInterface,
	}>> {
		const Q = this.query('app.shipment', 'shipment', transaction)
			.innerJoinAndMapOne('shipment.history', HistoryShipmentRecord, 'history', 'history.shipment_id = shipment.id AND history.status = :status AND history.created_at < :date', { status: SHIPMENT_STATUSES.DELIVERED, date })
			.where('shipment.status = :status', { status: SHIPMENT_STATUSES.DELIVERED })
			.orWhere('shipment.status = :sts and shipment.confirmed_at is null', { sts: SHIPMENT_STATUSES.DELIVERY_CONFIRMED })

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(shipment_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('ShipmentRecord', {
			id: shipment_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	@RepositoryModel.bound
	async getBy(
		shipment: Parameter<Partial<ShipmentInterface>>,
		transaction: EntityManager,
	): Promise<ShipmentInterface> {
		return this.getOne('ShipmentRecord', {
			...shipment,
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getShipmentDeliveredStatus(
		transaction: EntityManager,
	): Promise<Array<{
		id: number,
	}>> {
		return this.query('app.shipment', 'shipment', transaction)
			.select('shipment.id')
			.where('shipment.status = :status', { status: SHIPMENT_STATUSES.DELIVERED })
			.andWhere(`shipment.updated_at < now() - INTERVAL '48 hours'`)
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
