import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'


@EntityRepository()
export default class InstagramCatalogRepository extends RepositoryModel {

	static __displayName = 'InstagramCatalogRepository'


	// ============================= INSERT =============================
	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	@RepositoryModel.bound
	async getData(variant_id: number, transaction: EntityManager) {
		return await this.queryCustom(`
		SELECT "catalog".id, "catalog".title, "catalog".brand,
		regexp_replace("catalog".description, E'[\\n\\r]+', ' ', 'g' ) "description",
		"catalog".availability, "catalog".condition, "catalog".category, "catalog".price, "catalog".sale_price, "catalog".link, "catalog".image_link, "catalog".additional_image_link, "catalog".item_group_id, "catalog".size, "catalog".color, "catalog".visibility
		FROM view.instagram_catalog "catalog"
		WHERE "catalog".id = ${variant_id}
		LIMIT 1
		`, undefined, transaction).then(this.parser)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
