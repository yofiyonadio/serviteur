import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'
// import { RawQuery as rawQ } from 'utils/helpers'

import {
	StylecardRecord,
	StylecardCollectionRecord,
	StylecardVariantRecord,
	StylecardUserAssetRecord,
	HistoryStylecardRecord,
	UserAssetRecord,
	VariantAssetRecord,
	BrandRecord,
	CategoryRecord,
	ColorRecord,
	SizeRecord,
	ProductRecord,
	VariantColorRecord,
	StyleboardRecord,
	OrderDetailRecord,
	CollectionRecord,
	VariantRecord,
	InventoryRecord,
	StylecardAnswerRecord,
	// VariantMeasurementRecord,
	// MeasurementRecord,
} from 'energie/app/records'

import {
	UserProfileInterface,
	MatchboxInterface,
	SizeInterface,
	ColorInterface,
	ProductInterface,
	BrandInterface,
	VariantAssetInterface,
	CategoryInterface,
	UserInterface,
	UserAssetInterface,
	VariantInterface,
	StylecardInterface,
	StylecardVariantInterface,
	HistoryStylecardInterface,
	StyleboardInterface,
	OrderDetailInterface,
	OrderInterface,
	CollectionInterface,
	InventoryInterface,
	StylecardAnswerInterface,
	VariantMeasurementInterface,
	MeasurementInterface,
} from 'energie/app/interfaces'

import { STYLECARD_STATUSES, ORDER_DETAILS } from 'energie/utils/constants/enum'

import { Parameter } from 'types/common'

import { CommonHelper, TimeHelper } from 'utils/helpers'
import ErrorModel, { ERROR_CODES } from 'app/models/error'

// import { ErrorModel } from 'app/models'
// import { ERRORS, ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class StylecardRepository extends RepositoryModel<{
	HistoryStylecardRecord: HistoryStylecardRecord,
	StylecardRecord: StylecardRecord,
	StylecardCollectionRecord: StylecardCollectionRecord,
	StylecardVariantRecord: StylecardVariantRecord,
	StylecardUserAssetRecord: StylecardUserAssetRecord,
	StylecardAnswerRecord: StylecardAnswerRecord,
}> {

	static __displayName = 'StylecardRepository'

	protected records = {
		HistoryStylecardRecord,
		StylecardRecord,
		StylecardCollectionRecord,
		StylecardVariantRecord,
		StylecardUserAssetRecord,
		StylecardAnswerRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<StylecardInterface>,
		user_asset_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<StylecardInterface> {
		return this.save('StylecardRecord', data, transaction).then(async stylecard => {
			if (user_asset_ids && user_asset_ids.length) {
				await this.save('StylecardUserAssetRecord', user_asset_ids.map(userAssetId => {
					return {
						stylecard_id: stylecard.id,
						user_asset_id: userAssetId,
					}
				}), transaction)
			}

			return stylecard
		})
	}

	@RepositoryModel.bound
	async setVariant(data: Parameter<StylecardVariantInterface, 'deleted_at'>, transaction: EntityManager): Promise<StylecardVariantInterface> {
		return this.save('StylecardVariantRecord', data, transaction)
	}

	@RepositoryModel.bound
	async setCollection(stylecard_id: number, collection_ids: number[], transaction: EntityManager) {
		return this.save('StylecardCollectionRecord', collection_ids.map(collection_id => {
			return {
				stylecard_id,
				collection_id,
			}
		}), transaction)
	}

	@RepositoryModel.bound
	async addAnswer(stylecard_id: number, question_id: number, user_id: number, answer: object, transaction: EntityManager) {
		return this.getLatestAnswer(stylecard_id, question_id, user_id, transaction).then(stylecardAnswer => {
			if (+stylecardAnswer.updated_at + (1000 * 60 * 60) > Date.now()) {
				// modify
				return this.renew('StylecardAnswerRecord', stylecardAnswer.id, {
					answer,
				})
			} else {
				// add
				return this.save('StylecardAnswerRecord', {
					stylecard_id,
					question_id,
					user_id,
					answer,
				}, transaction).then(() => {
					return true
				})
			}
		}).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.save('StylecardAnswerRecord', {
					stylecard_id,
					question_id,
					user_id,
					answer,
				}, transaction).then(() => {
					return true
				})
			}

			throw err
		})
	}

	@RepositoryModel.bound
	async addUser(stylecard_ids: number | number[], user_ids: number | number[], transaction: EntityManager) {
		const values = Array.isArray(stylecard_ids) ? stylecard_ids.reduce((i, stylecard_id) => {
			if (Array.isArray(user_ids)) {
				return i.concat(...user_ids.map(user_id => {
					return {
						stylecard_id,
						user_id,
					}
				}))
			} else {
				return i.concat({
					stylecard_id,
					user_id: user_ids,
				})
			}
		}, []) : Array.isArray(user_ids) ? user_ids.map(user_id => {
			return {
				stylecard_id: stylecard_ids,
				user_id,
			}
		}) : {
			stylecard_id: stylecard_ids,
			user_id: user_ids,
		}

		return this.queryInsert('app.user_stylecard', transaction)
			.values(values)
			.execute()
			.then(result => result.raw)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async publish(
		changer_user_id: number,
		stylecard_id: number,
		status: STYLECARD_STATUSES.PUBLISHED | undefined,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('StylecardRecord', stylecard_id, {
			status: status || STYLECARD_STATUSES.PUBLISHED,
			note,
		}, {
			transaction,
		}).then(isPublished => {
			if (isPublished) {
				this.save('HistoryStylecardRecord', {
					changer_user_id,
					stylecard_id,
					status: status || STYLECARD_STATUSES.PUBLISHED,
					note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async publishBatch(
		changer_user_id: number,
		stylecard_ids: number[],
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('StylecardRecord', 'id in (:...stylecard_ids)', {
			status: STYLECARD_STATUSES.PUBLISHED,
			note,
		}, {
			parameters: {
				stylecard_ids,
			},
			transaction,
		}).then(isPublished => {
			if (isPublished) {
				this.queryInsert('history.stylecard')
					.values(stylecard_ids.map(stylecard_id => {
						return {
							changer_user_id,
							stylecard_id,
							status: STYLECARD_STATUSES.PUBLISHED,
							note,
						}
					}))
					.execute()
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async startStyling(
		changer_user_id: number,
		stylecard_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return await this.renew('StylecardRecord', stylecard_id, {
			status: STYLECARD_STATUSES.STYLING,
		}, {
			transaction,
		}).then(isPublished => {
			if (isPublished) {
				this.save('HistoryStylecardRecord', {
					changer_user_id,
					stylecard_id,
					status: STYLECARD_STATUSES.STYLING,
					note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		stylecard_id: number,
		update: Partial<Parameter<StylecardInterface>>,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StylecardRecord', stylecard_id, CommonHelper.stripKey(update), {
			transaction,
		}).then(isPublished => {
			if (isPublished) {
				this.save('HistoryStylecardRecord', {
					changer_user_id,
					stylecard_id,
					status: update.status,
					stylist_id: update.stylist_id,
					note: note || update.note,
					lineup: update.lineup,
					stylist_note: update.stylist_note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async updateVariant(
		stylecard_variant_id: number,
		update: Partial<Parameter<StylecardVariantInterface>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StylecardVariantRecord', stylecard_variant_id, CommonHelper.stripUndefined(update), {
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter<WI extends boolean, WV extends boolean, WH extends boolean, WS extends boolean, WM extends boolean, WB extends boolean, WO extends boolean, WU extends boolean, WC extends boolean, WA extends boolean>(
		offset: number = 0,
		limit: number = 12,
		filter: {
			stylecard_ids?: number[]
			search?: string,
			status?: STYLECARD_STATUSES,
			date?: Date,
			user_id?: number,
			variant_id?: number,
			exclude_stylecard_id?: number,
			newest_first?: boolean,
			have_order?: boolean,
			with_variant?: WV,
			with_history?: WH,
			with_stylist?: WS,
			with_matchbox?: WM,
			with_styleboard?: WB,
			with_order?: WO,
			with_user?: WU,
			with_collection?: WC,
			with_variant_shallow?: WA,
			filter_collection?: string,
			have_collection?: boolean,
			is_bundle?: boolean,
			is_public?: boolean,
			is_master?: boolean,
			is_recomendation?: boolean,
			with_inventory?: WI,
			inventory_only?: boolean,
			variant_available_only?: boolean,
			random_sort?: boolean,
		},
		transaction: EntityManager,
	): Promise<{
		data: Array<StylecardInterface & (WS extends true ? {
			stylist: UserInterface & {
				profile: UserProfileInterface,
			},
		} : {}) & (WH extends true ? {
			history?: HistoryStylecardInterface,
		} : {}) & (WV extends true ? {
			stylecardVariants: Array<StylecardVariantInterface & {
				variant: VariantInterface & (WI extends true ? {
					inventories?: InventoryInterface[],
				} : {}),
				product: ProductInterface,
				brand: BrandInterface,
				category: CategoryInterface,
				colors: ColorInterface[],
				size: SizeInterface,
				asset?: VariantAssetInterface,
			}>,
		} : WA extends true ? {
			stylecardVariants: Array<StylecardVariantInterface & {
				variant: VariantInterface & (WI extends true ? {
					inventories: InventoryInterface[],
				} : {}),
			}>,
		} : {}) & (WM extends true ? {
			matchbox: MatchboxInterface & {
				brand: BrandInterface,
			},
		} : {}) & (WB extends true ? {
			styleboards: Array<StyleboardInterface & (WO extends true ? {
				orderDetail?: OrderDetailInterface & {
					order: OrderInterface,
				},
			} : {}) & (WU extends true ? {
				user?: UserInterface & {
					profile: UserProfileInterface,
				},
			} : {})>,
		} : {}) & (WC extends true ? {
			collections?: CollectionInterface[],
		} : {})>,
		count: number,
	}> {
		let Q = this.query('app.stylecard', 'stylecard', transaction)
			.where('stylecard.deleted_at IS NULL')

		if (filter.stylecard_ids && filter.stylecard_ids.length > 0) {
			Q = Q.andWhere('stylecard.id in (:...stylecard_ids)', { stylecard_ids: filter.stylecard_ids})
		}

		if (filter.random_sort) {
			Q = Q.orderBy('random()')
		} else {
			Q = Q.orderBy('stylecard.created_at', 'DESC')
		}

		if (filter.with_collection) {
			// joined
			Q = Q.leftJoin('stylecard.stylecardCollections', 'sc')
				// joined
				.leftJoinAndMapMany('stylecard.collections', CollectionRecord, 'collections', 'collections.id = sc.collection_id')

			if (filter.filter_collection) {
				Q = Q.andWhere('collections.slug = :slug', { slug: filter.filter_collection })
			}

			if (filter.have_collection) {
				Q = Q.andWhere('sc.stylecard_id IS NOT NULL')
			}
		}

		if (filter.with_variant) {
			// joined
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
				// joined
				.leftJoinAndSelect('sv.variant', 'variant')
				// joined
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				// joined
				.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
				// joined
				.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
				// joined
				.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
				// joined
				.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				// joined
				.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
				// joined
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'variant_asset')
						.orderBy('variant_asset.index', 'ASC')
						.addOrderBy('variant_asset.order', 'ASC')
				}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')
				// joined
				.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')

			if (filter.variant_id) {
				// joined
				Q = Q.innerJoin(sq => {
					return sq.subQuery()
						.select('svr.stylecard_id')
						.from(StylecardVariantRecord, 'svr')
						.where('svr.variant_id = :variant_id', { variant_id: filter.variant_id })
				}, 'filterVariant', `"filterVariant"."svr_stylecard_id" = stylecard.id`)
			}

			if (filter.variant_available_only) {
				Q = Q.andWhere('variant.is_available is true')
			}

			if (filter.with_inventory) {
				// joined
				Q = Q.leftJoinAndMapMany('variant.inventories', InventoryRecord, 'inventory', `inventory.variant_id = variant.id and inventory.status = 'AVAILABLE'`)
			}
		}

		if (filter.with_matchbox) {
			// joined
			Q = Q.leftJoinAndSelect('stylecard.matchbox', 'matchbox')
				.leftJoinAndSelect('matchbox.brand', 'mbrand')
		}

		if (filter.with_stylist) {
			// joined
			Q = Q.leftJoinAndSelect('stylecard.stylist', 'stylist')
				// joined
				.leftJoinAndSelect('stylist.profile', 'stylist_profile')
		}

		if (filter.with_history) {
			// joined
			Q = Q.leftJoin(query => {
				return query.from(HistoryStylecardRecord, 'history_stylecard')
					.orderBy('history_stylecard.id', 'DESC')
					.where('history_stylecard.note IS NOT NULL')
			}, 'hs', 'hs.stylecard_id = stylecard.id')
				// joined
				.leftJoinAndMapOne('stylecard.history', HistoryStylecardRecord, 'history', 'history.id = hs.id')
		}



		if (filter.with_styleboard) {
			// joined
			Q = Q.leftJoin('stylecard.stylecardStyleboards', 'scb')
				.leftJoinAndMapMany('stylecard.styleboards', StyleboardRecord, 'styleboard', 'styleboard.id = scb.styleboard_id')

			if (filter.with_order) {
				// joined
				Q = Q.leftJoinAndMapOne('styleboard.orderDetail', OrderDetailRecord, 'od', 'od.ref_id = styleboard.id AND od.type = :styleboard_type', { styleboard_type: ORDER_DETAILS.STYLEBOARD })
					.leftJoinAndSelect('od.order', 'ord')
			}

			if (filter.with_user) {
				// joined
				Q = Q.leftJoinAndSelect('styleboard.user', 'user')
					.leftJoinAndSelect('user.profile', 'profile')
			}
		}


		// -----------------
		if (filter.with_variant_shallow && !filter.with_variant) {
			// joined
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
				// joined
				.leftJoinAndSelect('sv.variant', 'variant')

			if (filter.with_inventory) {
				// joined
				Q = Q.leftJoinAndMapMany('variant.inventories', InventoryRecord, 'inventory', `inventory.variant_id = variant.id and inventory.status = 'AVAILABLE'`)
			}
		}

		if (filter.search) {
			if (filter.with_variant) {
				// whered
				Q = Q.andWhere('(brand.title ILIKE :search OR product.title ILIKE :search OR (stylecard.id)::text LIKE :search OR stylecard.title ILIKE :search)', { search: `%${filter.search}%` })
			} else {
				// whered
				Q = Q.andWhere('(stylist_profile.first_name || \' \' || stylist_profile.last_name ILIKE :search)', { search: `%${filter.search}%` })
			}
		}

		// non used
		if (filter.have_order) {
			if (!filter.with_styleboard) {
				// error
				Q = Q.innerJoin('stylecard.styleboard', 'styleboard')
			}
			if (filter.with_order) {
				// whered
				Q = Q.andWhere('od.id IS NOT NULL')
			} else {
				// joined
				Q = Q.innerJoin(OrderDetailRecord, 'od', 'od.ref_id = styleboard.id AND od.type = :styleboard_type', { styleboard_type: ORDER_DETAILS.STYLEBOARD })
			}
		}

		if (filter.status) {
			Q = Q.andWhere('stylecard.status = :status', { status: filter.status })
		}

		if (filter.user_id) {
			Q = Q.andWhere('stylist.id = :user_id', { user_id: filter.user_id })
		}

		if (filter.is_bundle) {
			// whered
			Q = Q.andWhere('stylecard.is_bundle IS TRUE')
				.andWhere('stylecard.expired_at > now()')
		}

		if (filter.is_public) {
			Q = Q.andWhere('stylecard.is_public IS TRUE')
				.andWhere(`stylecard.status in ('PUBLISHED')`)
		} else if (filter.is_public === false) {
			Q = Q.andWhere('stylecard.is_public IS FALSE')
		}

		if (filter.is_master) {
			Q = Q.andWhere('stylecard.is_master IS TRUE')
		} else if (filter.is_master === false) {
			Q = Q.andWhere('stylecard.is_master IS FALSE')
		}

		if (filter.is_recomendation) {
			this.log(TimeHelper.moment().subtract(7, 'd').toDate(), TimeHelper.moment().format())
			Q = Q.andWhere(`stylecard.is_recomendation IS TRUE AND stylecard.status in ('PUBLISHED')`)
				.andWhere('stylecard.updated_at BETWEEN :start AND :end', {
					start: TimeHelper.moment().subtract(10, 'd').toDate(),
					end: TimeHelper.moment().format(),
				})
		} else if (filter.is_recomendation === false) {
			Q = Q.andWhere('stylecard.is_recomendation IS FALSE')
		}

		if (filter.date) {
			Q = Q.andWhere('od.shipment_at BETWEEN :start AND :end', {
				start: TimeHelper.moment(filter.date).startOf('d').toDate(),
				end: TimeHelper.moment(filter.date).endOf('d').toDate(),
			})
		}

		if (filter.inventory_only) {
			Q = Q.andWhere('stylecard.inventory_only is true')
		}

		if (filter.exclude_stylecard_id) {
			Q = Q.andWhere('stylecard.id != :exclude_stylecard_id', { exclude_stylecard_id: filter.exclude_stylecard_id })
		}

		/*
		this.queryCustom(`
		SELECT DISTINCT ON (a.stylecard_id) a.*, b.* FROM mview.mvw_stylecards "a"
		LEFT JOIN mview.mvw_variants "b" ON a.stylecard_variant_id = b.variant_id
		WHERE a."stylecard_deleted_at" IS NULL
		AND a."sc_collection_stylecard_id" IS NOT NULL
		AND b."variant_is_available" is true
		AND a."stylecard_is_bundle" IS TRUE
		AND a."stylecard_expired_at" > now()
		AND a."stylecard_is_public" IS TRUE
		AND a."stylecard_status" in ('PUBLISHED', 'RESOLVED')
		`).then(resss => {
			//this.log(resss[0])
		})
		*/

		// const rawQuery = `
		// SELECT 
		// ${rawQ.select({ record: StylecardRecord, from: 'a', agg: 'DISTINCT', prefix_column: true })},
		// ${rawQ.mapper({
		// 	collection: [{
		// 		record: CollectionRecord,
		// 		from: 'a',
		// 	}],
		// 	stylecard_variant: [{
		// 		record: StylecardVariantRecord,
		// 		from: 'a',
		// 		child: {
		// 			variant: {
		// 				record: VariantRecord,
		// 				from: 'b'
		// 			}
		// 		}
		// 	}]
		// })}
		// FROM mview.mvw_stylecards "a"
		// LEFT JOIN mview.mvw_variants "b" ON b.variant_id = a.stylecard_variant_id
		// WHERE a.stylecard_collection_stylecard_id IS NOT NULL
		// GROUP BY a.stylecard_id
		// LIMIT 1
		// `

		// this.queryCustom(rawQuery).then(res => {
		// 	this.log(rawQuery)
		// 	this.log(res[0])
		// })

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount) as any

	}

	@RepositoryModel.bound
	async filterPrestyled(
		offset: number = 0,
		limit: number = 10,
		filter: {
			ids?: number[],
			with_collection?: boolean,
		},
		transaction: EntityManager,
	) {
		let Q = this.query('app.stylecard', 'stylecard', transaction)
			.where('stylecard.deleted_at IS NULL')
			.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
			.leftJoinAndSelect('sv.variant', 'variant')
			.andWhere('stylecard.is_bundle IS TRUE')
			.andWhere('stylecard.expired_at > now()')
			.andWhere('stylecard.is_public IS TRUE')
			.andWhere(`stylecard.status in ('PUBLISHED')`)
			// .andWhere('variant.is_available is true')
			.orderBy('stylecard.created_at', 'DESC')

		if (filter.ids && filter.ids.length > 0) {
			Q = Q.andWhere('stylecard.id in (:...stylecard_ids)', { stylecard_ids: filter.ids})
		}

		if (filter.with_collection) {
			Q = Q.leftJoin('stylecard.stylecardCollections', 'sc')
				.leftJoinAndMapMany('stylecard.collections', CollectionRecord, 'collections', 'collections.id = sc.collection_id')
				.andWhere('sc.stylecard_id IS NOT NULL')
		}
		
		return Q.skip(offset)
				.take(limit)
				.getManyAndCount()
		.then(datas => {
			return {
				data: datas[0],
				count: datas[1],
			}
		}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async filterByVariantId(
		variant_id: number,
		filter: {
			master_only?: boolean,
		},
		transaction: EntityManager,
	) {
		let Q = this.query('app.stylecard', 'stylecard', transaction)
			.where('stylecard.deleted_at IS NULL')
			.andWhere('stylecard.image IS NOT NULL')
			.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
			.leftJoinAndSelect('sv.variant', 'variant')
			.andWhere('variant.id = :variant_id', { variant_id })

		if (filter.master_only) {
			Q = Q.andWhere('stylecard.is_master IS TRUE')
		}

		return Q
			.take(40)
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getVariants(
		stylecard_id: number,
		transaction: EntityManager,
	) {
		const Q = this.query('app.stylecard_variant', 'sv', transaction)
			.where('sv.deleted_at IS NULL')
			.andWhere('sv.stylecard_id = :stylecard_id', { stylecard_id })
		return Q
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async get<D extends boolean, V extends boolean, H extends boolean, DV extends boolean, WSA extends boolean, WA extends boolean, WS extends boolean, WU extends boolean, WO extends boolean, WC extends boolean, N extends boolean, Return extends StylecardInterface & (D extends true ? {
		matchbox: MatchboxInterface,
		stylist: UserInterface & {
			profile: UserProfileInterface & (WSA extends true ? {
				asset?: UserAssetInterface,
			} : {}),
		},
		assets: UserAssetInterface[],
	} : {}) & (V extends true ? {
		stylecardVariants: Array<StylecardVariantInterface & (DV extends true ? {
			variant: VariantInterface,
			product: ProductInterface & {
				variants: Array<VariantInterface & {
					colors: ColorInterface[],
					size: SizeInterface,
					asset: VariantAssetInterface,
					variantMeasurements: Array<VariantMeasurementInterface & {
						measurement: MeasurementInterface,
					}>,
				}>,
			},
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset?: VariantAssetInterface,
		} : {})>,
	} : {}) & (H extends true ? {
		histories: HistoryStylecardInterface[],
	} : {}) & (WS extends true ? {
		styleboards: Array<StyleboardInterface & (WU extends true ? {
			user: UserInterface & {
				profile: UserProfileInterface & {
					asset?: UserAssetInterface,
				},
			},
		} : {}) & (WO extends true ? {
			orderDetails: Array<OrderDetailInterface & {
				order: OrderInterface,
			}>,
		} : {})>,
	} : {}) & (WC extends true ? {
		collections?: CollectionInterface[],
	} : {})>({
		stylecard_id_or_ids,
		stylist_id_or_ids,
		slug,
		many,
	}: {
		stylecard_id_or_ids?: number | number[],
		stylist_id_or_ids?: number | number[],
		slug?: string,
		many: N,
	}, {
		deep,
		with_history,
		with_variants,
		with_stylist_asset,
		with_assets,
		with_styleboard,
		with_collection,
		with_user,
		with_order,
		deep_variants,
		is_bundle,
		is_public,
		is_master,
		status,
	}: {
		deep?: D,
		with_variants?: V,
		with_history?: H,
		with_stylist_asset?: WSA,
		with_assets?: WA,
		with_styleboard?: WS,
		with_collection?: WC,
		with_user?: WU,
		with_order?: WO,
		deep_variants?: DV,
		is_bundle?: boolean,
		is_public?: boolean,
		is_master?: boolean,
		status?: STYLECARD_STATUSES,
	}, transaction: EntityManager): Promise<N extends true ? Return[] : Return> {
		let Q = this.query('app.stylecard', 'stylecard', transaction)

		if (deep) {
			Q = Q.leftJoinAndSelect('stylecard.matchbox', 'matchbox')
				.leftJoinAndSelect('stylecard.stylist', 'stylist')
				.leftJoinAndSelect('stylist.profile', 'stylist_profile')
				.leftJoin('stylecard.stylecardUserAssets', 'sua')
				.leftJoinAndMapMany('stylecard.assets', UserAssetRecord, 'assets', 'assets.id = sua.user_asset_id')

			if (with_stylist_asset) {
				Q = Q.leftJoinAndSelect('stylist_profile.asset', 'stylistProfileAsset')
			}
		}

		if (with_history) {
			Q = Q.leftJoinAndMapMany('stylecard.histories', HistoryStylecardRecord, 'histories', 'histories.stylecard_id = stylecard.id')
		}

		if (with_styleboard) {
			Q = Q.leftJoin('stylecard.stylecardStyleboards', 'scsb')
				.leftJoinAndMapMany('stylecard.styleboards', StyleboardRecord, 'styleboard', 'styleboard.id = scsb.styleboard_id')

			if (with_user) {
				Q = Q.leftJoinAndSelect('styleboard.user', 'user')
					.leftJoinAndSelect('user.profile', 'user_profile')
					.leftJoinAndSelect('user_profile.asset', 'userProfileAsset')
			}

			if (with_order) {
				Q = Q.leftJoin(OrderDetailRecord, 'od', 'od.ref_id = styleboard.id AND od.type = :od_styleboard', { od_styleboard: ORDER_DETAILS.STYLEBOARD })
					.leftJoinAndMapMany('styleboard.orderDetails', OrderDetailRecord, 'orderDetail', 'orderDetail.id = od.id')
					.leftJoinAndSelect('orderDetail.order', 'order')
			}
		}

		if (with_collection) {
			Q = Q.leftJoin('stylecard.stylecardCollections', 'sc')
				.leftJoinAndMapMany('stylecard.collections', CollectionRecord, 'collections', 'collections.id = sc.collection_id')
		}

		if (with_variants) {
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
				.addOrderBy('sv.id', 'ASC')

			if (deep_variants) {
				Q = Q.leftJoinAndSelect('sv.variant', 'variant')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'variant_color')
							.orderBy('variant_color.order', 'ASC')
					}, 'vc', 'vc.variant_id = variant.id')
					.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
					.leftJoinAndMapMany('product.variants', VariantRecord, 'variants', 'variants.product_id = product.id')
					.where('variants.deleted_at IS NULL')
					.leftJoinAndSelect('variants.variantMeasurements', 'variantMeasurements')
					.leftJoinAndSelect('variantMeasurements.measurement', 'measurement')
					.leftJoin(query => {
						return query.from(VariantColorRecord, '_variant_color')
							.orderBy('_variant_color.order', 'ASC')
					}, '_vc', '_vc.variant_id = variants.id')
					.leftJoinAndMapMany('variants.colors', ColorRecord, 'vcolor', 'vcolor.id = _vc.color_id')
					.leftJoinAndMapOne('variants.size', SizeRecord, 'vsize', 'vsize.id = variants.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, '_variant_asset')
							.orderBy('_variant_asset.index', 'ASC')
							.addOrderBy('_variant_asset.order', 'ASC')
					}, '_va', '_va.variant_id = variants.id AND _va.deleted_at IS NULL')
					.leftJoinAndMapOne('variants.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
					.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
					.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
					.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
					.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'variant_asset')
							.orderBy('variant_asset.index', 'ASC')
							.addOrderBy('variant_asset.order', 'ASC')
					}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')

				if (with_assets) {
					Q = Q.leftJoinAndMapMany('sv.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
				} else {
					Q = Q.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')
				}
			}
		}

		if (stylecard_id_or_ids) {
			if (Array.isArray(stylecard_id_or_ids)) {
				Q = Q.where(`stylecard.id IN (:...stylecard_id_or_ids)`, { stylecard_id_or_ids })
			} else {
				Q = Q.where(`stylecard.id = :stylecard_id_or_ids`, { stylecard_id_or_ids })
			}
		} else if (stylist_id_or_ids) {
			if (Array.isArray(stylist_id_or_ids)) {
				Q = Q.where(`stylecard.stylist_id IN (:...stylist_id_or_ids)`, { stylist_id_or_ids })
			} else {
				Q = Q.where(`stylecard.stylist_id = :stylist_id_or_ids`, { stylist_id_or_ids })
			}
		} else if (slug) {
			Q = Q.where(`stylecard.slug = :slug`, { slug })
		}

		if (is_bundle) {
			Q = Q.andWhere('stylecard.is_bundle IS TRUE')
		} else if (is_bundle === false) {
			Q = Q.andWhere('stylecard.is_bundle IS FALSE')
		}

		if (is_public) {
			Q = Q.andWhere('stylecard.is_public IS TRUE')
		} else if (is_public === false) {
			Q = Q.andWhere('stylecard.is_public IS FALSE')
		}

		if (is_master) {
			Q = Q.andWhere('stylecard.is_master IS TRUE')
		} else if (is_master === false) {
			Q = Q.andWhere('stylecard.is_master IS FALSE')
		}

		if (status) {
			Q = Q.andWhere('stylecard.status = :status', { status })
		}

		if (many) {
			return Q.getMany().then(this.parser) as any
		} else {
			return Q.getOne().then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getStylecard<D extends boolean, V extends boolean, H extends boolean, DV extends boolean, WSA extends boolean, WA extends boolean, WS extends boolean, WU extends boolean, WO extends boolean, WC extends boolean, N extends boolean, Return extends StylecardInterface & (D extends true ? {
		matchbox: MatchboxInterface,
		stylist: UserInterface & {
			profile: UserProfileInterface & (WSA extends true ? {
				asset?: UserAssetInterface,
			} : {}),
		},
		assets: UserAssetInterface[],
	} : {}) & (V extends true ? {
		stylecardVariants: Array<StylecardVariantInterface & (DV extends true ? {
			variant: VariantInterface,
			product: ProductInterface & {
				variants: Array<VariantInterface & {
					colors: ColorInterface[],
					size: SizeInterface,
					asset: VariantAssetInterface,
					variantMeasurements: Array<VariantMeasurementInterface & {
						measurement: MeasurementInterface,
					}>,
				}>,
			},
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset?: VariantAssetInterface,
		} : {})>,
	} : {}) & (H extends true ? {
		histories: HistoryStylecardInterface[],
	} : {}) & (WS extends true ? {
		styleboards: Array<StyleboardInterface & (WU extends true ? {
			user: UserInterface & {
				profile: UserProfileInterface & {
					asset?: UserAssetInterface,
				},
			},
		} : {}) & (WO extends true ? {
			orderDetails: Array<OrderDetailInterface & {
				order: OrderInterface,
			}>,
		} : {})>,
	} : {}) & (WC extends true ? {
		collections?: CollectionInterface[],
	} : {})>({
		stylecard_id_or_ids,
		stylist_id_or_ids,
		slug,
		many,
	}: {
		stylecard_id_or_ids?: number | number[],
		stylist_id_or_ids?: number | number[],
		slug?: string,
		many: N,
	}, {
		deep,
		with_history,
		with_variants,
		with_stylist_asset,
		with_assets,
		with_styleboard,
		with_collection,
		with_user,
		with_order,
		deep_variants,
		is_bundle,
		is_public,
		is_master,
		status,
	}: {
		deep?: D,
		with_variants?: V,
		with_history?: H,
		with_stylist_asset?: WSA,
		with_assets?: WA,
		with_styleboard?: WS,
		with_collection?: WC,
		with_user?: WU,
		with_order?: WO,
		deep_variants?: DV,
		is_bundle?: boolean,
		is_public?: boolean,
		is_master?: boolean,
		status?: STYLECARD_STATUSES,
	}, transaction: EntityManager): Promise<N extends true ? Return[] : Return> {
		let Q = this.query('app.stylecard', 'stylecard', transaction)

		if (deep) {
			Q = Q.leftJoinAndSelect('stylecard.matchbox', 'matchbox')
				.leftJoinAndSelect('stylecard.stylist', 'stylist')
				.leftJoinAndSelect('stylist.profile', 'stylist_profile')
				.leftJoin('stylecard.stylecardUserAssets', 'sua')
				.leftJoinAndMapMany('stylecard.assets', UserAssetRecord, 'assets', 'assets.id = sua.user_asset_id')

			if (with_stylist_asset) {
				Q = Q.leftJoinAndSelect('stylist_profile.asset', 'stylistProfileAsset')
			}
		}

		if (with_history) {
			Q = Q.leftJoinAndMapMany('stylecard.histories', HistoryStylecardRecord, 'histories', 'histories.stylecard_id = stylecard.id')
		}

		if (with_styleboard) {
			Q = Q.leftJoin('stylecard.stylecardStyleboards', 'scsb')
				.leftJoinAndMapMany('stylecard.styleboards', StyleboardRecord, 'styleboard', 'styleboard.id = scsb.styleboard_id')

			if (with_user) {
				Q = Q.leftJoinAndSelect('styleboard.user', 'user')
					.leftJoinAndSelect('user.profile', 'user_profile')
					.leftJoinAndSelect('user_profile.asset', 'userProfileAsset')
			}

			if (with_order) {
				Q = Q.leftJoin(OrderDetailRecord, 'od', 'od.ref_id = styleboard.id AND od.type = :od_styleboard', { od_styleboard: ORDER_DETAILS.STYLEBOARD })
					.leftJoinAndMapMany('styleboard.orderDetails', OrderDetailRecord, 'orderDetail', 'orderDetail.id = od.id')
					.leftJoinAndSelect('orderDetail.order', 'order')
			}
		}

		if (with_collection) {
			Q = Q.leftJoin('stylecard.stylecardCollections', 'sc')
				.leftJoinAndMapMany('stylecard.collections', CollectionRecord, 'collections', 'collections.id = sc.collection_id')
		}

		if (with_variants) {
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
				.addOrderBy('sv.id', 'ASC')

			if (deep_variants) {
				Q = Q.leftJoinAndSelect('sv.variant', 'variant')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'variant_color')
							.orderBy('variant_color.order', 'ASC')
					}, 'vc', 'vc.variant_id = variant.id')
					.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
					.leftJoinAndMapMany('product.variants', VariantRecord, 'variants', 'variants.product_id = product.id')
					.where('variants.deleted_at IS NULL')
					.leftJoinAndSelect('variants.variantMeasurements', 'variantMeasurements')
					.leftJoinAndSelect('variantMeasurements.measurement', 'measurement')
					.leftJoin(query => {
						return query.from(VariantColorRecord, '_variant_color')
							.orderBy('_variant_color.order', 'ASC')
					}, '_vc', '_vc.variant_id = variants.id')
					.leftJoinAndMapMany('variants.colors', ColorRecord, 'vcolor', 'vcolor.id = _vc.color_id')
					.leftJoinAndMapOne('variants.size', SizeRecord, 'vsize', 'vsize.id = variants.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, '_variant_asset')
							.orderBy('_variant_asset.index', 'ASC')
							.addOrderBy('_variant_asset.order', 'ASC')
					}, '_va', '_va.variant_id = variants.id AND _va.deleted_at IS NULL')
					.leftJoinAndMapOne('variants.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
					.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
					.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
					.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
					.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'variant_asset')
							.orderBy('variant_asset.index', 'ASC')
							.addOrderBy('variant_asset.order', 'ASC')
					}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')

				if (with_assets) {
					Q = Q.leftJoinAndMapMany('sv.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
				} else {
					Q = Q.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')
				}
			}
		}

		if (stylecard_id_or_ids) {
			if (Array.isArray(stylecard_id_or_ids)) {
				Q = Q.where(`stylecard.id IN (:...stylecard_id_or_ids)`, { stylecard_id_or_ids })
			} else {
				Q = Q.where(`stylecard.id = :stylecard_id_or_ids`, { stylecard_id_or_ids })
			}
		} else if (stylist_id_or_ids) {
			if (Array.isArray(stylist_id_or_ids)) {
				Q = Q.where(`stylecard.stylist_id IN (:...stylist_id_or_ids)`, { stylist_id_or_ids })
			} else {
				Q = Q.where(`stylecard.stylist_id = :stylist_id_or_ids`, { stylist_id_or_ids })
			}
		} else if (slug) {
			Q = Q.where(`stylecard.slug = :slug`, { slug })
		}

		if (is_bundle) {
			Q = Q.andWhere('stylecard.is_bundle IS TRUE')
		} else if (is_bundle === false) {
			Q = Q.andWhere('stylecard.is_bundle IS FALSE')
		}

		if (is_public) {
			Q = Q.andWhere('stylecard.is_public IS TRUE')
		} else if (is_public === false) {
			Q = Q.andWhere('stylecard.is_public IS FALSE')
		}

		if (is_master) {
			Q = Q.andWhere('stylecard.is_master IS TRUE')
		} else if (is_master === false) {
			Q = Q.andWhere('stylecard.is_master IS FALSE')
		}

		if (status) {
			Q = Q.andWhere('stylecard.status = :status', { status })
		}

		if (many) {
			return Q.getMany().then(this.parser) as any
		} else {
			return Q.getOne().then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getPrestyled<D extends boolean, V extends boolean, H extends boolean, DV extends boolean, WSA extends boolean, WA extends boolean, WS extends boolean, WU extends boolean, WO extends boolean, WC extends boolean, N extends boolean, Return extends StylecardInterface & (D extends true ? {
		matchbox: MatchboxInterface,
		stylist: UserInterface & {
			profile: UserProfileInterface & (WSA extends true ? {
				asset?: UserAssetInterface,
			} : {}),
		},
		assets: UserAssetInterface[],
	} : {}) & (V extends true ? {
		stylecardVariants: Array<StylecardVariantInterface & (DV extends true ? {
			variant: VariantInterface,
			product: ProductInterface & {
				variants: Array<VariantInterface & {
					colors: ColorInterface[],
					size: SizeInterface,
					asset: VariantAssetInterface,
					variantMeasurements: Array<VariantMeasurementInterface & {
						measurement: MeasurementInterface,
					}>,
				}>,
			},
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset?: VariantAssetInterface,
		} : {})>,
	} : {}) & (H extends true ? {
		histories: HistoryStylecardInterface[],
	} : {}) & (WS extends true ? {
		styleboards: Array<StyleboardInterface & (WU extends true ? {
			user: UserInterface & {
				profile: UserProfileInterface & {
					asset?: UserAssetInterface,
				},
			},
		} : {}) & (WO extends true ? {
			orderDetails: Array<OrderDetailInterface & {
				order: OrderInterface,
			}>,
		} : {})>,
	} : {}) & (WC extends true ? {
		collections?: CollectionInterface[],
	} : {})>({
		stylecard_id_or_ids,
		stylist_id_or_ids,
		slug,
		many,
	}: {
		stylecard_id_or_ids?: number | number[],
		stylist_id_or_ids?: number | number[],
		slug?: string,
		many: N,
	}, {
		deep,
		with_history,
		with_variants,
		with_stylist_asset,
		with_assets,
		with_styleboard,
		with_collection,
		with_user,
		with_order,
		deep_variants,
		is_bundle,
		is_public,
		is_master,
		status,
	}: {
		deep?: D,
		with_variants?: V,
		with_history?: H,
		with_stylist_asset?: WSA,
		with_assets?: WA,
		with_styleboard?: WS,
		with_collection?: WC,
		with_user?: WU,
		with_order?: WO,
		deep_variants?: DV,
		is_bundle?: boolean,
		is_public?: boolean,
		is_master?: boolean,
		status?: STYLECARD_STATUSES,
	}, transaction: EntityManager): Promise<N extends true ? Return[] : Return> {
		let Q = this.query('app.stylecard', 'stylecard', transaction)

		if (deep) {
			Q = Q.leftJoinAndSelect('stylecard.matchbox', 'matchbox')
				.leftJoinAndSelect('stylecard.stylist', 'stylist')
				.leftJoinAndSelect('stylist.profile', 'stylist_profile')
				.leftJoin('stylecard.stylecardUserAssets', 'sua')
				.leftJoinAndMapMany('stylecard.assets', UserAssetRecord, 'assets', 'assets.id = sua.user_asset_id')

			if (with_stylist_asset) {
				Q = Q.leftJoinAndSelect('stylist_profile.asset', 'stylistProfileAsset')
			}
		}

		if (with_history) {
			Q = Q.leftJoinAndMapMany('stylecard.histories', HistoryStylecardRecord, 'histories', 'histories.stylecard_id = stylecard.id')
		}

		if (with_styleboard) {
			Q = Q.leftJoin('stylecard.stylecardStyleboards', 'scsb')
				.leftJoinAndMapMany('stylecard.styleboards', StyleboardRecord, 'styleboard', 'styleboard.id = scsb.styleboard_id')

			if (with_user) {
				Q = Q.leftJoinAndSelect('styleboard.user', 'user')
					.leftJoinAndSelect('user.profile', 'user_profile')
					.leftJoinAndSelect('user_profile.asset', 'userProfileAsset')
			}

			if (with_order) {
				Q = Q.leftJoin(OrderDetailRecord, 'od', 'od.ref_id = styleboard.id AND od.type = :od_styleboard', { od_styleboard: ORDER_DETAILS.STYLEBOARD })
					.leftJoinAndMapMany('styleboard.orderDetails', OrderDetailRecord, 'orderDetail', 'orderDetail.id = od.id')
					.leftJoinAndSelect('orderDetail.order', 'order')
			}
		}

		if (with_collection) {
			Q = Q.leftJoin('stylecard.stylecardCollections', 'sc')
				.leftJoinAndMapMany('stylecard.collections', CollectionRecord, 'collections', 'collections.id = sc.collection_id')
		}

		if (with_variants) {
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at IS NULL')
				.addOrderBy('sv.id', 'ASC')

			if (deep_variants) {
				Q = Q.leftJoinAndSelect('sv.variant', 'variant')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'variant_color')
							.orderBy('variant_color.order', 'ASC')
					}, 'vc', 'vc.variant_id = variant.id')
					.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
					.leftJoinAndMapMany('product.variants', VariantRecord, 'variants', 'variants.product_id = product.id')
					.andWhere('variants.deleted_at IS NULL')
					.leftJoinAndSelect('variants.variantMeasurements', 'variantMeasurements')
					.leftJoinAndSelect('variantMeasurements.measurement', 'measurement')
					.leftJoin(query => {
						return query.from(VariantColorRecord, '_variant_color')
							.orderBy('_variant_color.order', 'ASC')
					}, '_vc', '_vc.variant_id = variants.id')
					.leftJoinAndMapMany('variants.colors', ColorRecord, 'vcolor', 'vcolor.id = _vc.color_id')
					.leftJoinAndMapOne('variants.size', SizeRecord, 'vsize', 'vsize.id = variants.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, '_variant_asset')
							.orderBy('_variant_asset.index', 'ASC')
							.addOrderBy('_variant_asset.order', 'ASC')
					}, '_va', '_va.variant_id = variants.id AND _va.deleted_at IS NULL')
					.leftJoinAndMapOne('variants.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
					.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
					.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
					.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
					.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'variant_asset')
							.orderBy('variant_asset.index', 'ASC')
							.addOrderBy('variant_asset.order', 'ASC')
					}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')

				if (with_assets) {
					Q = Q.leftJoinAndMapMany('sv.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
				} else {
					Q = Q.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')
				}
			}
		}

		if (stylecard_id_or_ids) {
			if (Array.isArray(stylecard_id_or_ids)) {
				Q = Q.where(`stylecard.id IN (:...stylecard_id_or_ids)`, { stylecard_id_or_ids })
			} else {
				Q = Q.where(`stylecard.id = :stylecard_id_or_ids`, { stylecard_id_or_ids })
			}
		} else if (stylist_id_or_ids) {
			if (Array.isArray(stylist_id_or_ids)) {
				Q = Q.where(`stylecard.stylist_id IN (:...stylist_id_or_ids)`, { stylist_id_or_ids })
			} else {
				Q = Q.where(`stylecard.stylist_id = :stylist_id_or_ids`, { stylist_id_or_ids })
			}
		} else if (slug) {
			Q = Q.where(`stylecard.slug = :slug`, { slug })
		}

		if (is_bundle) {
			Q = Q.andWhere('stylecard.is_bundle IS TRUE')
		} else if (is_bundle === false) {
			Q = Q.andWhere('stylecard.is_bundle IS FALSE')
		}

		if (is_public) {
			Q = Q.andWhere('stylecard.is_public IS TRUE')
		} else if (is_public === false) {
			Q = Q.andWhere('stylecard.is_public IS FALSE')
		}

		if (is_master) {
			Q = Q.andWhere('stylecard.is_master IS TRUE')
		} else if (is_master === false) {
			Q = Q.andWhere('stylecard.is_master IS FALSE')
		}

		if (status) {
			Q = Q.andWhere('stylecard.status = :status', { status })
		}

		if (many) {
			return Q.getMany().then(this.parser) as any
		} else {
			return Q.getOne().then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getNotes(
		stylecard_ids: number[],
		transaction: EntityManager,
	): Promise<Array<StylecardInterface & {
		stylist: UserInterface & {
			profile: UserProfileInterface & {
				signature: UserAssetInterface,
			},
		},
	}>> {
		return this.query('app.stylecard', 'stylecard', transaction)
			.leftJoinAndSelect('stylecard.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndSelect('stylistProfile.signature', 'signature')
			.where('stylecard.id IN (:...stylecard_ids)', { stylecard_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylecardVariants<D extends boolean, WA extends boolean, WS extends boolean>(
		stylecard_variant_ids: number[],
		{
			deep,
			with_assets,
			with_stylecard,
		}: {
			deep?: D,
			with_assets?: WA,
			with_stylecard?: WS,
		},
		transaction: EntityManager,
	): Promise<Array<StylecardVariantInterface & (D extends true ? {
		variant: VariantInterface,
		product: ProductInterface,
		brand: BrandInterface,
		category: CategoryInterface,
		colors: ColorInterface[],
		size: SizeInterface,
	} : {}) & (WA extends true ? {
		assets: VariantAssetInterface[],
	} : {
		asset?: VariantAssetInterface,
	}) & (WS extends true ? {
		stylecard: StylecardInterface,
	} : {})>> {
		let Q = this.query('app.stylecard_variant', 'sv', transaction)
			.where('sv.id IN (:...stylecard_variant_ids)', { stylecard_variant_ids })

		if (deep) {
			Q = Q.leftJoinAndSelect('sv.variant', 'variant')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
				.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
				.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
				.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'variant_asset')
						.orderBy('variant_asset.index', 'ASC')
						.addOrderBy('variant_asset.order', 'ASC')
				}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')

			if (with_assets) {
				Q = Q.leftJoinAndMapMany('sv.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
			} else {
				Q = Q.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')
			}
		}

		if (with_stylecard) {
			Q = Q.leftJoinAndSelect('sv.stylecard', 'stylecard')
		}

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBundleDiscount(
		stylecard_id: number,
		transaction: EntityManager,
	): Promise<{
		stylecard_id: number,
		variants: Array<{
			id: number,
			product_id: number,
			discount: number,
		}>,
	}> {
		return this.query('app.stylecard_variant', 'stylecard_variant', transaction)
			.innerJoin('stylecard_variant.variant', 'variant')
			.innerJoin('variant.product', 'product')
			.innerJoinAndMapMany('product.variants', VariantRecord, 'variants', 'variants.product_id = product.id')
			.select('stylecard_variant.stylecard_id', 'stylecard_id')
			.addSelect(`array_agg(json_build_object(
				'id', variants.id,
				'product_id', variants.product_id,
				'discount', stylecard_variant.bundle_discount
			))`, 'variants')
			.where('stylecard_variant.stylecard_id = :stylecard_id', { stylecard_id })
			.andWhere('stylecard_variant.deleted_at is null')
			.groupBy('stylecard_variant.stylecard_id')
			.getRawOne()
	}

	@RepositoryModel.bound
	async getCollection(transaction: EntityManager): Promise<CollectionInterface[]> {
		return this.query('master.collection', 'collection', transaction)
			.where('collection.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylecardsFromVariant(variant_id: number, transaction: EntityManager): Promise<StylecardInterface[]> {
		return this.query('app.stylecard', 'stylecard', transaction)
			.innerJoin('stylecard.stylecardVariants', 'sv')
			.innerJoin('sv.variant', 'variant')
			.innerJoin('variant.product', 'product')
			.innerJoin('product.variants', 'variants', 'variants.id = :variant_id', { variant_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLatestAnswer(stylecard_id: number, question_id: number, user_id: number | undefined, transaction: EntityManager): Promise<StylecardAnswerInterface> {
		const Q = this.query('app.stylecard_answer', 'answer', transaction)
			.where('answer.stylecard_id = :stylecard_id and answer.question_id = :question_id', { stylecard_id, question_id })
			.orderBy('answer.created_at', 'DESC')

		if (user_id) {
			return Q.andWhere('answer.user_id = :user_id', { user_id })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getStylecardRecomendationAnswer(
		offset: number,
		limit: number,
		transaction: EntityManager,
	): Promise<Array<{
		stylecard_id: number,
		cluster_type: string,
		cluster_no: number,
		like: number,
		dislike: number,
	}>> {
		return transaction.createQueryBuilder()
			.select([
				'stylecard.id',
				'cluster.type as cluster_type',
				'cluster.type_no as cluster_no',
				'coalesce(sc_like.count, 0) as like',
				'coalesce(sc_dislike.count, 0) as dislike',
			])
			.from('app.stylecard', 'stylecard')
			.leftJoin('cluster', 'cluster', 'cluster.id = stylecard.cluster_id')
			.leftJoin(sq => {
				return sq.subQuery().select([
					'sc_a.stylecard_id as stylecard_id',
					'count(sc_a.user_id) as count',
				])
					.from('app.stylecard_answer', 'sc_a')
					.where(`(sc_a.answer ->> 'BOOLEAN')::boolean is true`)
					.groupBy('sc_a.stylecard_id')
			}, 'sc_like', 'sc_like.stylecard_id = stylecard.id')
			.leftJoin(sq => {
				return sq.subQuery().select([
					'sc_a.stylecard_id as stylecard_id',
					'count(sc_a.user_id) as count',
				])
					.from('app.stylecard_answer', 'sc_a')
					.where(`(sc_a.answer ->> 'BOOLEAN')::boolean is false`)
					.groupBy('sc_a.stylecard_id')
			}, 'sc_dislike', 'sc_dislike.stylecard_id = stylecard.id')
			.where(sq => {
				return `stylecard.id in (` + sq.subQuery()
					.select('sca.stylecard_id', 'stylecard_id')
					.from('app.stylecard_answer', 'sca')
					.groupBy('sca.stylecard_id')
					.getSql() + ')'
			})
			.offset(offset)
			.limit(limit)
			.orderBy('stylecard.id', 'ASC')
			.getRawMany()
	}

	@RepositoryModel.bound
	async getCountViewStylecard(
		stylecard_id: number,
		transaction: EntityManager,
	): Promise<number> {
		return this.query('app.user_stylecard', 'userStylecard', transaction)
			.where('userStylecard.stylecard_id = :stylecard_id', { stylecard_id })
			.andWhere('userStylecard.seen_at is not null')
			.getCount()
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async removeVariant(
		stylecard_variant_id: number,
		transaction: EntityManager,
	) {
		// TODO: cancel purchase request, on manager?
		return await this.renew('StylecardVariantRecord', stylecard_variant_id, {
			deleted_at: new Date(),
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async detachCollection(
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.queryDelete('app.stylecard_collection', transaction)
			.where('stylecard_id = :stylecard_id', { stylecard_id })
			.execute()
	}

	// ============================ PRIVATES ============================

}
