import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	TagRecord,
} from 'energie/app/records'

import {
	TagInterface,
} from 'energie/app/interfaces'

// import {
// 	TagInterface,
// } from 'energie/app/interfaces'


@EntityRepository()
export default class TagRepository extends RepositoryModel<{
	TagRecord: TagRecord,
}> {

	static __displayName = 'TagRepository'

	protected records = {
		TagRecord,
	}

	// ============================= INSERT =============================
@RepositoryModel.bound
	async insert(
		id: number,
		tag_id: number,
		title: string,
		description: string,
		transaction: EntityManager,
	) {
		return this.saveOrUpdate('TagRecord', [{
			id,
			tag_id,
			title,
			description,
		}], transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getIds(transaction: EntityManager): Promise<Array<{
		id: number,
		tags: TagInterface[],
	}>> {
		// cannot change, nested where
		return this.query('master.tag', 'tag', transaction)
			.select(['tag.id'])
			.leftJoinAndSelect('tag.tags', 'tags')
			.where('tag.deleted_at IS NULL')
			.andWhere('tags.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
