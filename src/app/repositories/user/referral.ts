import RepositoryModel from 'app/models/repository'
import { EntityManager, EntityRepository } from 'typeorm'

import { UserInterface, UserProfileInterface, UserReferralInterface } from 'energie/app/interfaces'

import {
	UserRecord,
	UserAddressRecord,
	UserAssetRecord,
	UserCouponRecord,
	UserNoteRecord,
	UserProfileRecord,
	UserReferralRecord,
	UserRoleRecord,
	UserSocialRecord,
	UserTokenRecord,
} from 'energie/app/records'

import { Parameter } from 'types/common'

import { REFERRAL_POINT_STATUSES } from 'energie/utils/constants/enum'

@EntityRepository()
export default class UserReferralRepository extends RepositoryModel<{
  UserRecord: UserRecord,
	UserAddressRecord: UserAddressRecord,
	UserAssetRecord: UserAssetRecord,
	UserCouponRecord: UserCouponRecord,
	UserNoteRecord: UserNoteRecord,
	UserProfileRecord: UserProfileRecord,
	UserReferralRecord: UserReferralRecord,
	UserRoleRecord: UserRoleRecord,
	UserSocialRecord: UserSocialRecord,
	UserTokenRecord: UserTokenRecord,
}> {

  static __displayName = 'UserReferralRepository'

  protected records = {
		UserRecord,
		UserAddressRecord,
		UserAssetRecord,
		UserCouponRecord,
		UserNoteRecord,
		UserProfileRecord,
		UserReferralRecord,
		UserRoleRecord,
		UserSocialRecord,
		UserTokenRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Partial<Parameter<UserReferralInterface>>,
		transaction: EntityManager,
	): Promise<UserReferralInterface> {
		return this.save('UserReferralRecord', data, transaction)
	}

	// ============================= UPDATE =============================

	@RepositoryModel.bound
	async updateStatus(
		user_id: number,
		user_reference: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate('app.user_referral', transaction)
			.set({
				referral_point_status: REFERRAL_POINT_STATUSES.TRANSFERRED,
			})
			.where('user_id = :user_id AND user_reference = :user_reference', {user_id, user_reference})
			.execute()
			.then(_ => true)
			.catch(_ => false)
	}


	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getReferral(
		user_id: number,
		filter: {
			status: string,
		},
		transaction: EntityManager,
	): Promise<{
		data: Array<UserReferralInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			user_referral: UserInterface & {
				profile: UserProfileInterface,
			},
		}>
		count: number,
	}> {
		let Q = this.query('app.user_referral', 'referral', transaction)
			.leftJoinAndMapOne('referral.user', UserRecord, 'user', 'referral.user_id = user.id')
			// .leftJoinAndSelect(UserRecord, 'user', 'referral.user_id = user.id')
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'user.user_profile_id = profile.id')
			.leftJoinAndMapOne('referral.user_referral', UserRecord, 'user_referral', 'referral.user_reference = user_referral.id')
			// .leftJoinAndSelect(UserRecord, 'user_ref', 'referral.user_reference = user_ref.id')
			.leftJoinAndMapOne('user_referral.profile', UserProfileRecord, 'profile_ref', 'user_referral.user_profile_id = profile_ref.id')
			.where('referral.user_id = :user_id', { user_id })

		if (filter.status) {
			Q = Q.andWhere(`referral.referral_point_status in (':status') `, {status: filter.status})
		}

			return Q.orderBy('referral.created_at', 'DESC')
			.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async verifyReferral(
		referral_code: string,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'user.user_profile_id = profile.id')
			.leftJoinAndMapOne('profile.referral', UserReferralRecord, 'referral', 'user.id = referral.user_id')
			.where('LOWER(profile.referral_code) = LOWER(:referral_code)', { referral_code })
			.getOne()
			.then(this.parser)
			.then(_ => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async getDetailReferral(
		user_id: number,
		user_reference: number,
		transaction: EntityManager,
	): Promise<UserReferralInterface & {
		user: UserInterface,
		user_referral: UserInterface,
	}> {
		return this.query('app.user_referral', 'referral', transaction)
			.leftJoinAndMapOne('referral.user', UserRecord, 'user', 'referral.user_id = user.id')
			.leftJoinAndMapOne('referral.user_referral', UserRecord, 'user_referral', 'referral.user_reference = user_referral.id')
			.where('referral.user_id = :user_id', { user_id })
			.andWhere('referral.user_reference = :user_reference', { user_reference })
			.orderBy('referral.created_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByUserRef(
		user_reference: number,
		transaction: EntityManager,
	): Promise<UserReferralInterface & {
		user: UserInterface,
		user_referral: UserInterface,
	}> {
		return this.query('app.user_referral', 'referral', transaction)
			.leftJoinAndMapOne('referral.user', UserRecord, 'user', 'referral.user_id = user.id')
			.leftJoinAndMapOne('referral.user_referral', UserRecord, 'user_referral', 'referral.user_reference = user_referral.id')
			.where('referral.user_reference = :user_reference', { user_reference })
			.andWhere(`referral.referral_point_status not in ('TRANSFERRED') `)
			.orderBy('referral.created_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}


}
