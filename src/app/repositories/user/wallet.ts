import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	// ExchangeRecord,
	OrderRecord,
	StylesheetInventoryRecord,
	UserRecord,
	UserBankRecord,
	UserRequestRecord,
	UserWalletRecord,
	UserWalletTransactionRecord,
	HistoryUserWalletTransactionRecord,
	ExchangeRecord,
} from 'energie/app/records'

import {
	UserWalletInterface,
	UserWalletTransactionInterface,
	UserBankInterface,
	UserRequestInterface,
	UserInterface,
	UserProfileInterface,
	StylesheetInventoryInterface,
	OrderInterface,
	ExchangeInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import { USER_REQUESTS, CODES, STATEMENT_SOURCES, WALLET_STATUSES } from 'energie/utils/constants/enum'


@EntityRepository()
export default class UserWalletRepository extends RepositoryModel<{
	UserBankRecord: UserBankRecord,
	UserRequestRecord: UserRequestRecord,
	UserWalletRecord: UserWalletRecord,
	UserWalletTransactionRecord: UserWalletTransactionRecord,
	HistoryUserWalletTransactionRecord: HistoryUserWalletTransactionRecord,
}> {

	static __displayName = 'UserWalletRepository'

	protected records = {
		UserBankRecord,
		UserRequestRecord,
		UserWalletRecord,
		UserWalletTransactionRecord,
		HistoryUserWalletTransactionRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async createBank(
		user_id: number,
		data: Parameter<UserBankInterface, 'user_id'>,
		transaction: EntityManager,
	): Promise<UserBankInterface> {
		return this.save('UserBankRecord', {
			user_id,
			...data,
		}, transaction)
	}

	@RepositoryModel.bound
	async createWallet(
		user_id: number,
		data: Parameter<UserWalletInterface, 'user_id'>,
		transaction: EntityManager,
	): Promise<UserWalletInterface> {
		return this.save('UserWalletRecord', {
			user_id,
			...data,
		}, transaction)
	}

	@RepositoryModel.bound
	async createWalletTransaction(
		user_wallet_id: number,
		data: Parameter<UserWalletTransactionInterface, 'user_wallet_id'>,
		transaction: EntityManager,
	): Promise<UserWalletTransactionInterface> {
		return this.save('UserWalletTransactionRecord', {
			user_wallet_id,
			...data,
		}, transaction)
	}

	@RepositoryModel.bound
	async createWithdrawRequest(
		user_id: number,
		amount: number,
		transaction: EntityManager,
	): Promise<UserRequestInterface> {
		return this.save('UserRequestRecord', {
			user_id,
			type: USER_REQUESTS.WITHDRAW,
			status: CODES.PENDING,
			metadata: {
				amount,
			},
		}, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateWallet(
		id: number,
		data: Partial<UserWalletInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<UserWalletInterface>('app.user_wallet', transaction)
			.set(data)
			.where('id = :id', { id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async updateBank(
		id: number,
		data: Partial<UserBankInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<UserBankInterface>('app.user_bank', transaction)
			.set(data)
			.where('id = :id', { id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async updateRequest(
		id: number,
		data: Partial<UserRequestInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<UserRequestInterface>('app.user_request', transaction)
			.set(data)
			.where('id = :id', { id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async updateTransactionStatus(
		changer_user_id: number,
		id: number,
		status: WALLET_STATUSES,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<UserWalletTransactionInterface>('app.user_wallet_transaction', transaction)
			.set({ status })
			.where('id = :id', { id })
			.execute()
			.then(() => true)
			.then(isUpdated => {
				if(isUpdated) {
					this.save('HistoryUserWalletTransactionRecord', {
						changer_user_id,
						user_wallet_transaction_id: id,
						status,
					})
				}

				return isUpdated
			})
			.catch(() => false)

	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getBanks(
		ids: number[],
		transaction: EntityManager,
	): Promise<UserBankInterface[]> {
		return this.query('app.user_bank', 'bank', transaction)
			.where('bank.id IN (:...ids)', { ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBanksOfUser(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserBankInterface[]> {
		return this.query('app.user_bank', 'bank', transaction)
			.where('bank.user_id = :user_id', { user_id })
			.orderBy('bank.created_at', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailOfUser(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
		banks: UserBankInterface[],
		wallets: Array<UserWalletInterface & {
			transactions: Array<UserWalletTransactionInterface & {
				request?: UserRequestInterface,
				order?: OrderInterface,
				stylesheetInventory?: StylesheetInventoryInterface,
				exchange?: ExchangeInterface
				user?: UserInterface & {
					profile: UserProfileInterface,
				},
			}>,
		}>,
	}> {
		return this.query('app.user', 'user', transaction)
			.addSelect('wallets.updated_at')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('user.banks', 'banks')
			.leftJoinAndSelect('user.wallets', 'wallets')
			.leftJoinAndSelect('wallets.transactions', 'transactions')
			.leftJoinAndMapOne('transactions.request', UserRequestRecord, 'request', 'request.id = transactions.ref_id AND transactions.source = :source_request', { source_request: STATEMENT_SOURCES.REQUEST })
			.leftJoinAndMapOne('transactions.order', OrderRecord, 'order', 'order.id = transactions.ref_id AND transactions.source = :source_order', { source_order: STATEMENT_SOURCES.ORDER })
			.leftJoinAndMapOne('transactions.stylesheetInventory', StylesheetInventoryRecord, 'sI', 'sI.id = transactions.ref_id AND transactions.source = :source_si', { source_si: STATEMENT_SOURCES.STYLESHEET_INVENTORY })
			.leftJoinAndMapOne('transactions.exchange', ExchangeRecord, 'exchange', 'exchange.id = transactions.ref_id AND transactions.source = :source_exchange', { source_exchange: STATEMENT_SOURCES.EXCHANGE })
			.leftJoinAndMapOne('transactions.user', UserRecord, 'us', 'us.id = transactions.ref_id AND transactions.source = :source_us', { source_us: STATEMENT_SOURCES.USER })
			.leftJoinAndSelect('us.profile', 'usProfile')
			.where('user.id = :user_id', { user_id })
			.orderBy('transactions.created_at', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRequest(
		id: number,
		user_id: number | null,
		transaction: EntityManager,
	): Promise<UserRequestInterface> {
		const Q = this.query('app.user_request', 'request', transaction)
			.where('request.id = :id', { id })

		if(user_id) {
			return Q
				.andWhere('request.user_id = :user_id', { user_id })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getWalletOfUser(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserWalletInterface> {
		return this.query('app.user_wallet', 'wallet', transaction)
			.where('wallet.user_id = :user_id', { user_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getWallet(
		id: number,
		transaction: EntityManager,
	): Promise<UserWalletInterface> {
		return this.query('app.user_wallet', 'wallet', transaction)
			.where('wallet.id = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	getWalletTransaction(type: STATEMENT_SOURCES, ref_id: number, filter: {limit?: number, offset?: number}, transaction: EntityManager): Promise<UserWalletTransactionInterface[]>
	getWalletTransaction(type: 'transaction', user_wallet_transaction_id: number, filter: {limit?: number, offset?: number}, transaction: EntityManager): Promise<UserWalletTransactionInterface>
	getWalletTransaction(type: 'wallet', user_wallet_id: number, filter: {limit?: number, offset?: number}, transaction: EntityManager): Promise<Array<UserWalletTransactionInterface & {
		request: UserRequestInterface,
	}>>

	@RepositoryModel.bound
	async getWalletTransaction(
		type: 'wallet' | 'transaction' | STATEMENT_SOURCES,
		user_wallet_or_transaction_id: number,
		filter: {
			limit?: number,
			offset?: number,
		} = {},
		transaction: EntityManager): Promise<any> {
		if(type === 'wallet') {
			let Q =  this.query('app.user_wallet_transaction', 'transaction', transaction)
				.where('transaction.user_wallet_id = :user_wallet_id', { user_wallet_id: user_wallet_or_transaction_id })
				.leftJoinAndMapOne('transaction.request', UserRequestRecord, 'request', 'request.id = transaction.ref_id AND transaction.source = :request', { request: STATEMENT_SOURCES.REQUEST })
				.orderBy('transaction.created_at', 'DESC')
				if (!!filter.limit) {
					Q = Q.take(filter.limit)
				}
				if (!!filter.offset) {
					Q = Q.skip(filter.offset)
				}
				return Q.getMany()
				.then(this.parser) as any
		} else if(type === 'transaction') {
			return this.query('app.user_wallet_transaction', 'transaction', transaction)
				.where('transaction.id = :user_wallet_or_transaction_id', { user_wallet_or_transaction_id })
				.getOne()
				.then(this.parser) as any
		} else {
			return this.query('app.user_wallet_transaction', 'transaction', transaction)
				.where('transaction.source = :type AND transaction.ref_id = :user_wallet_or_transaction_id', { type, user_wallet_or_transaction_id })
				.orderBy('transaction.created_at', 'DESC')
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getTransactionsOf(wallet_transaction_id: number, transaction: EntityManager): Promise<UserWalletTransactionInterface[]> {
		return this.query('app.user_wallet_transaction', 'transaction', transaction)
			.where('transaction.ref_id = :wallet_transaction_id AND transaction.source = :source', { wallet_transaction_id, source: STATEMENT_SOURCES.WALLET_TRANSACTION })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRecursiveTransactionsOf(wallet_transaction_id: number, transaction: EntityManager): Promise<UserWalletTransactionInterface[]> {
		return this.queryCustom(`
			WITH RECURSIVE transaction(id, type, amount, status, source, ref_id) AS (
				SELECT id, type, amount, status, source, ref_id FROM app.user_wallet_transaction WHERE id = $1
				UNION
					SELECT w.id, w.type, w.amount, w.status, w.source, w.ref_id
					FROM app.user_wallet_transaction w
					INNER JOIN transaction t on t.id = w.ref_id AND w.source = 'WALLET_TRANSACTION'
			)
			SELECT *
			FROM transaction;
		`, [wallet_transaction_id], transaction) as any
	}

	async getWalletAndPromoTransaction(transaction: EntityManager): Promise<Array<UserWalletInterface & {
			transactions: UserWalletTransactionInterface[],
	}>> {
		return this.queryCustom(`
				select
					uwt.user_wallet_id user_wallet_id
				from app.user_wallet_transaction uwt
				where uwt.source = 'CAMPAIGN'
				group by uwt.user_wallet_id
			`, undefined, transaction)
			.then((data: Array<{user_wallet_id: number}>) => {
				return this.query('app.user_wallet', 'wallet', transaction)
					.leftJoinAndSelect('wallet.transactions', 'transactions')
					.where('wallet.id in (:...user_wallet_ids)', {user_wallet_ids: data.map(d => d.user_wallet_id)})
					.getMany()
					.then(this.parser) as any
			})
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async cancelWithdrawRequest(
		request_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<UserRequestInterface>('app.user_request', transaction)
			.where('id = :request_id', { request_id })
			.set({
				cancelled_at: new Date(),
			})
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================ PRIVATES ============================

}
