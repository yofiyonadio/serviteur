import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
} from 'typeorm'

import {
	UserCardRecord,
} from 'energie/app/records'

import {
	UserCardInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'


@EntityRepository()
export default class UserCardRepository extends RepositoryModel<{
	UserCardRecord: UserCardRecord,
}> {

	static __displayName = 'UserCardRepository'

	protected records = {
		UserCardRecord,
	}

	// ============================= INSERT =============================
	async insertCard(
		data: Parameter<UserCardInterface>,
		transaction: EntityManager,
	) {
		return this.save('UserCardRecord', data, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async getCards(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserCardInterface[]> {
		return this.query('app.user_card', 'card', transaction)
			.where('card.user_id = :user_id', { user_id })
			.orderBy('card.id', 'DESC')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
