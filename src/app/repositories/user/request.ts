import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	UserRequestRecord,
	UserAssetRecord,
	StyleboardRecord,
	VariantRecord,
	StylecardRecord,
	VariantColorRecord,
	ProductRecord,
	VariantAssetRecord,
	BrandRecord,
	CategoryRecord,
	ColorRecord,
	SizeRecord,
	UserRecord,
	VariantAnswerRecord,
	OrderRequestRecord,
	StyleboardUserAssetRecord,
	InventoryRecord,
} from 'energie/app/records'

import {
	UserInterface,
	UserProfileInterface,
	UserBankInterface,
	UserAssetInterface,
	StyleboardInterface,
	StylecardInterface,
	VariantInterface,
	ProductInterface,
	BrandInterface,
	CategoryInterface,
	SizeInterface,
	ColorInterface,
	VariantAssetInterface,
	VariantAnswerInterface,
	OrderRequestInterface,
	InventoryInterface,
} from 'energie/app/interfaces'

import UserRequestInterface, { UserRequestMetadataInterface } from 'energie/app/interfaces/user.request'

// import { STATEMENTS, STATEMENT_SOURCES } from 'energie/utils/constants/enum'

// import { Parameter } from 'types/common'

import { USER_REQUESTS, CODES } from 'energie/utils/constants/enum'
// import {
// 	TypeformHelper,
// 	TimeHelper,
// } from 'utils/helpers'

// import {
// 	groupBy,
// } from 'lodash'

@EntityRepository()
export default class UserRequestRepository extends RepositoryModel<{
	UserRequestRecord: UserRequestRecord,
}> {

	static __displayName = 'UserRequestRepository'

	protected records = {
		UserRequestRecord,
	}

	// ============================= INSERT =============================
@RepositoryModel.bound
	async insert(
		user_id: number,
		type: USER_REQUESTS,
		metadata: UserRequestMetadataInterface | null,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<UserRequestInterface> {
		return this.save('UserRequestRecord', {
			user_id,
			type,
			metadata: metadata as any ?? {},
			status: CODES.PENDING,
			note,
		}, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(request_id: number, data: Partial<UserRequestInterface>, transaction: EntityManager) {
		return this.renew('UserRequestRecord', request_id, data as any, {
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			user_id?: number,
			code?: CODES,
			type?: USER_REQUESTS,
			is_active?: boolean,
			is_expired?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<{
		count: number,
		data: Array<UserRequestInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			stylist: UserInterface & {
				profile: UserProfileInterface,
				picture: UserAssetInterface,
			},
			styleboard?: StyleboardInterface & {
				stylecards: Array<StylecardInterface & {
					variants: Array<VariantInterface & {
						assets: VariantAssetInterface[],
						answer: VariantAnswerInterface,
						inventories: InventoryInterface[],
					}>,
				}>,
				assets?: UserAssetInterface[],
				order: OrderRequestInterface,
			},
		}>,
	}> {
		let Q = this.query('app.user_request', 'request', transaction)
			.leftJoinAndSelect('request.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.skip(offset)
			.take(limit)
			.orderBy('request.created_at', 'ASC')
			.where('request.type IS NOT NULL')

		if (search) {
			Q = Q.andWhere(`request.id::text ILIKE :search OR (profile.first_name || ' ' || profile.last_name) ILIKE :search`, { search: `%${ search }%` })
		}

		if(filter.code) {
			Q = Q.andWhere('request.status = :code', { code: filter.code })
		}

		if(filter.type) {
			if(filter.type === USER_REQUESTS.STYLING) {
				Q = Q.andWhere('request.type = :type', { type: filter.type })

					// left join styleboard
					.leftJoinAndMapOne('request.styleboard', StyleboardRecord, 'styleboard', `styleboard.id = (request.metadata ->> 'styleboard_id')::int`)

					// get styleboard stylist
					.leftJoinAndMapOne('request.stylist', UserRecord, 'stylist', 'stylist.id = styleboard.stylist_id')
					.leftJoinAndSelect('stylist.profile', 'stylist_profile')
					.leftJoinAndMapOne('stylist.picture', UserAssetRecord, 'picture', 'picture.id = stylist_profile.cover_id')

					// get deep to variant
					.leftJoin('styleboard.styleboardStylecards', 'styleboardStylecards')
					.leftJoinAndMapMany('styleboard.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = styleboardStylecards.stylecard_id')
					.leftJoin('stylecard.stylecardVariants', 'stylecardVariant')
					.leftJoinAndMapMany('stylecard.variants', VariantRecord, 'variant', 'variant.id = stylecardVariant.variant_id')
					.leftJoinAndMapOne('variant.answer', VariantAnswerRecord, 'variant_answer', 'variant_answer.variant_id = variant.id and variant_answer.user_id = :user_id', {user_id: filter.user_id})
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'variant_asset')
							.orderBy('variant_asset.order', 'ASC')
					}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')
					.leftJoinAndMapMany('variant.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
					.leftJoinAndMapMany('variant.inventories', InventoryRecord, 'inventory', `inventory.variant_id = variant.id and inventory.deleted_at is null and inventory.status = 'AVAILABLE'`)

					// get deeper to order
					.leftJoinAndMapOne('styleboard.order', OrderRequestRecord, 'order_request', `order_request.ref_id = styleboard.id and order_request.type = 'STYLEBOARD'`)

					// get styleboard image reference
					.leftJoin(StyleboardUserAssetRecord, 'styleboard_user_asset', 'styleboard_user_asset.styleboard_id = styleboard.id')
					.leftJoinAndMapMany('styleboard.assets', UserAssetRecord, 'user_asset', 'user_asset.id = styleboard_user_asset.user_asset_id')

					if(filter.is_active && !filter.is_expired) {
						Q = Q.andWhere(`styleboard.status not in ('RESOLVED', 'REDEEMED', 'EXCEPTION')`)
					}

					if(!filter.is_active && filter.is_expired) {
						Q = Q.andWhere(`styleboard.status in ('RESOLVED', 'REDEEMED', 'EXCEPTION')`)
					}

				} else {
				Q = Q.andWhere('request.type = :type', { type: filter.type })
			}
		}

		if(filter.user_id) {
			Q = Q.andWhere('request.user_id = :user_id', {user_id: filter.user_id})
		}

		return Q.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async get(
		request_id: number,
		config: {
			type?: USER_REQUESTS,
			user_id?: number,
		} = {},
		transaction: EntityManager): Promise<UserRequestInterface & {
		user: UserInterface & {
			profile: UserProfileInterface & {
				stylist?: UserInterface & {
					profile: UserProfileInterface,
				},
			},
			banks?: UserBankInterface[],
			receipt?: UserAssetInterface,
		},
		styleboard?: StyleboardInterface & {
			stylecards?: Array<StylecardInterface & {
				variants: Array<VariantInterface & {
					product?: ProductInterface & {
						variants: Array<VariantInterface & {
							size: SizeInterface,
						}>,
					},
					brand: BrandInterface,
					category: CategoryInterface,
					size: SizeInterface,
					colors: ColorInterface[],
					assets?: VariantAssetInterface[],
					answer?: VariantAnswerInterface,
					inventories?: InventoryInterface[],
				}>,
			}>,
			assets?: UserAssetInterface[],
		},
	}> {
		let Q = this.query('app.user_request', 'request', transaction)
			.leftJoinAndSelect('request.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('profile.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')

		if (config.type === USER_REQUESTS.STYLING) {
			Q = Q.leftJoinAndMapOne('request.styleboard', StyleboardRecord, 'styleboard', `styleboard.id = (request.metadata ->> 'styleboard_id')::int`)
					// get stylecards
					.leftJoin('styleboard.styleboardStylecards', 'styleboardStylecards')
					.leftJoinAndMapMany('styleboard.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = styleboardStylecards.stylecard_id')
					.leftJoin('stylecard.stylecardVariants', 'stylecardVariant')

					// get deep to variant
					.leftJoinAndMapMany('stylecard.variants', VariantRecord, 'variant', 'variant.id = stylecardVariant.variant_id')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'variant_color')
							.orderBy('variant_color.order', 'ASC')
					}, 'vc', 'vc.variant_id = variant.id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'variant_asset')
							.orderBy('variant_asset.order', 'ASC')
					}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')
					.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
					.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
					.leftJoinAndMapOne('variant.category', CategoryRecord, 'category', 'category.id = product.category_id')
					.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
					.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
					.leftJoinAndMapMany('variant.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
					.leftJoinAndMapOne('variant.answer', VariantAnswerRecord, 'variant_answer', 'variant_answer.variant_id = variant.id and variant_answer.user_id = :user_id', {user_id: config.user_id})
					.leftJoinAndMapMany('variant.inventories', InventoryRecord, 'inventory', `inventory.variant_id = variant.id and inventory.deleted_at is null and inventory.status = 'AVAILABLE'`)

					// get deeper to product variant
					.leftJoinAndMapMany('product.variants', VariantRecord, 'pv', 'pv.product_id = product.id')
					.leftJoinAndMapOne('pv.size', SizeRecord, 'vsize', 'vsize.id = pv.size_id')

					// get styleboard image reference
					.leftJoin(StyleboardUserAssetRecord, 'styleboard_user_asset', 'styleboard_user_asset.styleboard_id = styleboard.id')
					.leftJoinAndMapMany('styleboard.assets', UserAssetRecord, 'user_asset', 'user_asset.id = styleboard_user_asset.user_asset_id')

					.andWhere(`request.type = 'STYLING'`)
		} else {
			Q = Q.leftJoinAndSelect('user.banks', 'banks')
			.leftJoinAndMapOne('user.receipt', UserAssetRecord, 'receipt', `receipt.id = (request.metadata ->> 'asset_id')::int`)
		}

		return Q.where('request.id = :request_id', { request_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLatestStylingRequest(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserRequestInterface & {
		styleboard: StyleboardInterface,
	}> {
		return this.query('app.user_request', 'request', transaction)
			.leftJoinAndMapOne('request.styleboard', StyleboardRecord, 'styleboard', `styleboard.id = (request.metadata ->> 'styleboard_id')::int`)
			.where(`request.type = 'STYLING'`)
			.andWhere('request.user_id = :user_id', {user_id})
			.orderBy('request.id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}
	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
