import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	UserRecord,
	UserAddressRecord,
	UserAssetRecord,
	UserCouponRecord,
	UserNoteRecord,
	UserProfileRecord,
	UserRoleRecord,
	UserSocialRecord,
	UserTokenRecord,
} from 'energie/app/records'

import { UserTokenInterface, UserInterface, UserSocialInterface, UserProfileInterface, UserRoleInterface, UserAddressInterface, UserAssetInterface, UserNoteInterface, UserRequestInterface } from 'energie/app/interfaces'

import TimeHelper from 'utils/helpers/time'

import ErrorModel, { ERROR_CODES, ERRORS } from 'app/models/error'
import { TOKENS, SOCIALS, USER_REQUESTS, CODES } from 'energie/utils/constants/enum'
import { Parameter } from 'types/common'

@EntityRepository()
export default class UserAccountRepository extends RepositoryModel<{
	UserRecord: UserRecord,
	UserCouponRecord: UserCouponRecord,
	UserNoteRecord: UserNoteRecord,
	UserProfileRecord: UserProfileRecord,
	UserRoleRecord: UserRoleRecord,
	UserSocialRecord: UserSocialRecord,
	UserTokenRecord: UserTokenRecord,
}> {

	static __displayName = 'UserAccountRepository'

	protected records = {
		UserRecord,
		UserCouponRecord,
		UserNoteRecord,
		UserProfileRecord,
		UserRoleRecord,
		UserSocialRecord,
		UserTokenRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async softRegister(
		id: number,
		firstName: string,
		lastName: string,
		email: string,
		phone: string,
		isVerified: boolean,
		isStyleProfileCompleted: boolean,
		roles: string[],
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
	}> {
		// BUG
		// Typeorm currently have a bug on PostrgreSQL when inserting
		// data on autogenerated column
		// so supplying id won't change the desired user's id
		// postgres will instead use the auto generated id
		// hence we must use Query Builder

		const profile = await this.save('UserProfileRecord', {
			first_name: firstName,
			last_name: lastName,
			phone,
			is_style_profile_completed: isStyleProfileCompleted,
		}, transaction)

		return this.queryCustom(
			`INSERT INTO "app"."user"(
				"created_at", "updated_at", "user_profile_id", "id", "email", "password", "token", "is_verified", "logged_at", "deleted_at"
			) VALUES (
				DEFAULT, DEFAULT, $1, $2, $3, DEFAULT, DEFAULT, $4, $5, DEFAULT) RETURNING "id", "created_at", "updated_at", "is_verified"`,
			[profile.id, id, email, isVerified, new Date()],
			transaction,
		).then(async () => {
			// ROLES

			if (roles.length) {
				await this.save('UserRoleRecord', roles.map(role => {
					return {
						id: undefined,
						user_id: id,
						role,
					}
				}), transaction)
			}

			return this.getOne('UserRecord', {
				id,
			}, { transaction }).then(user => {
				return {
					...user,
					profile,
				}
			})
		})
	}

	@RepositoryModel.bound
	async insertUserSocial(userSocial: Parameter<UserSocialInterface>, transaction: EntityManager): Promise<UserSocialInterface> {
		return this.save('UserSocialRecord', userSocial, transaction)
	}

@RepositoryModel.bound
	async insertUserToken(userToken: Parameter<UserTokenInterface>, transaction: EntityManager): Promise<UserTokenInterface> {
		return this.save('UserTokenRecord', userToken, transaction)
	}

	@RepositoryModel.bound
	async insertUser(user: Parameter<UserInterface>, transaction: EntityManager): Promise<UserInterface> {
		return this.save('UserRecord', user, transaction)
	}

	@RepositoryModel.bound
	async insertUserProfile(profile: Parameter<UserProfileInterface>, transaction: EntityManager): Promise<UserProfileInterface> {
		return this.save('UserProfileRecord', profile, transaction)
	}

	@RepositoryModel.bound
	async insertNote(userNote: Parameter<UserNoteInterface>, transaction: EntityManager): Promise<UserNoteInterface> {
		return this.save('UserNoteRecord', userNote, transaction)
	}

	@RepositoryModel.bound
	async insertRole(userRole: Parameter<UserRoleInterface>, transaction: EntityManager): Promise<UserRoleInterface> {
		return this.save('UserRoleRecord', userRole, transaction)
	}

	@RepositoryModel.bound
	async insertCoupon(user_id: number, coupon_id: number) {
		return this.save('UserCouponRecord', {
			user_id,
			coupon_id,
		})
	}
	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateNote(id: number, note: Partial<UserNoteInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('UserNoteRecord', id, note, { transaction })
	}

	@RepositoryModel.bound
	async updateUser(id: number, user: Partial<UserInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('UserRecord', id, user, { transaction })
	}

	@RepositoryModel.bound
	async updateToken(id: number, token: Partial<UserTokenInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('UserTokenRecord', id, token, { transaction })
	}

	@RepositoryModel.bound
	async updateSocial(ref_id: string, type: SOCIALS , social: Partial<UserSocialInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('UserSocialRecord', `type = :type AND ref_id = :ref_id`, social, {
			parameters: {
				type,
				ref_id,
			},
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async get(transaction: EntityManager): Promise<UserInterface[]> {
		return this.query('app.user', 'user', transaction)
			.getMany()
			.then(this.parser) as any
	}

	async getById(user_id: number, withProfile: false | undefined, transaction: EntityManager): Promise<UserInterface>
	async getById(user_id: number, withProfile: true | undefined, transaction: EntityManager): Promise<UserInterface & { profile: UserProfileInterface }>

	@RepositoryModel.bound
	async getById(
		user_id: number,
		withProfile?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		const Q = this.query('app.user', 'user', transaction)
			.addSelect('user.password')
			.where('user.id = :user_id', { user_id })

		if(withProfile) {
			return Q.leftJoinAndSelect('user.profile', 'profile')
				.getOne()
				.then(this.parser)
		} else {
			return Q
				.getOne()
				.then(this.parser)
		}
	}

	async getDetail(user_ids: number[], deep: false | undefined, transaction: EntityManager): Promise<Array<UserInterface & {
		profile: UserProfileInterface & {
			stylist: UserInterface & {
				profile: UserProfileInterface,
			},
		},
		roles: UserRoleInterface[],
	}>>
	async getDetail(user_ids: number[], deep: true, transaction?: EntityManager): Promise<Array<UserInterface & {
		profile: UserProfileInterface & {
			stylist: UserInterface & {
				profile: UserProfileInterface,
			},
			signature: UserAssetInterface,
			cover: UserAssetInterface,
		},
		address: UserAddressInterface,
		asset: UserAssetInterface,
		roles: UserRoleInterface[],
		addresses: UserAddressInterface[],
		// notes: Array<UserNoteInterface & {
		// 	creator: UserInterface & {
		// 		profile: UserProfileInterface,
		// 	},
		// }>,
	}>>

	@RepositoryModel.bound
	async getDetail(user_ids: number[], deep?: boolean, transaction?: EntityManager): Promise<any> {
		const Q = this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('profile.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndSelect('user.roles', 'roles', 'roles.deleted_at IS NULL')
			.orderBy('roles.created_at', 'ASC')

		if(deep) {
			return Q
				.leftJoinAndSelect('user.addresses', 'addresses', 'addresses.deleted_at IS NULL')
				// .leftJoinAndSelect('user.notes', 'notes', 'notes.deleted_at IS NULL')
				// .leftJoinAndSelect('notes.creator', 'creator')
				// .leftJoinAndSelect('creator.profile', 'creatorProfile')
				// .orderBy('notes.created_at', 'DESC')
				.leftJoinAndMapOne('user.address', UserAddressRecord, 'mainAddress', 'mainAddress.id = profile.address_id')
				.leftJoinAndMapOne('user.asset', UserAssetRecord, 'mainAsset', 'mainAsset.id = profile.asset_id')
				.leftJoinAndSelect('profile.signature', 'signature')
				.leftJoinAndSelect('profile.cover', 'cover')
				.addOrderBy('addresses.updated_at', 'DESC')
				.where('user.id IN (:...user_ids)', { user_ids })
				.getMany()
				.then(this.parser)
		} else {
			return Q
				.where('user.id IN (:...user_ids)', { user_ids })
				.getMany()
				.then(this.parser)
		}
	}

	async getByEmail(email: string, withProfile?: false, transaction?: EntityManager): Promise<UserInterface>
	async getByEmail(email: string, withProfile?: true, transaction?: EntityManager): Promise<UserInterface & { profile: UserProfileInterface, roles: UserRoleInterface[] }>

	@RepositoryModel.bound
	async getByEmail(
		email: string,
		withProfile?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		if (withProfile) {
			return this.query('app.user', 'user', transaction)
				.leftJoinAndSelect('user.profile', 'profile')
				.leftJoinAndSelect('user.roles', 'roles')
				.where('user.email = :email', { email })
				.addSelect(['user.password', 'user.logged_at'])
				.getOne()
				.then(this.parser)
		} else {
			return this.query('app.user', 'user', transaction)
				.where('user.email  =:email', { email })
				.addSelect(['user.password', 'user.logged_at'])
				.getOne()
				.then(this.parser)
		}
	}

	async getByPhone(phone: string, withProfile?: false, transaction?: EntityManager): Promise<UserInterface>
	async getByPhone(phone: string, withProfile?: true, transaction?: EntityManager): Promise<UserInterface & { profile: UserProfileInterface, roles: UserRoleInterface[] }>

	@RepositoryModel.bound
	async getByPhone(
		phone: string,
		withProfile?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		if (withProfile) {
			return this.query('app.user', 'user', transaction)
				.leftJoinAndSelect('user.profile', 'profile')
				.leftJoinAndSelect('user.roles', 'roles')
				.where('profile.phone = :phone', { phone })
				.addSelect(['user.password', 'user.logged_at'])
				.getOne()
				.then(this.parser)
		} else {
			return this.query('app.user', 'user', transaction)
				.leftJoinAndSelect('user.profile', 'profile')
				.where('profile.phone = :phone', { phone })
				.addSelect(['user.password', 'user.logged_at'])
				.getOne()
				.then(this.parser)
		}
	}

	@RepositoryModel.bound
	async getChangeStylistRequest(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserRequestInterface> {
		return this.query('app.user_request', 'request', transaction)
			.where('request.user_id = :user_id', { user_id })
			.andWhere('request.status = :pending', { pending: CODES.PENDING })
			.andWhere('request.cancelled_at IS NULL')
			.andWhere('request.type = :type', { type: USER_REQUESTS.CHANGE_STYLIST })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getNote(
		user_id: number,
		note_id: number,
		transaction: EntityManager,
	): Promise<UserNoteInterface & {
		creator: UserInterface & {
			profile: UserProfileInterface,
			asset: UserAssetInterface,
		},
	}> {
		return this.query('app.user_note', 'notes', transaction)
			.addSelect('notes.updated_at')
			.leftJoinAndSelect('notes.creator', 'creator')
			.leftJoinAndSelect('creator.profile', 'creatorProfile')
			.leftJoinAndMapOne('creator.asset', UserAssetRecord, 'asset', 'asset.id = creatorProfile.asset_id')
			.where('notes.user_id = :user_id', { user_id })
			.andWhere('notes.id = :note_id', { note_id })
			.andWhere('notes.deleted_at IS NULL')
			.orderBy('notes.created_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getNotes(
		user_id: number,
		transaction: EntityManager,
	): Promise<Array<UserNoteInterface & {
		creator: UserInterface & {
			profile: UserProfileInterface,
			asset: UserAssetInterface,
		},
	}>> {
		return this.query('app.user_note', 'notes', transaction)
			.addSelect('notes.updated_at')
			.leftJoinAndSelect('notes.creator', 'creator')
			.leftJoinAndSelect('creator.profile', 'creatorProfile')
			.leftJoinAndMapOne('creator.asset', UserAssetRecord, 'asset', 'asset.id = creatorProfile.asset_id')
			.where('notes.user_id = :user_id', { user_id })
			.andWhere('notes.deleted_at IS NULL')
			.orderBy('notes.created_at', 'DESC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRoles(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserRoleInterface[]> {
		return this.getMany('UserRoleRecord', {
			user_id,
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getToken(
		email: string,
		token: string,
		type: TOKENS,
		transaction: EntityManager,
	): Promise<UserTokenInterface & {
		user: UserInterface,
	}> {
		return this.query('app.user_token', 'token', transaction)
			.leftJoinAndSelect('token.user', 'user')
			.where('user.email = :email', { email })
			.andWhere('token.token = :token', { token })
			.andWhere('token.used_at IS NULL')
			.getOne()
			.then(this.parser)
			.catch(async err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					// find adjacent token and update next_retry_at
					return this.query('app.user_token', 'token', transaction)
						.leftJoinAndSelect('token.user', 'user')
						.where('user.email = :email', { email })
						.andWhere('token.type = :type', { type })
						.andWhere('token.used_at IS NULL')
						.getOne()
						.then(this.parser)
						.then(async (userToken: UserTokenInterface & {
							user: UserInterface,
						}) => {
							if (Date.now() > +userToken.next_retry_at) {
								let nextRetry = userToken.next_retry_at ? TimeHelper.moment(userToken.next_retry_at) : TimeHelper.moment()

								const iteration = (userToken.verification_attempt ?? 0) + 1

								nextRetry = nextRetry.add(iteration > 3 ? Math.pow(5, iteration - 2) : 0, 's')

								await this.renew('UserTokenRecord', 'user_id = :user_id AND type = :type AND used_at IS NULL', {
									next_retry_at: nextRetry.toDate(),
									verification_attempt: iteration,
								}, {
									parameters: {
										user_id: userToken.user.id,
										type,
									},
									// We use this.manager because we don't want it to be cancelled
									transaction: this.manager,
								})

								throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, iteration > 5 ? nextRetry.toDate() : undefined)
							}

							throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, userToken.next_retry_at)
						})
				}

				throw err
			}) as any
	}

	@RepositoryModel.bound
	async getTokenBy(user_id: number, type: TOKENS, transaction: EntityManager): Promise<UserTokenInterface> {
		return this.query('app.user_token', 'token', transaction)
			.addSelect('token.updated_at')
			.where('token.user_id = :user_id AND token.type = :type AND token.used_at IS NULL', { user_id, type })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserByToken(token: string, transaction: EntityManager): Promise<UserInterface & {
		profile: UserProfileInterface,
		roles: UserRoleInterface[],
	}> {
		return this.query('app.user', 'user', transaction)
			.addSelect(['user.password', 'user.token'])
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('user.roles', 'roles')
			.where('user.token = :token', { token })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserSocial(
		ref_id: string,
		type: SOCIALS,
		transaction: EntityManager,
	): Promise<UserSocialInterface> {
		return this.query('app.user_social', 'user_social', transaction)
			.where('user_social.ref_id = :ref_id', { ref_id })
			.andWhere('user_social.type  = :type', { type })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserSocialByUserId(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserSocialInterface> {
		return this.query('app.user_social', 'user_social', transaction)
			.where('user_social.user_id = :user_id', { user_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserStylist(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface & {
			asset: UserAssetInterface,
			cover: UserAssetInterface,
		},
	}> {
		return this.query('app.user', 'user', transaction)
			.leftJoin('user.profile', 'profile')
			.leftJoinAndMapOne('user.stylist', UserRecord, 'stylist', 'profile.stylist_id = stylist.id')
			.leftJoinAndSelect('stylist.profile', 'sp')
			.leftJoinAndSelect('sp.asset', 'asset')
			.leftJoinAndSelect('sp.cover', 'cover')
			.where('user.id = :user_id', { user_id })
			.getOne()
			.then((d: any) => {
				return d.stylist
			})
			.then(this.parser) as any
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async deleteNote(id: number, transaction: EntityManager): Promise<boolean> {
		return await this.renew('UserNoteRecord', id, {
			deleted_at: new Date(),
		}, { transaction })
	}

	@RepositoryModel.bound
	async deleteRole(user_id: number, role: string, transaction: EntityManager): Promise<boolean> {
		return this.query('app.user_role', 'user_role', transaction)
			.where('user_id = :user_id', { user_id })
			.andWhere('role = :role', { role })
			.delete()
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================ PRIVATES ============================

}
