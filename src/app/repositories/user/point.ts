import RepositoryModel from 'app/models/repository'
import { EntityManager, EntityRepository } from 'typeorm'

import {
	UserPointRecord,
	UserPointTransactionRecord,
	UserProfileRecord,
} from 'energie/app/records'

import { UserInterface, UserPointInterface, UserPointTransactionInterface, UserProfileInterface } from 'energie/app/interfaces'
import { Parameter } from 'types/common'


@EntityRepository()
export default class UserPointRepository extends RepositoryModel<{
	UserPointRecord: UserPointRecord,
	UserPointTransactionRecord: UserPointTransactionRecord,
}> {

	static __displayName = 'UserPointRepository'

	protected records = {
		UserPointRecord,
		UserPointTransactionRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async createPoint(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserPointInterface> {
		return this.save('UserPointRecord', { user_id }, transaction)
	}

	@RepositoryModel.bound
	async createTransaction(
		user_point_id: number,
		data: Parameter<UserPointTransactionInterface, 'user_point_id'>,
		transaction: EntityManager,
	): Promise<UserPointTransactionInterface> {
		return this.save('UserPointTransactionRecord', {
			user_point_id,
			...data,
		}, transaction)
	}

	// @RepositoryModel.bound
	// async migrateUserPointTransaction(
	// 	transaction: EntityManager,
	// ) {
	// 	return this.queryCustom(`
	// 	INSERT INTO app.user_point_transaction
	// 	(user_point_id, amount, "type", source, "status", note, metadata, expired_at)
	// 	SELECT  x.id, x.balance, 'CREDIT', 'SYSTEM', 'APPLIED', 'Point Migration', json_build_object('change', x.balance), (NOW()::DATE + 180)::TIMESTAMP  FROM app.user_point "x";
	// 	UPDATE app.user_point_transaction
	// 	SET
	// 	source = 'DISABLE'
	// 	WHERE created_at::TIMESTAMP < NOW()::TIMESTAMP
	// 	`, undefined, transaction)
	// }

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updatePoint(
		user_id: number,
		data: Parameter<UserPointInterface, 'user_id'>,
		transaction: EntityManager,
	) {
		return this.renew('UserPointRecord', 'user_id = :user_id', data, {
			parameters: {
				user_id,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async updatePointTransaction(
		id: number,
		data: object,
		transaction: EntityManager,
	) {
		return this.renew('UserPointTransactionRecord', id, data, {
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getPoint(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserPointInterface> {
		return this.getOne('UserPointRecord', { user_id }, { transaction })
	}

	@RepositoryModel.bound
	async getAllUserPoint(
		transaction: EntityManager,
	) {
		return this.queryCustom(`SELECT * FROM app.user_point "t" WHERE t.balance > 0`, undefined, transaction)
	}

	@RepositoryModel.bound
	async getPointExpiredbyDate(
		user_point_id: number,
		transaction: EntityManager,
	) {
		return this.queryCustom(`
		SELECT MAX(t.id) "id", SUM((t.metadata->>'change')::INT)"change", MIN(t.expired_at) "expired_at" FROM app.user_point_transaction "t"
		WHERE
		t.user_point_id = ${user_point_id}
		AND t.expired_at::DATE = '2022-03-07T02:28:24.968Z'::DATE
		AND t."type" = 'CREDIT'
		order BY
		MIN(t.expired_at), MIN(t.id)
		`, undefined, transaction)
	}

	@RepositoryModel.bound
	async getUserPointExpired(
		user_point_id: number,
		transaction: EntityManager,
	) {
		return this.queryCustom(`
		SELECT * FROM app.user_point_transaction "t"
		WHERE t.expired_at::DATE <= NOW()::DATE
		AND t.user_point_id = ${user_point_id}
		order BY t.expired_at, t.id`, undefined, transaction)
	}

	@RepositoryModel.bound
	async getTotalActivePoint(
		user_point_id: number,
		last_point_id: number,
		transaction: EntityManager,
	) {
		return this.queryCustom(`
			SELECT * FROM (
			(SELECT
			COALESCE(SUM((t.metadata->>'change')::INT),0)::INT "totals",
			MIN(t.id) "last_id",
			MIN(t.created_at) "last_created_at",
			MIN(t.metadata::TEXT)::JSON "last_metadata"
			FROM app.user_point_transaction "t"
			WHERE t.user_point_id = ${user_point_id}
			AND t."type" = 'CREDIT'
			AND t.expired_at::DATE > NOW()::DATE
			AND t.id >= ${last_point_id}
			order BY MIN(t.created_at) DESC, MIN(t.id) DESC)
			UNION ALL
			(SELECT
			0 "totals",
			t.id "last_id",
			t.created_at "last_created_at",
			t.metadata "last_metadata"
			FROM app.user_point_transaction "t"
			WHERE t.user_point_id = ${user_point_id}
			AND t."type" = 'CREDIT'
			AND t.id >= ${last_point_id}
			order BY t.created_at DESC, t.id DESC LIMIT 1)
			) "t"
			order BY t.last_id, t.totals DESC LIMIT 1
		`, undefined, transaction)
	}

	async getActivePointTransaction(
		user_point_id: number,
		last_point_id: number,
		amount: number,
		transaction: EntityManager,
	) {
		return this.queryCustom(`
			SELECT * FROM (
			(SELECT *, (t.metadata->>'change')::INT "usage", ((t.metadata->>'change')::INT - (t.metadata->>'change')::INT)::INT "change", (t.expired_at::DATE - NOW()::DATE)::INT "age" FROM (SELECT *,
			(SELECT SUM((metadata->>'change')::INT) FROM app.user_point_transaction WHERE id <= t.id AND user_point_id = ${user_point_id} AND "type" = 'CREDIT' AND expired_at::DATE > NOW()::DATE AND id >= ${last_point_id} order BY MIN(expired_at) ASC)::INT "agg"
			FROM app.user_point_transaction "t"
			WHERE t.user_point_id = ${user_point_id}
			AND t."type" = 'CREDIT'
			AND t.expired_at::DATE > NOW()::DATE
			AND t.id >= ${last_point_id} ) "t"
			WHERE t.agg < ${amount}
			order BY t.expired_at ASC)
			UNION ALL
			(SELECT *, (((t.metadata->>'change')::INT + ${amount}) - agg)::INT "usage", ((t.metadata->>'change')::INT - (((t.metadata->>'change')::INT + ${amount}) - agg))::INT "change", (t.expired_at::DATE - NOW()::DATE)::INT FROM (SELECT *,
			(SELECT SUM((metadata->>'change')::INT) FROM app.user_point_transaction WHERE id <= t.id AND user_point_id = ${user_point_id} AND "type" = 'CREDIT' AND expired_at::DATE > NOW()::DATE AND id >= ${last_point_id} order BY MIN(expired_at) ASC)::INT "agg"
			FROM app.user_point_transaction "t"
			WHERE t.user_point_id = ${user_point_id}
			AND t."type" = 'CREDIT'
			AND t.expired_at::DATE > NOW()::DATE
			AND t.id >= ${last_point_id} ) "t"
			WHERE t.agg >= ${amount}
			order BY t.expired_at ASC
			LIMIT 2)
			) "t"
			WHERE t.usage >= 0
		`, undefined, transaction)
	}

	@RepositoryModel.bound
	async getDetailTransaction(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
		point: UserPointInterface & {
			transactions: UserPointTransactionInterface[],
		},
	}> {
		return this.query('app.user', 'user', transaction)
			.where('user.id = :user_id', { user_id })
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')

			.leftJoinAndMapOne('user.point', UserPointRecord, 'point', 'point.user_id = user.id')
			.leftJoinAndMapMany('point.transactions', UserPointTransactionRecord, 'transaction', 'transaction.user_point_id = point.id')

			.getOne()
			.then(this.parser) as any
	}

	// -------------------- DoB Loyalty Point Scheduler
	@RepositoryModel.bound
	async getDobPoints() {
		return this.queryCustom(`SELECT * FROM (SELECT DISTINCT ON (a.user_id) a.id "answer_id", a.user_id, a.question_id, (a.answer->>'DATE')::DATE user_dob, b.balance, c.amount, c."status", c.note FROM app.user_answer "a"
		LEFT JOIN app.user_point "b" ON b.user_id = a.user_id
		LEFT JOIN app.user_point_transaction "c" ON c.user_point_id = b.id
		AND c.note = 'Birthday Loyalty Program Rewards'
		AND c."status" = 'APPLIED'
		WHERE a.question_id = 56
		AND EXTRACT(MONTH FROM (a.answer->>'DATE')::DATE) = EXTRACT(MONTH FROM NOW()::DATE)
		AND EXTRACT(DAY FROM (a.answer->>'DATE')::DATE) = EXTRACT(DAY FROM NOW()::DATE)
		order BY a.user_id) "t"
		WHERE t.note IS NULL`)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
