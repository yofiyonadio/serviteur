import RepositoryModel from 'app/models/repository'
import { EntityManager, EntityRepository } from 'typeorm'

import {
	UserWardrobeRecord,
	ProductRecord,
	VariantRecord,
	OrderDetailRecord,
} from 'energie/app/records'

import { TimeHelper } from 'utils/helpers'

import { UserWardrobeInterface, OrderDetailInterface } from 'energie/app/interfaces'


@EntityRepository()
export default class UserWardrobeRepository extends RepositoryModel<{
	UserWardrobeRecord: UserWardrobeRecord,
	ProductRecord: ProductRecord,
	VariantRecord: VariantRecord,
	OrderDetailRecord: OrderDetailRecord,
}> {

	static __displayName = 'UserWardrobeRepository'

	protected records = {
		UserWardrobeRecord,
		ProductRecord,
		VariantRecord,
		OrderDetailRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async createWardrobe(
		user_id: number,
		product_name: string,
		category: string,
		image: string,
		type: string,
		color_id: number,
		price: number | undefined,
		transaction: EntityManager,
	): Promise<UserWardrobeInterface> {
		return this.save('UserWardrobeRecord',
			{
				user_id,
				product_name,
				category,
				image,
				type,
				color_id,
				price
			}, transaction)
	}

	@RepositoryModel.bound
	async createOrUpdateWardrobe(
		data: UserWardrobeInterface[],
		transaction: EntityManager
	): Promise<boolean> {
		return this.saveOrUpdate('UserWardrobeRecord', data, transaction)
	}




	// ============================= UPDATE =============================

	@RepositoryModel.bound
	async updateWardrobe(
		id: number,
		wardrobe: Partial<UserWardrobeInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('UserWardrobeRecord', id, wardrobe, { transaction })
	}





	// ============================= GETTER =============================


	@RepositoryModel.bound
	async getUserWardrobe(
		user_id: number,
		transaction: EntityManager,
	): Promise<Array<Partial<UserWardrobeInterface>>> {
		return this.queryCustom(
			`(
				SELECT a.id, a.created_at, a.user_id, a.product_name, a.category, a.image, a."type", a.color_id, a.price, 'USER' "source" FROM app.user_wardrobe "a"
				WHERE a.user_id = ${user_id}
				AND a.deleted_at IS NULL
				)
				UNION ALL
				(
				SELECT
				DISTINCT ON (c.id )
				c.id "id",
				b.created_at "order_at",
				b.user_id,
				c.title "product_name",
				h.title "category",
				i.url "image",
				g.title "type",
				j.color_id,
				e.price,
				'ORDER' "source"
				FROM app."order" "b"
				LEFT JOIN app.order_detail "c" ON b.id = c.order_id
				LEFT JOIN app.inventory "d" ON d.id = c.ref_id
				LEFT JOIN app.variant "e" ON e.id = d.variant_id
				LEFT JOIN app.product "f" ON f.id = e.product_id
				LEFT JOIN master.category "g" ON g.id = f.category_id
				LEFT JOIN master.category "h" ON h.id = g.category_id
				LEFT JOIN app.variant_asset "i" ON i.variant_id = e.id
				LEFT JOIN app.variant_color "j" ON j.variant_id = e.id
				WHERE b.user_id = ${user_id}
				AND b."status" = 'RESOLVED'
				AND c.wardrobe_deleted_at IS NULL
				AND c.title NOT ILIKE '%Matchbox%'
				)`, undefined, transaction)
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserWardrobeDetail(
		id: number,
		source: string,
		transaction: EntityManager,
	): Promise<Array<Partial<UserWardrobeInterface>>> {
		if (source === 'USER') {
			return this.queryCustom(
				`	SELECT a.id, a.created_at, a.user_id, a.product_name, a.category, a.image, a."type", a.color_id, a.price, 'USER' "source" FROM app.user_wardrobe "a"
					WHERE a.id = ${id}
					AND a.deleted_at IS NULL`, undefined, transaction)
				.then(this.parser).then(results => results[0]) as any
		}

		if (source === 'ORDER') {
			return this.queryCustom(
				`	SELECT
					DISTINCT ON (c.id )
					c.id "id",
					b.created_at "order_at",
					b.user_id,
					c.title "product_name",
					h.title "category",
					i.url "image",
					g.title "type",
					j.color_id,
					e.price,
					'ORDER' "source"
					FROM app."order" "b"
					LEFT JOIN app.order_detail "c" ON b.id = c.order_id
					LEFT JOIN app.inventory "d" ON d.id = c.ref_id
					LEFT JOIN app.variant "e" ON e.id = d.variant_id
					LEFT JOIN app.product "f" ON f.id = e.product_id
					LEFT JOIN master.category "g" ON g.id = f.category_id
					LEFT JOIN master.category "h" ON h.id = g.category_id
					LEFT JOIN app.variant_asset "i" ON i.variant_id = e.id
					LEFT JOIN app.variant_color "j" ON j.variant_id = e.id
					WHERE c.id = ${id}
					AND b."status" = 'RESOLVED'
					AND c.wardrobe_deleted_at IS NULL`, undefined, transaction)
				.then(this.parser).then(results => results[0]) as any
		}

	}


	// ============================= DELETE =============================

	@RepositoryModel.bound
	async deleteWardrobe(
		id: number,
		source: string,
		transaction: EntityManager,
	): Promise<boolean> {

		if (source === 'USER') {
			return this.renew('UserWardrobeRecord', id, {
				deleted_at: TimeHelper.moment().toDate()
			} as UserWardrobeInterface, { transaction })
		}

		if (source === 'ORDER') {
			return this.renew('OrderDetailRecord', id, {
				wardrobe_deleted_at: TimeHelper.moment().toDate()
			} as Partial<OrderDetailInterface>, { transaction })
		}

	}

	// ============================ PRIVATES ============================

}
