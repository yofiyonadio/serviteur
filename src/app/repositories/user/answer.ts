import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	BrandRecord,
	ColorRecord,
	ProductRecord,
	SizeRecord,
	StylecardAnswerRecord,
	StylecardRecord,
	UserAnswerRecord,
	VariantAnswerRecord,
	VariantAssetRecord,
	VariantColorRecord,
	VariantRecord,
} from 'energie/app/records'

import {
	BrandInterface,
	ColorInterface,
	InventoryInterface,
	ProductInterface,
	SizeInterface,
	StylecardInterface,
	UserAnswerInterface,
	UserInterface,
	VariantAnswerInterface,
	VariantAssetInterface,
	VariantInterface,
} from 'energie/app/interfaces'

import {
	TypeformHelper,
	TimeHelper,
} from 'utils/helpers'

import {
	groupBy,
} from 'lodash'

@EntityRepository()
export default class UserAnswerRepository extends RepositoryModel<{
	UserAnswerRecord: UserAnswerRecord,
}> {

	static __displayName = 'UserAnswerRepository'

	protected records = {
		UserAnswerRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		user_id: number,
		question_id: number,
		answer: object,
		transaction: EntityManager,
	): Promise<boolean> {
		const latestAnswer: UserAnswerInterface | null = (await this.getByQuestionIds(user_id, [question_id], null, false, transaction).catch(() => {
			return [null]
		}))[0]

		if (latestAnswer && latestAnswer.id && (TimeHelper.moment(latestAnswer.updated_at).diff(Date.now(), 'h') > -1)) {
			return this.renew('UserAnswerRecord', latestAnswer.id, {
				answer,
			}, {
				transaction,
			})
		} else {
			return this.save('UserAnswerRecord', {
				user_id,
				question_id,
				answer,
			}, transaction).then(() => {
				return true
			}).catch(err => {
				this.warn(err)

				return false
			})
		}
	}

	@RepositoryModel.bound
	async insertBig(data: UserAnswerRecord[]): Promise<boolean> {
		return this.saveBig('app', 'user_answer', data)
	}

	@RepositoryModel.bound
	async insertFromTypeform(
		user_id: number,
		typeform: {
			answers: object[],
		},
		transaction: EntityManager,
	) {
		return await this.save('UserAnswerRecord', typeform.answers.map(answer => {
			const parsedAnswer = TypeformHelper.parseAnswers(answer)

			return {
				user_id,
				question_id: parsedAnswer[0],
				answer: parsedAnswer[1],
			}
		}), transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getLikes(
		user_id: number,
		type: 'variant' | 'stylecard',
		option: {
			with_dislike?: boolean,
		} = {},
		filter: {
			offset?: number,
			limit?: number,
		} = {},
		transaction: EntityManager,
	): Promise<UserInterface & {
		likes: Array<{
			variant?: VariantInterface & {
				inventory: InventoryInterface,
				product: ProductInterface,
				brand: BrandInterface,
				size: SizeInterface,
				colors: ColorInterface[],
				asset: VariantAssetInterface,
			},
			stylecard?: StylecardInterface,
		}>,
		dislikes?: Array<VariantAnswerInterface & {
			variant?: VariantInterface & {
				inventory: InventoryInterface,
				product: ProductInterface,
				brand: BrandInterface,
				size: SizeInterface,
				colors: ColorInterface[],
				asset: VariantAssetInterface,
			},
			stylecard?: StylecardInterface,
		}>,
	}> {
		let Q = this.query('app.user', 'user', transaction)
			.where('user.id = :user_id', {user_id})

			if (type === 'variant') {
				Q = Q.innerJoin(sq => {
					return sq.subQuery()
						.select([
							'max(__va.id) id',
							'__va.user_id user_id',
							'__va.variant_id variant_id',
						])
						.from(VariantAnswerRecord, '__va')
						.where('__va.user_id = :user_id', {user_id})
						.groupBy('__va.user_id')
						.addGroupBy('__va.variant_id')
						.offset(filter.offset)
						.limit(filter.limit)
				}, '_va_', '_va_.user_id = user.id')
				.leftJoinAndMapMany('user.likes', VariantAnswerRecord, 'lva', `lva.user_id = user.id and lva.id = _va_.id and (lva.answer ->> 'BOOLEAN')::boolean is true`)

				// get liked variants
				.leftJoinAndMapOne('lva.variant', VariantRecord, 'lvariant', 'lvariant.id = lva.variant_id')
				.leftJoinAndMapOne('lvariant.product', ProductRecord, 'lproduct', 'lproduct.id = lvariant.product_id')
				.leftJoinAndMapOne('lvariant.brand', BrandRecord, 'lbrand', 'lbrand.id = lproduct.brand_id')
				.leftJoinAndMapOne('lvariant.size', SizeRecord, 'lsize', 'lsize.id = lvariant.size_id')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'lvariant_color')
					.orderBy('lvariant_color.order', 'ASC')
				}, 'lvc', 'lvc.variant_id = lvariant.id')
				.leftJoinAndMapMany('lvariant.colors', ColorRecord, 'lcolor', 'lcolor.id = lvc.color_id')
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'l_variant_asset')
					.orderBy('l_variant_asset.order', 'ASC')
					.addOrderBy('l_variant_asset.id', 'ASC')
				}, 'l_va', 'l_va.variant_id = lvariant.id AND l_va.deleted_at IS NULL')
				.leftJoinAndMapOne('lvariant.asset', VariantAssetRecord, 'lvasset', 'lvasset.id = l_va.id')

				if (option.with_dislike) {
					Q = Q.leftJoinAndMapMany('user.dislikes', VariantAnswerRecord, 'dva', `dva.user_id = user.id and dva.id = _va_.id and (dva.answer ->> 'BOOLEAN')::boolean is false`)

					// get disliked variants
					.leftJoinAndMapOne('dva.variant', VariantRecord, 'dvariant', 'dvariant.id = dva.variant_id')
					.leftJoinAndMapOne('dvariant.product', ProductRecord, 'dproduct', 'dproduct.id = dvariant.product_id')
					.leftJoinAndMapOne('dvariant.brand', BrandRecord, 'dbrand', 'dbrand.id = dproduct.brand_id')
					.leftJoinAndMapOne('dvariant.size', SizeRecord, 'dsize', 'dsize.id = dvariant.size_id')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'dvariant_color')
						.orderBy('dvariant_color.order', 'ASC')
					}, 'dvc', 'dvc.variant_id = dvariant.id')
					.leftJoinAndMapMany('dvariant.colors', ColorRecord, 'dcolor', 'dcolor.id = dvc.color_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'd_variant_asset')
						.orderBy('d_variant_asset.order', 'ASC')
						.addOrderBy('d_variant_asset.id', 'ASC')
					}, 'd_va', 'd_va.variant_id = dvariant.id AND d_va.deleted_at IS NULL')
					.leftJoinAndMapOne('dvariant.asset', VariantAssetRecord, 'dvasset', 'dvasset.id = d_va.id')
				}
			}

			if (type === 'stylecard') {
				Q = Q.innerJoin(sq => {
					return sq.subQuery()
						.select([
							'max(__sa.id) id',
							'__sa.user_id user_id',
							'__sa.stylecard_id stylecard_id',
						])
						.from(StylecardAnswerRecord, '__sa')
						.where('__sa.user_id = :user_id', {user_id})
						.groupBy('__sa.user_id')
						.addGroupBy('__sa.stylecard_id')
						.offset(filter.offset)
						.limit(filter.limit)
				}, '_sa_', '_sa_.user_id = user.id')
				.leftJoinAndMapMany('user.likes', StylecardAnswerRecord, 'lsa', `lsa.user_id = user.id and lsa.id = _sa_.id and (lsa.answer ->> 'BOOLEAN')::boolean is true`)

				// get liked stylecards
				.leftJoinAndMapOne('lsa.stylecard', StylecardRecord, 'lstylecard', 'lstylecard.id = lsa.stylecard_id')

				if (option.with_dislike) {
					Q = Q.leftJoinAndMapMany('user.dislikes', StylecardAnswerRecord, 'dsa', `dsa.user_id = user.id and dsa.id = _sa_.id and (dsa.answer ->> 'BOOLEAN')::boolean is false`)

					// get disliked stylecards
					.leftJoinAndMapOne('dsa.stylecard', StylecardRecord, 'dstylecard', 'dstylecard.id = dsa.stylecard_id')
				}
			}

		return Q.getOne()
		.then(this.parser) as any
	}

	async getAll(user_id: number, date: Date | null, many: false | undefined, transaction: EntityManager): Promise<UserAnswerInterface[]>
	async getAll(user_id: number, date: Date | null, many: true | undefined, transaction: EntityManager): Promise<UserAnswerInterface[][]>

	@RepositoryModel.bound
	async getAll(
		user_id: number,
		date: Date | null = null,
		many: boolean = false,
		transaction?: EntityManager,
	) {
		if(many) {
			return this.query('app.user_answer', 'answer', transaction)
				.addSelect('answer.updated_at')
				.innerJoin('answer.question', 'question', 'question.deleted_at IS NULL')
				.where(date ? 'answer.user_id = :user_id AND answer.updated_at <= :date' : 'answer.user_id = :user_id', { user_id, date: TimeHelper.queryLessThanEqual(date) })
				.orderBy('answer.question_id', 'ASC')
				.addOrderBy('answer.updated_at', 'DESC')
				.addOrderBy('answer.created_at', 'DESC')
				.getMany()
				.then(this.parser)
				.then((datas: UserAnswerInterface[]) => {
					return Object.values(groupBy(datas, data => data.question_id))
				})
		} else {
			return this.query('app.user_answer', 'answers', transaction)
				.addSelect('answers.updated_at')
				.innerJoin('answers.question', 'question', 'question.deleted_at IS NULL')
				.innerJoin(query => {
					return query.from('app.user_answer', 'u_a')
						.select(['u_a.question_id as question_id', 'MAX(u_a.id) as id'])
						.where(date ? 'u_a.updated_at <= :date AND u_a.user_id = :user_id' : 'u_a.user_id = :user_id', { user_id, date: TimeHelper.queryLessThanEqual(date) })
						.groupBy('u_a.question_id')
				}, 'answer', 'answer.id = answers.id')
				.orderBy('answers.question_id', 'ASC')
				.getMany()
				.then(this.parser)
		}
	}

	async getByGroupId(user_id: number, group_id: number, date: Date | null, many: false | undefined, transaction: EntityManager): Promise<UserAnswerInterface[]>
	async getByGroupId(user_id: number, group_id: number, date: Date | null, many: true | undefined, transaction: EntityManager): Promise<UserAnswerInterface[][]>

	@RepositoryModel.bound
	async getByGroupId(
		user_id: number,
		group_id: number,
		date: Date | null = null,
		many: boolean = false,
		transaction?: EntityManager,
	): Promise<any> {
		if (many) {
			return this.query('app.user_answer', 'answer', transaction)
				.addSelect('answer.updated_at')
				.innerJoin('answer.question', 'question', 'question.deleted_at IS NULL')
				.innerJoin('question.questionGroup', 'group', 'group.id = :group_id', { group_id })
				.where(date ? 'answer.user_id = :user_id AND answer.updated_at <= :date' : 'answer.user_id = :user_id', { user_id, date: TimeHelper.queryLessThanEqual(date) })
				.orderBy('answer.question_id', 'ASC')
				.addOrderBy('answer.updated_at', 'DESC')
				.addOrderBy('answer.created_at', 'DESC')
				.getMany()
				.then(this.parser)
				.then((datas: UserAnswerInterface[]) => {
					return Object.values(groupBy(datas, data => data.question_id))
				})
		} else {
			return this.query('app.user_answer', 'answers', transaction)
				.addSelect('answers.updated_at')
				.innerJoin('answers.question', 'question', 'question.deleted_at IS NULL')
				.innerJoin('question.questionGroup', 'group', 'group.id = :group_id', { group_id })
				.innerJoin(query => {
					return query.from('app.user_answer', 'u_a')
						.select(['u_a.question_id as question_id', 'MAX(u_a.id) as id'])
						.where(date ? 'u_a.updated_at <= :date AND u_a.user_id = :user_id' : 'u_a.user_id = :user_id', { user_id, date: TimeHelper.queryLessThanEqual(date) })
						.groupBy('u_a.question_id')
				}, 'answer', 'answer.id = answers.id')
				.orderBy('answers.question_id', 'ASC')
				.getMany()
				.then(this.parser)
		}
	}

	async getByQuestionIds(user_id: number, question_ids: number[], date: Date | null, many: false | undefined, transaction: EntityManager): Promise<UserAnswerInterface[]>
	async getByQuestionIds(user_id: number, question_ids: number[], date: Date | null, many: true | undefined, transaction: EntityManager): Promise<UserAnswerInterface[][]>

	@RepositoryModel.bound
	async getByQuestionIds(
		user_id: number,
		question_ids: number[],
		date: Date | null = null,
		many: boolean = false,
		transaction?: EntityManager,
	) {
		if(many) {
			return this.query('app.user_answer', 'user_answer', transaction)
				.addSelect('user_answer.updated_at')
				.where('user_answer.user_id = :user_id', { user_id })
				.andWhere(date ? 'user_answer.question_id IN (:...question_ids) AND user_answer.updated_at <= :date' : 'user_answer.question_id IN (:...question_ids)', { question_ids, date: TimeHelper.queryLessThanEqual(date) })
				.orderBy('user_answer.created_at', 'DESC')
				.getMany()
				.then(this.parser)
				.then((datas: UserAnswerInterface[]) => {
					return Object.values(groupBy(datas, data => data.question_id))
				})
		} else {
			return this.query('app.user_answer', 'user_answer', transaction)
				.addSelect('user_answer.updated_at')
				.innerJoin(query => {
					return query.from('app.user_answer', 'u_a')
						.select(['u_a.question_id as question_id', 'MAX(u_a.id) as id'])
						.where('u_a.user_id = :user_id', { user_id })
						.andWhere(date ? 'u_a.question_id IN (:...question_ids) AND u_a.updated_at <= :date' : 'u_a.question_id IN (:...question_ids)', { question_ids, date: TimeHelper.queryLessThanEqual(date) })
						.groupBy('u_a.question_id')
				}, 'answer', 'answer.id = user_answer.id')
				.getMany()
				.then(this.parser)
		}
	}

	// ============================= FOR CLUSTERING =============================
	async dataClient(user_id: number, transaction: EntityManager): Promise<{
		id: number,
		first_name: string,
		last_name: string,
		email: string,
		spq: Array<{
			question: string,
			answer: string,
		}>,
	}> {
		return this.queryCustom(`
			select
				u.id,
				up.first_name,
				up.last_name,
				u.email,
				array_agg(json_build_object(
					'question', q.title,
					'answer', (
					case
						when ua.answer::text like '%DROPDOWN%' then (
							case
								when ua.answer::text like '%CHOICES%' then concat(ua.answer ->> 'DROPDOWN'::text, ' ', replace(array_to_string(ARRAY( SELECT json_array_elements(ua.answer -> 'CHOICES') AS json_array_elements), ', '), '"', ''))
								else ua.answer ->> 'DROPDOWN'
							end)
						when ua.answer::text like '%CHOICE%' then replace(array_to_string(ARRAY( SELECT json_array_elements(ua.answer -> 'CHOICE') AS json_array_elements), ', '), '"', '')
						when ua.answer::text like '%TAB%' then (ua.answer ->> 'TAB')::text
						when ua.answer::text like '%BOOLEAN%' then (ua.answer ->> 'BOOLEAN')::text
						when ua.answer::text like '%TEXT_SHORT%' then (ua.answer ->> 'TEXT_SHORT')::text
						when ua.answer::text like '%NUMBER%' then (ua.answer ->> 'NUMBER')::text
						when ua.answer::text like '%SCALE%' then (ARRAY( SELECT json_array_elements(q.metadata -> 'choices'::text) AS json_array_elements))[((ua.answer ->> 'SCALE'::text)::integer) + 1] ->> 'title'::text
						when ua.answer::text like '%TEXT_LONG%' then (ua.answer ->> 'TEXT_LONG')::text
						when ua.answer::text like '%URL%' then (ua.answer ->> 'URL')::text
					end
					)
				)) spq
			from app."user" u
			left join app.user_profile up on up.id = u.user_profile_id
			left join app.user_answer ua on ua.user_id = u.id
			inner join (
				select
					max(_ua.id) id,
					_ua.user_id,
					_ua.question_id
				from app.user_answer _ua
				group by _ua.user_id, _ua.question_id
			) __ua on __ua.id = ua.id and __ua.user_id = u.id
			left join master.question q on q.id = ua.question_id
			where u.id = $1
			group by u.id, up.id
		`, [user_id], transaction)
		.then(this.parser)
		.then(result => result[0]) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
