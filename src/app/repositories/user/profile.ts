import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	UserRecord,
	UserAddressRecord,
	UserDeviceRecord,
	UserProfileRecord,
	UserStylecardRecord,
	StylecardRecord,
} from 'energie/app/records'
import {
	UserInterface,
	UserAddressInterface,
	UserAssetInterface,
	UserProfileInterface,
	LocationInterface,
	UserRoleInterface,
	StylecardInterface,
	UserStylecardInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import { STYLEBOARD_STATUSES, STYLESHEET_STATUSES } from 'energie/utils/constants/enum'
import { TimeHelper } from 'utils/helpers'


@EntityRepository()
export default class UserProfileRepository extends RepositoryModel<{
	UserRecord: UserRecord,
	UserAddressRecord: UserAddressRecord,
	UserDeviceRecord: UserDeviceRecord,
	UserProfileRecord: UserProfileRecord,
	UserStylecardRecord: UserStylecardRecord,
}> {

	static __displayName = 'UserProfileRepository'

	protected records = {
		UserRecord,
		UserAddressRecord,
		UserDeviceRecord,
		UserProfileRecord,
		UserStylecardRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<UserProfileInterface>,
		transaction: EntityManager,
	): Promise<UserProfileInterface> {
		return this.save('UserProfileRecord', data, transaction)
	}

	@RepositoryModel.bound
	async insertRefCode(
		user_profile_id: number,
		referral_code: string,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('UserProfileRecord', user_profile_id, { referral_code }, { transaction })
	}

	@RepositoryModel.bound
	async insertBig(
		datas: UserProfileInterface[],
		transaction: EntityManager,
	): Promise<boolean> {
		return this.saveBig('app', 'user_profile', datas, transaction)
	}

	@RepositoryModel.bound
	async insertAddress(
		user_id: number,
		address: Parameter<UserAddressInterface>,
		transaction: EntityManager,
	): Promise<UserAddressInterface> {
		return this.save('UserAddressRecord', {
			user_id,
			...address,
		}, transaction)
	}

	@RepositoryModel.bound
	async addStylecards(
		user_id: number,
		stylecard_ids: number[],
		transaction: EntityManager,
	) {
		return this.queryInsert('app.user_stylecard', transaction)
			.values(stylecard_ids.map(stylecard_id => {
				return {
					user_id,
					stylecard_id,
				}
			}))
			.execute()
			.then(result => result.raw)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateProfile(user_id: number, profile: Partial<Parameter<UserProfileInterface>>, transaction: EntityManager) {
		return this.query('app.user', 'user', transaction)
			.select(['user.id', 'user.user_profile_id'])
			.where('user.id = :user_id', { user_id })
			.getOne()
			.then(this.parser)
			.then((user: Pick<UserInterface, 'id' | 'user_profile_id'>) => {
				return this.renew('UserProfileRecord', user.user_profile_id, profile, { transaction })
			})
	}

	@RepositoryModel.bound
	async updateProfileByProfileId(user_profile_id: number, profile: Partial<Parameter<UserProfileInterface>>, transaction: EntityManager) {
		return this.renew('UserProfileRecord', user_profile_id, profile, { transaction })
	}

	@RepositoryModel.bound
	async updateAddress(id: number, address: Partial<Parameter<UserAddressInterface>>, transaction: EntityManager) {
		return this.renew('UserAddressRecord', id, address, { transaction })
	}

	// @RepositoryModel.bound
	// async setStylist(user_id: number, stylist_id: number | undefined | boolean, transaction: EntityManager): Promise<number> {
	// 	if (typeof stylist_id === 'number') {
	// 		return this.query('app.user', 'user', transaction)
	// 			.where('user.id = :user_id', { user_id })
	// 			.getOne()
	// 			.then((user: UserInterface) => {
	// 				return this.renew('UserProfileRecord', user.user_profile_id, {
	// 					stylist_id,
	// 				}, { transaction }).then(isUpdated => {
	// 					if(isUpdated) {
	// 						return stylist_id
	// 					} else {
	// 						return null
	// 					}
	// 				})
	// 			})
	// 	} else {
	// 		// Auto set
	// 		return this.query('app.user', 'user', transaction)
	// 			.leftJoinAndSelect('user.profile', 'profile')
	// 			.where('user.id = :user_id', { user_id })
	// 			.getOne()
	// 			.then(async (user: UserInterface & {
	// 				profile: UserProfileInterface,
	// 			}) => {
	// 				// Check if style profile completed or if forced is true
	// 				if (!!stylist_id || user.profile.is_style_profile_completed) {
	// 					return this.query('app.user', 'user', transaction)
	// 						.innerJoin('user.profile', 'profile')
	// 						.loadRelationCountAndMap('user.activeStyleCount', 'user.stylesheets', 'stylesheets', query => {
	// 							return query.from('app.stylesheet', 'stylesheet')
	// 								.where('stylesheet.id = stylesheets.id')
	// 								.andWhere('stylesheet.status IN (:...status)', { status: [STYLESHEET_STATUSES.PENDING, STYLESHEET_STATUSES.STYLING] })
	// 						})
	// 						.loadRelationCountAndMap('user.doneStyleCount', 'user.stylesheets', 'doneStylesheets', query => {
	// 							return query.from('app.stylesheet', 'stylesheet')
	// 								.where('stylesheet.id = doneStylesheets.id')
	// 								.andWhere('stylesheet.status IN (:...status)', { status: [STYLESHEET_STATUSES.PUBLISHED] })
	// 						})
	// 						.loadRelationCountAndMap('user.userCount', 'user.userProfiles', 'profiles', query => {
	// 							return query.from('app.user_profile', 'userProfile')
	// 								.where('userProfile.id = profiles.id')
	// 						})
	// 						.innerJoinAndSelect('user.roles', 'roles', 'roles.role IN (:...stylistOrHead) AND roles.deleted_at IS NULL', { stylistOrHead: ['stylist', 'headstylist'] })
	// 						.innerJoin('user.roles', 'stylist', 'stylist.role = :stylist AND stylist.deleted_at IS NULL', { stylist: 'stylist' })
	// 						.getMany()
	// 						.then(async (users: Array<UserInterface & {
	// 							activeStyleCount: number,
	// 							doneStyleCount: number,
	// 							userCount: number,
	// 							roles: UserRoleInterface[],
	// 						}>) => {
	// 							if (!users.length) {
	// 								return null
	// 							}

	// 							const sorted = sortBy(users.map(u => {
	// 								const isHead = u.roles.map(r => r.role).indexOf('headstylist') > -1
	// 								return {
	// 									id: u.id,
	// 									email: u.email,
	// 									// ===============================================================
	// 									// How to count the value:
	// 									// 120% active style count + 10% done style count + 10% user count
	// 									// If head stylist * 2
	// 									// ===============================================================
	// 									value: ((.8 * u.activeStyleCount) + (.1 * u.doneStyleCount) + (.1 * u.userCount)) * (isHead ? 2 : 1),
	// 									count: `Active: ${ u.activeStyleCount }, Done: ${ u.doneStyleCount }, Users: ${ u.userCount }`,
	// 								}
	// 							}), 'value')

	// 							return sorted.shift().id
	// 						}).then(async id => {
	// 							return this.renew('UserProfileRecord', user.user_profile_id, {
	// 								stylist_id: id,
	// 							}, { transaction }).then(isUpdated => {
	// 								if (isUpdated) {
	// 									return id
	// 								} else {
	// 									return null
	// 								}
	// 							})
	// 						})
	// 				} else {
	// 					return null
	// 				}
	// 			})
	// 	}
	// }

	@RepositoryModel.bound
	async setStylist(user_id: number, stylist_id: number | undefined | boolean, transaction: EntityManager): Promise<number> {
		if (typeof stylist_id === 'number') {
			return this.query('app.user', 'user', transaction)
				.where('user.id = :user_id', { user_id })
				.getOne()
				.then((user: UserInterface) => {
					return this.renew('UserProfileRecord', user.user_profile_id, {
						stylist_id,
					}, { transaction }).then(isUpdated => {
						if (isUpdated) {
							return stylist_id
						} else {
							return null
						}
					})
				})
		} else {
			// Auto set
			return this.query('app.user', 'user', transaction)
				.leftJoinAndSelect('user.profile', 'profile')
				.where('user.id = :user_id', { user_id })
				.getOne()
				.then(async (user: UserInterface & {
					profile: UserProfileInterface,
				}) => {
					// Check if style profile completed or if forced is true
					if (!!stylist_id || user.profile.is_style_profile_completed) {
						return this.query('app.user', 'user', transaction)
							.innerJoin('user.profile', 'profile')
							.loadRelationCountAndMap('user.activeStylesheetCount', 'user.stylesheets', 'stylesheets', query => {
								return query.from('app.stylesheet', 'stylesheet')
									.where('stylesheet.id = stylesheets.id')
									.andWhere('stylesheet.status IN (:...status)', { status: [STYLESHEET_STATUSES.PENDING, STYLESHEET_STATUSES.STYLING] })
							})
							.loadRelationCountAndMap('user.activeStyleboardCount', 'user.styleboards', 'styleboards', query => {
								return query.from('app.styleboard', 'styleboard')
									.where('styleboard.id = styleboards.id')
									.andWhere('styleboard.status IN (:...status)', { status: [STYLEBOARD_STATUSES.PENDING, STYLEBOARD_STATUSES.STYLING] })
							})
							.loadRelationCountAndMap('user.clientCount', 'user.userProfiles', 'clients', query => {
								return query.from('app.user', 'client')
									.leftJoin(UserProfileRecord, 'client_profile', 'client_profile.id = client.user_profile_id')
									.where('client_profile.id = clients.id')
									.andWhere(`client.created_at between (current_date - '7 hours'::interval) and ((current_date + '1 days'::interval) - '7 hours'::interval)`)
							})
							.innerJoinAndSelect('user.roles', 'roles', 'roles.role IN (:...stylistOrHead) AND roles.deleted_at IS NULL', { stylistOrHead: ['stylist', 'headstylist'] })
							.innerJoin('user.roles', 'stylist', 'stylist.role = :stylist AND stylist.deleted_at IS NULL', { stylist: 'stylist' })
							.getMany()
							.then(async (users: Array<UserInterface & {
								activeStylesheetCount: number,
								activeStyleboardCount: number,
								clientCount: number,
								roles: UserRoleInterface[],
							}>) => {
								if (!users.length) {
									return null
								}

								//
								// STYLIST PLACEMENT WORK
								//

								const activeStylist: { [day: number]: string[] } = {
									0: ['amira@helloyuna.io', 'angelin@helloyuna.io'],													// sunday
									1: ['amira@helloyuna.io', 'angelin@helloyuna.io', 'elisabeth@helloyuna.io', 'artika@helloyuna.io'],	// monday
									2: ['angelin@helloyuna.io', 'elisabeth@helloyuna.io', 'artika@helloyuna.io'],						// tuesday
									3: ['amira@helloyuna.io', 'angelin@helloyuna.io', 'elisabeth@helloyuna.io', 'artika@helloyuna.io'],	// wednesday
									4: ['amira@helloyuna.io', 'artika@helloyuna.io'],													// thursday
									5: ['angelin@helloyuna.io', 'amira@helloyuna.io', 'elisabeth@helloyuna.io'],						// friday
									6: ['elisabeth@helloyuna.io', 'artika@helloyuna.io'],
								}

								const today = new Date().getDay()
								const tomorow = today + 1

								const loadCount = users.reduce((i, stylist) => {
									const load = ((stylist.activeStyleboardCount + stylist.activeStylesheetCount) * 2) + stylist.clientCount
									const isHeadStylist = stylist.roles.findIndex(role => role.role === 'headstylist') > -1
									const isActive = new Date().getHours() > 17 ? activeStylist[tomorow].indexOf(stylist.email) > -1 : activeStylist[today].indexOf(stylist.email) > -1
									const old = TimeHelper.moment().diff(stylist.created_at, 'days')

									switch (true) {
										case isHeadStylist:
										case !isActive:
										case old < 7:
											return i
										default:
											return i.concat({
												id: stylist.id,
												email: stylist.email,
												old,
												activeStylesheetCount: stylist.activeStylesheetCount,
												activeStyleboardCount: stylist.activeStyleboardCount,
												clientCount: stylist.clientCount,
												load,
											})
									}
								}, [] as Array<{
									id: number,
									email: string,
									old: number,
									activeStylesheetCount: number,
									activeStyleboardCount: number,
									clientCount: number,
									load: number,
								}>).sort((a, b) => a.load - b.load)

								// console.table(loadCount)

								return loadCount[0].id
							}).then(async id => {
								return this.renew('UserProfileRecord', user.user_profile_id, {
									stylist_id: id,
								}, { transaction }).then(isUpdated => {
									if (isUpdated) {
										return id
									} else {
										return null
									}
								})
							})
					} else {
						return null
					}
				})
		}
	}

	@RepositoryModel.bound
	async viewStylecard(
		user_id: number,
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.user_stylecard', transaction)
			.set({
				seen_at: new Date(),
			})
			.where('user_id = :user_id AND stylecard_id = :stylecard_id', { user_id, stylecard_id })
			.execute()
			.then(r => true)
			.catch(e => {
				this.warn(e)

				throw e
			})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getUserStylecard(
		user_id: number,
		option: {
			random?: boolean,
			get_all_recomendation?: boolean,
		} = {},
		filter: {
			offset?: number,
			limit?: number,
		} = {},
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
		stylecards: Array<StylecardInterface & {
			userStylecard: UserStylecardInterface,
		}>,
	}> {
		const Q = this.query('app.user', 'user', transaction)
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
			.where('user.id = :user_id', { user_id })

		if (option.get_all_recomendation) {
			Q.leftJoin(sq => {
				const q = sq.subQuery()
					.select('*')
					.from(StylecardRecord, 'sc')
					.innerJoin(UserStylecardRecord, 'us', 'us.stylecard_id = sc.id')
					.where('sc.is_recomendation is true')
					.andWhere('sc.cluster_id is not null')

				if (option.random) {
					q.orderBy('random()')
				}

				if (filter.offset >= 0 && filter.limit >= 0) {
					q.offset(filter.offset)
						.limit(filter.limit)
				}

				return q
			}, 'sq_stylecard', '(sq_stylecard.cluster_id != profile.cluster_id) or true')
				.leftJoinAndMapMany('user.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = sq_stylecard.id')
		} else {
			Q.leftJoin(sq => {
				const q = sq.subQuery()
					.select('*')
					.from(StylecardRecord, 'sc')
					.innerJoin(UserStylecardRecord, 'us', 'us.stylecard_id = sc.id')
					.where('us.user_id = :user_id', { user_id })

				if (option.random) {
					q.orderBy('random()')
				} else {
					q.orderBy('us.added_at', 'DESC')
						.addOrderBy('sc.created_at', 'DESC')
				}

				if (filter.offset >= 0 && filter.limit >= 0) {
					q.offset(filter.offset)
						.limit(filter.limit)
				}

				return q
			}, 'sq_stylecard', 'sq_stylecard.user_id = user.id')
				.leftJoinAndMapMany('user.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = sq_stylecard.id')
				.leftJoinAndMapOne('stylecard.userStylecard', UserStylecardRecord, 'userStylecard', 'userStylecard.stylecard_id = stylecard.id')
		}

		return Q.getOne() as any
	}

	@RepositoryModel.bound
	async getAllUser(transaction: EntityManager): Promise<Array<UserInterface & { profile: UserProfileInterface }>> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')
			.orderBy('user.id', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 12,
		filter: {
			user_id?: number,
			search?: string,
			spq_status?: 'complete' | 'incomplete',
			role?: string,
			have_order?: boolean,
			have_stylist?: boolean,
			date?: string,
			eligible_for_order?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<{
		count: number,
		data: Array<UserInterface & {
			profile: UserProfileInterface & {
				stylist: UserInterface & {
					profile: UserProfileInterface,
				},
			}
			roles: UserRoleInterface[],
		}>,
	}> {

		let Q = this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('profile.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndSelect('user.roles', 'userRoles')
			.where('user.deleted_at IS NULL')
			.orderBy('user.created_at', 'DESC')

		if (filter.role !== undefined) {
			Q = Q.innerJoin('user.roles', 'roles', 'roles.role = :role', { role: filter.role })
		}

		if (filter.have_order === true) {
			Q = Q.andWhere(query => {
				return `user.id IN ${query.subQuery()
					.from('app.user', 'user2')
					.select('user2.id')
					.innerJoin('user2.orders', 'orders')
					.getSql()
					}`
			})
		} else if (filter.have_order === false) {
			Q = Q.andWhere(query => {
				return `user.id NOT IN ${query.subQuery()
					.from('app.user', 'user2')
					.select('user2.id')
					.innerJoin('user2.orders', 'orders')
					.getSql()
					}`
			})
		}

		if (filter.spq_status !== undefined) {
			Q = Q.andWhere('profile.is_style_profile_completed = :is_style_profile_completed', { is_style_profile_completed: filter.spq_status === 'complete' ? true : false })
		}

		if (filter.search !== undefined) {
			Q = Q.andWhere(`profile.first_name || ' ' || profile.last_name ILIKE '%${filter.search}%' OR stylistProfile.first_name || ' ' || stylistProfile.last_name ILIKE '%${filter.search}%' OR user.email ILIKE '%${filter.search}%' OR (user.id)::text ILIKE '%${filter.search}%'`)
		}

		if (filter.user_id !== undefined) {
			Q = Q.andWhere('stylist.id = :user_id', { user_id: filter.user_id })
		}

		if (filter.have_stylist === true) {
			Q = Q.andWhere('profile.stylist_id IS NOT NULL')
		} else if (filter.have_stylist === false) {
			Q = Q.andWhere('profile.stylist_id IS NULL')
		}

		if (filter.date !== undefined) {
			Q = Q.andWhere(`user.created_at >= (NOW() - INTERVAL '${filter.date}')`)
		}

		if (filter.eligible_for_order === true) {
			Q = Q.andWhere('user.is_verified = TRUE')
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserAssetByIds(
		user_id: number,
		ids: number[],
		transaction: EntityManager,
	): Promise<UserAssetInterface[]> {
		return this.query('app.user_asset', 'user_asset', transaction)
			.where('user_asset.user_id = :user_id', { user_id })
			.andWhere('user_asset.id IN (:...ids)', { ids })
			.andWhere('user_asset.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCompleteProfile(
		user_id: number,
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
	}> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')
			.addSelect('profile.style_profile_completion_updated_at')
			.addSelect('profile.style_profile_updated_at')
			.where('user.id = :user_id', { user_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserProfile(
		filter: {
			user_id?: number,
			referral_code?: string,
		},
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
	}> {
		let Q = this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')

		if (filter.user_id) {
			Q = Q.where('user.id = :user_id', { user_id: filter.user_id })
		} else if (filter.referral_code) {
			Q = Q.where('LOWER(profile.referral_code) = LOWER(:referral_code)', { referral_code: filter.referral_code })
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCompleteUsersProfile(
		user_id: number,
		transaction: EntityManager,
	): Promise<Array<UserInterface & {
		profile: UserProfileInterface,
	}>> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndSelect('user.profile', 'profile')
			.addSelect('profile.style_profile_completion_updated_at')
			.addSelect('profile.style_profile_updated_at')
			.where('user.id = :user_id', { user_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddresses(user_id: number | undefined, ids: number[] | undefined, transaction: EntityManager): Promise<UserAddressInterface[]> {
		if (ids !== undefined) {
			return this.getMany('UserAddressRecord', user_id ? {
				id: In(ids),
				user_id,
			} : {
				id: In(ids),
			}, {
				transaction,
			})
		} else {
			return this.getMany('UserAddressRecord', {
				user_id,
			}, {
				transaction,
			})
		}
	}

	@RepositoryModel.bound
	async getAddressDeep(address_id: number, transaction: EntityManager): Promise<UserAddressInterface & {
		location: LocationInterface,
	}> {
		return this.query('app.user_address', 'address', transaction)
			.leftJoinAndSelect('address.location', 'location')
			.where('address.id = :address_id', { address_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserWithNoStylist(
		offset: number = 0,
		limit: number = 20,
		transaction: EntityManager,
	): Promise<[
		number,
		Array<UserProfileInterface & {
			user: UserInterface,
		}>
	]> {
		return this.getManyAndCount('UserProfileRecord', {
			stylist_id: null,
		}, {
			relations: ['user'],
			select: ['id'],
			skip: offset,
			take: limit,
			transaction,
		}).then(datas => {
			return {
				data: datas[0],
				count: datas[1],
			}
		}) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
