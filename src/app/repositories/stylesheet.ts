import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	HistoryStylesheetRecord,
	// MatchboxAssetRecord,
	StylesheetRecord,
	StylesheetInventoryRecord,
	StylesheetUserAssetRecord,
	UserAssetRecord,
	VariantAssetRecord,
	BrandRecord,
	CategoryRecord,
	ColorRecord,
	SizeRecord,
	OrderDetailRecord,
	HistoryPurchaseRequestRecord,
	HistoryPurchaseOrderRecord,
	FeedbackRecord,
	ProductRecord,
	HistoryOrderDetailRecord,
	HistoryInventoryRecord,
	VariantRecord,
	VariantColorRecord,
} from 'energie/app/records'

import {
	StylesheetInterface,
	StylesheetUserAssetInterface,
	MatchboxAssetInterface,
	UserProfileInterface,
	MatchboxInterface,
	StylesheetInventoryInterface,
	InventoryInterface,
	PurchaseRequestInterface,
	SizeInterface,
	ColorInterface,
	ProductInterface,
	BrandInterface,
	VariantAssetInterface,
	CategoryInterface,
	UserInterface,
	UserAssetInterface,
	FeedbackInterface,
	OrderDetailInterface,
	HistoryStylesheetInterface,
	PurchaseOrderInterface,
	HistoryPurchaseRequestInterface,
	HistoryPurchaseOrderInterface,
	OrderInterface,
	VariantInterface,
	HistoryOrderDetailInterface,
	HistoryInventoryInterface,
	OrderDetailCouponInterface,
	OrderRequestInterface,
	OrderAddressInterface,
} from 'energie/app/interfaces'

import { STYLESHEET_STATUSES, ORDER_DETAILS, FEEDBACKS, STYLESHEETS, ORDER_DETAIL_STATUSES, PURCHASE_REQUESTS } from 'energie/utils/constants/enum'

import { Parameter } from 'types/common'

import { CommonHelper, TimeHelper } from 'utils/helpers'
// import { ErrorModel } from 'app/models'
// import { ERRORS, ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class StylesheetRepository extends RepositoryModel<{
	HistoryStylesheetRecord: HistoryStylesheetRecord,
	StylesheetRecord: StylesheetRecord,
	StylesheetInventoryRecord: StylesheetInventoryRecord,
	StylesheetUserAssetRecord: StylesheetUserAssetRecord,
}> {

	static __displayName = 'StylesheetRepository'

	protected records = {
		HistoryStylesheetRecord,
		StylesheetRecord,
		StylesheetInventoryRecord,
		StylesheetUserAssetRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<StylesheetInterface>,
		user_asset_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<StylesheetInterface> {
		return this.save('StylesheetRecord', data, transaction).then(async stylesheet => {
			if (user_asset_ids && user_asset_ids.length) {
				await this.save('StylesheetUserAssetRecord', user_asset_ids.map(userAssetId => {
					return {
						stylesheet_id: stylesheet.id,
						user_asset_id: userAssetId,
					} as StylesheetUserAssetInterface
				}), transaction)
			}

			return stylesheet
		})
	}

@RepositoryModel.bound
	async setInventory(data: Parameter<StylesheetInventoryInterface, 'deleted_at'>, transaction: EntityManager): Promise<StylesheetInventoryInterface> {
		return this.save('StylesheetInventoryRecord', data, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async publish(
		changer_user_id: number,
		stylesheet_id: number,
		status: STYLESHEET_STATUSES.PUBLISHED | undefined,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('StylesheetRecord', stylesheet_id, {
			status: status || STYLESHEET_STATUSES.PUBLISHED,
			note,
		}, {
			transaction,
		}).then(isPublished => {
			if(isPublished) {
				this.save('HistoryStylesheetRecord', {
					changer_user_id,
					stylesheet_id,
					status: status || STYLESHEET_STATUSES.PUBLISHED,
					note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async startStyling(
		changer_user_id: number,
		stylesheet_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return await this.renew('StylesheetRecord', stylesheet_id, {
			status: STYLESHEET_STATUSES.STYLING,
		}, {
			transaction,
		}).then(isPublished => {
			if (isPublished) {
				this.save('HistoryStylesheetRecord', {
					changer_user_id,
					stylesheet_id,
					status: STYLESHEET_STATUSES.STYLING,
					note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		stylesheet_id: number,
		update: Partial<Parameter<StylesheetInterface, 'packet_id'>>,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StylesheetRecord', stylesheet_id, CommonHelper.stripKey(update as StylesheetInterface, 'packet_id'), {
			transaction,
		}).then(isPublished => {
			if(isPublished) {
				this.save('HistoryStylesheetRecord', {
					changer_user_id,
					stylesheet_id,
					status: update.status,
					stylist_id: update.stylist_id,
					note: note || update.note,
					lineup: update.lineup,
					stylist_note: update.stylist_note,
				})
			}

			return isPublished
		})
	}

	@RepositoryModel.bound
	async updateInventory(
		stylesheet_inventory_id: number,
		update: Partial<Parameter<StylesheetInventoryInterface>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StylesheetInventoryRecord', stylesheet_inventory_id, update, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updatePacketId(stylesheet_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<StylesheetInterface>('app.stylesheet', transaction)
			.set({ packet_id })
			.where(Array.isArray(stylesheet_id_or_ids) ? 'id IN (:...stylesheet_id_or_ids)' : 'id = :stylesheet_id_or_ids', { stylesheet_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 12,
		filter: {
			type?: STYLESHEETS,
			search?: string,
			status?: STYLESHEET_STATUSES,
			date?: Date,
			user_id?: number,
			have_order?: boolean,
			with_inventories?: boolean,
			with_history?: boolean,
			no_shipment?: boolean,
			without_po?: boolean,
			newest_first?: boolean,
			order_detail_status?: ORDER_DETAIL_STATUSES,
			purchase_request_status?: PURCHASE_REQUESTS,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<StylesheetInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			stylist?: UserInterface & {
				profile: UserProfileInterface,
			},
			matchbox: MatchboxInterface & {
				brand: BrandInterface,
			},
			orderDetail?: OrderDetailInterface & {
				order: OrderInterface,
				history?: HistoryOrderDetailInterface,
			},
			stylesheetInventories?: Array<StylesheetInventoryInterface & {
				inventory?: InventoryInterface & {
					variant: VariantInterface,
					product: ProductInterface,
				},
				purchaseRequest?: PurchaseRequestInterface & {
					purchaseOrder: PurchaseOrderInterface,
				},
				brand: BrandInterface,
				seller: BrandInterface,
				category: CategoryInterface,
				colors: ColorInterface[],
				size: SizeInterface,
				asset?: VariantAssetInterface,
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.leftJoinAndSelect('stylesheet.user', 'user')
			.leftJoinAndSelect('user.profile', 'client_profile')
			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylist_profile')
			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', '(orderDetail.type = :order_detail_type AND orderDetail.ref_id = stylesheet.id)', { order_detail_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('orderDetail.order', 'order')
			.leftJoinAndSelect('matchbox.brand', 'mbrand')
			.where('stylesheet.deleted_at IS NULL')
			.orderBy('stylesheet.id', 'DESC')

		if(filter.type) {
			Q = Q.andWhere('stylesheet.type = :stylesheet_type', { stylesheet_type: filter.type })
		}

		if(filter.search) {
			if(filter.with_inventories) {
				Q = Q.andWhere('(brand.title ILIKE :search OR product.title ILIKE :search OR (stylesheet.id)::text LIKE :search OR stylesheet.title ILIKE :search)', { search: `%${ filter.search }%` })
			} else {
				Q = Q.andWhere('(client_profile.first_name || \' \' || client_profile.last_name ILIKE :search OR stylist_profile.first_name || \' \' || stylist_profile.last_name ILIKE :search OR order.number LIKE :search)', { search: `%${ filter.search }%` })
			}
		}

		if(filter.status) {
			Q = Q.andWhere('stylesheet.status = :status', { status: filter.status })
		}

		if(filter.user_id) {
			Q = Q.andWhere('stylist.id = :user_id', { user_id: filter.user_id })
		}

		if(filter.date) {
			Q = Q.andWhere('orderDetail.shipment_at BETWEEN :start AND :end', {
				start: TimeHelper.moment(filter.date).startOf('d').toDate(),
				end: TimeHelper.moment(filter.date).endOf('d').toDate(),
			})
		}

		if(filter.have_order) {
			Q = Q.andWhere(query => {
				return `EXISTS ${query.subQuery()
						.select('1')
						.from('app.order_detail', 'order_detail')
						.where(`(order_detail.ref_id = stylesheet.id AND order_detail.type = '${ORDER_DETAILS.STYLESHEET}')`)
						.getSql()
					}`
				})
		} else if(filter.have_order === false) {
			Q = Q.andWhere(query => {
					return `NOT EXISTS ${query.subQuery()
						.select('1')
						.from('app.order_detail', 'order_detail')
						.where(`(order_detail.ref_id = stylesheet.id AND order_detail.type = '${ORDER_DETAILS.STYLESHEET}')`)
						.getSql()
					}`
				})
		}

		if(filter.order_detail_status) {
			Q = Q.andWhere('orderDetail.status = :order_detail_status', { order_detail_status: filter.order_detail_status })
		}

		if(filter.with_history) {
			Q = Q.leftJoinAndMapOne('orderDetail.history', query => {
				return query.from(HistoryOrderDetailRecord, 'h_order_detail')
					.orderBy('h_order_detail.created_at', 'DESC')
					.where('h_order_detail.note IS NOT NULL')
			}, 'history', 'history.order_detail_id = orderDetail.id')
		}

		if (filter.no_shipment) {
			Q = Q.leftJoin('orderDetail.orderDetailShipments', 'oDS')
				// .andWhere('orderDetail.status = :order_detail_packed', { order_detail_packed: ORDER_DETAIL_STATUSES.PRIMED })
				.andWhere(query => {
					return `NOT EXISTS ${query.subQuery()
						.select('1')
						.from('app.shipment', 'shipment')
						.where(`(oDS.shipment_id = shipment.id AND shipment.is_facade = FALSE AND shipment.is_return = FALSE)`)
						.getSql()
					}`
				})
		}

		if(filter.with_inventories) {
			Q = Q.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
				.leftJoinAndSelect('sI.inventory', 'inventory')
				.leftJoinAndSelect('sI.purchaseRequest', 'pr')
				.leftJoinAndSelect('pr.purchaseOrder', 'po')
				.leftJoin(VariantRecord, 'variant', 'variant.id = coalesce(inventory.variant_id, pr.variant_id)')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'variantColors', '"variantColors".variant_id = variant.id')
				.leftJoinAndMapOne('inventory.product', ProductRecord, 'product', 'product.id = variant.product_id')
				.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', 'brand.id = coalesce(product.brand_id, pr.brand_id)')
				.leftJoinAndMapOne('sI.seller', BrandRecord, 'seller', 'seller.id = coalesce(inventory.brand_id, pr.seller_id)')
				.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', 'category.id = coalesce(product.category_id, pr.category_id)')
				.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', 'color.id = coalesce("variantColors".color_id, pr.color_id)')
				.leftJoinAndMapOne('sI.size', SizeRecord, 'size', 'size.id = coalesce(variant.size_id, pr.size_id)')
				.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')

			if(filter.purchase_request_status) {
				Q = Q.andWhere(query => {
						return `EXISTS ${query.subQuery()
							.select('1')
							.from('app.stylesheet', '_stylesheet')
							.innerJoin('_stylesheet.stylesheetInventories', '_sI', '_sI.deleted_at IS NULL')
							.innerJoin('_sI.purchaseRequest', '_pR', `_pR.status = '${ filter.purchase_request_status }' AND _pR.deleted_at IS NULL`)
							.where('_stylesheet.id = stylesheet.id')
							.getSql()
						}`
					})
			}

			if(filter.without_po) {
				Q = Q.andWhere(query => {
					return `EXISTS ${query.subQuery()
						.select('1')
						.from('app.stylesheet', '_stylesheet')
						.innerJoin('_stylesheet.stylesheetInventories', '_sI', '_sI.deleted_at IS NULL')
						.innerJoin('_sI.purchaseRequest', '_pR', `(_pR.purchase_order_id IS NULL AND _pR.deleted_at IS NULL)`)
						.where('_stylesheet.id = stylesheet.id')
						.getSql()
						}`
				})
			}
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any
	}

	async get(stylesheet_id: number, deep: false, with_inventories: false, transaction: EntityManager): Promise<StylesheetInterface>
	async get(stylesheet_id: number, deep: false, with_inventories: true, transaction: EntityManager): Promise<StylesheetInterface & { orderDetail?: OrderDetailInterface, stylesheetInventories: StylesheetInventoryInterface[] }>
	async get(stylesheet_id: number, deep: true, with_inventories: false, transaction: EntityManager): Promise<StylesheetInterface & {
		matchbox: MatchboxInterface,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylist?: UserInterface & {
			profile: UserProfileInterface,
		},
		userAssets: UserAssetInterface[],
		orderDetail?: OrderDetailInterface,
		history?: HistoryStylesheetInterface,
	}>
	async get(stylesheet_id: number, deep: true, with_inventories: true, transaction: EntityManager): Promise<StylesheetInterface & {
		matchbox: MatchboxInterface,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylist?: UserInterface & {
			profile: UserProfileInterface,
		},
		userAssets: UserAssetInterface[],
		orderDetail?: OrderDetailInterface,
		stylesheetInventories: StylesheetInventoryInterface[],
		histories?: HistoryStylesheetInterface[],
	}>

	@RepositoryModel.bound
	async get(
		stylesheet_id: number,
		deep?: boolean,
		with_inventories?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		if(deep) {
			const Q = this.query('app.stylesheet', 'stylesheet', transaction)
				.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
				.leftJoinAndSelect('stylesheet.user', 'user')
				.leftJoinAndSelect('user.profile', 'client_profile')
				.leftJoinAndSelect('stylesheet.stylist', 'stylist')
				.leftJoinAndSelect('stylist.profile', 'stylist_profile')
				.leftJoin('stylesheet.stylesheetUserAssets', 'sytlesheetUserAssets')
				.leftJoinAndMapMany('stylesheet.userAssets', UserAssetRecord, 'user_asset', 'user_asset.id = sytlesheetUserAssets.user_asset_id')
				.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.type = :stylesheet_type AND orderDetail.ref_id = stylesheet.id', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
				.leftJoinAndMapMany('stylesheet.histories', HistoryStylesheetRecord, 'histories', 'histories.stylesheet_id = stylesheet.id')
				.where(`stylesheet.id = :stylesheet_id`, { stylesheet_id })
				.addSelect('stylesheet.updated_at')

			if(with_inventories) {
				return Q
					.leftJoinAndSelect('stylesheet.stylesheetInventories', 'stylesheetInventories', 'stylesheetInventories.deleted_at IS NULL')
					.orderBy('stylesheetInventories.created_at', 'ASC')
					.getOne()
					.then(this.parser)
			} else {
				return Q
					.getOne()
					.then(this.parser)
			}
		} else {
			if (with_inventories) {
				return this.query('app.stylesheet', 'stylesheet', transaction)
					.leftJoinAndSelect('stylesheet.stylesheetInventories', 'stylesheetInventories', 'stylesheetInventories.deleted_at IS NULL')
					.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :order_detail_stylesheet', { order_detail_stylesheet: ORDER_DETAILS.STYLESHEET })
					.where(`stylesheet.id = :stylesheet_id`, { stylesheet_id })
					.orderBy('stylesheetInventories.created_at', 'ASC')
					.getOne()
					.then(this.parser)
			} else {
				return this.getOne('StylesheetRecord', {
					id: stylesheet_id,
				}, {
					transaction,
				})
			}
		}
	}

	@RepositoryModel.bound
	async getByUserId(
		user_id: number,
		transaction: EntityManager,
	): Promise<StylesheetInterface[]> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.where('stylesheet.user_id = :user_id', { user_id })
			.andWhere('stylesheet.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddress(
		stylesheet_ids: number[],
		transaction: EntityManager,
	): Promise<Array<StylesheetInterface & {
		orderDetail: OrderDetailInterface & {
			orderRequest: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		},
	}>> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.innerJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :order_detail_stylesheet', { order_detail_stylesheet: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('orderDetail.orderRequest', 'orderRequest')
			.leftJoinAndSelect('orderRequest.address', 'address')
			.where('stylesheet.id IN (:...stylesheet_ids)', { stylesheet_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAsset(
		stylesheet_id: number,
		transaction: EntityManager,
	): Promise<MatchboxAssetInterface> {
		return this.query('app.matchbox_asset', 'asset', transaction)
			.leftJoin('asset.matchbox', 'matchbox')
			.leftJoin('matchbox.stylesheets', 'stylesheets')
			.where('stylesheets.id = :id', { id: stylesheet_id })
			.andWhere('asset.deleted_at IS NULL')
			.orderBy('asset.order', 'ASC')
			.orderBy('asset.updated_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getNotes(
		stylesheet_ids: number[],
		transaction: EntityManager,
	): Promise<Array<StylesheetInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylist: UserInterface & {
			profile: UserProfileInterface & {
				signature: UserAssetInterface,
			},
		},
	}>> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.user', 'user')
			.leftJoinAndSelect('user.profile', 'userProfile')
			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndSelect('stylistProfile.signature', 'signature')
			.where('stylesheet.id IN (:...stylesheet_ids)', { stylesheet_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getInvoices(
		stylesheet_ids: number[],
		transaction: EntityManager,
	): Promise<Array<StylesheetInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylist: UserInterface & {
			profile: UserProfileInterface,
		},
		orderDetail: OrderDetailInterface & {
			orderDetailCoupons: OrderDetailCouponInterface[],
		},
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory: InventoryInterface,
			variant: VariantInterface,
			product: ProductInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			brand: BrandInterface,
		}>,
	}>> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.user', 'user')
			.leftJoinAndSelect('user.profile', 'userProfile')
			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('orderDetail.orderDetailCoupons', 'coupons')
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndMapOne('sI.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapOne('sI.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.where('stylesheet.id IN (:...stylesheet_ids)', { stylesheet_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailedInventoriesFromStylesheet(
		id: number,
		transaction: EntityManager,
	): Promise<StylesheetInterface & {
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory?: InventoryInterface & {
				variant: VariantInterface,
				product: ProductInterface,
			},
			purchaseRequest?: PurchaseRequestInterface,
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset?: VariantAssetInterface,
		}>,
	}> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapOne('inventory.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.where('stylesheet.id = :id', { id })
			.orderBy('sI.created_at', 'ASC')
			.addOrderBy('asset.order', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	async getDetailedInventories(
		stylesheet_inventory_ids: number[],
		with_history: true,
		transaction: EntityManager,
	): Promise<Array<StylesheetInventoryInterface & {
		inventory?: InventoryInterface & {
			history?: HistoryInventoryInterface,
		},
		purchaseRequest?: PurchaseRequestInterface & {
			history?: HistoryPurchaseRequestInterface,
			purchaseOrder?: PurchaseOrderInterface & {
				history?: HistoryPurchaseOrderInterface,
			},
		},
		variant: VariantInterface,
		product: ProductInterface,
		brand: BrandInterface,
		category: CategoryInterface,
		colors: ColorInterface[],
		size: SizeInterface,
		asset?: VariantAssetInterface,
	}>>
	async getDetailedInventories(
		stylesheet_inventory_ids: number[],
		with_history: false | undefined,
		transaction: EntityManager,
	): Promise<Array<StylesheetInventoryInterface & {
		inventory?: InventoryInterface & {
		},
		purchaseRequest?: PurchaseRequestInterface & {
			purchaseOrder?: PurchaseOrderInterface,
		},
		variant: VariantInterface,
		product: ProductInterface,
		brand: BrandInterface,
		category: CategoryInterface,
		colors: ColorInterface[],
		size: SizeInterface,
		asset?: VariantAssetInterface,
	}>>

	@RepositoryModel.bound
	async getDetailedInventories(
		stylesheet_inventory_ids: number[],
		with_history?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		const Q = this.query('app.stylesheet_inventory', 'sI', transaction)
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndSelect('pr.purchaseOrder', 'po')
			.leftJoinAndMapOne('sI.variant', VariantRecord, 'variant', '(CASE WHEN sI.inventory_id IS NOT NULL THEN variant.id = inventory.variant_id ELSE variant.id = pr.variant_id END)')
			.leftJoinAndMapOne('sI.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.where('sI.id IN (:...stylesheet_inventory_ids)', { stylesheet_inventory_ids })
			.orderBy('sI.created_at', 'ASC')
			.addOrderBy('asset.order', 'ASC')

		if(with_history) {
			return Q
				.leftJoinAndMapOne('inventory.history', HistoryInventoryRecord, 'iHistory', 'iHistory.inventory_id = inventory.id AND (iHistory.status)::text = inventory.status::text')
				.leftJoinAndMapOne('pr.history', HistoryPurchaseRequestRecord, 'prHistory', 'prHistory.purchase_request_id = pr.id AND (prHistory.status)::text = pr.status::text')
				.leftJoinAndMapOne('po.history', HistoryPurchaseOrderRecord, 'poHistory', 'poHistory.purchase_order_id = po.id AND (poHistory.status)::text = po.status::text')
				.addOrderBy('iHistory.created_at', 'DESC')
				.addOrderBy('prHistory.created_at', 'DESC')
				.addOrderBy('poHistory.created_at', 'DESC')
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getDetailed(
		stylesheet_id: number,
		transaction: EntityManager,
	): Promise<StylesheetInterface & {
		matchbox: MatchboxInterface & {
			brand: BrandInterface,
		},
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylist: UserInterface & {
			profile: UserProfileInterface & {
				asset: UserAssetInterface,
			},
		},
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			purchaseRequest?: PurchaseRequestInterface & {
				purchaseOrder?: PurchaseOrderInterface,
			},
			inventory?: InventoryInterface,
			variant?: VariantInterface,
			product?: ProductInterface,
			category: CategoryInterface,
			brand: BrandInterface,
			size: SizeInterface,
			asset?: VariantAssetInterface,
			colors: ColorInterface[],
		}>,
		orderDetail?: OrderDetailInterface & {
			order: OrderInterface,
		},
	}> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.leftJoinAndSelect('matchbox.brand', 'matchboxBrand')

			.leftJoinAndSelect('stylesheet.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')

			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndSelect('stylistProfile.asset', 'stylistAsset')

			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndSelect('pr.purchaseOrder', 'po')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndMapOne('sI.variant', VariantRecord, 'variant', '(CASE WHEN sI.inventory_id IS NOT NULL THEN variant.id = inventory.variant_id ELSE variant.id = pr.variant_id END)')
			.leftJoinAndMapOne('sI.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.orderBy('sI.created_at', 'ASC')
			.addOrderBy('asset.order', 'ASC')

			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('orderDetail.order', 'order')

			.addOrderBy('stylesheet.created_at', 'DESC')
			.where('stylesheet.id = :stylesheet_id', { stylesheet_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRefundedInventories(
		stylesheet_id: number,
		transaction: EntityManager,
	): Promise<Array<StylesheetInventoryInterface & {
		purchaseRequest?: PurchaseRequestInterface & {
			purchaseOrder?: PurchaseOrderInterface,
		},
		inventory?: InventoryInterface,
		variant?: VariantInterface,
		product?: ProductInterface,
		category: CategoryInterface,
		brand: BrandInterface,
		size: SizeInterface,
		asset?: VariantAssetInterface,
		colors: ColorInterface[],
	}>> {
		return this.query('app.stylesheet_inventory', 'sI', transaction)
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndSelect('pr.purchaseOrder', 'po')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndMapOne('sI.variant', VariantRecord, 'variant', '(CASE WHEN sI.inventory_id IS NOT NULL THEN variant.id = inventory.variant_id ELSE variant.id = pr.variant_id END)')
			.leftJoinAndMapOne('sI.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.innerJoin('sI.stylesheet', 'stylesheet', 'stylesheet.id = :stylesheet_id', { stylesheet_id })
			.orderBy('sI.created_at', 'ASC')
			.addOrderBy('asset.order', 'ASC')
			.where('sI.is_refunded = TRUE')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getInventoriesFromStylesheet(
		id: number,
		with_order_detail: boolean,
		transaction: EntityManager,
	): Promise<StylesheetInterface & {
		orderDetail?: OrderDetailInterface,
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory?: InventoryInterface,
		}>,
	}> {
		const Q = this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.where('stylesheet.id = :id', { id })
			.orderBy('sI.created_at', 'ASC')

		if(with_order_detail) {
			return Q
				.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'od', 'od.type = :type AND od.ref_id = stylesheet.id', { type: ORDER_DETAILS.STYLESHEET })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getInventoryOfPurchaseRequest(
		purchase_request_id: number,
		transaction: EntityManager,
	): Promise<StylesheetInventoryInterface & {
		stylesheet: StylesheetInterface,
	}> {
		return this.query('app.stylesheet_inventory', 'sI', transaction)
			.leftJoinAndSelect('sI.stylesheet', 'stylesheet')
			.where('sI.purchase_request_id = :purchase_request_id', { purchase_request_id })
			.getOne()
			.then(this.parser) as any
	}

@RepositoryModel.bound
	async getStylesheetInventories(
		stylesheet_inventory_ids: number[],
		transaction: EntityManager,
	): Promise<StylesheetInventoryInterface[]> {
		return this.query('app.stylesheet_inventory', 'sI', transaction)
			.where('sI.id IN (:...stylesheet_inventory_ids)', { stylesheet_inventory_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStyleHistory(
		user_id: number,
		transaction: EntityManager,
	): Promise<Array<StylesheetInterface & {
		matchbox: MatchboxInterface & {
			brand: BrandInterface,
		},
		stylist: UserInterface & {
			profile: UserProfileInterface,
		},
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory: InventoryInterface,
			asset?: VariantAssetInterface,
		}>,
		orderDetail?: OrderDetailInterface & {
			feedback?: FeedbackInterface,
			order: OrderInterface,
		},
	}>> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.leftJoinAndSelect('matchbox.brand', 'brand')

			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'profile')

			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'stylesheetInventories', 'stylesheetInventories.deleted_at IS NULL AND stylesheetInventories.is_locked = FALSE')
			.leftJoinAndSelect('stylesheetInventories.inventory', 'inventories')
			.leftJoinAndMapOne('stylesheetInventories.asset', VariantAssetRecord, 'variant_asset', 'variant_asset.variant_id = inventories.variant_id AND variant_asset.deleted_at IS NULL')

			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndMapOne('orderDetail.feedback', FeedbackRecord, 'feedback', 'feedback.type = :feedback_stylesheet AND feedback.ref_id = stylesheet.id', { feedback_stylesheet: FEEDBACKS.STYLESHEET })
			.leftJoinAndSelect('orderDetail.order', 'order')

			.orderBy('variant_asset.order', 'ASC')
			.addOrderBy('stylesheet.created_at', 'DESC')
			.where('stylesheet.user_id = :user_id AND stylesheet.deleted_at IS NULL', { user_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylistStylesheets(
		stylist_id: number,
		transaction: EntityManager,
	): Promise<Array<StylesheetInterface & {
		matchbox: MatchboxInterface & {
			brand: BrandInterface,
		},
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory?: InventoryInterface & {
				variant: VariantInterface,
				product: ProductInterface,
			},
			purchaseRequest?: PurchaseRequestInterface,
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
			asset?: VariantAssetInterface,
		}>,
		orderDetail?: OrderDetailInterface,
	}>> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.leftJoinAndSelect('matchbox.brand', 'matchboxBrand')

			.leftJoinAndSelect('stylesheet.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')

			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapOne('inventory.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndSelect('sI.purchaseRequest', 'pr')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.leftJoinAndMapOne('sI.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.orderBy('sI.created_at', 'ASC')
			.addOrderBy('asset.order', 'ASC')

			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })

			.addOrderBy('stylesheet.created_at', 'DESC')
			.where('stylesheet.stylist_id = :stylist_id AND stylesheet.deleted_at IS NULL AND stylesheet.status = :status', { stylist_id, status: STYLESHEET_STATUSES.STYLING })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getInventory(stylesheet_inventory_id: number, transaction: EntityManager): Promise<StylesheetInventoryInterface> {
		return this.getOne('StylesheetInventoryRecord', { id: stylesheet_inventory_id }, { transaction })
	}

	@RepositoryModel.bound
	async getDeepPacketId(stylesheet_id: number, transaction: EntityManager): Promise<StylesheetInterface & {
		matchbox: MatchboxInterface,
	}> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.where('stylesheet.id = :stylesheet_id', {stylesheet_id})
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderDetailFrom(stylesheet_id: number, transaction: EntityManager): Promise<OrderDetailInterface> {
		return this.query('app.order_detail', 'detail', transaction)
			.where('detail.ref_id = :stylesheet_id AND detail.type = :stylesheet_type', { stylesheet_id, stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.orderBy('detail.id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(stylesheet_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('StylesheetRecord', {
			id: stylesheet_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	@RepositoryModel.bound
	async getVariantId(
		stylesheet_inventory_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.stylesheet_inventory', 'stylesheetInventories', transaction)
			.leftJoin('stylesheetInventories.inventory', 'inventory')
			.leftJoin('stylesheetInventories.purchaseRequest', 'purchaseRequest')
			.addSelect(['inventory.variant_id', 'purchaseRequest.variant_id'])
			.where('stylesheetInventories.id = :stylesheet_inventory_id', {stylesheet_inventory_id})
			.getOne()
			.then(this.parser)
			.then((stylesheetInventories: StylesheetInventoryInterface & {
				inventory: InventoryInterface,
				purchaseRequest: PurchaseRequestInterface,
			}) => {
				if(stylesheetInventories.inventory_id !== null) {
					return stylesheetInventories.inventory.variant_id
				} else {
					return stylesheetInventories.purchaseRequest.variant_id
				}
			})
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async removeInventory(
		stylesheet_inventory_id: number | undefined,
		transaction: EntityManager,
	) {
		// TODO: cancel purchase request, on manager?
		return await this.renew('StylesheetInventoryRecord', stylesheet_inventory_id, {
			deleted_at: new Date(),
		}, {
			transaction,
		})
	}

	// ============================ PRIVATES ============================

}
