import RepositoryModel, { Keys } from 'app/models/repository'
import { EntityRepository, EntityManager, Brackets, Like } from 'typeorm'

import {
	ProductRecord,
	ProductTagRecord,
	VariantAssetRecord,
	HistoryProductRecord,
	CategoryRecord,
	InventoryRecord,
	StylesheetRecord,
	PromotionVariantRecord,
	VariantRecord,
} from 'energie/app/records'

import {
	BrandInterface,
	CategoryInterface,
	ProductInterface,
	PacketInterface,
	TagInterface,
	VariantInterface,
	SizeInterface,
	VariantAssetInterface,
	ColorInterface,
	VariantColorInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import { INVENTORIES } from 'energie/utils/constants/enum'

import {
	isEmpty,
} from 'lodash'


@EntityRepository()
export default class ProductRepository extends RepositoryModel<{
	HistoryProductRecord: HistoryProductRecord,
	ProductRecord: ProductRecord,
	ProductTagRecord: ProductTagRecord,
}> {

	static __displayName = 'ProductRepository'

	protected records = {
		HistoryProductRecord,
		ProductRecord,
		ProductTagRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		changer_user_id: number,
		data: Parameter<ProductInterface>,
		tag_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<ProductInterface> {
		if (data.spu) {
			data.spu = data.spu.toUpperCase()
		}
		const product = await this.save('ProductRecord', data, transaction)

		await Promise.all([
			tag_ids && tag_ids.length !== 0 ? this.save('ProductTagRecord', tag_ids.map((tag_id): Keys<ProductTagRecord> => {
				return {
					product_id: product.id,
					tag_id,
				}
			}), transaction) : [],
		])

		if (!!product?.id) {
			this.save('HistoryProductRecord', {
				product_id: product.id,
				changer_user_id,
				title: product.title,
				is_published: product.is_published,
				note: 'New Product Created',
			})
		}

		return product
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		product_id: number,
		update: Partial<Parameter<ProductInterface>>,
		tag_ids: number[] | undefined,
		note: string,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('ProductRecord', product_id, update, {
			transaction,
		}).then(async isUpdated => {
			await Promise.all([
				tag_ids ? this.removeTag(product_id, transaction).then(() => {
					if (tag_ids.length) {
						return this.save('ProductTagRecord', tag_ids.map((tag_id): Keys<ProductTagRecord> => {
							return {
								product_id,
								tag_id,
							}
						}), transaction)
					}
				}) : null,
			])

			this.save('HistoryProductRecord', {
				changer_user_id,
				product_id,
				title: update.title,
				is_published: update.is_published,
				note,
			})

			return true
		}).catch(err => {
			this.warn(err)

			return false
		})
	}

	@RepositoryModel.bound
	async updatePacketId(product_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<ProductInterface>('app.product', transaction)
			.set({ packet_id })
			.where(Array.isArray(product_id_or_ids) ? 'id IN (:...product_id_or_ids)' : 'id = :product_id_or_ids', { product_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async updateRemarks(product_id: number, remarks: string, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<ProductInterface>('app.product', transaction)
			.set({ remarks })
			.where('id = :product_id', { product_id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 20,
		filter: {
			product_ids?: string,
			brand_ids?: string,
			category_ids?: string,
			size_ids?: string,
			color_ids?: string,
			price?: string,
			search?: string,
			merchants?: string[],
			available_only?: boolean,
			yuna_product?: boolean,
			discount_only?: boolean,
			blacklist_man?: boolean,
			start_discount?: string,
			recent_only?: boolean,
		} = {},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		internalProductOnly: boolean,
		exclude_zero_inventory: boolean = true,
		include_zero_variant: boolean = true,
		must_published: boolean = true,
		transaction: EntityManager,
	): Promise<{
		data: Array<ProductInterface & {
			brand: BrandInterface,
			category: CategoryInterface,
			variants: Array<VariantInterface & {
				size: SizeInterface,
				asset?: VariantAssetInterface,
				variantColors: Array<VariantColorInterface & {
					color: ColorInterface,
				}>,
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.product', 'product', transaction)
			.innerJoinAndSelect('product.brand', 'brand', 'brand.deleted_at IS NULL')
			.innerJoinAndSelect('product.category', 'category', 'category.deleted_at IS NULL')
			.leftJoinAndSelect(CategoryRecord, 'parentCategory', 'parentCategory.id = category.category_id')
			.where('product.deleted_at IS NULL')

		if (must_published) {
			Q = Q.andWhere('product.is_published = TRUE')
		}

		if (include_zero_variant) {
			Q = Q.leftJoinAndSelect('product.variants', 'variant', 'variant.deleted_at IS NULL')
		} else {
			Q = Q.innerJoinAndSelect('product.variants', 'variant', 'variant.deleted_at IS NULL')
		}

		Q = Q.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at is null')
			.leftJoinAndSelect('variant.variantColors', 'variantColors')
			.leftJoinAndSelect('variantColors.color', 'color')
			.orderBy('variantColors.order', 'ASC')
			.addOrderBy('asset.index', 'ASC')
			.addOrderBy('asset.order', 'ASC')


		if (!isEmpty(filter)) {
			Q = Q.andWhere(new Brackets(query => {
				const filters: string[] = Object.keys(filter).map(key => {
					if (key === 'price') {
						return filter[key].split('-').map((price, i) => {
							return `variant.retail_price ${i === 0 ? '>=' : '<='} ${parseInt(price, 10)}`
						}).join(' AND ')
					} else if (key === 'search') {
						const search = filter.search.split('').map(s => {
							if (s === `'`) {
								s = `''`
							}
							return s
						}).join('')
						return `(product.title ILIKE '%${search}%' OR brand.title ILIKE '%${search}%')`
					} else if (
						key === 'color_ids'
						|| key === 'size_ids'
						|| key === 'brand_ids'
						// || key === 'category_ids'
						|| key === 'product_ids'
					) {
						return `${key.split('_')[0]}.id IN (${filter[key]})`
					} else if (key === 'category_ids') {
						return `(category.id in (${filter[key]}) or category.category_id in (${filter[key]}) or parentCategory.category_id in (${filter[key]}))`
					} else if (key === 'merchants') {
						return `(product.metadata -> 'merchants')::jsonb ?| array[${filter.merchants.map(merchant => `'${merchant}'`).join(',')}]`
					} else if (key === 'available_only') {
						return `variant.is_available is true`
					} else if (key === 'discount_only') {
						return `variant.retail_price > variant.price`
					} else if (key === 'recent_only') {
						return `"product"."updated_at"::DATE-NOW()::DATE >= -14`
					} else if (key === 'blacklist_man') {
						return `category.category_id <> 247`
					} else if (key === 'start_discount') {
						return `((variant.retail_price-variant.price)*100)/NULLIF(variant.retail_price,0) >= ${filter[key]}`
					}
				})

				let SQ = query.where(filters.shift())
				// loop the other
				filters.forEach(f => {
					SQ = SQ.andWhere(f)
				})

				return SQ
			}))
		}

		if (exclude_zero_inventory) {
			Q = Q.innerJoin('variant.inventories', 'inventories', 'inventories.deleted_at IS NULL AND inventories.status = :status', { status: INVENTORIES.AVAILABLE })
		}

		if (internalProductOnly) {
			Q = Q.andWhere(new Brackets(q => {
				return q.where(sq => {
					return 'exists' + sq.subQuery()
						.select('1')
						.from(InventoryRecord, 'inventory')
						.where('inventory.variant_id = variant.id')
						.andWhere('inventory.deleted_at is null')
						.getSql()
				}).orWhere('variant.have_consignment_stock is true')
			}))
		}

		if (!!limit) {
			Q = Q.take(limit)
		}

		if (!!offset) {
			Q = Q.skip(offset)
		}

		if (sort_by.created_at) {
			Q = Q.addOrderBy('product.created_at', sort_by.created_at)
		}

		if (sort_by.updated_at) {
			Q = Q.addOrderBy('product.updated_at', sort_by.updated_at)
		}

		if (sort_by.price) {
			Q = Q.addOrderBy('variant.price', sort_by.price)
		}

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any

	}

	async getDetail(product_id: number, transaction: EntityManager): Promise<ProductInterface>
	async getDetail(product_id: number, deep: true, transaction: EntityManager): Promise<ProductInterface & {
		brand: BrandInterface,
		category: CategoryInterface,
		packet: PacketInterface,
	}>

	@RepositoryModel.bound
	async getDetail(
		product_id: number,
		deepOrTransaction?: true | EntityManager,
		transaction?: EntityManager,
	): Promise<any> {
		if (deepOrTransaction === true) {
			return this.query('app.product', 'product', transaction)
				.leftJoinAndSelect('product.brand', 'brand')
				.leftJoinAndSelect('product.category', 'category')
				.leftJoinAndSelect('product.packet', 'packet')
				.where('product.id = :product_id', { product_id })
				.andWhere('product.deleted_at IS NULL')
				.addSelect('product.updated_at')
				.getOne()
				.then(this.parser) as any
		} else {
			return this.query('app.product', 'product', deepOrTransaction)
				.where('product.id = :product_id', { product_id })
				.andWhere('product.deleted_at IS NULL')
				.addSelect('product.updated_at')
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getPacket(product_id: number, transaction: EntityManager): Promise<PacketInterface> {
		return this.query('app.product', 'product', transaction)
			.where('product.id = :product_id', { product_id })
			.leftJoinAndSelect('product.packet', 'packet')
			.getOne()
			.then(this.parser)
			.then((product: ProductInterface & { packet: PacketInterface }) => product.packet)

	}

	@RepositoryModel.bound
	async getPacketId(product_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('ProductRecord', {
			id: product_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(product => product.packet_id)
	}

	@RepositoryModel.bound
	async getItemSoldCount(
		product_id: number,
		transaction: EntityManager,
	) {
		let Q = this.query('app.order_detail', 'orderDetail', transaction)
			.where('orderDetail.type = :type', { type: 'INVENTORY' })
			.andWhere('orderDetail.status = :status', { status: 'RESOLVED' })
			.leftJoinAndSelect(InventoryRecord, 'inventory', 'inventory.id = orderDetail.ref_id')
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('variant.product', 'product')
			.andWhere('product.id = :product_id', { product_id })

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylesheetItemSoldCount(
		product_id: number,
		transaction: EntityManager,
	) {
		let Q = this.query('app.order_detail', 'orderDetail', transaction)
			.where('orderDetail.type = :type', { type: 'STYLESHEET' })
			.andWhere('orderDetail.status = :status', { status: 'RESOLVED' })
			.leftJoinAndSelect(StylesheetRecord, 'stylesheet', 'stylesheet.id = orderDetail.ref_id')
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'stylesheetInventory')
			.leftJoinAndSelect(InventoryRecord, 'inventory', 'inventory.id = stylesheetInventory.inventory_id')
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('variant.product', 'product')
			.andWhere('product.id = :product_id', { product_id })

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async getInventories(
		product_id: number,
		availailable_only: boolean,
		transaction: EntityManager,
	) {
		let Q = this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('variant.product', 'product')
			.where('product.id = :product_id', { product_id })
			.andWhere('inventory.rack != :consign', { consign: 'consign' })
			.andWhere('inventory.rack != :conisgn', { conisgn: 'conisgn' }) // ada typo penulisan 'consign' di beberapa data

		if (!!availailable_only) {
			Q = Q.andWhere('inventory.deleted_at IS NULL AND inventory.status = :status', { status: INVENTORIES.AVAILABLE })
		}
		
		return Q.getManyAndCount()
		.then(datas => {
			return {
				data: datas[0],
				count: datas[1],
			}
		}).then(this.parser) as any
	}


	@RepositoryModel.bound
	async getPromotion(
		product_id: number,
		transaction: EntityManager,
	) {
		let Q = this.query('app.promotion', 'promotion', transaction)
			.leftJoinAndSelect(PromotionVariantRecord, 'promotionVariant', 'promotionVariant.promotion_id = promotion.id')
			.leftJoinAndSelect(VariantRecord, 'variant', 'variant.id = promotionVariant.variant_id')
			.addSelect("CASE WHEN promotion.published_at > now() then 1 when promotion.published_at < now() AND (promotion.expired_at > now() OR promotion.expired_at IS NULL) then 2 else 3 end", "is_active")
			.where('variant.product_id = :product_id', { product_id })
			.orderBy('is_active', "ASC")
			.addOrderBy('promotion.expired_at', 'DESC')

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async getTags(
		product_id: number,
		transaction: EntityManager,
	): Promise<TagInterface[]> {
		return this.query('master.tag', 'tag', transaction)
			.innerJoin('tag.tagProducts', 'productTags', 'productTags.product_id = :product_id', { product_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByTitle(
		title: string,
		transaction: EntityManager,
	): Promise<ProductRecord> {
		return this.getOne('ProductRecord', {
			title: Like(title),
		}, { transaction })
	}

	@RepositoryModel.bound
	async getByVariantIds(
		variant_ids: number[],
		transaction: EntityManager,
	): Promise<ProductInterface[]> {
		return this.query('app.product', 'product', transaction)
			.innerJoin('product.variants', 'variants', 'variants.id IN (:...variant_ids)', { variant_ids })
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async remove(changer_user_id: number, product_id: number, transaction: EntityManager) {
		return this.renew('ProductRecord', product_id, {
			deleted_at: new Date(),
		}, {
			transaction,
		}).then(() => {
			this.save('HistoryProductRecord', {
				changer_user_id,
				product_id,
				note: 'delete product',
			})
		})
	}

	@RepositoryModel.bound
	async removeTag(
		product_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.query('app.product_tag', 'product_tag', transaction)
			.where('product_id = :product_id', { product_id })
			.delete()
			.execute()
			.then(() => {
				return true
			})
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	// ============================ PRIVATES ============================
}
