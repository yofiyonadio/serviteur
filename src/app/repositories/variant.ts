import RepositoryModel, { Keys } from 'app/models/repository'
import { EntityRepository, EntityManager, SelectQueryBuilder } from 'typeorm'

import {
	VariantAnswerRecord,
	VariantColorRecord,
	VariantMeasurementRecord,
	VariantTagRecord,
	VariantRecord,
	ColorRecord,
	FeedbackRecord,
	TagRecord,
	InventoryRecord,
	ProductRecord,
	BrandRecord,
	VariantAssetRecord,
	CategoryRecord,
	SizeRecord,
	SchedulerPreOrderRecord,
	StylesheetInventoryRecord,
	StyleboardRecord,
	HistoryVariantRecord,
	PromotionVariantRecord,
} from 'energie/app/records'

import {
	VariantInterface,
	PacketInterface,
	ProductInterface,
	VariantColorInterface,
	VariantMeasurementInterface,
	VariantTagInterface,
	VariantAssetInterface,
	InventoryInterface,
	BrandInterface,
	ColorInterface,
	SizeInterface,
	TagInterface,
	CategoryInterface,
	MeasurementInterface,
	FeedbackInterface,
	FeedbackAnswerInterface,
	VariantAnswerInterface,
	StyleboardInterface,
	// StylecardVariantInterface,
} from 'energie/app/interfaces'

import {
	INVENTORIES,
	FEEDBACKS,
	SCHEDULER_STAT,
} from 'energie/utils/constants/enum'
import { Parameter } from 'types/common'

import { CommonHelper } from 'utils/helpers'

import { isEmpty } from 'lodash'
import { ErrorModel } from 'app/models'
import { ERROR_CODES } from 'app/models/error'
import DEFAULTS from 'utils/constants/default'


@EntityRepository()
export default class VariantRepository extends RepositoryModel<{
	VariantColorRecord: VariantColorRecord,
	VariantMeasurementRecord: VariantMeasurementRecord,
	VariantTagRecord: VariantTagRecord,
	VariantAnswerRecord: VariantAnswerRecord,
	VariantRecord: VariantRecord,
	HistoryVariantRecord: HistoryVariantRecord,
}> {

	static __displayName = 'VariantRepository'

	protected records = {
		HistoryVariantRecord,
		VariantColorRecord,
		VariantMeasurementRecord,
		VariantTagRecord,
		VariantAnswerRecord,
		VariantRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		changer_user_id: number,
		data: Parameter<VariantInterface>,
		color_ids: number[],
		tag_ids: number[] | undefined,
		measurements: { [key: string]: string } | undefined,
		transaction: EntityManager,
	): Promise<VariantInterface & {
		variantColors: VariantColorInterface[],
		variantMeasurements: VariantMeasurementInterface[],
		variantTags: VariantTagInterface[],
	}> {
		const variant = await this.save('VariantRecord', data, transaction)

		return Promise.all([
			this.save('VariantColorRecord', color_ids.map((vC, index): Keys<VariantColorRecord> => {
				return {
					variant_id: variant.id,
					color_id: vC,
					order: index,
				}
			}), transaction),
			!isEmpty(measurements) ? this.save('VariantMeasurementRecord', Object.keys(measurements).map((key): object => {
				return {
					variant_id: variant.id,
					measurement_id: parseInt(key, 10),
					value: measurements[key],
				}
			}), transaction) : null,
			!isEmpty(tag_ids) ? this.save('VariantTagRecord', tag_ids.map((tag_id): Keys<VariantTagRecord> => {
				return {
					variant_id: variant.id,
					tag_id,
				}
			}), transaction) : null,
		]).then(([variantColors, variantMeasurements, variantTags]) => {
			this.save('HistoryVariantRecord', {
				changer_user_id,
				variant_id: variant.id,
				price: variant.price,
				retail_price: variant.retail_price,
				is_available: variant.is_available,
				is_published: variant.is_published,
				note: 'add new variant',
			})

			return {
				...variant,
				variantColors,
				variantMeasurements,
				variantTags,
			}
		})
	}

	@RepositoryModel.bound
	async addHistory(
		changer_user_id: number,
		variant_id: number,
		note: string,
	) {
		this.save('HistoryVariantRecord', {
			changer_user_id,
			variant_id,
			note,
		})
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		variant_id: number,
		update: Partial<Parameter<VariantInterface, 'packet_id'>>,
		color_ids: number[] | undefined,
		tag_ids: number[] | undefined,
		measurements: { [key: string]: string } | undefined,
		transaction: EntityManager,
	) {
		return this.renew('VariantRecord', variant_id, CommonHelper.stripKey(update as VariantInterface, 'packet_id'), {
			transaction,
		}).then(async isUpdated => {
			if (isUpdated) {
				await Promise.all([
					color_ids ? this.removeVariantColor(variant_id, transaction).then(() => {
						if (color_ids.length) {
							return this.save('VariantColorRecord', color_ids.map((color, index): Keys<VariantColorRecord> => {
								return {
									variant_id,
									color_id: color,
									order: index,
								}
							}), transaction)
						}
					}) : null,
					!isEmpty(measurements) ? this.removeVariantMeasurement(variant_id, transaction).then(() => {
						return this.save('VariantMeasurementRecord', Object.keys(measurements).map(key => {
							return {
								variant_id,
								measurement_id: parseInt(key, 10),
								value: measurements[key],
							}
						}), transaction)
					}) : null,
					tag_ids ? this.removeVariantTag(variant_id, transaction).then(() => {
						if (tag_ids.length) {
							return this.save('VariantTagRecord', tag_ids.map((tagId): Keys<VariantTagRecord> => {
								return {
									variant_id,
									tag_id: tagId,
								}
							}), transaction)
						}
					}) : null,
				])
			}

			this.save('HistoryVariantRecord', {
				changer_user_id,
				variant_id,
				price: update.price,
				retail_price: update.retail_price,
				is_available: update.is_available,
				is_published: update.is_published,
				note: update.note,
			})

			return isUpdated
		})
	}

	async updateDiscount(
		variantData: string,
		transaction: EntityManager,
	) {
		return this.queryCustom(
			`INSERT INTO app.variant (id, price, product_id, size_id, is_published, is_available, have_consignment_stock, limited_stock)
			 VALUES ${variantData}
			 ON CONFLICT (id)
			 DO
			 	UPDATE 
					SET price = EXCLUDED.price;`,
			undefined, transaction
		)
	}

	@RepositoryModel.bound
	async updateDiscountManual(
		transaction: EntityManager
	) {
		return this.queryCustom(`
			INSERT INTO app.variant (id, price, product_id, size_id, is_published, is_available, have_consignment_stock, limited_stock)
			SELECT DISTINCT ON (t.id)
			id, 
			CASE
				WHEN status = 'ACTIVE' AND TYPE = 'TOTAL'
				THEN t.amount
				WHEN status = 'ACTIVE' AND TYPE = 'PERCENT'
				THEN (t.retail_price - (t.retail_price * t.amount / 100))
				WHEN status = 'EXPIRED'
				THEN t.retail_price
				ELSE t.price
			END AS new_price,
			product_id,
			size_id, is_published, is_available, have_consignment_stock, limited_stock
			FROM (select a.published_at, a.expired_at, a.id promotion_id, b.type, b.amount,
			CASE
				WHEN 
					a.published_at <= now() AND (a.expired_at > now() OR a.expired_at IS NULL)
				THEN 'ACTIVE'
				ELSE 'EXPIRED'
				END AS status,
			c.* FROM app.promotion a
			LEFT JOIN app.promotion_variant b ON b.promotion_id = a.id
			LEFT JOIN app.variant c ON c.id = b.variant_id
			WHERE b.is_active is true
			ORDER BY c.id, status, a.published_at DESC ) t
			ON CONFLICT (id)
			DO
			UPDATE 
			SET price = excluded.price;
		`, undefined, transaction)
	}

	@RepositoryModel.bound
	async updateDiscountById(
		variant_id: number,
		transaction: EntityManager
	) {
		return this.queryCustom(`
			INSERT INTO app.variant (id, price, product_id, size_id, is_published, is_available, have_consignment_stock, limited_stock)
			SELECT DISTINCT ON (t.id)
			id, 
			CASE
				WHEN status = 'ACTIVE' AND TYPE = 'TOTAL'
				THEN t.amount
				WHEN status = 'ACTIVE' AND TYPE = 'PERCENT'
				THEN (t.retail_price - (t.retail_price * t.amount / 100))
				WHEN status = 'EXPIRED'
				THEN t.retail_price
				ELSE t.price
			END AS new_price,
			product_id,
			size_id, is_published, is_available, have_consignment_stock, limited_stock
			FROM (select a.published_at, a.expired_at, a.id promotion_id, b.type, b.amount,
			CASE
				WHEN 
					a.published_at <= now() AND (a.expired_at > now() OR a.expired_at IS NULL)
				THEN 'ACTIVE'
				ELSE 'EXPIRED'
				END AS status,
			c.* FROM app.promotion a
			LEFT JOIN app.promotion_variant b ON b.promotion_id = a.id
			LEFT JOIN app.variant c ON c.id = b.variant_id
			WHERE b.is_active is true AND b.variant_id = ${variant_id}
			ORDER BY c.id, status, a.published_at DESC ) t
			ON CONFLICT (id)
			DO
			UPDATE 
			SET price = excluded.price;
		`, undefined, transaction)
	}

	@RepositoryModel.bound
	async updatePacketId(variant_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<VariantInterface>('app.variant', transaction)
			.set({ packet_id })
			.where(Array.isArray(variant_id_or_ids) ? 'id IN (:...variant_id_or_ids)' : 'id = :variant_id_or_ids', { variant_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async updateAvailable(
		variant_id: number,
		is_available: boolean,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.queryUpdate<VariantInterface>('app.variant', transaction)
			.set({ is_available })
			.where('id = :variant_id', { variant_id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================

	@RepositoryModel.bound
	async getDiscount(operator: string, percent: number, transaction: EntityManager): Promise<VariantInterface[]> {
		return this.queryCustom(`SELECT
		t.product_id,
		t.id "variant_id",
		t2.color_id,
		t.price,
		t.retail_price,
		((t.retail_price-t.price)*100)/NULLIF(t.retail_price,0) "percent",
		json_build_object('title',t3.title,'hex',t3.hex)::json "colors"
		FROM app.variant "t"
		LEFT JOIN app.variant_color "t2"
		ON t.id = t2.variant_id
		LEFT JOIN master.color "t3"
		ON t2.color_id = t3.id
		WHERE t.deleted_at IS NULL
		AND t.is_available = TRUE
		AND ((t.retail_price-t.price)*100)/NULLIF(t.retail_price,0) ${operator} ${percent}
		order BY t.product_id, t.id`, undefined, transaction)
			.then(this.parser) as any
	}


	@RepositoryModel.bound
	async filter(
		search: number | string,
		offset: number = 0,
		limit: number = 20,
		have_inventory: boolean = false,
		transaction: EntityManager,
	): Promise<{
		data: Array<VariantInterface & {
			product: ProductInterface & {
				brand: BrandInterface,
			},
			variantColors: Array<VariantColorInterface & {
				color: ColorInterface,
			}>,
			size: SizeInterface,
			assets: VariantAssetInterface[],
			packet: PacketInterface,
			quantity: number,
			quantity_detail?: {
				available: number,
				total: number,
			},
		}>
		count: number,
	}> {
		const Q = this.queryDeep(this.query('app.variant', 'variant', transaction))
			.leftJoinAndSelect('variant.packet', 'packet')
			.andWhere(`${typeof search === 'string' ? '(brand.title || \' \' || product.title) ILIKE :search' : 'variant.product_id = :search'}`, { search: typeof search === 'string' ? `%${search}%` : search })
			.andWhere('variant.deleted_at is null')
			.skip(offset)
			.take(limit)
		/*
				this.queryDeep(this.query('app.variant', 'variant', transaction))
					.leftJoinAndSelect('variant.packet', 'packet')
					.andWhere(`${typeof search === 'string' ? '(brand.title || \' \' || product.title) ILIKE :search' : 'variant.product_id = :search'}`, { search: typeof search === 'string' ? `%${search}%` : search })
					.andWhere('variant.deleted_at is null')
					.skip(offset)
					.take(limit)
					.leftJoin('variant.inventories', 'inventory', 'inventory.deleted_at IS NULL')
					.addSelect('inventory.id')
					.addSelect('inventory.status')
					.getRawMany()
		*/

		if (have_inventory) {
			return Q
				.innerJoin('variant.inventories', 'inventory', 'inventory.status = :status AND inventory.deleted_at IS NULL', { status: INVENTORIES.AVAILABLE })
				.addSelect('inventory.id')
				.getManyAndCount()
				.then(this.parser)
				.then(datas => {
					return {
						data: datas[0].map((d: any) => {
							return CommonHelper.stripUndefined({
								...d,
								inventories: undefined,
								quantity: d.inventories.length,
							})
						}),
						count: datas[1],
					}
				})
				.catch(err => {
					this.warn(err)

					return []
				}) as any
		} else {
			return Q
				.leftJoin('variant.inventories', 'inventory', 'inventory.deleted_at IS NULL')
				.addSelect('inventory.id')
				.addSelect('inventory.status')
				.getManyAndCount()
				.then(this.parser)
				.then(datas => {

					return {
						data: datas[0].map((d: any) => {
							return CommonHelper.stripUndefined({
								...d,
								inventories: undefined,
								quantity: d.inventories.length,
								quantity_detail: {
									available: d.inventories.filter(i => i.status === INVENTORIES.AVAILABLE).length,
									total: d.inventories.length,
								},
							})
						}),
						count: datas[1],
					}
				})
				.catch(err => {
					this.warn(err)

					return []
				}) as any
		}
	}

	@RepositoryModel.bound
	async variantList(
		offset: number = 0,
		limit: number = 20,
		filter: {
			product_ids?: string,
			brand_ids?: string,
			category_ids?: string,
			size_ids?: string,
			color_ids?: string,
			price?: string,
			search?: string,
			merchants?: string[],
			available_only?: boolean,
			yuna_product?: boolean,
			discount_only?: boolean,
			blacklist_man?: boolean,
		} = {},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		internalProductOnly: boolean,
		exclude_zero_inventory: boolean = true,
		include_zero_variant: boolean = true,
		transaction: EntityManager,
	) {

		const rawQuery = ({ count }: { count: boolean }) => {

			let Q = `select * from (select * from view.mvw_variant_colors AS "v" `

			if (count) {
				Q = `select * from (select DISTINCT ON (v.product_id) count(*) OVER()::INT AS "totals", * from view.mvw_variant_colors AS "v" `
			}

			Q = Q + ` WHERE (`

			const filterss: string[] = Object.keys(filter).map(key => {
				if (key === 'price') {
					return filter[key].split('-').map((price, i) => {
						return `v."retail_price" ${i === 0 ? '>=' : '<='} ${parseInt(price, 10)}`
					}).join(' AND ')
				}

				if (key === 'search') {
					return `(v."var_product_title" ILIKE '%${filter[key]}%' OR v."var_brand_title" ILIKE '%${filter[key]}%')`
				}

				if (key === 'color_ids') {
					return `v."var_color_id" IN (${filter[key]})`
				}

				if (key === 'size_ids') {
					return `v."var_size_id" IN (${filter[key]})`
				}

				if (key === 'brand_ids') {
					return `v."var_brand_id" IN (${filter[key]})`
				}

				if (key === 'product_ids') {
					return `v."var_product_id" IN (${filter[key]})`
				}

				if (key === 'category_ids') {
					return `(v."var_category_id" in (${filter[key]}) or v."var_category_category_id" in (${filter[key]}) or v."var_parentCategory_id" in (${filter[key]}))`
				}

				if (key === 'merchants') {
					return `(v.product->'metadata'->'merchants')::jsonb ?| array[${filter.merchants.map(merchant => `'${merchant}'`).join(',')}]`
				}

				if (key === 'available_only') {
					return `v.is_available is true`
				}

				if (key === 'discount_only') {
					return `v.retail_price > v.price`
				}

				if (key === 'blacklist_man') {
					return `v."var_category_id" <> 247`
				}
			})

			Q = Q + filterss.join(' AND ') + `)`

			if (internalProductOnly) {
				Q = Q + ` AND (exists(SELECT 1 FROM "app"."inventory" "inventory" WHERE "inventory"."variant_id" = "v"."id" AND "inventory"."deleted_at" is not null) OR "v"."have_consignment_stock" is true)`
			}

			Q = Q + ` AND "v"."deleted_at" IS NULL`

			Q = Q + `) "v"`

			Q = Q + ` ORDER BY `

			if (sort_by.created_at) {
				Q = Q + ` "v"."created_at" ${sort_by.created_at}, `
			}

			if (sort_by.updated_at) {
				Q = Q + ` "v"."var_product_updated_at" ${sort_by.updated_at}, `
			}

			if (sort_by.price) {
				Q = Q + ` "v"."price" ${sort_by.price}, `
			}

			Q = Q + ` "v"."var_variantColors_order" ASC, v."var_asset_index" ASC, v."var_asset_order" ASC `

			if (!!offset) {
				Q = Q + ` OFFSET ` + offset
			}

			if (!!limit) {
				Q = Q + ` LIMIT ` + limit
			}

			return Q
		}

		return this.queryCustom(rawQuery({ count: true }), undefined, transaction)
			.then(datas => {
				return {
					data: datas,
					count: !isEmpty(datas[0]) ? datas[0].totals as number : 0,
				}
			}) as any


	}


	@RepositoryModel.bound
	async get<D extends boolean, WQ extends boolean, WA extends boolean, AO extends boolean>(
		variant_id: number,
		{
			detailed,
			with_quantity,
			with_assets,
			available_only,
		}: {
			available_only?: AO,
			detailed?: D,
			with_quantity?: WQ,
			with_assets?: WA,
		},
		transaction: EntityManager,
	): Promise<VariantInterface & (D extends true ? {
		product: ProductInterface,
		brand: BrandInterface,
		category: CategoryInterface,
		colors: ColorInterface[],
		size: SizeInterface,
		inventories: InventoryInterface[],
		po: SchedulerPreOrderRecord,
	} & (WA extends true ? {
		assets: VariantAssetInterface[],
	} : {
		asset?: VariantAssetInterface,
	}) : {}) & (WQ extends true ? {
		quantity: number,
	} : {})> {
		let Q = this.query('app.variant', 'variant', transaction)
			.where('variant.id = :variant_id', { variant_id })

		if (detailed) {
			Q = Q.leftJoinAndSelect('variant.product', 'product')
				.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
				.leftJoinAndMapOne('variant.category', CategoryRecord, 'category', 'category.id = product.category_id')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				.leftJoinAndSelect('variant.size', 'size')
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'variant_asset')
						.orderBy('variant_asset.order', 'ASC')
				}, 'va', 'va.variant_id = variant.id')
				.leftJoinAndSelect('variant.inventories', 'inventory')
				.leftJoinAndMapOne('variant.po', SchedulerPreOrderRecord, 'po', `po.variant_id = variant.id AND po.status = '${SCHEDULER_STAT.RUN}'`)

			if (with_assets) {
				Q = Q.leftJoinAndMapMany('variant.assets', VariantAssetRecord, 'assets', 'assets.id = va.id AND assets.deleted_at IS NULL')
			} else {
				Q = Q.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.id = va.id AND asset.deleted_at IS NULL')
			}
		}

		if (with_quantity) {
			Q = Q.loadRelationCountAndMap('variant.quantity', 'variant.inventories', 'inventory', query => {
				return query.where('inventory.deleted_at IS NULL AND invnetory.status IN (:...status)', {
					status: [INVENTORIES.AVAILABLE],
				})
			})
		}

		if (available_only) {
			Q = Q.andWhere('variant.is_available is true')
		}

		return Q
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getProduct(
		variant_id: number,
		transaction: EntityManager,
	): Promise<ProductInterface> {
		return this.query('app.product', 'product', transaction)
			.innerJoin('product.variants', 'variants', 'variants.id = :variant_id', { variant_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getColorAndSize(
		variant_id: number,
		transaction: EntityManager,
	): Promise<VariantInterface & {
		variantColors: Array<VariantColorInterface & {
			color: ColorInterface,
		}>,
		size: SizeInterface,
	}> {
		return this.query('app.variant', 'variant', transaction)
			.leftJoinAndSelect('variant.variantColors', 'variantColors')
			.leftJoinAndSelect('variantColors.color', 'color')
			.leftJoinAndSelect('variant.size', 'size')
			.where('variant.id = :variant_id', { variant_id })
			.andWhere('variant.deleted_at IS NULL')
			.orderBy('variantColors.order', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBrand(
		variant_id: number,
		transaction: EntityManager,
	): Promise<BrandInterface> {
		return this.query('app.brand', 'brand', transaction)
			.innerJoin('brand.products', 'product')
			.innerJoin('product.variants', 'variant', 'variant.id = :variant_id', { variant_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetail(
		variant_id: number,
		transaction: EntityManager,
	): Promise<VariantInterface & {
		product: ProductInterface & {
			brand: BrandInterface,
			category: CategoryInterface,
		},
		variantColors: Array<VariantColorInterface & {
			color: ColorInterface,
		}>,
		variantMeasurements: VariantMeasurementInterface[],
		size: SizeInterface,
		assets: VariantAssetInterface[],
		po: SchedulerPreOrderRecord,
		quantity: number,
	}> {
		return this.queryDeep(this.query('app.variant', 'variant', transaction))
			.leftJoinAndSelect('product.category', 'category')
			.leftJoinAndSelect('variant.variantMeasurements', 'measurements')
			.leftJoinAndMapOne('variant.po', SchedulerPreOrderRecord, 'po', `po.variant_id = variant.id AND po.status = '${SCHEDULER_STAT.RUN}'`)
			.leftJoinAndMapMany('variant.quantity', InventoryRecord, 'inventory', 'inventory.variant_id = variant.id AND inventory.deleted_at IS NULL AND inventory.status NOT IN (:...status)', { status: [INVENTORIES.BOOKED, INVENTORIES.UNAVAILABLE, INVENTORIES.DEFECT] })
			.addSelect('variant.updated_at')
			.where('variant.id = :variant_id', { variant_id })
			.andWhere('variant.deleted_at IS NULL')
			.andWhere('product.deleted_at IS NULL')
			.andWhere('brand.deleted_at IS NULL')
			.getOne()
			.then(this.parser)
			.then((res: any) => {
				return {
					...res,
					quantity: res.quantity.length,
				}
			})
	}

	@RepositoryModel.bound
	async getDetailFlat(
		variant_id: number,
		transaction: EntityManager,
	): Promise<VariantInterface & {
		product: ProductInterface,
		brand: BrandInterface,
		asset: VariantAssetInterface,
		category: CategoryInterface,
		size: SizeInterface,
		colors: ColorInterface[],
		inventories: InventoryInterface[],
		po: SchedulerPreOrderRecord,
	}> {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.id = :variant_id', { variant_id })
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.category', CategoryRecord, 'category', 'category.id = product.category_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoinAndMapOne('variant.po', SchedulerPreOrderRecord, 'po', `po.variant_id = variant.id AND po.status = '${SCHEDULER_STAT.RUN}'`)
			.leftJoin('variant.variantColors', 'vc')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoinAndSelect('variant.inventories', 'inventories')
			.orderBy('asset.index', 'ASC')
			.addOrderBy('asset.order', 'ASC')
			.addOrderBy('vc.order', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailFlats(
		variant_ids: number[],
		offset: number = 0,
		limit: number = 40,
		filter: {
			price: string,
		},
		transaction: EntityManager,
		operator?: string,
		percent?: number,
	): Promise<Array<VariantInterface & {
		product: ProductInterface,
		brand: BrandInterface,
		asset: VariantAssetInterface,
		category: CategoryInterface,
		category_type: CategoryInterface,
		size: SizeInterface,
		colors: ColorInterface[],
		inventories: InventoryInterface[],
	}> | {
		data: Array<VariantInterface & {
			product: ProductInterface,
			brand: BrandInterface,
			asset: VariantAssetInterface,
			category: CategoryInterface,
			category_type: CategoryInterface,
			size: SizeInterface,
			colors: ColorInterface[],
			inventories: InventoryInterface[],
		}>,
		count: number,
	}> {
		let Q = this.query('app.variant', 'variant', transaction)
			.distinctOn(['variant.id'])
			.addSelect(`'tes counter'`, 'countersss')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.category', CategoryRecord, 'category', 'category.id = product.category_id')
			.leftJoinAndMapOne('variant.category_type', CategoryRecord, 'category_from', 'category_from.id = category.category_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin('variant.variantColors', 'vc')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoinAndSelect('variant.inventories', 'inventories')
			.addOrderBy('variant.id', 'ASC')
		// .addOrderBy('vc.order', 'ASC')

		variant_ids.length > 0 ? Q.where('variant.id in (:...variant_ids)', { variant_ids }) : Q.where('')

		if (operator && percent >= 0) {
			Q.andWhere(`((variant.retail_price-variant.price)*100)/NULLIF(variant.retail_price,0) ${operator} ${percent}`)
		}

		Q = Q.addOrderBy('asset.order', 'ASC')

		if (filter.price === 'asc') {
			Q = Q.addOrderBy('variant.price', 'ASC')
		} else if (filter.price === 'desc') {
			Q = Q.addOrderBy('variant.price', 'DESC')
		}

		if (!!offset) {
			Q = Q.skip(offset)
		}

		if (!!limit) {
			Q = Q.take(limit)
		}

		if (operator && percent >= 0) {
			return Q
				.getManyAndCount()
				.then(datas => {
					return {
						data: datas[0],
						count: datas[1],
					}
				})
				.then(this.parser) as any
		}

		return Q
			.getMany()
			.then(this.parser) as any

	}


	@RepositoryModel.bound
	async getGroupedTags(
		variant_id: number,
		transaction: EntityManager,
	): Promise<Array<TagInterface & {
		tags: TagInterface[],
	}>> {
		return this.query('master.tag', 'tag', transaction)
			.innerJoinAndSelect('tag.tags', 'tags')
			.leftJoin('tags.tagVariants', 'variantTag')
			.leftJoin('tags.tagProducts', 'productTag')
			.where('variantTag.variant_id = :variant_id', { variant_id })
			.orWhere(query => {
				return `productTag.product_id IN ${query.subQuery().from('app.variant', 'variant')
					.select('variant.product_id')
					.where('variant.id = :variant_id', { variant_id })
					.getQuery()
					}`
			})
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAvailableInventoryCount(variant_id: number, transaction: EntityManager) {
		return this.query('app.inventory', 'inventory', transaction)
			.where('inventory.variant_id = :variant_id', { variant_id })
			.andWhere('inventory.deleted_at IS NULL')
			.andWhere('inventory.status = :available', { available: INVENTORIES.AVAILABLE })
			.getCount()
	}

	async getInventories(variant_id: number, offset: number, limit: number, transaction: EntityManager): Promise<{
		data: InventoryInterface[],
		count: number,
	}>
	async getInventories(variant_id: number, offset: number, limit: number, type: 'id', transaction: EntityManager): Promise<{
		data: number[],
		count: number,
	}>

	@RepositoryModel.bound
	async getInventories(
		variant_id: number,
		offset: number,
		limit: number,
		typeOrTransaction?: 'id' | EntityManager,
		transaction?: EntityManager,
	): Promise<any> {

		const trx = typeOrTransaction === 'id' ? transaction : typeOrTransaction

		const Q = this.query('app.inventory', 'inventory', trx)
			.where('inventory.variant_id = :variant_id', { variant_id })
			.andWhere('inventory.deleted_at IS NULL')
			.orderBy('inventory.status', 'ASC')
			// ASC, we want to list old inventories first
			.addOrderBy('inventory.updated_at', 'ASC')
			.skip(offset)
			.take(limit)

		if (typeOrTransaction === 'id') {
			return Q.select('inventory.id')
				.getManyAndCount()
				.then(async datas => {
					return {
						data: datas[0].map((i: InventoryInterface) => i.id),
						count: datas[1],
					}
				}).then(this.parser) as any
		} else {
			return Q
				.getManyAndCount()
				.then(async datas => {
					return {
						data: datas[0],
						count: datas[1],
					}
				}).then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getMeasurements(
		variant_id: number,
		transaction: EntityManager,
	): Promise<Array<MeasurementInterface & {
		measurementVariants: VariantMeasurementInterface[],
	}>> {
		return this.query('master.measurement', 'measurement', transaction)
			.innerJoinAndSelect('measurement.measurementVariants', 'measurementVariant', 'measurementVariant.variant_id = :variant_id', { variant_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(variant_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('VariantRecord', {
			id: variant_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(variant => variant.packet_id)
	}

	@RepositoryModel.bound
	async getTags(
		variant_id: number,
		transaction: EntityManager,
	): Promise<TagInterface[]> {
		return this.query('master.tag', 'tag', transaction)
			.innerJoin('tag.tagVariants', 'variantTag', 'variantTag.variant_id = :variant_id', { variant_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getProductDetail(
		user_id: number,
		variant_id: number,
		order_detail_id: number | undefined,
		stylesheet_inventory_id: number | undefined,
		transaction: EntityManager,
	): Promise<ProductInterface & {
		brand: BrandInterface,
		tags: TagInterface[],
		category: CategoryInterface,
		variants: Array<VariantInterface & {
			tags: TagInterface[],
			variantMeasurements: Array<VariantMeasurementInterface & {
				measurement: MeasurementInterface,
			}>,
			size: SizeInterface,
			assets: VariantAssetInterface[],
			colors: ColorInterface[],
			inventories: InventoryInterface[],
			feedback?: FeedbackInterface & {
				answers: FeedbackAnswerInterface[],
			},
		}>,
	}> {
		let Q = this.query('app.product', 'product', transaction)
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndSelect('product.category', 'category')
			.leftJoin('product.productTags', 'productTag')
			.leftJoinAndMapMany('product.productTags', TagRecord, 'product_tag', 'product_tag.id = productTag.tag_id')
			.leftJoinAndSelect('product.variants', 'variant', 'variant.is_published IS TRUE and variant.deleted_at IS NULL')
			.leftJoinAndSelect('variant.assets', 'assets')
			.leftJoin('variant.variantTags', 'variantTag')
			.leftJoinAndMapMany('variant.tags', TagRecord, 'variant_tag', 'variant_tag.id = variantTag.tag_id')
			.leftJoinAndSelect('variant.variantMeasurements', 'variantMeasurements')
			.leftJoinAndSelect('variantMeasurements.measurement', 'measurement')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndSelect('variant.inventories', 'inventory', 'inventory.deleted_at IS NULL')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = variantColors.color_id')
			.where(sq => {
				return 'product.id = ' + sq.subQuery()
					.select('v.product_id')
					.from('app.variant', 'v')
					.where(`v.id = ${variant_id}`)
					.getSql()
			})
			.andWhere('variant.is_published is true and variant.deleted_at is null')
			.andWhere('inventory.deleted_at is null')

		if (order_detail_id) {
			Q = Q
				.leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', 'feedback.ref_id = variant.id and feedback.type = :type and feedback.user_id = :user_id and feedback.order_detail_id = :order_detail_id', { type: FEEDBACKS.VARIANT, user_id, order_detail_id })
				.leftJoinAndSelect('feedback.answers', 'answers')
		}

		if (stylesheet_inventory_id) {
			return Q
				.leftJoinAndMapOne('product.sI', StylesheetInventoryRecord, 'sI', 'sI.id = :stylesheet_inventory_id', { stylesheet_inventory_id })
				.leftJoin('sI.purchaseRequest', 'pR')
				.leftJoin('sI.inventory', 'inv')
				.andWhere('inv.variant_id = :variant_id OR pR.variant_id = :variant_id', { variant_id })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getProductDetailFromVariant(
		variant_id: number,
		user_id: number | undefined,
		styleboard_id: number | undefined,
		transaction: EntityManager,
	): Promise<ProductInterface & {
		brand: BrandInterface,
		category: CategoryInterface,
		category_type: CategoryInterface,
		variants: Array<VariantInterface & {
			size: SizeInterface,
			colors: ColorInterface[],
			assets: VariantAssetInterface[],
			inventories: InventoryInterface[],
			variantMeasurements: Array<VariantMeasurementInterface & {
				measurement: MeasurementInterface,
			}>,
			tags: TagInterface[],
			answer?: VariantAnswerInterface,
			po?: SchedulerPreOrderRecord,
		}>,
		styleboard?: StyleboardInterface,
		// stylesheetInventory?: StylesheetInventoryInterface,
		// stylecardVariant?: StylecardVariantInterface,
	}> {
		let Q = this.query('app.variant', 'v', transaction)
			.leftJoinAndSelect('v.product', 'product')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndSelect('product.category', 'category')
			.leftJoinAndMapOne('product.category_type', CategoryRecord, 'category_from', 'category_from.id = category.category_id')
			.leftJoinAndSelect('product.variants', 'variant', 'variant.deleted_at IS NULL')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = variantColors.color_id')
			.leftJoinAndSelect('variant.assets', 'assets', 'assets.deleted_at IS NULL')
			.leftJoinAndSelect('variant.inventories', 'inventory', 'inventory.deleted_at IS NULL AND inventory.status = :available', { available: INVENTORIES.AVAILABLE })
			.leftJoinAndSelect('variant.variantMeasurements', 'variantMeasurements')
			.leftJoinAndSelect('variantMeasurements.measurement', 'measurement')
			.leftJoin('variant.variantTags', 'vt')
			.leftJoinAndMapMany('variant.tags', TagRecord, 'tags', 'tags.id = vt.tag_id')
			.leftJoinAndMapOne('variant.po', SchedulerPreOrderRecord, 'po', `po.variant_id = variant.id AND po.status = '${SCHEDULER_STAT.RUN}'`)
			.where('v.id = :variant_id', { variant_id })
			.orderBy('size.id')
			.addOrderBy('variantColors.order')
			.addOrderBy('assets.index')
			.addOrderBy('assets.order')
			.addOrderBy('measurement.id')

		if (user_id) {
			Q = Q.leftJoinAndMapOne('variant.answer', VariantAnswerRecord, 'answer', 'answer.variant_id = variant.id AND answer.user_id = :user_id AND answer.question_id = :question_id', {
				user_id,
				question_id: DEFAULTS.VARIANT_FEEDBACK_QUESTION_ID,
			}).addOrderBy('answer.created_at', 'DESC')
		}

		if (styleboard_id) {
			Q = Q.leftJoinAndMapOne('product.styleboard', StyleboardRecord, 'styleboard', 'styleboard.id = :styleboard_id', { styleboard_id })
		}

		return Q
			.getOne()
			.then(this.parser)
			.then((v: any) => v.product) as any
	}

	@RepositoryModel.bound
	async getAnswers(variant_id: number, question_id: number | undefined, user_id: number | undefined, transaction: EntityManager): Promise<VariantAnswerInterface[]> {
		let Q = this.query('app.variant_answer', 'answer', transaction)
			.where('answer.variant_id = :variant_id', { variant_id })
			.orderBy('answer.created_at', 'DESC')

		if (question_id) {
			Q = Q.andWhere('answer.question_id = :question_id', { question_id })
		}

		if (user_id) {
			Q = Q.andWhere('answer.user_id = :user_id', { user_id })
		}

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLatestAnswer(variant_id: number, question_id: number, user_id: number | undefined, transaction: EntityManager): Promise<VariantAnswerInterface> {
		const Q = this.query('app.variant_answer', 'answer', transaction)
			.where('answer.variant_id = :variant_id and answer.question_id = :question_id', { variant_id, question_id })
			.orderBy('answer.created_at', 'DESC')

		if (user_id) {
			return Q.andWhere('answer.user_id = :user_id', { user_id })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getByProductId(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.product_id = :product_id', { product_id })
			.andWhere('variant.deleted_at IS NULL')
			.getManyAndCount()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByPromotionId(
		promotion_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.deleted_at IS NULL')
			.leftJoinAndMapMany('variant.promotion_variant', PromotionVariantRecord, 'promotionVariant', 'promotionVariant.variant_id = variant.id')
			.andWhere('promotionVariant.promotion_id = :promotion_id', { promotion_id })
			.getManyAndCount()
			.then(this.parser) as any
	}

	// ============================= METHODS ============================
	@RepositoryModel.bound
	async addAnswer(variant_id: number, question_id: number, user_id: number, answer: object, transaction: EntityManager) {
		return this.getLatestAnswer(variant_id, question_id, user_id, transaction).then(variantAnswer => {
			if (+variantAnswer.updated_at + (1000 * 60 * 60) > Date.now()) {
				// modify
				return this.renew('VariantAnswerRecord', variantAnswer.id, {
					answer,
				})
			} else {
				// add
				return this.save('VariantAnswerRecord', {
					variant_id,
					question_id,
					user_id,
					answer,
				}, transaction).then(() => {
					return true
				})
			}
		}).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.save('VariantAnswerRecord', {
					variant_id,
					question_id,
					user_id,
					answer,
				}, transaction).then(() => {
					return true
				})
			}

			throw err
		})
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async remove(changer_user_id: number, variant_id: number, transaction: EntityManager) {
		return this.renew('VariantRecord', variant_id, {
			deleted_at: new Date(),
		}, {
			transaction,
		}).then(() => {
			this.save('HistoryVariantRecord', {
				changer_user_id,
				variant_id,
				note: 'delete variant',
			})
		})
	}

	@RepositoryModel.bound
	async removeVariantColor(variant_id: number, transaction: EntityManager) {
		return this.query('app.variant_color', 'variant_color', transaction)
			.where('variant_id = :variant_id', { variant_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async removeVariantMeasurement(variant_id: number, transaction: EntityManager) {
		return this.query('app.variant_measurement', 'variant_measurement', transaction)
			.where('variant_id = :variant_id', { variant_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async removeVariantTag(variant_id: number, transaction: EntityManager) {
		return this.query('app.variant_tag', 'variant_tag', transaction)
			.where('variant_id = :variant_id', { variant_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async removeAnswer(variant_id: number, question_id: number, user_id: number, transaction: EntityManager) {
		return this.query('app.variant_answer', 'answer', transaction)
			.where('variant_id = :variant_id AND question_id = :question_id AND user_id = :user_id', { variant_id, question_id, user_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	// ============================ PRIVATES ============================
	private queryDeep(query: SelectQueryBuilder<{}>) {
		return query
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndSelect('variant.variantColors', 'variantColors')
			.leftJoinAndSelect('variantColors.color', 'colors')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndMapMany('variant.assets', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id and asset.deleted_at is null')
			.orderBy('variantColors.order', 'ASC')
			.addOrderBy('asset.index', 'ASC')
			.addOrderBy('asset.order', 'ASC')
	}

}
