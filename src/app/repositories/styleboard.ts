import {
	// ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	StyleboardRecord,
	StyleboardStylecardRecord,
	HistoryStyleboardRecord,
	ProductRecord,
	BrandRecord,
	CategoryRecord,
	ColorRecord,
	SizeRecord,
	VariantAssetRecord,
	OrderDetailRecord,
	OrderRequestRecord,
	HistoryOrderDetailRecord,
	FeedbackRecord,
	FeedbackAnswerRecord,
	VariantColorRecord,
	VariantAnswerRecord,
	StyleboardUserAssetRecord,
	StylecardRecord,
	UserAssetRecord,
	InventoryRecord,
	OrderRecord,
	UserRecord,
	UserProfileRecord,
	UserRequestRecord,
} from 'energie/app/records'

import {
	StyleboardInterface,
	VariantInterface,
	VariantAssetInterface,
	SizeInterface,
	ProductInterface,
	BrandInterface,
	CategoryInterface,
	ColorInterface,
	ServiceInterface,
	OrderDetailInterface,
	OrderRequestInterface,
	HistoryOrderDetailInterface,
	UserInterface,
	UserProfileInterface,
	UserAssetInterface,
	FeedbackInterface,
	FeedbackAnswerInterface,
	OrderInterface,
	VariantAnswerInterface,
	StyleboardUserAssetInterface,
	StylecardInterface,
	StylecardVariantInterface,
	InventoryInterface,
	HistoryStyleboardInterface,
	UserRequestInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'
import { CommonHelper, TimeHelper } from 'utils/helpers'

import { ORDER_DETAILS, ORDER_DETAIL_STATUSES, FEEDBACKS, STYLEBOARD_STATUSES, STYLECARD_STATUSES, ORDER_REQUESTS } from 'energie/utils/constants/enum'
import DEFAULTS from 'utils/constants/default'


@EntityRepository()
export default class StyleboardRepository extends RepositoryModel<{
	StyleboardRecord: StyleboardRecord,
	StyleboardStylecardRecord: StyleboardStylecardRecord,
	StyleboardUserAssetRecord: StyleboardUserAssetRecord,
	HistoryStyleboardRecord: HistoryStyleboardRecord,
}> {

	static __displayName = 'StyleboardRepository'

	protected records = {
		StyleboardRecord,
		StyleboardStylecardRecord,
		StyleboardUserAssetRecord,
		HistoryStyleboardRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<StyleboardInterface>,
		stylecard_ids: number[],
		user_asset_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		return this.save('StyleboardRecord', data, transaction).then(async styleboard => {
			await Promise.all(stylecard_ids.map(async (stylecardId, order) => {
				return this.save('StyleboardStylecardRecord', {
					styleboard_id: styleboard.id,
					stylecard_id: stylecardId,
					order,
				}, transaction)
			}))

			if (user_asset_ids && user_asset_ids.length) {
				await this.save('StyleboardUserAssetRecord', user_asset_ids.map(userAssetId => {
					return {
						styleboard_id: styleboard.id,
						user_asset_id: userAssetId,
					} as StyleboardUserAssetInterface
				}), transaction)
			}

			return styleboard
		})
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		styleboard_id: number,
		update: Partial<StyleboardInterface>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('StyleboardRecord', styleboard_id, update, { transaction })
			.then(isPublished => {
				if(isPublished) {
					this.save('HistoryStyleboardRecord', {
						changer_user_id,
						styleboard_id,
						status: update.status,
						stylist_id: update.stylist_id,
						note: note || update.note,
						stylist_note: update.stylist_note,
					})
				}

				return isPublished
			})
	}

	@RepositoryModel.bound
	async updateStylboardUserAsset(
		styleboard_id: number,
		user_asset_ids: number[],
		transaction: EntityManager,
	) {
		return await this.queryDelete('app.styleboard_user_asset', transaction)
			.where('styleboard_id = :styleboard_id', {styleboard_id})
			.execute()
			.then(async () => {
				if(user_asset_ids.length > 0) {
					return await this.save('StyleboardUserAssetRecord', user_asset_ids.map(userAssetId => {
						return {
							styleboard_id,
							user_asset_id: userAssetId,
						} as StyleboardUserAssetInterface
					}), transaction)
				} else {
					return true
				}
			})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter<WV extends boolean, WH extends boolean>(
		offset: number = 0,
		limit: number = 12,
		filter: {
			search?: string,
			status?: STYLEBOARD_STATUSES,
			date?: Date,
			user_id?: number,
			have_order?: boolean,
			no_shipment?: boolean,
			newest_first?: boolean,
			order_detail_status?: ORDER_DETAIL_STATUSES,
			stylecard_status?: STYLECARD_STATUSES,
			with_variant?: WV,
			with_history?: WH,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<StyleboardInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			stylist?: UserInterface & {
				profile: UserProfileInterface,
			},
			service: ServiceInterface & {
				brand: BrandInterface,
			},
			order?: OrderInterface & {
				orderDetail: OrderDetailInterface,
			} & (WH extends true ? {
				history?: HistoryOrderDetailInterface,
			} : {}),
			// orderDetail?: OrderDetailInterface & {
			// 	order: OrderInterface,
			// } & (WH extends true ? {
			// 	history?: HistoryOrderDetailInterface,
			// } : {}),
		} & (WV extends true ? {
			stylecards: Array<StylecardInterface & {
				stylecardVariants: Array<StylecardVariantInterface & {
					variant: VariantInterface,
					product: ProductInterface,
					brand: BrandInterface,
					category: CategoryInterface,
					colors: ColorInterface[],
					size: SizeInterface,
					asset?: VariantAssetInterface,
				}>,
			}>,
		} : {})>,
		count: number,
	}> {
		let Q = this.query('app.styleboard', 'styleboard', transaction)
			.leftJoinAndSelect('styleboard.service', 'service')
			.leftJoinAndSelect('styleboard.user', 'user')
			.leftJoinAndSelect('user.profile', 'client_profile')
			.leftJoinAndSelect('styleboard.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylist_profile')
			.leftJoin(OrderRequestRecord, 'orderRequest', `(orderRequest.metadata ->> 'styleboard_id')::int = styleboard.id`)
			.leftJoin(OrderDetailRecord, 'orderDetail', `(orderDetail.type = 'STYLEBOARD' and orderDetail.ref_id = styleboard.id)`)
			// .leftJoinAndMapOne('styleboard.orderDetail', OrderDetailRecord, 'orderDetail', '(orderDetail.type = :order_detail_type AND orderDetail.ref_id = styleboard.id)', { order_detail_type: ORDER_DETAILS.STYLEBOARD })
			.leftJoinAndMapOne('styleboard.order', OrderRecord, 'order', `order.id = coalesce(orderRequest.order_id, orderDetail.order_id)`)
			.leftJoinAndSelect('service.brand', 'sbrand')
			.where('TRUE')
			.orderBy('styleboard.id', 'DESC')

		if(filter.search) {
			if(filter.with_variant) {
				Q = Q.andWhere('(brand.title ILIKE :search OR product.title ILIKE :search OR (styleboard.id)::text LIKE :search)', { search: `%${filter.search}%` })
			} else {
				Q = Q.andWhere('(client_profile.first_name || \' \' || client_profile.last_name ILIKE :search OR stylist_profile.first_name || \' \' || stylist_profile.last_name ILIKE :search OR order.number LIKE :search OR (styleboard.id)::text LIKE :search)', { search: `%${filter.search}%` })
			}
		}

		if(filter.status) {
			Q = Q.andWhere('styleboard.status = :status', { status: filter.status })
		}

		if(filter.user_id) {
			Q = Q.andWhere('stylist.id = :user_id', { user_id: filter.user_id })
		}

		if(filter.date) {
			Q = Q.andWhere('orderDetail.shipment_at BETWEEN :start AND :end', {
				start: TimeHelper.moment(filter.date).startOf('d').toDate(),
				end: TimeHelper.moment(filter.date).endOf('d').toDate(),
			})
		}

		if(filter.have_order) {
			Q = Q.andWhere(query => {
				return `EXISTS ${query.subQuery()
						.select('1')
						.from('app.order_detail', 'order_detail')
						.where(`(order_detail.ref_id = styleboard.id AND order_detail.type = '${ORDER_DETAILS.STYLEBOARD}')`)
						.getSql()
					}`
				})
		} else if(filter.have_order === false) {
			Q = Q.andWhere(query => {
					return `NOT EXISTS ${query.subQuery()
						.select('1')
						.from('app.order_detail', 'order_detail')
						.where(`(order_detail.ref_id = styleboard.id AND order_detail.type = '${ORDER_DETAILS.STYLEBOARD}')`)
						.getSql()
					}`
				})
		}

		if(filter.order_detail_status) {
			Q = Q.andWhere('orderDetail.status = :order_detail_status', { order_detail_status: filter.order_detail_status })
		}

		if(filter.with_history) {
			Q = Q.leftJoinAndMapOne('orderDetail.history', query => {
				return query.from(HistoryOrderDetailRecord, 'h_order_detail')
					.orderBy('h_order_detail.created_at', 'DESC')
					.where('h_order_detail.note IS NOT NULL')
			}, 'history', 'history.order_detail_id = orderDetail.id')
		}

		if (filter.no_shipment) {
			Q = Q.leftJoin('orderDetail.orderDetailShipments', 'oDS')
				.andWhere(query => {
					return `NOT EXISTS ${query.subQuery()
						.select('1')
						.from('app.shipment', 'shipment')
						.where(`(oDS.shipment_id = shipment.id AND shipment.is_facade = FALSE AND shipment.is_return = FALSE)`)
						.getSql()
					}`
				})
		}

		if(filter.with_variant) {
			Q = Q.leftJoin('styleboard.styleboardStylecards', 'sc')
				.leftJoinAndMapMany('styleboard.stylecards', StylecardRecord, 'stylecards', 'stylecards.id = sc.stylecard_id')
				.leftJoinAndSelect('stylecards.stylecardVariants', 'sv')
				.leftJoinAndSelect('sv.variant', 'variant')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
				.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
				.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
				.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'variant_asset')
						.orderBy('variant_asset.index', 'ASC')
						.addOrderBy('variant_asset.id', 'ASC')
				}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')
				.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')

			if (filter.stylecard_status) {
				Q = Q.andWhere('stylecards.status = :stylecard_status', { stylecard_status: filter.stylecard_status })
			}
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async get<D extends boolean, WH extends boolean, WA extends boolean, WAS extends boolean, WC extends boolean, WF extends boolean, WR extends boolean, WSE extends boolean, WSC extends boolean, WS extends boolean, WU extends boolean, WSV extends boolean, WP extends boolean, SA extends boolean>(
		styleboard_id: number,
		{
			user_id,
			deep,
			with_answer,
			with_assets,
			with_count,
			with_feedback,
			with_purchase,
			with_request,
			with_service,
			with_stylecard,
			with_stylecard_variant,
			with_stylist,
			with_user,
			with_styleboard_assets,
			with_history,
		}: {
			user_id?: number,
			deep?: D,
			with_answer?: WA,
			with_assets?: WAS,
			with_count?: WC,
			with_feedback?: WF,
			with_purchase?: WP,
			with_request?: WR,
			with_service?: WSE,
			with_stylecard?: WSC,
			with_stylecard_variant?: WSV,
			with_stylist?: WS,
			with_user?: WU,
			with_styleboard_assets?: SA,
			with_history?: WH,
		},
		transaction: EntityManager,
	): Promise<StyleboardInterface & (WC extends true ? {
		count: number,
	} : {}) & (WR extends true ? {
		request?: UserRequestInterface,
	} : {}) & (WSE extends true ? {
		service: ServiceInterface,
	} : {}) & (WS extends true ? {
		stylist?: UserInterface & {
			profile: UserProfileInterface & {
				asset: UserAssetInterface,
			},
		},
	} : {}) & (WU extends true ? {
		user?: UserInterface & {
			profile: UserProfileInterface,
		},
	} : {}) & (WSC extends true ? {
		stylecards: Array<StylecardInterface & (WSV extends true ? {
			stylecardVariants: Array<StylecardVariantInterface & (D extends true ? {
				variant: VariantInterface,
				product: ProductInterface,
				brand: BrandInterface,
				category: CategoryInterface,
				colors: ColorInterface[],
				size: SizeInterface,
				inventories: InventoryInterface[],
			} & (WAS extends true ? {
				assets: VariantAssetInterface[],
			} : {
				asset?: VariantAssetInterface,
			}) : {}) & (WA extends true ? {
				answer?: VariantAnswerInterface,
			} : {}) & (WF extends true ? {
				feedback?: FeedbackInterface & {
					answer: FeedbackAnswerInterface,
				},
			} : {}) & (WP extends true ? {
				requests: OrderRequestInterface[],
			}: {})>,
		} : {})>,
	} : {}) & (SA extends true ? {
		assets: UserAssetInterface[],
	} : {}) & (WH extends true ? {
		histories: Array<HistoryStyleboardInterface & {
			user?: UserInterface & {
				profile: UserProfileInterface,
			},
			stylist?: UserInterface & {
				profile: UserProfileInterface,
			},
		}>,
	} : {})> {
		let Q = this.query('app.styleboard', 'styleboard', transaction)

		if (deep || with_stylecard || with_feedback || with_answer || with_count) {
			Q = Q.leftJoin('styleboard.styleboardStylecards', 'sc')
				.leftJoinAndMapMany('styleboard.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = sc.stylecard_id')
		}

		if (deep || with_stylecard_variant || with_feedback || with_answer || with_count) {
			Q = Q.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at is null')
				.orderBy('sv.id')
		}

		if (deep || with_feedback || with_purchase) {
			Q = Q.leftJoinAndSelect('sv.variant', 'variant')
		}

		if (deep || with_purchase) {
			Q = Q.leftJoinAndMapMany('sv.requests', OrderRequestRecord, 'requests', 'requests.type = :request_type AND requests.ref_id = variant.id', { request_type: ORDER_REQUESTS.VARIANT })
		}

		if (deep) {
			Q = Q.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				.leftJoinAndMapOne('sv.product', ProductRecord, 'product', 'product.id = variant.product_id')
				.leftJoinAndMapOne('sv.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
				.leftJoinAndMapOne('sv.category', CategoryRecord, 'category', 'category.id = product.category_id')
				.leftJoinAndMapMany('sv.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				.leftJoinAndMapOne('sv.size', SizeRecord, 'size', 'size.id = variant.size_id')
				.leftJoin(query => {
					return query.from(VariantAssetRecord, 'variant_asset')
						.orderBy('variant_asset.index', 'ASC')
						.addOrderBy('variant_asset.id', 'ASC')
				}, 'va', 'va.variant_id = variant.id AND va.deleted_at IS NULL')
				.leftJoinAndMapMany('sv.inventories', InventoryRecord, 'inventory', `inventory.variant_id = variant.id and inventory.deleted_at is null and inventory.status = 'AVAILABLE'`)

			if (with_assets) {
				Q = Q.leftJoinAndMapMany('sv.assets', VariantAssetRecord, 'assets', 'assets.id = va.id')
			} else {
				Q = Q.leftJoinAndMapOne('sv.asset', VariantAssetRecord, 'asset', 'asset.id = va.id')
			}
		}

		if (with_service) {
			Q = Q.leftJoinAndSelect('styleboard.service', 'service')
		}

		// if (with_request || with_feedback) {
		// 	Q = Q.leftJoinAndMapOne('styleboard.orderDetail', OrderDetailRecord, 'detail', 'detail.ref_id = styleboard.id AND detail.type = :detail_styleboard', { detail_styleboard: ORDER_DETAILS.STYLEBOARD })
		// }

		if (with_request) {
			Q = Q.leftJoinAndMapOne('styleboard.request', UserRequestRecord, 'request', `(request.metadata ->> 'styleboard_id')::int = styleboard.id`)
		}

		if (with_feedback) {
			Q = Q.leftJoin(OrderDetailRecord, 'detail', 'detail.ref_id = styleboard.id AND detail.type = :detail_styleboard', { detail_styleboard: ORDER_DETAILS.STYLEBOARD })
				.leftJoinAndMapOne('sv.feedback', FeedbackRecord, 'feedback', 'feedback.order_detail_id = detail.id AND feedback.type = :variant_feedback AND feedback.ref_id = variant.id', { variant_feedback: FEEDBACKS.VARIANT })
				.leftJoinAndMapOne('feedback.answer', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
		}

		if (with_answer && user_id) {
			Q = Q.leftJoinAndMapOne('sv.answer', VariantAnswerRecord, 'answer', 'answer.variant_id = variant.id AND answer.user_id = :user_id AND answer.question_id = :question_id', {
				user_id,
				question_id: DEFAULTS.VARIANT_FEEDBACK_QUESTION_ID,
			}).addOrderBy('answer.created_at', 'DESC')
		}

		if (user_id) {
			Q = Q.andWhere('styleboard.user_id = :user_id', { user_id })
		}

		if (with_stylist) {
			Q = Q.leftJoinAndSelect('styleboard.stylist', 'stylist')
				.leftJoinAndSelect('stylist.profile', 'profile')
				.leftJoinAndSelect('profile.asset', 'passet')
		}

		if (with_user) {
			Q = Q.leftJoinAndSelect('styleboard.user', 'user')
				.leftJoinAndSelect('user.profile', 'uProfile')
		}

		if (with_styleboard_assets) {
			Q = Q.leftJoin(StyleboardUserAssetRecord, 'styleboard_user_asset', 'styleboard_user_asset.styleboard_id = styleboard.id')
				.leftJoinAndMapMany('styleboard.assets', UserAssetRecord, 'user_asset', 'user_asset.id = styleboard_user_asset.user_asset_id')
		}

		if (with_history) {
			Q = Q.leftJoinAndMapMany('styleboard.histories', HistoryStyleboardRecord, 'history', 'history.styleboard_id = styleboard.id')
				.leftJoinAndMapOne('history.user', UserRecord, 'historyUser', 'historyUser.id = history.changer_user_id')
				.leftJoinAndMapOne('historyUser.profile', UserProfileRecord, 'historyUserProfile', 'historyUserProfile.id = historyUser.user_profile_id')
				.leftJoinAndMapOne('history.stylist', UserRecord, 'historyStylist', 'historyStylist.id = history.stylist_id')
				.leftJoinAndMapOne('historyStylist.profile', UserProfileRecord, 'historyStylistProfile', 'historyStylistProfile.id = historyStylist.user_profile_id')
		}

		return Q.where('styleboard.id = :id', {id: styleboard_id})
			.getOne()
			.then(this.parser)
			.then((data: any) => {
				return {
					...data,
					...( with_count ? {
						count: data.stylecards.map(s => s.stylecardVariants.length).reduce(CommonHelper.sum, 0),
					} : {}),
				}
			})
	}

	@RepositoryModel.bound
	async getWithResponse(
		styleboard_id: number,
		user_id: number | undefined,
		transaction: EntityManager,
	): Promise<StyleboardInterface & {
		stylecards: Array<StylecardInterface & {
			stylecardVariants: StylecardVariantInterface[],
		}>,
	}> {
		let Q = this.query('app.styleboard', 'styleboard', transaction)
			.leftJoin('styleboard.styleboardStylecards', 'sc')
			.leftJoinAndMapMany('styleboard.stylecards', StylecardRecord, 'stylecard', 'stylecard.id = sc.stylecard_id')
			.leftJoinAndSelect('stylecard.stylecardVariants', 'sv', 'sv.deleted_at is null')
			.where('styleboard.id = :styleboard_id', { styleboard_id })

		if (user_id) {
			Q = Q.andWhere('styleboard.user_id = :user_id', { user_id })
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getFromOrderDetail(
		order_detail_id: number,
		user_id: number | undefined,
		transaction: EntityManager,
	): Promise<StyleboardInterface> {
		let Q = this.query('app.styleboard', 'styleboard', transaction)
			.innerJoin(OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = styleboard.id AND orderDetail.type = :styleboard AND orderDetail.id = :order_detail_id', { styleboard: ORDER_DETAILS.STYLEBOARD, order_detail_id })

		if (user_id) {
			Q = Q.where('styleboard.user_id = :user_id', { user_id })
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getFromStylecard<WOD extends boolean>(
		stylecard_id: number,
		config: {
			with_order_detail?: WOD,
		},
		transaction: EntityManager,
	): Promise<StyleboardInterface & (WOD extends true ? {
		orderDetail?: OrderDetailInterface,
	} : {})> {
		let Q = this.query('app.styleboard', 'styleboard', transaction)
			.innerJoin('styleboard.styleboardStylecards', 'sc', 'sc.stylecard_id = :stylecard_id', { stylecard_id })

		if (config.with_order_detail) {
			Q = Q.leftJoinAndMapOne('styleboard.orderDetail', OrderDetailRecord, 'detail', 'detail.ref_id = styleboard.id AND detail.type = :styleboard', { styleboard: ORDER_DETAILS.STYLEBOARD })
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getExpired(transaction: EntityManager): Promise<StyleboardInterface[]> {
		return this.query('app.styleboard', 'styleboard', transaction)
			.where('styleboard.expired_at <= current_timestamp AND styleboard.status NOT IN (:resolved, :redeemed)', { resolved: STYLEBOARD_STATUSES.RESOLVED, redeemed: STYLEBOARD_STATUSES.REDEEMED })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getExpiring(transaction: EntityManager): Promise<Array<StyleboardInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		orderDetail: OrderDetailInterface,
	}>> {
		return this.query('app.styleboard', 'styleboard', transaction)
			.leftJoinAndSelect('styleboard.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndMapOne('styleboard.orderDetail', OrderDetailRecord, 'detail', 'detail.ref_id = styleboard.id AND detail.type = :styleboard', { styleboard: ORDER_DETAILS.STYLEBOARD })
			.where('styleboard.expired_at <= (current_timestamp + interval :interval) AND styleboard.status NOT IN (:resolved, :redeemed)', { resolved: STYLEBOARD_STATUSES.RESOLVED, redeemed: STYLEBOARD_STATUSES.REDEEMED, interval: '12 hour' })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStyleboardsFromVariant(variant_id: number, transaction: EntityManager): Promise<StyleboardInterface[]> {
		return this.query('app.styleboard', 'styleboard', transaction)
			.innerJoin('styleboard.styleboardStylecards', 'ss')
			.innerJoin('ss.stylecard', 'stylecard')
			.innerJoin('stylecard.stylecardVariants', 'sv')
			.innerJoin('sv.variant', 'variant')
			.innerJoin('variant.product', 'product')
			.innerJoin('product.variants', 'variants', 'variants.id = :variant_id', { variant_id })
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
