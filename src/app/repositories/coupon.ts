import {
	ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	CouponRecord,
	MatchboxCouponRecord,
	OrderDetailCouponRecord,
	ProductCouponRecord,
	UserCouponRecord,
	VariantCouponRecord,
	BrandRecord,
	MatchboxAssetRecord,
	VariantRecord,
	VariantAssetRecord,
	ProductRecord,
	UserAssetRecord,
	ServiceAssetRecord,
	ServiceCouponRecord,
	StylecardCouponRecord,
	StylecardVariantRecord,
	InventoryRecord,
} from 'energie/app/records'

import {
	CouponInterface,
	MatchboxCouponInterface,
	ProductCouponInterface,
	UserCouponInterface,
	VariantCouponInterface,
	InventoryInterface,
	VariantInterface,
	MatchboxInterface,
	BrandInterface,
	MatchboxAssetInterface,
	ProductInterface,
	VariantAssetInterface,
	UserInterface,
	UserProfileInterface,
	UserAssetInterface,
	ServiceInterface,
	ServiceAssetInterface,
	ServiceCouponInterface,
	StylecardCouponInterface,
	StylecardInterface,
} from 'energie/app/interfaces'

import { Parameter, ObjectOrArray } from 'types/common'

import { COUPON_VALIDITY, ORDERS } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import { isEmpty } from 'lodash'


@EntityRepository()
export default class CouponRepository extends RepositoryModel<{
	CouponRecord: CouponRecord,
	MatchboxCouponRecord: MatchboxCouponRecord,
	OrderDetailCouponRecord: OrderDetailCouponRecord,
	ProductCouponRecord: ProductCouponRecord,
	ServiceCouponRecord: ServiceCouponRecord,
	UserCouponRecord: UserCouponRecord,
	VariantCouponRecord: VariantCouponRecord,
	StylecardCouponRecord: StylecardCouponRecord,
}> {

	static __displayName = 'CouponRepository'

	protected records = {
		CouponRecord,
		MatchboxCouponRecord,
		OrderDetailCouponRecord,
		ProductCouponRecord,
		ServiceCouponRecord,
		UserCouponRecord,
		VariantCouponRecord,
		StylecardCouponRecord,
	}

	// ============================= INSERT =============================
	async insert(coupon: Parameter<CouponInterface>, transaction: EntityManager): Promise<CouponInterface>
	async insert(coupons: Array<Parameter<CouponInterface>>, transaction: EntityManager): Promise<CouponInterface[]>

	@RepositoryModel.bound
	async insert(coupon: ObjectOrArray<Parameter<CouponInterface>>): Promise<any> {
		if (Array.isArray(coupon)) {
			return await this.save('CouponRecord', coupon as CouponInterface[])
		} else {
			return await this.save('CouponRecord', coupon as CouponInterface)
		}
	}

	@RepositoryModel.bound
	async addCouponMatchbox(
		coupon_id: number,
		matchbox_id: number,
		transaction: EntityManager,
	): Promise<MatchboxCouponInterface> {
		return this.save('MatchboxCouponRecord', {
			matchbox_id,
			coupon_id,
		}, transaction)
	}

	@RepositoryModel.bound
	async addCouponProduct(
		coupon_id: number,
		product_id: number,
		amount: number | null,
		transaction: EntityManager,
	): Promise<ProductCouponInterface> {
		return this.save('ProductCouponRecord', {
			product_id,
			coupon_id,
			amount,
		}, transaction)
	}

	@RepositoryModel.bound
	async addCouponService(
		coupon_id: number,
		service_id: number,
		transaction: EntityManager,
	): Promise<ServiceCouponInterface> {
		return this.save('ServiceCouponRecord', {
			service_id,
			coupon_id,
		}, transaction)
	}

	@RepositoryModel.bound
	async addCouponUser(
		coupon_id: number,
		user_id: number,
		transaction: EntityManager,
	): Promise<UserCouponInterface> {
		return this.save('UserCouponRecord', {
			user_id,
			coupon_id,
		}, transaction)
	}

	@RepositoryModel.bound
	async addCouponVariant(
		coupon_id: number,
		variant_id: number,
		transaction: EntityManager,
	): Promise<VariantCouponInterface> {
		return this.save('VariantCouponRecord', {
			variant_id,
			coupon_id,
		}, transaction)
	}

	@RepositoryModel.bound
	async addCouponStylecard(
		coupon_id: number,
		stylecard_id: number,
		transaction: EntityManager,
	): Promise<StylecardCouponInterface> {
		return this.save('StylecardCouponRecord', {
			stylecard_id,
			coupon_id,
		}, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		coupon_id: number,
		data: Partial<Parameter<CouponInterface>> | undefined,
		couponMatchboxes: number[] | undefined,
		couponProducts: number[] | undefined,
		couponServices: number[] | undefined,
		couponUsers: number[] | undefined,
		couponVariants: number[] | undefined,
		couponStylecards: number[] | undefined,
		transaction: EntityManager,
	): Promise<any> {
		if(!isEmpty(data)) {
			await this.renew('CouponRecord', coupon_id, data, {
				transaction,
			})

			return this.setCoupon(coupon_id, couponMatchboxes, couponProducts, couponServices, couponUsers, couponVariants, couponStylecards, transaction)
		} else {
			return this.setCoupon(coupon_id, couponMatchboxes, couponProducts, couponServices, couponUsers, couponVariants, couponStylecards, transaction)
		}
	}

	@RepositoryModel.bound
	async updateCouponProduct(
		coupon_id: number,
		product_id: number,
		amount: number,
		transaction: EntityManager,
	) {
		return this.renew('ProductCouponRecord', 'coupon_id = :coupon_id and product_id = :product_id', {
			amount,
		}, {
			parameters: {
				coupon_id,
				product_id,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async getCouponFromOrderDetailInput(order_detail_id: number, transaction: EntityManager) {
		let Q = this.query('app.coupon', 'coupon', transaction)
			.leftJoin('coupon.couponOrderDetails', 'couponOrderDetails')
			.where('couponOrderDetails.order_detail_id = :orderDetailId', { orderDetailId: order_detail_id})

		return Q.getOne()
		.then(this.parser) as any
	}
	
	// ============================= GETTER =============================
	// :: PromoCouponManager: 539
 	// :: PromoCouponManager: 373
	@RepositoryModel.bound
	 async filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			validity?: COUPON_VALIDITY,
			date?: '1 days' | '7 days' | '1 months' | '2 months' | '3 months' | '1 years',
			expired?: boolean,
			published?: boolean,
			is_public?: boolean,
			order_detail_id?: number,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<CouponInterface & {
			count: number,
			paid_count: number,
		}>,
		count: number,
	}> {
		let Q = this.query('app.coupon', 'coupon', transaction)
			.addSelect('coupon.expired_at')
			.loadRelationCountAndMap('coupon.count', 'coupon.couponOrderDetails', 'cOD', qb => {
				return qb.innerJoin(qb2 => {
					return qb2.select('MAX(orderDetail.id) od_id')
						.from('app.coupon', 'coupon')
						.leftJoin('coupon.couponOrderDetails', 'couponOrderDetails')
						.leftJoin('couponOrderDetails.orderDetail', 'orderDetail')
						.leftJoin('orderDetail.order', 'order')
						.groupBy('order.id')
				}, 'coo', 'coo.od_id = cOD.order_detail_id')
			})
			.loadRelationCountAndMap('coupon.paid_count', 'coupon.couponOrderDetails', 'cOD2', qb => {
				return qb.innerJoin(qb2 => {
					return qb2.select('MAX(orderDetail.id) od_id')
						.from('app.coupon', 'coupon')
						.leftJoin('coupon.couponOrderDetails', 'couponOrderDetails')
						.leftJoin('couponOrderDetails.orderDetail', 'orderDetail')
						.innerJoin('orderDetail.order', 'order', 'order.status IN (:...valid_statuses)', { valid_statuses: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION, ORDERS.PROCESSING, ORDERS.RESOLVED] })
						.groupBy('order.id')
				}, 'coo', 'coo.od_id = cOD2.order_detail_id')
			})
			// dashboard buat sort published at > expired at
			// .orderBy('coupon.published_at < NOW()::timestamp and (coupon.expired_at is null or coupon.expired_at::timestamp > NOW())', 'DESC')
			.addOrderBy('coupon.created_at', 'DESC')
			.addOrderBy('coupon.updated_at', 'DESC')
			.skip(offset)
			.take(limit)

		if(search) {
			Q = Q.where('coupon.title ILIKE :search', { search: `%${ search }%` })
		} else {
			Q = Q.where(query => {
				return `coupon.id NOT IN ${query.subQuery().from('app.user_profile', 'up')
					.select('up.coupon_id')
					.where('up.coupon_id IS NOT NULL')
					.getSql()
				}`
			})
		}

		if(filter.validity) {
			Q = Q.andWhere('coupon.valid_for ILIKE :validity', { validity: `%${ filter.validity }%` })
		}

		if(filter.date) {
			Q = Q.andWhere(`coupon.created_at > (current_date - interval '${filter.date}')`)
		}

		if(filter.expired) {
			Q = Q.andWhere('(coupon.expired_at IS NOT NULL AND coupon.expired_at <= current_timestamp)')
		} else if(filter.expired === false) {
			Q = Q.andWhere('(coupon.expired_at IS NULL OR coupon.expired_at > current_timestamp)')
		}

		if(filter.published) {
			Q = Q.andWhere('(coupon.published_at IS NOT NULL AND coupon.published_at <= current_timestamp)')
		} else if(filter.published === false) {
			Q = Q.andWhere('(coupon.published_at IS NULL OR coupon.published_at > current_timestamp)')
		}

		if(filter.is_public) {
			Q = Q.andWhere(`coupon.is_public IS ${filter.is_public}`)
		}

		if(filter.order_detail_id) {
			Q = Q.leftJoinAndSelect('coupon.couponOrderDetails', 'couponOrderDetails')
				.andWhere('couponOrderDetails.order_detail_id = :orderDetailId', { orderDetailId: filter.order_detail_id})
		}

		return Q.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAvailableCoupon(
		user_id: number | undefined,
		type: COUPON_VALIDITY | undefined,
		for_all_recommendation: boolean | undefined,
		is_public: boolean,
		transaction: EntityManager,
	): Promise<CouponInterface[]> {
		let Q = this.query('app.coupon', 'coupon', transaction)
			// first we exclude any user related coupons
			.where('(coupon.valid_for_all = TRUE OR coupon.valid_for NOT LIKE \'%USER%\')')
			// publish and expiricy
			.andWhere('coupon.published_at < current_timestamp')
			.andWhere('(coupon.expired_at IS NULL OR coupon.expired_at > current_timestamp)')

		if (is_public) {
			Q = Q.andWhere('coupon.is_public is TRUE')
		}

		if (type) {
			Q = Q.andWhere('coupon.valid_for LIKE :type', { type: `%${ type }%` })
		}

		if (!!for_all_recommendation) {
			Q = Q.andWhere(`(coupon.metadata ->> 'for_all_recommendation')::boolean is true`)
		}

		if (user_id) {
			Q = Q.orWhere(query => {
					return `(coupon.valid_for LIKE \'%USER%\' AND coupon.published_at < current_timestamp AND (coupon.expired_at IS NULL OR coupon.expired_at > current_timestamp) AND coupon.id IN(${
						query.subQuery().from('app.user_coupon', 'uc')
							.select('uc.coupon_id')
							.where(`uc.user_id = ${ user_id }`)
							.getQuery()
					}))`
				})
		}

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCoupon(id: number, transaction: EntityManager): Promise<CouponInterface> {
		return this.getOne('CouponRecord', {
			id,
		}, { transaction })
	}

	@RepositoryModel.bound
	async getCouponWithCount(id: number, transaction: EntityManager): Promise<CouponInterface & {
		count: number,
		paid_count: number,
	}> {
		return this.query('app.coupon', 'coupon', transaction)
			.addSelect('coupon.expired_at')
			.orderBy('coupon.created_at', 'DESC')
			.loadRelationCountAndMap('coupon.count', 'coupon.couponOrderDetails', 'cOD', qb => {
				return qb.innerJoin(qb2 => {
					return qb2.select('MAX(orderDetail.id) od_id')
						.from('app.coupon', 'coupon')
						.leftJoin('coupon.couponOrderDetails', 'couponOrderDetails')
						.leftJoin('couponOrderDetails.orderDetail', 'orderDetail')
						.leftJoin('orderDetail.order', 'order')
						.groupBy('order.id')
				}, 'coo', 'coo.od_id = cOD.order_detail_id')
			})
			.loadRelationCountAndMap('coupon.paid_count', 'coupon.couponOrderDetails', 'cOD2', qb => {
				return qb.innerJoin(qb2 => {
					return qb2.select('MAX(orderDetail.id) od_id')
						.from('app.coupon', 'coupon')
						.leftJoin('coupon.couponOrderDetails', 'couponOrderDetails')
						.leftJoin('couponOrderDetails.orderDetail', 'orderDetail')
						.innerJoin('orderDetail.order', 'order', 'order.status IN (:...valid_statuses)', { valid_statuses: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION, ORDERS.PROCESSING, ORDERS.RESOLVED] })
						.groupBy('order.id')
				}, 'coo', 'coo.od_id = cOD2.order_detail_id')
			})
			.where('coupon.id = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponMatchbox(id: number, transaction: EntityManager): Promise<Array<MatchboxCouponInterface & {
		matchbox: MatchboxInterface,
		brand: BrandInterface,
		asset: MatchboxAssetInterface,
	}>> {
		return this.query('app.matchbox_coupon', 'mc', transaction)
			.leftJoinAndSelect('mc.matchbox', 'matchbox')
			.leftJoinAndMapOne('mc.brand', BrandRecord, 'brand', 'brand.id = matchbox.brand_id')
			.leftJoinAndMapOne('mc.asset', MatchboxAssetRecord, 'asset', 'asset.matchbox_id = matchbox.id AND asset.deleted_at IS NULL')
			.where('mc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponProduct(id: number, transaction: EntityManager): Promise<Array<ProductCouponInterface & {
		product: ProductInterface,
		variants: VariantInterface[],
		brand: BrandInterface,
		asset: VariantAssetInterface,
	}>> {
		return this.query('app.product_coupon', 'pc', transaction)
			.leftJoinAndSelect('pc.product', 'product')
			.leftJoinAndMapMany('pc.variants', VariantRecord, 'variant', 'variant.product_id = product.id')
			.leftJoinAndMapOne('pc.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('pc.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.where('pc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponService(id: number, transaction: EntityManager): Promise<Array<ServiceCouponInterface & {
		service: ServiceInterface,
		brand: BrandInterface,
		asset: ServiceAssetInterface,
	}>> {
		return this.query('app.service_coupon', 'sc', transaction)
			.leftJoinAndSelect('sc.service', 'service')
			.leftJoinAndMapOne('sc.brand', BrandRecord, 'brand', 'brand.id = service.brand_id')
			.leftJoinAndMapOne('sc.asset', ServiceAssetRecord, 'asset', 'asset.service_id = service.id')
			.where('sc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponUser(id: number, transaction: EntityManager): Promise<Array<UserCouponInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		asset: UserAssetInterface,
	}>> {
		return this.query('app.user_coupon', 'uc', transaction)
			.leftJoinAndSelect('uc.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndMapOne('uc.asset', UserAssetRecord, 'asset', 'asset.user_id = user.id')
			.where('uc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponVariant(id: number, transaction: EntityManager): Promise<Array<VariantCouponInterface & {
		variant: VariantInterface
		product: ProductInterface,
		brand: BrandInterface,
		asset: VariantAssetInterface,
	}>> {
		return this.query('app.variant_coupon', 'vc', transaction)
			.leftJoinAndSelect('vc.variant', 'variant')
			.leftJoinAndMapOne('vc.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('vc.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('vc.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.where('vc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getCouponStylecard(id: number, transaction: EntityManager): Promise<Array<StylecardInterface & {
		variants: Array<VariantInterface & {
			product: ProductInterface,
			brand: BrandInterface,
			asset: VariantAssetInterface,
		}>,
	}>> {
		return this.query('app.stylecard', 'stylecard', transaction)
			.leftJoin(StylecardVariantRecord, 'sv', 'sv.stylecard_id = stylecard.id and sv.deleted_at IS NULL')
			.leftJoinAndMapMany('stylecard.variants', VariantRecord, 'variant', 'variant.id = sv.variant_id')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.innerJoin(StylecardCouponRecord, 'sc', 'sc.stylecard_id = stylecard.id')
			.where('sc.coupon_id = :id', { id })
			.orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByCode(code: string, transaction: EntityManager): Promise<CouponInterface> {
		return this.query('app.coupon', 'coupon', transaction)
			.where('LOWER(coupon.title) = LOWER(:code)', { code })
			.andWhere('coupon.published_at <= now()')
			.andWhere('(coupon.expired_at IS NULL OR coupon.expired_at > now())')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getValidityForUser(user_id: number, coupon_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.coupon', 'coupon', transaction)
			.innerJoin('coupon.couponUsers', 'couponUser', 'couponUser.user_id = :user_id', { user_id })
			.where('coupon.id = :coupon_id', { coupon_id })
			.getCount()
			.then(count => {
				return !!count
			})
	}

	@RepositoryModel.bound
	async getValidityForMatchbox(matchbox_id: number, coupon_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.coupon', 'coupon', transaction)
			.innerJoin('coupon.couponMatchboxes', 'couponMatchbox', 'couponMatchbox.matchbox_id = :matchbox_id', { matchbox_id })
			.where('coupon.id = :coupon_id', { coupon_id })
			.getCount()
			.then(count => {
				return !!count
			})
	}

	@RepositoryModel.bound
	async getValidityForService(service_id: number, coupon_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.coupon', 'coupon', transaction)
			.innerJoin('coupon.couponServices', 'couponService', 'couponService.service_id = :service_id', { service_id })
			.where('coupon.id = :coupon_id', { coupon_id })
			.getCount()
			.then(count => {
				return !!count
			})
	}

	@RepositoryModel.bound
	async getValidityForInventory(inventory_id: number, coupon_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.where('inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(async (inventory: InventoryInterface & { variant: VariantInterface }) => {
				return this.query('app.coupon', 'coupon', transaction)
					.innerJoin('coupon.couponProducts', 'couponProducts', 'couponProducts.product_id = :product_id', { product_id: inventory.variant.product_id })
					.where('coupon.id = :coupon_id', { coupon_id })
					.getCount()
					.then(count => {
						if (count === 0) {
							return this.query('app.coupon', 'coupon', transaction)
								.innerJoin('coupon.couponVariants', 'couponVariants', 'couponVariants.variant_id = :variant_id', { variant_id: inventory.variant_id })
								.where('coupon.id = :coupon_id', { coupon_id })
								.getCount()
								.then(_count => {
									return !!_count
								})
						} else {
							return true
						}
					})
			})
	}

	@RepositoryModel.bound
	async getValidityForVariant(
		variant_id: number,
		coupon_id: number,
		valid_for_all: boolean,
		internal_product_only: boolean,
		transaction: EntityManager,
	): Promise<boolean> {
		const Q =  this.query('app.variant', 'variant', transaction)
			.where('variant.id = :variant_id', { variant_id })

		if (internal_product_only) {
			Q.andWhere(sq => {
				return '(variant.have_consignment_stock is true or EXISTS' + sq.subQuery()
					.select('1')
					.from(InventoryRecord, 'inventory')
					.where('inventory.variant_id = variant.id')
					.andWhere(`inventory.status = 'AVAILABLE'`)
					.getSql() + ')'
			})
		}

		return Q.getOne()
			.then(async (variant: VariantInterface) => {
				if (valid_for_all) {
					return !!variant
				} else {
					if (isEmpty(variant)) {
						return false
					}

					return this.query('app.coupon', 'coupon', transaction)
						.innerJoin('coupon.couponProducts', 'couponProducts', 'couponProducts.product_id = :product_id', { product_id: variant.product_id })
						.where('coupon.id = :coupon_id', { coupon_id })
						.getCount()
						.then(count => {
							if (count === 0) {
								return this.query('app.coupon', 'coupon', transaction)
									.innerJoin('coupon.couponVariants', 'couponVariants', 'couponVariants.variant_id = :variant_id', { variant_id })
									.where('coupon.id = :coupon_id', { coupon_id })
									.getCount()
									.then(_count => {
										return !!_count
									})
							} else {
								return true
							}
						})
				}
			})
	}

	@RepositoryModel.bound
	async getValidityForStylecard(stylecard_id: number, coupon_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.stylecard_coupon', 'sc', transaction)
			.where('sc.stylecard_id = :stylecard_id', {stylecard_id})
			.andWhere('sc.coupon_id = :coupon_id', {coupon_id})
			.getCount()
			.then(count => !!count)
	}

	@RepositoryModel.bound
	async getValidityForStylecardCluster(
		stylecard_id: number,
		is_recomendation: boolean | undefined,
		cluster_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		let Q = this.query('app.stylecard', 'sc', transaction)
			.where('sc.id = :stylecard_id', {stylecard_id})

		if (is_recomendation) {
			Q = Q.andWhere('sc.is_recomendation is :is_recomendation', {is_recomendation})
		}

		if (!is_recomendation && !!cluster_ids && cluster_ids.length > 0) {
			Q.andWhere('sc.cluster_id in (:...cluster_ids)', {cluster_ids})
		}

		return Q.getCount()
			.then(count => !!count)
	}

	@RepositoryModel.bound
	async getUsage(
		coupon_id: number,
		success_only: boolean = true,
		transaction: EntityManager,
	): Promise<number> {
		const Q = this.query('app.order', 'order', transaction)
			.innerJoin('order.orderDetails', 'orderDetails')
			.innerJoin('orderDetails.orderDetailCoupons', 'odc', 'odc.coupon_id = :coupon_id', { coupon_id })

		if (success_only) {
			return Q
				.where('order.status IN (:...valid_statuses)', { valid_statuses: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION, ORDERS.PROCESSING, ORDERS.RESOLVED] })
				.getCount()
		} else {
			return Q.getCount()
		}
	}

	@RepositoryModel.bound
	async getUserUsage(
		user_id: number,
		coupon_id: number | undefined,
		success_only: boolean = true,
		transaction: EntityManager,
	): Promise<number> {
		const Q = this.query('app.order', 'order', transaction)
			.innerJoin('order.orderDetails', 'orderDetails')
			.innerJoin('orderDetails.orderDetailCoupons', 'odc', 'odc.coupon_id = :coupon_id', { coupon_id })
			.where('order.user_id = :user_id', { user_id })

		if (success_only) {
			return Q
				.andWhere('order.status IN (:...valid_statuses)', { valid_statuses: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION, ORDERS.PROCESSING, ORDERS.RESOLVED] })
				.getCount()
		} else {
			return Q.getCount()
		}
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async deleteCouponMatchbox(
		coupon_id: number,
		matchbox_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.matchbox_coupon', 'mc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('matchbox_id = :matchbox_id', { matchbox_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async deleteCouponProduct(
		coupon_id: number,
		product_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.product_coupon', 'pc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('product_id = :product_id', { product_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async deleteCouponService(
		coupon_id: number,
		service_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.service_coupon', 'sc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('service_id = :service_id', { service_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async deleteCouponUser(
		coupon_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.user_coupon', 'uc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('user_id = :user_id', { user_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async deleteCouponVariant(
		coupon_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.variant_coupon', 'vc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('variant_id = :variant_id', { variant_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async deleteCouponStylecard(
		coupon_id: number,
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.stylecard_coupon', 'vc', transaction)
			.where('coupon_id = :coupon_id', { coupon_id })
			.andWhere('stylecard_id = :stylecard_id', { stylecard_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}


	// ============================ PRIVATES ============================
	private async setCoupon(
		coupon_id: number,
		couponMatchboxes: number[] | undefined,
		couponProducts: number[] | undefined,
		couponServices: number[] | undefined,
		couponUsers: number[] | undefined,
		couponVariants: number[] | undefined,
		couponStylecards: number[] | undefined,
		transaction: EntityManager,
	): Promise<[
		StylecardCouponInterface[],
		MatchboxCouponInterface[],
		ProductCouponInterface[],
		ServiceCouponInterface[],
		UserCouponInterface[],
		VariantCouponInterface[],
	]> {
		return Promise.all([
			!isEmpty(couponStylecards) ? this.removeCouponStylecards(coupon_id, transaction).then(isDeleted => {
				if(isDeleted) {
					return this.save('StylecardCouponRecord', couponStylecards.map(stylecard_id => {
						return {
							stylecard_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
			!isEmpty(couponMatchboxes) ? this.removeCouponMatchboxes(coupon_id, transaction).then(isDeleted => {
				if(isDeleted) {
					return this.save('MatchboxCouponRecord', couponMatchboxes.map(matchbox_id => {
						return {
							matchbox_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
			!isEmpty(couponProducts) ? this.removeCouponProducts(coupon_id, transaction).then(isDeleted => {
				if(isDeleted) {
					return this.save('ProductCouponRecord', couponProducts.map(product_id => {
						return {
							product_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
			!isEmpty(couponServices) ? this.removeCouponServices(coupon_id, transaction).then(isDeleted => {
				if (isDeleted) {
					return this.save('ServiceCouponRecord', couponServices.map(service_id => {
						return {
							service_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
			!isEmpty(couponUsers) ? this.removeCouponUsers(coupon_id, transaction).then(isDeleted => {
				if(isDeleted) {
					return this.save('UserCouponRecord', couponUsers.map(user_id => {
						return {
							user_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
			!isEmpty(couponVariants) ? this.removeCouponVariants(coupon_id, transaction).then(isDeleted => {
				if(isDeleted) {
					return this.save('VariantCouponRecord', couponVariants.map(variant_id => {
						return {
							variant_id,
							coupon_id,
						}
					}), transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Cannot delete previous data.')
				}
			}) : [],
		])
	}

	private async removeCouponMatchboxes(coupon_id: number, transaction: EntityManager) {
		return this.query('app.matchbox_coupon', 'matchbox_coupon', transaction)
			.where('matchbox_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	private async removeCouponStylecards(coupon_id: number, transaction: EntityManager) {
		return this.query('app.stylecard_coupon', 'stylecard_coupon', transaction)
			.where('stylecard_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	private async removeCouponProducts(coupon_id: number, transaction: EntityManager) {
		return this.query('app.product_coupon', 'product_coupon', transaction)
			.where('product_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	private async removeCouponServices(coupon_id: number, transaction: EntityManager) {
		return this.query('app.service_coupon', 'service_coupon', transaction)
			.where('service_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	private async removeCouponUsers(coupon_id: number, transaction: EntityManager) {
		return this.query('app.user_coupon', 'user_coupon', transaction)
			.where('user_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	private async removeCouponVariants(coupon_id: number, transaction: EntityManager) {
		return this.query('app.variant_coupon', 'variant_coupon', transaction)
			.where('variant_coupon.coupon_id = :coupon_id', { coupon_id })
			.delete()
			.execute()
			.then(() => true)
			.catch(err => {
				this.warn(err)

				return false
			})
	}

}
