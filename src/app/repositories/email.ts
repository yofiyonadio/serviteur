import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	EmailRecord,
} from 'energie/app/records'

import {
	TEMPLATES,
} from 'app/handlers/mandrill'

import {
	EmailInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'
import { SENDGRID_TEMPLATES } from 'app/handlers/send.grid'

type TEMPLATE_KEYS = keyof typeof TEMPLATES
type SENDGRID_TEMPLATE_KEYS = keyof typeof SENDGRID_TEMPLATES


@EntityRepository()
export default class EmailRepository extends RepositoryModel<{
	EmailRecord: EmailRecord,
}> {

	static __displayName = 'EmailRepository'

	protected records = {
		EmailRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(data: Parameter<EmailInterface>): Promise<EmailInterface> {
		return this.save('EmailRecord', data)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async getLatest(type: TEMPLATE_KEYS | TEMPLATE_KEYS[] | SENDGRID_TEMPLATE_KEYS | SENDGRID_TEMPLATE_KEYS[], ref_id: number, transaction: EntityManager): Promise<EmailInterface>
	async getLatest(type: TEMPLATE_KEYS | TEMPLATE_KEYS[] | SENDGRID_TEMPLATE_KEYS | SENDGRID_TEMPLATE_KEYS[], ref_ids: number[], transaction: EntityManager): Promise<EmailInterface[]>

	@RepositoryModel.bound
	async getLatest(
		type: TEMPLATE_KEYS | TEMPLATE_KEYS[],
		ref_id_or_ids: number | number[],
		transaction?: EntityManager,
	): Promise<EmailInterface | EmailInterface[]> {
		const Q = this.query('app.email', 'email', transaction)
			.where(Array.isArray(ref_id_or_ids) ? 'email.ref_id IN (:...ref_id_or_ids)' : 'email.ref_id = :ref_id_or_ids', { ref_id_or_ids })
			.andWhere(Array.isArray(type) ? `email.type IN (:...type)` : 'email.type = :type', { type })
			.orderBy('email.created_at', 'DESC')

		if(Array.isArray(ref_id_or_ids)) {
			return Q
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
