import RepositoryModel, { Keys } from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	CategoryRecord,
	CategoryMeasurementRecord,
	CategorySizeRecord,
	CategoryTagRecord,
} from 'energie/app/records'

import {
	CategoryInterface,
} from 'energie/app/interfaces'

@EntityRepository()
export default class CategoryRepository extends RepositoryModel<{
	CategoryRecord: CategoryRecord,
	CategoryMeasurementRecord: CategoryMeasurementRecord,
	CategorySizeRecord: CategorySizeRecord,
	CategoryTagRecord: CategoryTagRecord,
}> {

	static __displayName = 'CategoryRepository'

	protected records = {
		CategoryRecord,
		CategoryMeasurementRecord,
		CategorySizeRecord,
		CategoryTagRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		id: number,
		category_id: number,
		title: string,
		description: string,
		measurementIds: number[] = [],
		sizeIds: number[] = [],
		tagIds: number[] = [],
		transaction: EntityManager,
	): Promise<boolean> {
		const category = await this.saveOrUpdate('CategoryRecord', [{
			id,
			category_id,
			title,
			description: description || null,
			// created_at: null,
		}], transaction)

		// Need to be not intrusive
		if (measurementIds.length) {
			// Filter non existent
			const mIds: number[] = (await Promise.all(measurementIds.map(async mId => {
				return this.getOne('CategoryMeasurementRecord', {
					category_id: id,
					measurement_id: mId,
				}).then(() => {
					return false
				}).catch(() => {
					return mId
				})
			}))).filter(mId => !!mId) as any

			if(mIds.length) {
				await this.save('CategoryMeasurementRecord', mIds.map((measurementId): Keys<CategoryMeasurementRecord> => {
					return {
						category_id: id,
						measurement_id: measurementId,
					}
				}), transaction)
			}
		}

		// Need to be not intrusive
		if (sizeIds.length) {
			// Filter non existent
			const sIds: number[] = (await Promise.all(sizeIds.map(async sId => {
				return this.getOne('CategorySizeRecord', {
					category_id: id,
					size_id: sId,
				}).then(() => {
					return false
				}).catch(() => {
					return sId
				})
			}))).filter(sId => !!sId) as any

			if (sIds.length) {
				await this.save('CategorySizeRecord', sIds.map((sizeId): Keys<CategorySizeRecord> => {
					return {
						category_id: id,
						size_id: sizeId,
					}
				}), transaction)
			}
		}

		// Need to be not intrusive
		if (tagIds.length) {
			// Filter non existent
			const tIds: number[] = (await Promise.all(tagIds.map(async tId => {
				return this.getOne('CategoryTagRecord', {
					category_id: id,
					tag_id: tId,
				}).then(() => {
					return false
				}).catch(() => {
					return tId
				})
			}))).filter(tId => !!tId) as any

			if (tIds.length) {
				await this.save('CategoryTagRecord', tIds.map((tagId): Keys<CategoryTagRecord> => {
					return {
						category_id: id,
						tag_id: tagId,
					}
				}), transaction)
			}
		}

		return category
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getById(category_id: number, transaction: EntityManager): Promise<{
		id: number,
		title: string,
		category_id: number | null,
		categories: CategoryInterface[],
	}> {
		return this.query('master.category', 'category', transaction)
			.select(['category.id', 'category.title', 'category.category_id', 'categories.id', 'categories.title', 'categories.category_id'])
			.leftJoin('category.parent', 'parent')
			.leftJoinAndSelect('category.categories', 'categories')
			.where('category.id = :category_id', { category_id })
			.andWhere('category.deleted_at IS NULL')
			.andWhere('categories.deleted_at IS NULL')
			.getOne()
			.then(this.parser) as any
	}


	// Excaption in refactoring transaction
	async getAllIds(transaction?: EntityManager): Promise<number[]>
	async getAllIds(complete?: boolean, transaction?: EntityManager): Promise<Array<Pick<CategoryInterface, 'id' | 'title' | 'description' | 'category_id'>>>

	@RepositoryModel.bound
	async getAllIds(transactionOrComplete?: boolean | EntityManager, transaction?: EntityManager): Promise<any> {
		return this.query('master.category', 'category', transaction)
			.select(transactionOrComplete === true ? ['category.id', 'category.title', 'category.description', 'category.category_id'] : ['category.id'])
			.where('category.deleted_at IS NULL')
			.getMany()
			.then(this.parser)
			.then(categories => {
				if(transactionOrComplete === true) {
					return categories
				} else {
					return categories.map((c: CategoryInterface) => c.id)
				}
			})
	}

	@RepositoryModel.bound
	async getRoots(transaction: EntityManager): Promise<Array<{
		id: number,
		title: string,
		category_id: number | null,
	}>> {
		return this.query('master.category', 'category', transaction)
			.select(['category.id', 'category.title', 'category.category_id'])
			.where('category.category_id IS NULL')
			.andWhere('category.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
