import AreaManualRepository from './area/manual'
import AreaTikiRepository from './area/tiki'
import AreaSicepatRepository from './area/sicepat'
import AssetRepository from './asset'
import BCARepository from './other/bca'
import BrandRepository from './brand'
import CampaignRepository from './campaign'
import CategoryRepository from './category'
import ColorRepository from './color'
import CollectionRepository from './collection'
import ClusterRepository from './cluster'
import CouponRepository from './coupon'
import EmailRepository from './email'
import ExchangeRepository from './exchange'
import FeedbackRepository from './feedback'
import HistoryRepository from './history'
import InstagramCatalogRepository from './instagram_catalog'
import InventoryRepository from './inventory'
import LocationRepository from './location'
import MatchboxRepository from './matchbox'
import MeasurementRepository from './measurement'
import MigrationRepository from './migration'
import NotificationRepository from './notification'
import OrderBagRepository from './order/bag'
import OrderDetailRepository from './order/detail'
import OrderRepository from './order'
import OrderPaymentRepository from './order/payment'
import PacketRepository from './packet'
import ProductRepository from './product'
import PromotionRepository from './promotion'
import SchedulerRepository from './scheduler'
import PurchaseRepository from './purchase'
import QuestionRepository from './question'
import ServiceRepository from './service'
import ShipmentRepository from './shipment'
import SizeRepository from './size'
import StyleboardRepository from './styleboard'
import StylesheetRepository from './stylesheet'
import TagRepository from './tag'
import TopupRepository from './topup'
import UserAccountRepository from './user/account'
import UserAnswerRepository from './user/answer'
import UserCardRepository from './user/card'
import UserProfileRepository from './user/profile'
import UserReferralRepository from './user/referral'
import UserRequestRepository from './user/request'
import UserWalletRepository from './user/wallet'
import UserPointRepository from './user/point'
import UserWardrobeRepository from './user/wardrobe'
import VariantRepository from './variant'
import VoucherRepository from './voucher'
import StorefrontRepository from './storefront'
import GoogleSheetsRepository from './googlesheets'


export {
	AreaManualRepository,
	AreaSicepatRepository,
	AreaTikiRepository,
	AssetRepository,
	BCARepository,
	BrandRepository,
	CampaignRepository,
	CategoryRepository,
	ColorRepository,
	CollectionRepository,
	CouponRepository,
	ClusterRepository,
	EmailRepository,
	ExchangeRepository,
	FeedbackRepository,
	HistoryRepository,
	InstagramCatalogRepository,
	InventoryRepository,
	LocationRepository,
	MatchboxRepository,
	MeasurementRepository,
	MigrationRepository,
	NotificationRepository,
	OrderBagRepository,
	OrderDetailRepository,
	OrderRepository,
	OrderPaymentRepository,
	PacketRepository,
	ProductRepository,
	PromotionRepository,
	SchedulerRepository,
	PurchaseRepository,
	QuestionRepository,
	ServiceRepository,
	ShipmentRepository,
	SizeRepository,
	StyleboardRepository,
	StylesheetRepository,
	TagRepository,
	TopupRepository,
	UserAccountRepository,
	UserAnswerRepository,
	UserCardRepository,
	UserProfileRepository,
	UserReferralRepository,
	UserRequestRepository,
	UserWalletRepository,
	UserPointRepository,
	UserWardrobeRepository,
	VariantRepository,
	VoucherRepository,
	StorefrontRepository,
	GoogleSheetsRepository,
}
