import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	HistoryClaimRecord,
	HistoryInventoryRecord,
	HistoryOrderDetailRecord,
	HistoryOrderRecord,
	HistoryPurchaseOrderRecord,
	HistoryPurchaseRequestRecord,
	HistoryShipmentRecord,
	HistoryStylesheetRecord,
	HistoryVoucherRecord,
} from 'energie/app/records'

import {
	HistoryClaimInterface,
	HistoryInventoryInterface,
	HistoryOrderDetailInterface,
	HistoryOrderInterface,
	HistoryPurchaseOrderInterface,
	HistoryPurchaseRequestInterface,
	HistoryShipmentInterface,
	HistoryStylesheetInterface,
	HistoryVoucherInterface,
} from 'energie/app/interfaces'
import InterfaceHistoryModel from 'energie/app/models/interface.history'

import { Parameter } from 'types/common'

@EntityRepository()
export default class HistoryRepository extends RepositoryModel<{
	HistoryClaimRecord: HistoryClaimRecord,
	HistoryInventoryRecord: HistoryInventoryRecord,
	HistoryOrderDetailRecord: HistoryOrderDetailRecord,
	HistoryOrderRecord: HistoryOrderRecord,
	HistoryPurchaseOrderRecord: HistoryPurchaseOrderRecord,
	HistoryPurchaseRequestRecord: HistoryPurchaseRequestRecord,
	HistoryShipmentRecord: HistoryShipmentRecord,
	HistoryStylesheetRecord: HistoryStylesheetRecord,
	HistoryVoucherRecord: HistoryVoucherRecord,
}> {

	static __displayName = 'HistoryRepository'

	protected records = {
		HistoryClaimRecord,
		HistoryInventoryRecord,
		HistoryOrderDetailRecord,
		HistoryOrderRecord,
		HistoryPurchaseOrderRecord,
		HistoryPurchaseRequestRecord,
		HistoryShipmentRecord,
		HistoryStylesheetRecord,
		HistoryVoucherRecord,
	}

	// ============================= INSERT =============================
	async record(type: 'CLAIM', data: Parameter<HistoryClaimInterface>): Promise<boolean>
	async record(type: 'INVENTORY', data: Parameter<HistoryInventoryInterface>): Promise<boolean>
	async record(type: 'ORDER_DETAIL', data: Parameter<HistoryOrderDetailInterface>): Promise<boolean>
	async record(type: 'ORDER', data: Parameter<HistoryOrderInterface>): Promise<boolean>
	async record(type: 'PURCHASE_ORDER', data: Parameter<HistoryPurchaseOrderInterface>): Promise<boolean>
	async record(type: 'PURCHASE_REQUEST', data: Parameter<HistoryPurchaseRequestInterface>): Promise<boolean>
	async record(type: 'SHIPMENT', data: Parameter<HistoryShipmentInterface>): Promise<boolean>
	async record(type: 'STYLESHEET', data: Parameter<HistoryStylesheetInterface>): Promise<boolean>
	async record(type: 'VOUCHER', data: Parameter<HistoryVoucherInterface>): Promise<boolean>

	@RepositoryModel.bound
	async record(
		type: 'INVENTORY' | 'ORDER_DETAIL' | 'ORDER' | 'PURCHASE_ORDER' | 'PURCHASE_REQUEST' | 'CLAIM' | 'SHIPMENT' | 'STYLESHEET' | 'VOUCHER',
		data: any,
	): Promise<boolean> {
		return Promise.resolve().then(async () => {
			switch (type) {
			case 'INVENTORY':
				return this.save('HistoryInventoryRecord', data)
			case 'ORDER_DETAIL':
				return this.save('HistoryOrderDetailRecord', data)
			case 'ORDER':
				return this.save('HistoryOrderRecord', data)
			case 'PURCHASE_ORDER':
				return this.save('HistoryPurchaseOrderRecord', data)
			case 'PURCHASE_REQUEST':
				return this.save('HistoryPurchaseRequestRecord', data)
			case 'CLAIM':
				return this.save('HistoryClaimRecord', data)
			case 'SHIPMENT':
				return this.save('HistoryShipmentRecord', data)
			case 'STYLESHEET':
				return this.save('HistoryStylesheetRecord', data)
			case 'VOUCHER':
				return this.save('HistoryVoucherRecord', data)
			}
		}).then(() => true).catch(err => {
			this.warn(err)

			return false
		})
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async get(type: 'CLAIM', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryClaimInterface>
	async get(type: 'CLAIM', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryClaimInterface[]>
	async get(type: 'CLAIM', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryClaimInterface[]>
	async get(type: 'CLAIM', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryClaimInterface[][]>
	async get(type: 'INVENTORY', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryInventoryInterface>
	async get(type: 'INVENTORY', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryInventoryInterface[]>
	async get(type: 'INVENTORY', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryInventoryInterface[]>
	async get(type: 'INVENTORY', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryInventoryInterface[][]>
	async get(type: 'ORDER_DETAIL', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryOrderDetailInterface>
	async get(type: 'ORDER_DETAIL', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryOrderDetailInterface[]>
	async get(type: 'ORDER_DETAIL', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryOrderDetailInterface[]>
	async get(type: 'ORDER_DETAIL', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryOrderDetailInterface[][]>
	async get(type: 'ORDER', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryOrderInterface>
	async get(type: 'ORDER', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryOrderInterface[]>
	async get(type: 'ORDER', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryOrderInterface[]>
	async get(type: 'ORDER', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryOrderInterface[][]>
	async get(type: 'PURCHASE_ORDER', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryPurchaseOrderInterface>
	async get(type: 'PURCHASE_ORDER', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryPurchaseOrderInterface[]>
	async get(type: 'PURCHASE_ORDER', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryPurchaseOrderInterface[]>
	async get(type: 'PURCHASE_ORDER', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryPurchaseOrderInterface[][]>
	async get(type: 'PURCHASE_REQUEST', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryPurchaseRequestInterface>
	async get(type: 'PURCHASE_REQUEST', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryPurchaseRequestInterface[]>
	async get(type: 'PURCHASE_REQUEST', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryPurchaseRequestInterface[]>
	async get(type: 'PURCHASE_REQUEST', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryPurchaseRequestInterface[][]>
	async get(type: 'SHIPMENT', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryShipmentInterface>
	async get(type: 'SHIPMENT', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryShipmentInterface[]>
	async get(type: 'SHIPMENT', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryShipmentInterface[]>
	async get(type: 'SHIPMENT', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryShipmentInterface[][]>
	async get(type: 'STYLESHEET', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryStylesheetInterface>
	async get(type: 'STYLESHEET', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryStylesheetInterface[]>
	async get(type: 'STYLESHEET', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryStylesheetInterface[]>
	async get(type: 'STYLESHEET', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryStylesheetInterface[][]>
	async get(type: 'VOUCHER', id: number, last_only: true | undefined, transaction: EntityManager): Promise<HistoryVoucherInterface>
	async get(type: 'VOUCHER', id: number, last_only: false | undefined, transaction: EntityManager): Promise<HistoryVoucherInterface[]>
	async get(type: 'VOUCHER', ids: number[], last_only: true | undefined, transaction: EntityManager): Promise<HistoryVoucherInterface[]>
	async get(type: 'VOUCHER', ids: number[], last_only: false | undefined, transaction: EntityManager): Promise<HistoryVoucherInterface[][]>

	@RepositoryModel.bound
	async get(
		type: 'CLAIM' | 'INVENTORY' | 'ORDER_DETAIL' | 'ORDER' | 'PURCHASE_ORDER' | 'PURCHASE_REQUEST' | 'SHIPMENT' | 'STYLESHEET' | 'VOUCHER',
		id_or_ids: number | number[],
		last_only: boolean = true,
		transaction?: EntityManager,
	): Promise<any> {
		const table = this.table(type)

		return this.query(`history.${table}`, table, transaction)
			.where(`${table}.${table}_id IN (:...id_or_ids)`, { id_or_ids: Array.isArray(id_or_ids) ? id_or_ids : [id_or_ids] })
			.orderBy(`${table}.created_at`, last_only ? 'DESC' : 'ASC')
			.getMany()
			.then(this.parser)
			.then((histories: InterfaceHistoryModel[]) => {
				if(Array.isArray(id_or_ids)) {
					if(last_only) {
						return id_or_ids.map(id => {
							return histories.find(history => history[`${table}_id`] === id) || null
						})
					} else {
						return id_or_ids.map(id => {
							return histories.filter(history => history[`${table}_id`] === id)
						})
					}
				} else {
					if(last_only) {
						return histories.find(history => history[`${table}_id`] === id_or_ids) || null
					} else {
						return histories
					}
				}
			})
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES =============================
	private table(type: 'INVENTORY' | 'ORDER_DETAIL' | 'ORDER' | 'PURCHASE_ORDER' | 'PURCHASE_REQUEST' | 'CLAIM' | 'SHIPMENT' | 'STYLESHEET' | 'VOUCHER') {
		switch(type) {
		case 'INVENTORY':
			return 'inventory'
		case 'ORDER_DETAIL':
			return 'order_detail'
		case 'ORDER':
			return 'order'
		case 'PURCHASE_ORDER':
			return 'purchase_order'
		case 'PURCHASE_REQUEST':
			return 'purchase_request'
		case 'CLAIM':
			return 'claim'
		case 'SHIPMENT':
			return 'shipment'
		case 'STYLESHEET':
			return 'stylesheet'
		case 'VOUCHER':
			return 'voucher'
		}
	}

}
