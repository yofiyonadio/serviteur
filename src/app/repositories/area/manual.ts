import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	AreaManualRecord,
} from 'energie/app/records'

import { AreaManualInterface } from 'energie/app/interfaces'


@EntityRepository()
export default class AreaManualRepository extends RepositoryModel<{
	AreaManualRecord: AreaManualRecord,
}> {

	static __displayName = 'AreaManualRepository'

	protected records = {
		AreaManualRecord,
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getById(
		ids: number [],
		transaction: EntityManager,
	): Promise<AreaManualInterface[]> {
		return this.getMany('AreaManualRecord', {
			id: In(ids),
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getByLocationId(
		location_ids: number[],
		transaction: EntityManager,
	): Promise<AreaManualInterface[]> {
		return this.getMany('AreaManualRecord', {
			location_id: In(location_ids),
		}, {
			transaction,
		})
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
