import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
	In,
} from 'typeorm'

import {
	AreaSicepatRecord,
	AreaSicepatLocationRecord,
	OtherSicepatWaybillRecord,
} from 'energie/app/records'

import {
	AreaSicepatInterface, OtherSicepatWaybillInterface,
	// OtherSicepatWaybillInterface,
} from 'energie/app/interfaces'

import InterfaceAddressModel from 'energie/app/models/interface.address'
import { Parameter } from 'types/common'
import { ErrorModel } from 'app/models'
import { ERRORS, ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class AreaSicepatRepository extends RepositoryModel<{
	AreaSicepatRecord: AreaSicepatRecord,
	AreaSicepatLocationRecord: AreaSicepatLocationRecord,
	OtherSicepatWaybillRecord: OtherSicepatWaybillRecord,
}> {

	static __displayName = 'AreaSicepatRepository'

	protected records = {
		AreaSicepatRecord,
		AreaSicepatLocationRecord,
		OtherSicepatWaybillRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(sicepatAreas: AreaSicepatInterface[], transaction: EntityManager) {
		return this.saveBig('area', 'sicepat', sicepatAreas, transaction)
	}

	@RepositoryModel.bound
	async combine(datas: Array<{location_id: number, sicepat_id: number}>, transaction: EntityManager) {
		return this.queryCustom('DELETE FROM area.sicepat_location', undefined, transaction)
			.then(() => this.chain(datas, transaction))
	}
	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async markWaybill(transaction: EntityManager): Promise<string> {
		return await this.getUnusedWaybillId(transaction)
			.then(async waybill => {
				await this.renew('OtherSicepatWaybillRecord', waybill.id, {
					used_at: new Date(),
				}, {
					transaction,
				})

				return waybill.waybill
			})
			.catch(err => {
				throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Waybill Empty')
			})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getUnusedWaybillId(transaction: EntityManager): Promise<{id: number, waybill: string}> {
		return this.query('other.sicepat_waybill', 'sw', transaction)
			.where('sw.used_at is null')
			.getOne()
			.then(this.parser)
			.then((result: {id: number, waybill: string}) => {
				return {
					id: result.id,
					waybill: result.waybill,
				}
			}) as any
	}

	@RepositoryModel.bound
	async getByWaybill(awb: string, transaction: EntityManager): Promise<OtherSicepatWaybillInterface> {
		return this.query('other.sicepat_waybill', 'sw', transaction)
			.where('sw.waybill = :awb', {awb})
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getById(
		id_or_ids: number | number[],
		transaction: EntityManager,
	): Promise<AreaSicepatInterface[]> {
		return this.getMany('AreaSicepatRecord', {id: Array.isArray(id_or_ids) ? In(id_or_ids) : id_or_ids}, {transaction})
	}

	@RepositoryModel.bound
	async getByLocationId(
		location_id_or_ids: number | number[],
		origin_or_destination: string,
		transaction: EntityManager,
	): Promise<AreaSicepatInterface[]> {
		let Q = this.query('area.sicepat', 's', transaction)

		switch(origin_or_destination) {
		case 'origin':
			Q = Q.where(`s.slug = 'origin'`)
				.andWhere(`s.origin_name % (select l.province_name from master."location" l where id in (${location_id_or_ids}))`)
			break
		case 'destination':
				Q = Q.leftJoin('sicepat_location', 'sl', 's.id = sl.sicepat_id')
					.where(`sl.location_id in (${location_id_or_ids})`)
					.andWhere(`s.slug = '${origin_or_destination}'`)
			break
		}

		return Q = Q.getMany()
			.then(this.parser)
			.then((results: AreaSicepatInterface[]) => results.map(result => {
				return {
					id: result.id,
					created_at: result.created_at,
					updated_at: result.updated_at,
					slug: result.slug,
					subdistrict: result.subdistrict,
					city: result.city,
					province: result.province,
					origin_name: result.origin_name,
					code: result.code,
				}
			})) as any
	}

	@RepositoryModel.bound
	async getCompleteAddress(
		address: Parameter<InterfaceAddressModel>,
		transaction: EntityManager,
	): Promise<{
		title: string,
		receiver: string,
		phone: string,
		address: string,
		district: string,
		city: string,
		province: string,
		postal: string,
		code: string,
	}> {
		return this.query('area.sicepat', 's', transaction)
			.leftJoin('sicepat_location', 'sl', 's.id = sl.sicepat_id')
			.where(`sl.location_id in (${address.location_id})`)
			.andWhere(`s.slug = 'destination'`)
			.getOne()
			.then(this.parser)
			.then((res: AreaSicepatInterface) => {
				return {
					title: address.title,
					receiver: address.receiver,
					phone: address.phone,
					address: address.address,
					district: res.subdistrict,
					city: res.city,
					province: res.province,
					postal: address.postal,
					code: res.code,
				}
			}) as any
	}

	// async getLocationSicepatIds(transaction: EntityManager): Promise<Array<{
	// 	location_id: number,
	// 	sicepat_id: number,
	// }>> {
	// 	return await this.queryCustom(
			// `select ` + `
			// 	l.id location_id,
			// 	s.id sicepat_id
			// from master."location" l ` + `
			// left join other.sicepat_area s on ` + `
			// 	lower(s.subdistrict) = lower(l.suburb_name) ` + `
			// 	and lower(l.province_name) = lower(s.province) ` + `
			// 	and (case ` + `
			// 			when position(',' in l.city_name) > 0 ` + `
			// 			then lower(left(l.city_name, position(',' in l.city_name)-1)) = lower(right(s.city, -5)) ` + `
			// 			else lower(l.city_name) = lower(right(s.city, -5)) ` + `
			// 		end) ` + `
			// where s.id is not null and s.code not in ('BYO10000', 'CJR10000', 'JPR10000')`
	// 	, undefined, transaction)
	// }

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

	private async chain(datas: Array<{location_id: number, sicepat_id: number}>, transaction: EntityManager) {
		const chunk = datas.splice(0, 2000)

		this.queryInsert('area.sicepat_location', transaction)
			.values(chunk)
			.execute()
			.then(result => this.log(`done chaining #${result.identifiers.length} location_id to sicepat_id`))
			.catch(this.warn)

		return datas.length ? this.chain(datas, transaction) : true
	}

}
