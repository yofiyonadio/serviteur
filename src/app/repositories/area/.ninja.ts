// import RepositoryModel from 'app/models/repository'
// import { EntityRepository, EntityManager, In } from 'typeorm'

// import {
// 	OtherNinjaAreaRecord,
// } from 'energie/app/records'

// import { OtherNinjaAreaInterface } from 'energie/app/interfaces'

// import { Parameter } from 'types/common'

// @EntityRepository()
// export default class OtherNinjaRepository extends RepositoryModel<{
// 	OtherNinjaAreaRecord: OtherNinjaAreaRecord,
// }> {

// 	static __displayName = 'OtherNinjaRepository'

// 	protected records = {
// 		OtherNinjaAreaRecord,
// 	}

// 	// ============================= INSERT =============================
// 	insert(tikiArea: OtherNinjaAreaInterface[] | undefined, transaction: EntityManager): Promise<boolean> {
// 		return this.saveBig('other', 'ninja_area', tikiArea, transaction)
// 	}
// 	// ============================= UPDATE =============================
// 	update(
// 		id: number,
// 		update: Partial<Parameter<OtherNinjaAreaInterface>>,
// 		transaction: EntityManager,
// 	) {
// 		return this.renew('OtherNinjaAreaRecord', id, update, {
// 			transaction,
// 		})
// 	}

// 	// ============================= GETTER =============================
// 	getById(
// 		id_or_ids: number | number [],
// 		transaction: EntityManager,
// 	): Promise<OtherNinjaAreaInterface[]> {
// 		return this.getMany('OtherNinjaAreaRecord', {
// 			id: Array.isArray(id_or_ids) ? In(id_or_ids) : id_or_ids,
// 		}, {
// 			transaction,
// 		})
// 	}

// 	getByLocationId(
// 		location_id_or_ids: number | number[],
// 		transaction: EntityManager,
// 	): Promise<OtherNinjaAreaInterface[]> {
// 		return this.getMany('OtherNinjaAreaRecord', {
// 			location_id: Array.isArray(location_id_or_ids) ? In(location_id_or_ids) : location_id_or_ids,
// 		}, {
// 			transaction,
// 		})
// 	}

// 	getAny(transaction: EntityManager): Promise<{ id: number}> {
// 		return this.query('other.ninja_area', 'tiki_area', transaction)
// 			.select('tiki_area.id')
// 			.getOne()
// 			.then(this.parser) as any
// 	}

// 	getLast(transaction: EntityManager): Promise<{ id: number}> {
// 		return this.query('other.ninja_area', 'ninja_area', transaction)
// 			.select('ninja_area.id')
// 			.orderBy('ninja_area.id', 'DESC')
// 			.getOne()
// 			.then(this.parser) as any
// 	}

// 	getUnsetLocationId(transaction: EntityManager): Promise<OtherNinjaAreaInterface[]> {
// 		return this.query('other.ninja_area', 'ninja_area', transaction)
// 			.where('ninja_area.location_id IS NULL')
// 			.getMany()
// 			.then(this.parser) as any
// 	}

// 	// ============================= DELETE =============================

// 	// ============================ PRIVATES ============================

// }
