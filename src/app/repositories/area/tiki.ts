import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	AreaTikiRecord,
} from 'energie/app/records'

import { AreaTikiInterface } from 'energie/app/interfaces'

import { Parameter } from 'types/common'

@EntityRepository()
export default class AreaTikiRepository extends RepositoryModel<{
	AreaTikiRecord: AreaTikiRecord,
}> {

	static __displayName = 'AreaTikiRepository'

	protected records = {
		AreaTikiRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(tikiArea: AreaTikiInterface[] | undefined, transaction: EntityManager): Promise<boolean> {
		return this.saveBig('other', 'tiki_area', tikiArea, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		id: number,
		update: Partial<Parameter<AreaTikiInterface>>,
		transaction: EntityManager,
	) {
		return this.renew('AreaTikiRecord', id, update, {
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getById(
		id_or_ids: number | number [],
		transaction: EntityManager,
	): Promise<AreaTikiInterface[]> {
		return this.getMany('AreaTikiRecord', {
			id: Array.isArray(id_or_ids) ? In(id_or_ids) : id_or_ids,
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getByLocationId(
		location_id_or_ids: number | number[],
		transaction: EntityManager,
	): Promise<AreaTikiInterface[]> {
		return this.getMany('AreaTikiRecord', {
			location_id: Array.isArray(location_id_or_ids) ? In(location_id_or_ids) : location_id_or_ids,
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getAny(transaction: EntityManager): Promise<{ id: number}> {
		return this.query('other.tiki_area', 'tiki_area', transaction)
			.select('tiki_area.id')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLast(transaction: EntityManager): Promise<{ id: number}> {
		return this.query('other.tiki_area', 'tiki_area', transaction)
			.select('tiki_area.id')
			.orderBy('tiki_area.id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUnsetLocationId(transaction: EntityManager): Promise<AreaTikiInterface[]> {
		return this.query('other.tiki_area', 'tiki_area', transaction)
			.where('tiki_area.location_id IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
