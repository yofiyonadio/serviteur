import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	CategoryQuestionRecord,
	QuestionGroupRecord,
	QuestionRecord,
} from 'energie/app/records'

import {
	QuestionInterface, CategoryQuestionInterface,
} from 'energie/app/interfaces'

import { CommonHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { QUESTIONS } from 'energie/utils/constants/enum'
import { Parameter } from 'types/common'


@EntityRepository()
export default class QuestionRepository extends RepositoryModel<{
	CategoryQuestionRecord: CategoryQuestionRecord,
	QuestionGroupRecord: QuestionGroupRecord,
	QuestionRecord: QuestionRecord,
}> {

	static __displayName = 'QuestionRepository'

	protected records = {
		CategoryQuestionRecord,
		QuestionGroupRecord,
		QuestionRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		id: number,
		title: string,
		description: string,
		questions: Array<Parameter<QuestionInterface> & {
			id: number,
			category_ids?: number[],
		}>,
		transaction: EntityManager,
	): Promise<boolean> {
		const group = await this.saveOrUpdate('QuestionGroupRecord', [{
			id,
			title,
			description,
		}], transaction)

		await this.saveOrUpdate('QuestionRecord', questions.map(q => {
			return CommonHelper.stripKey(q, 'category_ids')
		}), transaction)

		const questionWithCategories = questions.filter(q => q.category_ids ? q.category_ids.length : false)

		if(questionWithCategories.length) {
			await this.save('CategoryQuestionRecord', questionWithCategories.map(q => {
				return q.category_ids.map(cId => {
					return {
						question_id: q.id,
						category_id: cId,
					}
				})
			}).reduce(CommonHelper.sumArray, []), transaction)
		}

		return group
	}

	@RepositoryModel.bound
	async insertQuestion(
		questions: Array<(Parameter<QuestionInterface> & {
			id: number,
			category_ids?: number[],
		}) | Partial<QuestionInterface> & {
			id: number,
		}>,
		transaction: EntityManager,
	): Promise<boolean> {
		await this.saveOrUpdate('QuestionRecord', questions.map(q => {
			if ((q as any).category_ids) {
				return CommonHelper.stripKey(q as any, 'category_ids')
			} else {
				return q
			}
		}), transaction)

		const questionWithCategories = questions.filter(q => (q as any).category_ids ? (q as any).category_ids.length : false)

		if (questionWithCategories.length) {
			await this.save('CategoryQuestionRecord', questionWithCategories.map(q => {
				return (q as any).category_ids.map(cId => {
					return {
						question_id: q.id,
						category_id: cId,
					}
				})
			}).reduce(CommonHelper.sumArray, []), transaction)
		}

		return true
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async getGroup(id: number | undefined, transaction: EntityManager): Promise<{
		id: number,
		title: string,
		description: string,
	}>
	async getGroup(ids: number[] | undefined, transaction: EntityManager): Promise<Array<{
		id: number,
		title: string,
		description: string,
	}>>

	@RepositoryModel.bound
	async getGroup(idOrIds: number | number[] | undefined, transaction: EntityManager): Promise<any> {
		if (Array.isArray(idOrIds)) {
			return this.getMany('QuestionGroupRecord', {
				id: In(idOrIds),
			}, {
				select: ['id', 'title', 'description'],
				transaction,
			})
		} else if (idOrIds) {
			return this.getOne('QuestionGroupRecord', {
				id: idOrIds,
			}, {
				select: ['id', 'title', 'description'],
				transaction,
			})
		} else {
			return this.getMany('QuestionGroupRecord', {
				id: In(DEFAULTS.STYLE_PROFILE_QUESTION_GROUP_ID),
			}, {
				select: ['id', 'title', 'description'],
				transaction,
			})
		}
	}

	@RepositoryModel.bound
	async getQuestionsPerGroup<N extends number | false, Return extends QuestionInterface & {questions: Array<QuestionInterface & {questions: QuestionInterface[]}>}>(
		filter: {
			group_id?: N,
			question_ids?: number[],
		},
		transaction: EntityManager,
	): Promise<N extends number ? Return : Return[]> {
		let Q = this.query('master.question_group', 'group', transaction)
			.leftJoinAndMapMany('group.questions', QuestionRecord, 'parentQuestion', 'parentQuestion.question_group_id = group.id and parentQuestion.deleted_at is null')
			.leftJoinAndMapMany('parentQuestion.questions', QuestionRecord, 'childQuestion', 'childQuestion.question_id = parentQuestion.id and childQuestion.deleted_at is null')
			.where('group.id in (1, 2, 3, 4)')

			if (filter.question_ids) {
				Q = Q.andWhere('childQuestion.id in (:...question_ids)', {question_ids: filter.question_ids})
			}

			if (filter.group_id) {
				return Q.andWhere('group.id = :group_id', {group_id: filter.group_id})
				.orderBy('group.id', 'ASC')
				.addOrderBy('parentQuestion.order', 'ASC')
				.addOrderBy('childQuestion.order', 'ASC')
				.getOne()
				.then(this.parser) as any
			} else {
				return Q.orderBy('group.id')
				.addOrderBy('parentQuestion.id')
				.addOrderBy('childQuestion.order')
				.getMany()
				.then(this.parser) as any
			}
	}

	@RepositoryModel.bound
	async getQuestionsByGroupId(id: number | number[], transaction: EntityManager): Promise<Array<QuestionInterface & {
		questionCategories: CategoryQuestionInterface[],
		questions: Array<QuestionInterface & {
			questionCategories: CategoryQuestionInterface[],
		}>,
	}>> {
		let Q = this.query('master.question', 'question', transaction)
			.leftJoinAndSelect('question.questions', 'questions', 'questions.deleted_at IS NULL')
			.leftJoinAndSelect('question.questionCategories', 'questionCategories')
			.leftJoinAndSelect('questions.questionCategories', 'questionsCategories')

		if(Array.isArray(id)) {
			Q = Q.where('question.question_group_id in (:...id)', { id })
		} else {
			Q = Q.where('question.question_group_id = :id', { id })
		}


		return Q.andWhere('question.deleted_at IS NULL')
			.orderBy('question.order')
			.addOrderBy('questions.order')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getQuestions(ids: number[] | undefined, transaction: EntityManager): Promise<Array<QuestionInterface & {
		questions: QuestionInterface[],
	}>> {
		if (ids) {
			return this.query('master.question', 'question', transaction)
				.leftJoinAndSelect('question.questions', 'questions', 'questions.deleted_at IS NULL')
				.where('question.id IN (:...ids)', { ids })
				.andWhere('question.deleted_at IS NULL')
				.orderBy('question.order')
				.addOrderBy('questions.order')
				.getMany()
				.then(this.parser) as any

		} else {
			// GET ALL
			return this.query('master.question', 'question', transaction)
				.leftJoinAndSelect('question.questions', 'questions', 'questions.deleted_at IS NULL')
				.where('question.question_group_id IS NOT NULL')
				.andWhere('question.deleted_at IS NULL')
				.orderBy('question.order')
				.addOrderBy('questions.order')
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getQuestionType(
		question_id: number,
		transaction: EntityManager,
	): Promise<QUESTIONS> {
		return this.query('master.question', 'question', transaction)
			.select('question.type')
			.where('question.id = :question_id', { question_id })
			.getOne()
			.then(this.parser)
			.then((q: QuestionInterface) => q.type)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
