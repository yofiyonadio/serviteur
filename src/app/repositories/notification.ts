import {
	// ErrorModel,
	RepositoryModel, ErrorModel,
} from 'app/models'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	CouponRecord,
	NotificationRecord,
	UserMessageRecord,
	UserNotificationRecord,
	UserRecord,
} from 'energie/app/records'

import {
	CouponInterface,
	NotificationInterface,
	UserInterface,
	UserMessageInterface,
} from 'energie/app/interfaces'

import {
	Parameter,
} from 'types/common'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { NOTIFICATIONS } from 'energie/utils/constants/enum'
import { TimeHelper } from 'utils/helpers'


@EntityRepository()
export default class NotificationRepository extends RepositoryModel<{
	CouponRecord: CouponRecord,
	NotificationRecord: NotificationRecord,
	UserMessageRecord: UserMessageRecord,
	UserNotificationRecord: UserNotificationRecord,
}> {

	static __displayName = 'NotificationRepository'

	protected records = {
		CouponRecord,
		NotificationRecord,
		UserMessageRecord,
		UserNotificationRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insertNotification(
		data: Parameter<NotificationInterface>,
		user_ids: number[] = [],
		coupon_ids: number[] = [],
		transaction: EntityManager): Promise<NotificationInterface> {
		// TODO, MUST INCLUDE ASSETS, COUPONS, USERS
		return this.save('NotificationRecord', data, transaction)
			.then(async res => {
				if(user_ids.length > 0) {
					await this.addToNotification(res.id, 'user', user_ids, transaction)
				}

				if(coupon_ids.length > 0) {
					await this.addToNotification(res.id, 'coupon', coupon_ids, transaction)
				}

				return res
			})
			.catch(err => {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, err.detail)
			})
	}

	@RepositoryModel.bound
	async insertMessage(data: Parameter<UserMessageInterface>, transaction: EntityManager): Promise<UserMessageInterface> {
		// TODO: MUST INCLUDE ASSETS, COUPONS
		return this.save('UserMessageRecord', data, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async seeMessage(message_id: number, user_id: number, transaction: EntityManager): Promise<boolean> {
		return this.renew('UserMessageRecord', `id = :id AND user_id = :user_id`, {
			seen_at: new Date(),
		}, {
			parameters: {
				id: message_id,
				user_id,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async seeNotification(notification_id: number, user_id: number, transaction: EntityManager): Promise<boolean> {
		return this.renew('UserNotificationRecord', `notification_id = :notification_id AND user_id = :user_id`, {
			seen_at: new Date(),
		}, {
			parameters: {
				notification_id,
				user_id,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async updateNotification(
		id: number,
		data: Partial<Parameter<NotificationInterface>>,
		user_ids: number[] = [],
		coupon_ids: number[] = [],
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('NotificationRecord', id, data, {transaction})
			.then(async () => {
				if(user_ids.length > 0) {
					await this.removeFromNotification('user', id, transaction)
					.then(() => {
						return this.addToNotification(id, 'user', user_ids, transaction)
					})
				}

				if(coupon_ids.length > 0) {
					await this.removeFromNotification('coupon', id, transaction)
					.then(() => {
						return this.addToNotification(id, 'coupon', coupon_ids, transaction)
					})
				}

				return true
			})
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	@RepositoryModel.bound
	async updateOrder(
		id: number,
		order: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.notification', transaction)
			.set({ order })
			.where('notification.id = :id', { id })
			.execute()
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getScheduledNotification(
		transaction: EntityManager,
	): Promise<Array<NotificationInterface & {
		users: UserInterface[],
	}>> {
		return this.query('app.notification', 'notif', transaction)
			.leftJoin(UserNotificationRecord, 'userNotif', 'userNotif.notification_id = notif.id')
			.leftJoinAndMapMany('notif.users', UserRecord, 'user', 'user.id = userNotif.user_id')
			.where('notif.push_notification is true')
			.andWhere('notif.pushed_at is null')
			.andWhere('notif.published_at < now()')
			.andWhere('user.id is not null')
			.getMany() as any
	}

	@RepositoryModel.bound
	async filterNotification(
		offset: number = 0,
		limit: number = 12,
		filter: {
			slug?: string,
			search?: string,
			type?: NOTIFICATIONS,
			publish_date?: Date,
			push_notification?: boolean,
			is_featured?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<NotificationInterface & {
		coupon: CouponInterface[],
	}> {
		let Q = this.query('app.notification', 'notification', transaction)
			.leftJoin('notification.notificationCoupons', 'nc')
			.leftJoinAndMapMany('notification.coupons', CouponRecord, 'coupon', 'coupon.id = nc.coupon_id')
			.orderBy('notification.order', 'ASC', 'NULLS LAST')
			.addOrderBy('notification.published_at', 'DESC', 'NULLS LAST')
			.where('TRUE')

		if(filter.slug) {
			Q = Q.andWhere('notification.slug ILIKE :slug', { slug: `%${filter.slug}` })
		}

		if(filter.search) {
			Q = Q.andWhere('notification.title ILIKE :search', { search: `%${filter.search}%` })
		}

		if(filter.type) {
			Q = Q.andWhere('notification.type = :type', { type: filter.type })
		}

		if(filter.publish_date) {
			Q = Q.andWhere('notification.published_at BETWEEN :start AND :end', {
				start: TimeHelper.moment(filter.publish_date).startOf('d').toDate(),
				end: TimeHelper.moment(filter.publish_date).endOf('d').toDate(),
			})
		}

		Q.addOrderBy('notification.order', 'ASC')
		Q.addOrderBy('notification.created_at', 'DESC')

		if(filter.push_notification) {
			Q = Q.andWhere('notification.push_notification IS :push_notif', {push_notif: filter.push_notification})
		}

		if(filter.is_featured) {
			Q = Q.andWhere('notification.is_featured IS :is_featured', {is_featured: filter.is_featured})
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async getMessageAndNotifications(user_id: number | undefined, is_featured: boolean | undefined, transaction: EntityManager): Promise<Array<(NotificationInterface & {
		group: 'NOTIFICATION',
		coupons?: CouponInterface,
	}) | (UserMessageInterface & {
		group: 'MESSAGE'
		coupons?: CouponInterface,
	})>> {
		const Q = this.query('app.notification', 'notification', transaction)
			.leftJoin('notification.notificationCoupons', 'nc')
			.leftJoinAndMapMany('notification.coupons', CouponRecord, 'coupon', 'coupon.id = nc.coupon_id')
			.where('(notification.published_at IS NULL OR notification.published_at <= current_timestamp)')
			.andWhere('(notification.expired_at IS NULL OR notification.expired_at > current_timestamp)')
			.andWhere(sq => {
				let sql = '(notification.id NOT IN ' + sq.subQuery()
					.select('uN.notification_id')
					.from('app.user_notification', 'uN')
					.getSql()

				if(user_id !== undefined) {
					sql = sql + ' OR notification.id IN' + sq.subQuery()
						.select('uN.notification_id')
						.from('app.user_notification', 'uN')
						.where(`uN.user_id = ${user_id}`)
						.getSql() + ')'
				} else {
					sql = sql + ')'
				}

				return  sql
			})

		if(is_featured) {
			Q.andWhere('notification.is_featured IS TRUE')
				.orderBy('notification.order', 'ASC')
				.addOrderBy('notification.created_at', 'DESC')
		} else {
			Q.orderBy('notification.created_at', 'DESC')
		}

		return Q.getMany()
			.then((notifications: Array<NotificationInterface & {
				coupons?: CouponInterface,
			}>) => {
				if (user_id !== undefined) {
					return this.query('app.user_message', 'message', transaction)
						.leftJoin('message.userMessageCoupons', 'umc')
						.leftJoinAndMapMany('message.coupons', CouponRecord, 'coupon', 'coupon.id = umc.coupon_id')
						.where('message.user_id = :user_id', { user_id })
						.andWhere('message.published_at < current_timestamp')
						.andWhere('(message.expired_at IS NULL OR message.expired_at > current_timestamp)')
						.orderBy('message.created_at', 'DESC')
						.getMany()
						.then((messages: Array<UserMessageInterface & {
							coupon?: CouponInterface,
						}>) => {
							return ([]).concat(messages.map(message => {
								return {
									...message,
									group: 'MESSAGE',
								}
							})).concat(notifications.map(notification => {
								return {
									...notification,
									group: 'NOTIFICATION',
								}
							})).sort((a, b) => {
								return +b.created_at - +a.created_at
							})
						})
				} else {
					return notifications.map(notification => {
						return {
							...notification,
							group: 'NOTIFICATION',
						}
					})
				}
			})
	}

	@RepositoryModel.bound
	async getNotifications(
		transaction: EntityManager
	) {
		const Q = this.query('app.notification', 'notification', transaction)
			.where('notification.order is NOT NULL')
			.orderBy('notification.order', 'ASC')
		return Q.getMany()
	}

	@RepositoryModel.bound
	async getUnreadMessageAndNotifications(user_id: number | undefined, transaction: EntityManager): Promise<{
		messages: number[],
		notifications: number[],
	}> {
		return this.query('app.notification', 'notification', transaction)
			.select('notification.id')
			.where('(notification.published_at IS NULL OR notification.published_at <= current_timestamp)')
			.andWhere('(notification.expired_at IS NULL OR notification.expired_at > current_timestamp)')
			.andWhere(sq => {
				let sql = '(notification.id NOT IN ' + sq.subQuery()
					.select('uN.notification_id')
					.from('app.user_notification', 'uN')
					.getSql()

				if(user_id !== undefined) {
					sql = sql + ' OR notification.id IN ' + sq.subQuery()
						.select('uN.notification_id')
						.from('app.user_notification', 'uN')
						.where(`uN.user_id = ${user_id}`)
						.getSql() + ')'
				} else {
					sql = sql + ')'
				}

				return  sql
			})
			.getMany()
			.then((notifications: Array<{ id: number }>) => {
				if (user_id !== undefined) {
					return this.query('app.user_message', 'message', transaction)
						.select('message.id')
						.where('message.user_id = :user_id', { user_id })
						.andWhere('message.published_at < current_timestamp')
						.andWhere('(message.expired_at IS NULL OR message.expired_at > current_timestamp)')
						.andWhere('message.seen_at IS NULL')
						.getMany()
						.then((messages: Array<{ id: number }>) => {
							return {
								messages: messages.map(m => m.id),
								notifications: notifications.map(n => n.id),
							}
						})
				} else {
					return {
						messages: [],
						notifications: notifications.map(n => n.id),
					}
				}
			})
	}

	@RepositoryModel.bound
	async getMessage(id: number, user_id: number, transaction: EntityManager): Promise<UserMessageInterface & {
		coupons?: CouponInterface[],
	}> {
		return this.query('app.user_message', 'message', transaction)
			.leftJoin('message.userMessageCoupons', 'umc')
			.leftJoinAndMapMany('message.coupons', CouponRecord, 'coupon', 'coupon.id = umc.coupon_id')
			.where('message.id = :id AND message.user_id = :user_id', { id, user_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getNotification<T extends boolean>(idOrSlug: number | string, with_coupon: T, transaction: EntityManager): Promise<NotificationInterface & (T extends true ? {
		coupons?: CouponInterface[],
	} : {})> {
		let Q = this.query('app.notification', 'notification', transaction)
			.where(typeof idOrSlug === 'number' ? 'notification.id = :idOrSlug' : 'notification.slug = :idOrSlug', { idOrSlug })

		if (with_coupon) {
			Q = Q.leftJoin('notification.notificationCoupons', 'nc')
				.leftJoinAndMapMany('notification.coupons', CouponRecord, 'coupon', 'coupon.id = nc.coupon_id')
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderedNofication(
		transaction: EntityManager,
	) {
		let Q = this.query('app.notification', 'notification', transaction)
			.where('notification.order IS NOT NULL')
			.orderBy('notification.order', 'ASC')
			.addOrderBy('notification.updated_at', 'DESC')
		
		return Q
			.getManyAndCount()
			.then(this.parseCount) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================
	async addToNotification(notification_id: number, type: 'user' | 'coupon', type_ids: number[], transaction: EntityManager) {
		await this.queryInsert(type === 'user' ? 'app.user_notification' : 'app.notification_coupon', transaction)
			.values(type_ids.map(type_id => {
				return {
					notification_id,
					[`${type}_id`]: type_id,
				}
			})).execute()
	}

	private async removeFromNotification(type: 'user' | 'coupon', notification_id: number, transaction: EntityManager) {
		return this.queryDelete(type === 'user' ? 'app.user_notification' : 'app.notification_coupon', transaction)
			.where('notification_id = :notification_id', {notification_id})
			.execute()
	}
}
