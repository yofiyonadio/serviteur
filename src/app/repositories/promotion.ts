import RepositoryModel from 'app/models/repository'
import { EntityManager, EntityRepository } from 'typeorm'


import {
	PromotionRecord,
	VariantAssetRecord,
	PromotionVariantRecord,
	VariantRecord,
	ProductRecord,
	BrandRecord,
	SizeRecord,
	ColorRecord,
	InventoryRecord,
} from 'energie/app/records'


import {
	VariantInterface,
	PromotionInterface,
	PromotionVariantInterface,
	VariantAssetInterface,
	ProductInterface,
	BrandInterface,
	SizeInterface,
	ColorInterface,
	InventoryInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'
import { COUPONS, INVENTORIES } from 'energie/utils/constants/enum'


@EntityRepository()
export default class PromotionRepository extends RepositoryModel<{
	PromotionRecord: PromotionRecord,
	PromotionVariantRecord: PromotionVariantRecord,
}> {
	static __displayName = 'PromotionRepository'

	protected records = {
		PromotionRecord,
		PromotionVariantRecord,
	}

	// ============================= INSERT =============================

	@RepositoryModel.bound
	async insert(
		promotion: Parameter<PromotionInterface>,
		transaction: EntityManager,
	): Promise<PromotionInterface> {
		return this.save('PromotionRecord', promotion, transaction)
	}

	@RepositoryModel.bound
	async insertVariant(
		variant_id: number,
		promotion_id: number,
		is_active: boolean = true,
		type: COUPONS = COUPONS.TOTAL,
		amount: number,
		transaction: EntityManager,
	): Promise<any> {
		return await this.save('PromotionVariantRecord', {variant_id, promotion_id, is_active, type, amount}, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		id: number,
		promotion: Parameter<PromotionInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('PromotionRecord', id, CommonHelper.stripUndefined(promotion), {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updateVariant(
		id: number,
		promotionVariant: Parameter<PromotionVariantInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('PromotionVariantRecord', id, CommonHelper.stripUndefined(promotionVariant), {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updateAll(
		promotion_id: number,
		is_active: boolean,
		type: COUPONS,
		amount: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.promotion_variant', transaction)
			.set({
				is_active,
				type,
				amount,
			})
			.where('promotion_id = :promotion_id', { promotion_id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================

	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			is_active?: boolean,
			published?: boolean,
			expired?: boolean,
		},
		transaction: EntityManager,	
	): Promise<{
		data: Array<PromotionInterface & {
			promotionVariant: Array<PromotionVariantInterface & {
				variant: VariantInterface & {
					asset?: VariantAssetInterface,
				},
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.promotion', 'promotion', transaction)
			.addSelect("CASE WHEN promotion.published_at > now() then 1 when promotion.published_at < now() AND (promotion.expired_at > now() OR promotion.expired_at IS NULL) then 2 else 3 end", "is_active")
		    // .addSelect("CASE WHEN promotion.published_at < now() then 1 WHEN promoAND (promotion.expired_at > now() OR promotion.expired_at IS NULL) then 1 else 2 end", "date_group")
			.orderBy('is_active')
			.addOrderBy('promotion.published_at', 'DESC')
			// .leftJoinAndMapMany('promotion.promotionVariant', PromotionVariantRecord, 'promotionVariant', 'promotionVariant.promotion_id = promotion.id')
			// .leftJoinAndMapOne('promotionVariant.variant', VariantRecord, 'variant', 'variant.id = promotionVariant.variant_id')
			// .leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at is null')
		
		if (!!search) {
			Q = Q.andWhere(`promotion.title ILIKE '%${search}%'`)
		}

		if (filter.is_active === true) {
			Q = Q.andWhere('promotion.published_at < now()')
				.andWhere('(promotion.expired_at > now() OR promotion.expired_at is null)')
		} else if (filter.is_active === false) {
			Q = Q.andWhere('promotion.published_at > now() OR promotion.expired_at < now()')
		}

		if (filter.published === true) {
			Q = Q.andWhere('promotion.published_at <= now()')
		}

		if (filter.expired === true) {
			Q = Q.andWhere('promotion.expired_at is not null AND promotion.expired_at < now()')
		}

		if (!!limit) {
			Q = Q.take(limit)
		}

		if (!!offset) {
			Q = Q.skip(offset)
		}

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any

	}

	@RepositoryModel.bound
	async getVariantForList(
		promotion_id: number,
		transaction: EntityManager,
	) {
		let Q = this.query('app.promotion_variant', 'promotionVariant', transaction)
			.leftJoinAndMapOne('promotionVariant.variant', VariantRecord, 'variant', 'variant.id = promotionVariant.variant_id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at is null')
			.where('promotionVariant.promotion_id = :promotion_id', { promotion_id })
			.take(6)
		
		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async filterFlat(
		filter: {
			is_active?: boolean,
		},
		transaction: EntityManager,	
	): Promise<{
		data: Array<PromotionInterface & {
			promotionVariant: Array<PromotionVariantInterface & {
				variant: VariantInterface
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.promotion', 'promotion', transaction)
			// .addSelect("CASE WHEN promotion.published_at < now() AND (promotion.expired_at > now() OR promotion.expired_at IS NULL) then 1 else 2 end", "is_active")
			.orderBy('promotion.published_at', 'DESC')
			// .addOrderBy('promotion.created_at', 'DESC')
			.leftJoinAndMapMany('promotion.promotionVariant', PromotionVariantRecord, 'promotionVariant', 'promotionVariant.promotion_id = promotion.id')
			.leftJoinAndMapOne('promotionVariant.variant', VariantRecord, 'variant', 'variant.id = promotionVariant.variant_id')
			// .leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at is null')

		if (filter.is_active === true) {
			Q = Q.andWhere('promotion.published_at < now()')
				.andWhere('(promotion.expired_at > now() OR promotion.expired_at is null)')
		} else if (filter.is_active === false) {
			Q = Q.andWhere('promotion.published_at > now() OR promotion.expired_at < now()')
		}

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any

	}

	@RepositoryModel.bound
	async filterVariant(
		promotion_id: number,
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		transaction: EntityManager,	
	): Promise<{
		data: Array<PromotionVariantInterface & {
			variant: VariantInterface & {
				asset?: VariantAssetInterface,
				product?: ProductInterface,
				brand?: BrandInterface,
				size?: SizeInterface,
				colors?: ColorInterface[],
				inventories?: InventoryInterface[],
			},
		}>,
		count: number,
	}> {
		let Q = this.query('app.promotion_variant', 'promotionVariant', transaction)
			.where('promotionVariant.promotion_id = :promotion_id', { promotion_id })
			.leftJoinAndMapOne('promotionVariant.variant', VariantRecord, 'variant', 'variant.id = promotionVariant.variant_id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at is null')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin('variant.variantColors', 'vc')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoinAndMapMany('variant.inventories', InventoryRecord, 'inventory', 'inventory.variant_id = variant.id AND inventory.status = :status AND inventory.deleted_at IS NULL', { status: INVENTORIES.AVAILABLE })
			.orderBy('promotionVariant.created_at', 'DESC')
		
		if (!!search) {
			Q = Q.andWhere(`product.title ILIKE '%${search}%' OR brand.title ILIKE '%${search}%'`)
		}

		if (!!limit) {
			Q = Q.take(limit)
		}

		if (!!offset) {
			Q = Q.skip(offset)
		}

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any

	}

	@RepositoryModel.bound
	async get(
		promotion_id: number,
		transaction: EntityManager,
	): Promise<PromotionInterface & {
		promotionVariant: Array<PromotionVariantInterface>,
	}> {
		let Q = this.query('app.promotion', 'promotion', transaction)
			.where('promotion.id = :promotion_id', { promotion_id })

		return Q.getOne()
		.then(this.parser) as any

	}

	@RepositoryModel.bound
	async getPromotionVariant(
		promotion_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.promotion_variant', 'pv', transaction)
			.where('pv.promotion_id = :promotion_id', { promotion_id })
			.andWhere('pv.variant_id = :variant_id', { variant_id })
			.getManyAndCount()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailForPriceUpdate(
		promotion_variant_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.promotion_variant', 'pv', transaction)
			.where('pv.id = :promotion_variant_id', { promotion_variant_id })
			.leftJoinAndMapOne('pv.variant', VariantRecord, 'variant', 'variant.id = pv.variant_id')
			.getOne()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	@RepositoryModel.bound
	async deleteVariant(
		promotion_id: number,
		variant_ids: number[],
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.queryDelete('app.promotion_variant', transaction)
		.where('promotion_variant.promotion_id = :promotion_id', { promotion_id })
		.andWhere('promotion_variant.variant_id IN (:...variant_ids)', { variant_ids })
		.execute()
		.then(_ => true)
	}
}

