import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	BrandAssetRecord,
	CampaignAssetRecord,
	CouponAssetRecord,
	MatchboxAssetRecord,
	PurchaseOrderAssetRecord,
	UserAssetRecord,
	VariantAssetRecord,
	ServiceAssetRecord,
	InventoryRecord,
	StyleboardRecord,
} from 'energie/app/records'

import {
	BrandAssetInterface,
	CouponAssetInterface,
	CampaignAssetInterface,
	MatchboxAssetInterface,
	PurchaseOrderAssetInterface,
	UserAssetInterface,
	VariantAssetInterface,
	ServiceAssetInterface,
	VoucherInterface,
} from 'energie/app/interfaces'
import InterfaceAssetModel from 'energie/app/models/interface.asset'

import { Parameter, ObjectOrArray, ObjectOrArrayParameter } from 'types/common'
import { ORDER_REQUESTS, VOUCHERS, ASSETS } from 'energie/utils/constants/enum'
import { ErrorModel } from 'app/models'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import { uniq } from 'lodash'

export type ASSET_TYPES = 'brand' | 'campaign' | 'coupon' | 'matchbox' | 'service' | 'purchasing' | 'user' | 'variant'

export type ReturnType<T extends ASSET_TYPES> = T extends 'brand' ? BrandAssetInterface
	: T extends 'campaign' ? CampaignAssetInterface
	: T extends 'coupon' ? CouponAssetInterface
	: T extends 'matchbox' ? MatchboxAssetInterface
	: T extends 'service' ? ServiceAssetInterface
	: T extends 'purchasing' ? PurchaseOrderAssetInterface
	: T extends 'user' ? UserAssetInterface
	: T extends 'variant' ? VariantAssetInterface
	: InterfaceAssetModel

@EntityRepository()
export default class AssetRepository extends RepositoryModel<{
	BrandAssetRecord: BrandAssetRecord,
	CampaignAssetRecord: CampaignAssetRecord,
	CouponAssetRecord: CouponAssetRecord,
	MatchboxAssetRecord: MatchboxAssetRecord,
	ServiceAssetRecord: ServiceAssetRecord,
	PurchaseOrderAssetRecord: PurchaseOrderAssetRecord,
	UserAssetRecord: UserAssetRecord,
	VariantAssetRecord: VariantAssetRecord,
}> {

	static __displayName = 'AssetRepository'

	protected records = {
		BrandAssetRecord,
		CampaignAssetRecord,
		CouponAssetRecord,
		MatchboxAssetRecord,
		ServiceAssetRecord,
		PurchaseOrderAssetRecord,
		UserAssetRecord,
		VariantAssetRecord,
	}

	// ============================= INSERT =============================
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.brand, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? BrandAssetInterface[] : BrandAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.campaign, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? CampaignAssetInterface[] : CampaignAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.coupon, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? CouponAssetInterface[] : CouponAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.matchbox, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? MatchboxAssetInterface[] : MatchboxAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.purchasing, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? MatchboxAssetInterface[] : PurchaseOrderAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.user, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? UserAssetInterface[] : UserAssetInterface>
	// async insert<T extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: ASSET_TYPES.variant, typeId: number, data: T, transaction: EntityManager): Promise<T extends any[] ? VariantAssetInterface[] : VariantAssetInterface>
	@RepositoryModel.bound
	async insert<T extends ASSET_TYPES, U extends ObjectOrArrayParameter<InterfaceAssetModel>>(type: T, typeId: number, data: U, transaction: EntityManager): Promise<U extends InterfaceAssetModel[] ? Array<ReturnType<T>> : ReturnType<T>> {
		// apply order/index when upload (only for) variant_asset
		// catch: kalo asset yg lama emang gapunya order/index, yg baru diupload juga gapunya
		let order: number
		let index: number

		if (type === 'variant') {
			// get variant_asset
			const variant_assets = await this.get('variant', typeId, true, transaction).catch(e => [])

			if(variant_assets.length === 0) {
				order = 0
				index = 0
			} else {
				// checking if the assets have the index
				const filtered = variant_assets.filter(v => !v.deleted_at)
				const unique = uniq(filtered.map(va => va.order))
	
				if(filtered.length === unique.length) {
					// sudah mempunyai order
					order = filtered.sort((a, b) => b.order - a.order)[0].order + 1
					index = filtered.sort((a, b) => b.index - a.index)[0].index + 1
				} else {
					// tidak mempunyai order
					await filtered.sort((a, b) => a.id - b.id).reduce(async (promise, va, i) => {
						return promise.then(() => {
							return this.renew(this.record(type), va.id, {
								order: i,
								index: i,
							}, { transaction })
						})
					}, Promise.resolve(null))
	
					order = filtered.length
					index = filtered.length
				}
			}
		}

		if (Array.isArray(data)) {
			return this.save(this.record(type), data.map((d, i) => {
				// Not needed cause the order taken from index above
				// if (!!d.order) {
				// 	throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Cannot insert asset with order')
				// }

				return {
					...d,
					[this.column(type)]: typeId,
					...(type === 'variant' ? {order: i, index: i} : {}),
				}
			}), transaction) as any
		} else {
			// if (!!(data as Parameter<InterfaceAssetModel>).order) {
			// 	throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Cannot insert asset with order')
			// }

			return this.save(this.record(type), {
				...(data as Parameter<InterfaceAssetModel>),
				[this.column(type)]: typeId,
				...(type === 'variant' ? {order, index} : {}),
			}, transaction) as any
		}
	}

	// ============================= UPDATE =============================
	// async update(type: ASSET_TYPES.brand, typeId: number, idOrNonce: number | string, data: Partial<Parameter<BrandAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.campaign, typeId: number, idOrNonce: number | string, data: Partial<Parameter<CampaignAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.coupon, typeId: number, idOrNonce: number | string, data: Partial<Parameter<CouponAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.matchbox, typeId: number, idOrNonce: number | string, data: Partial<Parameter<MatchboxAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.purchasing, typeId: number, idOrNonce: number | string, data: Partial<Parameter<PurchaseOrderAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.user, typeId: number, idOrNonce: number | string, data: Partial<Parameter<UserAssetInterface>>, transaction: EntityManager): Promise<boolean>
	// async update(type: ASSET_TYPES.variant, typeId: number, idOrNonce: number | string, data: Partial<Parameter<VariantAssetInterface>>, transaction: EntityManager): Promise<boolean>
	@RepositoryModel.bound
	async update<T extends ASSET_TYPES>(type: T, typeId: number, idOrNonce: number | string, data: Partial<Parameter<ReturnType<T>>>, transaction: EntityManager): Promise<boolean> {
		if (!!data.order) {
			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Cannot insert asset with order')
		}

		return this.renew(this.record(type), `${ typeof idOrNonce === 'number' ? 'id' : 'nonce' } = :id_or_nonce AND ${ this.column(type) } = :type_id`, data, {
			parameters: {
				id_or_nonce: typeof idOrNonce === 'number' ? idOrNonce : `'${ idOrNonce }'`,
				type_id: typeId,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async reorderAssets(
		type: ASSET_TYPES,
		ids: number[],
		transaction: EntityManager,
	) {
		return await ids.reduce(async (promise, id, i) => {
			return promise.then(() => {
				return this.renew(this.record(type), id, {
					order: i,
					index: i,
				}, {transaction})
			})
		}, Promise.resolve(null))
	}

	// ============================= GETTER =============================
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.brand, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? BrandAssetInterface[] : BrandAssetInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.campaign, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? CampaignAssetInterface[] : CampaignAssetInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.coupon, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? CouponAssetInterface[] : CouponAssetInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.matchbox, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? MatchboxAssetInterface[] : MatchboxAssetInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.purchasing, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? HistoryPurchaseOrderInterface[] : HistoryPurchaseOrderInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.user, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? UserAssetInterface[] : UserAssetInterface>
	// async get<T extends ObjectOrArray<number | string>>(type: ASSET_TYPES.variant, typeId: number, idOrNonce: T, transaction: EntityManager): Promise<T extends any[] ? VariantAssetInterface[] : VariantAssetInterface>
	async get<T extends ASSET_TYPES, U extends ObjectOrArray<number>, V extends boolean>(type: T, typeIdOrIds: U, many: V, transaction: EntityManager): Promise<U extends any[] ? Array<ReturnType<T>> : V extends true ? Array<ReturnType<T>> : ReturnType<T>>
	async get<T extends ASSET_TYPES, U extends ObjectOrArray<number | string>>(type: T, typeId: number, idOrNonce: U, transaction: EntityManager): Promise<U extends any[] ? Array<ReturnType<T>> : ReturnType<T>>

	@RepositoryModel.bound
	async get<T extends ASSET_TYPES, U extends ObjectOrArray<number | string>>(
		type: T,
		typeIdOrIds: number | number[],
		idOrNonceOrMany?: U | boolean,
		transaction?: EntityManager,
	): Promise<any> {

		const isIdOrNonceProvided = typeof idOrNonceOrMany === 'boolean' || idOrNonceOrMany === undefined ? false : true

		if(isIdOrNonceProvided) {
			const Q = this.query(this.table(type), 'asset', transaction)
				.where(...this.select(idOrNonceOrMany as U))
				.andWhere(`asset.${this.column(type)} = :type_id`, { type_id: typeIdOrIds as number })
				.andWhere('asset.deleted_at IS NULL')

			if (this.table(type) === 'app.variant_asset') {
				Q.orderBy('asset.index', 'ASC')
				.addOrderBy('asset.order', 'ASC')
			} else {
				Q.orderBy('asset.order', 'ASC')
			}

			if (Array.isArray(idOrNonceOrMany as U)) {
				return Q.addOrderBy('asset.id', 'ASC')
					.getMany()
					.then(this.parser)
			} else {
				return Q.addOrderBy('asset.id', 'ASC')
					.getOne()
					.then(this.parser)
			}
		} else {
			const isTypeIdArray = Array.isArray(typeIdOrIds)

			const Q = this.query(this.table(type), 'asset', transaction)
				.where(`asset.${this.column(type)} ${isTypeIdArray ? 'IN (:...type_id)' : '= :type_id'}`, { type_id: typeIdOrIds })
				.andWhere('asset.deleted_at IS NULL')


				if (this.column(type) === 'variant_id') {
					Q.orderBy('asset.index', 'ASC')
					.addOrderBy('asset.order', 'ASC')
				} else {
					Q.orderBy('asset.order', 'ASC')
				}

			return Q.addOrderBy('asset.id', 'ASC')
				.getMany()
				.then(this.parser)
				.then((datas: Array<ReturnType<T>>) => {
					if(idOrNonceOrMany === true || idOrNonceOrMany === undefined) {
						// get many per data
						if(isTypeIdArray) {
							return (typeIdOrIds as number[]).map(id => {
								return datas.filter(data => {
									return data[this.column(type)] === id
								})
							})
						} else {
							return datas
						}
					} else {
						// get one per data
						if(isTypeIdArray) {
							return (typeIdOrIds as number[]).map(id => {
								return datas.find(data => {
									return data[this.column(type)] === id
								}) || null
							})
						} else {
							return datas[0] || null
						}
					}
				}) as any
		}
	}

	@RepositoryModel.bound
	async getByOrderRequestType(type: ORDER_REQUESTS, reference_id: number, transaction: EntityManager): Promise<InterfaceAssetModel> {
		if (type === ORDER_REQUESTS.VARIANT) {
			return this.query('app.variant_asset', 'asset', transaction)
				.innerJoin('asset.variant', 'variant', 'variant.id = :reference_id', { reference_id })
				.orderBy('asset.index', 'ASC')
				.addOrderBy('asset.order', 'ASC')
				.getOne()
				.then(this.parser) as any
		} else if (type === ORDER_REQUESTS.MATCHBOX) {
			return this.query('app.matchbox_asset', 'asset', transaction)
				.innerJoin('asset.matchbox', 'matchbox', 'matchbox.id = :reference_id', { reference_id })
				.orderBy('asset.order', 'ASC')
				.getOne()
				.then(this.parser) as any
		} else if (type === ORDER_REQUESTS.SERVICE) {
			return this.query('app.service_asset', 'asset', transaction)
				.innerJoin('asset.service', 'service', 'service.id = :reference_id', { reference_id })
				.orderBy('asset.order', 'ASC')
				.getOne()
				.then(this.parser) as any
		} else if (type === ORDER_REQUESTS.STYLEBOARD) {
			return this.query('app.service_asset', 'asset', transaction)
				.innerJoin('asset.service', 'service')
				.innerJoin(StyleboardRecord, 'styleboard', 'styleboard.service_id = service.id')
				.orderBy('asset.order', 'ASC')
				.getOne()
				.then(this.parser) as any
		} else if (type === ORDER_REQUESTS.TOPUP) {
			return {
				id: null,
				created_at: null,
				type: ASSETS.IMAGE,
				url: 'app/topup.jpg',
				metadata: {
					version: 1,
				},
			}
		} else if (type === ORDER_REQUESTS.VOUCHER) {
			return this.query('app.voucher', 'voucher', transaction)
				.leftJoinAndMapOne('voucher.masset', MatchboxAssetRecord, 'masset', 'masset.matchbox_id = voucher.ref_id AND voucher.type = :matchbox_voucher', { matchbox_voucher: VOUCHERS.MATCHBOX })
				.leftJoin(InventoryRecord, 'inventory', 'inventory.id = voucher.ref_id AND voucher.type = :inventory_voucher', { inventory_voucher: VOUCHERS.INVENTORY })
				.leftJoinAndMapOne('voucher.iasset', VariantAssetRecord, 'iasset', 'iasset.variant_id = inventory.variant_id')
				.leftJoinAndMapOne('voucher.sasset', ServiceAssetRecord, 'sasset', 'sasset.service_id = voucher.ref_id AND voucher.type = :service_voucher', { service_voucher: VOUCHERS.SERVICE })
				.where('voucher.id = :reference_id', { reference_id })
				.getOne()
				.then(this.parser)
				.then((voucher: VoucherInterface & {
					masset?: MatchboxAssetInterface,
					iasset?: VariantAssetInterface,
					sasset?: ServiceAssetInterface,
				}) => {
					if (voucher.type === VOUCHERS.MATCHBOX) {
						return voucher.masset
					} else if (voucher.type === VOUCHERS.INVENTORY) {
						return voucher.iasset
					} else if (voucher.type === VOUCHERS.SERVICE) {
						return voucher.sasset
					} else if (voucher.type === VOUCHERS.TOPUP_AMOUNT) {
						return {
							id: null,
							created_at: null,
							type: ASSETS.IMAGE,
							url: 'app/topup.jpg',
							metadata: {
								version: 1,
							},
						}
					} else {
						return null
					}
				})
		} else {
			return null
		}
	}

	// ============================= DELETE =============================
	async delete(type: ASSET_TYPES, assetId: number, transaction: EntityManager): Promise<boolean>
	async delete(type: ASSET_TYPES, typeId: number, idOrNonce: number | string, transaction: EntityManager): Promise<boolean>

	@RepositoryModel.bound
	async delete(type: ASSET_TYPES, assetIdOrTypeId: number, idOrNonceOrTransaction?: number | string | EntityManager, transaction?: EntityManager): Promise<boolean> {
		if(typeof idOrNonceOrTransaction === 'number' || typeof idOrNonceOrTransaction === 'string') {
			// Soft delete instead of hard delete
			return this.renew(this.record(type), `${ typeof idOrNonceOrTransaction === 'number' ? 'id' : 'nonce' } = :id_or_nonce AND ${ this.column(type) } = :type_id`, {
				deleted_at: new Date(),
			}, {
				parameters: {
					id_or_nonce: idOrNonceOrTransaction,
					type_id: assetIdOrTypeId,
				},
				transaction,
			})
		} else {
			return this.renew(this.record(type), `id = :id`, {
				deleted_at: new Date(),
			}, {
				parameters: {
					id: assetIdOrTypeId,
				},
				transaction: idOrNonceOrTransaction,
			})
		}
	}

	@RepositoryModel.bound
	async batchDelete(
		type: ASSET_TYPES,
		type_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew(this.record(type), `${type}_id = :type_id`, {
			deleted_at: new Date(),
		}, {
			parameters: {
				type_id,
			},
			transaction,
		})
	}


	// ============================ PRIVATES ============================
	private column(type: ASSET_TYPES) {
		switch (type) {
		case 'brand':
			return 'brand_id'
		case 'campaign':
			return 'campaign_id'
		case 'coupon':
			return 'coupon_id'
		case 'matchbox':
			return 'matchbox_id'
		case 'service':
			return 'service_id'
		case 'purchasing':
			return 'purchase_order_id'
		case 'user':
			return 'user_id'
		case 'variant':
			return 'variant_id'
		}
	}

	private table(type: ASSET_TYPES) {
		switch (type) {
		case 'brand':
			return 'app.brand_asset'
		case 'campaign':
			return 'app.campaign_asset'
		case 'coupon':
			return 'app.coupon_asset'
		case 'matchbox':
			return 'app.matchbox_asset'
		case 'service':
			return 'app.service_asset'
		case 'purchasing':
			return 'app.purchase_order_asset'
		case 'user':
			return 'app.user_asset'
		case 'variant':
			return 'app.variant_asset'
		}
	}

	private record(type: ASSET_TYPES) {
		switch (type) {
		case 'brand':
			return 'BrandAssetRecord'
		case 'campaign':
			return 'CampaignAssetRecord'
		case 'coupon':
			return 'CouponAssetRecord'
		case 'matchbox':
			return 'MatchboxAssetRecord'
		case 'service':
			return 'ServiceAssetRecord'
		case 'purchasing':
			return 'PurchaseOrderAssetRecord'
		case 'user':
			return 'UserAssetRecord'
		case 'variant':
			return 'VariantAssetRecord'
		}
	}

	private select(idOrNonce: ObjectOrArray<number | string>): [string, object] {
		if(Array.isArray(idOrNonce)) {
			return [`asset.${typeof idOrNonce[0] === 'number' ? 'id' : 'nonce'} IN (:...idOrNonce)`, { idOrNonce }]
		} else {
			return [`asset.${typeof idOrNonce === 'number' ? 'id' : 'nonce'} = :idOrNonce`, { idOrNonce }]
		}
	}

}
