import {
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	PurchaseOrderRecord,
	PurchaseRequestRecord,
	HistoryPurchaseOrderRecord,
	HistoryPurchaseRequestRecord,
	StylesheetInventoryRecord,
	OrderDetailRecord,
	MatchboxRecord,
	StylesheetRecord,
	// PurchaseOrderRecord
} from 'energie/app/records'

import {
	BrandInterface,
	ColorInterface,
	PurchaseOrderInterface,
	PurchaseRequestInterface,
	SizeInterface,
	CategoryInterface,
	UserInterface,
	UserProfileInterface,
	StylesheetInventoryInterface,
	StylesheetInterface,
	OrderDetailInterface,
	MatchboxInterface,
	// PurchaseOrderAssetInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import { STYLESHEET_STATUSES, ORDER_DETAILS, PURCHASES, PURCHASE_REQUESTS } from 'energie/utils/constants/enum'

// import purchase from 'controllers/admin/purchase';


@EntityRepository()
export default class PurchaseRepository extends RepositoryModel<{
	PurchaseOrderRecord: PurchaseOrderRecord,
	PurchaseRequestRecord: PurchaseRequestRecord,
	HistoryPurchaseOrderRecord: HistoryPurchaseOrderRecord,
	HistoryPurchaseRequestRecord: HistoryPurchaseRequestRecord,
}> {

	static __displayName = 'PurchaseRepository'

	protected records = {
		PurchaseOrderRecord,
		PurchaseRequestRecord,
		HistoryPurchaseOrderRecord,
		HistoryPurchaseRequestRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insertOrder(
		changer_user_id: number,
		update: Parameter<PurchaseOrderInterface>,
		transaction: EntityManager,
	): Promise<PurchaseOrderInterface> {
		return this.save('PurchaseOrderRecord', update, transaction).then(async data => {
			await this.save('HistoryPurchaseOrderRecord', {
				changer_user_id,
				purchase_order_id: data.id,
				status: update.status,
				note: update.note,
			})

			return data
		})
	}

	@RepositoryModel.bound
	async insertRequest(
		changer_user_id: number,
		data: Parameter<PurchaseRequestInterface>,
		transaction: EntityManager,
	): Promise<PurchaseRequestInterface> {
		return this.save('PurchaseRequestRecord', {
			user_id: changer_user_id,
			...data,
		}, transaction).then(async pr => {
			await this.save('HistoryPurchaseRequestRecord', {
				changer_user_id,
				purchase_request_id: pr.id,
				purchase_order_id: data.purchase_order_id,
				status: data.status,
				retail_price: data.retail_price,
				price: data.price,
				quantity: data.quantity,
				note: data.note,
			})

			return pr
		})
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateOrder(
		changer_user_id: number,
		purchase_order_id: number,
		update: Partial<Parameter<PurchaseOrderInterface>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.renew('PurchaseOrderRecord', purchase_order_id, update, {
			transaction,
		}).then(isUpdated => {
			if (isUpdated) {
				this.save('HistoryPurchaseOrderRecord', {
					changer_user_id,
					purchase_order_id,
					status: update.status,
					note: update.note,
				}) // no need to use transaction on history
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updateRequest(
		changer_user_id: number,
		purchase_request_id: number,
		update: Partial<Parameter<PurchaseRequestInterface>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.renew('PurchaseRequestRecord', purchase_request_id, update, {
			transaction,
		}).then(async isUpdated => {
			if (isUpdated) {
				this.save('HistoryPurchaseRequestRecord', {
					changer_user_id,
					purchase_request_id,
					purchase_order_id: update.purchase_order_id,
					status: update.status,
					retail_price: update.retail_price,
					price: update.price,
					quantity: update.quantity,
					note: update.note,
				}) // no need to use transaction on history
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filterRequest(
		offset: number,
		limit: number,
		filter: {
			status?: PURCHASE_REQUESTS,
			search?: string,
			published?: boolean,
			have_order?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<PurchaseRequestInterface & {
			brand: BrandInterface,
			seller: BrandInterface,
			category: CategoryInterface,
			color: ColorInterface,
			size: SizeInterface,
			stylesheet?: StylesheetInterface,
			matchbox?: MatchboxInterface,
			user: UserInterface & {
				profile: UserProfileInterface,
			},
		}>,
		count: number,
	}> {
		let Q = this.query('app.purchase_request', 'pr', transaction)
			.leftJoinAndSelect('pr.brand', 'brand')
			.leftJoinAndSelect('pr.seller', 'seller')
			.leftJoinAndSelect('pr.category', 'category')
			.leftJoinAndSelect('pr.color', 'color')
			.leftJoinAndSelect('pr.size', 'size')
			.leftJoinAndSelect('pr.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoin('pr.stylesheetInventory', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndMapOne('pr.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = sI.stylesheet_id')
			.leftJoinAndMapOne('pr.matchbox', MatchboxRecord, 'matchbox', 'matchbox.id = stylesheet.matchbox_id')
			.where('pr.deleted_at IS NULL')
			.orderBy('pr.created_at', 'ASC')
			.skip(offset)
			.take(limit)

		if(filter.status) {
			Q = Q.andWhere('pr.status = :status', { status: filter.status })
		}

		if(filter.search) {
			Q = Q.andWhere(`(pr.title ILIKE :search OR brand.title ILIKE :search OR seller.title ILIKE :search OR color.title ILIKE :search or category.title ILIKE :search)`, { search : `%${ filter.search }%` })
		}

		if(filter.published) {
			Q = Q.andWhere('stylesheet.status = :stylesheet_published', { stylesheet_published: STYLESHEET_STATUSES.PUBLISHED })
		}

		if(filter.have_order === true) {
			Q = Q.andWhere('pr.purchase_order_id IS NOT NULL')
		} else if(filter.have_order === false) {
			Q = Q.andWhere('pr.purchase_order_id IS NULL')
		}

		return Q.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async filterOrder(
		offset: number = 0,
		limit: number = 40,
		filter: {
			status?: PURCHASES,
			search?: string,
			user_id?: number,
		} = {},
		option: {
			include_user?: boolean,
			include_request?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<PurchaseOrderInterface & {
			user?: UserInterface & {
				profile: UserProfileInterface,
			},
			purchaseRequests: Array<PurchaseRequestInterface & {
				brand?: BrandInterface,
				seller?: BrandInterface,
				category?: CategoryInterface,
				color?: ColorInterface,
				size?: SizeInterface,
				stylesheetInventory?: StylesheetInventoryInterface,
			}>,
		}>,
		count: number,
	}> {
		let Q = this.query('app.purchase_order', 'po', transaction)
			.addSelect('po.updated_at')
			.leftJoinAndSelect('po.purchaseRequests', 'pr', 'pr.deleted_at IS NULL')
			.skip(offset)
			.take(limit)
			.orderBy('po.id', 'DESC')

		if (filter.status) {
			Q = Q.where('po.status = :status', { status: filter.status })
		} else {
			Q = Q.where('po.status = :pending', { pending: PURCHASES.PENDING })
		}

		if (filter.search) {
			Q = Q.andWhere(`(po.title ILIKE :search`, { search: `%${filter.search}%` })
		}

		if(filter.user_id) {
			Q = Q.andWhere('po.user_id = :user_id', { user_id: filter.user_id })
		}

		if(option.include_user) {
			Q = Q.leftJoinAndSelect('po.user', 'user')
				.leftJoinAndSelect('user.profile', 'profile')
		}

		if(option.include_request) {
			Q = Q.leftJoinAndSelect('pr.brand', 'brand')
				.leftJoinAndSelect('pr.seller', 'seller')
				.leftJoinAndSelect('pr.category', 'category')
				.leftJoinAndSelect('pr.color', 'color')
				.leftJoinAndSelect('pr.size', 'size')
				.leftJoinAndMapOne('pr.stylesheetInventory', StylesheetInventoryRecord, 'sI', 'sI.purchase_request_id = pr.id AND sI.deleted_at IS NULL')
		}

		return Q.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			})
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylesheetFromOrder(
		ids: number[],
		transaction: EntityManager,
	): Promise<StylesheetInterface[]> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.innerJoin('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.innerJoin('sI.purchaseRequest', 'pr')
			.innerJoin('pr.purchaseOrder', 'po', 'po.id IN (:...ids)', { ids })
			.where('stylesheet.status = :stylesheet_published', { stylesheet_published: STYLESHEET_STATUSES.PUBLISHED })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRequestWithStylesheet(
		ids: number[],
		transaction: EntityManager,
	): Promise<Array<PurchaseRequestInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		stylesheetInventory: StylesheetInventoryInterface & {
			stylesheet: StylesheetInterface & {
				orderDetail: OrderDetailInterface,
			},
		},
		brand: BrandInterface,
		seller: BrandInterface,
		category: CategoryInterface,
		color: ColorInterface,
		size: SizeInterface,
	}>> {
		return this.query('app.purchase_request', 'pr', transaction)
			.leftJoinAndSelect('pr.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndMapOne('pr.stylesheetInventory', StylesheetInventoryRecord, 'stylesheetInventory', 'stylesheetInventory.purchase_request_id = pr.id')
			.leftJoinAndSelect('stylesheetInventory.stylesheet', 'stylesheet')
			.leftJoinAndMapOne('stylesheet.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.ref_id = stylesheet.id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('pr.brand', 'brand')
			.leftJoinAndSelect('pr.seller', 'seller')
			.leftJoinAndSelect('pr.category', 'category')
			.leftJoinAndSelect('pr.color', 'color')
			.leftJoinAndSelect('pr.size', 'size')
			.where('pr.id IN (:...ids)', { ids })
			.getMany()
			.then(this.parser) as any
	}

	async getRequest(id: number, deep: false | undefined, transaction: EntityManager): Promise<PurchaseRequestInterface>
	async getRequest(id: number, deep: true | undefined, transaction: EntityManager): Promise<PurchaseRequestInterface & {
		brand: BrandInterface,
		seller: BrandInterface,
		category: CategoryInterface,
		color: ColorInterface,
		size: SizeInterface,
	}>
	async getRequest(ids: number[], deep: false | undefined, transaction: EntityManager): Promise<PurchaseRequestInterface[]>
	async getRequest(ids: number[], deep: true | undefined, transaction: EntityManager): Promise<Array<PurchaseRequestInterface & {
		brand: BrandInterface,
		seller: BrandInterface,
		category: CategoryInterface,
		color: ColorInterface,
		size: SizeInterface,
	}>>

	@RepositoryModel.bound
	async getRequest(
		idOrIds: number | number[],
		deep?: boolean,
		transaction?: EntityManager,
	): Promise<any> {
		if(deep) {
			const Q = this.query('app.purchase_request', 'pr', transaction)
				.leftJoinAndSelect('pr.brand', 'brand')
				.leftJoinAndSelect('pr.seller', 'seller')
				.leftJoinAndSelect('pr.category', 'category')
				.leftJoinAndSelect('pr.color', 'color')
				.leftJoinAndSelect('pr.size', 'size')

			if(Array.isArray(idOrIds)) {
				return Q.where('pr.id IN (:...ids)', { ids: idOrIds })
					.getMany()
					.then(this.parser)
			} else {
				return Q.where('pr.id = :id', { id: idOrIds })
					.getOne()
					.then(this.parser)
			}
		} else {
			if (Array.isArray(idOrIds)) {
				return this.getMany('PurchaseRequestRecord', {
					id: In(idOrIds),
				}, {
					transaction,
				})
			} else {
				return this.getOne('PurchaseRequestRecord', {
					id: idOrIds,
				}, {
					transaction,
				})
			}
		}
	}

	@RepositoryModel.bound
	async getStylesheetInventoryOf(
		purchase_request_id: number,
		transaction: EntityManager,
	): Promise<StylesheetInventoryInterface> {
		return this.query('app.stylesheet_inventory', 'sI', transaction)
			.innerJoinAndSelect('sI.purchaseRequest', 'pR', 'pR.id = :purchase_request_id', { purchase_request_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserOrder(
		user_id: number,
		status: PURCHASES,
		transaction: EntityManager,
	): Promise<Array<PurchaseOrderInterface & {
		purchaseRequests: PurchaseRequestInterface[],
	}>> {
		return this.query('app.purchase_order', 'po', transaction)
			.leftJoinAndSelect('po.purchaseRequests', 'pr', 'pr.deleted_at IS NULL')
			.where('po.user_id = :user_id AND po.status = :status', { user_id, status })
			.getMany()
			.then(this.parser) as any
	}

	async getOrder(ids: number[], deep: 'FALSE', transaction: EntityManager): Promise<PurchaseOrderInterface[]>
	async getOrder(ids: number[], deep: 'SHALLOW', transaction: EntityManager): Promise<Array<PurchaseOrderInterface & {
		purchaseRequests: PurchaseRequestInterface[],
	}>>
	async getOrder(ids: number[], deep: 'TRUE', transaction: EntityManager): Promise<Array<PurchaseOrderInterface & {
		purchaseRequests: Array<PurchaseRequestInterface & {
			brand: BrandInterface,
			seller: BrandInterface,
			color: ColorInterface,
			size: SizeInterface,
			category: CategoryInterface,
		}>,
	}>>

	@RepositoryModel.bound
	async getOrder(ids: number[], deep: 'FALSE' | 'SHALLOW' | 'TRUE', transaction: EntityManager): Promise<any> {
		if(deep === 'TRUE' || deep === 'SHALLOW') {

			const Q = this.query('app.purchase_order', 'po', transaction)
				.addSelect('po.updated_at')
				.leftJoinAndSelect('po.purchaseRequests', 'pr', 'pr.deleted_at IS NULL')
				.where('po.id IN (:...ids)', { ids })

			if(deep === 'TRUE') {
				return Q
					.leftJoinAndSelect('pr.brand', 'brand')
					.leftJoinAndSelect('pr.seller', 'seller')
					.leftJoinAndSelect('pr.category', 'category')
					.leftJoinAndSelect('pr.color', 'color')
					.leftJoinAndSelect('pr.size', 'size')
					.getMany()
					.then(this.parser)
			} else {
				return Q
					.getMany()
					.then(this.parser)
			}
		} else {

			return this.query('app.purchase_order', 'po', transaction)
				.where('po.id IN (:...ids)', { ids })
				.getMany()
				.then(this.parser)

		}
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async deleteRequest(
		purchase_request_id: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate<PurchaseRequestInterface>('app.purchase_request', transaction)
			.set({
				deleted_at: new Date(),
			})
			.where('id = :purchase_request_id', { purchase_request_id })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================ PRIVATES ============================

}
