import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	InventoryRecord,
	HistoryVoucherRecord,
	MatchboxAssetRecord,
	MatchboxRecord,
	VariantAssetRecord,
	VoucherRecord,
	ServiceRecord,
	ColorRecord,
	TopupAmountRecord,
} from 'energie/app/records'

import {
	VariantAssetInterface,
	MatchboxAssetInterface,
	VoucherInterface,
	MatchboxInterface,
	InventoryInterface,
	BrandInterface,
	VariantInterface,
	ProductInterface,
	ColorInterface,
	SizeInterface,
	VariantColorInterface,
	HistoryVoucherInterface,
	ServiceInterface,
	ServiceAssetInterface,
	CategoryInterface,
	TopupAmountInterface,
	TopupInterface,
	// PacketInterface,
} from 'energie/app/interfaces'

import CommonHelper from 'utils/helpers/common'
// import Defaults from 'utils/constants/default'
import { Parameter } from 'types/common'

import { VOUCHER_STATUSES, VOUCHERS } from 'energie/utils/constants/enum'


@EntityRepository()
export default class VoucherRepository extends RepositoryModel<{
	HistoryVoucherRecord: HistoryVoucherRecord,
	VoucherRecord: VoucherRecord,
}> {

	static __displayName = 'VoucherRepository'

	protected records = {
		HistoryVoucherRecord,
		VoucherRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		code: string,
		type: VOUCHERS,
		ref_id: number,
		status: VOUCHER_STATUSES,
		packet_id: number,
		note: string | undefined,
		name: string | undefined,
		email: string | undefined,
		free_shipping: boolean | undefined,
		is_physical: boolean | undefined,
		expired_at: Date | null,
		transaction: EntityManager,
	): Promise<VoucherRecord> {
		return await this.save('VoucherRecord', {
			code,
			type,
			ref_id,
			status,
			note,
			name,
			packet_id,
			email,
			free_shipping,
			is_physical,
			expired_at,
		}, transaction) as any
	}

	// @RepositoryModel.bound
	// async insertRedeem(
	// 	code: string,
	// 	user_id: number,
	// 	status: REDEEM_STATUSES,
	// 	metadata?: OrderDetailMatchboxMetadataInterface | OrderDetailInventoryMetadataInterface,
	// 	type?: ORDER_REQUESTS,
	// 	ref_id?: number,
	// 	note?: string,
	// 	processed_at?: Date,
	// 	transaction?: EntityManager,
	// ) {
	// 	return this.save('RedeemRecord', {
	// 		voucher_code: code,
	// 		user_id,
	// 		status,
	// 		type,
	// 		ref_id,
	// 		note,
	// 		metadata,
	// 		processed_at,
	// 	}, transaction)
	// }

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		voucher_id: number,
		update: Partial<Parameter<VoucherInterface, 'packet_id'>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('VoucherRecord', voucher_id, CommonHelper.stripKey(update as VoucherInterface, 'packet_id'), {
			transaction,
		}).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryVoucherRecord', {
					changer_user_id,
					voucher_id,
					status: update.status,
					note: note || update.note,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updatePacketId(voucher_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<VoucherInterface>('app.voucher', transaction)
			.set({ packet_id })
			.where(Array.isArray(voucher_id_or_ids) ? 'id IN (:...voucher_id_or_ids)' : 'id = :voucher_id_or_ids', { voucher_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// @RepositoryModel.bound
	// async updateRedeem(
	// 	redeem_id: number,
	// 	update: Partial<Parameter<RedeemInterface>>,
	// 	note?: string,
	// 	transaction?: EntityManager,
	// ) {
	// 	return this.renew('RedeemRecord', redeem_id, update, {
	// 		transaction,
	// 	}).then(isUpdated => {
	// 		if(isUpdated) {
	// 			this.save('HistoryRedeemRecord', {
	// 				redeem_id,
	// 				status: update.status,
	// 				note,
	// 			})
	// 		}

	// 		return isUpdated
	// 	})
	// }

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number,
		limit: number,
		status: VOUCHER_STATUSES | null,
		date: Date | null,
		transaction: EntityManager,
	): Promise<{
		data: VoucherInterface[],
		count: number,
	}> {
		let Q = this.query('app.voucher', 'voucher', transaction)

		if (status !== null) {
			Q = Q.where('voucher.status = :status', { status })
		}

		if (date !== null) {
			Q = Q.where('voucher.created_at >= :date', { date: date.toISOString() })
		}

		return Q.offset(offset)
			.limit(limit)
			.getManyAndCount()
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			}).then(this.parser) as any
	}

	// @RepositoryModel.bound
	// async filterRedeem(
	// 	offset: number,
	// 	limit: number,
	// 	status: REDEEM_STATUSES | null,
	// 	date: Date | null,
	// 	transaction?: EntityManager,
	// ): Promise<{
	// 	data: Array<RedeemInterface & {
	// 		user: UserInterface & {
	// 			profile: UserProfileInterface,
	// 		},
	// 		voucher?: VoucherInterface,
	// 		orderRequest?: OrderRequestInterface,
	// 		orderDetail?: OrderDetailInterface,
	// 		order?: OrderInterface,
	// 	}>,
	// 	count: number,
	// }> {
	// 	let Q = this.query('app.redeem', 'redeem', transaction)

	// 	if (status !== null) {
	// 		Q = Q.where('redeem.status = :status', { status })
	// 	}

	// 	if (date !== null) {
	// 		Q = Q.where('redeem.created_at >= :date', { date: date.toISOString() })
	// 	}

	// 	return Q
	// 		.innerJoinAndSelect('redeem.user', 'user')
	// 		.innerJoinAndSelect('user.profile', 'profile')
	// 		.leftJoinAndMapOne('redeem.voucher', VoucherRecord, 'voucher', 'redeem.voucher_code = voucher.code')
	// 		.leftJoinAndMapOne('redeem.orderRequest', OrderRequestRecord, 'order_request', 'order_request.type = :type AND order_request.ref_id = voucher.id', { type: ORDER_REQUESTS.VOUCHER })
	// 		.leftJoinAndMapOne('redeem.orderDetail', OrderDetailRecord, 'order_detail', 'order_detail.order_request_id = order_request.id')
	// 		.leftJoinAndMapOne('redeem.order', OrderRecord, 'order', 'order.id = order_request.order_id')
	// 		.offset(offset)
	// 		.limit(limit)
	// 		.getManyAndCount()
	// 		.then(datas => {
	// 			return {
	// 				data: datas[0],
	// 				count: datas[1],
	// 			}
	// 		}).then(this.parser) as any
	// }

	@RepositoryModel.bound
	async getAsset(
		voucher_id: number,
		transaction: EntityManager,
	): Promise<VariantAssetInterface | MatchboxAssetInterface> {
		return this.query('app.voucher', 'voucher', transaction)
			.leftJoin(InventoryRecord, 'inventory', 'voucher.type = :inventory_type AND voucher.ref_id = inventory.id', { inventory_type: VOUCHERS.INVENTORY })
			.leftJoinAndMapOne('voucher.variantAsset', VariantAssetRecord, 'variant_asset', 'variant_asset.variant_id = inventory.variant_id')
			.leftJoinAndMapOne('voucher.matchboxAsset', MatchboxAssetRecord, 'matchbox_asset', 'voucher.type = :matchbox_type AND voucher.ref_id = matchbox_asset.matchbox_id', { matchbox_type: VOUCHERS.MATCHBOX })
			.where('voucher.id = :voucher_id', { voucher_id })
			.orderBy('variant_asset.order', 'ASC')
			.addOrderBy('variant_asset.updated_at', 'DESC')
			.addOrderBy('matchbox_asset.order', 'ASC')
			.addOrderBy('matchbox_asset.updated_at', 'DESC')
			.getOne()
			.then(this.parser)
			.then((voucher: any) => {
				return voucher.variantAsset || voucher.matchboxAsset
			})
	}

	@RepositoryModel.bound
	async getDeep(
		voucher_id: number,
		transaction: EntityManager,
	): Promise<VoucherInterface & {
		matchbox?: MatchboxInterface & {
			brand: BrandInterface,
			assets: MatchboxAssetInterface[],
		},
		inventory?: InventoryInterface & {
			variant: VariantInterface & {
				product: ProductInterface & {
					brand: BrandInterface,
				},
				variantColors: Array<VariantColorInterface & {
					color: ColorInterface,
				}>,
				size: SizeInterface,
				assets: VariantAssetInterface[],
			},
		},
	}> {
		return await this.query('app.voucher', 'voucher', transaction)
			.leftJoinAndMapOne('voucher.inventory', InventoryRecord, 'inventory', 'voucher.type = :inventory_type AND voucher.ref_id = inventory.id', { inventory_type: VOUCHERS.INVENTORY })
			.leftJoinAndMapOne('voucher.matchbox', MatchboxRecord, 'matchbox', 'voucher.type = :matchbox_type AND voucher.ref_id = matchbox.id', { matchbox_type: VOUCHERS.MATCHBOX })
			.leftJoinAndSelect('matchbox.brand', 'matchboxBrand')
			.leftJoinAndSelect('matchbox.assets', 'matchboxAssets')
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndSelect('variant.variantColors', 'variantColors')
			.leftJoinAndSelect('variantColors.color', 'color')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndSelect('variant.assets', 'assets')
			.where('voucher.id = :voucher_id', { voucher_id })
			.orderBy('assets.order', 'ASC')
			.addOrderBy('variantColors.order', 'ASC')
			.addOrderBy('assets.updated_at', 'DESC')
			.addOrderBy('matchboxAssets.order', 'ASC')
			.addOrderBy('matchboxAssets.updated_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(voucher_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('VoucherRecord', {
			id: voucher_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	@RepositoryModel.bound
	async getUnredeemedVoucher(
		codeOrId: number | string,
		status: VOUCHER_STATUSES[],
		transaction: EntityManager,
	): Promise<VoucherInterface> {
		return this.query('app.voucher', 'voucher', transaction)
			.where(typeof codeOrId === 'number' ? 'voucher.id = :codeOrId' : 'voucher.code = :codeOrId', { codeOrId })
			.andWhere('voucher.status IN (:...status)', { status })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getVoucherWithReference(
		codeOrId: number | string,
		availableOnly: boolean,
		transaction: EntityManager,
	): Promise<VoucherInterface & {
		matchbox?: MatchboxInterface & {
			brand: BrandInterface,
			assets: MatchboxAssetInterface[],
		},
		service?: ServiceInterface & {
			brand: BrandInterface,
			assets: ServiceAssetInterface[],
		},
		inventory?: InventoryInterface & {
			variant: VariantInterface & {
				product: ProductInterface & {
					brand: BrandInterface,
					category: CategoryInterface,
				},
				assets: VariantAssetInterface[],
				colors: ColorInterface[],
				size: SizeInterface,
			},
			brand: BrandInterface,
		},
		topupAmount?: TopupAmountInterface & {
			topup: TopupInterface,
		},
	}> {
		let Q = this.query('app.voucher', 'voucher', transaction)
			.leftJoinAndMapOne('voucher.matchbox', MatchboxRecord, 'matchbox', 'matchbox.id = voucher.ref_id AND voucher.type = :matchbox_type', { matchbox_type: VOUCHERS.MATCHBOX })
			.leftJoinAndSelect('matchbox.brand', 'mBrand')
			.leftJoinAndSelect('matchbox.assets', 'mAssets')
			.leftJoinAndMapOne('voucher.service', ServiceRecord, 'service', 'service.id = voucher.ref_id AND voucher.type = :service_type', { service_type: VOUCHERS.SERVICE })
			.leftJoinAndSelect('service.brand', 'sBrand')
			.leftJoinAndSelect('service.assets', 'sAssets')
			.leftJoinAndMapOne('voucher.inventory', InventoryRecord, 'inventory', 'inventory.id = voucher.ref_id AND voucher.type = :inventory_type', { inventory_type: VOUCHERS.INVENTORY })
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('inventory.brand', 'seller')
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('product.brand', 'iBrand')
			.leftJoinAndSelect('product.category', 'category')
			.leftJoinAndSelect('variant.assets', 'vAssets')
			.leftJoin('variant.variantColors', 'vC')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vC.color_id')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndMapOne('voucher.topupAmount', TopupAmountRecord, 'topupAmount', 'topupAmount.id = voucher.ref_id AND voucher.type = :topup_type', { topup_type: VOUCHERS.TOPUP_AMOUNT })
			.leftJoinAndSelect('topupAmount.topup', 'topup')
			.where(typeof codeOrId === 'number' ? 'voucher.id = :codeOrId' : 'voucher.code = :codeOrId', { codeOrId })

		if(availableOnly) {
			Q = Q.andWhere('voucher.status = :status', { status: VOUCHER_STATUSES.AVAILABLE })
		}

		return Q
			.getOne()
			.then(this.parser) as any
	}

	// @RepositoryModel.bound
	// async getRedeemDetail(
	// 	redeemIdOrCode: number | string,
	// 	transaction?: EntityManager,
	// ): Promise<RedeemInterface & {
	// 	user: UserInterface & {
	// 		profile: UserProfileInterface,
	// 	},
	// 	voucher?: VoucherInterface,
	// 	orderRequest?: OrderRequestInterface,
	// 	orderDetail?: OrderDetailInterface,
	// 	order?: OrderInterface,
	// }> {
	// 	return this.query('app.redeem', 'redeem', transaction)
	// 		.leftJoinAndSelect('redeem.user', 'user')
	// 		.leftJoinAndSelect('user.profile', 'profile')
	// 		.leftJoinAndMapOne('redeem.voucher', VoucherRecord, 'voucher', 'redeem.voucher_code = voucher.code')
	// 		.leftJoinAndMapOne('redeem.orderRequest', OrderRequestRecord, 'order_request', 'order_request.type = :type AND order_request.ref_id = voucher.id', { type: ORDER_REQUESTS.VOUCHER })
	// 		.leftJoinAndMapOne('redeem.orderDetail', OrderDetailRecord, 'order_detail', 'order_detail.order_request_id = order_request.id')
	// 		.leftJoinAndMapOne('redeem.order', OrderRecord, 'order', 'order.id = order_request.order_id')
	// 		.where(typeof redeemIdOrCode === 'number' ? 'redeem.id = :redeemIdOrCode' : 'redeem.voucher_code = :redeemIdOrCode', { redeemIdOrCode })
	// 		.getOne()
	// 		.then(this.parser) as any
	// }

	@RepositoryModel.bound
	async getVoucher(voucher_id_or_code: number | string, transaction: EntityManager): Promise<VoucherInterface> {
		return this.getOne('VoucherRecord', typeof voucher_id_or_code === 'number' ? {
			id: voucher_id_or_code,
		 } : {
			code: voucher_id_or_code,
		 }, {
			 transaction,
		 })
	}

	@RepositoryModel.bound
	async getVoucherLastHistory(voucher_id: number, transaction: EntityManager): Promise<HistoryVoucherInterface> {
		return this.getOne('HistoryVoucherRecord', {
			id: voucher_id,
		}, {
			order: {
				id: 'DESC',
			},
			transaction,
		})
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
