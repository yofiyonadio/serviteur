import RepositoryModel, { Keys } from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	BrandRecord,
	BrandAddressRecord,
} from 'energie/app/records'

import {
	BrandInterface,
	BrandAddressInterface,
	LocationInterface,
} from 'energie/app/interfaces'

import { Parameter, ObjectOrArray } from 'types/common'


@EntityRepository()
export default class BrandRepository extends RepositoryModel<{
	BrandRecord: BrandRecord,
	BrandAddressRecord: BrandAddressRecord,
}> {

	static __displayName = 'BrandRepository'

	protected records = {
		BrandRecord,
		BrandAddressRecord,
	}

	// ============================= INSERT =============================
	async insert(data: Parameter<BrandInterface>, transaction: EntityManager): Promise<BrandInterface>
	async insert(data: Array<Parameter<BrandInterface>>, transaction: EntityManager): Promise<BrandInterface[]>
	async insert(data: Parameter<BrandInterface>, addresses: BrandAddressInterface[], transaction: EntityManager): Promise<BrandInterface & { addresses: BrandAddressInterface[] }>

	@RepositoryModel.bound
	async insert(
		data: ObjectOrArray<Parameter<BrandInterface>>,
		addressesOrTransaction?: BrandAddressInterface[] | EntityManager,
		transaction?: EntityManager,
	): Promise<any> {
		const isIncludeAddress = Array.isArray(addressesOrTransaction) ? true : false
		const entityManager: EntityManager = isIncludeAddress ? transaction : addressesOrTransaction as EntityManager

		const brand = await this.save('BrandRecord', data as any, entityManager)

		if(isIncludeAddress) {
			return {
				...brand,
				addresses: await this.insertAddress(brand.id, addressesOrTransaction as BrandAddressInterface[], entityManager),
			}
		}

		return brand
	}

	async insertAddress(brand_id: number, data: Parameter<BrandAddressInterface, 'brand_id'>, transaction: EntityManager): Promise<BrandAddressInterface>
	async insertAddress(brand_id: number, datas: Array<Parameter<BrandAddressInterface, 'brand_id'>>, transaction: EntityManager): Promise<BrandAddressInterface[]>

	@RepositoryModel.bound
	async insertAddress(
		brand_id: number,
		data: ObjectOrArray<Parameter<BrandAddressInterface, 'brand_id'>>,
		transaction?: EntityManager,
	): Promise<any> {
		if(Array.isArray(data)) {
			return await this.save('BrandAddressRecord', data.map((d): Keys<BrandAddressRecord> => {
				return {
					brand_id,
					...d,
				}
			}), transaction)
		} else {
			return await this.save('BrandAddressRecord', {
				brand_id,
				...data,
			}, transaction)
		}
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(brand_id: number, brand: Partial<BrandInterface>, transaction: EntityManager) {
		return await this.renew('BrandRecord', brand_id, brand, { transaction })
	}

	@RepositoryModel.bound
	async updateAddress(brand_address_id: number, address: Partial<BrandAddressInterface>, transaction: EntityManager) {
		return await this.renew('BrandAddressRecord', brand_address_id, address, { transaction })
	}

	// ============================= GETTER =============================
	async get(brand_id: number,  {consignment_stock}, transaction: EntityManager): Promise<BrandInterface>
	async get(brand_id: undefined,  {consignment_stock}, transaction: EntityManager): Promise<BrandInterface[]>

	@RepositoryModel.bound
	async get(
		brand_id: number | undefined,
		filter: {
			consignment_stock?: boolean | undefined,
		},
		transaction: EntityManager,
		): Promise<BrandInterface | BrandInterface[]> {

		if (brand_id) {
			return this.getOne('BrandRecord', { id: brand_id }, { transaction })
		}

		let Q = this.query('app.brand', 'brand', transaction)

		if (filter.consignment_stock) {
			Q = Q.leftJoinAndSelect('brand.products', 'products')
				 .leftJoinAndSelect('products.variants', 'variants')
				 .where('variants.have_consignment_stock = true')
		}

		return Q.getMany()
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddresses(brand_address_id_or_ids: number | number[], transaction: EntityManager): Promise<BrandAddressInterface[]> {
		return this.getMany('BrandAddressRecord', {
			id: Array.isArray(brand_address_id_or_ids) ? In(brand_address_id_or_ids) : brand_address_id_or_ids,
		}, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async getByCategoryIds(category_ids: number[], transaction: EntityManager): Promise<BrandInterface[]> {
		return this.query('app.brand', 'brand', transaction)
			.innerJoin('brand.products', 'products', 'products.category_id IN (:...category_ids)', { category_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByTitle(title: string, transaction: EntityManager): Promise<BrandInterface> {
		return this.query('app.brand', 'brand', transaction)
			.where('brand.title ILIKE :title', { title: `%${title}%` })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getMainAddress(id: number, transaction: EntityManager): Promise<BrandAddressInterface> {
		return this.query('app.brand_address', 'address', transaction)
			.innerJoin('address.brand', 'brand', 'brand.brand_address_id = address.id AND brand.id = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddressDeep(address_id: number, transaction: EntityManager): Promise<BrandAddressInterface & {
		location: LocationInterface,
	}> {
		return this.query('app.brand_address', 'address', transaction)
			.leftJoinAndSelect('address.location', 'location')
			.where('address.id = :address_id', { address_id })
			.getOne()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
