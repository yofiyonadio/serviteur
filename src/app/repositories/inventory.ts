import { ErrorModel, RepositoryModel } from 'app/models'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { EntityRepository, EntityManager, SelectQueryBuilder } from 'typeorm'

import {
	HistoryInventoryRecord,
	InventoryRecord,
	InventoryBookingRecord,
	UserRecord,
	OrderDetailRecord,
	StylesheetRecord,
	VoucherRecord,
	ProductRecord,
	BrandRecord,
	ColorRecord,
	SizeRecord,
	VariantAssetRecord,
	CategoryRecord,
	VariantTagRecord,
	VariantRecord,
	VariantColorRecord,
	TagRecord,
	VariantMeasurementRecord,
	MeasurementRecord,
} from 'energie/app/records'

import {
	InventoryInterface,
	VariantInterface,
	VariantAssetInterface,
	BrandInterface,
	ProductInterface,
	VariantColorInterface,
	ColorInterface,
	BrandAddressInterface,
	SizeInterface,
	InventoryBookingInterface,
	UserInterface,
	UserProfileInterface,
	OrderDetailInterface,
	OrderInterface,
	StylesheetInterface,
	MatchboxInterface,
	VoucherInterface,
	CategoryInterface,
	HistoryInventoryInterface,
	TagInterface,
	MeasurementInterface,
} from 'energie/app/interfaces'

import { InventoryDeepInterface } from 'app/interfaces/deep'

import { Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'

import { INVENTORIES, BOOKING_SOURCES } from 'energie/utils/constants/enum'
import { isEmpty } from 'lodash'

@EntityRepository()
export default class InventoryRepository extends RepositoryModel<{
	HistoryInventoryRecord: HistoryInventoryRecord,
	InventoryRecord: InventoryRecord,
	InventoryBookingRecord: InventoryBookingRecord,
}> {

	static __displayName = 'InventoryRepository'

	protected records = {
		HistoryInventoryRecord,
		InventoryRecord,
		InventoryBookingRecord,
	}

	// ============================= INSERT =============================
	async insert(changer_user_id: number, data: Parameter<InventoryInterface>, transaction: EntityManager): Promise<InventoryInterface>
	async insert(changer_user_id: number, data: Array<Parameter<InventoryInterface>>, transaction: EntityManager): Promise<InventoryInterface[]>

	@RepositoryModel.bound
	async insert(changer_user_id: number, data: Parameter<InventoryInterface> | Array<Parameter<InventoryInterface>>, transaction: EntityManager): Promise<any> {
		if(Array.isArray(data)) {
			return this.save('InventoryRecord', data, transaction)
				.then(inventories => {
					inventories.map(inventory => {
						this.save('HistoryInventoryRecord', {
							changer_user_id,
							inventory_id: inventory.id,
							status: inventory.status,
							note: 'Inventory added',
							price: inventory.price,
						})
					})

					return inventories
				})
		} else {
			return this.save('InventoryRecord', data, transaction)
				.then(inventory => {
					this.save('HistoryInventoryRecord', {
						changer_user_id,
						inventory_id: inventory.id,
						status: inventory.status,
						note: 'Inventory added',
						price: inventory.price,
					})

					return inventory
				})
		}
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async book(
		changer_user_id: number,
		inventory_id: number,
		source: BOOKING_SOURCES,
		reference_id: number,
		note: string | undefined,
		strict: boolean = false,
		transaction: EntityManager,
	) {
		return this.renew('InventoryRecord', strict ? `id = :id AND status = :status` : inventory_id, {
			status: INVENTORIES.BOOKED,
		}, {
			parameters: {
				id: inventory_id,
				status: INVENTORIES.AVAILABLE,
			},
			transaction,
		}).then(isUpdated => {
			if(isUpdated) {
				return this.save('InventoryBookingRecord', {
					inventory_id,
					ref_id: reference_id,
					source,
					note,
				}, transaction).then(() => true).catch(err => {
					this.warn(err)

					return false
				})
			}

			return isUpdated
		}).then(async isUpdated => {
			if(isUpdated) {
				this.save('HistoryInventoryRecord', {
					changer_user_id,
					inventory_id,
					note,
					status: INVENTORIES.BOOKED,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		inventory_id: number,
		update: Partial<Parameter<InventoryInterface, 'packet_id'>>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('InventoryRecord', inventory_id, CommonHelper.stripKey(update as InventoryInterface, 'packet_id'), {
			transaction,
		}).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryInventoryRecord', {
					changer_user_id,
					inventory_id,
					status: update.status,
					brand_address_id: update.brand_address_id,
					note: update.note,
					price: update.price,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updatePacketId(inventory_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<InventoryInterface>('app.inventory', transaction)
			.set({ packet_id })
			.where(Array.isArray(inventory_id_or_ids) ? 'id IN (:...inventory_id_or_ids)' : 'id = :inventory_id_or_ids', { inventory_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	async updateStatus(changer_user_id: number, id: number, code: INVENTORIES.AVAILABLE | INVENTORIES.UNAVAILABLE | INVENTORIES.DEFECT, note: string | undefined, transaction: EntityManager): Promise<boolean>
	async updateStatus(changer_user_id: number, id: number, code: INVENTORIES.EXCEPTION, note: string, transaction: EntityManager): Promise<boolean>
	async updateStatus(changer_user_id: number, id: number, code: INVENTORIES.BOOKED, source: BOOKING_SOURCES, reference_id: number, note: string | undefined, transaction: EntityManager): Promise<boolean>

	@RepositoryModel.bound
	async updateStatus(
		changer_user_id: number,
		id: number,
		code: INVENTORIES,
		noteOrSource?: string | BOOKING_SOURCES,
		transactionOrReferenceId?: EntityManager | number,
		note?: string,
		transaction?: EntityManager,
	): Promise<boolean> {
		if(code === INVENTORIES.BOOKED) {
			if(!noteOrSource || ! transactionOrReferenceId) {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_103, 'Please supply source and ref_id when updating status to booking')
			}

			return await this.renew('InventoryRecord', id, {
				status: code,
				...(note === undefined ? {} : { note }),
			}, {
				transaction,
			}).then(async isUpdated => {
				if (isUpdated) {
					await this.save('InventoryBookingRecord', {
						inventory_id: id,
						source: noteOrSource as BOOKING_SOURCES,
						ref_id: transactionOrReferenceId as number,
					})
				}

				return isUpdated
			}).then(isUpdated => {
				if (isUpdated) {
					this.save('HistoryInventoryRecord', {
						changer_user_id,
						inventory_id: id,
						status: code,
						note,
					})
				}

				return isUpdated
			})
		} else {
			return await this.renew('InventoryRecord', id, {
				status: code,
				...(noteOrSource === undefined ? {} : { note: noteOrSource }),
			}, {
				transaction: transactionOrReferenceId as EntityManager,
			}).then(isUpdated => {
				if (isUpdated) {
					this.save('HistoryInventoryRecord', {
						changer_user_id,
						inventory_id: id,
						status: code,
						note: noteOrSource,
					})
				}

				return isUpdated
			})
		}
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async filter(
		offset: number,
		limit: number,
		search: string | undefined,
		date: string | undefined,
		status: INVENTORIES | undefined,
		user_id: number | undefined,
		category_ids: number[] | undefined,
		brand_ids: number[] | undefined,
		size_ids: number[] | undefined,
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	): Promise<{
		count: number,
		data: Array<InventoryInterface & {
			variant: VariantInterface & {
				product: ProductInterface & {
					category: CategoryInterface,
					brand: BrandInterface,
				},
				variantColors: Array <VariantColorInterface & {
					color: ColorInterface,
				}>,
				size: SizeInterface,
				assets: VariantAssetInterface[],
			},
			booking?: InventoryBookingInterface,
			brandAddress: BrandAddressInterface,
			brand: BrandInterface,
		}>,
	}> {
		let Q = this.queryDeep(transaction)
			// .addSelect('inventory.updated_at')
			.leftJoin(subquery => {
				return subquery.from('app.inventory_booking', 'inventory_booking')
					.select('inventory_id, MAX(inventory_booking.id) id')
					.groupBy('inventory_id')
			}, 'last_booking', 'last_booking.inventory_id = inventory.id AND inventory.status = :status', { status: INVENTORIES.BOOKED })
			.leftJoinAndMapOne('inventory.booking', InventoryBookingRecord, 'booking', 'booking.id = last_booking.id')
			.leftJoinAndSelect('inventory.brand', 'seller')
			.leftJoinAndSelect('product.category', 'category')
			.where('inventory.deleted_at IS NULL')

		if (search) {
			if (search.match(/\[([a-z]*)\] +(.*)/)) {
				const match = search.match(/\[([a-z]*)\] +(.*)/)
				const key = match[1]
				const _search = match[2]

				Q = Q.andWhere(`(inventory.${key})::text ILIKE :search`, { search: _search })
			} else {
				Q = Q.andWhere(`(product.title ILIKE :search OR brand.title ILIKE :search)`, { search: `%${search}%` })
			}
		}

		if(category_ids) {
			Q = Q.andWhere('product.category_id IN (:...category_ids)', { category_ids })
		}

		if(brand_ids) {
			Q = Q.andWhere('brand.id IN (:...brand_ids)', { brand_ids })
		}

		if(size_ids) {
			Q = Q.andWhere('variant.size_id IN (:...size_ids)', { size_ids })
		}

		if (date) {
			Q = Q.andWhere(`inventory.updated_at > current_date - interval '${date}'`)
		}

		if (status) {
			Q = Q.andWhere('inventory.status = :last_status', { last_status: status })
		}

		if (user_id) {
			Q = Q.innerJoin('inventory.bookings', 'myBooking', 'myBooking.id = last_booking.id AND inventory.status = :status AND myBooking.ref_id = :user_id', { user_id })
		}

		Q = Q.skip(offset).take(limit).orderBy('inventory.status', 'ASC')

		if (sort_by.created_at) {
			Q = Q.addOrderBy('inventory.created_at', sort_by.created_at)
		}

		if (sort_by.updated_at) {
			Q = Q.addOrderBy('inventory.updated_at', sort_by.updated_at)
		}

		if (sort_by.price) {
			Q = Q.addOrderBy('inventory.price', sort_by.price)
		}

		if (isEmpty(sort_by)) {
			Q = Q.addOrderBy('inventory.updated_at', 'DESC')
		}

		return Q.getManyAndCount()
			.then(this.parser)
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			})as any
	}

	@RepositoryModel.bound
	async get(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<InventoryInterface> {
		return this.query('app.inventory', 'inventory', transaction)
			.where('inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getWithVariant(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<InventoryInterface & {
		variant: VariantInterface,
	}> {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.where('inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddress(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<BrandAddressInterface> {
		return this.query('app.brand_address', 'address', transaction)
			.innerJoin('address.inventories', 'inventory', 'inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(this.parser) as any
	}

	// Get single asset from inventory
	// Used by order to display order detail asset
	@RepositoryModel.bound
	async getAsset(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<VariantAssetInterface> {
		return this.query('app.variant_asset', 'asset', transaction)
			.leftJoin('asset.variant', 'variant')
			.leftJoin('variant.inventories', 'inventories')
			.where('inventories.id = :id', { id: inventory_id })
			.orderBy('asset.order', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	async getBooked(source: BOOKING_SOURCES, id: number, inventory_id: number | undefined, transaction: EntityManager): Promise<InventoryInterface>
	async getBooked(source: BOOKING_SOURCES, id: number, inventory_id: number | undefined, deep: true | undefined, transaction: EntityManager): Promise<InventoryDeepInterface>

	@RepositoryModel.bound
	async getBooked(
		source: BOOKING_SOURCES,
		id: number,
		inventory_id?: number,
		transactionOrDeep?: EntityManager | boolean,
		transaction?: EntityManager,
	): Promise<any> {
		const isDeep = typeof transactionOrDeep === 'boolean' ? !!transactionOrDeep : false
		const trx = typeof transactionOrDeep === 'boolean' ? transaction : transactionOrDeep
		const Q = this.queryBooking((isDeep ? this.queryDeep(trx) : this.query('app.inventory', 'inventory', trx)), source, id)

		if(inventory_id === undefined) {
			return Q.getOne()
				.then(this.parser)
		} else {
			return Q.where('inventory.id = :inventory_id', { inventory_id })
				.getOne()
				.then(this.parser)
		}
	}

	@RepositoryModel.bound
	async getBookedSimple(source: BOOKING_SOURCES, inventory_id: number, transaction: EntityManager): Promise<InventoryInterface> {
		return this.query('app.inventory', 'inventory', transaction)
			.innerJoin(subquery => {
				return subquery.from('app.inventory_booking', 'inventory_booking')
					.select('inventory_id, MAX(inventory_booking.id) id')
					.groupBy('inventory_id')
			}, 'last_booking', 'last_booking.inventory_id = inventory.id AND inventory.status = :status', { status: INVENTORIES.BOOKED })
			.innerJoin(InventoryBookingRecord, 'booking', 'booking.id = last_booking.id AND booking.source = :source', { source })
			.where('inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBooking(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<InventoryBookingInterface> {
		return this.query('app.inventory_booking', 'booking', transaction)
			.where('booking.inventory_id = :inventory_id', { inventory_id })
			.orderBy('id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBrand(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<BrandInterface> {
		return this.query('app.brand', 'brand', transaction)
			.innerJoin('brand.products', 'product')
			.innerJoin('product.variants', 'variant')
			.innerJoin('variant.inventories', 'inventory', 'inventory.id = :inventory_id', { inventory_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDeep(
		inventory_id: number,
		available_only: boolean | undefined,
		transaction: EntityManager,
	): Promise<InventoryDeepInterface> {
		const Q = this.queryDeep(transaction)
			.where('inventory.id = :inventory_id', { inventory_id })

		if(available_only) {
			return Q
				.andWhere('inventory.deleted_at IS NULL')
				.andWhere('inventory.status = :status', { status: INVENTORIES.AVAILABLE })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getDeepFlat(
		inventory_id: number,
		available_only: boolean | undefined,
		transaction: EntityManager,
	): Promise<InventoryInterface & {
		variant: VariantInterface,
		product: ProductInterface,
		brand: BrandInterface,
		asset: VariantAssetInterface,
		category: CategoryInterface,
		size: SizeInterface,
		colors: ColorInterface[],
	}> {
		const Q = this.query('app.inventory', 'inventory', transaction)
			.where('inventory.id = :inventory_id', { inventory_id })
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndMapOne('inventory.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('inventory.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('inventory.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.leftJoinAndMapOne('inventory.category', CategoryRecord, 'category', 'category.id = product.category_id')
			.leftJoinAndMapOne('inventory.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin('variant.variantColors', 'vc')
			.leftJoinAndMapMany('inventory.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.orderBy('asset.order', 'ASC')
			.addOrderBy('vc.order', 'ASC')

		if (available_only) {
			return Q
				.andWhere('inventory.deleted_at IS NULL')
				.andWhere('inventory.status = :status', { status: INVENTORIES.AVAILABLE })
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getDeepMany(
		inventory_ids: number[],
		available_only?: boolean,
		transaction?: EntityManager,
	): Promise<Array<InventoryInterface & {
		variant: VariantInterface,
		product: ProductInterface,
		brand: BrandInterface,
		colors: ColorInterface[],
		size: SizeInterface,
		assets: VariantAssetInterface[],
	}>> {
		const Q = this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndMapOne('inventory.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('inventory.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('inventory.colors', ColorRecord, 'color', 'color.id = variantColors.color_id')
			.leftJoinAndMapOne('inventory.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoinAndMapMany('inventory.assets', VariantAssetRecord, 'assets', 'assets.variant_id = variant.id AND assets.deleted_at IS NULL')
			.where('inventory.id IN (:...inventory_ids)', { inventory_ids })
			.andWhere('inventory.deleted_at IS NULL')
			.orderBy('assets.order', 'ASC')
			.addOrderBy('variantColors.order', 'ASC')

		if (available_only) {
			return Q
				.andWhere('inventory.status = :status', { status: INVENTORIES.AVAILABLE })
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getVariantId(inventory_id: number, transaction): Promise<number> {
		return this.query('app.inventory', 'inventory', transaction)
			.select('inventory.variant_id')
			.where('inventory.id = :inventory_id', {inventory_id})
			.getOne()
			.then(this.parser)
			.then((inventory: InventoryInterface) => inventory.variant_id)
	}

	@RepositoryModel.bound
	async getIdFromVariant(
		variant_id: number,
		quantity: number = 1,
		transaction: EntityManager,
	): Promise<number[]> {
		return this.query('app.inventory', 'inventory', transaction)
			.select('inventory.id')
			.where('inventory.variant_id = :variant_id', { variant_id })
			.andWhere('inventory.deleted_at IS NULL')
			.andWhere('inventory.status = :status', { status: INVENTORIES.AVAILABLE })
			.take(quantity)
			.getMany()
			.then((inventories: InventoryInterface[]) => inventories.map(i => i.id))
			.then(this.parser)
	}

	async getFromVariant(variant_id: number, quantity: number | undefined, deep: false | undefined, available: boolean, transaction: EntityManager): Promise<InventoryInterface[]>
	async getFromVariant(variant_id: number, quantity: number | undefined, deep: true | undefined, available: boolean, transaction: EntityManager): Promise<InventoryDeepInterface[]>

	@RepositoryModel.bound
	async getFromVariant(
		variant_id: number,
		quantity: number = 1,
		deep: boolean = true,
		available: boolean = true,
		transaction: EntityManager,
	): Promise<InventoryDeepInterface[] | InventoryInterface[]> {
		const Q = (deep ? this.queryDeep(transaction) : this.query('app.inventory', 'inventory', transaction))
			.where('inventory.variant_id = :variant_id', { variant_id })
			.andWhere('inventory.deleted_at IS NULL')

		if (available) {
			return Q
				.andWhere('inventory.status = :status', { status: INVENTORIES.AVAILABLE })
				.take(quantity)
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.take(quantity)
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getHistories(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<{
		bookings: Array<{
			source: BOOKING_SOURCES,
			ref_id: number,
			created_at: Date,
			orderDetail?: OrderDetailInterface & {
				order: OrderInterface,
			},
			stylesheet?: StylesheetInterface & {
				matchbox: MatchboxInterface,
			},
			user?: UserInterface & {
				profile: UserProfileInterface,
			},
			voucher?: VoucherInterface,
		}>,
		histories: Array<{
			status?: INVENTORIES,
			note?: string,
			price?: number,
			brand_address_id?: number,
			created_at: Date,
		}>,
	}> {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.bookings', 'bookings')
			.leftJoinAndMapMany('inventory.histories', HistoryInventoryRecord, 'histories', 'histories.inventory_id = inventory.id')
			.leftJoinAndMapOne('bookings.orderDetail', OrderDetailRecord, 'orderDetail', 'bookings.source = :order_detail_source AND bookings.ref_id = orderDetail.id', { order_detail_source: BOOKING_SOURCES.ORDER_DETAIL })
			.leftJoinAndSelect('orderDetail.order', 'order')
			.leftJoinAndMapOne('bookings.stylesheet', StylesheetRecord, 'stylesheet', 'bookings.source = :stylesheet_source AND bookings.ref_id = stylesheet.id', { stylesheet_source: BOOKING_SOURCES.STYLESHEET })
			.leftJoinAndSelect('stylesheet.matchbox', 'matchbox')
			.leftJoinAndMapOne('bookings.user', UserRecord, 'user', 'bookings.source = :user_source AND bookings.ref_id = user.id', { user_source: BOOKING_SOURCES.USER })
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndMapOne('bookings.voucher', VoucherRecord, 'voucher', 'bookings.source = :voucher_source AND bookings.ref_id = voucher.id', { voucher_source: BOOKING_SOURCES.VOUCHER })
			.where('inventory.id = :inventory_id', { inventory_id })
			.orderBy('histories.id', 'DESC')
			.addOrderBy('bookings.id', 'DESC')
			.getOne()
			.then(this.parser)
			.then((inventory: InventoryInterface & {
				bookings: Array<InventoryBookingInterface & {
					orderDetail: OrderDetailInterface,
					stylesheet: StylesheetInterface,
					user: UserInterface,
					voucher: VoucherInterface,
				}>,
				histories: HistoryInventoryInterface[],
			}) => {
				return {
					bookings: inventory.bookings.map(booking => {
						return {
							source: booking.source,
							ref_id: booking.ref_id,
							created_at: booking.created_at,
							orderDetail: booking.orderDetail,
							stylesheet: booking.stylesheet,
							user: booking.user,
							voucher: booking.voucher,
						}
					}),
					histories: inventory.histories.map(history => {
						return {
							status: history.status,
							note: history.note,
							price: history.price,
							brand_address_id: history.brand_address_id,
							created_at: history.created_at,
						}
					}),
				}
			}) as any
	}

	async getIdFromBooking(source: BOOKING_SOURCES, id: number, transaction: EntityManager): Promise<number | null>
	async getIdFromBooking(source: BOOKING_SOURCES, id: number, strict: boolean | undefined, transaction: EntityManager): Promise<number | null>

	@RepositoryModel.bound
	async getIdFromBooking(
		source: BOOKING_SOURCES,
		id: number,
		transactionOrStrict?: EntityManager | boolean,
		transaction?: EntityManager,
	) {
		const isStrict = typeof transactionOrStrict === 'boolean' ? transactionOrStrict : true
		const trx = typeof transactionOrStrict === 'boolean' ? transaction : transactionOrStrict

		if (isStrict) {
			// Is Strict only return value if the booking is inventory's latest booking
			return this.queryBooking(this.query('app.inventory', 'inventory', trx), source, id)
				.select(['inventory.id'])
				.getOne()
				.then(this.parser)
				.then((inventory: InventoryInterface) => inventory.id)
		} else {
			return this.getOne('InventoryBookingRecord', {
				ref_id: id,
				source,
			}, {
				select: ['inventory_id'],
				transaction,
			}).then(booking => {
				return booking.inventory_id
			})
		}
	}

	@RepositoryModel.bound
	async getStatus(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<{
		status: INVENTORIES,
		source: BOOKING_SOURCES | null,
		ref_id: number | null,
	}> {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect(query => {
				return query.from('app.inventory_booking', 'bookings')
					.where('bookings.inventory_id = :inventory_id', { inventory_id })
					.orderBy('bookings.id', 'DESC')
					.take(1)
			}, 'booking', 'booking.inventory_id = inventory.id')
			.where('inventory.id = :inventory_id', { inventory_id })
			.getRawOne()
			.then(this.parser)
			.then(inventory => {
				return {
					status: inventory.inventory_status,
					source: inventory.source,
					ref_id: inventory.ref_id,
				}
			}) as any
	}

	@RepositoryModel.bound
	async getListInventory(
		filter: {
			offset?: number,
			limit?: number,
			search?: string,
			sort_by_created_at?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	): Promise<{
		count: number,
		data: Array<InventoryInterface & {
			variant: VariantInterface & {
				product: ProductInterface,
				brand: BrandInterface,
				size: SizeInterface,
				colors: ColorInterface[],
				tags: TagInterface[],
				measurements: MeasurementInterface[],
			},
		}>,
	}> {
		const Q = this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndMapOne('inventory.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(VariantColorRecord, 'variant_color', 'variant_color.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'colors', 'colors.id = variant_color.color_id')
			.leftJoin(VariantTagRecord, 'variant_tag', 'variant_tag.variant_id = variant.id')
			.leftJoinAndMapMany('variant.tags', TagRecord, 'tags', 'tags.id = variant_tag.tag_id')
			.leftJoin(VariantMeasurementRecord, 'variant_measurement', 'variant_measurement.variant_id = variant.id')
			.leftJoinAndMapMany('variant.measurements', MeasurementRecord, 'measurements', 'measurements.id = variant_measurement.measurement_id')

			if (!!filter.search) {
				Q.andWhere('product.title ILIKE :search OR brand.title ILIKE :search', { search: `%${filter.search}%` })
			}

			if (filter.limit) {
				Q.take(filter.limit)
			}

			if (filter.offset) {
				Q.skip(filter.offset)
			}

			if (filter.sort_by_created_at) {
				Q.orderBy('inventory.created_at', filter.sort_by_created_at)
			} else {
				Q.orderBy('inventory.id')
			}

		return Q.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async getPacketId(inventory_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('InventoryRecord', {
			id: inventory_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	@RepositoryModel.bound
	async getDeepPacketId(inventory_id: number, transaction: EntityManager): Promise<InventoryInterface & {
		variant: VariantInterface & {
			product: ProductInterface,
		},
	}> {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('variant.product', 'product')
			.where('inventory.id = :inventory_id', {inventory_id})
			.getOne()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async removeInventory(
		idOrIds: number | number[],
		transaction: EntityManager,
	) {
		return this.renew('InventoryRecord', Array.isArray(idOrIds) ? `id IN (:...id_or_ids)` : idOrIds as number, {
			deleted_at: new Date(),
		}, {
			parameters: {
				id_or_ids: idOrIds,
			},
			transaction,
		})
	}

	// ============================ PRIVATES ============================
	private queryDeep(transaction: EntityManager) {
		return this.query('app.inventory', 'inventory', transaction)
			.leftJoinAndSelect('inventory.variant', 'variant')
			.leftJoinAndSelect('inventory.brandAddress', 'brandAddress')
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndSelect('variant.variantColors', 'variantColors')
			.leftJoinAndSelect('variantColors.color', 'color')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndSelect('variant.assets', 'assets')
			.orderBy('variantColors.order', 'ASC')
			.addOrderBy('assets.order', 'ASC')
	}

	private queryBooking(query: SelectQueryBuilder<{}>, source: BOOKING_SOURCES, ref_id: number) {
		return query
			.innerJoin(subquery => {
				return subquery.from('app.inventory_booking', 'inventory_booking')
					.select('inventory_id, MAX(inventory_booking.id) id')
					.groupBy('inventory_id')
			}, 'last_booking', 'last_booking.inventory_id = inventory.id AND inventory.status = :status', { status: INVENTORIES.BOOKED })
			.innerJoin(InventoryBookingRecord, 'booking', 'booking.id = last_booking.id AND booking.source = :source AND booking.ref_id = :ref_id', { source, ref_id })
	}
}
