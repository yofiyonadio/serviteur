import RepositoryModel from 'app/models/repository'
import { EntityManager, EntityRepository } from 'typeorm'


import {
	StorefrontScheduleRecord,
	StorefrontRecord,
	VariantAssetRecord,
	VariantStorefrontRecord,
	VariantRecord,
	ColorRecord,
	VariantColorRecord,
	CouponRecord,
	ProductRecord,
	BrandRecord,
} from 'energie/app/records'


import {
	VariantInterface,
	ColorInterface,
	CouponInterface,
	StorefrontInterface,
	StorefrontScheduleInterface,
	VariantAssetInterface,
	SizeInterface,
	ProductInterface,
	BrandInterface,
} from 'energie/app/interfaces'

import { ObjectOrArray, Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'


@EntityRepository()
export default class StorefrontRepository extends RepositoryModel<{
	StorefrontRecord: StorefrontRecord,
	StorefrontScheduleRecord: StorefrontScheduleRecord,
	VariantStorefrontRecord: VariantStorefrontRecord,
}> {
	static __displayName = 'StorefrontRepository'

	protected records = {
		StorefrontRecord,
		StorefrontScheduleRecord,
		VariantStorefrontRecord,
	}

	// ============================= INSERT =============================

	async insertSchedule(schedule: Parameter<StorefrontScheduleInterface>, transaction: EntityManager): Promise<StorefrontScheduleInterface>
	async insertSchedule(schedules: Array<Parameter<StorefrontScheduleInterface>>, transaction: EntityManager): Promise<StorefrontScheduleInterface[]>

	@RepositoryModel.bound
	async insertSchedule(
		schedule: ObjectOrArray<Parameter<StorefrontScheduleInterface>>,
		transaction: EntityManager,
	): Promise<any> {
		if (Array.isArray(schedule)) {
			return await this.save('StorefrontScheduleRecord', schedule as StorefrontScheduleInterface[], transaction)
		} else {
			return await this.save('StorefrontScheduleRecord', schedule as StorefrontScheduleInterface, transaction)
		}
	}

	@RepositoryModel.bound
	async insert(
		storefront: Parameter<StorefrontInterface>,
		transaction: EntityManager,
	): Promise<StorefrontInterface> {
		return this.save('StorefrontRecord', storefront, transaction)
	}

	@RepositoryModel.bound
	async insertVariant(
		variant_id: number,
		storefront_id: number,
		transaction: EntityManager,
	): Promise<any> {
		return await this.save('VariantStorefrontRecord', { variant_id, storefront_id }, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		id: number,
		storefront: Parameter<StorefrontInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StorefrontRecord', id, CommonHelper.stripUndefined(storefront), {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updateSchedule(
		id: number,
		start_date: Date,
		end_date: Date,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('StorefrontScheduleRecord', id, CommonHelper.stripUndefined({ start_date, end_date }), { transaction })
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getVariants(
		offset: number,
		limit: number,
		filter: {
			id: number,
		},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	): Promise<VariantInterface & {
		size: SizeInterface,
		asset: VariantAssetInterface,
		product: ProductInterface,
		brand: BrandInterface,
		colors: ColorInterface[],
	}> {
	// ) {
		let Q = this.query('app.variant', 'variant', transaction)
			.leftJoinAndSelect(VariantStorefrontRecord, 'storefrontVariant', 'variant.id = storefrontVariant.variant_id')
			
			Q = Q.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')

			.where('variant.deleted_at IS NULL AND storefrontVariant.storefront_id = :id', { id: filter.id })


		if (sort_by.created_at) {
			Q = Q.addOrderBy('variant.created_at', sort_by.created_at)
		}

		if (sort_by.updated_at) {
			Q = Q.addOrderBy('variant.updated_at', sort_by.updated_at)
		}

		if (sort_by.price) {
			Q = Q.addOrderBy('variant.price', sort_by.price)
		}

		if (!!limit) {
			Q = Q.take(limit)
		}

		if (!!offset) {
			Q = Q.skip(offset)
		}
		return Q.getManyAndCount().then(datas => {
			return {
				count: datas[1],
				data: datas[0].map((v : any) => {
					return {
						id: v.id,
						title: v.product.title,
						brand: v.brand.title,
						price: v.price,
						size: v.size.title,
						retail_price: v.retail_price,
						image: CommonHelper.allowKey(v.asset, 'id', 'url'),
						colors: v.colors.map(color => {
							return {
								id: color.id,
								title: color.title,
								hex: color.hex,
							}
						}),
					}
				})
			}
		}).then(this.parser) as any
	}

	@RepositoryModel.bound
	async filter(
		filter: {
			offset: number,
			limit: number,
		},
		query: {
			include_zero_variant: boolean,
			show_on_page: boolean,
			price: string,
			with_variant?: boolean,
		},
		transaction: EntityManager,
	): Promise<{
		data: Array<StorefrontInterface & {
			variants: Array<VariantInterface & {
				size: SizeInterface,
				asset: VariantAssetInterface,
				product: ProductInterface,
				brand: BrandInterface,
				colors: ColorInterface[],
			}>,
			schedules: StorefrontScheduleInterface[],
			coupon: CouponInterface,
		}>,
		count: number,
	}> {
		let Q = this.query('app.storefront', 'storefront', transaction)
		
		if (query.with_variant) {
			Q = Q.leftJoin(VariantStorefrontRecord, 'storefrontVariant', 'storefront.id = storefrontVariant.storefront_id')
			.leftJoinAndMapMany('storefront.variants', VariantRecord, 'variant', 'storefrontVariant.variant_id = variant.id AND variant.deleted_at IS NULL')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'variant.product_id = product.id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			Q = Q.orderBy('colors.order', 'ASC')
				.orderBy('asset.index', 'ASC')
			.orderBy('storefront.created_at', 'DESC')
		}

		if (filter.offset) {
			Q = Q.skip(filter.offset)
		}

		if (filter.limit) {
			Q = Q.take(filter.limit)
		}

		if (query.show_on_page) {
			Q = Q.andWhere('storefront.show_on_page IS TRUE')
		}

		if (query.price === 'ASC') {
			Q = Q.orderBy('variant.price', 'ASC')
		} else if (query.price === 'DESC') {
			Q = Q.orderBy('variant.price', 'DESC')
		}

		return Q.getManyAndCount()
			.then(this.parseCount) as any
	}

	@RepositoryModel.bound
	async get(
		filter: {
			slug?: string,
			id?: number,
		},
		include_zero_variant: boolean,
		with_variant: boolean,
		transaction: EntityManager,
	): Promise<StorefrontInterface & {
		variants: Array<VariantInterface & {
			size: SizeInterface,
			asset: VariantAssetInterface,
			product: ProductInterface,
			brand: BrandInterface,
			colors: ColorInterface[],
		}>,
		schedules: StorefrontScheduleInterface[],
		coupon: CouponInterface,
	}> {
		let Q = this.query('app.storefront', 'storefront', transaction)
			.leftJoin(VariantStorefrontRecord, 'storefrontVariant', 'storefront.id = storefrontVariant.storefront_id')
			.leftJoinAndMapMany('storefront.schedules', StorefrontScheduleRecord, 'schedule', 'storefront.id = schedule.storefront_id')
			.leftJoinAndMapOne('storefront.coupon', CouponRecord, 'coupon', 'storefront.coupon_id = coupon.id')
			.leftJoinAndMapMany('storefront.variants', VariantRecord, 'variant', 'storefrontVariant.variant_id = variant.id AND variant.deleted_at IS NULL')

		if (with_variant) {
			Q = Q.leftJoinAndSelect('variant.size', 'size')
				.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'variant.product_id = product.id')
				.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'product.brand_id = brand.id')
				.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id AND asset.deleted_at IS NULL')
				.leftJoin(query => {
					return query.from(VariantColorRecord, 'variant_color')
						.orderBy('variant_color.order', 'ASC')
				}, 'vc', 'vc.variant_id = variant.id')
				.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
				Q = Q.orderBy('colors.order', 'ASC')
					.orderBy('asset.index', 'ASC')
		}
		if (filter.slug) {
			Q = Q.where('storefront.slug = :slug', { slug: filter.slug })
		} else if (filter.id) {
			Q = Q.where('storefront.id = :id', { id: filter.id })
		}

		return Q.getOne()
			.then(this.parser) as any

	}

	@RepositoryModel.bound
	async getStorefrontVariant(
		storefront_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.variant_storefront', 'vs', transaction)
			.where('vs.storefront_id = :storefront_id', { storefront_id })
			.andWhere('vs.variant_id = :variant_id', { variant_id })
			.getManyAndCount()
			.then(this.parser) as any
	}


	// ============================= DELETE =============================

	@RepositoryModel.bound
	async deleteVariant(
		storefront_id: number,
		variant_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.queryDelete('app.variant_storefront', transaction)
			.where('variant_storefront.variant_id = :variant_id', { variant_id })
			.andWhere('variant_storefront.storefront_id = :storefront_id', { storefront_id })
			.execute()
			.then(_ => true)
	}

	@RepositoryModel.bound
	async deleteSchedule(
		id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return await this.queryDelete('app.storefront_schedule', transaction)
			.where('storefront_schedule.storefront_id = :id', { id })
			.execute().then(_ => true)
	}
}

