import {
	ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	TopupRecord,
	TopupAmountRecord,
} from 'energie/app/records'

import {
	TopupInterface,
	TopupAmountInterface,
} from 'energie/app/interfaces'

import { ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class TopupRepository extends RepositoryModel<{
	TopupRecord: TopupRecord,
	TopupAmountRecord: TopupAmountRecord,
}> {

	static __displayName = 'TopupRepository'

	protected records = {
		TopupRecord,
		TopupAmountRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(data: Partial<TopupInterface>, transaction: EntityManager): Promise<TopupInterface> {
		return this.save('TopupRecord', data, transaction)
	}

	@RepositoryModel.bound
	async insertAmount(data: Partial<TopupAmountInterface>, transaction: EntityManager): Promise<TopupInterface & {
		amount: number,
	}> {
		return this.getOne('TopupAmountRecord', {
			topup_id: data.topup_id,
			amount: data.amount,
		}, {
			transaction,
		}).catch(async err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.save('TopupAmountRecord', {
					topup_id: data.topup_id,
					amount: data.amount,
				}, transaction)
			}

			throw err
		}).then(tA => {
			return this.getTopupAmount(tA.id, transaction)
		})
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getBy(
		type: 'id' | 'slug' = 'slug',
		id: number | string,
		transaction: EntityManager,
	): Promise<TopupInterface> {
		return this.query('app.topup', 'topup', transaction)
			.where(type === 'id' ? 'topup.id = :id' : 'topup.slug = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getTopupAmount(topup_amount_id: number, transaction: EntityManager): Promise<TopupInterface & {
		amount: number,
		topup_id: number,
	}> {
		return this.query('app.topup_amount', 'tA', transaction)
			.innerJoinAndSelect('tA.topup', 'topup')
			.where('tA.id = :topup_amount_id', { topup_amount_id })
			.getOne()
			.then(this.parser)
			.then((data: any) => {
				return {
					...data.topup,
					id: data.id,
					topup_id: data.topup_id,
					amount: data.amount,
				}
			})
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
