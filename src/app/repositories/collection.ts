import RepositoryModel from 'app/models/repository'

import {
   EntityRepository,
   EntityManager,
   In,
} from 'typeorm'

import {
	CollectionRecord,
} from 'energie/app/records'

import {
   CollectionInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'


@EntityRepository()
export default class CollectionRepository extends RepositoryModel<{
	CollectionRecord: CollectionRecord,
}> {

	static __displayName = 'CollectionRepository'

	protected records = {
		CollectionRecord,
   }

   // ============================= INSERT =============================
   async upsertCollection(datas: Array<Parameter<CollectionInterface>>, transaction: EntityManager) {
		return this.insertOrUpdate('master.collection', datas, undefined, transaction)
   }

	// ============================= UPDATE =============================

   // ============================= GETTER =============================
	@RepositoryModel.bound
	async getByIds(ids: number[], transaction: EntityManager): Promise<CollectionInterface[]> {
		return this.getMany('CollectionRecord', {
			id: In(ids),
		}, { transaction }) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
