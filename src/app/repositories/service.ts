import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	ServiceRecord,
	ServiceAssetRecord,
	ServiceCouponRecord,
} from 'energie/app/records'

import {
	ServiceInterface, BrandInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class ServiceRepository extends RepositoryModel<{
	ServiceRecord: ServiceRecord,
	ServiceAssetRecord: ServiceAssetRecord,
	ServiceCouponRecord: ServiceCouponRecord,
}> {

	static __displayName = 'ServiceRepository'

	protected records = {
		ServiceRecord,
		ServiceAssetRecord,
		ServiceCouponRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(data: Partial<ServiceInterface>, transaction: EntityManager): Promise<ServiceInterface> {
		return this.save('ServiceRecord', data, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getBy(
		type: 'id' | 'slug' = 'slug',
		id: number | string,
		with_brand: boolean,
		transaction: EntityManager,
	): Promise<ServiceInterface & {
		brand?: BrandInterface,
	}> {
		const Q = this.query('app.service', 'service', transaction)
			.where(type === 'id' ? 'service.id = :id' : 'service.slug = :id', { id })

		if (with_brand) {
			return Q
				.leftJoinAndSelect('service.brand', 'brand')
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getBrand(service_id: number, transaction: EntityManager): Promise<BrandInterface> {
		return this.query('app.brand', 'brand', transaction)
			.innerJoin('brand.services', 'service', 'service.id = :service_id', { service_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByTitle(title: string, transaction: EntityManager): Promise<Array<ServiceInterface & {
		brand: BrandInterface,
		// asset: ServiceAssetInterface,
	}>> {
		return this.query('app.service', 'service', transaction)
			.leftJoinAndSelect('service.brand', 'brand')
			// .leftJoinAndMapOne('service.asset', ServiceAssetRecord, 'asset', 'asset.service_id = service.id AND asset.deleted_at IS NULL')
			.where(`service.title ILIKE '%${title}%'`)
			// .orderBy('asset', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(service_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('ServiceRecord', {
			id: service_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
