import {
	ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	ExchangeRecord,
	ExchangeDetailRecord,
	HistoryExchangeRecord,
	HistoryExchangeDetailRecord,
	ShipmentRecord,
	StylesheetInventoryRecord,
	InventoryRecord,
	StylesheetRecord,
	ProductRecord,
	BrandRecord,
	VariantAssetRecord,
	MatchboxAssetRecord,
	// CategoryRecord,
	ColorRecord,
	SizeRecord,
	OrderRecord,
	VariantRecord,
	// QuestionRecord,
	FeedbackRecord,
	// OrderRequestRecord,
	VariantColorRecord,
	ShipmentOrderDetailRecord,
	OrderDetailRecord,
	ShipmentAddressRecord,
	PacketRecord,
	FeedbackAnswerRecord,
	QuestionRecord,
	UserRecord,
	UserProfileRecord,
	OrderDetailCampaignRecord,
	ShipmentExchangeDetailRecord,
	UserAddressRecord,
} from 'energie/app/records'

import {
	ExchangeInterface,
	ExchangeDetailInterface,
	OrderDetailInterface,
	ShipmentInterface,
	HistoryExchangeInterface,
	HistoryExchangeDetailInterface,
	VariantInterface,
	ProductInterface,
	BrandInterface,
	ColorInterface,
	SizeInterface,
	InventoryInterface,
	OrderInterface,
	UserInterface,
	UserProfileInterface,
	StylesheetInterface,
	// StylesheetInventoryInterface,
	VariantAssetInterface,
	MatchboxAssetInterface,
	// CategoryInterface,
	FeedbackInterface,
	FeedbackAnswerInterface,
	// QuestionInterface,
	// VoucherInterface,
	// ServiceInterface,
	ShipmentAddressInterface,
	PacketInterface,
	QuestionInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	UserAddressInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

// import DEFAULTS from 'utils/constants/default'
import { CODES, EXCHANGES, ORDER_DETAILS } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class ExchangeRepository extends RepositoryModel<{
	ExchangeRecord: ExchangeRecord,
	ExchangeDetailRecord: ExchangeDetailRecord,
	HistoryExchangeRecord: HistoryExchangeRecord,
	HistoryExchangeDetailRecord: HistoryExchangeDetailRecord,
}> {

	static __displayName = 'ExchangeRepository'

	protected records = {
		ExchangeRecord,
		ExchangeDetailRecord,
		HistoryExchangeRecord,
		HistoryExchangeDetailRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insertExchange(data: Parameter<ExchangeInterface>, transaction: EntityManager): Promise<ExchangeInterface> {
		return await this.save('ExchangeRecord', data, transaction)
	}

	@RepositoryModel.bound
	async insertDetail(data: Array<Parameter<ExchangeDetailInterface>>, transaction: EntityManager): Promise<ExchangeDetailInterface[]> {
		return await this.save('ExchangeDetailRecord', data, transaction)
	}

	@RepositoryModel.bound
	async insertShipment(
		datas: Array<{
			shipment_id: number,
			exchange_detail_id: number,
		}>,
		transaction: EntityManager,
	) {
		return this.queryInsert('app.shipment_exchange_detail', transaction)
			.values(datas)
			.execute()
			.then(insert => insert.raw)
	}
	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(id: number, changer_user_id: number, data: Partial<ExchangeInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('ExchangeRecord', id, data, { transaction }).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryExchangeRecord', {
					exchange_id: id,
					changer_user_id,
					status: data.status,
					note: data.note,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updateDetail(id: number, changer_user_id: number, data: Partial<ExchangeDetailInterface>, transaction: EntityManager): Promise<boolean> {
		return await this.renew('ExchangeDetailRecord', id, data, { transaction }).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryExchangeDetailRecord', {
					exchange_detail_id: id,
					changer_user_id,
					status: data.status,
					note: data.note,
				})
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async get<
		WD extends boolean,
		WF extends boolean,
		WRS extends boolean,
		WU extends boolean,
		WO extends boolean,
		WV extends boolean,
		WUA extends boolean,
		WRLS extends boolean,
		WRI extends boolean,
		WRA extends boolean,
		Return extends ExchangeInterface & (
		WU extends true ? {
			user: UserInterface & {
				profile: UserProfileInterface,
			} & (
			WUA extends true ? {
			addresses: UserAddressInterface[],
			} : {}),
		} : {}) & (
		WRS extends true ? {
			return_shipment?: ShipmentInterface,
		} : {}) & (
		WD extends true ? {
		details: Array<ExchangeDetailInterface & (
			WO extends true ? {
			order: OrderInterface & {
				detail: OrderDetailInterface,
			},
			} : {}) & (
			WV extends true ? {
			variant: VariantInterface & {
				inventory: InventoryInterface,
				product: ProductInterface,
				brand: BrandInterface,
				size: SizeInterface,
				colors: ColorInterface[],
				asset: VariantAssetInterface,
			},
			} : {}) & (
			WRLS extends true ? {
			replacement_shipment?: ShipmentInterface & (
				WRA extends true ? {
				address: ShipmentAddressInterface,
				} : {}),
			} : {}) & (
			WRI extends true ? {
			replacement_item?: VariantInterface & {
				product: ProductInterface,
				brand: BrandInterface,
				asset: VariantAssetInterface,
				size?: SizeInterface,
				colors?: ColorInterface[],
			},
			} : {}) & (
			WF extends true ? {
			feedback: FeedbackInterface & {
				answers: Array<FeedbackAnswerInterface & {
					question: QuestionInterface,
				}>,
			},
			} : {}
		)>,
		} : {})
	>(
		id: number,
		order_id: number,
		option: {
			with_details?: WD,
			with_feedback?: WF,
			with_return_shipment?: WRS,
			with_replacement_shipment?: WRLS,
			with_replacement_address?: WRA,
			with_replacement_item?: WRI,
			with_user?: WU,
			with_user_addresses?: WUA,
			with_order?: WO,
			with_variant?: WV,
		} = {},
		filter: {
			approved_exchange?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<Return> {
		let Q = this.query('app.exchange', 'exchange', transaction)
		if (!!id) {
			Q = Q.where('exchange.id = :id', {id})

		}

		if (!!option.with_user) {
			Q = Q.leftJoinAndMapOne('exchange.user', UserRecord, 'user', 'user.id = exchange.user_id')
				.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')

			if (!!option.with_user_addresses) {
				Q = Q.leftJoinAndMapMany('user.addresses', UserAddressRecord, 'address', 'address.user_id = user.id')
			}
		}

		if (!!option.with_details) {
			Q = Q.leftJoinAndMapMany('exchange.details', ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
				.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = detail.id')
				.leftJoin(OrderDetailRecord, '__orderDetail__', '__orderDetail__.id = detail.order_detail_id')

			if (!!option.with_order) {
				Q = Q.leftJoinAndMapOne('detail.order', OrderRecord, 'order', 'order.id = __orderDetail__.order_id')
					.leftJoinAndMapOne('order.detail', OrderDetailRecord, 'orderDetail', 'orderDetail.order_id = order.id and orderDetail.id = __orderDetail__.id')
			}

			if (!!order_id) {
				Q = Q.where('order.id = :order_id', { order_id })
			}

			if (!!option.with_variant) {
				Q = Q.leftJoin(InventoryRecord, 'inventory', 'inventory.id = detail.inventory_id')
					.leftJoinAndMapOne('detail.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
					.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'vI', 'vI.id = inventory.id and vI.variant_id = variant.id')
					.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
					.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
					.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
					.leftJoin(query => {
						return query.from(VariantColorRecord, 'variant_color')
							.orderBy('variant_color.order', 'ASC')
					}, 'vc', 'vc.variant_id = variant.id')
					.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, '_variant_asset')
							.orderBy('_variant_asset.order', 'ASC')
							.addOrderBy('_variant_asset.id', 'ASC')
					}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
					.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
			}

			if (!!option.with_replacement_shipment) {
				Q =	Q.leftJoinAndMapOne('detail.replacement_shipment', ShipmentRecord, 'replacement_shipment', 'replacement_shipment.id = sed.shipment_id and replacement_shipment.is_return is false')

				if (!!option.with_replacement_address) {
					Q = Q.leftJoinAndMapOne('replacement_shipment.address', ShipmentAddressRecord, 'rad', 'rad.id = replacement_shipment.destination_id')
				}
			}

			if (!!option.with_replacement_item) {
				Q = Q.leftJoin(InventoryRecord, 'replacement_item', 'replacement_item.id = detail.replacement_inventory_id')
					.leftJoinAndMapOne('detail.replacement_item', VariantRecord, 'rivariant', 'rivariant.id = replacement_item.variant_id')
					.leftJoinAndMapOne('rivariant.product', ProductRecord, 'riproduct', 'riproduct.id = rivariant.product_id')
					.leftJoinAndMapOne('rivariant.brand', BrandRecord, 'ribrand', 'ribrand.id = riproduct.brand_id')
					.leftJoinAndMapOne('rivariant.size', SizeRecord, 'risize', 'risize.id = rivariant.size_id')
						.leftJoin(query => {
							return query.from(VariantColorRecord, 'rivariant_color')
								.orderBy('rivariant_color.order', 'ASC')
						}, 'rivc', 'rivc.variant_id = rivariant.id')
						.leftJoinAndMapMany('rivariant.colors', ColorRecord, 'ricolor', 'ricolor.id = rivc.color_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'ri_variant_asset')
							.orderBy('ri_variant_asset.order', 'ASC')
							.addOrderBy('ri_variant_asset.id', 'ASC')
					}, 'ri_va', 'ri_va.variant_id = rivariant.id AND ri_va.deleted_at IS NULL')
					.leftJoinAndMapOne('rivariant.asset', VariantAssetRecord, 'rivasset', 'rivasset.id = ri_va.id')
			}

			if (!!option.with_feedback) {
				Q = Q.leftJoinAndMapOne('detail.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = detail.order_detail_id and feedback.user_id = exchange.user_id and feedback.type = 'VARIANT' and feedback.ref_id = variant.id`)
					.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
					.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
			}
		} else {
			Q = Q.leftJoin(ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
				.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = detail.id')
		}

		if (!!option.with_return_shipment) {
			Q = Q.leftJoinAndMapOne('exchange.return_shipment', ShipmentRecord, 'return_shipment', 'return_shipment.id = sed.shipment_id and return_shipment.is_return is true')
		}

		if (!!filter.approved_exchange) {
			Q = Q.andWhere(`detail.status = 'APPROVED'`)
		}
		
		if (!!id) {
			return	Q.getOne()
				.then(this.parser) as any
		} else {
			return Q.getManyAndCount()
			.then(this.parser)
			.then(res => {
				return {
					data: res[0],
					count: res[1],
				}
			}) as any
		}
	}

	@RepositoryModel.bound
	async filter(
		offset: number,
		limit: number,
		status: CODES | null | undefined,
		search: string | undefined,
		transaction: EntityManager,
	): Promise<{
		data: Array<ExchangeInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			details: Array<ExchangeDetailInterface & {
				orderDetail: OrderDetailInterface & {
					order: OrderInterface & {
					},
				},
				/** DEPRECATED!!!!!!!!!!!!!!!!! */
				replacement?: OrderDetailInterface,
			}>,
			return_shipment?: ShipmentInterface,
		}>,
		count: number,
	}> {
		let Q = this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndMapMany('exchange.details', ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
			.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = detail.id')

			// get user
			.leftJoinAndMapOne('exchange.user', UserRecord, 'user', 'user.id = exchange.user_id')
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')

			// get order
			.leftJoinAndMapOne('detail.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.id = detail.order_detail_id')
			.leftJoinAndMapOne('orderDetail.order', OrderRecord, 'order', 'order.id = orderDetail.order_id')

			// DEPRECATED!!!!!!!!!!!
			// get order detail replacement id
			.leftJoinAndMapOne('detail.replacement', OrderDetailRecord, 'replacement', 'replacement.id = detail.replacement_id')

			// get return shipment
			.leftJoinAndMapOne('exchange.return_shipment', ShipmentRecord, 'return_shipment', 'return_shipment.id = sed.shipment_id and return_shipment.is_return is true')

		if(status) {
			Q = Q.andWhere('exchange.status = :status', { status })
		} else if(status === null) {
			Q = Q.andWhere('exchange.status IS NULL')
		}

		if(search) {
			Q = Q.andWhere(`(order.number ILIKE '%${search}%' OR profile.first_name || ' ' || profile.last_name ILIKE '%${search}%')`)
		}

		return Q.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parser)
			.then(res => {
				return {
					data: res[0],
					count: res[1],
				}
			}) as any
	}

	/** list of exchange with no return_shipment */
	@RepositoryModel.bound
	async getNoShipmentList(
		user_id: number,
		filter: {
			expired?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<Array<FeedbackInterface & {
		answer: FeedbackAnswerRecord,
		variant: VariantInterface & {
			inventory: InventoryInterface,
			product: ProductInterface,
			brand: BrandInterface,
			size: SizeInterface,
			colors: ColorInterface[],
			asset: VariantAssetInterface,
		},
		shipment: ShipmentInterface,
	}>>  {
			// first get feedback with return or exchange answer
		const Q = this.query('app.feedback', 'feedback', transaction)
			.leftJoinAndMapOne('feedback.answer', FeedbackAnswerRecord, 'answer', `answer.feedback_id = feedback.id and answer.question_id = 69`)
			.where('feedback.user_id = :user_id', {user_id})
			.andWhere(`(answer.answer ->> 'TAB')::text in ('Return', 'Exchange')`)

			// get order detail
			.leftJoin(OrderDetailRecord, 'orderDetail', 'orderDetail.id = feedback.order_detail_id')

			// get variant
			.leftJoinAndMapOne('feedback.variant', VariantRecord, 'variant', `variant.id = feedback.ref_id and feedback.type = 'VARIANT'`)

			// get stylesheet if order from matchbox
			.leftJoin(StylesheetInventoryRecord, 'si', `si.stylesheet_id = orderDetail.ref_id and orderDetail.type = 'STYLESHEET' and si.deleted_at is null`)
			.leftJoin(InventoryRecord, 'si_inventory', `si_inventory.id = si.inventory_id and si_inventory.variant_id = variant.id`)

			// get item if order from variant
			.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

			// get real item
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(si_inventory.id, od_inventory.id) and inventory.variant_id = variant.id')
			.andWhere('inventory.id is not null')

			// get product detail
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')

			// get order shipment
			.leftJoin(ShipmentOrderDetailRecord, 'sod', 'sod.order_detail_id = feedback.order_detail_id')
			.leftJoinAndMapOne('feedback.shipment', ShipmentRecord, 'shipment', 'shipment.id = sod.shipment_id and shipment.is_facade is false and shipment.is_return is false')

			// filter with no exchange
			.andWhere(sq => {
				return `not exists ` + sq.subQuery()
					.select('1')
					.from(ExchangeDetailRecord, 'exchangeDetail')
					.where('exchangeDetail.order_detail_id = orderDetail.id')
					.andWhere('exchangeDetail.inventory_id = inventory.id')
					.getSql()
			})

			// filter that has no exchange shipment
			.andWhere(`(shipment.confirmed_at + '7 days'::interval) ${!!filter.expired ? '<' : '>'} now()`)

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserExchange<
		D extends boolean,
		WRI extends boolean,
		Return extends ExchangeInterface & {
			return_shipment: ShipmentInterface,
			details: Array<ExchangeDetailInterface & {
				order: OrderInterface,
				variant: VariantInterface & {
					product: ProductInterface,
					brand: BrandInterface,
					asset: VariantAssetInterface,
					size?: SizeInterface,
					colors?: ColorInterface[],
				},
				replacement_shipment?: ShipmentInterface & (
					WRI extends true ? {
						variant: VariantInterface & {
							product: ProductInterface,
							brand: BrandInterface,
							asset: VariantAssetInterface,
							size?: SizeInterface,
							colors?: ColorInterface[],
						},
					} : {}
				),
			}>,
		}>(
		user_id: number,
		option: {
			detailed?: D,
			with_replacement_items?: WRI,
		} = {},
		filter: {
			exchange_id?: number,
			offset?: number,
			limit?: number,
			history?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<D extends true ? Return : Return[]> {
		let Q =  this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndMapMany('exchange.details', ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
			.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = detail.id')
			.leftJoinAndMapOne('exchange.return_shipment', ShipmentRecord, 'return_shipment', 'return_shipment.id = sed.shipment_id and return_shipment.is_return is true')
			.where('exchange.user_id = :user_id', {user_id})

			// get replacement shipments
			.leftJoinAndMapOne('detail.replacement_shipment', ShipmentRecord, 'replacement_shipment', 'replacement_shipment.id = sed.shipment_id and replacement_shipment.is_return is false')

			// get the items
			.leftJoin(InventoryRecord, 'inventory', `inventory.id = detail.inventory_id`)
			.leftJoinAndMapOne('detail.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')

			if (option.detailed) {
				// get order
				Q = Q.leftJoin(OrderDetailRecord, 'orderDetail', 'orderDetail.id = detail.order_detail_id')
				.leftJoinAndMapOne('detail.order', OrderRecord, 'order', 'order.id = orderDetail.order_id')

				if (option.with_replacement_items) {
					Q.leftJoin(InventoryRecord, 'replacement_item', 'replacement_item.id = detail.replacement_inventory_id')
					.leftJoinAndMapOne('replacement_shipment.variant', VariantRecord, 'rivariant', 'rivariant.id = replacement_item.variant_id')
					.leftJoinAndMapOne('rivariant.product', ProductRecord, 'riproduct', 'riproduct.id = rivariant.product_id')
					.leftJoinAndMapOne('rivariant.brand', BrandRecord, 'ribrand', 'ribrand.id = riproduct.brand_id')
					.leftJoinAndMapOne('rivariant.size', SizeRecord, 'risize', 'risize.id = rivariant.size_id')
						.leftJoin(query => {
							return query.from(VariantColorRecord, 'rivariant_color')
								.orderBy('rivariant_color.order', 'ASC')
						}, 'rivc', 'rivc.variant_id = rivariant.id')
						.leftJoinAndMapMany('rivariant.colors', ColorRecord, 'ricolor', 'ricolor.id = rivc.color_id')
					.leftJoin(query => {
						return query.from(VariantAssetRecord, 'ri_variant_asset')
							.orderBy('ri_variant_asset.order', 'ASC')
							.addOrderBy('ri_variant_asset.id', 'ASC')
					}, 'ri_va', 'ri_va.variant_id = rivariant.id AND ri_va.deleted_at IS NULL')
					.leftJoinAndMapOne('rivariant.asset', VariantAssetRecord, 'rivasset', 'rivasset.id = ri_va.id')
				}

				return Q.andWhere('exchange.id = :exchange_id', {exchange_id: filter.exchange_id})
						.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
						.leftJoin(query => {
							return query.from(VariantColorRecord, 'variant_color')
								.orderBy('variant_color.order', 'ASC')
						}, 'vc', 'vc.variant_id = variant.id')
						.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
						.getOne()
						.then(this.parser) as any
			} else {
				if (!!filter.history) {
					Q = Q.andWhere(`exchange.status != 'PENDING'`)
				} else {
					Q = Q.andWhere(`exchange.status = 'PENDING'`)
				}

				return Q
					.skip(filter.offset)
					.take(filter.limit)
					.getMany()
					.then(this.parser) as any
			}
	}

	@RepositoryModel.bound
	async getOrderDetailsForRefundValue(
		order_detail_ids: number[],
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		variants: Array<VariantInterface & {
			inventory: InventoryInterface,
			quantity: number,
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		facade_shipment: ShipmentInterface,
	}>> {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.where('orderDetail.id in (:...order_detail_ids)', {order_detail_ids})

			// get discount
			.leftJoinAndMapMany('orderDetail.orderDetailCampaigns', OrderDetailCampaignRecord, 'campaign', 'campaign.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailCoupons', OrderDetailCampaignRecord, 'coupon', 'coupon.order_detail_id = orderDetail.id')

			// get shipment
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = orderDetail.id')
			.leftJoinAndMapOne('orderDetail.facade_shipment', ShipmentRecord, 'fshipment', 'fshipment.id = sOd.shipment_id and fshipment.is_facade is true and fshipment.is_return is false')

			// get stylesheet if order from matchbox
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\' and si.deleted_at is null')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')

			// get item if order from variant
			.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

			// get variant
			.leftJoinAndMapMany('orderDetail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_inventory.variant_id, si_inventory.variant_id)')
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(od_inventory.id, si_inventory.id)')
			.loadRelationCountAndMap('variant.quantity', 'variant.inventories', 'inventory', query => {
				return query.where(`inventory.deleted_at IS NULL AND inventory.status = 'AVAILABLE'`)
			})

			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getExchangeDetailFromReplacementShipment(
		shipment_id: number,
		transaction: EntityManager,
	): Promise<ExchangeDetailInterface[]> {
		return this.query('app.exchange_detail', 'exchangeDetail', transaction)
			.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = exchangeDetail.id')
			.leftJoin(ShipmentRecord, 'shipment', 'shipment.id = sed.shipment_id')
			.where('shipment.id = :shipment_id', {shipment_id})
			.andWhere('shipment.is_facade is false')
			.andWhere('shipment.is_return is false')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderDetailShipmentFromExchangeDetail(
		exchange_detail_ids: number[],
		transaction: EntityManager,
	): Promise<Array<ExchangeDetailInterface & {
		address: ShipmentAddressInterface,
		packet: PacketInterface,
	}>> {
		return this.query('app.exchange_detail', 'exchange_detail', transaction)
			.leftJoin(ExchangeRecord, 'exchange', 'exchange.id = exchange_detail.exchange_id')
			.leftJoin(OrderDetailRecord, 'order_detail', 'order_detail.id = exchange.order_detail_id')
			.leftJoin(ShipmentOrderDetailRecord, 'sod', 'sod.order_detail_id = order_detail.id')
			.leftJoin(ShipmentRecord, 'shipment', 'shipment.id = sod.shipment_id')
			.leftJoinAndMapOne('exchange_detail.address', ShipmentAddressRecord, 'address', 'address.id = shipment.destination_id')
			.leftJoinAndMapOne('exchange_detail.packet', PacketRecord, 'packet', 'packet.id = shipment.packet_id')
			.where('exchange_detail.id in (:...exchange_detail_ids)', {exchange_detail_ids})
			.andWhere('shipment.is_facade is false')
			.andWhere('shipment.is_return is false')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStatus(status: CODES, transaction: EntityManager): Promise<ExchangeInterface[]> {
		switch (status) {
		case CODES.SUCCESS:
			return this.getMany('ExchangeRecord', {status: CODES.SUCCESS}, { transaction })
		case CODES.FAILED:
			return this.getMany('ExchangeRecord', {status: CODES.FAILED}, { transaction })
		case CODES.EXCEPTION:
			return this.getMany('ExchangeRecord', {status: CODES.EXCEPTION}, { transaction })
		default:
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_103)
		}
	}

	@RepositoryModel.bound
	async getByOrderDetailId(order_detail_id: number, transaction: EntityManager): Promise<ExchangeInterface> {
		return this.getOne('ExchangeRecord', {order_detail_id}, { transaction }) as any
	}

	@RepositoryModel.bound
	async getByReplacement(
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<ExchangeInterface> {
		return this.getOne('ExchangeRecord', {
			replacement_id: order_detail_id,
		}, {
			transaction,
		}) as any
	}

	@RepositoryModel.bound
	async getDeepWithoutDetails(exchange_ids: number[], transaction: EntityManager): Promise<Array<ExchangeInterface & {
		orderDetail: OrderDetailInterface & {
			order: OrderInterface & {
				user: UserInterface & {
					profile: UserProfileInterface,
				},
			},
			stylesheet?: StylesheetInterface & {
				stylist: UserInterface & {
					profile: UserProfileInterface,
				},
				asset: MatchboxAssetInterface,
			},
		},
		replacement?: OrderDetailInterface & {
			stylesheet?: StylesheetInterface & {
				stylist: UserInterface & {
					profile: UserProfileInterface,
				},
				asset: MatchboxAssetInterface,
			},
		},
		item_replacement?: InventoryInterface & {
			variant: VariantInterface & {
				product: ProductInterface,
				brand: BrandInterface,
				asset: VariantAssetInterface,
				size: SizeInterface,
				colors: ColorInterface[],
			},
		}
		return_shipment?: ShipmentInterface,
		replacement_shipment?: ShipmentInterface,
	}>> {
		return this.query('app.exchange', 'exchange', transaction)
			.leftJoin(ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
			.leftJoin(ShipmentExchangeDetailRecord, 'sed', 'sed.exchange_detail_id = detail.id')

			.leftJoinAndMapOne('exchange.orderDetail', OrderDetailRecord, 'orderDetail', 'orderDetail.id = detail.order_detail_id')
			.leftJoinAndSelect('orderDetail.order', 'order')
			.leftJoinAndSelect('order.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')

			.leftJoinAndMapOne('orderDetail.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = orderDetail.ref_id AND orderDetail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('stylesheet.stylist', 'stylist')
			.leftJoinAndSelect('stylist.profile', 'stylistProfile')
			.leftJoinAndMapOne('stylesheet.asset', MatchboxAssetRecord, 'matchboxAsset', 'matchboxAsset.matchbox_id = stylesheet.matchbox_id')

			// deprecated query, remove after transition
			.leftJoinAndMapOne('exchange.replacement', OrderDetailRecord, 'replacement', 'replacement.id = detail.replacement_id')
			.leftJoinAndMapOne('replacement.stylesheet', StylesheetRecord, 'replacementStylesheet', 'replacementStylesheet.id = replacement.ref_id AND replacement.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('replacementStylesheet.stylist', 'replacementStylist')
			.leftJoinAndSelect('replacementStylist.profile', 'replacementStylistProfile')
			.leftJoinAndMapOne('replacementStylesheet.asset', MatchboxAssetRecord, 'repMatchboxAsset', 'repMatchboxAsset.matchbox_id = replacementStylesheet.matchbox_id')

			// get replacement inventory
			.leftJoinAndMapOne('exchange.item_replacement', InventoryRecord, 'item_replacement', 'item_replacement.id = detail.replacement_inventory_id')
			.leftJoinAndMapOne('item_replacement.variant', VariantRecord, 'variant', 'variant.id = item_replacement.variant_id')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
						.leftJoin(query => {
							return query.from(VariantColorRecord, 'variant_color')
								.orderBy('variant_color.order', 'ASC')
						}, 'vc', 'vc.variant_id = variant.id')
						.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')

			.leftJoinAndMapOne('exchange.return_shipment', ShipmentRecord, 'return_shipment', 'return_shipment.id = sed.shipment_id and return_shipment.is_return is true')
			.leftJoinAndMapOne('exchange.replacement_shipment', ShipmentRecord, 'replacement_shipment', 'replacement_shipment.id = sed.shipment_id and replacement_shipment.is_return is false')

			.where('exchange.deleted_at IS NULL')
			.andWhere('exchange.id IN (:...ids)', { ids: exchange_ids })
			.orderBy('return_shipment.created_at', 'DESC')
			.addOrderBy('matchboxAsset.order', 'ASC')
			.addOrderBy('repMatchboxAsset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailsWithFeedback(exchange_id: number, transaction: EntityManager): Promise<ExchangeInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
			addresses: UserAddressInterface[],
		}
		details: Array<ExchangeDetailInterface & {
			variant: VariantInterface & {
				product: ProductInterface,
				brand: BrandInterface,
				size: SizeInterface,
				colors: ColorInterface[],
				asset: VariantAssetInterface,
			},
			feedback: FeedbackInterface & {
				answers: Array<FeedbackAnswerInterface & {
					question: QuestionInterface,
				}>,
			},
		}>,
	}> {
		return this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndMapMany('exchange.details', ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')
			.where('exchange.id = :exchange_id', {exchange_id})

			// get item
			.leftJoin(InventoryRecord, 'inventory', `inventory.id = detail.inventory_id`)
			.leftJoinAndMapOne('detail.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')

			// get feedback
			.leftJoinAndMapOne('detail.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = detail.order_detail_id and feedback.ref_id = variant.id and feedback.type = 'VARIANT'`)
			.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')

			// get user
			.leftJoinAndMapOne('exchange.user', UserRecord, 'user', 'user.id = exchange.user_id')
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
			.leftJoinAndMapMany('user.addresses', UserAddressRecord, 'address', 'address.user_id = user.id')

			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetails(exchange_ids: number[], transaction: EntityManager): Promise<Array<ExchangeInterface & {
		details: ExchangeDetailInterface[],
		orderDetail?: OrderDetailInterface,
		shipment?: ShipmentInterface,
		// replacement?: OrderDetailInterface & {
		// 	orderDetailShipment?: Array<ShipmentOrderDetailInterface & {
		// 		shipment: ShipmentInterface,
		// 	}>,
		// },
	}>> {
		return this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndSelect('exchange.details', 'details')
			.leftJoinAndSelect('exchange.orderDetail', 'orderDetail')
			.leftJoin('orderDetail.orderDetailShipments', 'orderDetailShipments')
			.leftJoinAndMapOne('exchange.shipment', ShipmentRecord, 'shipment', 'shipment.id = orderDetailShipments.shipment_id AND shipment.is_facade = :is_facade AND shipment.is_return = :is_return', { is_facade: false, is_return: true })
			.where('exchange.deleted_at IS NULL')
			.andWhere('exchange.id IN (:...ids)', { ids: exchange_ids })
			.orderBy('shipment.created_at', 'DESC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailWithOrder(exchange_ids: number[], transaction: EntityManager): Promise<Array<ExchangeInterface & {
		details: ExchangeDetailInterface[],
		orderDetail: OrderDetailInterface,
		order: OrderInterface,
	}>> {
		return this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndSelect('exchange.details', 'details')
			.leftJoinAndSelect('exchange.orderDetail', 'orderDetail')
			.leftJoinAndMapOne('exchange.order', OrderRecord, 'order', 'order.id = orderDetail.order_id')
			.where('exchange.deleted_at IS NULL')
			.andWhere('exchange.id IN (:...ids)', { ids: exchange_ids })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getHistory(user_id: number, exchange_ids: number[], transaction: EntityManager): Promise<Array<ExchangeInterface & {
		histories: HistoryExchangeInterface[],
		details: Array<ExchangeDetailInterface & {
			histories: HistoryExchangeDetailInterface[],
			inventory: InventoryInterface,
			variant: VariantInterface,
			product: ProductInterface,
			brand: BrandInterface,
			colors: ColorInterface[],
			size: SizeInterface,
		}>,
	}>> {
		return this.query('app.exchange', 'exchange', transaction)
			.innerJoin('exchange.orderDetail', 'oD')
			.innerJoin('oD.order', 'order', 'order.user_id = :user_id', { user_id })
			.leftJoinAndSelect('exchange.details', 'detail')
			.leftJoinAndMapMany('exchange.histories', HistoryExchangeRecord, 'histories', 'histories.exchange_id = exchange.id AND histories.status IS NOT NULL AND histories.note IS NOT NULL')
			.leftJoinAndMapMany('detail.histories', HistoryExchangeDetailRecord, 'detailHistories', 'detailHistories.exchange_detail_id = detail.id AND detailHistories.status IS NOT NULL AND detailHistories.note IS NOT NULL')
			.leftJoinAndMapOne('detail.stylesheetInventory', StylesheetInventoryRecord, 'stylesheetInventory', 'stylesheetInventory.id = detail.ref_id AND detail.type = :stylesheet_inventory_type', { stylesheet_inventory_type: EXCHANGES.STYLESHEET_INVENTORY })
			.leftJoinAndMapOne('detail.inventory', InventoryRecord, 'inventory', 'inventory.id = stylesheetInventory.inventory_id OR (inventory.id = detail.ref_id AND detail.type = :inventory_type)', { inventory_type: EXCHANGES.INVENTORY })
			.leftJoinAndMapOne('detail.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoinAndMapOne('detail.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('detail.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('detail.colors', ColorRecord, 'colors', 'colors.id = variantColors.color_id')
			.leftJoinAndMapOne('detail.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.where('exchange.id IN (:...ids)', { ids: exchange_ids })
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async remove(id: number, transaction: EntityManager) {
		return this.renew('ExchangeRecord', id, {
			deleted_at: new Date(),
		}, {
			transaction,
		})
	}
}
