import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	ExchangeDetailRecord,
	InventoryRecord,
	LocationRecord,
	OrderDetailCampaignRecord,
	OrderDetailCouponRecord,
	OrderDetailRecord,
	OrderRecord,
	OrderRequestRecord,
	ProductRecord,
	ShipmentOrderDetailRecord,
	ShipmentRecord,
	StyleboardUserAssetRecord,
	StylesheetInventoryRecord,
	UserAnswerRecord,
	UserAssetRecord,
	UserProfileRecord,
	UserRecord,
	VariantAnswerRecord,
	VariantRecord,
} from 'energie/app/records'

import {
	ExchangeDetailInterface,
	ExchangeInterface,
	InventoryInterface,
	OrderInterface,
	ShipmentExchangeDetailInterface,
	ShipmentInterface,
	ProductInterface,
	StyleboardInterface,
	StyleboardUserAssetInterface,
	StylesheetInterface,
	StylesheetInventoryInterface,
	UserAnswerInterface,
	UserAssetInterface,
	UserInterface,
	UserProfileInterface,
	VariantInterface,
	OrderDetailInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	AreaManualInterface,
	LocationInterface,
	FeedbackAnswerInterface,
	StylecardInterface,
	CategoryInterface,
} from 'energie/app/interfaces'
import { CODES, EXCHANGES, ORDERS, USER_REQUESTS } from 'energie/utils/constants/enum'
import { CommonHelper } from 'utils/helpers'

@EntityRepository()
export default class MigrationRepository extends RepositoryModel<{
	UserAnswerRecord: UserAnswerRecord,
	VariantAnswerRecord: VariantAnswerRecord,
}> {

	static __displayName = 'MigrationRepository'

	protected records = {
		UserAnswerRecord,
		VariantAnswerRecord,
	}

	@RepositoryModel.bound
	async reset(tableName: string, transaction: EntityManager) {
		await this.queryCustom(`delete from app.${tableName}`, undefined, transaction)
			.then(async () => {
				await this.queryCustom(
					`SELECT setval('app.${tableName}_id_seq', (case when (SELECT MAX(id) FROM "app"."${tableName}") is null then 1 else (SELECT MAX(id) FROM "app"."${tableName}") end), TRUE)`
				, undefined, transaction)
			})
	}


	@RepositoryModel.bound
	async fixSequence(tableName: string | string[], transaction: EntityManager) {
		if(Array.isArray(tableName)) {
			await tableName.reduce((p, t) => {
				return p.then(async () => {
					await this.queryCustom(
						`SELECT setval('app.${t}_id_seq', (case when (SELECT MAX(id) FROM "app"."${t}") is null then 1 else (SELECT MAX(id) FROM "app"."${t}") end), TRUE)`
					, undefined, transaction)
				})
			}, Promise.resolve([]))
		} else {
			await this.queryCustom(
				`SELECT setval('app.${tableName}_id_seq', (case when (SELECT MAX(id) FROM "app"."${tableName}") is null then 1 else (SELECT MAX(id) FROM "app"."${tableName}") end), TRUE)`
			, undefined, transaction)
		}
	}

	@RepositoryModel.bound
	async insertUserRequest(
		datas: Array<{
			user_id: number,
			outfit: string,
			attire: string,
			product: string,
			styleboard_id: number,
			asset_ids: number[],
		}>,
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.user_request', datas.map(data => {
			return {
				user_id: data.user_id,
				type: USER_REQUESTS.STYLING,
				status: CODES.SUCCESS,
				metadata: {
					type: 'outfit',
					...CommonHelper.allowKey(data, 'outfit', 'attire', 'product', 'styleboard_id', 'asset_ids'),
				},
			}
		}), ['id'], transaction)
	}

	@RepositoryModel.bound
	async updateOrderStatus(
		orders: Array<Pick<OrderInterface, 'id' | 'user_id' | 'number' | 'status'>>,
		transaction: EntityManager,
	) {
		return Promise.all(orders.map(order => {
			this.log(`Update app.order id ${order.id}`)

			return this.queryUpdate('app.order', transaction)
				.set({
					status: ORDERS.RESOLVED,
				})
				.where('id = :id', {id: order.id})
				.execute()
		}))
	}

	@RepositoryModel.bound
	async upsertUserAnswer(
		datas: UserAnswerInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.user_answer', datas, undefined, transaction)
	}

	@RepositoryModel.bound
	async updateUserProfile(
		datas: UserProfileInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.user_profile', datas, undefined, transaction)
	}

	@RepositoryModel.bound
	async upsertVariantPrices(
		variants: VariantInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.variant', variants, undefined, transaction)
	}

	@RepositoryModel.bound
	async upsertExchange(
		exchanges: ExchangeInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.exchange', exchanges, undefined, transaction)
	}

	@RepositoryModel.bound
	async upsertExchangeDetails(
		exchangeDetails: ExchangeDetailInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.exchange_detail', exchangeDetails, undefined, transaction)
	}

	@RepositoryModel.bound
	async upsertShipmentExchangeDetails(
		shipmentExchangeDetails: ShipmentExchangeDetailInterface[],
		transaction: EntityManager,
	) {
		return this.insertOrUpdate('app.shipment_exchange_detail', shipmentExchangeDetails, ['shipment_id', 'exchange_detail_id'], transaction)
	}

	async updateUserStylist(
		profile_id: number,
		stylist_id: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.user_profile', transaction)
			.set({
				stylist_id,
			})
			.where('id = :profile_id', {profile_id})
			.execute()
			.then(result => result.raw)
	}

	async batchUpdateVariantPrices(
		product_id: number,
		price: number,
		retail_price: number,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.variant', transaction)
			.set({
				price,
				retail_price,
			})
			.where('product_id = :product_id', {product_id})
			.execute()
			.then(result => result.raw)
	}

	// async migrateVariantAsset(
	// 	variantAsset: {
	// 		image_id: string,
	// 		variant_id: string,
	// 		order: string,
	// 		index: string,
	// 	},
	// 	transaction: EntityManager,
	// ) {
	// 	return this.queryUpdate('app.variant_asset', transaction)
	// 		.set({
	// 			order: variantAsset.order,
	// 			index: variantAsset.index,
	// 		})
	// 		.where('id = :image_id', {image_id: variantAsset.image_id})
	// 		.execute()
	// 		.then(result => {
	// 			this.log('done' + variantAsset.image_id)
	// 			result.raw
	// 		  }
	// 		)
	// }

	async migrateVariantAsset(
		variantAssetData: string,
		transaction: EntityManager,
	) {
		return this.queryCustom(
			`INSERT INTO app.variant_asset (id, variant_id, "order", index, url, type)
			 VALUES ${variantAssetData}
			 ON CONFLICT (id)
			 DO
			 	UPDATE
					SET "order" = EXCLUDED.order,
						index = EXCLUDED.index;`,
			undefined, transaction,
		)
	}

	async batchUpdateStylecard(stylecards: StylecardInterface[], transaction: EntityManager) {
		return this.queryUpdate('app.stylecard', transaction)
			.set({
				deleted_at: new Date(),
				is_public: false,
			})
			.where('id in (:...ids)', {ids: stylecards.map(stylecard => stylecard.id)})
			.execute()
			.then(result => result.raw)
	}

	@RepositoryModel.bound
	async batchUpdateProductCategory(product_id: number, category_id: number, transaction: EntityManager) {
		return this.queryUpdate('app.product', transaction)
			.set({
				category_id,
			})
			.where('id = :product_id', { product_id })
			.execute()
			.then(result => result.raw)
	}

	@RepositoryModel.bound
	async batchUpdateProduct(products, transaction: EntityManager) {
		return this.queryUpdate('app.product', transaction)
			.set({
				deleted_at: new Date(),
			})
			.where('id in (:...ids)', { ids: products })
			.execute()
			.then(result => result.raw)
	}

	@RepositoryModel.bound
	async batchUpdateVariant(variants, transaction: EntityManager) {
		return this.queryUpdate('app.variant', transaction)
			.set({
				is_available: false,
			})
			.where('id in (:...ids)', {ids: variants[0].map(v => v.id)})
			.execute()
			.then(result => result.raw)
	}
	
	@RepositoryModel.bound
	async revertDeletedStylistProduct(products, transaction: EntityManager) {
		return this.queryUpdate('app.product', transaction)
			.set({
				deleted_at: null,
			})
			.where('id in (:...ids)', {ids: products[0].map(p => p.product_id)})
			.execute()
			.then(result => result.raw)
	}
	// ============================= UPSERT =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getCategoryByTitle(title: string, transaction: EntityManager): Promise<CategoryInterface> {
		return this.query('master.category', 'category', transaction)
			.where('lower(category.title) = :title', {title})
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylistProduct(transaction: EntityManager) {
		return this.query('history.product', 'historyProduct', transaction)
		.where('historyProduct.note = :note', { note: 'New Product Created'})
		.leftJoinAndSelect(ProductRecord, 'product', 'product.id = historyProduct.product_id')
		.andWhere('product.deleted_at is NULL')
		.leftJoinAndSelect(UserRecord, 'user', 'user.id = historyProduct.changer_user_id')
		.leftJoinAndSelect('user.roles', 'roles')
		.andWhere('roles.role = :role', { role: 'stylist'})
		.andWhere('"historyProduct"."created_at"::DATE-NOW()::DATE <= -14')
		.andWhere('historyProduct.created_at > :date', { date: new Date(2021, 7, 1)})
		.getManyAndCount()
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDeletedStylistProduct(transaction: EntityManager) {
		return this.query('history.product', 'historyProduct', transaction)
		.where('historyProduct.note = :note', { note: 'New Product Created'})
		.leftJoinAndSelect(ProductRecord, 'product', 'product.id = historyProduct.product_id')
		.andWhere('product.deleted_at is NOT NULL')
		.leftJoinAndSelect(UserRecord, 'user', 'user.id = historyProduct.changer_user_id')
		.leftJoinAndSelect('user.roles', 'roles')
		.andWhere('roles.role = :role', { role: 'stylist'})
		.andWhere('"historyProduct"."created_at"::DATE-NOW()::DATE <= -14')
		.andWhere('historyProduct.created_at > :date', { date: new Date(2021, 7, 30)})
		.getManyAndCount()
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylistVariant(
		product_ids: number[],
		transaction: EntityManager,
	) {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.product_id in (:...product_ids)', { product_ids })
			.andWhere('variant.is_available is true')
			.getManyAndCount()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAllPrestyled(transaction: EntityManager): Promise<StylecardInterface[]> {
		return this.query('app.stylecard', 'stylecard', transaction)
		.where('stylecard.is_public is true')
		.andWhere('stylecard.is_recomendation is false')
		.andWhere('stylecard.id not in (17547, 17546, 17548, 17549, 17550)')
		.getMany()
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylistClient(stylist_id: number, transaction: EntityManager): Promise<UserInterface[]> {
		return this.query('app.user', 'user', transaction)
			.leftJoin(UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
			.where('profile.stylist_id = :stylist_id', {stylist_id})
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUnavailableInventory(transaction: EntityManager): Promise<VariantInterface[]> {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.is_available is true')
			.andWhere('variant.deleted_at is null')
			.andWhere('variant.have_consignment_stock is false')
			.andWhere(query => {
				return `NOT EXISTS ${query.subQuery()
					.select('1')
					.from('app.inventory', 'inventory')
					.where('inventory.deleted_at is null')
					.andWhere(`inventory.status = 'AVAILABLE'` )
					.andWhere('inventory.variant_id = variant.id')
					.getSql()
				}`
			})
			.groupBy('variant.id')
			.orderBy('variant.id', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStylingStyleboard(transaction: EntityManager): Promise<StyleboardInterface[]> {
		return this.query('app.styleboard', 'styleboard', transaction)
			.where(`styleboard.status IN ('PENDING','STYLING')`)
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getThreeStarFeedback(transaction: EntityManager): Promise<FeedbackAnswerInterface[]> {
		const Q = this.query('app.feedback_answer', 'feedback_answer', transaction)
		.where('feedback_answer.question_id in (64, 68)')

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async users(ids: number[], transaction: EntityManager): Promise<Array<UserInterface & {
		profile: UserProfileInterface,
		stylist: UserInterface & {
			profile: UserProfileInterface,
		},
		image?: UserAssetInterface,
	}>> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
			.leftJoinAndMapOne('user.stylist', UserRecord, 'stylist', 'stylist.id = profile.stylist_id')
			.leftJoinAndMapOne('stylist.profile', UserProfileRecord, 'sprofile', 'sprofile.id = stylist.user_profile_id')
			.leftJoinAndMapOne('user.image', UserAssetRecord, 'image', 'image.id = profile.cover_id')
			.where('user.id in (:...ids)', {ids})
			.getMany()
			.then(this.parser) as any
	}

	async getExchange(transaction: EntityManager): Promise<Array<{
		id: number,
		created_at: Date,
		deleted_at: Date,
		order_detail_id: number,
		replacement_id: number,
		status: CODES,
		note: string,
		details: Array<{
			id: number,
			created_at: Date,
			updated_at: Date,
			exchange_id: number,
			type: EXCHANGES,
			ref_id: number,
			status: CODES,
			note: string,
			is_refund: boolean,
			stylesheetInventory?: StylesheetInventoryInterface,
			inventory?: InventoryInterface,
		}>,
		order: OrderInterface,
		shipment?: ShipmentInterface,
	}>> {
		return this.query('app.exchange', 'exchange', transaction)
			.leftJoinAndMapMany('exchange.details', ExchangeDetailRecord, 'detail', 'detail.exchange_id = exchange.id')

			// get order
			.leftJoin(OrderDetailRecord, 'orderDetail', 'orderDetail.id = exchange.order_detail_id')
			.leftJoinAndMapOne('exchange.order', OrderRecord, 'order', 'order.id = orderDetail.order_id')

			// get shipment
			.leftJoin(ShipmentOrderDetailRecord, 'sod', 'sod.order_detail_id = orderDetail.id')
			.leftJoinAndMapOne('exchange.shipment', ShipmentRecord, 'shipment', 'shipment.id = sod.shipment_id and shipment.is_return is true and shipment.is_facade is false')

			// get stylesheet inventory
			.leftJoinAndMapOne('detail.stylesheetInventory', StylesheetInventoryRecord, 'si', `si.id = detail.ref_id and detail.type = 'STYLESHEET_INVENTORY'`)

			// get inventory
			.leftJoinAndMapOne('detail.inventory', InventoryRecord, 'i', `i.id = detail.ref_id and detail.type = 'INVENTORY'`)

			// filter new exchange
			.where('exchange.order_detail_id is not null')

			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async variants(ids: number[], transaction: EntityManager): Promise<VariantInterface[]> {
		return this.query('app.variant', 'variant', transaction)
			.where('variant.id in (:...ids)', {ids})
			.orderBy('variant.id', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async product(
		ids: number[],
		option: {
			with_variant?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<Array<ProductInterface & {
		variants: VariantInterface[],
	}>> {
		const Q = this.query('app.product', 'product', transaction)

			if (option.with_variant) {
				Q.leftJoinAndMapMany('product.variants', VariantRecord, 'variant', 'variant.product_id = product.id')
			}

		return Q.where('product.id in (:...ids)', {ids})
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async userProfile(transaction: EntityManager): Promise<UserProfileInterface[]> {
		return this.query('app.user_profile', 'user_profile', transaction)
			.orderBy('user_profile.id')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async styleboard(transaction: EntityManager): Promise<Array<StyleboardInterface & {
		order: OrderInterface,
		assets: StyleboardUserAssetInterface[],
	}>> {
		return this.query('app.styleboard', 'styleboard', transaction)
			.innerJoin(OrderDetailRecord, 'orderDetail', `orderDetail.ref_id = styleboard.id and orderDetail.type = 'STYLEBOARD'`)
			.leftJoinAndMapOne('styleboard.order', OrderRecord, 'order', 'order.id = orderDetail.order_id')
			.leftJoinAndMapMany('styleboard.assets', StyleboardUserAssetRecord, 'asset', 'asset.styleboard_id = styleboard.id')
			// .where('styleboard.id >= 1950')
			.andWhere(`styleboard.status not in ('EXCEPTION')`)
			.orderBy('styleboard.user_id', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async userAnswer(transaction: EntityManager): Promise<UserAnswerInterface[]> {
		return this.query('app.user_answer', 'user_answer', transaction)
			.where(`(user_answer.answer ->> 'CHOICE')::text not ilike '[%'`)
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getActiveStylesheet(transaction: EntityManager): Promise<StylesheetInterface[]> {
		return this.query('app.stylesheet', 'stylesheet', transaction)
			.where(`stylesheet.created_at < '2020-10-2'`)
			.andWhere(`stylesheet.status in ('PENDING', 'STYLING')`)
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getForCountRefund(
		order_id: number,
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		variants: Array<VariantInterface & {
			inventory: InventoryInterface,
			quantity: number,
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		facade_shipment: ShipmentInterface,
	}>> {
		const Q = this.query('app.order_detail', 'orderDetail', transaction)
			.leftJoin(OrderRequestRecord, 'orderRequest', 'orderRequest.id = orderDetail.order_request_id')
			.where('orderDetail.order_id = :order_id', {order_id})

			// get stylesheet if order from matchbox
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\' and si.deleted_at is null')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')

			// get item if order from variant
			.leftJoin(VariantRecord, 'od_variant', `od_variant.id = orderRequest.ref_id and orderRequest.type = 'VARIANT'`)
			.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

			// get variant
			.leftJoinAndMapMany('orderDetail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_variant.id, si_inventory.variant_id)')
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(si_inventory.id, od_inventory.id)')
			.loadRelationCountAndMap('variant.quantity', 'variant.inventories', 'i', query => {
				return query.where(`i.deleted_at IS NULL AND i.status = 'AVAILABLE'`)
			})

			// get discount
			.leftJoinAndMapMany('orderDetail.orderDetailCampaigns', OrderDetailCampaignRecord, 'campaign', 'campaign.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailCoupons', OrderDetailCouponRecord, 'coupon', 'coupon.order_detail_id = orderDetail.id')

			// get shipment
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = orderDetail.id')
			.leftJoinAndMapOne('orderDetail.facade_shipment', ShipmentRecord, 'fshipment', 'fshipment.id = sOd.shipment_id and fshipment.is_facade is true and fshipment.is_return is false')

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRecentOngkir(transaction: EntityManager): Promise<Array<AreaManualInterface & {location: LocationInterface}>> {
		return this.query('area.manual', 'manual', transaction)
		.leftJoinAndMapOne('manual.location', LocationRecord, 'location', 'location.id = manual.location_id')
		.getMany()
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOldCategoryProduct(transaction: EntityManager) {
		const category_ids = [114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 126, 127]
		return this.query('app.product', 'p', transaction)
		.where('p.category_id IN (:...category_ids)', { category_ids })
		.getMany()
		.then(this.parser) as any
	}
}
