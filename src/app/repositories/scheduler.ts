import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import { Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'
import { VARIANT_STAT } from 'energie/utils/constants/enum'


import {
	PromotionSchedulerRecord,
	SchedulerPreOrderRecord,
} from 'energie/app/records'

import {
	SchedulerPreOrderInterface,
	PromotionSchedulerInterface,
} from 'energie/app/interfaces'

@EntityRepository()
export default class SchedulerRepository extends RepositoryModel<{
	PromotionSchedulerRecord: PromotionSchedulerRecord,
	SchedulerPreOrderRecord: SchedulerPreOrderRecord,
}> {

	static __displayName = 'SchedulerRepository'

	protected records = {
		PromotionSchedulerRecord,
		SchedulerPreOrderRecord,
	}

	// ============================= INSERT =============================

	@RepositoryModel.bound
	async insert(
		promotionScheduler: Parameter<PromotionSchedulerInterface>,
		transaction: EntityManager,
	): Promise<PromotionSchedulerInterface> {
		return this.save('PromotionSchedulerRecord', promotionScheduler, transaction)
	}

	// ============================= UPDATE =============================

	@RepositoryModel.bound
	async update(
		promotion_id: number,
		promotionScheduler: Parameter<PromotionSchedulerInterface>,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.renew('PromotionSchedulerRecord', 'promotion_id = :promotion_id', CommonHelper.stripUndefined(promotionScheduler), {
			parameters: {
				promotion_id,
			},
			transaction,
		})
	}

	// ============================= GETTER =============================
	//

	@RepositoryModel.bound
	async get(transaction: EntityManager) {
		return await this.queryCustom(`
		SELECT * FROM scheduler.promotion "t"
		WHERE TO_CHAR(t.start_date, 'yyyy-mm-dd HH24:MI') = TO_CHAR(NOW(), 'yyyy-mm-dd HH24:MI')
		OR TO_CHAR(t.end_date, 'yyyy-mm-dd HH24:MI') = TO_CHAR(NOW(), 'yyyy-mm-dd HH24:MI')
		AND t."status" = 'RUN'
		`, undefined, transaction)
			.then(result => {
				return result
			})
	}

	@RepositoryModel.bound
	async getPreOrder(transaction: EntityManager) {
		return await this.queryCustom(`
		WITH var (dates) as (
			values ('yyyy-mm-dd HH24:MI')
		 )
		 SELECT
		 t.*,
		 CASE
		 WHEN TO_CHAR(t.start_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 THEN 'START'
		 WHEN  TO_CHAR(t.end_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 THEN 'END'
		 WHEN  TO_CHAR(t.shipment_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 THEN 'SHIPMENT'
		 ELSE NULL
		 END AS "SCHEDULE_TYPE"
		 FROM scheduler.pre_order "t", var
		 WHERE TO_CHAR(t.start_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 OR TO_CHAR(t.end_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 OR TO_CHAR(t.shipment_date, var.dates) = TO_CHAR(NOW() AT TIME ZONE 'Asia/Jakarta', var.dates)
		 AND t."status" = 'RUN'
		`, undefined, transaction)
			.then(result => {
				return result
			})
	}
	//
	// ============================= INSERT =============================


	@RepositoryModel.bound
	async insertPreOrder(order_status: VARIANT_STAT, data: Parameter<SchedulerPreOrderInterface>, transaction: EntityManager) {
		// this.save('SchedulerPreOrderRecord', data, transaction)
		await this.query('scheduler.pre_order', 'po', transaction)
			.where('variant_id = :variant_id', { variant_id: data.variant_id })
			.getOne()
			.then((result: SchedulerPreOrderInterface) => {
				if (!result) {
					if (order_status === VARIANT_STAT.PO_FIXED || order_status === VARIANT_STAT.PO_INTERVAL) {
						this.save('SchedulerPreOrderRecord', data, transaction)
					}
				} else {
					this.renew('SchedulerPreOrderRecord', result.id, data, {
						transaction,
					})
				}
			})

	}


	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
