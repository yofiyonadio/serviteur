import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, IsNull } from 'typeorm'

import {
	ColorRecord,
	HistoryOrderDetailRecord,
	OrderDetailRecord,
	OrderRequestRecord,
	OrderDetailCampaignRecord,
	OrderDetailCouponRecord,
	OrderDetailTransactionRecord,
	StylesheetRecord,
	ShipmentRecord,
	HistoryShipmentRecord,
	OrderAddressRecord,
	InventoryRecord,
	VoucherRecord,
	MatchboxRecord,
	ServiceRecord,
	CategoryRecord,
	FeedbackRecord,
	OrderPaymentRecord,
	VariantRecord,
	VariantAssetRecord,
	UserAssetRecord,
	UserProfileRecord,
	StyleboardRecord,
	UserWalletTransactionRecord,
	TopupRecord,
	StylesheetInventoryRecord,
	ProductRecord,
	BrandRecord,
	SizeRecord,
	CampaignRecord,
	CouponRecord,
	ShipmentOrderDetailRecord,
} from 'energie/app/records'

import {
	ShipmentAddressInterface,
	OrderRequestInterface,
	OrderInterface,
	OrderDetailInterface,
	UserInterface,
	UserProfileInterface,
	ShipmentOrderDetailInterface,
	ShipmentInterface,
	FeedbackInterface,
	StylesheetInterface,
	MatchboxInterface,
	PacketInterface,
	HistoryShipmentInterface,
	OrderAddressInterface,
	VoucherInterface,
	HistoryOrderDetailInterface,
	OrderPaymentInterface,
	StylesheetInventoryInterface,
	InventoryInterface,
	StylesheetUserAssetInterface,
	CategoryInterface,
	ServiceInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	VariantInterface,
	VariantAssetInterface,
	ProductInterface,
	BrandInterface,
	ColorInterface,
	SizeInterface,
	FeedbackAnswerInterface,
	QuestionInterface,
	StyleboardInterface,
	UserAssetInterface,
	UserWalletTransactionInterface,
	TopupInterface,
	OrderDetailTransactionInterface,
	ShipmentCampaignInterface,
	ShipmentCouponInterface,
	CampaignInterface,
	CouponInterface,
} from 'energie/app/interfaces'

import { CommonHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

import { ORDER_DETAILS, FEEDBACKS, ORDER_DETAIL_STATUSES, SHIPMENT_STATUSES, ORDERS, PAYMENTS, VOUCHERS, ORDER_REQUESTS, STATEMENT_SOURCES } from 'energie/utils/constants/enum'

import { uniqBy } from 'lodash'


@EntityRepository()
export default class OrderDetailRepository extends RepositoryModel<{
	OrderDetailRecord: OrderDetailRecord,
	OrderDetailCampaignRecord: OrderDetailCampaignRecord,
	OrderDetailCouponRecord: OrderDetailCouponRecord,
	OrderDetailTransactionRecord: OrderDetailTransactionRecord,
	HistoryOrderDetailRecord: HistoryOrderDetailRecord,
}> {

	static __displayName = 'OrderDetailRepository'

	protected records = {
		OrderDetailRecord,
		OrderDetailCampaignRecord,
		OrderDetailCouponRecord,
		OrderDetailTransactionRecord,
		HistoryOrderDetailRecord,
	}

	// ============================= INSERT =============================
	insert(detail: Parameter<OrderDetailInterface>, transaction: EntityManager): Promise<OrderDetailInterface>
	insert(details: Array<Parameter<OrderDetailInterface>>, transaction: EntityManager): Promise<OrderDetailInterface[]>

	@RepositoryModel.bound
	async insert(
		detail: Parameter<OrderDetailInterface> | Array<Parameter<OrderDetailInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('OrderDetailRecord', detail as any, transaction)
	}

	insertCampaign(campaign: Parameter<OrderDetailCampaignInterface>, transaction: EntityManager): Promise<OrderDetailCampaignInterface>
	insertCampaign(campaigns: Array<Parameter<OrderDetailCampaignInterface>>, transaction: EntityManager): Promise<OrderDetailCampaignInterface[]>

	@RepositoryModel.bound
	async insertCampaign(
		campaign: Parameter<OrderDetailCampaignInterface> | Array<Parameter<OrderDetailCampaignInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('OrderDetailCampaignRecord', campaign as any, transaction)
	}

	insertCoupon(coupon: Parameter<OrderDetailCouponInterface>, transaction: EntityManager): Promise<OrderDetailCouponInterface>
	insertCoupon(coupons: Array<Parameter<OrderDetailCouponInterface>>, transaction: EntityManager): Promise<OrderDetailCouponInterface[]>

	@RepositoryModel.bound
	async insertCoupon(
		coupon: Parameter<OrderDetailCouponInterface> | Array<Parameter<OrderDetailCouponInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('OrderDetailCouponRecord', coupon as any, transaction)
	}

	@RepositoryModel.bound
	async insertTransaction(transaction_id: number, order_detail_id: number, amount: number, note: string | undefined, transaction: EntityManager): Promise<OrderDetailTransactionInterface> {
		return this.save('OrderDetailTransactionRecord', {
			order_detail_id,
			user_wallet_transaction_id: transaction_id,
			amount,
			note,
		}, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		order_detail_id: number,
		update: Partial<Parameter<OrderDetailInterface, 'packet_id'>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('OrderDetailRecord', order_detail_id, CommonHelper.stripKey(update as OrderDetailInterface, 'packet_id'), {
			transaction,
		}).then(isUpdated => {
			if (isUpdated) {
				this.save('HistoryOrderDetailRecord', {
					changer_user_id,
					order_detail_id,
					status: update.status,
					ref_id: update.ref_id,
					note,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updatePacketId(voucher_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<VoucherInterface>('app.voucher', transaction)
			.set({ packet_id })
			.where(Array.isArray(voucher_id_or_ids) ? 'id IN (:...voucher_id_or_ids)' : 'id = :voucher_id_or_ids', { voucher_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	@RepositoryModel.bound
	async systemUpdate(
		order_detail_id: number,
		update: Partial<Parameter<OrderDetailInterface, 'packet_id'>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('OrderDetailRecord', order_detail_id, CommonHelper.stripKey(update as OrderDetailInterface, 'packet_id'), {
			transaction,
		}).then(isUpdated => {
			if (isUpdated) {
				this.save('HistoryOrderDetailRecord', {
					order_detail_id,
					status: update.status,
					ref_id: update.ref_id,
					note: `Automated System Update.${note ? `\n${note}` : ''}`,
				})
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	async getByIds(order_detail_ids: number[], deep: boolean | undefined, transaction: EntityManager): Promise<Array<OrderDetailInterface & {
		orderRequest: OrderRequestInterface,
	}>>
	async getByIds(order_detail_ids: number[], deep: true | undefined, transaction: EntityManager): Promise<Array<OrderDetailInterface & {
		orderRequest: OrderRequestInterface,
		order: OrderInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
		},
		orderDetailShipments: Array<ShipmentOrderDetailInterface & {
			shipment: ShipmentInterface & {
				destination: ShipmentAddressInterface,
			},
		}>,
	}>>

	@RepositoryModel.bound
	async getByIds(
		order_detail_ids: number[],
		deep: boolean | undefined,
		transaction: EntityManager,
	): Promise<any> {
		const Q = this.query('app.order_detail', 'order_detail', transaction)
			.leftJoinAndSelect('order_detail.orderRequest', 'orderRequest')
			.where('order_detail.id IN (:...order_detail_ids)', { order_detail_ids })

		if (deep) {
			return Q
				.leftJoinAndSelect('order_detail.order', 'order')
				.leftJoinAndSelect('order.user', 'user')
				.leftJoinAndSelect('user.profile', 'profile')
				.leftJoinAndSelect('order_detail.orderDetailShipments', 'orderDetailShipments')
				.leftJoinAndSelect('orderDetailShipments.shipment', 'shipment')
				.leftJoinAndSelect('shipment.destination', 'destination')
				.orderBy('order.updated_at', 'DESC')
				.addOrderBy('shipment.created_at', 'DESC')
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.getMany()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getOrderTransactions(
		limit: number | undefined,
		sort_by: string | undefined,
		transaction: EntityManager,
	): Promise<any> {
		let Q = `
		SELECT
		a.user_id,
		MAX(e.email) "email",
		MAX(f.phone) "phone_primary",
		array_to_json(array_remove(ARRAY_AGG(DISTINCT g.phone), NULL)) "phone_secondary",
		COALESCE(SUM(b.price)::int, 0)::int "price_total",
		COALESCE(COUNT(a.user_id)::int, 0)::int "order_total"
		--COALESCE(SUM(c.amount), 0) "point_total",
		--COALESCE(SUM(d.amount), 0) "wallet_total"
		FROM app."order" "a"
		LEFT JOIN app.order_detail "b" ON b.order_id = a.id AND b."status" = 'RESOLVED'
		--LEFT JOIN app.user_point_transaction "c" ON c.ref_id = a.id AND c.source = 'ORDER' AND c."type" = 'CREDIT' AND c."status" = 'APPLIED'
		--LEFT JOIN app.user_wallet_transaction "d" ON d.ref_id = a.id AND d.source = 'ORDER' AND d."type" = 'CREDIT' AND d."status" = 'APPLIED'
		LEFT JOIN app.user "e" ON a.user_id = e.id
		LEFT JOIN app.user_profile "f" ON e.user_profile_id = f.id
		LEFT JOIN app.order_address "g" ON a.id = g.id
		WHERE a."status" IN ('RESOLVED', 'PAID', 'PAID_WITH_EXCEPTION', 'PROCESSING')
		GROUP BY a.user_id
		order BY `

		if (sort_by === 'nominal') {
			Q = Q + ` COALESCE(SUM(b.price), 0)::int DESC, COALESCE(COUNT(a.user_id), 0)::int DESC, `
		}

		if (sort_by === 'count') {
			Q = Q + ` COALESCE(COUNT(a.user_id), 0)::int DESC, COALESCE(SUM(b.price), 0)::int DESC, `
		}

		Q = Q + ` COALESCE(SUM(b.price), 0)::int DESC, COALESCE(COUNT(a.user_id), 0)::int DESC, `
		Q = Q + ` COALESCE(COUNT(a.user_id), 0)::int DESC, COALESCE(SUM(b.price), 0)::int DESC `

		if (limit) {
			Q = Q + ` limit ${limit} `
		} else {
			Q = Q + ` limit 500 `
		}

		return this.queryCustom(Q, undefined, transaction)
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByRefId(order_details_type: ORDER_DETAILS, refId: number, transaction: EntityManager): Promise<OrderDetailInterface> {
		return this.getOne('OrderDetailRecord', { type: order_details_type, ref_id: refId }, { transaction })
	}

	@RepositoryModel.bound
	async filterUnshippedOrderDetail(
		offset: number,
		limit: number,
		filter: {
			search?: string,
			status?: ORDER_DETAIL_STATUSES,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<OrderDetailInterface & {
			order: OrderInterface,
			address?: OrderAddressInterface,
		}>,
		count: number,
	}> {
		let Q = this.query('app.order_detail', 'od', transaction)
			.leftJoinAndSelect('od.order', 'order')
			.leftJoin('od.orderRequest', 'requests')
			.leftJoinAndMapOne('od.address', OrderAddressRecord, 'address', 'address.id = requests.order_address_id')
			.leftJoin('od.orderDetailShipments', 'ods')
			.where(query => {
				// Is not replacement
				return `NOT EXISTS ${query.subQuery()
					.from('app.shipment', 'shipment')
					.select('1')
					.where('shipment.id = ods.shipment_id AND shipment.is_facade = FALSE')
					.getSql()}`
			})
			.andWhere('od.ref_id IS NOT NULL')

		if (filter.search) {
			Q = Q.andWhere('order.number ILIKE :search', { search: `%${filter.search}%` })
		}

		if (filter.status) {
			Q = Q.andWhere('od.status = :status', { status: filter.status })
		}

		return Q.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUnsuppliedOrderDetail(
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface> {
		return this.getOne('OrderDetailRecord', {
			id: order_detail_id,
			ref_id: IsNull(),
		}, {
			transaction,
		})
	}

	// get unique address by order detail shipments
	// so that given several order details, we got to know
	// its destination(s)
	@RepositoryModel.bound
	async getDetailPacket(
		orderDetailIdOrIds: number | number[],
		transaction: EntityManager,
	): Promise<PacketInterface[]> {
		return this.query('app.packet', 'packet', transaction)
			.innerJoin('packet.orderDetails', 'orderDetails', Array.isArray(orderDetailIdOrIds) ? 'orderDetails.id IN (:...idOrIds) ' : 'orderDetails.id = :idOrIds', { idOrIds: orderDetailIdOrIds })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(order_detail_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('OrderDetailRecord', {
			id: order_detail_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	// get unique address by order detail shipments
	// so that given several order details, we got to know
	// its destination(s)
	@RepositoryModel.bound
	async getShipmentAddress(
		orderDetailIdOrIds: number | number[],
		transaction: EntityManager,
	): Promise<ShipmentAddressInterface[]> {
		return this.query('app.shipment', 'shipment', transaction)
			.innerJoin('shipment.shipmentOrderDetails', 'shipmentOrderDetails', Array.isArray(orderDetailIdOrIds) ? 'shipmentOrderDetails.order_detail_id IN (:...idOrIds) ' : 'shipmentOrderDetails.order_detail_id = :idOrIds', { idOrIds: orderDetailIdOrIds })
			.leftJoinAndSelect('shipment.destination', 'destination')
			.getMany()
			.then(this.parser)
			.then((shipments: Array<ShipmentInterface & {
				destination: ShipmentAddressInterface,
			}>) => {
				return uniqBy(shipments.map(shipment => shipment.destination), address => {
					return address.id
				})
			}) as any
	}

	@RepositoryModel.bound
	async getMatches(
		// To be used by cron if undefined
		id: number | undefined,
		user_id: number | undefined,
		is_active: boolean = true,
		filter: {
			date?: Date,
			status?: ORDER_DETAIL_STATUSES,
			with_feedback?: boolean,
			with_history?: boolean,
			with_refund?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		order: OrderInterface,
		request: OrderRequestInterface,
		feedback?: FeedbackInterface,
		shipment?: ShipmentInterface,
		return?: ShipmentInterface,
		stylesheet?: StylesheetInterface & {
			stylesheetInventories?: StylesheetInventoryInterface[],
		},
		styleboard?: StyleboardInterface,
		inventory?: InventoryInterface,
		transaction?: UserWalletTransactionInterface,
		voucher?: VoucherInterface,
		processing?: HistoryOrderDetailInterface,
		primed?: HistoryOrderDetailInterface,
		resolved?: HistoryOrderDetailInterface,
		delivered?: HistoryShipmentInterface,
		confirmed?: HistoryShipmentInterface,
	}>> {
		let Q = this.query('app.order_detail', 'orderDetail', transaction)

			.leftJoinAndSelect('orderDetail.order', 'order')
			.addSelect(['order.created_at', 'styleboard.updated_at', 'shipment.updated_at', 'return.updated_at'])

			.leftJoinAndMapOne('orderDetail.request', OrderRequestRecord, 'request', 'request.id = orderDetail.order_request_id')

			.leftJoin('orderDetail.orderDetailShipments', 'oDS')
			.leftJoin(query => {
				return query.from('app.shipment', 's')
					.where('s.is_facade = false')
					.orderBy('s.created_at', 'DESC')
			}, 's', 'oDS.shipment_id = s.id')
			.leftJoinAndMapOne('orderDetail.shipment', ShipmentRecord, 'shipment', 's.is_return = false AND shipment.id = s.id')
			.leftJoinAndMapOne('orderDetail.return', ShipmentRecord, 'return', 's.is_return = true AND return.id = s.id')

			.leftJoinAndMapOne('orderDetail.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = orderDetail.ref_id AND orderDetail.type = :order_detail_stylesheet', { order_detail_stylesheet: ORDER_DETAILS.STYLESHEET })

			.leftJoinAndMapOne('orderDetail.styleboard', StyleboardRecord, 'styleboard', 'styleboard.id = orderDetail.ref_id AND orderDetail.type = :order_detail_styleboard', { order_detail_styleboard: ORDER_DETAILS.STYLEBOARD })

			.leftJoinAndMapOne('orderDetail.inventory', InventoryRecord, 'inventory', 'inventory.id = orderDetail.ref_id AND orderDetail.type = :order_detail_inventory', { order_detail_inventory: ORDER_DETAILS.INVENTORY })

			.leftJoinAndMapOne('orderDetail.transaction', UserWalletTransactionRecord, 'transaction', 'transaction.id = orderDetail.ref_id AND orderDetail.type = :order_detail_transaction', { order_detail_transaction: ORDER_DETAILS.WALLET_TRANSACTION })

			.leftJoinAndMapOne('orderDetail.voucher', VoucherRecord, 'voucher', 'voucher.id = orderDetail.ref_id AND orderDetail.type = :order_detail_voucher', { order_detail_voucher: ORDER_DETAILS.VOUCHER })


		if (id) {
			Q = Q.where('orderDetail.id = :id', { id })
		} else {
			Q = Q.where(is_active ? 'order.status IN (:...actives)' : 'order.status NOT IN (:...actives)', { actives: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION, ORDERS.PENDING_PAYMENT, ORDERS.PROCESSING] })
		}

		if (user_id) {
			Q = Q.andWhere('order.user_id = :user_id', { user_id })
		}

		if (filter.date) {
			Q = Q.andWhere('s.status IN (:...return) AND s.confirmed_at IS NOT NULL', { return: [SHIPMENT_STATUSES.DELIVERY_CONFIRMED, SHIPMENT_STATUSES.DELIVERED, SHIPMENT_STATUSES.ON_COURIER, SHIPMENT_STATUSES.PROCESSING] })
				.andWhere('shipment.confirmed_at < :date', { date: filter.date })
		}

		if (filter.status) {
			Q = Q.andWhere('orderDetail.status = :status', { status: filter.status })
		}

		if (filter.with_feedback) {
			Q = Q.leftJoinAndMapOne('orderDetail.feedback', FeedbackRecord, 'feedbacks', 'feedbacks.order_detail_id = orderDetail.id')
		}

		if (filter.with_refund) {
			Q = Q.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NOT NULL OR sI.is_refunded IS NOT NULL')
		}

		if (filter.with_history) {
			Q = Q.leftJoin(query => {
				return query.from(HistoryOrderDetailRecord, 'h')
					.orderBy('h.created_at', 'DESC')
			}, 'history', 'history.order_detail_id = orderDetail.id')
				.leftJoin(query => {
					return query.from(HistoryShipmentRecord, 'h')
						.orderBy('h.created_at', 'DESC')
				}, 'shistory', 'shistory.shipment_id = s.id AND s.is_return = FALSE')
				.leftJoinAndMapOne('orderDetail.processing', HistoryOrderDetailRecord, 'processing', 'processing.id = history.id AND processing.status = :processing', { processing: ORDER_DETAIL_STATUSES.PROCESSING })
				.leftJoinAndMapOne('orderDetail.primed', HistoryOrderDetailRecord, 'primed', 'primed.id = history.id AND primed.status = :primed ', { primed: ORDER_DETAIL_STATUSES.PRIMED })
				.leftJoinAndMapOne('orderDetail.resolved', HistoryOrderDetailRecord, 'resolved', 'resolved.id = history.id AND resolved.status = :resolved', { resolved: ORDER_DETAIL_STATUSES.RESOLVED })
				.leftJoinAndMapOne('orderDetail.delivered', HistoryShipmentRecord, 'delivered', 'delivered.id = shistory.id AND delivered.status = :delivered', { delivered: SHIPMENT_STATUSES.DELIVERED })
				.leftJoinAndMapOne('orderDetail.confirmed', HistoryShipmentRecord, 'confirmed', 'confirmed.id = shistory.id AND confirmed.status = :confirmed', { confirmed: SHIPMENT_STATUSES.DELIVERY_CONFIRMED })
		}

		return Q.andWhere(`orderDetail.type not in ('STYLEBOARD')`)
			.getMany()
			.then(this.parser) as any

	}

	@RepositoryModel.bound
	async getPostponed(
		transaction: EntityManager,
	): Promise<OrderDetailInterface[]> {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.innerJoin('orderDetail.order', 'order', 'order.status IN (:...order_status)', { order_status: [ORDERS.PAID, ORDERS.PAID_WITH_EXCEPTION] })
			.innerJoin('order.user', 'user')
			.innerJoin('user.profile', 'profile', 'profile.is_style_profile_completed = FALSE')
			.where('orderDetail.type in (:order_detail_stylesheet, :order_detail_styleboard)', { order_detail_stylesheet: ORDER_DETAILS.STYLESHEET, order_detail_styleboard: ORDER_DETAILS.STYLEBOARD })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBelong(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface> {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.innerJoin('orderDetail.order', 'order')
			.where('orderDetail.id = :order_detail_id', { order_detail_id })
			.andWhere('order.user_id = :user_id', { user_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailedHistory(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface & {
		order: OrderInterface & {
			payments: OrderPaymentInterface[],
		},
		shipments?: Array<ShipmentInterface & {
			histories: HistoryShipmentInterface[],
		}>,
		histories: HistoryOrderDetailInterface[],
	}> {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.leftJoinAndSelect('orderDetail.order', 'order')
			.leftJoin('order.orderPayments', 'oP')
			.leftJoinAndMapMany('order.payments', OrderPaymentRecord, 'payments', 'payments.code != :payment AND payments.id = oP.id', { payment: PAYMENTS.PENDING })
			.leftJoin('orderDetail.orderDetailShipments', 'orderDetailShipments')
			.leftJoinAndMapMany('orderDetail.shipments', ShipmentRecord, 'shipment', 'shipment.id = orderDetailShipments.shipment_id AND shipment.is_facade = :is_facade', { is_facade: false })
			.leftJoinAndMapMany('shipment.histories', HistoryShipmentRecord, 'shipmentHistories', 'shipmentHistories.shipment_id = shipment.id AND shipmentHistories.status IS NOT NULL')
			.leftJoinAndMapMany('orderDetail.histories', HistoryOrderDetailRecord, 'histories', 'histories.order_detail_id = orderDetail.id AND histories.status IS NOT NULL')
			.where('order.user_id = :user_id', { user_id })
			.andWhere('orderDetail.id = :order_detail_id', { order_detail_id })
			.andWhere('orderDetail.ref_id IS NOT NULL')
			.orderBy('shipment.created_at', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailedShallow<D extends boolean>(
		order_detail_ids: number[],
		withDiscount: D,
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		stylesheet?: StylesheetInterface & {
			matchbox: MatchboxInterface,
			stylesheetUserAssets: StylesheetUserAssetInterface[],
			stylesheetInventories: Array<StylesheetInventoryInterface & {
				inventory: InventoryInterface,
				category: CategoryInterface,
			}>,
		},
		inventory?: InventoryInterface,
		voucher?: VoucherInterface & {
			matchbox?: MatchboxInterface,
			inventory?: InventoryInterface,
		},
		shipments: ShipmentInterface[],
		orderDetailCampaigns: true extends D ? OrderDetailCampaignInterface[] : never,
		orderDetailCoupons: true extends D ? OrderDetailCouponInterface[] : never,
		orderDetailTransactions: true extends D ? OrderDetailTransactionInterface[] : never,
	}>> {
		const Q = this.query('app.order_detail', 'detail', transaction)
			.leftJoinAndMapOne('detail.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = detail.ref_id AND detail.type = :stylesheet_detail_type', { stylesheet_detail_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndSelect('stylesheet.matchbox', 'sMatchbox')
			.leftJoinAndSelect('stylesheet.stylesheetUserAssets', 'stylesheetUserAssets')
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'stylesheetInventories', 'stylesheetInventories.deleted_at IS NULL')
			.leftJoinAndSelect('stylesheetInventories.inventory', 'sInventory')
			.leftJoin('sInventory.variant', 'sVariant')
			.leftJoin('sVariant.product', 'sProduct')
			.leftJoinAndMapOne('stylesheetInventories.category', CategoryRecord, 'sCategory', 'sCategory.id = sProduct.category_id')

			.leftJoinAndMapOne('detail.inventory', InventoryRecord, 'inventory', 'inventory.id = detail.ref_id AND detail.type = :inventory_detail_type', { inventory_detail_type: ORDER_DETAILS.INVENTORY })

			.leftJoinAndMapOne('detail.voucher', VoucherRecord, 'voucher', 'voucher.id = detail.ref_id AND detail.type = :voucher_detail_type', { voucher_detail_type: ORDER_DETAILS.VOUCHER })
			.leftJoinAndMapOne('voucher.matchbox', MatchboxRecord, 'matchbox', 'matchbox.id = voucher.ref_id AND voucher.type = :matchbox_voucher_type', { matchbox_voucher_type: VOUCHERS.MATCHBOX })
			.leftJoinAndMapOne('voucher.inventory', InventoryRecord, 'vInventory', 'vInventory.id = voucher.ref_id AND voucher.type = :inventory_voucher_type', { inventory_voucher_type: VOUCHERS.INVENTORY })

			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = detail.id')
			.leftJoinAndMapMany('detail.shipments', ShipmentRecord, 'shipment', 'shipment.id = sOd.shipment_id and shipment.is_facade is true')

			.where('detail.id IN (:...order_detail_ids)', { order_detail_ids })

		if (withDiscount === true) {
			return Q
				.leftJoinAndSelect('detail.orderDetailCampaigns', 'campaigns')
				.leftJoinAndSelect('detail.orderDetailCoupons', 'coupons')
				.leftJoinAndSelect('detail.orderDetailTransactions', 'transactions')
				.getMany()
				.then(this.parser) as any
		} else {
			return Q
				.getMany()
				.then(this.parser) as any
		}

	}

	@RepositoryModel.bound
	async getDetail<I extends boolean, D extends boolean, R extends boolean, S extends boolean, F extends boolean, O extends boolean>(
		order_detail_ids: number[],
		{
			with_detailed_item,
			with_discount,
			with_request,
			with_shipment,
			with_feedback,
			with_order,
		}: {
			with_detailed_item?: I,
			with_discount?: D,
			with_request?: R,
			with_shipment?: S,
			with_feedback?: F,
			with_order?: O,
		},
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & (D extends true ? {
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		orderDetailTransactions: Array<OrderDetailTransactionInterface & {
			transaction: UserWalletTransactionInterface,
		}>,
	} : {}) & (R extends true ? {
		request: OrderRequestInterface & {
			address?: OrderAddressInterface,
		},
	} : {}) & (I extends true ? {
		variants: Array<VariantInterface & {
			inventory: InventoryInterface,
		}>,
	} : {}) & (S extends true ? {
		shipments: Array<ShipmentInterface & (D extends true ? {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		} : {})>,
	} : {}) & (F extends true ? {
		feedbacks: Array<FeedbackInterface & {
			answers: FeedbackAnswerInterface[],
		}>,
	} : {}) & (D extends true ? {
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
	} : {}) & (O extends true ? {
		order: OrderInterface,
	} : {})>> {
		let Q = this.query('app.order_detail', 'detail', transaction)

			.where('detail.id IN (:...order_detail_ids)', { order_detail_ids })

		if (with_discount) {
			Q = Q
				.leftJoinAndSelect('detail.orderDetailCampaigns', 'campaigns')
				.leftJoinAndSelect('detail.orderDetailCoupons', 'coupons')
				.leftJoinAndSelect('detail.orderDetailTransactions', 'transactions')
				.leftJoinAndSelect('transactions.transaction', 'transaction')
		}

		if (with_request) {
			Q = Q.leftJoinAndMapOne('detail.request', OrderRequestRecord, 'request', 'request.id = detail.order_request_id')
				.leftJoinAndSelect('request.address', 'address')
		}

		if (with_detailed_item) {
			// get stylesheet if order from matchbox
			Q = Q.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = detail.ref_id and detail.type = \'STYLESHEET\' and si.deleted_at is null')
				.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')

				// get item if order from variant
				.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = detail.ref_id and detail.type = 'INVENTORY'`)

				// get variant
				.leftJoinAndMapMany('detail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_inventory.variant_id, si_inventory.variant_id)')
				.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(od_inventory.id, si_inventory.id)')
		}

		if (with_shipment) {
			Q = Q.leftJoin('detail.orderDetailShipments', 'ods')
				.leftJoinAndMapMany('detail.shipments', ShipmentRecord, 'shipment', 'shipment.id = ods.shipment_id AND shipment.is_facade = FALSE')

			if (with_discount) {
				Q = Q.leftJoinAndSelect('shipment.shipmentCampaigns', 'shipmentCampaign')
					.leftJoinAndSelect('shipment.shipmentCoupons', 'shipmentCoupon')
			}
		}

		if (with_discount) {
			Q = Q.leftJoinAndMapMany('detail.campaigns', CampaignRecord, 'campaign', `campaign.id = campaigns.campaign_id${with_shipment ? ' OR campaign.id = shipmentCampaign.campaign_id' : ''}`)
				.leftJoinAndMapMany('detail.coupons', CouponRecord, 'coupon', `coupon.id = coupons.coupon_id${with_shipment ? ' OR coupon.id = shipmentCoupon.coupon_id' : ''}`)
		}

		if (with_feedback) {
			Q = Q.leftJoinAndSelect('detail.feedbacks', 'feedbacks')
				.leftJoinAndSelect('feedbacks.answers', 'answers')
		}

		if (with_order) {
			Q = Q.leftJoinAndSelect('detail.order', 'order')
		}

		return Q
			.getMany()
			.then(this.parser) as any

	}

	@RepositoryModel.bound
	async checkFeedback(
		order_detail_id: number,
		user_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.query('app.order_detail', 'detail', transaction)
			.select('detail.id')
			.innerJoin('detail.feedbacks', 'feedback')
			.where('detail.id = :order_detail_id', { order_detail_id })
			.andWhere('feedback.user_id = :user_id', { user_id })
			.getOne()
			.then(this.parser)
			.then(() => true)
	}

	@RepositoryModel.bound
	async getWithFeedback(
		order_detail_id: number,
		user_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface & {
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		orderDetailTransactions: OrderDetailTransactionInterface[],
		shipments: ShipmentInterface[],
		feedbacks: Array<FeedbackInterface & {
			variant?: VariantInterface & {
				asset: VariantAssetInterface,
				product: ProductInterface & {
					brand: BrandInterface,
				},
				size: SizeInterface,
				colors: ColorInterface[],
			}
			stylesheet?: StylesheetInterface & {
				stylesheetInventories: Array<StylesheetInventoryInterface & {
					inventory: InventoryInterface,
				}>,
			},
			answers: Array<FeedbackAnswerInterface & {
				question: QuestionInterface,
			}>,
		}>,
	}> {
		return this.query('app.order_detail', 'detail', transaction)
			.leftJoinAndSelect('detail.orderDetailCampaigns', 'campaigns')
			.leftJoinAndSelect('detail.orderDetailCoupons', 'coupons')
			.leftJoinAndSelect('detail.orderDetailTransactions', 'transactions')
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = detail.id')
			.leftJoinAndMapMany('detail.shipments', ShipmentRecord, 'shipment', 'shipment.id = sOd.shipment_id and shipment.is_facade is true')
			.leftJoinAndSelect('detail.feedbacks', 'feedback')
			.leftJoinAndMapOne('feedback.variant', VariantRecord, 'variant', 'feedback.type = :variant_feedback AND feedback.ref_id = variant.id', { variant_feedback: FEEDBACKS.VARIANT })
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = variantColors.color_id')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndMapOne('feedback.stylesheet', StylesheetRecord, 'stylesheet', 'feedback.type = :stylesheet_feedback AND feedback.ref_id = stylesheet.id', { stylesheet_feedback: FEEDBACKS.STYLESHEET })
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndSelect('feedback.answers', 'answers')
			.leftJoinAndSelect('answers.question', 'question')
			.where('feedback.order_detail_id = :order_detail_id', { order_detail_id })
			.andWhere('feedback.user_id = :user_id', { user_id })
			.orderBy('asset.order', 'ASC')
			.addOrderBy('question.order', 'ASC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByStylesheetId(
		stylesheet_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface & {
		shipments: ShipmentInterface[],
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		orderDetailTransactions: OrderDetailTransactionInterface[],
	}> {
		return this.query('app.order_detail', 'detail', transaction)
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = detail.id')
			.leftJoinAndMapMany('detail.shipments', ShipmentRecord, 'shipment', 'shipment.id = sOd.shipment_id and shipment.is_facade is true')
			.leftJoinAndSelect('detail.orderDetailCampaigns', 'campaigns')
			.leftJoinAndSelect('detail.orderDetailCoupons', 'coupons')
			.leftJoinAndSelect('detail.orderDetailTransactions', 'transactions')
			.where('detail.ref_id = :stylesheet_id', { stylesheet_id })
			.andWhere('detail.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrder(
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
	}> {
		return this.query('app.order', 'order', transaction)
			.innerJoin('order.orderDetails', 'orderDetails', 'orderDetails.id = :order_detail_id', { order_detail_id })
			.innerJoinAndSelect('order.user', 'user')
			.innerJoinAndSelect('user.profile', 'profile')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderWithDetails(
		type: 'order' | 'order_detail',
		order_or_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderInterface & {
		orderDetails: OrderDetailInterface[],
	}> {
		if (type === 'order_detail') {
			return this.query('app.order', 'order', transaction)
				.innerJoinAndMapOne('order.detail', query => {
					return query.from('app.order_detail', 'detail')
						.where('detail.id = :order_detail_id', { order_detail_id: order_or_detail_id })
				}, 'orderDetail', '"orderDetail"."order_id" = order.id')
				.leftJoinAndSelect('order.orderDetails', 'orderDetails')
				.getOne()
				.then(this.parser) as any
		} else {
			return this.query('app.order', 'order', transaction)
				.leftJoinAndSelect('order.orderDetails', 'orderDetails')
				.where('order.id = :order_id', { order_id: order_or_detail_id })
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getWithAddressAndPacket(
		user_id: number | undefined,
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<OrderDetailInterface & {
		address: OrderAddressInterface,
		packet: PacketInterface,
	}> {
		let Q = this.query('app.order_detail', 'orderDetail', transaction)

		if (user_id) {
			Q = Q.innerJoin('orderDetail.order', 'order', 'order.user_id = :user_id', { user_id })
		}

		return Q = Q.leftJoinAndSelect('orderDetail.packet', 'packet')
			.leftJoin('orderDetail.orderRequest', 'request')
			.leftJoinAndMapOne('orderDetail.address', OrderAddressRecord, 'address', 'request.order_address_id = address.id')
			.where('orderDetail.id = :order_detail_id', { order_detail_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderDetailFromVoucherRedeem(id: number, transaction: EntityManager): Promise<OrderDetailInterface> {
		return this.query('app.order_detail', 'od1', transaction)
			.innerJoin(OrderRequestRecord, 'oq', 'oq.ref_id = od1.ref_id and oq.type = :requestType', { requestType: ORDER_REQUESTS.VOUCHER })
			.innerJoin('oq.orderDetails', 'od2')
			.where('od1.type = :type and od2.id = :id', { type: ORDER_DETAILS.VOUCHER, id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRequestShallow(order_detail_id: number, transaction: EntityManager): Promise<OrderRequestInterface> {
		return this.query('app.order_request', 'request', transaction)
			.innerJoin(OrderDetailRecord, 'detail', 'detail.order_request_id = request.id AND detail.id = :order_detail_id', { order_detail_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getRequest(order_detail_id: number, transaction: EntityManager): Promise<OrderRequestInterface & {
		voucher?: VoucherInterface,
		matchbox?: MatchboxInterface,
		service?: ServiceInterface,
		styleboard?: StyleboardInterface,
		topup?: TopupInterface,
		variant?: VariantInterface & {
			product: ProductInterface & {
				brand: BrandInterface,
			},
		},
		address: OrderAddressInterface,
	}> {
		return this.query('app.order_request', 'request', transaction)
			.innerJoin(OrderDetailRecord, 'detail', 'detail.order_request_id = request.id AND detail.id = :order_detail_id', { order_detail_id })
			.leftJoinAndSelect('request.address', 'address')
			.leftJoinAndMapOne('request.voucher', VoucherRecord, 'voucher', 'request.ref_id = voucher.id AND request.type = :voucher_request', { voucher_request: ORDER_REQUESTS.VOUCHER })

			.leftJoinAndMapOne('request.matchbox', MatchboxRecord, 'matchbox', 'request.ref_id = matchbox.id AND request.type = :matchbox_request', { matchbox_request: ORDER_REQUESTS.MATCHBOX })
			.leftJoinAndMapOne('request.service', ServiceRecord, 'service', 'request.ref_id = service.id AND request.type = :service_request', { service_request: ORDER_REQUESTS.MATCHBOX })
			.leftJoinAndMapOne('request.styleboard', StyleboardRecord, 'styleboard', 'request.ref_id = styleboard.id AND request.type = :styleboard_request', { styleboard_request: ORDER_REQUESTS.STYLEBOARD })
			.leftJoinAndMapOne('request.topup', TopupRecord, 'topup', 'request.ref_id = topup.id AND request.type = :topup_request', { topup_request: ORDER_REQUESTS.TOPUP })
			.leftJoinAndMapOne('request.variant', VariantRecord, 'variant', 'request.ref_id = variant.id AND request.type = :variant_request', { variant_request: ORDER_REQUESTS.VARIANT })
			.leftJoinAndSelect('variant.product', 'vProduct')
			.leftJoinAndSelect('vProduct.brand', 'vBrand')

			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getSummary(order_detail_id: number, transaction: EntityManager): Promise<OrderDetailInterface & {
		service?: {},
		inventory?: {},
		styleboard?: {
			stage_name: string,
			outfit: string,
			attire: string,
			product: string,
			note: string,
		},
		stylesheet?: {
			lineup: string,
			note: string,
			images: string[],
		},
		voucher?: {
			receiver: string,
			email: string,
			note: string,
		},
	}> {
		return this.query('app.order_detail', 'order_detail', transaction)
			.where('order_detail.id = :order_detail_id', { order_detail_id })
			.getOne()
			.then(this.parser)
			.then((order_detail: OrderDetailInterface) => {
				switch (order_detail.type) {
					case ORDER_DETAILS.STYLEBOARD:
						return this.query('app.order_detail', 'orderDetail', transaction)
							.leftJoinAndMapOne('orderDetail.styleboard', StyleboardRecord, 'styleboard', 'styleboard.id = orderDetail.ref_id')
							.leftJoin('styleboard.stylist', 'stylist')
							.leftJoinAndMapOne('styleboard.stylistProfile', UserProfileRecord, 'stylistProfile', 'stylistProfile.id = stylist.user_profile_id')
							.where('orderDetail.id = :id', { id: order_detail.id })
							.getOne()
							.then(this.parser)
							.then((orderDetail: OrderDetailInterface & {
								styleboard: StyleboardInterface & {
									stylistProfile: UserProfileInterface,
								},
							}) => {
								return {
									...orderDetail,
									styleboard: {
										stage_name: orderDetail.styleboard.stylistProfile.stage_name,
										outfit: orderDetail.styleboard.outfit,
										attire: orderDetail.styleboard.attire,
										product: orderDetail.styleboard.product,
										note: orderDetail.styleboard.note,
									},
								}
							}) as any
					case ORDER_DETAILS.STYLESHEET:
						return this.query('app.order_detail', 'orderDetail', transaction)
							.leftJoinAndMapOne('orderDetail.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = orderDetail.ref_id')
							.leftJoin('stylesheet.stylesheetUserAssets', 'stylesheetUserAsset')
							.leftJoinAndMapMany('stylesheet.userAssets', UserAssetRecord, 'userAssets', 'userAssets.id = stylesheetUserAsset.user_asset_id')
							.where('orderDetail.id = :id', { id: order_detail.id })
							.getOne()
							.then(this.parser)
							.then((orderDetail: OrderDetailInterface & {
								stylesheet: StylesheetInterface & {
									userAssets: UserAssetInterface[],
								},
							}) => {
								return {
									...orderDetail,
									stylesheet: {
										lineup: orderDetail.stylesheet.lineup,
										note: orderDetail.stylesheet.note,
										images: orderDetail.stylesheet.userAssets.map(userAsset => userAsset.url),
									},
								}
							}) as any
					case ORDER_DETAILS.INVENTORY:
						return {
							...order_detail,
							inventory: {},
						}
					case ORDER_DETAILS.VOUCHER:
						return this.query('app.order_detail', 'orderDetail', transaction)
							.leftJoinAndMapOne('orderDetail.voucher', VoucherRecord, 'voucher', 'voucher.id = orderDetail.ref_id')
							.where('orderDetail.id = :id', { id: order_detail.id })
							.getOne()
							.then(this.parser)
							.then((orderDetail: OrderDetailInterface & {
								voucher: VoucherInterface,
							}) => {
								return {
									...orderDetail,
									voucher: {
										receiver: orderDetail.voucher.name,
										email: orderDetail.voucher.email,
										note: orderDetail.voucher.note,
									},
								}
							}) as any
				}
			})
	}

	@RepositoryModel.bound
	async getTransactions(user_id: number, order_detail_id: number, transaction: EntityManager): Promise<Array<OrderDetailTransactionInterface & {
		stylesheetInventory: StylesheetInventoryInterface & {
			inventory: InventoryInterface,
			variant: VariantInterface,
			product: ProductInterface,
			brand: BrandInterface,
			category: CategoryInterface,
			colors: ColorInterface[],
			size: SizeInterface,
		},
	}>> {
		return this.query('app.order_detail_transaction', 'tx', transaction)
			.innerJoin(OrderDetailRecord, 'detail', 'tx.order_detail_id = :order_detail_id', { order_detail_id })
			.innerJoin('detail.order', 'order', 'order.user_id = :user_id', { user_id })
			.leftJoin('tx.transaction', 'wT')
			.leftJoinAndMapOne('tx.stylesheetInventory', StylesheetInventoryRecord, 'sI', 'sI.id = wT.ref_id AND wT.source = :stylesheet_inventory_transaction', { stylesheet_inventory_transaction: STATEMENT_SOURCES.STYLESHEET_INVENTORY })
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndMapOne('sI.variant', VariantRecord, 'variant', 'variant.id = inventory.variant_id')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapOne('sI.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoin('sI.purchaseRequest', 'pr')
			.leftJoinAndMapOne('sI.brand', BrandRecord, 'brand', '(CASE WHEN product.brand_id IS NOT NULL THEN brand.id = product.brand_id ELSE brand.id = pr.brand_id END)')
			.leftJoinAndMapOne('sI.category', CategoryRecord, 'category', '(CASE WHEN product.category_id IS NOT NULL THEN category.id = product.category_id ELSE category.id = pr.category_id END)')
			.leftJoinAndMapMany('sI.colors', ColorRecord, 'color', '(CASE WHEN variantColors.color_id IS NOT NULL THEN color.id = variantColors.color_id ELSE color.id = pr.color_id END)')
			.leftJoinAndMapOne('sI.size', SizeRecord, 'size', '(CASE WHEN variant.size_id IS NOT NULL THEN size.id = variant.size_id ELSE size.id = pr.size_id END)')
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
