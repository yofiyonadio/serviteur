import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
} from 'typeorm'

import {
	BagRecord,
	BagItemRecord,
	UserRecord,
	UserProfileRecord,
} from 'energie/app/records'
import { Parameter } from 'types/common'
import { BagInterface, BagItemInterface, UserInterface, UserProfileInterface } from 'energie/app/interfaces'
import { ORDER_REQUESTS } from 'energie/utils/constants/enum'


@EntityRepository()
export default class OrderBagRepository extends RepositoryModel<{
	BagRecord: BagRecord,
	BagItemRecord: BagItemRecord,
}> {

	static __displayName = 'OrderBagRepository'

	protected records = {
		BagRecord,
		BagItemRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<BagInterface>,
		transaction: EntityManager,
	) {
		return this.save('BagRecord', data, transaction)
	}

	@RepositoryModel.bound
	async insertItems(
		datas: Array<Parameter<BagItemInterface>>,
		transaction: EntityManager,
	) {
		return this.save('BagItemRecord', datas, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateItem(
		id: number,
		bag_id: number,
		data: Partial<BagItemInterface>,
		transaction: EntityManager,
	) {
		return this.renew('BagItemRecord', 'id = :id and bag_id = :bag_id', data, {
			parameters: {
				id,
				bag_id,
			},
			transaction,
		})
	}

	@RepositoryModel.bound
	async orderItem(
		bag_id: number,
		ref_id: number,
		type: ORDER_REQUESTS,
		transaction: EntityManager,
	) {
		return this.renew('BagItemRecord', 'bag_id = :bag_id and ref_id = :ref_id and type = :type', { ordered_at: new Date() }, {
			parameters: {
				bag_id,
				ref_id,
				type,
			},
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getAllBag(transaction: EntityManager): Promise<Array<BagInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		items: BagItemInterface[],
	}>> {
		return this.query('app.bag', 'bag', transaction)

			.leftJoinAndMapOne('bag.user', UserRecord, 'user', 'user.id = bag.user_id')
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')

			.leftJoinAndMapMany('bag.items', BagItemRecord, 'item', 'item.bag_id = bag.id and item.deleted_at is null and item.ordered_at is null')

			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getBagAbandonment(transaction: EntityManager): Promise<BagItemInterface[]> {
		return this.queryCustom(`SELECT a.user_id, a.id AS bag_id, c.email,
		array_to_json(ARRAY_AGG(DISTINCT b.id)) AS bag_item_ids,
		COUNT(DISTINCT b.ref_id) AS ref_amount,
		array_to_json(ARRAY_AGG(DISTINCT b.ref_id)) AS refs,
		array_to_json(ARRAY_AGG(DISTINCT b.type)) AS types,
		array_to_json(ARRAY_AGG(DISTINCT d.title)) AS titles,
		array_to_json(ARRAY_AGG(DISTINCT d.price)) AS prices,
		array_to_json(ARRAY_AGG(DISTINCT d.retail_price)) AS retail_prices,
		array_to_json(ARRAY_AGG(DISTINCT d.image_url)) AS image_urls,
		array_to_json(ARRAY_AGG(DISTINCT d.brand)) AS brands,
		SUM(DISTINCT d.price)::int AS price_total,
		MAX(DISTINCT b.created_at) AS updated_at
		FROM app.bag AS a
		LEFT JOIN app.bag_item AS b
		ON a.id = b.bag_id
		LEFT JOIN app.user AS c
		ON a.user_id = c.id
		LEFT JOIN VIEW.item_merger AS d
		ON d.id = b.ref_id AND d.column = b.type::TEXT
		LEFT JOIN app.email AS e
		ON e.ref_id = b.id AND e.type = 'BAG_ABANDONMENT'
		WHERE b.deleted_at IS NULL
		AND b.ordered_at IS NULL
		AND e.id IS NULL
		GROUP BY a.user_id, a.id, c.email
		--HAVING
		--((date_part('day'::text, (now() - max(b.created_at))) * (24)::double PRECISION) +
		--date_part('hour'::text, (now() - max(b.created_at)))) >= 0
		ORDER BY updated_at DESC LIMIT 1000`, undefined, transaction)
			.then(this.parser) as any
	}
	@RepositoryModel.bound
	async getUserBag<
		Many extends boolean,
		U extends true,
		Return extends BagRecord & (
			U extends true ? {
				user: UserInterface & {
					profile: UserProfileInterface,
				},
			} : {}) & {
				items: BagItemInterface[],
			}>(
				user_id: number | undefined,
				option: {
					get_many?: Many,
					with_items?: boolean,
					with_user?: U,
				} = {},
				filter: {
					date?: Date,
					for_test?: boolean,
				} = {},
				transaction: EntityManager,
	): Promise<Many extends true ? Return[] : Return> {
		const Q = this.query('app.bag', 'bag', transaction)
			.where('true')

		if (filter.for_test) {
			Q.andWhere('bag.user_id in (22, 14351)')
		}

		if (!!user_id) {
			Q.andWhere('bag.user_id = :user_id', { user_id })
		}

		if (option.with_items) {
			Q.leftJoinAndMapMany('bag.items', BagItemRecord, 'item', 'item.bag_id = bag.id and item.deleted_at is null and item.ordered_at is null')
		}

		if (option.with_user) {
			Q.leftJoinAndMapOne('bag.user', UserRecord, 'user', 'user.id = bag.user_id')
				.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
		}

		if (!!filter.date) {
			Q.andWhere('item.created_at < :date', { date: filter.date })
		}

		if (option.get_many) {
			return Q.getMany()
				.then(this.parser) as any
		} else {
			return Q.getOne()
				.then(this.parser) as any
		}
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async deleteItem(
		id: number,
		bag_id: number,
		transaction: EntityManager,
	) {
		return this.renew('BagItemRecord', 'id = :id and bag_id = :bag_id', { deleted_at: new Date() }, {
			parameters: {
				id,
				bag_id,
			},
			transaction,
		})
	}

	// ============================ PRIVATES ============================
}
