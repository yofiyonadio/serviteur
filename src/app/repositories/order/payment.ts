import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	OrderPaymentRecord,
	PaymentChannelRecord,
} from 'energie/app/records'

import {
	OrderPaymentInterface,
	PaymentChannelInterface,
} from 'energie/app/interfaces'

import { PAYMENTS, PAYMENT_AGENTS } from 'energie/utils/constants/enum'
import { Parameter } from 'types/common'

@EntityRepository()
export default class OrderPaymentRepository extends RepositoryModel<{
	OrderPaymentRecord: OrderPaymentRecord,
	PaymentChannelRecord: PaymentChannelRecord,
}> {

	static __displayName = 'OrderPaymentRepository'

	protected records = {
		OrderPaymentRecord,
		PaymentChannelRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		order_id: number,
		code: PAYMENTS,
		agent: PAYMENT_AGENTS,
		method: string | undefined,
		metadata: object | undefined,
		transaction: EntityManager,
	): Promise<OrderPaymentInterface> {
		return this.save('OrderPaymentRecord', {
			order_id,
			code,
			agent,
			method,
			metadata,
		}, transaction)
	}

	async insertPaymentMethod(
		datas: Array<Parameter<PaymentChannelInterface>>,
		transaction: EntityManager,
	): Promise<PaymentChannelInterface[]> {
		return this.save('PaymentChannelRecord', datas, transaction)
	}

	// ============================= UPDATE =============================
	async updateActivePayment(
		order_id: number,
		data: Partial<Parameter<OrderPaymentInterface>>,
		transaction: EntityManager,
	) {
		return this.renew('OrderPaymentRecord', 'order_id = :order_id AND code = :status', data, {
			parameters: {
				order_id,
				status: PAYMENTS.PENDING,
			},
			transaction,
		})
	}

	// ============================= GETTER =============================
	async getPaymentMethods(transaction: EntityManager): Promise<PaymentChannelInterface[]> {
		return this.query('other.payment_channel', 'channel', transaction).getMany() as any
	}

	async getPaymentWithStatus(
		order_id: number,
		status: PAYMENTS,
		transaction: EntityManager,
	): Promise<OrderPaymentInterface> {
		return this.query('app.order_payment', 'payment', transaction)
			.where('payment.order_id = :order_id', {order_id})
			.andWhere('payment.code = :status', {status})
			.getOne()
			.then(this.parser) as any
	}

	async getPaymentsByOrderNumber(
		order_number: string,
		transaction: EntityManager,
	): Promise<OrderPaymentInterface[]> {
		return this.query('app.order_payment', 'payment', transaction)
			.innerJoin('payment.order', 'order', 'order.number = :order_number', { order_number })
			.orderBy('payment.created_at', 'DESC')
			.getMany()
			.then(this.parser) as any
	}

	async getPaymentCode(
		payment_code: string,
		transaction: EntityManager,
	): Promise<number> {
		return this.query('app.order', 'order', transaction)
			.where(`(order.metadata ->> 'payment_code')::text = :payment_code`, {payment_code})
			.orderBy('order.created_at', 'DESC')
			.getOne()
			.then(this.parser)
			.then(result => (result as any).id)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
