import {
	ErrorModel,
	RepositoryModel,
} from 'app/models'
import { EntityRepository, EntityManager, IsNull } from 'typeorm'

import {
	HistoryOrderRecord,
	OrderRecord,
	OrderAddressRecord,
	OrderRequestRecord,
	ShipmentRecord,
	CampaignRecord,
	CouponRecord,
	UserWalletTransactionRecord,
	VoucherRecord,
	StylesheetRecord,
	StyleboardRecord,
	InventoryRecord,
	ServiceRecord,
	MatchboxRecord,
	TopupAmountRecord,
	ProductRecord,
	BrandRecord,
	HistoryOrderDetailRecord,
	HistoryShipmentRecord,
	HistoryStyleboardRecord,
	HistoryStylesheetRecord,
	VariantRecord,
	OrderDetailCouponRecord,
	OrderDetailRecord,
	FeedbackRecord,
	FeedbackAnswerRecord,
	OrderDetailTransactionRecord,
	ShipmentCampaignRecord,
	ShipmentCouponRecord,
	ShipmentAddressRecord,
	UserPointTransactionRecord,
	StylesheetInventoryRecord,
	OrderDetailCampaignRecord,
	ShipmentOrderDetailRecord,
	OrderPaymentRecord,
	UserRecord,
	UserProfileRecord,
	VariantColorRecord,
	ColorRecord,
	VariantAssetRecord,
	SizeRecord,
	UserAddressRecord,
	QuestionRecord,
} from 'energie/app/records'

import {
	OrderInterface,
	OrderAddressInterface,
	OrderDetailInterface,
	OrderPaymentInterface,
	OrderRequestInterface,
	UserProfileInterface,
	UserInterface,
	ShipmentInterface,
	CampaignInterface,
	CouponInterface,
	ShipmentOrderDetailInterface,
	OrderDetailCouponInterface,
	OrderDetailCampaignInterface,
	UserWalletTransactionInterface,
	HistoryOrderInterface,
	// StylesheetInterface,
	MatchboxInterface,
	BrandInterface,
	// InventoryInterface,
	// VariantInterface,
	ProductInterface,
	ServiceInterface,
	// StyleboardInterface,
	VoucherInterface,
	TopupAmountInterface,
	OrderDetailTransactionInterface,
	ShipmentCampaignInterface,
	ShipmentCouponInterface,
	StyleboardInterface,
	HistoryShipmentInterface,
	HistoryOrderDetailInterface,
	HistoryStyleboardInterface,
	StylesheetInterface,
	HistoryStylesheetInterface,
	FeedbackInterface,
	FeedbackAnswerInterface,
	ShipmentAddressInterface,
	UserPointTransactionInterface,
	InventoryInterface,
	VariantInterface,
	// ShipmentAddressInterface,
} from 'energie/app/interfaces'

import CommonHelper from 'utils/helpers/common'
import TimeHelper from 'utils/helpers/time'

import { Parameter } from 'types/common'

import DEFAULTS from 'utils/constants/default'
import { ORDERS, STATEMENT_SOURCES, ORDER_DETAILS, VOUCHERS, ORDER_REQUESTS, SHIPMENT_STATUSES } from 'energie/utils/constants/enum'
import { ERROR_CODES } from 'app/models/error'


@EntityRepository()
export default class OrderRepository extends RepositoryModel<{
	HistoryOrderRecord: HistoryOrderRecord,
	OrderRecord: OrderRecord,
	OrderAddressRecord: OrderAddressRecord,
	OrderRequestRecord: OrderRequestRecord,
}> {

	static __displayName = 'OrderRepository'

	protected records = {
		HistoryOrderRecord,
		OrderRecord,
		OrderAddressRecord,
		OrderRequestRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		order: Parameter<OrderInterface>,
		transaction: EntityManager,
	): Promise<OrderInterface> {
		return this.save('OrderRecord', order, transaction)
	}

	insertRequest(request: Parameter<OrderRequestInterface>, transaction: EntityManager): Promise<OrderRequestInterface>
	insertRequest(requests: Array<Parameter<OrderRequestInterface>>, transaction: EntityManager): Promise<OrderRequestInterface[]>

	@RepositoryModel.bound
	async insertRequest(
		request: Parameter<OrderRequestInterface> | Array<Parameter<OrderRequestInterface>>,
		transaction?: EntityManager,
	): Promise<any> {
		return this.save('OrderRequestRecord', request as any, transaction)
	}

	@RepositoryModel.bound
	async insertAddress(
		address: Parameter<OrderAddressInterface>,
		transaction: EntityManager,
	): Promise<OrderAddressInterface> {
		// this means that address is not unique
		return this.getOne('OrderAddressRecord', {
			order_id: address.order_id,
			location_id: address.location_id || IsNull(),
			title: CommonHelper.default(address.title, ''),
			receiver: address.receiver || IsNull(),
			phone: address.phone || IsNull(),
			address: address.address || IsNull(),
			district: address.district || IsNull(),
			postal: address.postal || IsNull(),
		}, {
			transaction,
		}).catch(async err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.save('OrderAddressRecord', {
					order_id: address.order_id,
					location_id: address.location_id,
					title: address.title || '',
					receiver: address.receiver,
					phone: address.phone,
					address: address.address,
					district: address.district,
					postal: address.postal,
					metadata: address.metadata,
				}, transaction)
			}

			throw err
		})
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(
		changer_user_id: number,
		order_id: number,
		update: Partial<OrderInterface>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.renew('OrderRecord', order_id, update, {
			transaction,
		}).then(isUpdated => {
			if(isUpdated) {
				this.save('HistoryOrderRecord', {
					changer_user_id,
					order_id,
					status: update.status,
					note,
					cancelled_at: update.cancelled_at,
				})
			}

			return isUpdated
		})
	}

	@RepositoryModel.bound
	async updateRequest(
		order_request_id: number,
		update: Partial<Parameter<OrderRequestInterface>>,
		transaction: EntityManager,
	) {
		return this.renew('OrderRequestRecord', order_request_id, update, {
			transaction,
		})
	}

	@RepositoryModel.bound
	async updateAddress(
		order_id: number,
		order_address_id: number,
		update: Partial<Parameter<OrderAddressInterface>>,
		transaction: EntityManager,
	) {
		return this.renew('OrderAddressRecord', `id = :id AND order_id = :order_id`, update, {
			parameters: {
				id: order_address_id,
				order_id,
			},
			transaction,
		})
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getOrderDetailById(
		order_id: number,
		transaction: EntityManager,
	) {
		let Q = this.query('app.order', 'order', transaction)
		.leftJoinAndMapMany('order.details', OrderDetailRecord, 'orderDetail', `orderDetail.order_id = order.id and orderDetail.status not in ('EXCEPTION')`)

		// get stylesheet if order from matchbox
		.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\' and si.deleted_at is null')
		.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')
		.where('order.id = :order_id', { order_id })

		// get item if order from variant
		.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

		// get variant
		.leftJoinAndMapOne('orderDetail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_inventory.variant_id, si_inventory.variant_id)')
		.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(od_inventory.id, si_inventory.id)')

		return Q.getOne()
		.then(this.parser)
	}

	@RepositoryModel.bound
	async getOrdersByUserId(
		user_id: number,
		filter: {
			offset?: number,
			limit?: number,
			status?: ORDERS,
			blacklist?: string,
		} = {},
		transaction: EntityManager,
	) {
		let Q = this.query('app.order', 'order', transaction)
		.leftJoinAndSelect('order.orderDetails', 'orderDetails')
		.leftJoinAndSelect('order.orderRequests', 'orderRequests')
		.orderBy('order.created_at', 'DESC')

		if(filter.status) {
			Q = Q.where('order.status = :status', { status: filter.status })
		} else {
			Q = Q.where("order.status in ('RESOLVED')")
		}

		Q = Q.andWhere('order.user_id = :user_id', { user_id })

		if(filter.offset) {
			Q = Q.skip(filter.offset)
		}

		if(filter.limit) {
			Q = Q.take(filter.limit)
		}

		return Q
		.getManyAndCount()
		.then(this.parseCount)
		.then(this.parser) as any
	}

	@RepositoryModel.bound
	async filter(
		offset: number = 0,
		limit: number = 12,
		filter: {
			search?: string,
			status?: ORDERS,
			date?: Date,
			shipment_date?: Date,
			with_coupon?: boolean,
			with_feedback?: boolean,
			campaign_id?: number,
		} = {},
		transaction: EntityManager,
	): Promise<{
		data: Array<OrderInterface & {
			user: UserInterface & {
				profile: UserProfileInterface,
			},
			orderDetails: Array<OrderDetailInterface & {
				coupon: CouponInterface,
				feedback?: FeedbackInterface & {
					answers: FeedbackAnswerInterface[],
				},
				orderDetailCampaigns: OrderDetailCampaignInterface[],
			}>,
			orderRequests: OrderRequestInterface[],
			campaigns: CampaignInterface[],
		}>,
		count: number,
	}> {

		let Q = this.query('app.order', 'order', transaction)
			.leftJoinAndSelect('order.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('order.orderDetails', 'orderDetails')
			.leftJoinAndSelect('order.orderRequests', 'orderRequests')
			.orderBy('order.created_at', 'DESC')

		if (filter.status) {
			Q = Q.where('order.status = :status', { status: filter.status })
		} else {
			Q = Q.where('order.status IS NOT NULL')
		}

		if (filter.search) {
			Q = Q.andWhere('( order.number ILIKE :search OR profile.first_name || \' \' || profile.last_name ILIKE :search OR user.email ILIKE :search )', { search: `%${ filter.search }%` })
		}

		if (filter.date) {
			Q = Q.andWhere('order.created_at BETWEEN :start AND :end', {
				start: TimeHelper.moment(filter.date).startOf('d').toDate(),
				end: TimeHelper.moment(filter.date).endOf('d').toDate(),
			})
		}

		if (filter.shipment_date) {
			Q = Q.andWhere('orderDetails.shipment_at BETWEEN :s_start AND :s_end', {
				s_start: TimeHelper.moment(filter.shipment_date).startOf('d').toDate(),
				s_end: TimeHelper.moment(filter.shipment_date).endOf('d').toDate(),
			})
		}

		if (filter.with_coupon) {
			Q = Q.leftJoin(OrderDetailCouponRecord, 'oDCoupon', 'oDCoupon.order_detail_id = orderDetails.id')
				.leftJoinAndMapOne('orderDetails.coupon', CouponRecord, 'coupon', 'coupon.id = oDCoupon.coupon_id')
		}

		if (filter.with_feedback) {
			Q = Q.leftJoinAndMapOne('orderDetails.feedback', FeedbackRecord, 'feedback', 'feedback.order_detail_id = orderDetails.id and feedback.user_id = order.user_id')
				.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'feedbackAnswers', 'feedbackAnswers.feedback_id = feedback.id')
		}

		if (filter.campaign_id) {
			Q = Q.leftJoinAndSelect('orderDetails.orderDetailCampaigns', 'orderDetailCampaigns')
				.leftJoinAndMapMany('order.campaigns', CampaignRecord, 'campaigns', 'campaigns.id = orderDetailCampaigns.campaign_id')
				.andWhere('campaigns.id = :campaign_id', { campaign_id: filter.campaign_id })
		}

		return Q
			.skip(offset)
			.take(limit)
			.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any

	}

	async get(id: number | string, deep: false | undefined, transaction: EntityManager): Promise<OrderInterface>
	async get(id: number | string, deep: true | undefined, transaction: EntityManager): Promise<OrderInterface & {
		orderDetails: Array<OrderDetailInterface & {
			shipments: ShipmentInterface[],
		}>,
	}>

	@RepositoryModel.bound
	async get(
		id: number | string,
		deep?: boolean,
		transaction?: EntityManager,
	): Promise<OrderInterface> {
		const Q = this.query('app.order', 'order', transaction)
			.where(typeof id === 'number' ? 'order.id = :id' : 'order.number = :id', { id })

		if(deep) {
			return Q
				.leftJoinAndSelect('order.orderDetails', 'orderDetails')
				.leftJoin('orderDetails.orderDetailShipments', 'orderDetailShipments')
				.leftJoinAndMapMany('orderDetails.shipments', ShipmentRecord, 'shipment', 'shipment.id = orderDetailShipments.shipment_id')
				.orderBy('shipment.created_at', 'ASC')
				.getOne()
				.then(this.parser) as any
		} else {
			return Q
				.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getCountInCurrentMinute(
		transaction: EntityManager,
		startMinute: Date = TimeHelper.moment().startOf('m').toDate(),
		endMinute: Date = TimeHelper.moment().startOf('m').toDate(),
	): Promise<number> {
		return this.query('app.order', 'order', transaction)
			.select('COUNT(DISTINCT(id))')
			.where(`order.created_at BETWEEN '${startMinute.toISOString()}' AND '${endMinute.toISOString()}'`)
			.getCount()
			.then(this.parser)
	}

	async getDetail(order_id: number | string, config: { user_id?: number, shipment?: boolean, wallet?: boolean, with_item?: boolean, ascending?: boolean } | undefined, transaction: EntityManager): Promise<OrderInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		addresses: OrderAddressInterface[],
		orderPayments: OrderPaymentInterface[],
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
		shipments: Array<ShipmentInterface & {
			addresses: ShipmentAddressInterface[],
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
			processing: HistoryShipmentInterface,
			delivered: HistoryShipmentInterface,
		}>,
		points: UserPointTransactionInterface[],
		wallets: UserWalletTransactionInterface[],
		orderDetails: Array<OrderDetailInterface & {
			orderRequest: OrderRequestInterface,
			voucher?: VoucherInterface,
			matchbox?: MatchboxInterface,
			service?: ServiceInterface,
			product?: ProductInterface,
			transaction?: UserWalletTransactionInterface,
			topup?: TopupAmountInterface,
			brand?: BrandInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
			orderDetailShipments: ShipmentOrderDetailInterface[],
		}>,
	}>
	async getDetail(order_id: number | string, config: { user_id?: number, shipment: false, wallet: false, with_item?: boolean, ascending?: boolean }, transaction: EntityManager): Promise<OrderInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		addresses: OrderAddressInterface[],
		orderPayments: OrderPaymentInterface[],
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
		orderDetails: Array<OrderDetailInterface & {
			orderRequest: OrderRequestInterface,
			voucher?: VoucherInterface,
			matchbox?: MatchboxInterface,
			service?: ServiceInterface,
			product?: ProductInterface,
			transaction?: UserWalletTransactionInterface,
			topup?: TopupAmountInterface,
			brand?: BrandInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
		}>,
	}>
	async getDetail(order_ids: Array<number | string>, config: { user_id?: number, shipment?: boolean, wallet?: boolean, with_item?: boolean, ascending?: boolean } | undefined, transaction: EntityManager): Promise<Array<OrderInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		addresses: OrderAddressInterface[],
		orderPayments: OrderPaymentInterface[],
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
		shipments: Array<ShipmentInterface & {
			addresses: ShipmentAddressInterface[],
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
			processing: HistoryShipmentInterface,
			delivered: HistoryShipmentInterface,
		}>,
		points: UserPointTransactionInterface[],
		wallets: UserWalletTransactionInterface[],
		orderDetails: Array<OrderDetailInterface & {
			orderRequest: OrderRequestInterface,
			voucher?: VoucherInterface,
			matchbox?: MatchboxInterface,
			service?: ServiceInterface,
			product?: ProductInterface,
			transaction?: UserWalletTransactionInterface,
			topup?: TopupAmountInterface,
			brand?: BrandInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
			orderDetailShipments: ShipmentOrderDetailInterface[],
		}>,
	}>>
	async getDetail(order_ids: Array<number | string>, config: { user_id?: number, shipment: false, wallet: false, with_item?: boolean, ascending?: boolean }, transaction: EntityManager): Promise<Array<OrderInterface & {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		addresses: OrderAddressInterface[],
		orderPayments: OrderPaymentInterface[],
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
		orderDetails: Array<OrderDetailInterface & {
			orderRequest: OrderRequestInterface,
			voucher?: VoucherInterface,
			matchbox?: MatchboxInterface,
			service?: ServiceInterface,
			product?: ProductInterface,
			transaction?: UserWalletTransactionInterface,
			topup?: TopupAmountInterface,
			brand?: BrandInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
		}>,
	}>>

	@RepositoryModel.bound
	async getDetail(
		orderIdOrIds: number | string | Array<number | string>,
		query: {
			user_id?: number,
			shipment?: boolean,
			ascending?: boolean,
			points?: boolean,
			wallet?: boolean,
			with_item?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<any> {
		const isArray = Array.isArray(orderIdOrIds)
		const includeShipment = query.shipment === false ? false : true
		const includePoint = query.wallet === false ? false : true
		const includeWallet = query.wallet === false ? false : true

		let Q = this.query('app.order', 'order', transaction)
			.addSelect('order.updated_at')
			.leftJoinAndSelect('order.user', 'user')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndSelect('order.addresses', 'orderAddresses')
			.leftJoinAndSelect('order.orderPayments', 'orderPayments')
			.leftJoinAndSelect('order.orderDetails', 'orderDetails')
			.leftJoinAndSelect('orderDetails.orderRequest', 'orderDetailRequest')
			.leftJoinAndSelect('orderDetails.orderDetailCampaigns', 'orderDetailCampaigns')
			.leftJoinAndSelect('orderDetails.orderDetailCoupons', 'orderDetailCoupons')
			.leftJoinAndSelect('orderDetails.orderDetailTransactions', 'orderDetailTransactions')
			.where(() => {
				if(isArray) {
					return `order.${typeof orderIdOrIds[0] === 'number' ? 'id' : 'number'} IN (:...orderIdOrIds)`
				} else {
					return `order.${typeof orderIdOrIds === 'number' ? 'id' : 'number'} = :orderIdOrIds`
				}
			}, { orderIdOrIds })
			.orderBy('order.updated_at', query.ascending ? 'ASC' : 'DESC')
			.addOrderBy('orderPayments.created_at', 'ASC')

		if (query.user_id) {
			Q = Q.andWhere('order.user_id = :user_id', { user_id: query.user_id })
		}

		if (query.with_item) {
			Q = Q.leftJoinAndMapOne('orderDetails.voucher', VoucherRecord, 'voucher', 'voucher.id = orderDetails.ref_id AND orderDetails.type = :od_voucher', { od_voucher: ORDER_DETAILS.VOUCHER })
				.leftJoin(VoucherRecord, 'rvoucher', 'rvoucher.id = orderDetailRequest.ref_id AND orderDetailRequest.type = :or_voucher', { or_voucher: ORDER_REQUESTS.VOUCHER })
				.leftJoin(InventoryRecord, 'vinv', 'vinv.id = voucher.ref_id AND voucher.type = :voucher_inventory', { voucher_inventory: VOUCHERS.INVENTORY })
				.leftJoin('vinv.variant', 'vivariant')
				// .leftJoin(VariantRecord, 'vivariant', `invariant.id = orderDetails.ref_id and orderDetails.type = 'VARIANT'`)
				.leftJoin(StylesheetRecord, 'ss', 'ss.id = orderDetails.ref_id AND orderDetails.type = :od_stylesheet', { od_stylesheet: ORDER_DETAILS.STYLESHEET })
				.leftJoinAndMapOne('orderDetails._sb', StyleboardRecord, 'sb', 'sb.id = orderDetails.ref_id AND orderDetails.type = :od_styleboard', { od_styleboard: ORDER_DETAILS.STYLEBOARD })
				// .leftJoin(InventoryRecord, 'inv', 'inv.id = orderDetails.ref_id AND orderDetails.type = :od_inventory', { od_inventory: ORDER_DETAILS.INVENTORY })
				// .leftJoin('inv.variant', 'invariant')
				.leftJoin(VariantRecord, 'invariant', `invariant.id = orderDetailRequest.ref_id and orderDetailRequest.type = 'VARIANT'`)

				.leftJoinAndMapOne('orderDetails.matchbox', MatchboxRecord, 'matchbox', `
					CASE
						WHEN ss.id IS NOT NULL THEN matchbox.id = ss.matchbox_id
						WHEN voucher.type = :v_matchbox THEN matchbox.id = voucher.ref_id
						WHEN ss.id IS NULL AND orderDetailRequest.type = :or_matchbox THEN matchbox.id = orderDetailRequest.ref_id
						WHEN ss.id IS NULL AND orderDetailRequest.type = :or_styleboard THEN matchbox.id = :session_result
						WHEN ss.id IS NULL AND rvoucher IS NOT NULL AND rvoucher.type = :v_matchbox THEN matchbox.id = rvoucher.ref_id
					END
				`, { or_matchbox: ORDER_REQUESTS.MATCHBOX, v_matchbox: VOUCHERS.MATCHBOX, or_styleboard: ORDER_REQUESTS.STYLEBOARD, session_result: DEFAULTS.STYLING_RESULT_MATCHBOX_ID })

				.leftJoinAndMapOne('orderDetails.service', ServiceRecord, 'service', `(
					CASE
						WHEN sb.id IS NOT NULL THEN service.id = sb.service_id
						WHEN voucher.type = :v_service THEN service.id = voucher.ref_id
						WHEN sb.id IS NULL AND orderDetailRequest.type = :or_service THEN service.id = orderDetailRequest.ref_id
						WHEN sb.id IS NULL AND rvoucher IS NOT NULL AND rvoucher.type = :v_service THEN service.id = rvoucher.ref_id
					END
				)`, { or_service: ORDER_REQUESTS.SERVICE, v_service: VOUCHERS.SERVICE })

				.leftJoinAndMapOne('orderDetails.product', ProductRecord, 'product', `product.id = coalesce(invariant.product_id, vivariant.product_id)`)

				.leftJoinAndMapOne('orderDetails.transaction', UserWalletTransactionRecord, 'transaction', 'transaction.id = orderDetails.ref_id AND orderDetails.type = :od_transaction', { od_transaction: ORDER_DETAILS.WALLET_TRANSACTION })
				.leftJoinAndMapOne('orderDetails.topup', TopupAmountRecord, 'topup', 'topup.id = voucher.ref_id AND voucher.type = :voucher_topup', { voucher_topup: VOUCHERS.TOPUP_AMOUNT })

				.leftJoinAndMapOne('orderDetails.brand', BrandRecord, 'brand', `brand.id = coalesce(matchbox.brand_id, service.brand_id, product.brand_id, :yuna_brand_id)`, { yuna_brand_id: DEFAULTS.YUNA_BRAND_ID })
		}

		if(includeShipment) {
			Q = Q
				.leftJoinAndSelect('orderDetails.orderDetailShipments', 'orderDetailShipments')
				.leftJoinAndMapMany('order.shipments', ShipmentRecord, 'shipments', 'shipments.id = orderDetailShipments.shipment_id')
				.leftJoinAndSelect('shipments.shipmentCampaigns', 'shipmentCampaigns')
				.leftJoinAndSelect('shipments.shipmentCoupons', 'shipmentCoupons')
				.leftJoinAndMapOne('shipments.processing', HistoryShipmentRecord, 'processing', `processing.shipment_id = shipments.id AND processing.status = 'PROCESSING'`)
				.leftJoinAndMapOne('shipments.delivered', HistoryShipmentRecord, 'delivered', 'delivered.shipment_id = shipments.id AND delivered.status = :delivered', { delivered: SHIPMENT_STATUSES.DELIVERED })
				.leftJoinAndMapMany('shipments.addresses', ShipmentAddressRecord, 'shipmentAddress', 'shipmentAddress.id = shipments.destination_id')
				.addOrderBy('shipments.created_at', 'DESC')
				// .leftJoinAndSelect('shipment.destination', 'destination')
		}

		if (includeWallet) {
			Q = Q
				.leftJoinAndMapMany('order.wallets', UserWalletTransactionRecord, 'wallets', 'wallets.source = :order_source AND wallets.ref_id = order.id', { order_source: STATEMENT_SOURCES.ORDER })
				.addOrderBy('wallets.created_at', 'DESC')
			// .leftJoinAndSelect('shipment.destination', 'destination')
		}

		if (includePoint) {
			Q = Q
				.leftJoinAndMapMany('order.points', UserPointTransactionRecord, 'point', 'point.source = :order_source AND point.ref_id = order.id', { order_source: STATEMENT_SOURCES.ORDER })
				.addOrderBy('point.created_at', 'DESC')
			// .leftJoinAndSelect('shipment.destination', 'destination')
		}

		Q = Q.leftJoinAndMapMany('order.campaigns', CampaignRecord, 'campaigns', `campaigns.id = orderDetailCampaigns.campaign_id${ includeShipment ? ' OR campaigns.id = shipmentCampaigns.campaign_id' : '' }`)
			.leftJoinAndMapMany('order.coupons', CouponRecord, 'coupons', `coupons.id = orderDetailCoupons.coupon_id${ includeShipment ? ' OR coupons.id = shipmentCoupons.coupon_id' : '' }`)

		if(isArray) {
			return Q.getMany()
				.then(this.parser) as any
		} else {
			return Q.getOne()
				.then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getDetailHistory(
		order_id: number,
		transaction: EntityManager,
	): Promise<OrderInterface & {
		orderPayments: OrderPaymentInterface[],
		orderDetails: Array<OrderDetailInterface & {
			styleboard: StyleboardInterface & {
				histories: HistoryStyleboardInterface[],
			},
			stylesheet: StylesheetInterface & {
				histories: HistoryStylesheetInterface[],
			},
			histories: HistoryOrderDetailInterface[],
		}>,
		shipments: Array<ShipmentInterface & {
			histories: HistoryShipmentInterface[],
		}>,
		histories: HistoryOrderInterface[],
	}> {
		return this.query('app.order', 'order', transaction)
			.leftJoinAndSelect('order.orderPayments', 'payments')
			.leftJoinAndSelect('order.orderDetails', 'details')
			.leftJoinAndMapOne('order.shipmentOrderDetails', ShipmentOrderDetailRecord, 'detailShipments', 'details.id = detailShipments.order_detail_id')
			.leftJoinAndMapMany('order.shipments', ShipmentRecord, 'shipment', 'shipment.id = detailShipments.shipment_id AND shipment.is_facade = FALSE')
			.leftJoinAndMapOne('details.styleboard', StyleboardRecord, 'styleboard', 'styleboard.id = details.ref_id AND details.type = :styleboard_type', { styleboard_type: ORDER_DETAILS.STYLEBOARD })
			.leftJoinAndMapOne('details.stylesheet', StylesheetRecord, 'stylesheet', 'stylesheet.id = details.ref_id AND details.type = :stylesheet_type', { stylesheet_type: ORDER_DETAILS.STYLESHEET })
			.leftJoinAndMapMany('order.histories', HistoryOrderRecord, 'oh', 'oh.order_id = order.id')
			.leftJoinAndMapMany('details.histories', HistoryOrderDetailRecord, 'odh', 'odh.order_detail_id = details.id')
			.leftJoinAndMapMany('shipment.histories', HistoryShipmentRecord, 'sh', 'sh.shipment_id = shipment.id')
			.leftJoinAndMapMany('styleboard.histories', HistoryStyleboardRecord, 'sbh', 'sbh.styleboard_id = styleboard.id')
			.leftJoinAndMapMany('stylesheet.histories', HistoryStylesheetRecord, 'sth', 'sth.stylesheet_id = stylesheet.id')
			.where('order.id = :order_id', { order_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailRelatedToPrices(order_id: number, transaction: EntityManager): Promise<OrderInterface & {
		orderDetails: Array<OrderDetailInterface & {
			orderRequest: OrderRequestInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
			orderDetailShipments: ShipmentOrderDetailInterface[],
		}>,
		campaigns: CampaignInterface[],
		coupons: CouponInterface[],
		points: UserPointTransactionInterface[],
		wallets: UserWalletTransactionInterface[],
		shipments: Array<ShipmentInterface & {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		}>,
	}> {
		return this.query('app.order', 'order', transaction)
			.addSelect('order.updated_at')

			.leftJoinAndSelect('order.orderDetails', 'orderDetails')
			.leftJoinAndSelect('orderDetails.orderRequest', 'orderDetailRequest')
			.leftJoinAndSelect('orderDetails.orderDetailCampaigns', 'orderDetailCampaigns')
			.leftJoinAndSelect('orderDetails.orderDetailCoupons', 'orderDetailCoupons')
			.leftJoinAndSelect('orderDetails.orderDetailTransactions', 'orderDetailTransactions')
			.leftJoinAndSelect('orderDetails.orderDetailShipments', 'orderDetailShipments')
			.leftJoinAndMapMany('order.campaigns', CampaignRecord, 'campaigns', 'campaigns.id = orderDetailCampaigns.campaign_id')
			.leftJoinAndMapMany('order.coupons', CouponRecord, 'coupons', 'coupons.id = orderDetailCoupons.coupon_id')
			.leftJoinAndMapMany('order.points', UserPointTransactionRecord, 'points', 'points.source = :order_source AND points.ref_id = order.id', { order_source: STATEMENT_SOURCES.ORDER })
			.leftJoinAndMapMany('order.wallets', UserWalletTransactionRecord, 'wallets', 'wallets.source = :order_source AND wallets.ref_id = order.id', { order_source: STATEMENT_SOURCES.ORDER })
			.leftJoinAndMapMany('order.shipments', ShipmentRecord, 'shipments', 'shipments.id = orderDetailShipments.shipment_id')
			.leftJoinAndSelect('shipments.shipmentCampaigns', 'shipmentCampaigns')
			.leftJoinAndSelect('shipments.shipmentCoupons', 'shipmentCoupons')
			.where('order.id = :order_id', { order_id })
			.addOrderBy('shipments.created_at', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getStatus(
		order_id: number,
		transaction: EntityManager,
	): Promise<ORDERS> {
		return this.getOne('OrderRecord', {
			id: order_id,
		}, {
			select: ['status'],
			transaction,
		}).then(order => order.status)
	}

	@RepositoryModel.bound
	async getUser(
		order_id: number,
		transaction: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
	}> {
		return this.query('app.user', 'user', transaction)
			.innerJoin('user.orders', 'order', 'order.id = :order_id', { order_id })
			.leftJoinAndSelect('user.profile', 'profile')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderFeedback(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.order', 'order', transaction)
			.where('order.id = :order_id', { order_id })
			.leftJoinAndMapOne('order.user', UserRecord, 'user', 'user.id = order.user_id')
			.leftJoinAndSelect('user.profile', 'profile')
			.leftJoinAndMapMany('user.addresses', UserAddressRecord, 'address', 'address.user_id = user.id')
			.leftJoinAndMapMany('order.details', OrderDetailRecord, 'orderDetail', 'orderDetail.order_id = order.id')
			// .leftJoin(InventoryRecord, 'inventory', `inventory.id = orderDetail.ref_id AND orderDetail.type = 'INVENTORY'`)
			// .leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\'')
			// .leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')
			// .leftJoinAndMapMany('orderDetail.variant', VariantRecord, 'variant', `
			// 		CASE
			// 			WHEN orderDetail.type = 'INVENTORY' THEN variant.id = inventory.variant_id
			// 			WHEN orderDetail.type = 'STYLESHEET' THEN variant.id = si_inventory.variant_id
			// 		END
			// 	`)
			// .leftJoinAndMapOne('variant.inventory', InventoryRecord, 'vI', `
			// 		CASE
			// 			WHEN orderDetail.type = 'INVENTORY' THEN vI.id = inventory.id and vI.variant_id = variant.id
			// 			WHEN orderDetail.type = 'STYLESHEET' THEN vI.id = si_inventory.id and vI.variant_id = variant.id
			// 		END
			// 	`)
			// .leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			// .leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			// .leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			// .leftJoin(query => {
			// 	return query.from(VariantColorRecord, 'variant_color')
			// 		.orderBy('variant_color.order', 'ASC')
			// }, 'vc', 'vc.variant_id = variant.id')
			// .leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			// .leftJoin(query => {
			// 	return query.from(VariantAssetRecord, '_variant_asset')
			// 		.orderBy('_variant_asset.order', 'ASC')
			// 		.addOrderBy('_variant_asset.id', 'ASC')
			// }, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			// .leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
			// .leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = orderDetail.id AND feedback.ref_id = variant.id AND feedback.type = 'VARIANT'`)
			// .leftJoinAndMapOne('orderDetail.stylesheet_feedback', FeedbackRecord, 's_feedback', `s_feedback.order_detail_id = orderDetail.id AND s_feedback.ref_id = si.stylesheet_id AND s_feedback.type = 'STYLESHEET'`)
			// .leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			// .leftJoinAndMapMany('s_feedback.answers', FeedbackAnswerRecord, 's_answer', 's_answer.feedback_id = s_feedback.id')
			// .leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
			// .leftJoinAndMapOne('s_answer.question', QuestionRecord, 's_question', 's_question.id = s_answer.question_id')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderFeedbackVariant(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.where('orderDetail.id = :order_detail_id', { order_detail_id })
			.leftJoin(InventoryRecord, 'inventory', `inventory.id = orderDetail.ref_id AND orderDetail.type = 'INVENTORY'`)
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\'')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')
			.leftJoinAndMapMany('orderDetail.variant', VariantRecord, 'variant', `variant.id = inventory.variant_id`)
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'vI', `vI.id = inventory.id and vI.variant_id = variant.id`)
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
			.leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = orderDetail.id AND feedback.ref_id = variant.id AND feedback.type = 'VARIANT'`)
			.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderFeedbackStylesheet(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.order_detail', 'orderDetail', transaction)
			.where('orderDetail.id = :order_detail_id', { order_detail_id })
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\'')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')
			.leftJoinAndMapMany('orderDetail.variant', VariantRecord, 'variant', `variant.id = si_inventory.variant_id`)
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'vI', `vI.id = si_inventory.id and vI.variant_id = variant.id`)
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
			.leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = orderDetail.id AND feedback.ref_id = variant.id AND feedback.type = 'VARIANT'`)
			.leftJoinAndMapOne('orderDetail.stylesheet_feedback', FeedbackRecord, 's_feedback', `s_feedback.order_detail_id = orderDetail.id AND s_feedback.ref_id = si.stylesheet_id AND s_feedback.type = 'STYLESHEET'`)
			.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			.leftJoinAndMapMany('s_feedback.answers', FeedbackAnswerRecord, 's_answer', 's_answer.feedback_id = s_feedback.id')
			.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
			.leftJoinAndMapOne('s_answer.question', QuestionRecord, 's_question', 's_question.id = s_answer.question_id')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getVariantForFeedback(
		order_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.query('app.order_request', 'or', transaction)
			.where('or.order_id = :order_id', { order_id })
			.leftJoinAndMapMany('or.variant', VariantRecord, 'variant', `or.type = 'VARIANT' AND variant.id = or.ref_id`)
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')
			.leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = :order_detail_id AND feedback.ref_id = variant.id AND feedback.type = 'VARIANT'`, { order_detail_id })
			.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserOrders(
		user_id: number,
		filter: {
			status_in?: ORDERS[],
		} = {},
		transaction: EntityManager,
	): Promise<Array<OrderInterface & {
		details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions: OrderDetailTransactionInterface[],
		}>,
		shipments: Array<ShipmentInterface & {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		}>
		points: UserPointTransactionInterface[],
		wallets: UserWalletTransactionInterface[],
	}>> {
		let Q = this.query('app.order', 'order', transaction)
			.leftJoinAndMapMany('order.details', OrderDetailRecord, 'orderDetail', 'orderDetail.order_id = order.id')
			.leftJoinAndMapOne('orderDetail.request', OrderRequestRecord, 'orderRequest', 'orderRequest.id = orderDetail.order_request_id')

			.leftJoinAndMapMany('orderDetail.orderDetailCampaigns', OrderDetailCampaignRecord, 'odCampaign', 'odCampaign.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailCoupons', OrderDetailCouponRecord, 'odCoupon', 'odCoupon.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailTransactions', OrderDetailTransactionRecord, 'odTransaction', 'odTransaction.order_detail_id = orderDetail.id')
			.leftJoin(ShipmentOrderDetailRecord, 'sod', 'sod.order_detail_id = orderDetail.id')

			.leftJoinAndMapMany('order.shipments', ShipmentRecord, 'shipment', 'shipment.id = sod.shipment_id')
			.leftJoinAndMapMany('shipment.shipmentCampaigns', ShipmentCampaignRecord, 'sCampaign', 'sCampaign.shipment_id = shipment.id')
			.leftJoinAndMapMany('shipment.shipmentCoupons', ShipmentCouponRecord, 'sCoupons', 'sCoupons.shipment_id = shipment.id')

			.leftJoinAndMapMany('order.wallets', UserWalletTransactionRecord, 'uwt', `uwt.source = 'ORDER' and uwt.ref_id = order.id`)
			.leftJoinAndMapMany('order.points', UserPointTransactionRecord, 'upt', `upt.source = 'ORDER' and upt.ref_id = order.id`)

			.where('order.user_id = :user_id', { user_id })
			.andWhere(`orderDetail.type not in ('STYLEBOARD')`)
			
			if (!!filter.status_in) {
				Q = Q.andWhere('order.status in (:...status_in)', {status_in: filter.status_in})
			} else {
				Q = Q.andWhere(`order.status not in ('EXPIRED', 'EXCEPTION')`)
			}

			return Q.orderBy('order.updated_at', 'DESC')
				.getMany()
				.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUserPaidOrderCount(
		user_id: number,
		transaction: EntityManager,
	): Promise<number> {
		return this.query('app.order_payment', 'payment', transaction)
			.leftJoinAndSelect('payment.order', 'order')
			.where('payment.code = \'SUCCESS\'')
			.andWhere('order.user_id = :user_id', { user_id })
			.groupBy('order.id')
			.getCount()
	}

	@RepositoryModel.bound
	async getUserCustomStylingOrder(
		user_id: number,
		transaction: EntityManager,
	): Promise<Array<OrderInterface & {
		orderDetails: Array<OrderDetailInterface & {
			styleboard: StyleboardInterface,
		}>,
	}>> {
		return this.query('app.order', 'order', transaction)
			.innerJoinAndSelect('order.orderDetails', 'detail', 'detail.type = :styleboard', {
				styleboard: ORDER_DETAILS.STYLEBOARD,
			})
			.innerJoinAndMapOne('detail.styleboard', StyleboardRecord, 'styleboard', 'styleboard.id = detail.ref_id')
			.where('order.user_id = :user_id', { user_id })
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getShallowWithDetails(
		order_id: number,
		transaction: EntityManager,
	): Promise<OrderInterface & {
		orderDetails: OrderDetailInterface[],
	}> {
		return this.query('app.order', 'order', transaction)
			.leftJoinAndSelect('order.orderDetails', 'orderDetails')
			.where('order.id = :order_id', { order_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderByMethods(
		user_id: number | undefined,
		agent: string | undefined,
		filter: {
			limit: number,
			offset: number,
		},
		transaction: EntityManager,
	): Promise<{
		data: Array<OrderInterface & {
			orderPayment: OrderPaymentInterface,
			user: UserInterface & {
				profile: UserProfileInterface,
			},
		}>,
		count: number,
	}> {
		let Q = this.query('app.order', 'order', transaction)

		if (agent) {
			Q = Q.leftJoinAndMapOne('order.orderPayment', OrderPaymentRecord, 'orderPayment', `orderPayment.order_id = order.id AND orderPayment.agent IN ('${agent}')`)
		} else {
			Q = Q.leftJoinAndMapOne('order.orderPayment', OrderPaymentRecord, 'orderPayment', `orderPayment.order_id = order.id`)
		}

		Q = Q.leftJoinAndMapOne('order.user', UserRecord, 'user', 'order.user_id = user.id')
		.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user.user_profile_id')
		if (user_id) {
			Q = Q.where('user_id = :user_id', {user_id})
			.andWhere(`order.status not in ('EXPIRED', 'EXCEPTION')`)
		} else {
			Q = Q.where(`order.status not in ('EXPIRED', 'EXCEPTION')`)
			.andWhere(`orderPayment.code not in ('FAILED', 'EXCEPTION')`)
		}

		if (filter.limit) {
			Q = Q.take(filter.limit)
		}
		if (filter.offset) {
			Q = Q.skip(filter.offset)
		}

		return Q
			.orderBy('order.id', 'DESC')
			.getManyAndCount()
			.then(this.parseCount)
			.then(this.parser) as any

	}

	@RepositoryModel.bound
	async getBy(
		number: string,
		transaction: EntityManager,
	): Promise<OrderInterface & {
		orderDetails: OrderDetailInterface[],
	}> {
		return this.query('app.order', 'order', transaction)
			.leftJoinAndSelect('order.orderDetails', 'orderDetails')
			.where('order.number = :number', { number })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPending<U extends boolean>(date: Date, {
		with_user,
	}: {
		with_user?: U,
	}, transaction: EntityManager): Promise<Array<OrderInterface & (U extends true ? {
		user: UserInterface & {
			profile: UserProfileInterface,
		},
	} : {})>> {
		let Q = this.query('app.order', 'order', transaction)
			.where('order.status = :status', { status: ORDERS.PENDING_PAYMENT })
			.andWhere('order.created_at <= :date', { date })

		if (with_user) {
			Q = Q.leftJoinAndSelect('order.user', 'user')
				.leftJoinAndSelect('user.profile', 'profile')
		}

		return Q.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLatestHistory(order_id: number, status: ORDERS[], transaction: EntityManager): Promise<HistoryOrderInterface> {
		return this.query('history.order', 'order', transaction)
			.where('order.status IN (:...status)', { status })
			.where('order.order_id = :order_id', { order_id })
			.orderBy('order.id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddressesByOrderDetailIds(
		order_detail_ids: number[],
		transaction: EntityManager,
	): Promise<OrderAddressInterface[]> {
		return this.query('app.order_address', 'address', transaction)
			.innerJoin('address.orderDetails', 'orderDetail', 'orderDetail.id IN (:...order_detail_ids)', { order_detail_ids })
			.orderBy('address.updated_at', 'DESC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderNumberAndUserFromOrderDetails(
		order_detail_ids: number[],
		transaction: EntityManager,
	): Promise<Array<Pick<OrderInterface, 'id' | 'number'> & {
		user: Pick<UserInterface, 'email' | 'id'> & {
			profile: Pick<UserProfileInterface, 'first_name' | 'last_name'>,
		},
	}>> {
		return this.query('app.order', 'order', transaction)
			.select(['order.id', 'order.number', 'user.id', 'user.email', 'profile.first_name', 'profile.last_name'])
			.innerJoin('order.orderDetails', 'orderDetail', 'orderDetail.id IN (:...order_detail_ids)', { order_detail_ids })
			.leftJoin('order.user', 'user')
			.leftJoin('user.profile', 'profile')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAddressesByIds(
		order_id: number,
		order_address_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<OrderAddressInterface[]> {
		const Q = this.query('app.order_address', 'address', transaction)
			.leftJoinAndSelect('address.location', 'location')
			.where('address.order_id = :order_id', { order_id })

		if(order_address_ids) {
			return Q.andWhere('address.id IN (:...order_address_ids)', { order_address_ids })
				.getMany()
				.then(this.parser) as any
		} else {
			return Q.getMany().then(this.parser) as any
		}
	}

	@RepositoryModel.bound
	async getForCountRefund(
		order_id: number,
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		variants: Array<VariantInterface & {
			inventory: InventoryInterface,
			quantity: number,
		}>,
		points: UserPointTransactionInterface[],
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		facade_shipment: ShipmentInterface,
	}>> {
		const Q = this.query('app.order_detail', 'orderDetail', transaction)
			.leftJoin(OrderRequestRecord, 'orderRequest', 'orderRequest.id = orderDetail.order_request_id')
			.where('orderDetail.order_id = :order_id', {order_id})

			// get stylesheet if order from matchbox
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\' and si.deleted_at is null')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')

			// get item if order from variant
			.leftJoin(VariantRecord, 'od_variant', `od_variant.id = orderRequest.ref_id and orderRequest.type = 'VARIANT'`)
			.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

			// get variant
			.leftJoinAndMapMany('orderDetail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_variant.id, si_inventory.variant_id)')
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(si_inventory.id, od_inventory.id)')
			.loadRelationCountAndMap('variant.quantity', 'variant.inventories', 'i', query => {
				return query.where(`i.deleted_at IS NULL AND i.status = 'AVAILABLE'`)
			})

			// get discount
			.leftJoinAndMapMany('orderDetail.orderDetailCampaigns', OrderDetailCampaignRecord, 'campaign', 'campaign.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailCoupons', OrderDetailCouponRecord, 'coupon', 'coupon.order_detail_id = orderDetail.id')

			// get shipment
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = orderDetail.id')
			.leftJoinAndMapOne('orderDetail.facade_shipment', ShipmentRecord, 'fshipment', 'fshipment.id = sOd.shipment_id and fshipment.is_facade is true and fshipment.is_return is false')

			// get user point
			.leftJoinAndMapMany('orderDetail.points', UserPointTransactionRecord, 'userPoint', `userPoint.ref_id = :order_id and userPoint.source = 'ORDER'`, {order_id})

		return Q.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
