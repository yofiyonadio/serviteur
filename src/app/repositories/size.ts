import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	SizeRecord,
} from 'energie/app/records'

import {
	SizeInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class SizeRepository extends RepositoryModel<{
	SizeRecord: SizeRecord,
}> {

	static __displayName = 'SizeRepository'

	protected records = {
		SizeRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		id: number,
		size_id: number,
		title: string,
		description: string,
		transaction: EntityManager,
	): Promise<boolean> {
		const size = await this.saveOrUpdate('SizeRecord', [{
			id,
			size_id,
			title,
			description,
		}], transaction)

		return size
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getAllIds(transaction: EntityManager): Promise<Array<{
		id: number,
		sizes: SizeInterface[],
	}>> {
		// cannot change, nested where
		return this.query('master.size', 'size', transaction)
			.select(['size.id'])
			.leftJoinAndSelect('size.sizes', 'sizes')
			.where('size.deleted_at IS NULL')
			.andWhere('sizes.deleted_at IS NULL')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByIds(ids: number[], transaction: EntityManager): Promise<SizeInterface[]> {
		return this.getMany('SizeRecord', {
			id: In(ids),
		}, { transaction }) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
