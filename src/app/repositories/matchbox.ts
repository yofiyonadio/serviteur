import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	MatchboxRecord,
	MatchboxAssetRecord,
} from 'energie/app/records'

import {
	// MatchboxAssetInterface,
	MatchboxInterface,
	BrandInterface,
	BrandAddressInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class MatchboxRepository extends RepositoryModel<{
	MatchboxRecord: MatchboxRecord,
	MatchboxAssetRecord: MatchboxAssetRecord,
}> {

	static __displayName = 'MatchboxRepository'

	protected records = {
		MatchboxRecord,
		MatchboxAssetRecord,
	}

	// ============================= INSERT =============================
@RepositoryModel.bound
	async insert(data: Partial<MatchboxInterface>, transaction: EntityManager): Promise<MatchboxInterface> {
		return this.save('MatchboxRecord', data, transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updatePacketId(matchbox_id_or_ids: number | number[], packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.queryUpdate<MatchboxInterface>('app.matchbox', transaction)
			.set({ packet_id })
			.where(Array.isArray(matchbox_id_or_ids) ? 'id IN (:...matchbox_id_or_ids)' : 'id = :matchbox_id_or_ids', { matchbox_id_or_ids })
			.execute()
			.then(() => true)
			.catch(() => false)
	}

	// ============================= GETTER =============================
	async getBy(type: 'id' | 'slug', id: number | string, include_address: true, transaction: EntityManager): Promise<MatchboxInterface & {
		brand: BrandInterface & {
			address: BrandAddressInterface,
		},
	}>
	async getBy(type: 'id' | 'slug', id: number | string, include_address: boolean | undefined, transaction: EntityManager): Promise<MatchboxInterface>

	@RepositoryModel.bound
	async getBy(
		type: 'id' | 'slug' = 'slug',
		id: number | string,
		include_address: boolean | undefined,
		transaction: EntityManager,
	): Promise<any> {
		let Q = this.query('app.matchbox', 'matchbox', transaction)
			.addSelect('matchbox.expired_at')
			.where(type === 'id' ? 'matchbox.id = :id' : 'matchbox.slug = :id', { id })

		if (include_address === true) {
			Q = Q
				.leftJoinAndSelect('matchbox.brand', 'brand')
				.leftJoinAndSelect('brand.address', 'address')
		}

		return Q
			.getOne()
			.then(this.parser) as any
	}

@RepositoryModel.bound
	async getBrand(matchbox_id: number, transaction: EntityManager): Promise<BrandInterface> {
		return this.query('app.brand', 'brand', transaction)
			.innerJoin('brand.matchboxes', 'matchbox', 'matchbox.id = :matchbox_id', { matchbox_id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getByTitle(title: string, transaction: EntityManager): Promise<Array<MatchboxInterface & {
		brand: BrandInterface,
		// asset: MatchboxAssetInterface,
	}>> {
		return this.query('app.matchbox', 'matchbox', transaction)
			.leftJoinAndSelect('matchbox.brand', 'brand')
			// .leftJoinAndMapOne('matchbox.asset', MatchboxAssetRecord, 'asset', 'asset.matchbox_id = matchbox.id AND asset.deleted_at IS NULL')
			.where(`matchbox.title ILIKE '%${title}%'`)
			// .orderBy('asset.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getPacketId(matchbox_id: number, transaction: EntityManager): Promise<number | null> {
		return this.getOne('MatchboxRecord', {
			id: matchbox_id,
		}, {
			select: ['packet_id'],
			transaction,
		}).then(data => data.packet_id)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
