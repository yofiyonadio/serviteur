import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	CampaignRecord, OrderDetailCampaignRecord, OrderDetailRecord, OrderPaymentRecord, OrderRecord,
} from 'energie/app/records'

import {
	CampaignInterface, OrderInterface, OrderPaymentInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class CampaignRepository extends RepositoryModel<{
	CampaignRecord: CampaignRecord,
}> {

	static __displayName = 'CampaignRepository'

	protected records = {
		CampaignRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(data: CampaignInterface[], transaction: EntityManager): Promise<boolean> {
		return this.saveOrUpdate('CampaignRecord', data, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async getAll(transaction: EntityManager): Promise<CampaignInterface[]> {
		return this.query('app.campaign', 'campaign', transaction)
			.where('campaign.published_at <= now()')
			.andWhere('(campaign.expired_at > now() OR campaign.expired_at IS NULL)')
			.addOrderBy('campaign.id', 'ASC')
			.getMany()
			.then() as any
	}

	async filter(transaction: EntityManager): Promise<CampaignInterface[]> {
		return this.query('app.campaign', 'campaign', transaction)
			.orderBy('campaign.id', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	async getBy(type: 'id', id: number, transaction: EntityManager): Promise<CampaignInterface>
	async getBy(type: 'slug', id: string, transaction: EntityManager): Promise<CampaignInterface>

	@RepositoryModel.bound
	async getBy(
		type: 'id' | 'slug',
		id: number | string,
		transaction?: EntityManager,
	): Promise<any> {
		return this.query('app.campaign', 'campaign', transaction)
			.where(type === 'id' ? 'campaign.id = :id' : 'campaign.slug = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getOrderHistory(
		campaign_id: number,
		user_id: number,
		transaction: EntityManager,
	): Promise<OrderInterface & {payment: OrderPaymentInterface}> {
		return this.query('app.campaign', 'campaign', transaction)
			.leftJoin(OrderDetailCampaignRecord, 'odc', 'odc.campaign_id = campaign.id')
			.leftJoin(OrderDetailRecord, 'od', 'od.id = odc.order_detail_id')
			.leftJoinAndMapOne('campaign.order', OrderRecord, 'order', 'order.id = od.order_id')
			.leftJoinAndMapOne('order.payment', OrderPaymentRecord, 'payment', `payment.order_id = order.id`)
			.where('campaign.id = :campaign_id', {campaign_id})
			.andWhere('order.user_id = :user_id', {user_id})
			.andWhere(`order.status not in ('EXPIRED', 'EXCEPTION')`)
			.andWhere(`payment.code not in ('FAILED', 'EXCEPTION')`)
			.orderBy('order.created_at', 'DESC')
			.getOne()
			.then((result: CampaignInterface & {
				order: OrderInterface & {
					payment: OrderPaymentInterface,
				},
			}) => {
				return result?.order ?? null
			})
	}

	getCampaignFromOrderDetailInput(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		const Q = this.query('app.campaign', 'campaign', transaction)
			.leftJoin('campaign.campaignOrderDetails', 'campaignOrderDetails')
			.where('campaignOrderDetails.order_detail_id = :orderDetailId', { orderDetailId: order_detail_id})

		return Q.getOne()
		.then(this.parser) as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
