import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
} from 'typeorm'

import {
	ClusterRecord, StylecardRecord, UserAssetRecord, UserProfileRecord, UserRecord,
} from 'energie/app/records'

import {
	ClusterInterface, StylecardInterface, UserAssetInterface, UserInterface, UserProfileInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'
import { isNullOrUndefined } from 'util'
import { STYLECARD_STATUSES } from 'energie/utils/constants/enum'


@EntityRepository()
export default class ClusterRepository extends RepositoryModel<{
	ClusterRecord: ClusterRecord,
}> {

	static __displayName = 'ClusterRepository'

	protected records = {
		ClusterRecord,
	}

	// ============================= INSERT =============================
	async insert(
		data: ClusterInterface,
		transaction: EntityManager,
	): Promise<ClusterInterface> {
		return this.save('ClusterRecord', data, transaction)
	}

	// ============================= UPDATE =============================
	async update(
		cluster_id: number,
		data: Parameter<ClusterInterface>,
		transaction: EntityManager,
	) {
		return this.renew('ClusterRecord', cluster_id, data, {transaction})
	}

	// ============================= GETTER =============================
	async filter(
		offset: number,
		limit: number,
		search: string | undefined,
		transaction: EntityManager,
	): Promise<{
		count: number,
		data: Array<ClusterInterface & {
			stylecards: StylecardInterface[],
		}>,
	}> {
		const Q = this.query('app.cluster', 'cluster', transaction)

		// if (with_stylecard) {
		// 	Q = Q.leftJoinAndMapMany('cluster.stylecards', StylecardRecord, 'stylecard', 'stylecard.cluster_id = cluster.id')
		// }

		if (search) {
			// if (search.match(/\[([a-z]*)\] +(.*)/)) {
			// 	const match = search.match(/\[([a-z]*)\] +(.*)/)
			// 	const key = match[1]
			// 	const _search = match[2]

			// 	Q = Q.andWhere(`(inventory.${key})::text ILIKE :search`, { search: _search })
			// } else {
			// 	Q = Q.andWhere(`(product.title ILIKE :search OR brand.title ILIKE :search)`, { search: `%${search}%` })
			// }
		}

		return Q
			.skip(offset)
			.take(limit)
			.orderBy('cluster.id', 'ASC')
			.getManyAndCount()
			.then(this.parser)
			.then(datas => {
				return {
					data: datas[0],
					count: datas[1],
				}
			})as any
	}

	async get(
		cluster_id: number | undefined,
		cluster_type: string | undefined,
		filter: {
			offset?: number,
			limit?: number,
		} = {},
		option: {
			type_no?: number,
			with_stylecard?: boolean,
			stylecard_status?: STYLECARD_STATUSES,
			limit?: number,
			by_published_date?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<ClusterInterface & {
		stylecards: Array<StylecardInterface & {
			stylist: UserInterface & {
				profile: UserProfileInterface,
			},
		}>,
	}> {
		const Q = this.query('app.cluster', 'cluster', transaction)
			.where('true')

		if (option.with_stylecard) {
			Q.leftJoin(sq => {
					const q = sq.subQuery()
						.select([
							'sc.id id',
							'sc.cluster_id cluster_id',
						])
						.from(StylecardRecord, 'sc')
						.leftJoin(ClusterRecord, 'c', 'c.id = sc.cluster_id')

						if (option.by_published_date) {
							q.orderBy('sc.updated_at', 'DESC')
						} else {
							q.orderBy('sc.id', 'DESC')
						}

						if (cluster_id && isNullOrUndefined(cluster_type)) {
							q.andWhere('c.id = :cluster_id', {cluster_id})
						} else if (cluster_type && isNullOrUndefined(cluster_id)) {
							q.andWhere('c.type = :cluster_type', {cluster_type})
								.andWhere('c.type_no = :type_no', {type_no: option.type_no})
						}

						if (filter.offset >= 0 && filter.limit >= 0) {
							q.offset(filter.offset).limit(filter.limit)
						}

						return q
				}, 'sq_sc', `"sq_sc"."cluster_id" = cluster.id`)
				.leftJoinAndMapMany('cluster.stylecards', StylecardRecord, 'stylecard', `stylecard.id = "sq_sc"."id"`)
				.leftJoinAndMapOne('stylecard.stylist', UserRecord, 'stylist', 'stylist.id = stylecard.stylist_id')
				.leftJoinAndMapOne('stylist.profile', UserProfileRecord, 'profile', 'profile.id = stylist.user_profile_id')
				.orderBy('stylecard.created_at', 'DESC')

			if (option.stylecard_status) {
				Q.andWhere(`stylecard.status = :status`, {status: option.stylecard_status})
			}
		}

		if (cluster_id && isNullOrUndefined(cluster_type)) {
			Q.andWhere('cluster.id = :cluster_id', {cluster_id})
		} else if (cluster_type && isNullOrUndefined(cluster_id)) {
			Q.andWhere('cluster.type = :cluster_type', {cluster_type})
				.andWhere('cluster.type_no = :type_no', {type_no: option.type_no})
		}

		return Q.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getUsers(
		cluster_id: number,
		transaction: EntityManager,
	): Promise<Array<UserInterface & {
		profile: UserProfileInterface,
		stylist: UserInterface & {
			profile: UserProfileInterface,
			asset: UserAssetInterface,
		},
	}>> {
		return this.query('app.user', 'user', transaction)
			.leftJoinAndMapOne('user.profile', UserProfileRecord, 'profile', 'profile.id = user_profile_id')
			.leftJoinAndMapOne('user.stylist', UserRecord, 'stylist', 'stylist.id = profile.stylist_id')
			.leftJoinAndMapOne('stylist.profile', UserProfileRecord, 'stylistProfile', 'stylistProfile.id = stylist.user_profile_id')
			.leftJoinAndMapOne('stylist.asset', UserAssetRecord, 'asset', 'asset.id = stylistProfile.asset_id')
			.where('profile.cluster_id = :cluster_id', {cluster_id})
			.getMany() as any
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================
}
