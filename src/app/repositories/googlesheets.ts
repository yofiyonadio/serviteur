import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'


@EntityRepository()
export default class GoogleSheetsRepository extends RepositoryModel {

	static __displayName = 'GoogleSheetsRepository'


	// ============================= INSERT =============================
	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	@RepositoryModel.bound
	async getData(query: string, transaction: EntityManager) {
		return await this.queryCustom(query, undefined, transaction)
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
