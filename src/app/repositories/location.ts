import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager } from 'typeorm'

import {
	LocationInterface,
} from 'energie/app/interfaces'

import {
	LocationRecord,
} from 'energie/app/records'


@EntityRepository()
export default class LocationRepository extends RepositoryModel<{
	LocationRecord: LocationRecord,
}> {

	static __displayName = 'LocationRepository'

	protected records = {
		LocationRecord,
	}

	// ============================= INSERT =============================
@RepositoryModel.bound
	async insert(locations: LocationInterface[], transaction: EntityManager): Promise<boolean> {
		return this.saveOrUpdate('LocationRecord', locations, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@RepositoryModel.bound
	async get(id: number, transaction: EntityManager): Promise<LocationInterface> {
		return this.query('master.location', 'location', transaction)
			.where('location.id = :id', { id })
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAll(transaction: EntityManager): Promise<LocationInterface[]> {
		return this.query('master.location', 'location', transaction)
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getProvinces(transaction: EntityManager): Promise<Array<{
		id: number,
		title: string,
	}>> {
		const manager = transaction || this.manager

		return manager
			.query('SELECT id, title FROM view.master_province ORDER BY title ASC')
			.then(this.parser)
	}

	@RepositoryModel.bound
	async getCities(province_id: number | undefined, transaction: EntityManager): Promise<Array<{
		id: number,
		title: string,
	}>> {
		const manager = transaction || this.manager

		return manager
			.query(`
				SELECT id, title
				FROM view.master_city
				${province_id !== undefined ? `WHERE province_id = ${province_id}` : ''}
				ORDER BY title ASC
			`)
			.then(this.parser)
	}

	@RepositoryModel.bound
	async getSuburbs(city_id: number | undefined, transaction: EntityManager): Promise<Array<{
		id: number,
		title: string,
		alias: string,
	}>> {
		const manager = transaction || this.manager

		return manager
			.query(`
				SELECT id, title
				FROM view.master_suburb
				${city_id !== undefined ? `WHERE city_id = ${city_id}` : ''}
				ORDER BY title ASC
			`)
			.then(this.parser)
	}

	@RepositoryModel.bound
	async getAreas(method: 'POSTAL' | 'SUBURB' | 'SEARCH', data: number | string | undefined, transaction: EntityManager): Promise<LocationInterface[]> {

		const Q = this.query('master.location', 'location', transaction)
			// .select(['location.id', 'location.area_name', 'location.suburb_name', 'location.city_name', 'location.province_name', 'location.zip_code'])
			.orderBy('location.area_name', 'ASC')

		if (method === 'POSTAL') {
			return Q.where('location.zip_code = :data', { data })
				.getMany()
				.then(this.parser) as any
		} else if (method === 'SUBURB') {
			return Q.where('location.suburb_id = :data', { data })
				.getMany()
				.then(this.parser) as any
		} else {
			return Q.where(`location.province_name ILIKE '%${ data }%' OR location.city_name ILIKE '%${ data }%' OR location.suburb_name ILIKE '%${ data }%' OR location.area_name ILIKE '%${ data }%' OR location.suburb_alias ILIKE '%${ data }%' OR location.area_alias ILIKE '%${ data }%'`)
				.getMany()
				.then(this.parser) as any
		}

	}

	@RepositoryModel.bound
	async getAny(transaction: EntityManager): Promise<{ id: number }> {
		return this.query('master.location', 'location', transaction)
			.select('location.id')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getLast(transaction: EntityManager): Promise<{ id: number }> {
		return this.query('master.location', 'location', transaction)
			.select('location.id')
			.orderBy('location.id', 'DESC')
			.getOne()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async count(transaction: EntityManager): Promise<number> {
		return this.query('master.location', 'location', transaction)
			.getCount()
	}

	@RepositoryModel.bound
	async getIds(transaction: EntityManager): Promise<number[]> {
		return this.query('master.location', 'location', transaction)
			.select('location.id')
			.getMany()
			.then(this.parser)
			.then((ds: LocationInterface[]) => ds.map(d => d.id))
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
