import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
} from 'typeorm'

import {
	BCATokenRecord,
} from 'energie/app/records'
import { BCATokenInterface } from 'energie/app/interfaces'


@EntityRepository()
export default class BCARepository extends RepositoryModel<{
	BCATokenRecord: BCATokenRecord,
}> {

	static __displayName = 'BCARepository'

	protected records = {
		BCATokenRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async upsert(
		cred: string,
		updated_at: Date,
		expired_at: Date,
		transaction: EntityManager,
	) {
		return transaction.createQueryBuilder()
			.insert().into('other.bca_token', ['id', 'token', 'updated_at', 'expired_at'])
			.values({
				id: 1,
				updated_at,
				token: cred,
				expired_at,
			})
			.orUpdate({
				overwrite: ['token', 'updated_at', 'expired_at'],
				conflict_target: ['id'],
			})
			.returning(['token'])
			.execute()
			.then(result => result.raw[0].token)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async getToken(transaction: EntityManager): Promise<BCATokenInterface> {
		return this.query('other.bca_token', 'bca', transaction)
			.getOne()
			.then(this.parser) as any
	}

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================
}
