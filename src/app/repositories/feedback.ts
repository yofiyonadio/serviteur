import RepositoryModel from 'app/models/repository'
import {
	EntityRepository,
	EntityManager,
} from 'typeorm'

import {
	FeedbackRecord,
	FeedbackAnswerRecord,
	VariantRecord,
	VariantAssetRecord,
	ColorRecord,
	StylesheetRecord,
	OrderDetailRecord,
	StylesheetInventoryRecord,
	InventoryRecord,
	ProductRecord,
	BrandRecord,
	SizeRecord,
	VariantColorRecord,
	ShipmentOrderDetailRecord,
	ShipmentRecord,
	OrderDetailCampaignRecord,
	OrderDetailCouponRecord,
	QuestionRecord,
	HistoryShipmentRecord,
	CategoryRecord,
	OrderRecord,
} from 'energie/app/records'

import {
	FeedbackAnswerInterface,
	FeedbackInterface,
	QuestionInterface,
	VariantInterface,
	VariantAssetInterface,
	ProductInterface,
	BrandInterface,
	SizeInterface,
	ColorInterface,
	StylesheetInventoryInterface,
	StylesheetInterface,
	InventoryInterface,
	OrderInterface,
	OrderDetailInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	ShipmentInterface,
	HistoryShipmentInterface,
	CategoryInterface,
} from 'energie/app/interfaces'

import { FEEDBACKS } from 'energie/utils/constants/enum'

import { Parameter } from 'types/common'

@EntityRepository()
export default class FeedbackRepository extends RepositoryModel<{
	FeedbackRecord: FeedbackRecord,
	FeedbackAnswerRecord: FeedbackAnswerRecord,
}> {

	static __displayName = 'FeedbackRepository'

	protected records = {
		FeedbackRecord,
		FeedbackAnswerRecord,
	}


	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(
		data: Parameter<FeedbackInterface>,
		answers: Array<Parameter<FeedbackAnswerInterface, 'feedback_id'>>,
		transaction: EntityManager,
	): Promise<FeedbackInterface & {
		answers: FeedbackAnswerInterface[],
	}> {
		return this.save('FeedbackRecord', data, transaction).then(async feedback => {
			return {
				...feedback,
				answers: await this.insertAnswer(feedback.id, answers, transaction),
			}
		})
	}

	@RepositoryModel.bound
	async insertAnswer(feedback_id: number, answers: Array<Parameter<FeedbackAnswerInterface, 'feedback_id'>>, transaction: EntityManager): Promise<FeedbackAnswerInterface[]> {
		return this.save('FeedbackAnswerRecord', await answers.map(answer => {
			return {
				feedback_id,
				question_id: answer.question_id,
				answer: answer.answer,
			}
		}), transaction)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async updateAnswer(
		feedback_id: number,
		answer: Partial<FeedbackAnswerInterface>,
		transaction: EntityManager,
	) {
		return this.queryUpdate('app.feedback_answer', transaction)
			.set(answer)
			.where('feedback_id = :feedback_id AND question_id = :question_id', {feedback_id, question_id: answer.question_id})
			.execute()
	}

	// ============================= GETTER =============================
	@RepositoryModel.bound
	// will rename to listUserFeedback
	async listUserFeedback<
		D extends boolean,
		Return extends OrderInterface & {
			details: Array<OrderDetailInterface & {
				variants: Array<VariantInterface & {
					inventory: InventoryInterface,
					product: ProductInterface,
					category: CategoryInterface,
					brand: BrandInterface,
					size: SizeInterface,
					colors: ColorInterface[],
					quantity: number,
					asset: VariantAssetInterface,
					feedback: FeedbackInterface & {
						answers: Array<FeedbackAnswerInterface & {
							question: QuestionInterface,
						}>,
					},
				}>
				orderDetailCampaigns: OrderDetailCampaignInterface[],
				orderDetailCoupons: OrderDetailCouponInterface[],
				facade_shipment: ShipmentInterface,
				shipment?: ShipmentInterface & {
					delivered: HistoryShipmentInterface,
				},
			}>,
		}>(
		user_id: number,
		option: {
			/** Detailed means only get one */
			detailed?: D,
			with_feedback?: boolean,
		} = {},
		filter: {
			offset?: number,
			limit?: number,
			order_detail_id?: number,
			order_detail_ids?: number[],
			empty_feedback?: boolean,
			history_only?: boolean,
			confirmed_date?: Date,
			order_detail_with_cashback?: boolean,
		} = {},
		transaction: EntityManager,
	): Promise<D extends true ? Return : Return[]> {
		let Q = this.query('app.order', 'order', transaction)
			.leftJoinAndMapMany('order.details', OrderDetailRecord, 'orderDetail', `orderDetail.order_id = order.id and orderDetail.status not in ('EXCEPTION')`)
			.where('true')
			.andWhere(`order.status = 'RESOLVED'`)

			// get additional detail
			.leftJoinAndMapMany('orderDetail.orderDetailCampaigns', OrderDetailCampaignRecord, 'campaign', 'campaign.order_detail_id = orderDetail.id')
			.leftJoinAndMapMany('orderDetail.orderDetailCoupons', OrderDetailCouponRecord, 'coupon', 'coupon.order_detail_id = orderDetail.id')

			// get shipment and delivered shipment history
			.leftJoin(ShipmentOrderDetailRecord, 'sOd', 'sOd.order_detail_id = orderDetail.id')
			.leftJoinAndMapOne('orderDetail.facade_shipment', ShipmentRecord, 'fshipment', 'fshipment.id = sOd.shipment_id and fshipment.is_facade is true and fshipment.is_return is false')
			.leftJoinAndMapOne('orderDetail.shipment', ShipmentRecord, 'shipment', 'shipment.id = sOd.shipment_id and shipment.is_facade is false and shipment.is_return is false')
			.leftJoinAndMapOne('shipment.delivered', HistoryShipmentRecord, 'delivered', `delivered.shipment_id = shipment.id and delivered.status = 'DELIVERED'`)

			// get stylesheet if order from matchbox
			.leftJoin(StylesheetInventoryRecord, 'si', 'si.stylesheet_id = orderDetail.ref_id and orderDetail.type = \'STYLESHEET\' and si.deleted_at is null')
			.leftJoin(InventoryRecord, 'si_inventory', 'si_inventory.id = si.inventory_id')

			// get item if order from variant
			.leftJoin(InventoryRecord, 'od_inventory', `od_inventory.id = orderDetail.ref_id and orderDetail.type = 'INVENTORY'`)

			// get variant
			.leftJoinAndMapMany('orderDetail.variants', VariantRecord, 'variant', 'variant.id = coalesce(od_inventory.variant_id, si_inventory.variant_id)')
			.leftJoinAndMapOne('variant.inventory', InventoryRecord, 'inventory', 'inventory.id = coalesce(od_inventory.id, si_inventory.id)')
			.leftJoinAndMapOne('variant.product', ProductRecord, 'product', 'product.id = variant.product_id')
			.leftJoin(sq => {
				return sq.subQuery()
					.select([
						`"type".id type_id`,
						`category.id category_id`,
						`child_category.id child_category_id`,
					])
					.from(CategoryRecord, `type`)
					.from(CategoryRecord, `category`)
					.from(CategoryRecord, `child_category`)
					.where(`"type".id = category.category_id`)
					.andWhere('category.id = child_category.category_id')
			}, 'vcategory', 'vcategory.child_category_id = product.category_id')
			.leftJoinAndMapOne('variant.category', CategoryRecord, 'category', 'category.id = vcategory.type_id')
			.leftJoinAndMapOne('variant.brand', BrandRecord, 'brand', 'brand.id = product.brand_id')
			.leftJoinAndMapOne('variant.size', SizeRecord, 'size', 'size.id = variant.size_id')
			.leftJoin(query => {
				return query.from(VariantColorRecord, 'variant_color')
					.orderBy('variant_color.order', 'ASC')
			}, 'vc', 'vc.variant_id = variant.id')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = vc.color_id')
			.loadRelationCountAndMap('variant.quantity', 'variant.inventories', 'inventory', query => {
				return query.where(`inventory.deleted_at IS NULL AND inventory.status = 'AVAILABLE'`)
			})
			.leftJoin(query => {
				return query.from(VariantAssetRecord, '_variant_asset')
					.orderBy('_variant_asset.order', 'ASC')
					.addOrderBy('_variant_asset.id', 'ASC')
			}, '_va', '_va.variant_id = variant.id AND _va.deleted_at IS NULL')
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'vasset', 'vasset.id = _va.id')

			if (!!user_id) {
				Q = Q.andWhere('order.user_id = :user_id', {user_id})
			}

			if (filter.order_detail_with_cashback) {
				Q = Q.andWhere(`(campaign.cashback > 0 or coupon.cashback > 0)`)
			}

			if (filter.confirmed_date) {
				Q = Q.andWhere('shipment.confirmed_at > :confirmed_date', { confirmed_date: filter.confirmed_date })
			}

			// only get order detail without feedback
			if (!option.with_feedback) {
				if (!!filter.empty_feedback) {
					Q = Q.andWhere(sq => {
						const sql = sq.subQuery()
							.select('1')
							.from(FeedbackRecord, 'f')
							.where('true')

							if (!!user_id) {
								sql.andWhere('f.user_id = :user_id', {user_id})
							}

							sql.andWhere('f.order_detail_id = orderDetail.id')
							.andWhere(`f.type = 'VARIANT'`)
							.andWhere('f.ref_id = variant.id')

						return 'not exists' + sql.getSql()
					})
				} else if (!!filter.history_only) {
					Q = Q.andWhere(sq => {
						const sql = sq.subQuery()
							.select('1')
							.from(FeedbackRecord, 'f')
							.where('true')

							if (!!user_id) {
								sql.andWhere('f.user_id = :user_id', {user_id})
							}

							sql.andWhere('f.order_detail_id = orderDetail.id')
							.andWhere(`f.type = 'VARIANT'`)
							.andWhere('f.ref_id = variant.id')
							.getSql()

							return 'exists' + sql.getSql()
					})
				}
			}

			if (!!filter.order_detail_id || !!filter.order_detail_ids) {
				// get feedback
				if (!!option.with_feedback && !filter.empty_feedback) {
					Q = Q.leftJoinAndMapOne('variant.feedback', FeedbackRecord, 'feedback', `feedback.order_detail_id = orderDetail.id and feedback.user_id = order.user_id and feedback.type = 'VARIANT' and feedback.ref_id = variant.id`)
					.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
					.leftJoinAndMapOne('answer.question', QuestionRecord, 'question', 'question.id = answer.question_id')
				}

				if (!!filter.order_detail_id && !filter.order_detail_ids) {
					Q = Q.andWhere('orderDetail.id = :order_detail_id', {order_detail_id: filter.order_detail_id})
				} else if (!!filter.order_detail_ids && !filter.order_detail_id) {
					Q = Q.andWhere('orderDetail.id in (:...order_detail_id)', {order_detail_ids: filter.order_detail_ids})
				}
			}

			if (!!filter.offset && !!filter.limit) {
				Q = Q.skip(filter.offset)
					.take(filter.limit)
			}

			if (!!option.detailed) {
				return Q.getOne()
					.then(this.parser) as any
			} else {
				return Q.getMany()
					.then(this.parser) as any
			}
	}

	@RepositoryModel.bound
	async getShallow(
		user_id: number,
		order_id: number,
		transaction: EntityManager,
	): Promise<Array<FeedbackInterface & {
		answers: FeedbackAnswerInterface[],
	}>> {
		return this.query('app.feedback', 'feedback', transaction)
			.leftJoinAndMapMany('feedback.answers', FeedbackAnswerRecord, 'answer', 'answer.feedback_id = feedback.id')
			.leftJoin(OrderDetailRecord, 'od', 'od.id = feedback.order_detail_id')
			.leftJoin(OrderRecord, 'o', 'o.id = od.order_id')
			.where('o.user_id = :user_id', {user_id})
			.andWhere('o.id = :order_id', {order_id})
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getDetailByOrderDetailId(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	): Promise<Array<FeedbackInterface & {
		variant?: VariantInterface & {
			asset: VariantAssetInterface,
			product: ProductInterface & {
				brand: BrandInterface,
			},
			size: SizeInterface,
			colors: ColorInterface[],
		}
		stylesheet?: StylesheetInterface & {
			stylesheetInventories: Array<StylesheetInventoryInterface & {
				inventory: InventoryInterface,
			}>,
		},
		answers: Array<FeedbackAnswerInterface & {
			question: QuestionInterface,
		}>,
	}>> {
		return this.query('app.feedback', 'feedback', transaction)
			.leftJoinAndMapOne('feedback.variant', VariantRecord, 'variant', 'feedback.type = :variant_feedback AND feedback.ref_id = variant.id', { variant_feedback: FEEDBACKS.VARIANT })
			.leftJoinAndMapOne('variant.asset', VariantAssetRecord, 'asset', 'asset.variant_id = variant.id')
			.leftJoinAndSelect('variant.product', 'product')
			.leftJoinAndSelect('variant.size', 'size')
			.leftJoin('variant.variantColors', 'variantColors')
			.leftJoinAndMapMany('variant.colors', ColorRecord, 'color', 'color.id = variantColors.color_id')
			.leftJoinAndSelect('product.brand', 'brand')
			.leftJoinAndMapOne('feedback.stylesheet', StylesheetRecord, 'stylesheet', 'feedback.type = :stylesheet_feedback AND feedback.ref_id = stylesheet.id', { stylesheet_feedback: FEEDBACKS.STYLESHEET })
			.leftJoinAndSelect('stylesheet.stylesheetInventories', 'sI', 'sI.deleted_at IS NULL')
			.leftJoinAndSelect('sI.inventory', 'inventory')
			.leftJoinAndSelect('feedback.answers', 'answers')
			.leftJoinAndSelect('answers.question', 'question')
			.where('feedback.order_detail_id = :order_detail_id', { order_detail_id })
			.andWhere('feedback.user_id = :user_id', { user_id })
			.orderBy('asset.order', 'ASC')
			.addOrderBy('question.order', 'ASC')
			.getMany()
			.then(this.parser) as any
	}

	@RepositoryModel.bound
	async getAnswerBy(
		ref_id: number,
		type: FEEDBACKS,
		transaction: EntityManager,
	): Promise<Array<FeedbackAnswerInterface & {
		question: QuestionInterface,
	}>> {
		return this.query('app.feedback_answer', 'feedback_answer', transaction)
			.leftJoin('feedback_answer.feedback', 'feedback')
			.leftJoinAndSelect('feedback_answer.question', 'question')
			.where('feedback.ref_id = :ref_id AND feedback.type = :type', { ref_id, type })
			.getMany()
			.then(this.parser) as any
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async resetFeedback(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.queryDelete('app.feedback', transaction)
			.where('user_id = :user_id', { user_id })
			.andWhere('order_detail_id = :order_detail_id', { order_detail_id })
			.execute()
			.then(() => true)
	}

	// ============================ PRIVATES ============================

}
