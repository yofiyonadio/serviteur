import RepositoryModel from 'app/models/repository'
import { EntityRepository, IsNull, EntityManager, In } from 'typeorm'

import {
	ColorRecord,
} from 'energie/app/records'

import {
	ColorInterface,
} from 'energie/app/interfaces'


@EntityRepository()
export default class ColorRepository extends RepositoryModel<{
	ColorRecord: ColorRecord,
}> {

	static __displayName = 'ColorRepository'

	protected records = {
		ColorRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(colors: Array<Partial<ColorInterface>>, transaction: EntityManager): Promise<boolean> {
		return this.saveOrUpdate('ColorRecord', colors, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async get(id: number, transaction: EntityManager): Promise<ColorInterface>
	async get(id: number[], transaction: EntityManager): Promise<ColorInterface[]>

	@RepositoryModel.bound
	async get(id: number | number[], transaction: EntityManager): Promise<ColorInterface | ColorInterface[]> {
		if(Array.isArray(id)) {
			return this.getMany('ColorRecord', {
				id: In(id),
			}, {
				transaction,
			})
		} else {
			return this.getOne('ColorRecord', {
				id,
			}, {
				transaction,
			})
		}
	}

	@RepositoryModel.bound
	async getIds(transaction: EntityManager): Promise<number[]> {
		return this.getMany('ColorRecord', {
			deleted_at: IsNull(),
		}, {
			select: ['id'],
			transaction,
		}).then(colors => colors.map(c => c.id))
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}
