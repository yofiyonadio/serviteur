import RepositoryModel from 'app/models/repository'
import { EntityRepository, EntityManager, In } from 'typeorm'

import {
	PacketRecord,
} from 'energie/app/records'

import {
	PacketInterface,
} from 'energie/app/interfaces'

import { CommonHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

export function guard(data: Partial<Parameter<PacketInterface>>) {
	const packet = {
		weight: data.weight as any === '' ? 0 : parseFloat(data.weight as any) || undefined,
		height: data.height as any === '' ? 0 : parseFloat(data.height as any) || undefined,
		length: data.length as any === '' ? 0 : parseFloat(data.length as any) || undefined,
		width: data.width as any === '' ? 0 : parseFloat(data.width as any) || undefined,
	} as Parameter<PacketInterface>

	if (packet.weight && packet.height && packet.length) {
		packet.volume = packet.weight * packet.height * packet.length
	}

	return CommonHelper.stripUndefined(packet)
}


@EntityRepository()
export default class PacketRepository extends RepositoryModel<{
	PacketRecord: PacketRecord,
}> {

	static __displayName = 'PacketRepository'

	protected records = {
		PacketRecord,
	}

	// ============================= INSERT =============================
	@RepositoryModel.bound
	async insert(data: Parameter<PacketInterface>, transaction: EntityManager): Promise<PacketInterface> {
		return this.save('PacketRecord', guard(data), transaction)
	}

	@RepositoryModel.bound
	async duplicate(packet_id: number, transaction: EntityManager): Promise<number> {
		return this.queryCustom(
			`INSERT INTO "app"."packet"("weight", "height", "length", "width", "volume")
			SELECT weight, height, length, width, volume
			FROM "app"."packet" WHERE id = $1
			RETURNING id`,
			[packet_id],
			transaction,
		).then(datas => datas[0].id)
	}

	// ============================= UPDATE =============================
	@RepositoryModel.bound
	async update(id: number, data: Partial<Parameter<PacketInterface>>, transaction: EntityManager) {
		return this.renew('PacketRecord', id, guard(data), {
			transaction,
		})
	}

	// ============================= GETTER =============================
	async getPackets(packet_ids: number[], transaction: EntityManager): Promise<PacketInterface[]>
	async getPackets(condition: Partial<PacketInterface>, transaction: EntityManager): Promise<PacketInterface>

	@RepositoryModel.bound
	async getPackets(packet_ids_or_condition: number[] | Partial<PacketInterface>, transaction: EntityManager): Promise<any> {
		if(Array.isArray(packet_ids_or_condition)) {
			return this.getMany('PacketRecord', {
				id: In(packet_ids_or_condition),
			}, { transaction }).then(packets => {
				return packet_ids_or_condition.map(packet_id => packets.find(p => p.id === packet_id))
			})
		} else {
			return this.getOne('PacketRecord', packet_ids_or_condition, { transaction })
		}
	}

	// ============================= DELETE =============================
	@RepositoryModel.bound
	async delete(packet_id: number, transaction: EntityManager): Promise<boolean> {
		return this.query('app.packet', 'packet', transaction)
			.where('id = :packet_id', { packet_id })
			.delete()
			.execute()
			.then(() => {
				return true
			})
			.catch(err => {
				this.warn(err)

				return false
			})
	}

	// ============================ PRIVATES ============================

}
