import { ControllerModel } from './models'
import Controllers from 'controllers'
import { Application } from 'express'
import { Connection } from 'typeorm'

import CommonHelper from 'utils/helpers/common'


export default {
	init(app: Application, connection: Connection) {
		Object.keys(Controllers).map(type => {
			const Route = Object.values(Controllers[type]).map((controller: ControllerModel) => {
				controller.setConnection(connection)

				return controller.route()
			}).reduce(CommonHelper.sumObject, {})

			Object.keys(Route).forEach(key => {
				Object.keys(Route[key]).forEach(method => {
					let _key = key

					if (type !== 'common') {
						_key = `/${ type }${ key }`
					}

					app[method](_key, Route[key][method])
				})
			})
		})
	},
}
