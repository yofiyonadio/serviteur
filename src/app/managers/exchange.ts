import { ManagerModel, ErrorModel } from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import InventoryManager from './inventory'
import OrderManager from './order'
import ShipmentManager from './shipment'
import UserManager from './user'
import BrandManager from './brand'

import {
	ExchangeRepository,
} from 'app/repositories'

import {
	ExchangeInterface,
	ExchangeDetailInterface,
	BrandInterface,
	ColorInterface,
	ProductInterface,
	SizeInterface,
	VariantAssetInterface,
	VariantInterface,
} from 'energie/app/interfaces'

import { CommonHelper, TimeHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

import DEFAULTS from 'utils/constants/default'
import { CODES, INVENTORIES, BOOKING_SOURCES, SHIPMENTS } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'
// import { PacketManager } from '.'
import { PacketManager, PromoManager } from '.'

class ExchangeManager extends ManagerModel {

	static __displayName = 'ExchangeManager'

	protected ExchangeRepository: ExchangeRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ExchangeRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createExchange(
		order_detail_id: number,
		details: Array<Parameter<ExchangeDetailInterface, 'exchange_id'>>,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<ExchangeInterface & {
		details: ExchangeDetailInterface[],
	}> {
		const exchange = await this.ExchangeRepository.insertExchange({
			// order_detail_id,
			user_id: 1,
			status: CODES.PENDING,
			note,
		}, transaction)

		const exchangeDetails = await this.ExchangeRepository.insertDetail(details.map(detail => {
			return {
				exchange_id: exchange.id,
				status: CODES.PENDING,
				...detail,
			}
		}), transaction)

		return {
			...exchange,
			details: exchangeDetails,
		}
	}

	get insertDetail() {
		return this.ExchangeRepository.insertDetail
	}

	@ManagerModel.bound
	async createReturnShipment(
		user_id: number,
		awb: string,
		courier: string,
		items: Array<{
			order_detail_id: number,
			inventory_id: number,
			is_refund: boolean,
		}>,
		transaction: EntityManager,
	) {
		// get orderDetails for refund value calculation
		const orderDetails = await this.ExchangeRepository.getOrderDetailsForRefundValue(items.map(item => item.order_detail_id), transaction)

		// get the refund value
		const refundValue = await orderDetails.reduce(async (promise, orderDetail) => {
			return promise.then(async init => {
				const refund = await OrderManager.utilities.refundProcessor(
					user_id,
					orderDetail.order_id,
					null,
					[],
					false,
					transaction,
				)

				return {
					...init,
					...refund,
				}
			})
		}, Promise.resolve({} as {
			[variant_id: number]: number,
		}))

		// create exchange
		const exchange = await this.ExchangeRepository.insertExchange({
			user_id,
			status: CODES.PENDING,
		}, transaction)

		// create the details
		const details = await this.ExchangeRepository.insertDetail(await Promise.all(items.map(async item => {
			const inventory = await InventoryManager.get(item.inventory_id, transaction)

			return {
				exchange_id: exchange.id,
				order_detail_id: item.order_detail_id,
				inventory_id: item.inventory_id,
				is_refund: item.is_refund,
				refund_value: refundValue[inventory.variant_id],
				status: CODES.PENDING,
			}
		})), transaction)

		// get shipment addreses
		const lastOrderDetailShipment = await ShipmentManager.getShipmentFromOrderDetail(items[0].order_detail_id, false, false, transaction)
		const origin  = lastOrderDetailShipment.destination
		const packet = lastOrderDetailShipment.packet
		const destination = await BrandManager.getAddresses(DEFAULTS.YUNA_BRAND_ID, transaction).then(addresses => addresses[0])

		// create shipment
		const shipment = await ShipmentManager.create(
			SHIPMENTS.MANUAL,
			packet,
			undefined,
			[origin, destination],
			false,
			true,
			{
				amount: 0,
				prices: {},
				awb,
				url: '',
				note: '',
				metadata: {},
				created_at: new Date(),
				courier,
				service: '',
			},
			false,
			transaction,
		)

		// update shipment
		await this.ExchangeRepository.insertShipment(details.map(detail => {
			return {
				shipment_id: shipment.id,
				exchange_detail_id: detail.id,
			}
		}), transaction)

		return shipment
	}

	@ManagerModel.bound
	async createReplacementShipment(
		changer_user_id: number,
		awb: string,
		type: SHIPMENTS,
		courier: string,
		exchange_id: number,
		user_address_id: number,
		exchange_detail_ids: number[],
		transaction: EntityManager,
	) {
		// get exchange
		const exchange = await this.ExchangeRepository.get(exchange_id, null, {
			with_details: true,
		}, {
			approved_exchange: true,
		}, transaction)

		// check exchange detail id
		if (exchange.details.map(detail => exchange_detail_ids.indexOf(detail.id) > -1 ? true : false).indexOf(false) > -1) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_101, 'Unknown Exchange Detail ID')
		}

		// get addresses
		const origin = await BrandManager.getAddresses(DEFAULTS.YUNA_BRAND_ID, transaction).then(addresses => addresses[0])
		const destination = await UserManager.address.get(user_address_id, transaction)
		const packet = await PacketManager.getPacket(DEFAULTS.PRODUCT_PACKET_ID, transaction)

		// create shipment
		const shipment = await ShipmentManager.create(
			type,
			packet,
			undefined,
			[origin, destination],
			false,
			false,
			{
				amount: 0,
				prices: {},
				awb,
				url: '',
				note: '',
				metadata: {},
				created_at: new Date(),
				courier,
				service: '',
			},
			false,
			transaction,
		)

		// update shipment
		await this.ExchangeRepository.insertShipment(exchange_detail_ids.map(id => {
			return {
				shipment_id: shipment.id,
				exchange_detail_id: id,
			}
		}), transaction)

		return shipment
	}

	async createExpiredExchange(
		user_id: number,
		items: Array<{
			order_detail_id: number,
			inventory_id: number,
			is_refund: boolean,
		}>,
		transaction: EntityManager,
	) {
		// create exchange
		const exchange = await this.ExchangeRepository.insertExchange({
			user_id,
			note: 'Expired',
			status: CODES.FAILED,
		}, transaction)

		// create the details
		const details = await this.ExchangeRepository.insertDetail(items.map(item => {
			return {
				exchange_id: exchange.id,
				order_detail_id: item.order_detail_id,
				inventory_id: item.inventory_id,
				is_refund: item.is_refund,
				refund_value: 0,
				note: 'Expired',
				status: CODES.FAILED,
			}
		}), transaction)

		return {
			...exchange,
			details,
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateDetailToRefund(
		changer_user_id: number,
		exchange_detail_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.ExchangeRepository.updateDetail(exchange_detail_id, changer_user_id, {
			is_refund: true,
		}, transaction)
	}

	@ManagerModel.bound
	async confirmReplacementShipment(
		changer_user_id: number,
		shipment_id: number,
		transaction: EntityManager,
	) {
		// confirm replacement shipment
		return ShipmentManager.confirmDelivery(
			changer_user_id,
			null,
			shipment_id,
			true,
			false,
			transaction,
		).then(async result => {
			// resolve exchange details
			const exchangedItems = await this.ExchangeRepository.getExchangeDetailFromReplacementShipment(shipment_id, transaction)

			// resolve exchange details
			Promise.all(exchangedItems.map(exchangeDetail => {
				this.ExchangeRepository.updateDetail(
					exchangeDetail.id,
					changer_user_id,
					{
						status: CODES.SUCCESS,
					}, transaction)
			})).then(async () => {
				const exchangeDetails = await this.ExchangeRepository.get(exchangedItems[0].exchange_id, null, {
					with_details: true,
				}, {}, transaction).then(exchange => exchange.details)

				let success = 0
				let failed = 0

				exchangeDetails.map(detail => {
					switch (detail.status) {
					case CODES.SUCCESS:
						success++
						break
					case CODES.FAILED:
						failed++
						break
					}
				})

				if (success > 0 && failed === 0) {
					/// SUCCESS
					await this.ExchangeRepository.update(exchangedItems[0].exchange_id, changer_user_id, {
						status: CODES.SUCCESS,
					}, transaction)
				} else if (success === 0 && failed > 0) {
					/// FAILED
					await this.ExchangeRepository.update(exchangedItems[0].exchange_id, changer_user_id, {
						status: CODES.FAILED,
					}, transaction)
				} else {
					/// EXCEPTION
					await this.ExchangeRepository.update(exchangedItems[0].exchange_id, changer_user_id, {
						status: CODES.EXCEPTION,
					}, transaction)
				}
			})

			return result
		})
	}

	// ============================= GETTER =============================
	get get() {
		return this.ExchangeRepository.get
	}

	get getByOrderDetailId() {
		return this.ExchangeRepository.getByOrderDetailId
	}

	@ManagerModel.bound
	async list(
		user_id: number,
		filter: {
			status?: 'available' | 'ongoing' | 'expired' | 'history'
			offset?: number,
			limit?: number,
		} = {},
		transaction: EntityManager,
	) {
		switch (filter.status) {
		case 'available':
		case 'expired':
			return this.ExchangeRepository.getNoShipmentList(user_id, {
				expired: filter.status === 'expired' ? true : false,
			}, transaction)
				.then(lists => {
					return lists.map(list => {
						return {
							feedback_id: list.id,
							created_at: list.created_at,
							order_detail_id: list.order_detail_id,
							inventory_id: list.variant.inventory.id,
							is_return: (list.answer.answer as any).TAB === 'Return' ? true : false,
							exchange_expired_at: TimeHelper.moment(list.shipment.confirmed_at).add(7, 'days'),
							variant: {
								id: list.variant?.id,
								product: list.variant?.product.title,
								brand: list.variant?.brand.title,
								size: list.variant?.size.title,
								colors: list.variant?.colors.map(color => CommonHelper.allowKey(color, 'id', 'hex', 'title', 'image')),
								image: CommonHelper.allowKey(list.variant?.asset, 'id', 'url', 'metadata.version'),
							},
						}
					})
				})
		case 'ongoing':
			return this.ExchangeRepository.getUserExchange(user_id, {detailed: false}, {...filter, history: false}, transaction)
			.then(lists => {
				return lists.map(list => {
					return {
						id: list.id,
						created_at: list.created_at,
						awb: list.return_shipment.awb,
						
						variants: list.details.map(detail => {
							return {
								id: detail.variant?.id,
								product: detail.variant?.product?.title,
								brand: detail.variant?.brand?.title,
								size: detail.variant?.size?.title,
								order_detail_id: detail.order_detail_id,
								inventory_id: detail.inventory_id,
								// is_return: (detail.answer?.answer as any)?.TAB === 'Return' ? true : false,
								colors: detail.variant?.colors?.map(color => CommonHelper.allowKey(color, 'id', 'hex', 'title', 'image')),
								image: CommonHelper.allowKey(detail.variant?.asset, 'id', 'url', 'metadata.version'),
							}
						}),
					}
				})
			})
		case 'history':
			return this.ExchangeRepository.getUserExchange(user_id, { detailed: false}, {...filter, history: true}, transaction)
			.then(lists => {
				return lists.map(list => {
					return {
						id: list.id,
						created_at: list.created_at,
						awb: list.return_shipment?.awb ?? null,
						variants: list.details.map(detail => {
							return {
								id: detail.variant?.id,
								product: detail.variant?.product.title,
								brand: detail.variant?.brand.title,
								size: detail.variant?.size.title,
								colors: detail.variant?.colors.map(color => CommonHelper.allowKey(color, 'id', 'hex', 'title', 'image')),
								image: CommonHelper.allowKey(detail.variant?.asset, 'id', 'url', 'metadata.version'),
							}
						}),
					}
				})
			})
		default:
			return []
		}
	}

	@ManagerModel.bound
	async filter(
		offset: number,
		limit: number,
		status: CODES | null | undefined,
		search: string | undefined,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.filter(offset, limit, status, search, transaction)
	}

	@ManagerModel.bound
	async getExchangeDetail(
		exchange_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		const replacements: Array<{
			id: number,
			type: string,
			awb: string,
			courier: string,
			status: string,
			variants: Array<VariantInterface & {
				product: ProductInterface,
				brand: BrandInterface,
				asset: VariantAssetInterface,
				size?: SizeInterface,
				colors?: ColorInterface[],
			}>,
		}> = []

		return this.ExchangeRepository.getUserExchange(user_id, {
			detailed: true,
			with_replacement_items: true,
		}, {exchange_id}, transaction)
			.then(list => {
				return {
					id: list.id,
					created_at: list.created_at,
					awb: list.return_shipment?.awb ?? null,
					note: list.note,
					status: list.status,
					details: list.details.map(detail => {
						// replacement handler
						if (!!detail.replacement_shipment?.id) {
							const index = replacements.findIndex(replacement => replacement.id === detail.replacement_shipment.id)

							if (index > -1) {
								replacements[index].variants.push(detail.replacement_shipment.variant)
							} else {
								replacements.push({
									id: detail.replacement_shipment.id,
									type: detail.replacement_shipment.type,
									awb: detail.replacement_shipment.awb,
									courier: detail.replacement_shipment.courier,
									status: detail.replacement_shipment.status,
									variants: [detail.replacement_shipment.variant],
								})
							}
						}

						return {
							id: detail.id,
							created_at: detail.created_at,
							status: detail.status,
							is_refund: detail.is_refund,
							note: detail.note,
							order: {
								id: detail.order.id,
								number: detail.order.number,
							},
							variant: {
								id: detail.variant.id,
								price: detail.variant.price,
								product: detail.variant.product.title,
								brand: detail.variant.brand.title,
								size: detail.variant.size.title,
								colors: detail.variant.colors.map(color => CommonHelper.allowKey(color, 'id', 'hex', 'title', 'image')),
								image: CommonHelper.allowKey(detail.variant.asset, 'id', 'url', 'metadata.version'),
							},
						}
					}),
					replacement_shipments: replacements.map(replacement => {
						return {
							...replacement,
							variants: replacement.variants.reduce((init, variant) => {
								if (variant) {
									return [...init, {
										id: variant.id,
										price: variant.price,
										product: variant.product.title,
										brand: variant.brand.title,
										size: variant.size.title,
										colors: variant.colors.map(color => CommonHelper.allowKey(color, 'id', 'hex', 'title', 'image')),
										image: CommonHelper.allowKey(variant.asset, 'id', 'url', 'metadata.version'),
									}]
								} else {
									return init
								}
							}, []),
						}
					}),
				}
			})
	}

	@ManagerModel.bound
	async getDeep(
		exchange_id: number,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.getDeepWithoutDetails([exchange_id], transaction).then(exchanges => exchanges[0])
	}

	// this one is used for admin
	@ManagerModel.bound
	async getDetail(
		exchange_id: number,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.getDetails([exchange_id], transaction).then(exchanges => exchanges[0])
	}

	get getDetailsWithFeedback() {
		return this.ExchangeRepository.getDetailsWithFeedback
	}

	@ManagerModel.bound
	async getDetailWithOrder(
		exchange_id: number,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.getDetailWithOrder([exchange_id], transaction).then(exchanges => exchanges[0])
	}

	@ManagerModel.bound
	async getDetailNote(
		user_id: number,
		exchange_id: number,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.getHistory(user_id, [exchange_id], transaction).then(exchanges => exchanges[0])
	}

	@ManagerModel.bound
	async getByReplacement(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.ExchangeRepository.getByReplacement(order_detail_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async ApproveOrReject(
		changer_user_id: number,
		exchange_id: number,
		details: Array<{
			exchange_detail_id: number,
			variant_id?: number,
			status: CODES,
			note?: string,
		}>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const exchangeDetails: ExchangeDetailInterface[] = []

		const exchange = await this.ExchangeRepository.get(exchange_id, null, {
			with_details: true,
			with_order: true,
			with_variant: true,
			with_return_shipment: true,
		}, {}, transaction)

		// ==============================================================
		// Reject if status already set
		// ==============================================================
		if (exchange.status !== CODES.PENDING) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'This exchange already resolved.')
		}

		// ==============================================================
		// Reject if exchange already cancelled
		// ==============================================================
		if (!!exchange.deleted_at) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'This exchange already cancelled.')
		}

		// ==============================================================
		// Reject if incomplete
		// ==============================================================
		if (details.length !== exchange.details.length) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'You need to supply all of exchange\'s detail in your request to approve.')
		}

		// ==============================================================
		// Confirm the delivery from theirs to ours, Reject if don't have any
		// ==============================================================

		// this.log('Exchange ', exchange)
		if(!exchange.return_shipment?.id) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Data shipment pengembalian dari user tidak ditemukan')
		} else {
			// await ShipmentManager.confirmDelivery(
			// 	changer_user_id,
			// 	null,
			// 	exchange.return_shipment.id,
			// 	true,
			// 	false,
			// 	transaction,
			// ).catch(error => {
			// 	throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'No shipping record from user')
			// })
		}

		// ==============================================================
		// Cashback handler
		// ==============================================================
		const order_ids = exchange.details.reduce((init, detail) => {
			return init.findIndex(fi => fi === detail.order.id) > -1 ? init : init.concat(detail.order.id)
		}, [] as number[])

		await order_ids.reduce(async (promise, id) => {
			return promise.then(async init => {
				await PromoManager.cashback.disputeHandler(exchange.details[0].order.user_id, id, transaction).catch(err => this.log('Cant dispute cashback ', err))
			})
		}, Promise.resolve(null)).catch(error => {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_100, order_ids) // ! Check is it correct?
		})

		// ==============================================================
		// Great! Now we can process it
		// ==============================================================
		await details.reduce(async (promise, detail) => {
			return promise.then(async init => {
				// get the exchangeDetail
				const exchangeDetail = exchange.details.filter(d => d.id === detail.exchange_detail_id)[0]

				// check Refund or Exchange
				if (exchangeDetail.is_refund) {
					if (detail.status === CODES.SUCCESS) {
						// if refund, mark inventory as returned
						await InventoryManager.update(changer_user_id, exchangeDetail.inventory_id, {
							status: INVENTORIES.AVAILABLE,
							note: 'Exchanged item. Was marked as refunded with fulfillment team.',
						}, undefined, transaction)

						// add refund value to user wallet and point
						await OrderManager.utilities.refundProcessor(
							exchange.user_id,
							exchangeDetail.order.id,
							null,
							[exchangeDetail.inventory_id],
							true,
							transaction,
						)
					}

					// push to exchangeDetails variable
					exchangeDetails.push({
						...exchangeDetail,
						status: detail.status,
					})

					// update exchange detail
					await this.ExchangeRepository.updateDetail(
						exchangeDetail.id,
						changer_user_id,
						{
							note: detail.note,
							status: detail.status,
						},
						transaction,
					)
				} else if (!exchangeDetail.is_refund) {
					if (detail.status === CODES.SUCCESS) {
						// if exchange, mark inventory as returned
						await InventoryManager.update(changer_user_id, exchangeDetail.inventory_id, {
							status: INVENTORIES.AVAILABLE,
							note: 'Exchanged item. Was marked as refunded with fulfillment team.',
						}, undefined, transaction)

						// get inventory replacement
						const inventory = await InventoryManager.getFromVariant(
							detail.variant_id,
							1,
							true,
							true,
							transaction,
						).then(i => i[0])

						// book inventory
						await InventoryManager.book(
							changer_user_id,
							inventory.id,
							BOOKING_SOURCES.ORDER_DETAIL,
							exchangeDetail.id,
							`Book from exchange #${exchange.id}`,
							false,
							transaction,
						)

						// push to exchangeDetails variable
						exchangeDetails.push({
							...exchangeDetail,
							status: CODES.APPROVED,
						})

						// update exchange detail replacement_inventory_id
						await this.ExchangeRepository.updateDetail(
							exchangeDetail.id,
							changer_user_id,
							{
								note: detail.note,
								status: CODES.APPROVED,
								replacement_inventory_id: inventory.id,
							},
							transaction,
						)
					} else {
						// push to exchangeDetails variable
						exchangeDetails.push({
							...exchangeDetail,
							status: detail.status,
						})

						// update exchange detail
						await this.ExchangeRepository.updateDetail(
							exchangeDetail.id,
							changer_user_id,
							{
								note: detail.note,
								status: detail.status,
							},
							transaction,
						)
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_099)
				}
			})
		}, Promise.resolve()).then(async () => {
			let success = 0
			let approved = 0
			let failed = 0

			exchangeDetails.map(detail => {
				switch (detail.status) {
				case CODES.SUCCESS:
					success++
					break
				case CODES.APPROVED:
					approved++
					break
				case CODES.FAILED:
					failed++
					break
				}
			})

			// hold resolved exchange if have one or more approved exchange status,
			// it'll be changed at user confirmation shipment
			if (approved === 0) {
				if (success > 0 && failed === 0) {
					/// SUCCESS
					await this.ExchangeRepository.update(exchange.id, changer_user_id, {
						note,
						status: CODES.SUCCESS,
					}, transaction)
				} else if (success === 0 && failed > 0) {
					/// FAILED
					await this.ExchangeRepository.update(exchange.id, changer_user_id, {
						note,
						status: CODES.FAILED,
					}, transaction)
				} else {
					/// EXCEPTION
					await this.ExchangeRepository.update(exchange.id, changer_user_id, {
						note,
						status: CODES.EXCEPTION,
					}, transaction)
				}
			}
		})

		// return something userfull
		return true
	}

	// ============================ PRIVATES ============================

}

export default new ExchangeManager()

