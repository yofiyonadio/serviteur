import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import NotificationManager from './notification'
import UserManager from './user'

import {
	UserAccountRepository,
	UserAnswerRepository,
	UserProfileRepository,
} from 'app/repositories'

import { UserInterface, UserProfileInterface } from 'energie/app/interfaces'

import AppleHandler from 'app/handlers/apple'
import FacebookHandler from 'app/handlers/facebook'
import GoogleHandler from 'app/handlers/google'

import { TimeHelper } from 'utils/helpers'

import { ERRORS, ERROR_CODES } from 'app/models/error'
import { REFERRAL_POINT_STATUSES, SOCIALS, STATEMENT_SOURCES, TOKENS } from 'energie/utils/constants/enum'

import bcrypt from 'bcrypt'
import { isNullOrUndefined } from 'util'

const POSSIBLE_CHARS = '0123456789'
const POSSIBLE_CHARS_LENGTH = POSSIBLE_CHARS.length


class AccountManager extends ManagerModel {

	static __displayName = 'AccountManager'

	protected UserAccountRepository: UserAccountRepository
	protected UserAnswerRepository: UserAnswerRepository
	protected UserProfileRepository: UserProfileRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserAccountRepository,
			UserProfileRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async createToken(email: string, password: string) {
		// Token is recreated when logging in
		return bcrypt.hash(password, 10).then((passhash: string) => {
			return bcrypt.hash(`${email}${Date.now()}${passhash}`, 10).then(tokehash => {
				return [passhash, tokehash]
			})
		})
	}

	@ManagerModel.bound
	async sendToken(
		type: TOKENS,
		email: string,
		force: boolean,
		transaction: EntityManager,
	) {
		if (!email) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Email not provided')
		}

		if (!type) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Type not provided')
		}

		const validEmail = email.toLowerCase()

		return this.UserAccountRepository.getByEmail(validEmail, true, transaction).then(async user => {
			if (type === TOKENS.VERIFICATION && user.is_verified) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'User already verified')
			}

			const token = this.makeid()

			return this.UserAccountRepository.getTokenBy(
				user.id,
				type,
				transaction,
			).then(async userToken => {
				if (force) {
					const changesCount = (userToken.email_changes ?? '').split(',').length + 1
					const nextRetry = TimeHelper.moment(userToken.updated_at).add(Math.pow(5, changesCount), 's')

					if (+nextRetry.toDate() <= Date.now()) {
						await this.UserAccountRepository.updateToken(userToken.id, {
							token,
							email_changes: userToken.email_changes ? userToken.email_changes + `,${email}` : email,
						}, transaction)
					} else {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, nextRetry.toDate())
					}
				} else if (+userToken.updated_at > (Date.now() - (1000 * 60 * 60 * 24))) {
					// Sudah dikirimkan dalam max. 1x24 jam yang lalu
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, userToken.updated_at)
				} else if (+userToken.next_retry_at > Date.now()) {
					// Token inactive, sudah pernah digunakan dan salah
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, userToken.next_retry_at)
				} else {
					await this.UserAccountRepository.updateToken(userToken.id, {
						token,
					}, transaction)
				}

				switch (type) {
					case TOKENS.VERIFICATION:
						return NotificationManager.sendVerificationNotice(
							user.id,
							user.email,
							token,
						)
					case TOKENS.RESET:
						return NotificationManager.sendForgotPasswordNotice(
							user.id,
							user.email,
							token,
						)
				}
			}).catch(async err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					await this.UserAccountRepository.insertUserToken({
						user_id: user.id,
						token,
						type,
						verification_attempt: 0,
						email_changes: email,
					}, transaction)

					switch (type) {
						case TOKENS.VERIFICATION:
							return NotificationManager.sendVerificationNotice(
								user.id,
								user.email,
								token,
							)
						case TOKENS.RESET:
							return NotificationManager.sendForgotPasswordNotice(
								user.id,
								user.email,
								token,
							)
					}
				}

				throw err
			})
		})
	}

	@ManagerModel.bound
	async checkEmailExistance(
		email: string,
		transaction: EntityManager,
	) {
		if (!email) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Email not provided')
		}

		const validEmail = email.toLowerCase()

		return this.UserAccountRepository.getByEmail(validEmail, false, transaction).then(() => true).catch(() => false)
	}

	@ManagerModel.bound
	async checkPhoneExistance(
		phone: string,
		transaction: EntityManager,
	) {
		if (!phone) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Phone not Provided')
		}

		return this.UserAccountRepository.getByPhone(phone, false, transaction).then(() => true).catch(() => false)
	}

	@ManagerModel.bound
	async checkTokenValidity(
		email: string,
		token: string,
		type: TOKENS,
		transaction: EntityManager,
	) {
		if (!email) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Email not provided')
		}

		if (!token) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Token not provided')
		}

		const validEmail = email.toLowerCase()

		return this.UserAccountRepository.getToken(validEmail, token, type, transaction).then(userToken => {
			if (+userToken.updated_at <= (Date.now() - (1000 * 60 * 60 * 24))) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, userToken.updated_at)
			} else if (+userToken.next_retry_at > Date.now()) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, userToken.next_retry_at)
			}

			return true
		})
	}

	@ManagerModel.bound
	async verifyUser(
		email: string,
		token: string,
		transaction: EntityManager,
	) {
		if (!email) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Email not provided')
		}

		if (!token) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Token not provided')
		}

		const validEmail = email.toLowerCase()

		return this.UserAccountRepository.getToken(validEmail, token, TOKENS.VERIFICATION, transaction).then(async userToken => {

			if (+userToken.updated_at <= (Date.now() - (1000 * 60 * 60 * 24))) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, userToken.updated_at)
			} else if (+userToken.next_retry_at > Date.now()) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, userToken.next_retry_at)
			}

			userToken.used_at = new Date()
			userToken.user.is_verified = true

			await this.UserAccountRepository.updateToken(userToken.id, {
				used_at: userToken.used_at,
			}, transaction)

			await this.UserAccountRepository.updateUser(userToken.user.id, {
				is_verified: true,
			}, transaction)


			// PENGECEKAN SUDAH PERNAH DIKIRIM ATAU BELUM
			const pointAlreadySent = await UserManager.point.getDetailTransaction(userToken.user.id, transaction).then(pointTransaction => pointTransaction.point.transactions.some(pointTransactions => pointTransactions.note === 'Verification Loyalty Program Rewards' && pointTransactions.status === 'APPLIED'))

			if (!pointAlreadySent) {
				await UserManager.point.depositPoint(userToken.user.id, 5000, STATEMENT_SOURCES.CAMPAIGN, null, 'Verification Loyalty Program Rewards', transaction)
			}


			// Promotion
			// const wallet = await UserManager.wallet.getWallet(userToken.user.id, transaction)
			// await UserManager.wallet.getWalletTransactionOfUser(userToken.user.id, transaction)
			// 	.then(async walletTransaction => {
			// 		if (walletTransaction.findIndex(f => f.source === STATEMENT_SOURCES.CAMPAIGN && f.ref_id === 11) < 0) {
			// 			await UserManager.wallet.depositInto(
			// 				wallet.id,
			// 				50000,
			// 				STATEMENT_SOURCES.CAMPAIGN,
			// 				11,
			// 				'First Register Campaign Get 50k',
			// 				transaction,
			// 			)
			// 		}
			// 	})
			// 	.catch(async error => {
			// 		if (error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101)) {
			// 			await UserManager.wallet.depositInto(
			// 				wallet.id,
			// 				50000,
			// 				STATEMENT_SOURCES.CAMPAIGN,
			// 				11,
			// 				'First Register Campaign Get 50k',
			// 				transaction,
			// 			)
			// 		}
			// 	})


			return true
		})
	}

	// ||==============================================||
	// || PASSWORD MANIPULATION                        ||
	// ||==============================================||
	@ManagerModel.bound
	async changePassword(
		user_id: number,
		currentPassword: string,
		password1: string,
		password2: string,
		transaction: EntityManager,
	) {
		if (!password1 || !password2) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Passwords not provided')
		}

		if (password1 !== password2) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Passwords don\'t match')
		}

		return this.UserAccountRepository.getById(user_id, false, transaction).then(user => {
			if (user.password) {
				return bcrypt.compare(currentPassword, user.password).then(isMatch => {
					if (!isMatch) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Wrong current password')
					} else {
						return user
					}
				})
			} else if (!currentPassword) {
				// first time set password
				return user
			}

			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103)
		}).then(async user => {
			return this.createToken(user.email, password1).then(([passhash, tokehash]) => {
				return this.UserAccountRepository.updateUser(user.id, {
					password: passhash,
					token: tokehash,
				}, transaction).then(() => {
					user.password = passhash
					user.token = tokehash

					return user
				})
			})
		}).then(() => {
			return true
		})
	}

	@ManagerModel.bound
	async changePasswordByToken(
		email: string,
		token: string,
		password1: string,
		password2: string,
		transaction: EntityManager,
	) {
		if (!email) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Email not provided')
		}

		if (!token) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Token not provided')
		}

		if (!password1 || !password2) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Passwords not provided')
		}

		if (password1 !== password2) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Passwords don\'t match')
		}

		const validEmail = email.toLowerCase()

		return this.UserAccountRepository.getToken(validEmail, token, TOKENS.RESET, transaction).then(async userToken => {
			if (+userToken.updated_at <= (Date.now() - (1000 * 60 * 60 * 24))) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, userToken.updated_at)
			} else if (+userToken.next_retry_at > Date.now()) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, userToken.next_retry_at)
			}

			const [passhash, tokehash] = await this.createToken(validEmail, password1)

			userToken.used_at = new Date()
			userToken.user.is_verified = true
			userToken.user.password = passhash
			userToken.user.token = tokehash

			await this.UserAccountRepository.updateUser(userToken.user.id, {
				is_verified: true,
				password: passhash,
				token: tokehash,
			}, transaction)

			await this.UserAccountRepository.updateToken(userToken.id, {
				used_at: userToken.used_at,
			}, transaction)

			return true
		})
	}

	// ||==============================================||
	// || LOGIN METHODS                                ||
	// ||==============================================||
	@ManagerModel.bound
	async fbLogin(
		accessToken: string,
		transaction: EntityManager,
	) {
		if (!accessToken) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Access token not provided')
		}

		return FacebookHandler.authenticate(accessToken).then(async ({
			id,
			name,
			email: _email,
			picture,
		}) => {
			if (_email) {
				const email = _email.toLowerCase()

				return this.UserAccountRepository.getByEmail(email, true, transaction).catch(async err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						const names = name.split(' ')

						throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, {
							email,
							first_name: names.shift(),
							last_name: names.join(' '),
						})
					}

					throw err
				}).then(async user => {
					const social = await this.UserAccountRepository.getUserSocial(id, SOCIALS.FACEBOOK, transaction)
						.then(async userSocial => {
							userSocial.token = accessToken

							await this.UserAccountRepository.updateSocial(userSocial.ref_id, userSocial.type, {
								token: accessToken,
							}, transaction)

							return userSocial
						}).catch(err => {
							if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
								return this.UserAccountRepository.insertUserSocial({
									type: SOCIALS.FACEBOOK,
									user_id: user.id,
									ref_id: id,
									token: accessToken,
								}, transaction)
							}

							throw err
						})

					// If different, return social users
					if (social.user_id !== user.id) {
						return this.UserAccountRepository.getById(social.user_id, true, transaction)
					}

					return user
				})
			} else {
				// User has not grant the email permission
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
			}
		}).then(async user => {
			if (user.is_blocked) {
				throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_106, 'Your account has been blocked')
			} else {
				// user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)
				if (user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)) {
					await this.UserAccountRepository.updateUser(user.id, {
						logged_at: new Date(),
					}, transaction)

					return user
				} else {
					// update user token using email & accessToken
					return this.createToken(user.email, accessToken).then(async tokens => {
						user.token = tokens[1]
						user.logged_at = new Date()

						await this.UserAccountRepository.updateUser(user.id, {
							token: tokens[1],
							logged_at: user.logged_at,
						}, transaction)

						return user
					})
				}
			}
		})
	}

	@ManagerModel.bound
	async googleLogin(
		accessToken: string,
		platform: 'ios' | 'android' | 'web',
		transaction: EntityManager,
	) {
		if (!accessToken) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Access token not provided')
		}

		return GoogleHandler.authenticate(accessToken, platform).then(async payload => {
			if (payload.email) {

				const email = payload.email.toLowerCase()

				return this.UserAccountRepository.getByEmail(email, true, transaction).catch(async err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						const names = payload.name.split(' ')

						throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, {
							email: payload.email,
							first_name: names.shift(),
							last_name: names.join(' '),
						})
					}

					throw err
				}).then(async user => {
					const social = await this.UserAccountRepository.getUserSocial(payload.sub, SOCIALS.GOOGLE, transaction)
						.then(async userSocial => {
							userSocial.token = accessToken

							await this.UserAccountRepository.updateSocial(userSocial.ref_id, userSocial.type, {
								token: accessToken,
							}, transaction)

							return userSocial
						}).catch(err => {
							if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
								return this.UserAccountRepository.insertUserSocial({
									type: SOCIALS.GOOGLE,
									user_id: user.id,
									ref_id: payload.sub,
									token: accessToken,
								}, transaction)
							}

							throw err
						})

					// If different, return social users
					if (social.user_id !== user.id) {
						return this.UserAccountRepository.getById(social.user_id, true, transaction)
					}

					return user
				})
			} else {
				// User has not grant the email permission
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
			}
		}).then(async user => {
			if (user.is_blocked) {
				throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_106, 'Your account has been blocked')
			} else {
				// user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)
				if (user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)) {
					await this.UserAccountRepository.updateUser(user.id, {
						logged_at: new Date(),
					}, transaction)

					return user
				} else {
					// update user token using email & accessToken
					return this.createToken(user.email, accessToken).then(async tokens => {
						user.token = tokens[1]
						user.logged_at = new Date()

						await this.UserAccountRepository.updateUser(user.id, {
							token: tokens[1],
							logged_at: user.logged_at,
						}, transaction)

						return user
					})
				}
			}
		})
	}

	@ManagerModel.bound
	async appleLogin(
		accessToken: string,
		nonce: string,
		firstName: string,
		lastName: string,
		transaction: EntityManager,
	) {
		if (!accessToken || !nonce) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Access token or nonce not provided')
		}

		return AppleHandler.authenticate(accessToken, nonce).then(async payload => {

			if (payload.email) {

				const email = payload.email.toLowerCase()

				return this.UserAccountRepository.getByEmail(email, true, transaction).catch(async err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.register(
							'apple',
							payload.email,
							firstName,
							lastName,
							undefined,
							undefined,
							accessToken,
							{},
							undefined,
							transaction,
						)
						// throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, {
						// 	email: payload.email,
						// 	first_name: firstName,
						// 	last_name: lastName,
						// })
					}

					throw err
				}).then(async user => {
					const social = await this.UserAccountRepository.getUserSocial(payload.sub, SOCIALS.APPLE, transaction)
						.then(async userSocial => {
							userSocial.token = accessToken

							await this.UserAccountRepository.updateSocial(userSocial.ref_id, userSocial.type, {
								token: accessToken,
							}, transaction)

							return userSocial
						}).catch(err => {
							if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
								return this.UserAccountRepository.insertUserSocial({
									type: SOCIALS.APPLE,
									user_id: user.id,
									ref_id: payload.sub,
									token: accessToken,
								}, transaction)
							}

							throw err
						})

					// If different, return social users
					if (social.user_id !== user.id) {
						return this.UserAccountRepository.getById(social.user_id, true, transaction)
					}

					return user
				})
			} else {
				// User has not grant the email permission
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
			}
		}).then(async user => {
			if (user.is_blocked) {
				throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_106, 'Your account has been blocked')
			} else {
				// user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)
				if (user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)) {
					await this.UserAccountRepository.updateUser(user.id, {
						logged_at: new Date(),
					}, transaction)

					return user
				} else {
					// update user token using email & accessToken
					return this.createToken(user.email, accessToken).then(async tokens => {
						user.token = tokens[1]
						user.logged_at = new Date()

						await this.UserAccountRepository.updateUser(user.id, {
							token: tokens[1],
							logged_at: user.logged_at,
						}, transaction)

						return user
					})
				}
			}
		})
	}

	@ManagerModel.bound
	async login(
		from: 'email' | 'phone',
		emailOrPhone: string,
		password: string,
		transaction: EntityManager,
	) {
		switch (from) {
			case 'email':
				const validEmail = emailOrPhone.toLowerCase()
				return this.UserAccountRepository.getByEmail(validEmail, true, transaction).then(async user => {
					if (user.is_blocked) {
						throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_106, 'Your account has been blocked')
					} else {
						if (user.password) {
							return bcrypt.compare(password, user.password).then(async isMatching => {
								if (isMatching) {
									if (user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)) {
										await this.UserAccountRepository.updateUser(user.id, {
											logged_at: new Date(),
										}, transaction)

										return user
									} else {
										// update user token using validEmail & password
										return this.createToken(validEmail, password).then(async tokens => {
											user.token = tokens[1]
											user.logged_at = new Date()

											await this.UserAccountRepository.updateUser(user.id, {
												token: tokens[1],
												logged_at: user.logged_at,
											}, transaction)

											return user
										})
									}
								} else {
									// User name and password doesn't match
									throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_103, 'User name and password doesn\'t match')
								}
							})
						} else {
							// must be from social login
							throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_105, 'Password not found')
						}
					}
				})
			case 'phone':
				const phone = emailOrPhone
				return this.UserAccountRepository.getByPhone(phone, true, transaction).then(async user => {
					if (user.is_blocked) {
						throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_106, 'Your account has been blocked')
					} else {
						if (user.password) {
							return bcrypt.compare(password, user.password).then(async isMatching => {
								if (isMatching) {
									if (user.token && TimeHelper.moment().subtract(3, 'd') < TimeHelper.moment(user.logged_at)) {
										await this.UserAccountRepository.updateUser(user.id, {
											logged_at: new Date(),
										}, transaction)

										return user
									} else {
										// update user token using phone & password
										return this.createToken(phone, password).then(async tokens => {
											user.token = tokens[1]
											user.logged_at = new Date()

											await this.UserAccountRepository.updateUser(user.id, {
												token: tokens[1],
												logged_at: user.logged_at,
											}, transaction)

											return user
										})
									}
								} else {
									// User name and password doesn't match
									throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_103, 'Phone and password doesn\'t match')
								}
							})
						} else {
							// must be from social login
							throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_105, 'Password not found')
						}
					}
				})
			default:
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100)
		}
	}

	@ManagerModel.bound
	async register(
		type: 'email' | 'google' | 'facebook' | 'apple',
		email: string,
		first_name: string,
		last_name: string,
		phone: string,
		password: string,
		accessToken: string,
		metadata: object | undefined,
		referral_code: string | undefined,
		transaction: EntityManager,
	) {
		const validEmail = email.toLowerCase()
		let token: string[]

		return this.UserAccountRepository.getByEmail(email, false, transaction).then(() => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Email already registered')
		}).catch(async error => {
			if (error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101)) {
				return Promise.resolve().then(async () => {
					switch (type) {
						case 'email':
							token = await this.createToken(validEmail, password)

							return {
								password: token[0],
								token: token[1],
								social: undefined,
								ref_id: undefined,
								picture: undefined,
								is_verified: false,
							}

						case 'google':
							return await GoogleHandler.authenticate(accessToken)
								.then(async payload => {
									token = await this.createToken(validEmail, accessToken)

									if (payload.email.toLowerCase() === validEmail) {
										return {
											password: undefined,
											token: token[1],
											social: SOCIALS.GOOGLE,
											ref_id: payload.sub,
											picture: payload.picture,
											is_verified: true,
										}
									} else {
										throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'Registered email and gmail didn\'t match')
									}
								}).catch(err => {
									if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_105)) {
										throw err
									}

									throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
								})
						case 'facebook':
							return await FacebookHandler.authenticate(accessToken).then(async ({
								id,
								email: _email,
								picture,
							}) => {
								token = await this.createToken(validEmail, accessToken)

								if (_email.toLowerCase() === validEmail) {
									return {
										password: undefined,
										token: token[1],
										social: SOCIALS.FACEBOOK,
										ref_id: id,
										picture: picture.data.url,
										is_verified: true,
									}
								} else {
									throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'Registered email and facebook mail didn\'t match')
								}
							}).catch(err => {
								if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_105)) {
									throw err
								}

								throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
							})
						case 'apple':
							return AppleHandler.authenticate(accessToken).then(async ({
								sub,
								email: _email,
							}) => {
								token = await this.createToken(validEmail, accessToken)

								if (_email.toLowerCase() === validEmail) {
									return {
										password: undefined,
										token: token[1],
										social: SOCIALS.APPLE,
										ref_id: sub,
										picture: null,
										is_verified: true,
									}
								} else {
									throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'Registered email and apple mail didn\'t match')
								}
							}).catch(err => {
								if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_105)) {
									throw err
								}

								throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'User has not grant the email permission')
							})
					}
				}).then(async res => {
					const user = await this.createUser(
						email,
						first_name,
						last_name,
						res.password,
						res.token,
						phone,
						metadata,
						res.is_verified,
						transaction,
					)

					if (referral_code) {
						const referred_user = await this.UserProfileRepository.getUserProfile({ referral_code }, transaction)
						const amount = 20000

						if (referred_user) {
							this.log(`Injecting Referral point to hold...`)
							// await UserManager.point.depositPoint(referred_user.id, amount, STATEMENT_SOURCES.USER, user.id, `Referral Point from ${referred_user.profile.first_name} ${referred_user.profile.last_name ? referred_user.profile.last_name : ''}`, transaction)
							await UserManager.referral.insert({
								user_id: referred_user.id,
								user_reference: user.id,
								amount,
								referral_point_status: REFERRAL_POINT_STATUSES.HOLD,
								note: `Referral Point from ${referred_user.profile.first_name} ${referred_user.profile.last_name ? referred_user.profile.last_name : ''}`,
							}, transaction)
							this.log(`Applying Point Referral to user...`)
							await UserManager.point.depositPoint(user.id, amount, STATEMENT_SOURCES.USER, referred_user.id, 'Bonus Referral Point', transaction)
						} else {
							throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Referred User Not Found!')
						}
					} else {
						await UserManager.point.depositPoint(user.id, 5000, STATEMENT_SOURCES.CAMPAIGN, null, 'Registration Loyalty Program Rewards', transaction)
					}


					if (type !== 'email') {
						await this.UserAccountRepository.insertUserSocial({
							type: res.social,
							user_id: user.id,
							ref_id: res.ref_id,
							token: accessToken,
						}, transaction)

						if (res.picture) {
							await UserManager.uploadProfile(user, res.picture, 0, transaction)
						}

						// Promotion TURNED OFF COZ THE CAMPAIGN ALR DEACTIVATED
						// const wallet = await UserManager.wallet.getWallet(user.id, transaction)
						// await UserManager.wallet.getWalletTransactionOfUser(user.id, transaction)
						// 	.then(async walletTransaction => {
						// 		if (walletTransaction.findIndex(f => f.source === STATEMENT_SOURCES.CAMPAIGN && f.ref_id === 11) < 0) {
						// 			await UserManager.wallet.depositInto(
						// 				wallet.id,
						// 				50000,
						// 				STATEMENT_SOURCES.CAMPAIGN,
						// 				11,
						// 				'First Register Campaign Get 50k',
						// 				transaction,
						// 			)
						// 		}
						// 	})
						// 	.catch(async e => {
						// 		if (e instanceof ErrorModel && e.is(ERROR_CODES.ERR_101)) {
						// 			await UserManager.wallet.depositInto(
						// 				wallet.id,
						// 				50000,
						// 				STATEMENT_SOURCES.CAMPAIGN,
						// 				11,
						// 				'First Register Campaign Get 50k',
						// 				transaction,
						// 			)
						// 		}
						// 	})
					}


					return user
				})
			}

			throw error
		})
	}

	@ManagerModel.bound
	async validatePassword(
		user_id: number,
		password: string,
		transaction: EntityManager,
	) {
		const social = await this.UserAccountRepository.getUserSocialByUserId(user_id, transaction).catch(() => null)

		return this.UserAccountRepository.getById(user_id, false, transaction).then(async user => {
			if (user.password) {
				if (isNullOrUndefined(social)) {
					if (isNullOrUndefined(password)) {
						return false
					}

					return bcrypt.compare(password, user.password).then(isMatch => {
						if (!isMatch) {
							return false
						} else {
							return true
						}
					})
				} else {
					return true
				}
			} else if (isNullOrUndefined(user.password)) {
				if (isNullOrUndefined(social)) {
					return false
				} else {
					return true
				}
			} else {
				return false
			}
		})
	}

	// ============================ PRIVATES ============================
	private async createUser(
		email: string,
		first_name: string,
		last_name: string,
		password: string | undefined,
		token: string | undefined,
		phone: string | undefined,
		metadata: object,
		is_verified: boolean = false,
		transaction?: EntityManager,
	): Promise<UserInterface & {
		profile: UserProfileInterface,
	}> {
		return Promise.resolve().then(async () => {
			const profile = await this.UserAccountRepository.insertUserProfile({
				first_name,
				last_name,
				phone,
			}, transaction)

			const user = await this.UserAccountRepository.insertUser({
				user_profile_id: profile.id,
				email,
				password,
				token,
				metadata,
				is_verified,
				logged_at: new Date(),
			}, transaction)

			// user.profile = profile

			await NotificationManager.sendWelcomeNotice(user.id, email, first_name)

			return {
				...user,
				profile,
			}
		})
	}

	private makeid(prefix: string = '', length: number = 4, sum: string = ''): string {
		if (sum.length < length) {
			return this.makeid(prefix, length, sum + POSSIBLE_CHARS.charAt(Math.floor(Math.random() * POSSIBLE_CHARS_LENGTH)))
		}

		return prefix + sum
	}

}

export default new AccountManager()
