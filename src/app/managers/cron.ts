import ManagerModel from 'app/models/manager'
import {
	Connection, EntityManager,
	// EntityManager,
} from 'typeorm'

import NotificationManager from './notification'
import OrderManager from './order'
import ShipmentManager from './shipment'
import ServiceManager from './service'
// import UserManager from './user'
import MigrationManager from './migration'
import PromotionManager from './promotion'
import SchedulerManager from './scheduler'
import FeedbackManager from './feedback'

import { TimeHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { FEEDBACKS, ORDERS, SHIPMENTS, STYLEBOARD_STATUSES } from 'energie/utils/constants/enum'

import cron from 'node-cron'
import { isEmpty } from 'lodash'
import { PromoManager } from '.'
// import { STATEMENT_SOURCES } from 'energie/utils/constants/enum'


// enum TIME_ZONE {
// 	'INDONESIA' = 'Asia/Jakarta',
// 	'SINGAPORE' = 'Asia/Singapore',
// }

type SECONDS = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
	| 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19
	| 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29
	| 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39
	| 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49
	| 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59

type HOURS = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
	| 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19
	| 20 | 21 | 22 | 23

type DAYS = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
	| 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19
	| 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29
	| 30 | 31

type MONTHS = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12


class CronManager extends ManagerModel {

	static __displayName = 'CronManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [
		], async () => {
			await OrderManager.initialize(connection)
			await ShipmentManager.initialize(connection)
			this.resolveOrder()
			this.expireOrder()
			this.notifyExpiringOrder()
			this.postponeShipment()
			this.confirmDelivery()
			this.updateDelivery(false, 0, 4, undefined)
			this.resolveStyleboard()
			this.expireStylistProducts()
			this.updateDiscount()

			// this.scheduledNotification()
			// this.bagReminder()
			// this.cashback()
			// this.bagAbandonment()
			// this.dobLoyaltyPoint()
			// this.userPointExpiredChecking()
			this.watcher()
		})
	}

	// ============================= INSERT =============================
	// ============================= UPDATE =============================
	// ============================= GETTER =============================
	// ============================= DELETE =============================
	// ============================= METHODS ============================

	watcher() {
		cron.schedule(this.schedule(0), () => {
			const now = TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss')
			this.log('watcher running... at ' + now)
			this.transaction(async transaction => {
				await SchedulerManager.get(transaction)
					.then(async (datas: object[]) => {
						Promise.allSettled(datas.map(async (data: any) => {
							return SchedulerManager.executor(SchedulerManager, data.function, transaction)
						}))
					})
				await SchedulerManager.getPreOrder(transaction)
					.then(async (datas: object[]) => {
						this.log('PreOrder running... at ' + now)
						Promise.allSettled(datas.map(async (data: any) => {
							return SchedulerManager.executor(SchedulerManager, data.function, transaction, {
								schedule_type: data.SCHEDULE_TYPE,
								variant_id: data.variant_id
							})
						}))
					})
			})
		})
	}


	cashback() {
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return PromoManager.cashback.cronHandler(transaction)
			})
		})
	}

	// bagAbandonment() {
	// cron.schedule(this.schedule(0, '*/3'), () => {
	/*		const now = TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss')
			OrderManager.bag.getBagAbandonment()
				.then(res => {
					this.log('Bag Abandonment run at ' + now)
				})
		})
	}
	*/

	// userPointExpiredChecking() {
	// 	// run every day
	// 	cron.schedule(this.schedule(0, 0, 0), () => {
	// 		this.transaction(transaction => {
	// 			return UserManager.point.getAllUserPoint(transaction).then(async (points: any) => {
	// 				return Promise.all(points.map(async point => {
	// 					if (!point.metadata.last_point) {
	// 						point.metadata = { last_point: { id: 0 } }
	// 					}
	// 					return UserManager.point.getTotalActivePoint(point.id, point.metadata.last_point.id, transaction).then(async totalActivePoint => {
	// 						if (totalActivePoint[0].totals < point.balance) {
	// 							await UserManager.point.createTransaction(
	// 								point.user_id,
	// 								point.balance - totalActivePoint[0].totals,
	// 								STATEMENT_SOURCES.SYSTEM,
	// 								null,
	// 								'Point expired by system',
	// 								transaction,
	// 							)
	// 							// const item = totalActivePoint[0]
	// 							/*
	// 							UserManager.point.updatePoint(point.user_id, {
	// 								balance: item.totals,
	// 								metadata: {
	// 									last_point: {
	// 										id: item.last_id,
	// 										change: item.last_metadata.change,
	// 										created_at: item.last_created_at,
	// 									},
	// 								},
	// 							}, transaction)
	// 							*/
	// 						}
	// 						return totalActivePoint[0]
	// 					})
	// 				}))
	// 			})
	// 		}).then(res => {
	// 			const now = TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss')
	// 			this.log('userPointExpiredChecking running at... ' + now)
	// 		})
	// 	})
	// }

	// --------------------- DoB Loyalty Point Scheduler
	// dobLoyaltyPoint() {
	// 	// run every day
	// 	cron.schedule(this.schedule(0, 0, 0), () => {
	// 		UserManager.point.getDobPoints()
	// 			.then((userPoints: any) => {
	// 				userPoints.map((userPoint: any) => {
	// 					this.transaction(async transaction => {
	// 						await UserManager.point.depositPoint(
	// 							userPoint.user_id,
	// 							50000,
	// 							STATEMENT_SOURCES.CAMPAIGN,
	// 							null,
	// 							'Birthday Loyalty Program Rewards',
	// 							transaction,
	// 						).then(res => {
	// 							this.log('Point sent to ===> ' + userPoint.user_id)
	// 						})
	// 					})
	// 				})

	// 			})
	// 	})
	// }

	bagReminder() {
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return OrderManager.bag.getUserBag(undefined, {
					get_many: true,
					with_items: true,
					with_user: true,
				}, {
					date: TimeHelper.moment().subtract(6, 'd').toDate(),
					for_test: true,
				}, transaction)
					.then(bags => {
						return bags.filter(bag => bag.items.length > 0 || isEmpty(bag.items))
					})
					.then(bags => {
						return Promise.all(bags.map(bag => {
							NotificationManager.push.sendBagReminderNotification(
								bag.user.id,
								bag.user.profile.first_name,
								'',
								transaction,
							)
						}))
					})
			})
		})
	}

	scheduledNotification() {
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				NotificationManager.push.broadcastScheduledMessage(transaction)
			})
		})
	}


	resolveOrder() {
		// 7 days after the date
		// run every hour
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return OrderManager.detail.getMyMatches(undefined, true, {
					date: TimeHelper.moment().subtract(7, 'd').toDate(),
				}, transaction).then(details => {
					return Promise.all(details.map(async detail => {
						return FeedbackManager.createWithExchange(
							null, // changer user id
							detail.order.user_id,
							detail.id,
							{
								type: FEEDBACKS.VARIANT,
								ref_id: detail.request.ref_id,
								answers: [{
									question_id: 69,
									answer: {
										TAB: 'Keep',
									}
								}]
							},
							undefined, // TODO set note
							transaction,
						).then(async () => {
							return OrderManager.detail.resolveOrderDetail(
								DEFAULTS.SYSTEM_USER_ID,
								detail.id,
								false,
								'Auto-closing order after 7 days of inactivity.',
								transaction,
							)
						})
					})).then(async () => {
						await details.reduce((order_ids, detail) => {
							if (order_ids.indexOf(detail.order_id) === -1) {
								return order_ids.concat(detail.order_id)
							} else {
								return order_ids
							}
						}, []).reduce((promise, order_id) => {
							return promise.then(async () => {
								await OrderManager.update(
									DEFAULTS.SYSTEM_USER_ID,
									order_id,
									{
										status: ORDERS.RESOLVED,
									},
									'Resolving order detail because of delivery confirmation.',
									transaction,
								)
							})
						}, Promise.resolve(null))
					})
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	expireOrder() {
		// 6 hours after order creation date
		// run every hour
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return OrderManager.getPending(TimeHelper.moment().subtract(6, 'h').toDate(), {}, transaction).then(orders => {
					return Promise.all(orders.map(async order => {
						return OrderManager.expireOrder(
							DEFAULTS.SYSTEM_USER_ID,
							order.id,
							'Auto-expiring order after 6 hours of no payment.',
							true,
							transaction,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	resolveStyleboard() {
		// 6 hours after order creation date
		// run every hour
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return ServiceManager.styleboard.getExpired(transaction).then(styleboards => {
					return Promise.all(styleboards.map(async styleboard => {
						return ServiceManager.styleboard.resolveStyleboard(
							DEFAULTS.SYSTEM_USER_ID,
							styleboard.id,
							STYLEBOARD_STATUSES.RESOLVED,
							'Auto-resolving styleboard after expired',
							transaction,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	notifyExpiringStyleboard() {
		// 12 hours before styleboard expired
		// run every hour
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return ServiceManager.styleboard.getExpiring(transaction).then(styleboards => {
					const cutOff = TimeHelper.moment().add(11, 'hour').toDate()

					return Promise.all(styleboards.filter(styleboard => {
						return +styleboard.expired_at >= +cutOff
					}).map(async styleboard => {
						return NotificationManager.sendMatchesExpiringNotice(
							styleboard.user.id,
							styleboard.user.profile.first_name,
							styleboard.orderDetail.id,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	notifyExpiringOrder() {
		// 5 hours after order creation date
		// run every hour
		cron.schedule(this.schedule(0, 0), () => {
			this.transaction(async transaction => {
				return OrderManager.getPending(TimeHelper.moment().subtract(5, 'h').toDate(), {
					with_user: true,
				}, transaction).then(orders => {
					return Promise.all(orders.map(async order => {
						return NotificationManager.sendExpiringOrderNotice(
							order.id,
							order.user_id,
							order.number,
							order.created_at,
							order.user.email,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	expireStylistProducts() {
		// should run once a day at 1 am
		cron.schedule(this.schedule(0, 0, '1'), () => {
			this.transaction(async transaction => {
				return MigrationManager.getStylistVariant(transaction)
					.then(variants => {
						if (variants[0].length > 0) {
							return MigrationManager.batchUpdateVariant(variants, transaction)
						}
					})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	postponeShipment() {
		// check every end of a day for
		// run every day
		cron.schedule(this.schedule(0, 0, 0), () => {
			this.transaction(async transaction => {
				return OrderManager.detail.getPostponed(transaction).then(details => {
					return Promise.all(details.map(async detail => {
						return OrderManager.postponeOrder(
							detail.order_id,
							detail.id,
							'Style profile isn\'t completed yet.',
							true,
							transaction,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	confirmDelivery() {
		// check at every morning and night
		// run every day
		cron.schedule(this.schedule(0, 0, '7,19'), () => {
			this.transaction(async transaction => {
				return ShipmentManager.getDelivered(TimeHelper.moment().subtract(2, 'd').toDate(), transaction).then(shipments => {
					return Promise.all(shipments.map(async shipment => {
						return ShipmentManager.confirmDelivery(
							DEFAULTS.SYSTEM_USER_ID,
							null,
							shipment.id,
							false,
							true,
							transaction,
						)
					}))
				})
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	updateDelivery(
		continuing: boolean = false,
		offset: number = 0,
		limit: number = 8,
		trx: EntityManager | undefined,
	) {
		// check every hour
		// run every day
		if (continuing) {
			return this.withTransaction(trx, async transaction => {
				return ShipmentManager.getActive(offset, limit, transaction).then(async result => {
					if (result.data.length) {
						await Promise.all(result.data.map(shipment => {
							switch (shipment.type) {
								case SHIPMENTS.TIKI:
									return shipment.awb ? ShipmentManager.getLatestShipmentHistory(shipment.type, shipment.awb).then(history => {
										return ShipmentManager.update(
											DEFAULTS.SYSTEM_USER_ID,
											shipment.id,
											{
												status: history.status,
												note: history.note,
											},
											false,
											TimeHelper.moment(history.created_at).toDate(),
											transaction,
										)
									}).catch(err => null) : null
								case SHIPMENTS.SICEPAT:
									return shipment.awb ? ShipmentManager.getLatestShipmentHistory(shipment.type, shipment.awb).then(history => {
										return ShipmentManager.update(
											DEFAULTS.SYSTEM_USER_ID,
											shipment.id,
											{
												status: history.status,
												note: history.note,
											},
											false,
											TimeHelper.moment(history.created_at).toDate(),
											transaction,
										)
									}).catch(err => null) : null
								case SHIPMENTS.MANUAL:
								default:
									return null
							}
						}))
					}

					if (result.count > offset + limit && result.data.length) {
						// there is more
						return this.updateDelivery(true, offset + limit, limit, transaction)
					} {
						// done
						return true
					}
				})
			})
		} else {
			cron.schedule(this.schedule(0, 0), () => {
				this.updateDelivery(true, offset, limit, undefined)
			})
		}
	}

	updateDiscount() {
		// run every day at 1 am and 1 pm
		cron.schedule(this.schedule(0, 0, '1,7,13,19'), () => {
			this.transaction(async transaction => {
				return PromotionManager.updateDiscountManual(transaction)
			}).catch(err => {
				this.warn(err)
			})
		})
	}

	// ============================ PRIVATES ============================
	private schedule(
		seconds: SECONDS | '*' | `*/${SECONDS}` = '*',
		minutes: SECONDS | '*' = '*',
		hours: HOURS | '*' | string = '*',
		days: DAYS | '*' = '*',
		months: MONTHS | '*' = '*',
	) {
		return `${seconds} ${minutes} ${hours} ${days} ${months} *`
	}
}

export default new CronManager()

