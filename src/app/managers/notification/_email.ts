import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import { EmailRepository } from 'app/repositories'

import { Parameter } from 'types/common'

import { TEMPLATES } from 'app/handlers/mandrill'
import SendGridHandler, { SENDGRID_TEMPLATES } from 'app/handlers/send.grid'

import {
	EmailInterface,
	OrderDetailInterface,
} from 'energie/app/interfaces'

import {
	// FormatHelper,
	CommonHelper,
	TimeHelper,
} from 'utils/helpers'

// import { Parameter } from 'types/common'
import { findIndex } from 'lodash'

import { CODES, ORDER_DETAILS } from 'energie/utils/constants/enum'

const REVERSE_TEMPLATE = Object.keys(SENDGRID_TEMPLATES).map(key => {
	return {
		[SENDGRID_TEMPLATES[key]]: key,
	}
}).reduce(CommonHelper.sumObject, {})


class EmailNotificationManager extends ManagerModel {

	static __displayName = 'EmailNotificationManager'

	protected EmailRepository: EmailRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			EmailRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getLatestMail(
		type: keyof typeof TEMPLATES | Array<keyof typeof TEMPLATES> | keyof typeof SENDGRID_TEMPLATES | Array<keyof typeof SENDGRID_TEMPLATES>,
		ref_id_or_ids: number | number[],
		transaction: EntityManager,
	) {
		if (Array.isArray(ref_id_or_ids)) {
			return this.EmailRepository.getLatest(type, ref_id_or_ids, transaction)
		} else {
			return this.EmailRepository.getLatest(type, ref_id_or_ids, transaction)
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async sendExpiringOrderMail(
		orderId: number,
		orderNumber: string,
		orderDate: Date,
		email: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_EXPIRING, {
			email,
		}, {
			vars: {
				orderNumber,
				orderDate: TimeHelper.format(orderDate, 'DD/MM/YYYY'),
				orderStatus: 'Menunggu Pembayaran',
				paymentDate: TimeHelper.format(TimeHelper.moment(orderDate).add(6, 'h').toDate(), 'DD MMM YYYY, HH:mm [WIB]'),
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendVerificationMail(
		userId: number,
		email: string,
		token: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.VERIFY, {
			email,
		}, {
			vars: {
				email,
				code: token,
			},
		}, {
			ref_id: userId,
		})
	}

	@ManagerModel.bound
	async sendForgotPasswordMail(
		userId: number,
		email: string,
		token: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.FORGOT_PASSWORD, {
			email,
		}, {
			vars: {
				email,
				code: token,
			},
		}, {
			ref_id: userId,
		})
	}

	@ManagerModel.bound
	async sendWelcomeMail(
		userId: number,
		email: string,
		firstName: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.WELCOME, {
			email,
		}, {
			vars: {
				firstName,
				email,
				url: `${process.env.MAIN_WEB_URL}profile/style`,
				group_id: 15212,
			},
		}, {
			ref_id: userId,
		})
	}

	@ManagerModel.bound
	async sendCheckoutConfirmationMail(
		orderId: number,
		email: string,
		orderNumber: string,
		orderDate: string,
		deadline: string,
		orderDetails: OrderDetailInterface[],
		status: string,
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_CONFIRMED, {
			email,
		}, {
			vars: {
				email,
				orderDate,
				status,
				paymentDeadline: deadline,
				orderNumber,
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
				url: `${process.env.MAIN_WEB_URL}order/view/ongoing`,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendPaymentCancelationMail(
		orderId: number,
		email: string,
		orderNumber: string,
		orderDetails: OrderDetailInterface[],
		reason: string = 'Unable to process payment',
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_CANCELLATION, {
			email,
		}, {
			vars: {
				email,
				reason,
				orderNumber,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendOrderCancelationMail(
		orderId: number,
		email: string,
		orderNumber: string,
		orderDetails: OrderDetailInterface[],
		reason: string = 'Unable to process payment',
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_CANCELLATION, {
			email,
		}, {
			vars: {
				email,
				reason,
				orderNumber,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendPaymentConfirmationMail(
		orderId: number,
		email: string,
		firstName: string,
		orderNumber: string,
		orderDate: string,
		orderDetails: OrderDetailInterface[],
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.PAYMENT_CONFIRMED, {
			email,
		}, {
			vars: {
				email,
				firstName,
				orderNumber,
				orderDate,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
				styleUrl: `${process.env.MAIN_WEB_URL}profile/style`,
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendPostponedOrderMail(
		userId: number,
		email: string,
		orderNumber: string,
		orderDate: string,
		reason: string = 'Incomplete Style Profile',
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_POSTPONED, {
			email,
		}, {
			vars: {
				email,
				reason,
				orderNumber,
				orderDate,
			},
		}, {
			ref_id: userId,
		})
	}

	@ManagerModel.bound
	async sendExpiredOrderMail(
		orderId: number,
		email: string,
		orderNumber: string,
		orderDate: string,
		orderDetails: OrderDetailInterface[],
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
		url: string = `${process.env.MAIN_WEB_URL}`,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_EXPIRED, {
			email,
		}, {
			vars: {
				email,
				url,
				orderNumber,
				orderDate,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async sendShippingMail(
		shipmentId: number,
		email: string,
		firstName: string,
		trackUrl: string,
		orderNumber: string,
		orderDetails: OrderDetailInterface[],
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_DISPATCHED, {
			email,
		}, {
			vars: {
				firstName,
				url: trackUrl,
				orderNumber,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
			},
		}, {
			ref_id: shipmentId,
		})
	}

	@ManagerModel.bound
	async sendGiftCardMail(
		voucherId: number,
		email: string,
		sender: string,
		matchboxPlan: string,
		promoCode: string,
		giftMessage: string,
		giftId: string,
		giftValidity: string,
		redeemUrl: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.GIFT_CARD, {
			email,
		}, {
			vars: {
				email,
				sender,
				matchboxPlan,
				promoCode,
				giftcardEnd: giftValidity,
				giftMessage,
				url: redeemUrl,
				giftId,
			},
		}, {
			ref_id: voucherId,
		})
	}

	@ManagerModel.bound
	async sendDeliveryMail(
		orderId: number,
		email: string,
		first_name: string,
		orderNumber: string,
		orderDetails: OrderDetailInterface[],
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.ORDER_DELIVERED, {
			email,
		}, {
			vars: {
				email,
				firstName: first_name,
				url: `${process.env.MAIN_WEB_URL}order/view/ongoing`,
				orderNumber,
				products: this.getDeepProductsFromOrderDetail(orderDetails),
			},
		}, {
			ref_id: orderId,
		})
	}

	@ManagerModel.bound
	async refundMail(
		orderId: number,
		email: string,
		orderNumber: string,
		products: Array<{
			variantName: string,
			variantDescription: string,
			price: string,
			qty: number,
		}>,
		subtotal: string,
		stylingFee: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.REFUND, {
			email,
		}, {
			vars: {
				email,
				orderNumber,
				products,
				subtotal,
				stylingFee,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			},
		}, {
			ref_id: orderId,
		})
	}

	// Weekly recomendation preview
	@ManagerModel.bound
	async previewRecommendationMail(
		userId: number,
		email: string,
		image: string,
		firstName: string,
		stylistName: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.RECOMMENDATION, {
			email,
		}, {
			vars: {
				email,
				image,
				firstName,
				stylistName,
				url: process.env.MAIN_WEB_URL,
				group_id: 15213,
			},
		}, {
			ref_id: userId,
		})
	}

	// styleboard preview
	@ManagerModel.bound
	async previewStyleboardMail(
		userId: number,
		email: string,
		image: string,
		FirstName: string,
		stylistName: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.PREVIEW, {
			email,
		}, {
			vars: {
				email,
				image,
				FirstName,
				stylistName,
				url: process.env.MAIN_WEB_URL,
				group_id: 15213,
			},
		}, {
			ref_id: userId,
		})
	}

	@ManagerModel.bound
	async sendHandoverStylistMail(
		userId: number,
		email: string,
		firstName: string,
		newStylistName: string,
		oldStylistName: string,
		image: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.HANDOVER_STYLIST, {
			email,
		}, {
			vars: {
				email,
				firstName,
				newStylist: newStylistName,
				oldStylist: oldStylistName,
				image,
				group_id: 15212,
			},
		}, {
			ref_id: userId,
		})
	}

	// @ManagerModel.bound
	// async tesEmail(
	// 	userId: number,
	// 	email: string,
	// 	firstName: string,
	// 	newStylistName: string,
	// 	oldStylistName: string,
	// 	image: string,
	// ) {
	// 	return this.sendGridEmail(SENDGRID_TEMPLATES.HANDOVER_STYLIST, {
	// 		email,
	// 	}, {
	// 		vars: {
	// 			email,
	// 			firstName,
	// 			newStylist: newStylistName,
	// 			oldStylist: oldStylistName,
	// 			image,
	// 			group_id: 15212,
	// 		},
	// 	}, {
	// 		ref_id: userId,
	// 	})
	// }

	@ManagerModel.bound
	async sendNotificationMail(
		email: string | string[],
		title: string,
		message: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.NOTIFICATION, {
			email,
		}, {
			vars: {
				email,
				emailTitle: title,
				message,
			},
		})
	}

	@ManagerModel.bound
	async sendSPQReminderMail(
		userId: number,
		email: string,
		firstName: string,
	) {
		return this.sendGridEmail(SENDGRID_TEMPLATES.SPQ_REMINDER, {
			email,
		}, {
			vars: {
				email,
				firstName,
				groud_id: 15213,
			},
		}, {
			ref_id: userId,
		})
	}


	async insertEmail(
		data: Parameter<EmailInterface>,
	) {
		return this.EmailRepository.insert(data)
			.then(res => {
				return res
			})
			.catch(err => {
				return err
			})
	}

	// @ManagerModel.bound
	// async sendBagAbandonment(
	// 	listBags: object[],
	// 	transaction: EntityManager,
	// ) {
	// 	const newBagsList = []
	// 	const includes = [
	// 		'yopi@helloyuna.io',
	// 		'yofiyonadio@gmail.com',
	// 	]
	// 	listBags.map((val: any) => {
	// 		if (includes.includes(val.email)) {
	// 			newBagsList.push({
	// 				templateId: SENDGRID_TEMPLATES.BAG_ABANDONMENT,
	// 				email: val.email,
	// 				vars: {
	// 					data: Object.keys(val.refs).map(i => {
	// 						return {
	// 							ref_id: val.refs[i],
	// 							bag_item_id: val.bag_item_ids[i],
	// 							title: val.titles[i],
	// 							brand: val.brands[i],
	// 							type: val.types[i],
	// 							price: val.prices[i],
	// 							retail_price: val.retail_prices[i],
	// 							image: val.image_urls[i],
	// 							// image: 'https://assets.helloyuna.io/' + val.image_urls[i],
	// 						}
	// 					}),
	// 					// --------------
	// 					options: {
	// 						user_id: val.user_id,
	// 						email: val.email,
	// 						bag_id: val.bag_id,
	// 					},
	// 					subject: 'Bag Abandonment',
	// 				},
	// 			})
	// 		}
	// 	})
	
	// 	return this.sendBulk(newBagsList)
	// 		.then(async res => {
	// 			const func: any = []
	// 			res.map(async (val: any) => {
	// 				val.value.data.vars.data.map((vall: any) => {
	// 					const results = {
	// 						status: val.status,
	// 						sendgrid: val.value.result,
	// 						datas: {
	// 							...val.value.data.vars.options,
	// 							items: vall,
	// 						},
	// 					}
	// 					func.push(
	// 						new Promise((resolve, reject) => {
	// 							this.insertEmail({
	// 								type: 'BAG_ABANDONMENT',
	// 								ref_id: vall.bag_item_id,
	// 								code: CODES.SUCCESS,
	// 								metadata: results,
	// 							})
	// 								.then(ress => {
	// 									resolve(results)
	// 								})
	// 								.catch(err => {
	// 									reject(err)
	// 								})
	// 						}))
	// 				})
	// 			})
	// 			return Promise.all(func)
	// 				.then(ress => {
	// 					return ress
	// 				}).catch(err => {
	// 					return err
	// 				})
	// 		})
	// 		.catch(err => {
	// 			return err
	// 		})
	// }

	// ============================ PRIVATES ============================
	private getDeepProductsFromOrderDetail(orderDetails: OrderDetailInterface[] = []) {
		return orderDetails.reduce((init, arr) => {
			switch (arr.type) {
				case ORDER_DETAILS.STYLEBOARD:
					return [
						...init,
						{
							variantName: arr.title,
							// variantDescription: arr.description,
							qty: 1,
							price: arr.price,
						},
					]
				case ORDER_DETAILS.STYLESHEET:
					return [
						...init,
						{
							variantName: arr.title,
							// variantDescription: arr.description,
							qty: 1,
							price: arr.price,
						},
					]
				case ORDER_DETAILS.INVENTORY:
					const i = findIndex(init, { variantName: arr.title, desc: arr.description })

					if (i !== -1) {
						init[i] = { ...init[i], qty: (init[i].qty + 1) }

						return init
					} else {
						return [
							...init,
							{
								variantName: arr.title,
								variantDescription: arr.description,
								qty: 1,
								price: arr.price,
							},
						]
					}
				case ORDER_DETAILS.VOUCHER:
					return [
						...init,
						{
							variantName: arr.title,
							// variantDescription: arr.description,
							qty: 1,
							price: arr.price,
						},
					]
			}
		}, [] as Array<{
			variantName: string,
			desc: string,
			qty: number,
			price: number,
		}>)
	}

	private async sendGridEmail(
		template_name: SENDGRID_TEMPLATES,
		to: {
			email: string | string[],
		},
		{
			vars,
			...message
		}: {
			subject?: string,
			important?: boolean,
			tags?: string[],
			vars: {
				[k: string]: any,
			},
		},
		record: {
			ref_id?: number,
			metadata?: object,
		} = {},
		from: {
			email: string,
			name: string,
		} = {
				email: 'noreply@helloyuna.io',
				name: 'Yuna & Co.',
			},
	): Promise<EmailInterface> {
		return SendGridHandler.send(template_name, to, { vars, ...message }, from)
			.then(data => {
				return this.EmailRepository.insert({
					type: REVERSE_TEMPLATE[template_name],
					ref_id: record.ref_id,
					code: CODES.SUCCESS,
					metadata: {
						sendgrid: data,
						...(record.metadata ?? {}),
					},
				})
			}).catch(err => {
				this.warn(err)

				throw err
			})
	}

}

export default new EmailNotificationManager()
