import {
	ManagerModel,
} from 'app/models'
import { Connection } from 'typeorm'

import EmailNotificationManager from './_email'
import PushNotificationManager from './_push'

import {
	OrderDetailInterface,
} from 'energie/app/interfaces'


class NotificationManager extends ManagerModel {

	static __displayName = 'NotificationManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [], async () => {
			await EmailNotificationManager.initialize(connection)
			await PushNotificationManager.initialize(connection)
		})
	}

	// tslint:disable-next-line:member-ordering
	get email(): typeof EmailNotificationManager {
		return EmailNotificationManager
	}

	// tslint:disable-next-line:member-ordering
	get push(): typeof PushNotificationManager {
		return PushNotificationManager
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async sendVerificationNotice(
		user_id: number,
		email: string,
		token: string,
	) {
		return EmailNotificationManager.sendVerificationMail(
			user_id,
			email,
			token,
		)
	}

	@ManagerModel.bound
	async sendForgotPasswordNotice(
		user_id: number,
		email: string,
		token: string,
	) {
		return EmailNotificationManager.sendForgotPasswordMail(
			user_id,
			email,
			token,
		)
	}

	@ManagerModel.bound
	async sendWelcomeNotice(
		user_id: number,
		email: string,
		first_name: string,
	) {
		return EmailNotificationManager.sendWelcomeMail(
			user_id,
			email,
			first_name,
		)
	}

	@ManagerModel.bound
	async sendCheckoutConfirmationNotice(
		user_id: number,
		email: string,
		order_id: number,
		order_number: string,
		order_date: string,
		deadline: string,
		order_details: OrderDetailInterface[],
		status: string,
		subtotal: string,
		handling: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendCheckoutConfirmationNotification(
				user_id,
				order_id,
				order_details.map(o => o.id),
				transaction,
			)
		})

		return EmailNotificationManager.sendCheckoutConfirmationMail(
			order_id,
			email,
			order_number,
			order_date,
			deadline,
			order_details,
			status,
			subtotal,
			handling,
			discount,
			shipping,
			adjustments,
			wallet,
			roundup,
			total,
		)
	}

	@ManagerModel.bound
	async sendPaymentCancelationNotice(
		order_id: number,
		email: string,
		order_number: string,
		order_details: OrderDetailInterface[],
		reason: string = 'Unable to process payment',
		subtotal: string,
		handling: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return EmailNotificationManager.sendPaymentCancelationMail(
			order_id,
			email,
			order_number,
			order_details,
			reason,
			subtotal,
			handling,
			discount,
			shipping,
			adjustments,
			wallet,
			roundup,
			total,
		)
	}

	@ManagerModel.bound
	async sendPaymentConfirmationNotice(
		user_id: number,
		email: string,
		first_name: string,
		order_id: number,
		order_number: string,
		order_date: string,
		order_details: OrderDetailInterface[],
		subtotal: string,
		handling: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendPaymentConfirmationNotification(
				user_id,
				order_id,
				order_details.map(o => o.id),
				transaction,
			)
		})

		return EmailNotificationManager.sendPaymentConfirmationMail(
			order_id,
			email,
			first_name,
			order_number,
			order_date,
			order_details,
			subtotal,
			handling,
			discount,
			shipping,
			adjustments,
			wallet,
			roundup,
			total,
		)
	}

	@ManagerModel.bound
	async sendPostponedOrderNotice(
		user_id: number,
		email: string,
		order_number: string,
		order_date: string,
		reason: string = 'Incomplete Style Profile',
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendPostponedOrderNotification(
				user_id,
				transaction,
			)
		})

		return EmailNotificationManager.sendPostponedOrderMail(
			user_id,
			email,
			order_number,
			order_date,
			reason,
		)
	}

	@ManagerModel.bound
	async sendExpiredOrderNotice(
		order_id: number,
		email: string,
		order_number: string,
		order_date: string,
		order_details: OrderDetailInterface[],
		subtotal: string,
		handling: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
		order_url: string = `${ process.env.MAIN_WEB_URL }`,
	) {
		return EmailNotificationManager.sendExpiredOrderMail(
			order_id,
			email,
			order_number,
			order_date,
			order_details,
			subtotal,
			handling,
			discount,
			shipping,
			adjustments,
			wallet,
			roundup,
			total,
			order_url,
		)
	}

	@ManagerModel.bound
	async sendExpiringOrderNotice(
		order_id: number,
		user_id: number,
		order_number: string,
		order_date: Date,
		email: string,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendExpiringOrderNotification(
				user_id,
				order_id,
				transaction,
			)
		}).catch(err => null)

		EmailNotificationManager.sendExpiringOrderMail(
			order_id,
			order_number,
			order_date,
			email,
		)

		return true
	}

	@ManagerModel.bound
	async sendShippingNotice(
		shipment_id: number,
		user_id: number,
		email: string,
		first_name: string,
		track_url: string,
		order_number: string,
		order_details: OrderDetailInterface[],
	) {
		this.transaction(transaction => {
			return Promise.all(order_details.map(async orderDetail => {
				return PushNotificationManager.sendShippingNotification(
					user_id,
					orderDetail.id,
					transaction,
				)
			}))
		})

		return EmailNotificationManager.sendShippingMail(
			shipment_id,
			email,
			first_name,
			track_url,
			order_number,
			order_details,
		)
	}

	@ManagerModel.bound
	async sendDeliveryNotice(
		order_id: number,
		user_id: number,
		email: string,
		first_name: string,
		order_number: string,
		order_details: OrderDetailInterface[],
	) {
		this.transaction(transaction => {
			return Promise.all(order_details.map(async orderDetail => {
				return PushNotificationManager.sendDeliveryNotification(
					user_id,
					orderDetail.id,
					transaction,
				)
			}))
		})

		return EmailNotificationManager.sendDeliveryMail(
			order_id,
			email,
			first_name,
			order_number,
			order_details,
		)
	}

	@ManagerModel.bound
	async sendDeliveryConfirmationNotice(
		user_id: number,
		order_id: number,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendDeliveryConfirmationNotification(
				user_id,
				order_id,
				transaction,
			)
		})

		return true
	}

	@ManagerModel.bound
	async sendStartStylingNotice(
		user_id: number,
		type: 'STYLESHEET' | 'STYLEBOARD',
		stylist: string,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendStartStylingNotification(
				user_id,
				type,
				stylist,
				transaction,
			)
		})

		return true
	}

	@ManagerModel.bound
	async sendStylingRequestNotice(
		user_id: number,
		request_id: number,
		email: string,
		first_name: string,
		stylist_image: string,
		stylist_name: string,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendStylingRequestNotification(
				user_id,
				request_id,
				transaction,
			)
		})

		return EmailNotificationManager.previewStyleboardMail(
			user_id,
			email,
			stylist_image,
			first_name,
			stylist_name,
		)
	}

	@ManagerModel.bound
	async sendWeeklyRecomendationNotice(
		user_id: number,
		cluster_id: number,
		email: string,
		first_name: string,
		stylist_image: string,
		stylist_name: string,
		url: string | undefined,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendRecomendationNotification(
				user_id,
				cluster_id,
				url,
				transaction,
			)
		})

		return EmailNotificationManager.previewRecommendationMail(
			user_id,
			email,
			stylist_image,
			first_name,
			stylist_name,
		)
	}

	@ManagerModel.bound
	async sendMatchesExpiringNotice(
		user_id: number,
		first_name: string,
		order_detail_id: number,
	) {
		this.transaction(transaction => {
			return PushNotificationManager.sendMatchesExpiringNotification(
				user_id,
				first_name,
				order_detail_id,
				transaction,
			)
		})

		return true
	}

	@ManagerModel.bound
	async sendOutOfStockNotice(
		order_id: number,
		email: string,
		order_number: string,
		// products with title,color,size,price,qty
		products: Array<{
			variantName: string,
			variantDescription: string,
			price: string,
			qty: number,
		}>,
		subtotal: string,
		handling: string,
		discount: string,
		shipping: string,
		adjustments: string,
		wallet: string,
		roundup: string,
		total: string,
	) {
		return EmailNotificationManager.refundMail(
			order_id,
			email,
			order_number,
			products,
			subtotal,
			handling,
			discount,
			shipping,
			adjustments,
			wallet,
			roundup,
			total,
		)
	}

	@ManagerModel.bound
	async sendGiftCardNotice(
		voucher_id: number,
		email: string,
		sender: string,
		matchbox_plan: string,
		coupon_code: string,
		gift_message: string,
		gift_id: string,
		gift_validity: string,
		redeem_url: string,
	) {
		return EmailNotificationManager.sendGiftCardMail(
			voucher_id,
			email,
			sender,
			matchbox_plan,
			coupon_code,
			gift_message,
			gift_id,
			gift_validity,
			redeem_url,
		)
	}

	// ============================ PRIVATES ============================

}

export default new NotificationManager()
