import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import OneSignalHandler from 'app/handlers/one.signal'

import { NotificationRepository } from 'app/repositories'

import { UserMessageInterface, NotificationInterface } from 'energie/app/interfaces'

import { TimeHelper, CommonHelper } from 'utils/helpers'

import { Parameter } from 'types/common'

import { MESSAGES, NOTIFICATIONS } from 'energie/utils/constants/enum'

import UserManager from '../user'
import { isEmpty } from 'lodash'


class PushNotificationManager extends ManagerModel {

	static __displayName = 'PushNotificationManager'

	protected NotificationRepository: NotificationRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			NotificationRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async insertNotification(
		data: Parameter<NotificationInterface>,
		call_to_action: {
			type?: 'link' | 'page',
			url?: string,
			title?: string,
			route?: string,
			params?: string,
		} | undefined,
		user_ids: number[] | undefined,
		coupon_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.insertNotification({
			...data,
			metadata: {
				...data.metadata,
				call_to_action,
			},
		}, user_ids, coupon_ids, transaction)
			.then(async res => {
				if(res.push_notification) {
					if(!+res.published_at && user_ids && user_ids.length > 0) {
						user_ids.reduce(async (promise, user_id) => {
							return promise.then(() => {
								OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
									title: res.title,
									subtitle: 'Yuna & Co.',
									message: res.description,
								}, {}, true)
							})
						}, Promise.resolve())
					} else {
						const users = await UserManager.getAll(transaction)
						await this.NotificationRepository.addToNotification(res.id, 'user', users.map(user => user.id), transaction)

						if (!+res.published_at) {
							users.reduce(async (promise, user) => {
								return promise.then(() => {
									OneSignalHandler.notify('user', user.id, process.env.MAIN_WEB_URL + 'inbox', {
										title: res.title,
										subtitle: 'Yuna & Co.',
										message: res.description,
									}, {}, true)
								})
							}, Promise.resolve())
						}
					}
				}

				return res
			})
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async seeMessage(message_id: number, user_id: number, transaction: EntityManager) {
		return this.NotificationRepository.seeMessage(message_id, user_id, transaction)
	}

	@ManagerModel.bound
	async seeNotification(notification_id: number, user_id: number, transaction: EntityManager) {
		return this.NotificationRepository.seeNotification(notification_id, user_id, transaction)
	}

	@ManagerModel.bound
	async updateNotification(
		id: number,
		data: Partial<Parameter<NotificationInterface>>,
		call_to_action: {
			type?: 'link' | 'page',
			url?: string,
			title?: string,
			route?: string,
			params?: string,
		} | undefined,
		user_ids: number[] | undefined,
		coupon_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		let metadata: {}

		if (!isEmpty(data.metadata) || !!call_to_action) {
			const notification = await this.NotificationRepository.getNotification(id, false, transaction)
			metadata = CommonHelper.stripUndefined({
				...notification.metadata,
				...data.metadata,
				...(call_to_action ? {call_to_action} : undefined),
			})
		}

		await this.NotificationRepository.updateNotification(
			id,
			CommonHelper.stripUndefined({
				...data,
				metadata,
			}),
			user_ids,
			coupon_ids,
			transaction,
		)
		.then(async () => {
			const orderedNotification = await this.NotificationRepository.getOrderedNofication(transaction)
			orderedNotification.data.map(async (oN, i) =>{
				if (i <= 5) {
					await this.NotificationRepository.updateOrder(oN.id, i + 1, transaction)
				} else {
					await this.NotificationRepository.updateOrder(oN.id, null, transaction)
				}
			})
		})
	}

	@ManagerModel.bound
	async broadcastScheduledMessage(
		transaction: EntityManager,
	) {
		const broadcasts = await this.NotificationRepository.getScheduledNotification(transaction)
		this.log(!isEmpty(broadcasts))

		if (!isEmpty(broadcasts)) {
			broadcasts.reduce(async (promise, broadcast) => {
				return promise.then(async () => {
					if (TimeHelper.moment(broadcast.published_at).diff(TimeHelper.moment(), 'hour') > -2) {
						await this.NotificationRepository.updateNotification(broadcast.id, {
							pushed_at: TimeHelper.moment().toDate(),
						}, undefined, undefined, transaction)

						broadcast.users.reduce(async (p, user) => {
							return p.then(async () => {
								OneSignalHandler.notify('user', user.id, process.env.MAIN_WEB_URL + 'inbox', {
									title: broadcast.title,
									subtitle: 'Yuna & Co.',
									message: broadcast.description,
								}, {}, true)
							})
						}, Promise.resolve())
					}
				})
			}, Promise.resolve())
		}
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filterNotification(
		offset: number = 0,
		limit: number = 12,
		filter: {
			slug?: string,
			search?: string,
			type?: NOTIFICATIONS,
			publish_date?: Date,
			push_notification?: boolean,
			is_featured?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.NotificationRepository.filterNotification(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	getAllMessageAndNotifications(
		user_id: number | undefined,
		is_featured: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getMessageAndNotifications(user_id, is_featured, transaction)
	}

	@ManagerModel.bound
	getNotifications(
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getNotifications(transaction)
	}

	@ManagerModel.bound
	getUnreadMessageAndNotifications(
		user_id: number | undefined,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getUnreadMessageAndNotifications(user_id, transaction)
	}

	@ManagerModel.bound
	getNotification(
		notification_id: number,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getNotification(notification_id, true, transaction)
	}

	@ManagerModel.bound
	getNotificationBySlug<T extends boolean>(
		notification_slug: string,
		with_coupon: T,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getNotification<T>(notification_slug, with_coupon, transaction)
	}

	@ManagerModel.bound
	getMessage(
		message_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.getMessage(message_id, user_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async sendCheckoutConfirmationNotification(
		user_id: number,
		order_id: number,
		order_detail_ids: number[],
		transaction: EntityManager,
	) {
		// Inbox Only
		const isSingleOrderDetail = order_detail_ids.length === 1

		return this.createMessage({
			user_id,
			title: 'Order kamu sudah dikonfirmasi',
			summary: 'Klik di sini untuk melihat order kamu.',
			description: 'Klik di sini untuk melihat order kamu.',
			type: isSingleOrderDetail ? MESSAGES.ORDER_DETAIL : MESSAGES.ORDER,
			ref_id: isSingleOrderDetail ? order_detail_ids[0] : order_id,
			published_at: new Date(),
			expired_at: TimeHelper.moment().add(3, 'day').toDate(),
			metadata: {},
		}, transaction)
	}

	@ManagerModel.bound
	async sendExpiringOrderNotification(
		user_id: number,
		order_id: number,
		transaction: EntityManager,
	) {
		return this.createMessage({
			user_id,
			title: 'Batas pembayaran akan segera berakhir',
			summary: 'Kamu punya waktu kurang dari 1 jam untuk menyelesaikan pembayaranmu.',
			description: 'Kamu punya waktu kurang dari 1 jam untuk menyelesaikan pembayaranmu.',
			type: MESSAGES.ORDER,
			ref_id: order_id,
			published_at: new Date(),
			expired_at: TimeHelper.moment().add(1, 'd').toDate(),
			metadata: {},
		}, transaction)
	}

	// @ManagerModel.bound
	// async sendPaymentCancelation(
	// 	user_id: number,
	// 	order_id: number,
	// 	order_detail_ids: number[],
	// 	transaction: EntityManager,
	// ) {
	// 	// Inbox Only
	// 	const isSingleOrderDetail = order_detail_ids.length === 1

	// 	return this.createMessage({
	// 		user_id,
	// 		title: 'Order kamu dibatalkan',
	// 		summary: 'Klik di sini untuk melihat order kamu.',
	// 		description: 'Klik di sini untuk melihat order kamu.',
	// 		type: isSingleOrderDetail ? MESSAGES.ORDER_DETAIL : MESSAGES.ORDER,
	// 		ref_id: isSingleOrderDetail ? order_detail_ids[0] : order_id,
	// 		published_at: new Date(),
	// 		expired_at: TimeHelper.moment().add(3, 'day').toDate(),
	// 	}, transaction)
	// }

	@ManagerModel.bound
	async sendPaymentConfirmationNotification(
		user_id: number,
		order_id: number,
		order_detail_ids: number[],
		transaction: EntityManager,
	) {
		const isSingleOrderDetail = order_detail_ids.length === 1

		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Pembayaran sudah diverifikasi',
			subtitle: 'Yuna & Co.',
			message: 'Terima kasih. Pesananmu telah kami terima dan akan segera diproses.',
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Pembayaran sudah diverifikasi',
				summary: 'Terima kasih. Pesananmu telah kami terima dan akan segera diproses.',
				description: 'Terima kasih. Pesananmu telah kami terima dan akan segera diproses.',
				type: isSingleOrderDetail ? MESSAGES.ORDER_DETAIL : MESSAGES.ORDER,
				ref_id: isSingleOrderDetail ? order_detail_ids[0] : order_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(3, 'day').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendBagReminderNotification(
		user_id: number,
		name: string,
		item_title: string,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: `${name}, ada yg ketinggalan! ✨`,
			subtitle: '',
			message: `${name}, produk dalam 🛍 shopping bag-mu akan terlihat lebih bagus di lemari kamu. Yuk selesaikan order sekarang!`,
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: `${name}, ada yg ketinggalan! ✨`,
				summary: `${name}, produk dalam 🛍 shopping bag-mu akan terlihat lebih bagus di lemari kamu. Yuk selesaikan order sekarang!`,
				description: `${name}, produk dalam 🛍 shopping bag-mu akan terlihat lebih bagus di lemari kamu. Yuk selesaikan order sekarang!`,
				type: null,
				ref_id: null,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(3, 'day').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	// Harusnya iya
	@ManagerModel.bound
	async sendPostponedOrderNotification(
		user_id: number,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Order kamu diundur',
			subtitle: 'Yuna & Co.',
			message: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Order kamu diundur',
				summary: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
				description: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
				type: MESSAGES.STYLE_PROFILE,
				ref_id: user_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(1, 'day').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	// // Harusnya iya
	@ManagerModel.bound
	async sendIncompleteStyleProfileNotification(
		user_id: number,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Style profile tidak lengkap',
			subtitle: 'Yuna & Co.',
			message: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Style profile tidak lengkap',
				summary: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
				description: 'Mohon lengkapi style profile kamu supaya stylist-mu bisa segera memulainya.',
				type: MESSAGES.STYLE_PROFILE,
				ref_id: user_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(1, 'day').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendStartStylingNotification(
		user_id: number,
		type: 'STYLESHEET' | 'STYLEBOARD',
		stylist: string,
		transaction: EntityManager,
	) {
		// Inbox Only
		return this.createMessage({
			user_id,
			title: 'Styling sudah dimulai',
			summary: `${ stylist } sudah mulai memilih item ${ type === 'STYLEBOARD' ? 'sesuai request-mu.' : 'untuk Instant Matchbox kamu.' }`,
			description: `${stylist} sudah mulai memilih item ${type === 'STYLEBOARD' ? 'sesuai request-mu.' : 'untuk Instant Matchbox kamu.'}`,
			type: MESSAGES.STYLE_PROFILE,
			ref_id: user_id,
			published_at: new Date(),
			expired_at: TimeHelper.moment().add(3, 'day').toDate(),
			metadata: {},
		}, transaction)
	}

	@ManagerModel.bound
	async sendStylingRequestNotification(
		user_id: number,
		request_id: number,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Rekomendasi untuk request kamu sudah siap untuk di-review',
			subtitle: 'Yuna & Co.',
			message: 'Segera review pilihan stylist-mu. Diskon styling akan berakhir dalam 48 jam.',
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Rekomendasi untuk request kamu sudah siap untuk di-review',
				summary: 'Segera review pilihan stylist-mu. Diskon styling akan berakhir dalam 48 jam.',
				description: 'Segera review pilihan stylist-mu. Diskon styling akan berakhir dalam 48 jam.',
				type: MESSAGES.REQUEST,
				ref_id: request_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(36, 'hour').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendRecomendationNotification(
		user_id: number,
		cluster_id: number,
		url: string | undefined,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, url ? url : process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Weekly Recommendation',
			subtitle: '',
			message: 'Stylist-mu menambahkan rekomendasi baru. Check it out!',
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Weekly Recommendation',
				summary: 'Stylist-mu menambahkan rekomendasi baru. Check it out!',
				description: 'Stylist-mu menambahkan rekomendasi baru. Check it out!',
				type: MESSAGES.CLUSTER,
				ref_id: cluster_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(36, 'hour').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendMatchesExpiringNotification(
		user_id: number,
		first_name: string,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Sesi kamu akan segera berakhir',
			subtitle: 'Yuna & Co.',
			message: `${ first_name }, segera review pilihan stylist-mu dan beli yang kamu suka sebelum waktu berakhir.`,
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Sesi kamu akan segera berakhir',
				summary: `${ first_name }, segera review pilihan stylist-mu dan beli yang kamu suka sebelum waktu berakhir.`,
				description: `${ first_name }, segera review pilihan stylist-mu dan beli yang kamu suka sebelum waktu berakhir.`,
				type: MESSAGES.ORDER_DETAIL,
				ref_id: order_detail_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(36, 'hour').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendShippingNotification(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		// Inbox Only
		return this.createMessage({
			user_id,
			title: 'Paket kamu dalam pengiriman',
			summary: `Paket kamu sedang dalam perjalanan ke alamatmu.`,
			description: `Paket kamu sedang dalam perjalanan ke alamatmu.`,
			type: MESSAGES.ORDER_DETAIL,
			ref_id: order_detail_id,
			published_at: new Date(),
			expired_at: TimeHelper.moment().add(3, 'day').toDate(),
			metadata: {},
		}, transaction)
	}

	@ManagerModel.bound
	async sendDeliveryNotification(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return OneSignalHandler.notify('user', user_id, process.env.MAIN_WEB_URL + 'inbox', {
			title: 'Paket kamu sudah tiba',
			subtitle: 'Yuna & Co.',
			message: `Segera lakukan konfirmasi dan beri feedback untuk stylist-mu.`,
		}, {}, false).catch(err => {
			return err
		}).then(result => {
			return this.createMessage({
				user_id,
				title: 'Paket kamu sudah tiba',
				summary: `Segera lakukan konfirmasi dan beri feedback untuk stylist-mu.`,
				description: `Segera lakukan konfirmasi dan beri feedback untuk stylist-mu.`,
				type: MESSAGES.ORDER_DETAIL,
				ref_id: order_detail_id,
				published_at: new Date(),
				expired_at: TimeHelper.moment().add(2, 'day').toDate(),
				metadata: result,
			}, transaction)
		})
	}

	@ManagerModel.bound
	async sendDeliveryConfirmationNotification(
		user_id: number,
		order_id: number,
		transaction: EntityManager,
	) {
		// Inbox Only
		return this.createMessage({
			user_id,
			title: 'Paket sudah berhasil kamu terima',
			summary: `Coba pilihan stylist-mu dan berikan feedback.`,
			description: `Coba pilihan stylist-mu dan berikan feedback.`,
			type: MESSAGES.ORDER,
			ref_id: order_id,
			published_at: new Date(),
			expired_at: TimeHelper.moment().add(3, 'day').toDate(),
			metadata: {},
		}, transaction)
	}

	// ============================ PRIVATES ============================
	private async createMessage(
		data: Parameter<UserMessageInterface>,
		transaction: EntityManager,
	) {
		return this.NotificationRepository.insertMessage(data, transaction)
	}

	// private async createNotification(
	// 	data: Parameter<NotificationInterface>,
	// 	transaction: EntityManager,
	// ) {
	// 	return this.NotificationRepository.insertNotification(data, transaction)
	// }
}

export default new PushNotificationManager()
