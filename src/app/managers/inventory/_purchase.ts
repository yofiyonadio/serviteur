import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import InventoryManager from './index'
import MatchboxManager from '../matchbox'
import ProductManager from '../product'

import { PurchaseRepository, VariantRepository } from 'app/repositories'

import { PurchaseOrderInterface, PurchaseRequestInterface, StylesheetInterface } from 'energie/app/interfaces'
import { Parameter, Without } from 'types/common'
import { CommonHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { STYLESHEET_STATUSES, PURCHASES, PURCHASE_REQUESTS, INVENTORIES, BOOKING_SOURCES } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { isNull } from 'lodash'


class InventoryPurchaseManager extends ManagerModel {

	static __displayName = 'InventoryPurchaseManager'

	protected PurchaseRepository: PurchaseRepository
	protected VariantRepository: VariantRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			PurchaseRepository,
			VariantRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createRequest(
		changer_user_id: number,
		data: Parameter<PurchaseRequestInterface>,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.insertRequest(changer_user_id, {
			...data,
			status: data.status || PURCHASE_REQUESTS.PENDING,
		}, transaction)
	}

	@ManagerModel.bound
	async createOrder(
		changer_user_id: number,
		purchase_request_ids: number[],
		title: string | undefined,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const purchaseRequests = await this.getRequestDetailDeep(purchase_request_ids, transaction)

		return this.PurchaseRepository.insertOrder(changer_user_id, {
			user_id: changer_user_id,
			title: title || `Purchase Order ${purchaseRequests[0].title}`,
			status: PURCHASES.PENDING,
			note,
		}, transaction).then(async purchaseOrder => {
			await Promise.all(purchase_request_ids.map(id => this.PurchaseRepository.updateRequest(changer_user_id, id, {
				purchase_order_id: purchaseOrder.id,
			}, transaction)))

			return true
		})
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateRequest(
		changer_user_id: number,
		purchase_request_id: number,
		update: Partial<Parameter<PurchaseRequestInterface>>,
		transaction: EntityManager,
	) {
		const pr_status = await this.PurchaseRepository.getRequest(purchase_request_id, false, transaction).then(pr => pr.status)

		if(pr_status === PURCHASE_REQUESTS.LOCKED && update.status && update.status !== PURCHASE_REQUESTS.LOCKED) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_010, `Purchase Request #${purchase_request_id} is locked`)
		}

		return this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, update, transaction).then(async isUpdated => {
			// update stylesheet inventories if inventory_id is not set
			if(isUpdated && (update.price || update.retail_price)) {
				const stylesheetInventory = await this.PurchaseRepository.getStylesheetInventoryOf(purchase_request_id, transaction)

				if(!stylesheetInventory.inventory_id) {
					await MatchboxManager.stylesheet.updateInventory(stylesheetInventory.id, {
						price: update.price ?? stylesheetInventory.price,
						retail_price: update.retail_price ?? stylesheetInventory.retail_price,
					}, transaction)
				}
			} else if(isUpdated && update.variant_id) {
				const stylesheetInventory = await this.PurchaseRepository.getStylesheetInventoryOf(purchase_request_id, transaction)

				if(!stylesheetInventory.inventory_id) {
					// get variant price
					const variant = await ProductManager.variant.get(update.variant_id, {}, transaction)

					await MatchboxManager.stylesheet.updateInventory(stylesheetInventory.id, {
						price: variant.price,
						retail_price: variant.retail_price,
					}, transaction)
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async updateOrder(
		changer_user_id: number,
		purchase_order_id: number,
		update: Partial<Parameter<PurchaseOrderInterface>>,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.updateOrder(
			changer_user_id,
			purchase_order_id,
			update,
			transaction,
		).then(async isUpdated => {
			if(isUpdated && update.status === PURCHASES.CANCELLED) {
				// Cancellation, reopen the stylesheet
				const stylesheets = await this.PurchaseRepository.getStylesheetFromOrder([purchase_order_id], transaction).catch(err => [] as StylesheetInterface[])

				if(stylesheets.length) {
					await Promise.all(stylesheets.map(stylesheet => {
						return MatchboxManager.stylesheet.update(changer_user_id, stylesheet.id, {
							status: STYLESHEET_STATUSES.STYLING,
						}, `Re-open because of purchase order #${purchase_order_id} cancellation.`, undefined, transaction)
					}))
				}
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number,
		limit: number,
		filter: {
			status?: PURCHASE_REQUESTS,
			search?: string,
			published?: boolean,
			have_order?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.filterRequest(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	async filterOrder(
		offset: number,
		limit: number,
		filter: {
			status?: PURCHASES,
			search?: string,
			user_id?: number,
		} = {},
		option: {
			include_user?: boolean,
			include_request?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.filterOrder(offset, limit, filter, option, transaction)
	}

	@ManagerModel.bound
	async getRequestDetailWithStylesheet(
		ids: number[],
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.getRequestWithStylesheet(ids, transaction)
	}

	@ManagerModel.bound
	async getRequestDetail(
		ids: number[],
		transaction: EntityManager,
	) {
		// TODO RECHECK
		return this.PurchaseRepository.getRequest(ids, false, transaction)
	}

	@ManagerModel.bound
	async getRequestDetailDeep(
		ids: number[],
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.getRequest(ids, true, transaction)
	}

	@ManagerModel.bound
	async getOrderDetail(
		ids: number[],
		transaction: EntityManager,
	) {
		// TODO RECHECK
		return this.PurchaseRepository.getOrder(ids, 'FALSE', transaction)
	}

	@ManagerModel.bound
	async getOrderDetailDeep(
		ids: number[],
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.getOrder(ids, 'TRUE', transaction)
	}

	@ManagerModel.bound
	async getUserOrder(
		user_id: number,
		status: PURCHASES = PURCHASES.PENDING,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.getUserOrder(user_id, status, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async deleteRequest(
		changer_user_id: number,
		purchase_request_id: number,
		note: string,
		transaction: EntityManager,
	) {
		// TODO: recalculate PO's total price
		return this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, {
			status: PURCHASE_REQUESTS.EXCEPTION,
			note: `Request cancelled. ${note}`,
			deleted_at: new Date(),
		}, transaction)
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async approveRequest(
		changer_user_id: number,
		purchase_request_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, CommonHelper.stripUndefined({
			status: PURCHASE_REQUESTS.APPROVED,
			note,
		}), transaction)
	}

	@ManagerModel.bound
	async rejectRequest(
		changer_user_id: number,
		purchase_request_id: number,
		data: {
			quantity?: number,
			status?: PURCHASE_REQUESTS,
		} = {},
		note: string,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, {
			status: data.status || PURCHASE_REQUESTS.REJECTED,
			...(data.quantity ? { quantity: data.quantity } : {}),
			note,
		}, transaction).then(async isRejected => {
			if(isRejected) {
				// ====================================================
				// ? Remove pR's stylesheet inventory from stylesheet
				// Let stylist remove the inventory from stylesheet
				// ====================================================
				const stylesheetInventory = await MatchboxManager.stylesheet.getInventoryOfPurchaseRequest(purchase_request_id, transaction).catch(err => null)
				// await MatchboxManager.stylesheet.removeInventory(changer_user_id, stylesheetInventory.id, false, false, transaction)

				// ====================================================
				// Re-open the stylesheet
				// ====================================================
				if(!isNull(stylesheetInventory)) {
					if(stylesheetInventory.stylesheet.status === STYLESHEET_STATUSES.PUBLISHED) {
						return MatchboxManager.stylesheet.update(changer_user_id, stylesheetInventory.stylesheet_id, {
							status: STYLESHEET_STATUSES.STYLING,
						}, `Re-open stylesheet because of purchase request #${purchase_request_id} rejection.`, undefined, transaction)
					}
				}
			}

			return isRejected
		})
	}

	@ManagerModel.bound
	async convertRequest(
		changer_user_id: number,
		purchase_request_id: number,
		variant_id: number,
		quantity: number,
		main: boolean,
		data: {
			brand_address_id?: number,
			packet_id?: number,
			rack?: string,
			note?: string,
			status?: INVENTORIES,
		} = {},
		transaction: EntityManager,
	) {
		let purchaseRequest = await this.PurchaseRepository.getRequest(purchase_request_id, false, transaction)
		const isPartial = quantity !== purchaseRequest.quantity
		const status = data.status ? data.status : INVENTORIES.AVAILABLE

		if (quantity > purchaseRequest.quantity) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Quantity is larger than purchase requests\' quantity.')
		} else if (quantity === 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Quantity cannot be zero!')
		}

		// ==================================================
		// 1. Split purchase request
		// If it's a main, then update current purchase req.
		// quantity with parameter's quantity
		// If not, update quantity with the subtraction
		// ammount & override
		// ==================================================
		if (isPartial) {
			// Create a new one
			const newRequest = await this.PurchaseRepository.insertRequest(changer_user_id, {
				user_id: changer_user_id,
				purchase_order_id: purchaseRequest.purchase_order_id,
				variant_id: purchaseRequest.variant_id,
				url: purchaseRequest.url,
				brand_id: purchaseRequest.brand_id,
				seller_id: purchaseRequest.seller_id,
				title: purchaseRequest.title,
				category_id: purchaseRequest.category_id,
				color_id: purchaseRequest.color_id,
				size_id: purchaseRequest.size_id,
				retail_price: purchaseRequest.retail_price,
				price: purchaseRequest.price,
				status: main ? purchaseRequest.status : PURCHASE_REQUESTS.RESOLVED,
				quantity: main ? purchaseRequest.quantity - quantity : quantity,
				note: purchaseRequest.note,
			}, transaction)

			if (main) {
				// Update current purchaseRequest quantity
				await this.PurchaseRepository.updateRequest(changer_user_id, purchaseRequest.id, {
					quantity,
					status: PURCHASE_REQUESTS.RESOLVED,
				}, transaction)

				purchaseRequest.quantity = quantity
			} else {
				// Update current purchaseRequest quantity with the subtraction amount
				await this.PurchaseRepository.updateRequest(changer_user_id, purchaseRequest.id, {
					quantity: purchaseRequest.quantity - quantity,
				}, transaction)

				// Override with new one
				purchaseRequest = newRequest
			}
		} else {
			await this.PurchaseRepository.updateRequest(changer_user_id, purchaseRequest.id, {
				status: PURCHASE_REQUESTS.RESOLVED,
			}, transaction)
		}

		// ==================================================
		// 2. Create inventory
		// If main, book one of the inventories into
		// stylesheet and update the stylesheet inventory
		// ==================================================
		const inventories = await InventoryManager.createManyWithStatus(changer_user_id, {
			variant_id,
			brand_id: purchaseRequest.seller_id,
			brand_address_id: data.brand_address_id || DEFAULTS.YUNA_BRAND_ADDRESS_ID,
			purchase_request_id: purchaseRequest.id,
			price: purchaseRequest.price,
			status,
			rack: data.rack,
			note: data.note,
		}, quantity, status, transaction)

		if (main) {
			const stylesheetInventory = await MatchboxManager.stylesheet.getInventoryOfPurchaseRequest(purchase_request_id, transaction)

			if(status === INVENTORIES.AVAILABLE) {
				// Book inventory into stylesheet
				await InventoryManager.book(changer_user_id, inventories[0].id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.stylesheet_id, 'Auto booking from converting purchase request into inventory.', true, transaction).then(isBooked => {
					if (!isBooked) {
						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Booking failed')
					}
				})
			} else {
				// Reopen stylesheet because of inventory defect / exception
				await MatchboxManager.stylesheet.update(changer_user_id, stylesheetInventory.stylesheet_id, {
					status: STYLESHEET_STATUSES.STYLING,
				}, `Re-open because of inventory #${inventories[0].id} defect / exception.`, undefined, transaction)
			}

			// Update stylesheet inventory
			await MatchboxManager.stylesheet.updateInventory(stylesheetInventory.id, {
				inventory_id: inventories[0].id,
				price: purchaseRequest.price,
				retail_price: purchaseRequest.retail_price,
			}, transaction)
		}

		// ==================================================
		// 3. Update PO if all is converted
		// ==================================================
		await this.updatePurchaseOrderCompletionStatus(changer_user_id, purchaseRequest.purchase_order_id, transaction)

		return inventories.map(i => i.id)
	}

	@ManagerModel.bound
	async despiseRequest(
		changer_user_id: number,
		purchase_request_id: number,
		quantity: number,
		main: boolean,
		note: string | undefined,
		transaction: EntityManager,
	) {
		let purchaseRequest = await this.PurchaseRepository.getRequest(purchase_request_id, false, transaction)
		const isPartial = quantity !== purchaseRequest.quantity

		if (quantity > purchaseRequest.quantity) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Quantity is larger than purchase requests\' quantity.')
		} else if (quantity === 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Quantity cannot be zero!')
		}

		// ==================================================
		// 1. Split purchase request
		// If it's a main, then update current purchase req.
		// quantity with parameter's quantity and update
		// stylesheet status
		// If not, update quantity with the subtraction
		// ammount & override
		// ==================================================
		if (isPartial) {
			// Create a new one
			const newRequest = await this.PurchaseRepository.insertRequest(changer_user_id, {
				user_id: changer_user_id,
				purchase_order_id: purchaseRequest.purchase_order_id,
				variant_id: purchaseRequest.variant_id,
				url: purchaseRequest.url,
				brand_id: purchaseRequest.brand_id,
				seller_id: purchaseRequest.seller_id,
				title: purchaseRequest.title,
				category_id: purchaseRequest.category_id,
				color_id: purchaseRequest.color_id,
				size_id: purchaseRequest.size_id,
				retail_price: purchaseRequest.retail_price,
				price: purchaseRequest.price,
				status: main ? purchaseRequest.status : PURCHASE_REQUESTS.EXCEPTION,
				quantity: main ? purchaseRequest.quantity - quantity : quantity,
				note: main ? purchaseRequest.note : note,
			}, transaction)

			if (main) {
				// Update current purchaseRequest quantity
				await this.PurchaseRepository.updateRequest(changer_user_id, purchaseRequest.id, {
					quantity,
				}, transaction)

				purchaseRequest.quantity = quantity

				// Reject the purchase request
				await this.rejectRequest(changer_user_id, purchaseRequest.id, {
					quantity,
					status: PURCHASE_REQUESTS.EXCEPTION,
				}, note, transaction)
			} else {
				// Update current purchaseRequest quantity with the subtraction amount
				await this.PurchaseRepository.updateRequest(changer_user_id, purchaseRequest.id, {
					quantity: purchaseRequest.quantity - quantity,
				}, transaction)

				// Override with new one
				purchaseRequest = newRequest
			}
		} else {
			await this.rejectRequest(changer_user_id, purchaseRequest.id, {
				status: PURCHASE_REQUESTS.EXCEPTION,
			}, note, transaction)
		}

		if(main) {
			await MatchboxManager.stylesheet.getInventoryOfPurchaseRequest(purchase_request_id, transaction).then(async stylesheetInventory => {
				// Reopen stylesheet because of purchase request exception
				await MatchboxManager.stylesheet.update(changer_user_id, stylesheetInventory.stylesheet_id, {
					status: STYLESHEET_STATUSES.STYLING,
				}, `Re-open because of purchase request #${purchase_request_id} exception.`, undefined, transaction)
			}).catch(err => null)
		}

		// ==================================================
		// 2. Update PO if all is converted
		// ==================================================
		await this.updatePurchaseOrderCompletionStatus(changer_user_id, purchaseRequest.purchase_order_id, transaction)

		return true
	}

	@ManagerModel.bound
	async revokeRequest(
		changer_user_id: number,
		purchase_request_id: number,
		purchase_order_id: number,
		note: string | undefined,
		replacement: Without<PurchaseRequestInterface, 'user_id' | 'purchase_order_id'> | undefined,
		transaction: EntityManager,
	) {
		await this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, CommonHelper.stripUndefined({
			status: PURCHASE_REQUESTS.PENDING,
			purchase_order_id: replacement ? null : purchase_order_id,
			note,
		}), transaction)

		if(replacement) {
			await this.PurchaseRepository.insertRequest(changer_user_id, {
				user_id: changer_user_id,
				purchase_order_id,
				variant_id: replacement.variant_id,
				url: replacement.url,
				brand_id: replacement.brand_id,
				seller_id: replacement.seller_id,
				title: replacement.title,
				category_id: replacement.category_id,
				color_id: replacement.color_id,
				size_id: replacement.size_id,
				retail_price: replacement.retail_price,
				price: replacement.price,
				status: replacement.status,
				quantity: replacement.quantity,
				note: replacement.note,
			}, transaction)

			return true
		} else {
			return true
		}

	}

	@ManagerModel.bound
	async removePurchaseRequestFromOrder(
		changer_user_id: number,
		purchase_request_id: number,
		transaction: EntityManager,
	) {
		return this.PurchaseRepository.updateRequest(changer_user_id, purchase_request_id, {
			purchase_order_id: null,
		}, transaction)
	}

	@ManagerModel.bound
	prices(purchaseRequests: PurchaseRequestInterface[]) {
		return purchaseRequests.filter(pr => !pr.deleted_at).map(pr => {
			return {
				price: pr.price * pr.quantity,
				retail_price: pr.retail_price * pr.quantity,
			}
		}).reduce((sum, curr) => {
			return {
				price: sum.price + curr.price,
				retail_price: sum.retail_price + curr.retail_price,
			}
		}, {
			price: 0,
			retail_price: 0,
		})
	}

	// ============================ PRIVATES ============================
	private async updatePurchaseOrderCompletionStatus(
		changer_user_id: number,
		purchase_order_id: number,
		transaction: EntityManager,
	) {
		const purchaseOrder = await this.PurchaseRepository.getOrder([purchase_order_id], 'SHALLOW', transaction).then(pos => pos[0])
		const haveUnresolvedOrUnexceptionedRequest = purchaseOrder.purchaseRequests.findIndex(pr => {
			return pr.status !== PURCHASE_REQUESTS.RESOLVED && pr.status !== PURCHASE_REQUESTS.EXCEPTION
		}) > -1
		const isAllResolved = purchaseOrder.purchaseRequests.findIndex(pr => {
			return pr.status !== PURCHASE_REQUESTS.RESOLVED
		}) === -1

		if (!haveUnresolvedOrUnexceptionedRequest) {
			if (isAllResolved) {
				await this.PurchaseRepository.updateOrder(changer_user_id, purchaseOrder.id, {
					status: PURCHASES.RESOLVED,
				}, transaction)
			} else {
				await this.PurchaseRepository.updateOrder(changer_user_id, purchaseOrder.id, {
					status: PURCHASES.EXCEPTION,
				}, transaction)
			}
		}
	}

}

export default new InventoryPurchaseManager()
