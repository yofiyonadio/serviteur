import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import InventoryPurchaseManager from './_purchase'
import MasterManager from '../master'
import MatchboxManager from '../matchbox'
import OrderManager from '../order'
import PacketManager from '../packet'
import ProductManager from '../product'

import InventoryRepository from 'app/repositories/inventory'

import { InventoryInterface, PacketInterface  } from 'energie/app/interfaces'
import { InventoryDeepInterface } from 'app/interfaces/deep'

import CommonHelper from 'utils/helpers/common'
import TimeHelper, { Moment } from 'utils/helpers/time'
import { Parameter } from 'types/common'

import { ERROR_CODES, ERRORS } from 'app/models/error'
import { INVENTORIES, BOOKING_SOURCES, STYLESHEET_STATUSES, ORDER_DETAIL_STATUSES } from 'energie/utils/constants/enum'

import { isEmpty } from 'lodash'
class InventoryManager extends ManagerModel {

	static __displayName = 'InventoryManager'

	protected InventoryRepository: InventoryRepository

	private LASTORDERDATE: Moment
	// private SHIPMENTDATE: Moment

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			InventoryRepository,
		], async () => {
			await InventoryPurchaseManager.initialize(connection)
			this.resyncDate()
		})
	}

	// tslint:disable-next-line:member-ordering
	get purchase(): typeof InventoryPurchaseManager {
		return InventoryPurchaseManager
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createManyWithStatus(
		changer_user_id: number,
		data: Parameter<InventoryInterface>,
		quantity: number | undefined,
		code: INVENTORIES | undefined,
		transaction: EntityManager,
	): Promise<InventoryInterface[]> {
		return this.InventoryRepository.insert(changer_user_id, await Promise.all(Array(Math.max(1, quantity || 1)).fill(data).map(async (inventory: InventoryInterface) => {
			return {
				...inventory,
				status: code,
			}
		})), transaction)
	}

	@ManagerModel.bound
	async createStatus(
		changer_user_id: number,
		inventory_id: number,
		code: INVENTORIES.AVAILABLE | INVENTORIES.UNAVAILABLE | INVENTORIES.DEFECT | INVENTORIES.EXCEPTION,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const updatedStatus = await this.InventoryRepository.updateStatus(changer_user_id, inventory_id, code as INVENTORIES.AVAILABLE, note, transaction)
		const { variant_id } = await this.InventoryRepository.get(inventory_id, transaction)
		const variant = await ProductManager.variant.getDetail(variant_id, transaction)

		if (code === 'AVAILABLE') {
			await this.changeVariantAvailability(variant_id, true, transaction)
		} else if ((code === 'UNAVAILABLE' && variant.quantity === 0) ||
		  (code === 'DEFECT' && (variant.quantity === 0 || variant.quantity === 1))) {
			await this.changeVariantAvailability(variant_id, false, transaction)
		}

		return updatedStatus
	}

	private async changeVariantAvailability(
		variant_id: number,
		value: boolean,
		transaction: EntityManager,
	) {
		const variant = await ProductManager.variant.getDetail(variant_id, transaction)
		if (value === false) {
			if (variant.limited_stock === true || variant.have_consignment_stock === false) {
				return await ProductManager.variant.updateAvailable(variant_id, value, transaction)
			}
		} else {
			return await ProductManager.variant.updateAvailable(variant_id, value, transaction)
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		inventory_id: number,
		data: Partial<Parameter<InventoryInterface, 'packet_id'>>,
		packet: Parameter<PacketInterface> | undefined,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.update(changer_user_id, inventory_id, CommonHelper.stripKey(data as InventoryInterface, 'packet_id'), transaction).then(async isUpdated => {
			if(isUpdated && !isEmpty(packet)) {
				await PacketManager.createOrUpdate('INVENTORY', inventory_id, undefined, packet, transaction)
			}

			return isUpdated
		})
		// TODO: update to order detail
	}

	@ManagerModel.bound
	async updateBookingSource(
		changer_user_id: number,
		type: 'VOUCHER_TO_ORDER' | 'ORDER_TO_VOUCHER',
		origin_id: number,
		target_id: number,
		transaction: EntityManager,
	) {
		return Promise.resolve().then(async () => {
			switch (type) {
			case 'VOUCHER_TO_ORDER':
				return this.getBookedId(BOOKING_SOURCES.VOUCHER, origin_id, transaction).catch(err => {
					this.warn(err)

					if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Voucher invalid. Voucher has no item.')
					}

					throw err
				})
			case 'ORDER_TO_VOUCHER':
			default:
				return this.getBookedId(BOOKING_SOURCES.ORDER_DETAIL, origin_id, transaction).catch(err => {
					this.warn(err)

					if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Voucher invalid. Voucher has no item.')
					}

					throw err
				})
			}
		}).then(async inventoryId => {
			switch (type) {
			case 'VOUCHER_TO_ORDER':
				await this.InventoryRepository.book(changer_user_id, inventoryId, BOOKING_SOURCES.ORDER_DETAIL, target_id, 'Move booking from voucher to order', false, transaction)
				break
			case 'ORDER_TO_VOUCHER':
			default:
				await this.InventoryRepository.book(changer_user_id, inventoryId, BOOKING_SOURCES.VOUCHER, target_id, 'Move booking from order to voucher', false, transaction)
				break
			}

			return inventoryId
		})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number,
		limit: number,
		search: string | undefined,
		date: string | undefined,
		status: INVENTORIES | undefined,
		user_id: number | undefined,
		category_ids: number[] | undefined,
		brand_ids: number[] | undefined,
		size_ids: number[] | undefined,
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	) {
		return this.InventoryRepository.filter(
			offset,
			limit,
			search,
			date,
			status,
			user_id,
			category_ids && category_ids.length ? await Promise.all(category_ids.map(cId => MasterManager.category.getChildrenIds(cId, transaction))).then(cIdss => cIdss.reduce(CommonHelper.sumArray, [])) : undefined,
			brand_ids,
			size_ids,
			sort_by,
			transaction,
		)
	}

	@ManagerModel.bound
	async get(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.get(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getWithVariant(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getWithVariant(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getDeepMany(
		inventory_ids: number[],
		available_only = true,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getDeepMany(inventory_ids, available_only, transaction)
	}

	@ManagerModel.bound
	async getListInventory(
		filter: {
			offset?: number,
			limit?: number,
			search?: string,
			sort_by_created_at?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getListInventory(filter, transaction)
	}

	@ManagerModel.bound
	async getLastBooking(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getBooking(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getAddress(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getAddress(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getBookedId(
		source: BOOKING_SOURCES.ORDER_DETAIL | BOOKING_SOURCES.VOUCHER,
		source_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getIdFromBooking(source, source_id, transaction)
	}

	@ManagerModel.bound
	async getBrand(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getBrand(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getDeep(
		inventory_id: number,
		available_only: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getDeep(inventory_id, available_only, transaction)
	}

	@ManagerModel.bound
	async getDeepFlat(
		inventory_id: number,
		available_only: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getDeepFlat(inventory_id, available_only, transaction)
	}

	async getVariantId(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getVariantId(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getIdFromVariant(
		variant_id: number,
		quantity: number = 1,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getIdFromVariant(variant_id, quantity, transaction)
	}

	async getFromVariant(variant_id: number, quantity: number | undefined, deep: true | undefined, available: boolean, trx: EntityManager): Promise<InventoryDeepInterface[]>
	async getFromVariant(variant_id: number, quantity: number | undefined, deep: false | undefined, available: boolean, trx: EntityManager): Promise<InventoryInterface[]>

	@ManagerModel.bound
	async getFromVariant(
		variant_id: number,
		quantity: number = 1,
		deep: boolean = true,
		available: boolean = true,
		transaction: EntityManager,
	) {
		if(deep) {
			return this.InventoryRepository.getFromVariant(variant_id, quantity, true, available, transaction)
		} else {
			return this.InventoryRepository.getFromVariant(variant_id, quantity, false, available, transaction)
		}
	}

	@ManagerModel.bound
	async getHistories(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getHistories(inventory_id, transaction)
	}

	@ManagerModel.bound
	async getPacket(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository
			.getDeep(inventory_id, false, transaction)
			.then(inventory => {
				return inventory.packet_id || inventory.variant.packet_id || inventory.variant.product.packet_id
			}).then(packet_id => {
				if (packet_id) {
					return packet_id
				} else {
					throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Inventory has no packet!')
				}
			})
	}

	@ManagerModel.bound
	async getStatus(
		inventory_id: number,
		transaction: EntityManager,
	) {
		return this.InventoryRepository.getStatus(inventory_id, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async remove(
		inventory_id: number[],
		transaction: EntityManager,
	) {
		return this.InventoryRepository.removeInventory(inventory_id, transaction)
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async book(
		changer_user_id: number,
		inventory_id: number,
		source: BOOKING_SOURCES,
		source_id: number,
		note: string | undefined,
		strict: boolean = false,
		transaction: EntityManager,
	) {
		return this.checkAvailability(inventory_id, transaction).then(async isAvailable => {
			if (isAvailable) {
				const bookInventory = await this.InventoryRepository.book(changer_user_id, inventory_id, source, source_id, note, strict, transaction)
				const { variant_id } = await this.InventoryRepository.get(inventory_id, transaction)
				const variant = await ProductManager.variant.getDetail(variant_id, transaction)
				if (variant.quantity === 0) {
					await this.changeVariantAvailability(variant_id, false, transaction)
				}

				return bookInventory
			}

			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_104, `Inventory ${inventory_id} is unavailable`)
		})
	}

	@ManagerModel.bound
	async cancelBooking(
		changer_user_id: number,
		source: BOOKING_SOURCES.ORDER_DETAIL | BOOKING_SOURCES.STYLESHEET | BOOKING_SOURCES.USER | BOOKING_SOURCES.VOUCHER,
		inventory_id: number,
		status: INVENTORIES.AVAILABLE | INVENTORIES.UNAVAILABLE | INVENTORIES.DEFECT | INVENTORIES.EXCEPTION = INVENTORIES.AVAILABLE,
		source_id: number | undefined,
		note: string | undefined,
		with_side_effect: boolean,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.checkIsBooked(inventory_id, source, source_id, transaction).then(isBooked => {
			if(isBooked) {
				if(status === INVENTORIES.AVAILABLE || status === INVENTORIES.UNAVAILABLE || status === INVENTORIES.DEFECT) {
					return this.InventoryRepository.updateStatus(changer_user_id, inventory_id, status || INVENTORIES.AVAILABLE, note as string || 'Booking cancelled.', transaction)
				} else {
					return this.InventoryRepository.updateStatus(changer_user_id, inventory_id, status || INVENTORIES.EXCEPTION, note as string || 'Booking cancelled.', transaction)
				}
			}

			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_104, `Booking is already cancelled`)
		}).then(async isUpdated => {
			const { variant_id } = await this.InventoryRepository.get(inventory_id, transaction)
			if (status === 'AVAILABLE') {
				await this.changeVariantAvailability(variant_id, true, transaction)
			}
			if(isUpdated && with_side_effect && source === BOOKING_SOURCES.STYLESHEET && source_id) {
				const stylesheet = await MatchboxManager.stylesheet.getShallow(source_id, transaction)

				if (stylesheet.status === STYLESHEET_STATUSES.PUBLISHED) {
					if(status === INVENTORIES.UNAVAILABLE) {
						// ====================================================
						// Pack the order detail
						// Check if all is done
						// ====================================================
						const detail = await MatchboxManager.stylesheet.getInventoriesFromStylesheet(source_id, true, transaction)
						const isAllPacked = detail.stylesheetInventories.findIndex(sI => {
							return sI.inventory ? sI.inventory.status !== INVENTORIES.UNAVAILABLE : true
						}) === -1

						if(isAllPacked && detail.orderDetail && detail.orderDetail.status === ORDER_DETAIL_STATUSES.PROCESSING) {
							return await MatchboxManager.stylesheet.packStylesheet(detail.id, transaction).then(async () => {
								return OrderManager.detail.update(changer_user_id, detail.orderDetail.id, {
									status: ORDER_DETAIL_STATUSES.PRIMED,
								}, 'Stylesheet packing success.', transaction)
							}).catch(async err => {
								const _note = err instanceof ErrorModel ? err.detail : typeof err.message === 'string' && err.message || 'Something went wrong when packing.'
								await OrderManager.detail.update(changer_user_id, detail.orderDetail.id, {
									status: ORDER_DETAIL_STATUSES.PROCESSING,
								}, _note, transaction)

								return _note
							})
						}

						return true
					} else {
						// ====================================================
						// Re-open the stylesheet
						// ====================================================
						return MatchboxManager.stylesheet.update(changer_user_id, source_id, {
							status: STYLESHEET_STATUSES.STYLING,
						}, `Re-open stylesheet because of booking cancelation on inventory: #${inventory_id}. Note: ${note}`, undefined, transaction)
					}
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async unpackInventory(
		changer_user_id: number,
		inventory_id: number,
		stylesheet_id: number,
		status: INVENTORIES.AVAILABLE | INVENTORIES.DEFECT | INVENTORIES.EXCEPTION | INVENTORIES.BOOKED = INVENTORIES.EXCEPTION,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const inventory = await this.InventoryRepository.get(inventory_id, transaction)

		if(inventory.status === INVENTORIES.UNAVAILABLE || inventory.status === INVENTORIES.BOOKED) {
			return Promise.resolve().then(() => {
				if(status === INVENTORIES.BOOKED) {
					// ====================================================
					// Changing back to booked
					// do nothing
					// ====================================================
					return this.InventoryRepository.book(changer_user_id, inventory_id, BOOKING_SOURCES.STYLESHEET, stylesheet_id, note, false, transaction)
				} else {
					return this.InventoryRepository.updateStatus(changer_user_id, inventory_id, status as INVENTORIES.EXCEPTION, note, transaction).then(async isUpdated => {
						if(isUpdated) {
							// ====================================================
							// Re-open the stylesheet
							// stylesheet must be changed to styling again
							// Only if already published
							// ====================================================
							const stylesheet = await MatchboxManager.stylesheet.getShallow(stylesheet_id, transaction)

							if(stylesheet.status === STYLESHEET_STATUSES.PUBLISHED) {
								await MatchboxManager.stylesheet.update(changer_user_id, stylesheet_id, {
									status: STYLESHEET_STATUSES.STYLING,
								}, `Re-open stylesheet because of unpacking of inventory: #${ inventory_id }. Note: ${ note }`, undefined, transaction)
							}
						}

						return isUpdated
					})
				}

			}).then(async isUpdated => {
				if(isUpdated) {
					// ====================================================
					// Re-process the order detail
					// Order detail must be changed back to processing
					// Only if already primed
					// ====================================================
					const orderDetail = await MatchboxManager.stylesheet.getOrderDetailOf(stylesheet_id, transaction)

					if (orderDetail.status === ORDER_DETAIL_STATUSES.PRIMED) {
						await OrderManager.detail.update(changer_user_id, orderDetail.id, {
							status: ORDER_DETAIL_STATUSES.PROCESSING,
						}, `Re-process order detail because of unpacking of inventory: #${ inventory_id }. Note: ${ note }`, transaction)
					}
				}

				return isUpdated
			})
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Inventory is not currently packed nor booked.')
		}
	}

	@ManagerModel.bound
	async bookFromVariant(
		changer_user_id: number,
		variant_id: number,
		source: BOOKING_SOURCES,
		source_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.getFromVariant(variant_id, 1, false, true, transaction).then(inventories => {
			return this.book(changer_user_id, inventories[0].id, source, source_id, note, true, transaction)
		})
	}

	get lastOrderDate() {
		this.resyncIfPassed()
		return this.LASTORDERDATE
	}

	shipmentDate(orderDate) {
		this.resyncIfPassed()
		return TimeHelper.calculateShipmentDate(3, orderDate)
	}

	localShipmentDate(orderDate) {
		this.resyncIfPassed()
		return TimeHelper.calculateShipmentDate(7, orderDate)
	}

	foreignShipmentDate(orderDate) {
		this.resyncIfPassed()
		return TimeHelper.calculateShipmentDate(11, orderDate)
	}

	// ============================ PRIVATES ============================
	private async checkAvailability(
		inventory_id: number,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.InventoryRepository.getStatus(inventory_id, transaction).then(status => {
			if(status.status === INVENTORIES.AVAILABLE) {
				return true
			} else {
				return false
			}
		}).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return false
			}

			throw err
		})
	}

	private async checkIsBooked(
		inventory_id: number,
		source: BOOKING_SOURCES,
		source_id: number | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		if(source_id) {
			return this.InventoryRepository.getBooked(source, source_id, inventory_id, transaction).then(inventory => {
				return true
			}).catch(err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					return false
				}

				throw err
			})
		} else {
			return this.InventoryRepository.getBookedSimple(source, inventory_id, transaction).then(inventory => {
				return true
			}).catch(err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					return false
				}

				throw err
			})
		}
	}

	private resyncDate() {
		const cutoff = TimeHelper.moment().hour() >= 13
		this.LASTORDERDATE = cutoff ? TimeHelper.moment().add(1, 'd').hour(13).minute(0).second(0).millisecond(0) : TimeHelper.moment().hour(13).minute(0).second(0).millisecond(0)
		// this.SHIPMENTDATE = TimeHelper.calculateShipmentDate(cutoff ? 2 : 1)
	}

	private resyncIfPassed() {
		if (+this.LASTORDERDATE < Date.now()) {
			this.resyncDate()
		}
	}

}

export default new InventoryManager()
