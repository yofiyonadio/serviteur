import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import AccountManager from '../account'

import {
	UserWalletRepository,
} from 'app/repositories'

import { UserBankInterface } from 'energie/app/interfaces'
import { UserRequestWithdrawMetadataInterface } from 'energie/app/interfaces/user.request'

import { Parameter } from 'types/common'
import { FormatHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES, ERRORS } from 'app/models/error'
import { STATEMENT_SOURCES, STATEMENTS, CODES, WALLET_STATUSES } from 'energie/utils/constants/enum'


class UserWalletManager extends ManagerModel {

	static __displayName = 'UserWalletManager'

	protected UserWalletRepository: UserWalletRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserWalletRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createBank(
		user_id: number,
		data: Parameter<UserBankInterface, 'user_id'>,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.createBank(user_id, data, transaction)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateBank(
		bank_id: number,
		data: Parameter<UserBankInterface, 'user_id'>,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.updateBank(bank_id, data, transaction)
	}

	// ============================= GETTER =============================
	async getWalletAndPromoTransaction(transaction: EntityManager) {
		return this.UserWalletRepository.getWalletAndPromoTransaction(transaction)
	}

	@ManagerModel.bound
	async getBank(
		id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getBanks([id], transaction).then(banks => banks[0])
	}

	@ManagerModel.bound
	async getBanksOfUser(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getBanksOfUser(user_id, transaction)
	}

	@ManagerModel.bound
	async getDetailOfUser(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getDetailOfUser(user_id, transaction)
	}

	@ManagerModel.bound
	async getRequest(
		request_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getRequest(request_id, user_id, transaction)
	}

	@ManagerModel.bound
	async getWallet(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getWalletOfUser(user_id, transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return this.UserWalletRepository.createWallet(user_id, {
					balance: 0,
				}, transaction)
			}

			throw err
		})
	}

	@ManagerModel.bound
	async getWalletTransaction(
		user_wallet_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getWalletTransaction('wallet', user_wallet_id, {limit: 0, offset: 0}, transaction)
	}

	@ManagerModel.bound
	async getWalletTransactionOfUser(
		user_id: number,
		filter: {
			limit?: number,
			offset?: number,
		} = {},
		transaction: EntityManager,
	) {
		const wallet = await this.getWallet(user_id, transaction)
		return this.UserWalletRepository.getWalletTransaction('wallet', wallet.id, filter, transaction)
	}

	@ManagerModel.bound
	async getWalletTransactionOf(
		source: STATEMENT_SOURCES,
		ref_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getWalletTransaction(source, ref_id, {limit: 0, offset: 0}, transaction)
	}

	@ManagerModel.bound
	async getRecursiveTransactionsOf(
		wallet_transaction_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getRecursiveTransactionsOf(wallet_transaction_id, transaction)
	}

	@ManagerModel.bound
	async getTransactionsOf(
		wallet_transaction_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getTransactionsOf(wallet_transaction_id, transaction)
	}

	@ManagerModel.bound
	async getTransaction(
		wallet_transaction_id: number,
		transaction: EntityManager,
	) {
		return this.UserWalletRepository.getWalletTransaction('transaction', wallet_transaction_id, {limit: 0, offset: 0}, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async depositInto(
		user_wallet_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		if(amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot deposit a value less than zero.')
		}

		return this.UserWalletRepository.createWalletTransaction(user_wallet_id, {
			amount,
			type: STATEMENTS.CREDIT,
			source,
			status: WALLET_STATUSES.APPLIED,
			note,
			ref_id,
		}, transaction).then(async wT => {
			const wallet = await this.UserWalletRepository.getWallet(user_wallet_id, transaction)

			return this.UserWalletRepository.updateWallet(user_wallet_id, {
				balance: wallet.balance + amount,
			}, transaction).then(isUpdated => {
				if(isUpdated) {
					return wT
				} else {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
				}
			})
		})
	}

	@ManagerModel.bound
	async withdrawFrom(
		user_wallet_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot deposit a value less than zero.')
		}

		const wallet = await this.UserWalletRepository.getWallet(user_wallet_id, transaction)

		if (
			wallet.balance - amount
			< 0
		) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Withdraw conceded, wallet balance is currently ${ FormatHelper.currency(wallet.balance, 'IDR') }.`)
		}

		return this.UserWalletRepository.createWalletTransaction(user_wallet_id, {
			amount,
			type: STATEMENTS.DEBIT,
			source,
			status: WALLET_STATUSES.APPLIED,
			note,
			ref_id,
		}, transaction).then(async wT => {
			return this.UserWalletRepository.updateWallet(user_wallet_id, {
				balance: wallet.balance - amount,
			}, transaction).then(isUpdated => {
				if (isUpdated) {
					return wT
				} else {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
				}
			})
		})
	}

	@ManagerModel.bound
	async pendingDepositInto(
		user_wallet_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot deposit a value less than zero.')
		}

		return this.UserWalletRepository.createWalletTransaction(user_wallet_id, {
			amount,
			type: STATEMENTS.CREDIT,
			source,
			status: WALLET_STATUSES.PENDING,
			ref_id,
			note,
		}, transaction)
	}

	@ManagerModel.bound
	async pendingWithdrawFrom(
		user_wallet_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot deposit a value less than zero.')
		}

		const wallet = await this.UserWalletRepository.getWallet(user_wallet_id, transaction)

		if (
			wallet.balance - amount
			< 0
		) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Withdraw conceded, wallet balance is currently ${FormatHelper.currency(wallet.balance, 'IDR')}.`)
		}

		return this.UserWalletRepository.createWalletTransaction(user_wallet_id, {
			amount,
			type: STATEMENTS.DEBIT,
			source,
			status: WALLET_STATUSES.APPLIED,
			note,
			ref_id,
		}, transaction).then(async wT => {
			return this.UserWalletRepository.updateWallet(user_wallet_id, {
				balance: wallet.balance - amount,
			}, transaction).then(isUpdated => {
				if (isUpdated) {
					return wT
				} else {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
				}
			})
		})
	}

	@ManagerModel.bound
	async applyPendingTransaction(
		changer_user_id: number,
		transaction_id: number,
		transaction: EntityManager,
	) {
		const trx = await this.UserWalletRepository.getWalletTransaction('transaction', transaction_id, {limit: 0, offset: 0}, transaction)

		if(trx.status === WALLET_STATUSES.PENDING) {
			await this.UserWalletRepository.updateTransactionStatus(changer_user_id, transaction_id, WALLET_STATUSES.APPLIED, transaction).then(isUpdated => {
				if(!isUpdated) {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating transaction status.')
				}
			})

			if (trx.type === STATEMENTS.CREDIT) {
				const wallet = await this.UserWalletRepository.getWallet(trx.user_wallet_id, transaction)

				return this.UserWalletRepository.updateWallet(trx.user_wallet_id, {
					balance: wallet.balance + trx.amount,
				}, transaction).then(isUpdated => {
					if (!isUpdated) {
						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
					}

					return isUpdated
				})
			} else {
				return true
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Transaction already ${ trx.status.toLowerCase() }.`)
		}
	}

	@ManagerModel.bound
	async cancelPendingTransaction(
		changer_user_id: number,
		transaction_id: number,
		transaction: EntityManager,
	) {
		const trx = await this.UserWalletRepository.getWalletTransaction('transaction', transaction_id, {limit: 0, offset: 0}, transaction)

		if (trx.status === WALLET_STATUSES.PENDING) {
			await this.UserWalletRepository.updateTransactionStatus(changer_user_id, transaction_id, WALLET_STATUSES.CANCELLED, transaction).then(isUpdated => {
				if (!isUpdated) {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating transaction status.')
				}
			})

			if (trx.type === STATEMENTS.CREDIT) {
				return true
			} else {
				// Re insert
				const wallet = await this.UserWalletRepository.getWallet(trx.user_wallet_id, transaction)

				return this.UserWalletRepository.updateWallet(trx.user_wallet_id, {
					balance: wallet.balance + trx.amount,
				}, transaction).then(isUpdated => {
					if (!isUpdated) {
						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
					}

					return isUpdated
				})
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Transaction already ${trx.status.toLowerCase()}.`)
		}
	}

	@ManagerModel.bound
	async approveWithdrawRequest(
		request_id: number,
		note: string | null,
		transaction: EntityManager,
	) {
		const request = await this.UserWalletRepository.getRequest(request_id, null, transaction)

		if(request.status !== CODES.PENDING) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request is already approved / declined.')
		}

		return this.UserWalletRepository.updateRequest(request_id, {
			status: CODES.SUCCESS,
			note,
		}, transaction)
	}

	@ManagerModel.bound
	async cancelWithdrawRequest(
		request_id: number,
		transaction: EntityManager,
	) {
		const request = await this.UserWalletRepository.getRequest(request_id, null, transaction)
		const wallet = await this.UserWalletRepository.getWalletOfUser(request.user_id, transaction)

		if (request.status !== CODES.PENDING) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request is already approved / declined.')
		}

		return this.UserWalletRepository.cancelWithdrawRequest(request_id, transaction).then(isUpdated => {
			if(isUpdated) {
				return this.depositInto(wallet.id, (request.metadata as UserRequestWithdrawMetadataInterface).amount, STATEMENT_SOURCES.REQUEST, request_id, 'Withdraw request cancelled.', transaction)
			} else {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Cancellation failed. Cannot update database.')
			}
		})
	}

	@ManagerModel.bound
	async declineWithdrawRequest(
		request_id: number,
		note: string,
		transaction: EntityManager,
	) {
		const request = await this.UserWalletRepository.getRequest(request_id, null, transaction)
		const wallet = await this.UserWalletRepository.getWalletOfUser(request.user_id, transaction)

		if (request.status !== CODES.PENDING) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request is already approved / declined.')
		}

		return this.UserWalletRepository.updateRequest(request_id, {
			status: CODES.FAILED,
			note,
		}, transaction).then(isUpdated => {
			if (isUpdated) {
				return this.depositInto(wallet.id, (request.metadata as UserRequestWithdrawMetadataInterface).amount, STATEMENT_SOURCES.REQUEST, request_id, 'Withdraw request declined.', transaction)
			} else {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Decline failed. Cannot update database.')
			}
		})
	}

	@ManagerModel.bound
	async requestWithdraw(
		user_id: number,
		amount: number,
		password: string,
		transaction: EntityManager,
	) {
		return AccountManager.validatePassword(user_id, password, transaction).then(async isValid => {
			if(isValid) {

				const banks = await this.UserWalletRepository.getBanksOfUser(user_id, transaction).catch(err => null)

				if(!banks) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'User have no bank account.')
				}

				const wallet = await this.UserWalletRepository.getWalletOfUser(user_id, transaction)

				if(amount < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Minimum withdraw amount is ${ FormatHelper.currency(DEFAULTS.MINIMUM_WITHDRAW_AMOUNT, 'IDR') }`)
				}

				return this.UserWalletRepository.createWithdrawRequest(user_id, amount, transaction).then(request => {
					return this.withdrawFrom(wallet.id, amount, STATEMENT_SOURCES.REQUEST, request.id, 'Withdraw request.', transaction)
				})
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Password doesn\'t match.')
			}
		})
	}

	// ============================ PRIVATES ============================

}

export default new UserWalletManager()
