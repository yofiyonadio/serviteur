import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	OrderRepository,
} from 'app/repositories'

import { ORDER_DETAIL_STATUSES } from 'energie/utils/constants/enum'

import DEFAULTS from 'utils/constants/default'


class UserOrderManager extends ManagerModel {

	static __displayName = 'UserOrderManager'

	protected OrderRepository: OrderRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	get getAll() {
		return this.OrderRepository.getUserOrders
	}

	@ManagerModel.bound
	async getPaidCount(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getUserPaidOrderCount(user_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async haveOngoingCustomStyling(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getUserCustomStylingOrder(user_id, transaction).then(orders => {
			return orders.findIndex(order => {
				return order.orderDetails.findIndex(detail => {
					// Order is ongoing
					return (detail.status === ORDER_DETAIL_STATUSES.PENDING || detail.status === ORDER_DETAIL_STATUSES.PROCESSING || detail.status === ORDER_DETAIL_STATUSES.PRIMED)
					// Styleboard service is Custom Styling
						&& detail.styleboard.service_id === DEFAULTS.CUSTOM_STYLING_SERVICE_ID
					// Styleboard is not expired
						&& +new Date(detail.styleboard.expired_at) > Date.now()
				}) > -1
			}) > -1
		}).catch(() => {
			return false
		})
	}

	// ============================ PRIVATES ============================

}

export default new UserOrderManager()
