import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'


import {
	UserPointRepository,
} from 'app/repositories'

import { FormatHelper } from 'utils/helpers'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import { STATEMENTS, STATEMENT_SOURCES, WALLET_STATUSES } from 'energie/utils/constants/enum'

class UserPointManager extends ManagerModel {

	static __displayName = 'UserPointManager'

	protected UserPointRepository: UserPointRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserPointRepository,
		])
	}

	// ============================= INSERT =============================
	async createTransaction(
		user_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		const point = await this.UserPointRepository.getPoint(user_id, transaction)

		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Amount cannot less than zero.')
		}

		if (point.balance <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot use point, balance less than zero.')
		}

		if (point.balance - amount < 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Amount used exceed point balance')
		}

		return this.UserPointRepository.createTransaction(point.id, {
			amount,
			type: STATEMENTS.DEBIT,
			source,
			ref_id,
			note,
			status: WALLET_STATUSES.APPLIED,
		}, transaction).then(async pointTransaction => {
			await this.UserPointRepository.updatePoint(user_id, {
				balance: point.balance - amount,
			}, transaction)

			return pointTransaction
		})
	}

	async depositPoint(
		user_id: number,
		amount: number,
		source: STATEMENT_SOURCES,
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		const point = await this.UserPointRepository.getPoint(user_id, transaction)
			.catch(err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					return this.UserPointRepository.createPoint(user_id, transaction)
				}

				throw err
			})

		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Amount cannot less than zero.')
		}

		return this.UserPointRepository.createTransaction(point.id, {
			amount,
			type: STATEMENTS.CREDIT,
			source,
			ref_id,
			note,
			status: WALLET_STATUSES.APPLIED,
		}, transaction).then(async pointTransaction => {
			this.log('injecting point')
			await this.UserPointRepository.updatePoint(user_id, {
				balance: point.balance + amount,
			}, transaction)
			this.log('point injected')

			return pointTransaction
		})
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async getPoint(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserPointRepository.getPoint(user_id, transaction)
			.catch(err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					return this.UserPointRepository.createPoint(user_id, transaction)
				}

				throw err
			})
	}

	get getDetailTransaction() {
		return this.UserPointRepository.getDetailTransaction
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	@ManagerModel.bound
	async withdrawFrom(
		user_id: number,
		user_point_id: number,
		amount: number,
		source: STATEMENT_SOURCES, 
		ref_id: number | null,
		note: string,
		transaction: EntityManager,
	) {
		if (amount <= 0) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot deposit a value less than zero.')
		}

		const point = await this.UserPointRepository.getPoint(user_id, transaction)
		if (
			point.balance - amount
			< 0
		) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Withdraw conceded, wallet balance is currently ${ FormatHelper.currency(point.balance, 'IDR') }.`)
		}

		return this.UserPointRepository.createTransaction(user_point_id, {
			amount,
			type: STATEMENTS.DEBIT,
			source,
			status: WALLET_STATUSES.APPLIED,
			note,
			ref_id,
		}, transaction).then(async wT => {
			return this.UserPointRepository.updatePoint(user_id, {
				balance: point.balance - amount,
			}, transaction).then(isUpdated => {
				if (isUpdated) {
					return wT
				} else {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed updating wallet balance.')
				}
			})
		})
	}

	// ============================ PRIVATES ============================

}

export default new UserPointManager()
