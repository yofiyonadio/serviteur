import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

// import UserAnswerManager from './answer'
import AccountManager from '../account'
import AssetManager from '../asset'
import NotificationManager from '../notification'
import OrderManager from '../order'
import UserAddressManager from './_address'
import UserAnswerManager from './_answer'
import UserOrderManager from './_order'
import UserRequestManager from './_request'
import UserWalletManager from './_wallet'
import UserCardManager from './_card'
import UserPointManager from './_point'
import UserReferralManager from './_referral'
import UserWardrobeManager from './_wardrobe'
import ServiceManager from '../service'

import {
	UserAccountRepository,
	UserProfileRepository,
	UserAnswerRepository,
	UserRequestRepository,
} from 'app/repositories'

import {
	UserInterface,
	UserProfileInterface,
	UserRequestInterface,
} from 'energie/app/interfaces'
import { UserRequestChangeStylistMetadataInterface } from 'energie/app/interfaces/user.request'

import { CommonHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

import { ERRORS, ERROR_CODES } from 'app/models/error'
import { NONCES } from '../asset'
import { TOKENS, USER_REQUESTS } from 'energie/utils/constants/enum'

import bcrypt from 'bcrypt'



class UserManager extends ManagerModel {

	static __displayName = 'UserManager'

	protected UserAccountRepository: UserAccountRepository
	protected UserProfileRepository: UserProfileRepository
	protected UserAnswerRepository: UserAnswerRepository
	protected UserRequestRepository: UserRequestRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserAccountRepository,
			UserAnswerRepository,
			UserProfileRepository,
			UserRequestRepository,
		], async () => {
			await UserAddressManager.initialize(connection)
			await UserAnswerManager.initialize(connection)
			await UserOrderManager.initialize(connection)
			await UserRequestManager.initialize(connection)
			await UserWalletManager.initialize(connection)
			await UserCardManager.initialize(connection)
			await UserPointManager.initialize(connection)
			await UserReferralManager.initialize(connection)
			await UserWardrobeManager.initialize(connection)
		})
	}

	get point(): typeof UserPointManager {
		return UserPointManager
	}

	// tslint:disable-next-line:member-ordering
	get address(): typeof UserAddressManager {
		return UserAddressManager
	}

	// tslint:disable-next-line:member-ordering
	get answer(): typeof UserAnswerManager {
		return UserAnswerManager
	}

	// tslint:disable-next-line:member-ordering
	get order(): typeof UserOrderManager {
		return UserOrderManager
	}

	// tslint:disable-next-line:member-ordering
	get request(): typeof UserRequestManager {
		return UserRequestManager
	}

	// tslint:disable-next-line:member-ordering
	get wallet(): typeof UserWalletManager {
		return UserWalletManager
	}

	// tslint:disable-next-line:member-ordering
	get card(): typeof UserCardManager {
		return UserCardManager
	}

	// tslint:disable-next-line:member-ordering
	get referral(): typeof UserReferralManager {
		return UserReferralManager
	}

	get wardrobe(): typeof UserWardrobeManager {
		return UserWardrobeManager
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		user: UserInterface,
		transaction: EntityManager,
	): Promise<UserInterface> {
		return this.UserAccountRepository.insertUser(user, transaction)
	}

	@ManagerModel.bound
	async createNote(
		user_id: number,
		creator_id: number,
		note: string,
		transaction: EntityManager,
	): Promise<UserProfileInterface> {
		return this.UserAccountRepository.insertNote({
			user_id,
			creator_id,
			note,
		}, transaction)
	}

	@ManagerModel.bound
	async sendFeedbackEmail(
		user_id: number,
		order_id: number,
		url: string,
		transaction: EntityManager,
	): Promise<UserProfileInterface> {
		const user = await this.UserAccountRepository.getById(user_id, true, transaction)
		const order = await OrderManager.get(order_id, transaction)

		// return NotificationManager.email.sendDeprecatedFeedbackMail(user.email, user.profile.first_name, user.profile.last_name, user.id, url)
		return NotificationManager.email.sendDeliveryMail(order.id, user.email, user.profile.first_name, order.number, order.orderDetails)
	}

	@ManagerModel.bound
	async createChangeStylistRequest(
		user_id: number,
		metadata: UserRequestChangeStylistMetadataInterface,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<UserRequestInterface> {
		const request = await this.getChangeStylistRequest(user_id, transaction).catch(() => null)

		if (!request) {
			return this.UserRequestRepository.insert(user_id, USER_REQUESTS.CHANGE_STYLIST, metadata, note, transaction)
		} else {
			return request
		}
	}

	@ManagerModel.bound
	async createProfile(
		user: UserInterface,
		profile: UserProfileInterface,
		transaction: EntityManager,
	): Promise<UserProfileInterface> {
		return this.UserProfileRepository.insert(profile, transaction).then(async _profile => {
			await this.UserAccountRepository.updateUser(user.id, {
				user_profile_id: _profile.id,
			}, transaction)
			return _profile
		})
	}

	@ManagerModel.bound
	async uploadProfile(
		user: UserInterface & {
			profile?: UserProfileInterface,
		},
		image: string,
		order: number | undefined,
		transaction: EntityManager,
	) {
		let profile: UserProfileInterface = user.profile

		if (!profile) {
			profile = await this.UserProfileRepository.insert({}, transaction)

			user.profile = profile

			await this.UserAccountRepository.updateUser(user.id, {
				user_profile_id: profile.id,
			}, transaction)
		}

		const profileImage = await AssetManager.upload('user', user.id, image, NONCES.PROFILE, {}, {}, order, transaction).catch(() => null)

		profile.asset_id = profileImage.id

		await this.UserProfileRepository.updateProfile(user.id, {
			asset_id: profileImage.id,
		}, transaction)

		return profileImage
	}

	@ManagerModel.bound
	async addStylecard(
		user_id: number,
		cluster_id: number,
		stylecard_ids: number[],
		transaction: EntityManager,
	) {
		const stylecards = await ServiceManager.stylecard.get({ stylecard_id_or_ids: stylecard_ids, many: true }, {}, transaction)
		const checkStylecardCluster = stylecards.reduce((i, stylecard) => stylecard.cluster_id === cluster_id ? i.concat(true) : i.concat(false), [])

		if (!!cluster_id && checkStylecardCluster.findIndex(stylecard => stylecard === false) > -1) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Stylecard not in cluster #${cluster_id}`)
		}

		return this.UserProfileRepository.addStylecards(user_id, stylecard_ids, transaction)
			.then(() => 'true')
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		user_id: number,
		data: Partial<Parameter<UserProfileInterface>> & {
			email?: string,
			is_verified?: boolean,
			is_blocked?: boolean,
		},
		send_email_otp: boolean,
		transaction: EntityManager,
	): Promise<boolean> {
		if (data.email) {
			await this.UserAccountRepository.updateUser(user_id, CommonHelper.stripUndefined({
				email: data.email,
				is_verified: data.is_verified,
			}), transaction)

			// Automatically send token of verification
			if (send_email_otp) {
				await AccountManager.sendToken(TOKENS.VERIFICATION, data.email, true, transaction)
			}
		}

		if (data.is_blocked) {
			await this.UserAccountRepository.updateUser(user_id, {
				is_blocked: true,
				token: null,
			}, transaction)
		}

		await this.UserProfileRepository.updateProfile(user_id, CommonHelper.stripUndefined({
			first_name: data.first_name,
			last_name: data.last_name,
			phone: data.phone,
			stage_name: data.stage_name,
			bio: data.bio,
			cluster_id: data.cluster_id,
		}), transaction)

		return true
	}

	@ManagerModel.bound
	async updateUserWhatsappSubs(
		user_id: number,
		is_whatsapp: boolean,
		whatsapp_subscription: boolean,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.updateUser(user_id, {
			metadata: {
				is_whatsapp,
				whatsapp_subscription,
			},
		}, transaction)
	}

	@ManagerModel.bound
	async updateNote(
		note_id: number,
		note: string,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.updateNote(note_id, {
			note,
		}, transaction)
	}

	@ManagerModel.bound
	async updateProfile(
		user_id: number,
		data: Partial<Parameter<UserProfileInterface>>,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.updateProfile(user_id, data, transaction)
	}

	get viewStylecard() {
		return this.UserProfileRepository.viewStylecard
	}

	@ManagerModel.bound
	async setRole(
		user_id: number,
		role: string,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.insertRole({
			user_id,
			role,
		}, transaction)
	}

	@ManagerModel.bound
	async setStylist(
		user_id: number,
		stylist_id: number | undefined | boolean,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.setStylist(user_id, stylist_id, transaction)
	}

	@ManagerModel.bound
	async checkSpqForUpdate(transaction: EntityManager) {
		const users = await this.UserProfileRepository.getAllUser(transaction)

		await users.reduce((promise, user) => {
			return promise.then(async () => {
				const start = Date.now()
				await UserAnswerManager.isStyleProfileCompletedDeep(user, transaction)
					.then(async isTrue => {
						if (user.profile.is_style_profile_completed !== isTrue) {
							return this.updateProfile(user.id, {
								is_style_profile_completed: isTrue,
								style_profile_completion_updated_at: new Date(),
							}, transaction)
						}
					})

				this.log(Date.now() - start, 'ms', `Check and update SPQ user #${user.id}`)
			})
		}, Promise.resolve(null))
	}

	// ============================= GETTER =============================
	get getStylecards() {
		return this.UserProfileRepository.getUserStylecard
	}

	@ManagerModel.bound
	async filter(
		offset: number | undefined,
		limit: number | undefined,
		filter: {
			user_id?: number,
			search?: string,
			spq_status?: 'complete' | 'incomplete',
			role?: string,
			have_order?: boolean,
			have_stylist?: boolean,
			date?: string,
			eligible_for_order?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.filter(
			offset,
			limit,
			filter,
			transaction,
		)
	}

	@ManagerModel.bound
	async getAll(transaction: EntityManager) {
		return this.UserAccountRepository.get(transaction)
	}

	@ManagerModel.bound
	async get(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getById(user_id, true, transaction)
	}

	@ManagerModel.bound
	async getChangeStylistRequest(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getChangeStylistRequest(user_id, transaction)
	}

	@ManagerModel.bound
	async getStylist(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getUserStylist(user_id, transaction)
	}

	@ManagerModel.bound
	async getByEmail(
		email: string,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getByEmail(email, false, transaction)
	}

	@ManagerModel.bound
	async getCompleteProfile(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.getCompleteProfile(user_id, transaction)
	}

	@ManagerModel.bound
	async getDetail(
		user_ids: number[],
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getDetail(user_ids, false, transaction)
	}

	@ManagerModel.bound
	async getOrInsertRefCode(
		user_id: number,
		transaction: EntityManager,
	) {
		const user = await this.getUserProfile({ user_id }, transaction)

		if (!!user.profile.referral_code) {
			return user.profile.referral_code
		} else {
			const refCode = `${user.profile.first_name}${user_id}`
			await this.insertRefCode(user.user_profile_id, refCode, transaction)
			return refCode
		}
	}

	@ManagerModel.bound
	async insertRefCode(
		user_profile_id: number,
		refCode: string,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.insertRefCode(user_profile_id, refCode, transaction)
	}

	@ManagerModel.bound
	async getUserProfile(
		filter: {
			user_id?: number,
			referral_code?: string,
		},
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.getUserProfile(filter, transaction)
	}

	@ManagerModel.bound
	async getDetailDeep(
		user_ids: number[],
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getDetail(user_ids, true, transaction)
	}

	@ManagerModel.bound
	async getNote(
		user_id: number,
		note_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getNote(user_id, note_id, transaction)
	}

	@ManagerModel.bound
	async getNotes(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getNotes(user_id, transaction)
	}

	@ManagerModel.bound
	async getRoles(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.getRoles(user_id, transaction)
			.then(roles => {
				return roles.map(role => {
					return role.role
				})
			})
	}

	@ManagerModel.bound
	async getUserWithNoStylist(
		offset: number = 0,
		limit: number = 20,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.getUserWithNoStylist(offset, limit, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async deleteNote(
		note_id: number,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.deleteNote(note_id, transaction)
	}

	@ManagerModel.bound
	async deleteRole(
		user_id: number,
		role: string,
		transaction: EntityManager,
	) {
		return this.UserAccountRepository.deleteRole(user_id, role, transaction)
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async authenticate(
		token: string,
		transaction: EntityManager,
	) {
		if (!token) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Token not provided')
		}

		return this.UserAccountRepository.getUserByToken(token, transaction)
	}

	@ManagerModel.bound
	async changePassword(
		user: UserInterface,
		password1: string,
		password2: string,
		currentPassword: string | undefined,
		transaction: EntityManager,
	) {
		if (!password1 || !password2) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Passwords not provided')
		}

		if (password1 !== password2) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Passwords don\'t match')
		}

		return Promise.resolve().then(() => {
			if (user.password) {
				return bcrypt.compare(currentPassword, user.password).then(isMatch => {
					if (!isMatch) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Current password do not match')
					} else {
						return
					}
				})
			} else if (user.is_verified) {
				return
			} else {
				return
				// throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_102, 'User has no password and unverified')
			}
		}).then(async () => {
			return AccountManager.createToken(
				user.email,
				password1,
			).then(async ([passhash]) => {
				user.password = passhash

				await this.UserAccountRepository.updateUser(user.id, {
					password: passhash,
				}, transaction)

				return true
			})
		})
	}

	@ManagerModel.bound
	async sendIncompleteSPQEmail(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.getCompleteProfile(user_id, transaction).then(async user => {
			if (user.profile.is_style_profile_completed) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'User already completed SPQ')
			} else {
				return await NotificationManager.email.sendSPQReminderMail(
					user.id,
					user.email,
					user.profile.first_name,
				)
			}
		})
	}

	@ManagerModel.bound
	async insertCoupon(
		user_id: number,
		coupon_id: number,
	) {
		return this.UserAccountRepository.insertCoupon(user_id, coupon_id)
	}

	// ============================ PRIVATES ============================

	// authenticate(token: string) {
	// 	return IntegrationService
	// 		.authenticate(token)
	// 		.then(async ({
	// 			id,
	// 			firstName,
	// 			lastName,
	// 			email,
	// 			phone,
	// 			isVerified,
	// 			isStyleProfileCompleted,
	// 			roles,
	// 		}) => {
	// 			return this.UserAccountRepository.getOne('UserRecord', {
	// 				id,
	// 			}).catch(err => {
	// 				if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
	// 					// DO REGISTER

	// 					return this.UserAccountRepository.softRegister(
	// 						id,
	// 						firstName,
	// 						lastName,
	// 						email,
	// 						phone,
	// 						isVerified,
	// 						isStyleProfileCompleted,
	// 						roles,
	// 						// user.defaultAddressId,
	// 						// user.profileMediaId,
	// 						// user.addressIds,
	// 						// user.batchIds,
	// 						// user.mediaIds
	// 					).then(user => {
	// 						// Get style profile
	// 						return IntegrationService.getTypeformByUserIdOrEmail({
	// 							userId: user.id,
	// 							email: user.email,
	// 						}, token).then(typeform => {
	// 							if(typeform) {
	// 								return UserAnswerManager.parseTypeform(user, typeform)
	// 							} else {
	// 								return user
	// 							}
	// 						})
	// 					}).catch(_err => {
	// 						this.warn(_err)

	// 						throw _err
	// 					})
	// 				}

	// 				throw err
	// 			})
	// 		})
	// }
}

export default new UserManager()
