import {
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
	UserWardrobeRepository,
} from 'app/repositories'

import {
	UserWardrobeInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

class UserWardrobeManager extends ManagerModel {

	static __displayName = 'UserWardrobeManager'

	protected UserWardrobeRepository: UserWardrobeRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserWardrobeRepository,
		])
	}

	// ============================= INSERT =============================
	async createWardrobe(
		data: Parameter<UserWardrobeInterface>,
		transaction: EntityManager,
	) {
		const {
			user_id,
			product_name,
			category,
			image,
			type,
			color_id,
			price,
		} = data

		return this.UserWardrobeRepository.createWardrobe(
			user_id,
			product_name,
			category,
			image,
			type,
			color_id,
			price,
			transaction)
			.then((wardrobe: UserWardrobeInterface) => {
				return wardrobe
			})
	}

	// ============================= UPDATE =============================

	async updateWardrobe(
		id: number,
		data: Partial<UserWardrobeInterface>,
		transaction: EntityManager,
	) {
		return this.UserWardrobeRepository.updateWardrobe(id, data, transaction)
	}

	// ============================= GETTER =============================
	async getUserWardrobe(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserWardrobeRepository.getUserWardrobe(user_id, transaction)
	}

	async getUserWardrobeDetail(
		id: number,
		source: string,
		transaction: EntityManager,
	) {
		return this.UserWardrobeRepository.getUserWardrobeDetail(id, source, transaction)
	}

	// ============================= DELETE =============================


	async deleteWardrobe(
		id: number,
		source: string,
		transaction: EntityManager,
	) {
		return this.UserWardrobeRepository.deleteWardrobe(id, source, transaction)
	}

	// ============================ PRIVATES ============================

}

export default new UserWardrobeManager()

