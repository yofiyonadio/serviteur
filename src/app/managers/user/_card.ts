import {
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
	UserCardRepository,
} from 'app/repositories'

import {
	UserCardInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'

class UserCardManager extends ManagerModel {

	static __displayName = 'UserCardManager'

	protected UserCardRepository: UserCardRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserCardRepository,
		])
	}

	// ============================= INSERT =============================
	async saveCard(
		data: Parameter<UserCardInterface>,
		transaction: EntityManager,
	) {
		const {
			user_id,
			token,
			masked_card_number,
			expired,
			name,
		} = data

		return this.UserCardRepository.insertCard({
			user_id,
			token,
			masked_card_number,
			expired,
			name,
		}, transaction)
		.then(card => {
			return {
				id: card.id,
				name: card.name,
				masked_card_number: card.masked_card_number,
				expired: card.expired,
				token: card.token,
			}
		})
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async getCards(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserCardRepository.getCards(user_id, transaction).catch(() => [] as UserCardInterface[])
	}

	// ============================= DELETE =============================

	// ============================ PRIVATES ============================

}

export default new UserCardManager()

