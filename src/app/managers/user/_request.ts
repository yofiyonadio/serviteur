import {
	ManagerModel, ErrorModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import UserManager from '../user'
import UserWalletManager from './_wallet'
import ServiceManager from '../service'

import {
	UserRequestRepository,
} from 'app/repositories'

import DEFAULTS from 'utils/constants/default'
import { CODES, USER_REQUESTS, STATEMENT_SOURCES, STYLEBOARD_STATUSES, STYLECARD_STATUSES } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { UserRequestWithdrawMetadataInterface } from 'energie/app/interfaces/user.request'
import { StylecardInterface } from 'energie/app/interfaces'
import { isEmpty, isNull } from 'lodash'
import { CommonHelper } from 'utils/helpers'
import {
	JenkinsHandler,
	OneSignalHandler
} from 'app/handlers'
import { isNullOrUndefined } from 'util'


class UserRequestManager extends ManagerModel {

	static __displayName = 'UserRequestManager'

	protected UserRequestRepository: UserRequestRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserRequestRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createStylingRequest(
		user_id: number,
		stylist_id: number,
		data: {
			product?: string,
			outfit?: string,
			attire?: string,
			note?: string,
			asset_ids?: number[],
			variant_id?: number,
		},
		transaction: EntityManager,
	) {
		const {
			product,
			outfit,
			attire,
			note,
			asset_ids,
			variant_id,
		} = data

		let stylistId: number
		// check if user has stylist
		if (isNullOrUndefined(stylist_id)) {
			stylistId = await UserManager.setStylist(user_id, true, transaction)

			// update stylist
			await UserManager.update(user_id, {
				stylist_id: stylistId,
			}, false, transaction)
		} else {
			stylistId = stylist_id
		}

		// get their latest styling request
		const _request = await this.UserRequestRepository.getLatestStylingRequest(user_id, transaction).catch(() => null)

		// resolve it
		if(!isNull(_request)) {
			if (_request.styleboard.status === STYLEBOARD_STATUSES.STYLING) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_106, 'Cannot create new request, you have ongoing request')
			}

			await this.UserRequestRepository.update(_request.id, {
				cancelled_at: new Date(),
			}, transaction)

			await ServiceManager.styleboard.update(_request.user_id, _request.styleboard.id, {
				status: STYLEBOARD_STATUSES.EXCEPTION,
			}, 'User Creating New Request', transaction)
		}

		// creating new user_request
		return await this.UserRequestRepository.insert(user_id, USER_REQUESTS.STYLING, data as any, note, transaction)
			.then(async request => {
				const service = await ServiceManager.get(1, false, transaction)

				// create stylecards first
				const stylecards: StylecardInterface[] = await Array(service.metadata.stylecard_count).fill(null).reduce((promise, stylecard) => {
					return promise.then(async (arr: StylecardInterface[]) => {
						return [...arr, await ServiceManager.stylecard.create({
							matchbox_id: DEFAULTS.CUSTOM_MATCHBOX_ID,
							stylist_id: stylistId,
							status: STYLECARD_STATUSES.PENDING,
							count: service.metadata.stylecard_item_count,
							note,
							is_master: false,
							is_bundle: false,
							is_public: false,
							inventory_only: false,
						}, asset_ids, undefined, transaction)]
					})
				}, Promise.resolve([]))

				// create styleboard
				const styleboard =  await ServiceManager.styleboard.create({
					service_id: 1,
					status: STYLEBOARD_STATUSES.PENDING,
					user_id,
					stylist_id: stylistId,
					product,
					outfit,
					attire,
					note,
					variant_id,
				} as any, stylecards.map(s => s.id), asset_ids, transaction)

				return {
					styleboard_id: styleboard.id,
					request_id: request.id,
					request_metadata: request.metadata,
				}
			})
			.then(async result => {
				// update user_request -> add styleboard_id to metadata and update status to success
				const updateRequest = await this.UserRequestRepository.update(result.request_id, {
					status: CODES.SUCCESS,
					metadata: {
						...result.request_metadata,
						styleboard_id: result.styleboard_id,
					} as any,
				}, transaction)

				const stylist = await UserManager.get(stylistId, transaction)

				await JenkinsHandler.notifyByNewStyleboard(stylist.profile?.phone)

				return updateRequest ? result : null
			})

	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateStylingRequest(
		user_id: number,
		data: {
			type?: string,
			product?: string,
			outfit?: string,
			attire?: string,
			note?: string,
			variant_id?: number,
		},
		asset_ids: number[] = [],
		reasonNote: string,
		transaction: EntityManager,
	) {
		const request = await this.UserRequestRepository.getLatestStylingRequest(user_id, transaction)

		if (request.styleboard.status !== (STYLEBOARD_STATUSES.PENDING || STYLEBOARD_STATUSES.STYLING)) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot update resolved request')
		}

		return await this.UserRequestRepository.update(request.id, {
			metadata: {
				...request.metadata,
				...(CommonHelper.stripUndefined({...data, asset_ids})),
			} as any,
		}, transaction)
		.then(async isUpdated => {
			if(isUpdated) {
				if(data.type === 'outfit') {
					await ServiceManager.styleboard.update(user_id, request.styleboard.id, {
						product: null,
						attire: data.attire,
						outfit: data.outfit,
						note: data.note,
						variant_id: data.variant_id,
					} as any, reasonNote, transaction)
				} else {
					await ServiceManager.styleboard.update(user_id, request.styleboard.id, {
						attire: null,
						outfit: null,
						product: data.product,
						note: data.note,
						variant_id: data.variant_id,
					} as any, reasonNote, transaction)
				}

				if(!isEmpty(asset_ids)) {
					await ServiceManager.styleboard.updateStylboardUserAsset(request.styleboard.id, asset_ids, transaction)
				}
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			user_id?: number,
			code?: CODES,
			type?: USER_REQUESTS,
			is_active?: boolean,
			is_expired?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.UserRequestRepository.filter(offset, limit, search, filter, transaction)
	}

	@ManagerModel.bound
	get(
		request_id: number,
		config: {
			type?: USER_REQUESTS,
			user_id?: number,
		} = {},
		transaction: EntityManager) {
		return this.UserRequestRepository.get(request_id, config, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async cancelSylingRequest(
		user_id: number,
		transaction: EntityManager,
	) {
		const request = await this.UserRequestRepository.getLatestStylingRequest(user_id, transaction)

		if (request.styleboard.status !== (STYLEBOARD_STATUSES.PENDING || STYLEBOARD_STATUSES.STYLING)) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot cancel resolved request')
		}

		return await this.UserRequestRepository.update(request.id, {
			cancelled_at: new Date(),
		}, transaction).then(async isUpdated => {
			if(isUpdated) {
				const styleboard = request.styleboard

				await ServiceManager.styleboard.update(request.user_id, styleboard.id, {
					status: STYLEBOARD_STATUSES.EXCEPTION,
				}, 'User Cancelled Request', transaction)
			}

			return isUpdated
		})
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async reject(request_id: number, note: string, transaction: EntityManager) {
		const request = await this.UserRequestRepository.get(request_id, {}, transaction)

		switch (request.type) {
		case USER_REQUESTS.CHANGE_STYLIST:
			// send notification
			OneSignalHandler.notify('user', request.user_id, process.env.MAIN_WEB_URL + 'inbox', {
				title: 'Your request is rejected!',
				subtitle: 'Yuna & Co.',
				message: note,
			}, {}, true)
			break
		case USER_REQUESTS.WITHDRAW:
			// Re-insert into wallet
			const wallet = await UserWalletManager.getWallet(request.user_id, transaction)
			await UserWalletManager.depositInto(wallet.id, (request.metadata as UserRequestWithdrawMetadataInterface).amount, STATEMENT_SOURCES.REQUEST, request.id, note || 'Withdraw request rejected.', transaction)

			break
		}

		return this.UserRequestRepository.update(request_id, {
			status: CODES.FAILED,
			note,
		}, transaction)
	}

	@ManagerModel.bound
	async approve(
		request_id: number,
		note: string | undefined,
		data: {
			stylist_id?: number,
			asset_id?: number,
		} = {},
		transaction: EntityManager,
	) {
		const request = await this.UserRequestRepository.get(request_id, {}, transaction)

		switch(request.type) {
		case USER_REQUESTS.CHANGE_STYLIST:
			if(!data.stylist_id) {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_103, 'Replacement stylist id not provided.')
			}

			await UserManager.updateProfile(request.user_id, {
				stylist_id: data.stylist_id,
			}, transaction)

			// send notification
			OneSignalHandler.notify('user', request.user_id, process.env.MAIN_WEB_URL + 'inbox', {
				title: 'Your request is approved!',
				subtitle: 'Yuna & Co.',
				message: note,
			}, {}, true)

			return await this.UserRequestRepository.update(request_id, {
				status: CODES.SUCCESS,
				note,
			}, transaction)
		case USER_REQUESTS.WITHDRAW:
			if(!data.asset_id) {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_103, 'Receipt image not provided')
			}

			return await this.UserRequestRepository.update(request_id, {
				status: CODES.SUCCESS,
				note,
				metadata: {
					...request.metadata,
					asset_id: data.asset_id,
				},
			}, transaction)
		}
	}

	// ============================ PRIVATES ============================
}

export default new UserRequestManager()
