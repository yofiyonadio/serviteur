import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
  UserReferralRepository,
} from 'app/repositories'

import { UserReferralInterface } from 'energie/app/interfaces'

import { Parameter } from 'types/common'


class UserReferralManager extends ManagerModel {

  static __displayName = 'UserReferralManager'

  protected UserReferralRepository: UserReferralRepository

  async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserReferralRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async insert(
		data: Partial<Parameter<UserReferralInterface>>,
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.insert(data, transaction)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateStatus(
		user_id: number,
		user_reference: number,
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.updateStatus(user_id, user_reference, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getReferral(
		user_id: number,
		filter: {
			status: string,
		},
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.getReferral(user_id, {status: filter.status}, transaction)
	}

	@ManagerModel.bound
	async getDetailReferral(
		user_id: number,
		user_reference: number,
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.getDetailReferral(user_id, user_reference, transaction)
	}

	@ManagerModel.bound
	async verifyReferral(
		referral_code: string,
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.verifyReferral(referral_code, transaction)
	}

	@ManagerModel.bound
	async getByUserRef(
		user_reference: number,
		transaction: EntityManager,
	) {
		return this.UserReferralRepository.getByUserRef(user_reference, transaction)
	}
}

export default new UserReferralManager()
