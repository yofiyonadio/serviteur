import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import MasterManager from '../master'

import {
	UserAnswerRepository,
	UserProfileRepository,
	UserAccountRepository,
} from 'app/repositories'

import {
	UserInterface,
	UserProfileInterface,
	UserAnswerInterface,
	QuestionInterface,
} from 'energie/app/interfaces'

import { QUESTIONS } from 'energie/utils/constants/enum'

import CacheHelper from 'utils/helpers/cache'

import {
	isEqual,
} from 'lodash'
import { CommonHelper } from 'utils/helpers'


class UserAnswerManager extends ManagerModel {

	static __displayName = 'UserAnswerManager'

	protected UserAccountRepository: UserAccountRepository
	protected UserAnswerRepository: UserAnswerRepository
	protected UserProfileRepository: UserProfileRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserAccountRepository,
			UserAnswerRepository,
			UserProfileRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	get getLikes() {
		return this.UserAnswerRepository.getLikes
	}

	@ManagerModel.bound
	async getByIds(
		userId: number,
		ids: number[],
		transaction: EntityManager,
	) {
		return this.UserAnswerRepository.getByQuestionIds(userId, ids, null, true, transaction)
	}

	@ManagerModel.bound
	async getByGroupId(
		user_id: number,
		group_id: number,
		transaction: EntityManager,
	) {
		return MasterManager.question.getIdsByGroupId(group_id, transaction).then((ids: number[]) => {
			return this.UserAnswerRepository.getByQuestionIds(user_id, ids, null, true, transaction)
		})
	}

	@ManagerModel.bound
	async getLatestByIds(
		userId: number,
		ids: number[],
		date: Date | null,
		transaction: EntityManager,
	) {
		if (date) {
			return this.UserAnswerRepository.getByQuestionIds(userId, ids, date, false, transaction)
		} else {
			return this.UserAnswerRepository.getByQuestionIds(userId, ids, null, false, transaction)
		}
	}

	@ManagerModel.bound
	async getLatestByGroupId(
		userId: number,
		groupId: number,
		date: Date | null,
		transaction: EntityManager,
	) {
		return MasterManager.question.getIdsByGroupId(groupId, transaction).then((ids: number[]) => {
			return this.UserAnswerRepository.getByQuestionIds(userId, ids, date, false, transaction)
		})
	}

	@ManagerModel.bound
	async getAll(
		userId: number,
		date: Date | null,
		transaction: EntityManager,
	) {
		return this.UserAnswerRepository.getAll(userId, date, true, transaction)
	}

	@ManagerModel.bound
	async getAllLatest(
		userId: number,
		date: Date | null,
		transaction: EntityManager,
	) {
		return this.UserAnswerRepository.getAll(userId, date, false, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async save(
		question_id: number,
		user_id: number,
		answer: object,
		transaction: EntityManager,
	) {
		return this.UserAnswerRepository.insert(
			user_id,
			question_id,
			answer,
			transaction,
		).then(() => {
			return this.UserAccountRepository.getById(user_id, true, transaction).then(async user => {
				// Renew updated at
				user.profile.style_profile_updated_at = new Date()

				await this.UserProfileRepository.updateProfile(user.id, {
					style_profile_updated_at: user.profile.style_profile_updated_at,
				}, transaction)

				return user.profile
			})
		})
	}

	@ManagerModel.bound
	async isStyleProfileCompletedShallow(
		user_id: number,
		transaction: EntityManager,
	): Promise<{
		status: boolean,
	} | {
		status: string,
		user: UserInterface & { profile: UserProfileInterface },
	}> {
		return this.UserProfileRepository.getCompleteProfile(user_id, transaction).then(user => {
			if(user.profile.style_profile_updated_at === null && user.profile.style_profile_completion_updated_at === null) {
				// Not yet updated
				return {
					// status: user.profile.is_style_profile_completed ?? false,
					status: 'updated',
					user,
				}
			}

			return +user.profile.style_profile_completion_updated_at === +user.profile.style_profile_updated_at ? {
				status: user.profile.is_style_profile_completed ?? false,
			} : {
				status: 'updated',
				user,
			}
		}).catch(err => {
			this.warn(err)

			return {
				status: false,
			}
		})
	}

	@ManagerModel.bound
	async isStyleProfileCompletedDeep(
		user: UserInterface & { profile: UserProfileInterface },
		transaction: EntityManager,
	) {
		let answers: UserAnswerInterface[]
		let questions: Array<QuestionInterface & { questions: QuestionInterface[] }>
		let flatQuestions: QuestionInterface[]
		let nonDefinitionAndRequiredQuestion: QuestionInterface[]
		let dynamicQuestions: QuestionInterface[]

		const cache = CacheHelper.get('questions')

		if(cache) {
			flatQuestions = cache.flatQuestions
			nonDefinitionAndRequiredQuestion = cache.nonDefinitionAndRequiredQuestion
			dynamicQuestions = cache.dynamicQuestions
		} else {
			questions = await MasterManager.question.getAllStyleProfileQuestions(transaction)
			flatQuestions = questions.map(question => {
				if(question.questions && question.questions.length) {
					return question.questions.concat(question)
				} else {
					return [question]
				}
			}).reduce((sum: QuestionInterface[], q: QuestionInterface[]) => {
				return sum.concat(q)
			}, [])
			// nonDefinitionQuestions =
			nonDefinitionAndRequiredQuestion = flatQuestions.filter(question => {
				return question.type !== QUESTIONS.DEFINITION && question.is_required === true
			})
			dynamicQuestions = nonDefinitionAndRequiredQuestion.filter(question => {
				return !isEqual(question.requirement, {})
			})

			CacheHelper.set('questions', {
				flatQuestions,
				nonDefinitionAndRequiredQuestion,
				dynamicQuestions,
			})
		}

		try {
			answers = await this.UserAnswerRepository.getByQuestionIds(user.id, flatQuestions.map(q => q.id), null, false, transaction)
		} catch(e) {
			answers = []
		}

		const answersAsMap = answers.reduce((sum, answer) => {
			if (answer && answer.question_id) {
				return {
					[answer.question_id]: answer.answer,
					...sum,
				}
			}

			return sum
		}, {})

		if (answers.length + dynamicQuestions.length < nonDefinitionAndRequiredQuestion.length) {
			// assuming that not all question is answered
			await this.renewStyleProfileCompletion(user.user_profile_id, false, user.profile.style_profile_updated_at, transaction)

			return false
		} else {
			// iterate and check wether requiredQuestion answered or not
			let haveInvalid = false
			const invalids = nonDefinitionAndRequiredQuestion.map((question, i) => {
				return answersAsMap[question.id] ? undefined : i
			}).filter(question => {
				return question !== undefined
			})

			if(invalids.length) {
				// check wether it is because custom requirement or not
				haveInvalid = invalids.findIndex(index => {
					const question = nonDefinitionAndRequiredQuestion[index]

					if(question.requirement) {
						if(question.requirement.answers) {
							return question.requirement.answers.reduce((sum, aReq) => {
								if (sum) {
									const answer = answersAsMap[aReq.question_id]

									if(answer) {
										return this.validate(answer.answer || answer, aReq)
									}

									return false
								}

								return sum
							}, true)
						}
					}

					return true
				}) > -1
			}

			await this.renewStyleProfileCompletion(user.user_profile_id, !haveInvalid, user.profile.style_profile_updated_at, transaction)

			return !haveInvalid
		}
	}

	@ManagerModel.bound
	async countAnswerCompletion(
		user_id: number,
		option: {
			count?: boolean,
			per_question_group?: number,
		} = {},
		transaction: EntityManager,
	) {
		let nonDefinitionAndRequiredQuestion: QuestionInterface[]
		const question = await this.manageQuestion(transaction)
		const answers: UserAnswerInterface[] = await this.UserAnswerRepository.getByQuestionIds(user_id, question.flatQuestions.map(q => q.id), null, false, transaction).catch(err => [])

		if(option.per_question_group) {
			nonDefinitionAndRequiredQuestion = question.questions
				.filter(q => q.question_group_id === option.per_question_group)
				.map(q => {
					if(q.questions && q.questions.length) {
						return q.questions.concat(q)
					} else {
						return [q]
					}
				})
				.reduce(CommonHelper.sumArray, [] as any)
				.filter(q => q.type !== QUESTIONS.DEFINITION && q.is_required === true)
		} else {
			nonDefinitionAndRequiredQuestion = question.nonDefinitionAndRequiredQuestion
		}

		question.dynamicQuestions.map(d => {
			answers.map(a => {
				if(d.requirement.answers[0].question_id === a.question_id && this.validate(a.answer, d.requirement.answers[0]) === false) {
					nonDefinitionAndRequiredQuestion = nonDefinitionAndRequiredQuestion.filter(filter => filter.id !== d.id)
				}
			})
		})

		const checkRequiredQuestion = nonDefinitionAndRequiredQuestion.map(q => {
			return answers.map(answer => {
				return q.id === answer.question_id && this.answerValidation(answer.answer)
			}).filter(filter => filter === true)
		}).filter(filter => filter.length > 0)

		const persentageCompletion = parseInt(((checkRequiredQuestion.length / nonDefinitionAndRequiredQuestion.length) * 100).toFixed(2), 10)
		const totalQuestion = nonDefinitionAndRequiredQuestion.length
		const completion = checkRequiredQuestion.length
		const requiredAnsweredLeft = totalQuestion - completion

		if(option.count) {
			return [completion, requiredAnsweredLeft, totalQuestion]
		} else {
			return persentageCompletion
		}
	}

	@ManagerModel.bound
	async renewStyleProfileCompletion(
		user_profile_id: number,
		is_completed: boolean,
		updated_at: Date,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.updateProfileByProfileId(user_profile_id, {
			is_style_profile_completed: is_completed,
			style_profile_completion_updated_at: updated_at,
		}, transaction)
	}

	async dataClient(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.UserAnswerRepository.dataClient(user_id, transaction)
	}
	// ============================ PRIVATES ============================
	private async manageQuestion(transaction: EntityManager) {
		const questions = await MasterManager.question.getAllStyleProfileQuestions(transaction)
		const flatQuestions = questions
			.map(question => {
				if(question.questions && question.questions.length) {
					return question.questions.concat(question)
				} else {
					return [question]
				}
			})
			.reduce((sum: QuestionInterface[], q: QuestionInterface[]) => sum.concat(q), [])
		const nonDefinitionAndRequiredQuestion = flatQuestions.filter(question => question.type !== QUESTIONS.DEFINITION && question.is_required === true)
		const dynamicQuestions = nonDefinitionAndRequiredQuestion.filter(question => !isEqual(question.requirement, {}))

		return {
			questions,
			flatQuestions,
			nonDefinitionAndRequiredQuestion,
			dynamicQuestions,
		}
	}

	private validate(answer: any, answerRequirement: any) {
		switch (answerRequirement.operator) {
		case 'IS':
			return isEqual(answer, answerRequirement.value)
		case 'NOT':
			return !isEqual(answer, answerRequirement.value)
		case 'LESS_THAN':
		case 'GREATER_THAN':
		default:
			// TODO:
			return false
		}
	}

	private answerValidation(answer: any) {
		switch(true) {
			case Array.isArray(answer.CHOICE) && (answer.CHOICE.length === 0 || answer.CHOICE.indexOf(null) > -1):
			case answer.NUMBER === null:
			case answer.SCALE === null:
			case answer.BOOLEAN === null:
			case answer.DATE === null:
				return false
			default:
				return true
		}
	}
}

export default new UserAnswerManager()
