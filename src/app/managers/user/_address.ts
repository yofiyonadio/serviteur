import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	UserProfileRepository,
} from 'app/repositories'

import {
	UserAddressInterface,
	UserInterface,
	UserProfileInterface,
} from 'energie/app/interfaces'
import { AddressMetadataInterface } from 'energie/app/models/interface.address'

import { CommonHelper } from 'utils/helpers'


class UserAddressManager extends ManagerModel {

	static __displayName = 'UserAddressManager'

	protected UserProfileRepository: UserProfileRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			UserProfileRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		user: Partial<UserInterface> & { profile: UserProfileInterface },
		title: string,
		receiver: string,
		phone: string,
		address: string,
		district: string,
		postal: string,
		metadata: AddressMetadataInterface,
		location_id: number,
		setAsDefault: boolean,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.insertAddress(user.id, {
			user_id: user.id,
			title,
			receiver,
			phone,
			address,
			district,
			postal,
			metadata,
			location_id,
		}, transaction).then(async addressRecord => {
			if (setAsDefault) {
				user.profile.address_id = addressRecord.id

				await this.UserProfileRepository.updateProfile(user.id, {
					address_id: addressRecord.id,
				}, transaction)
			}

			return addressRecord
		})
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		user: UserInterface & { profile: UserProfileInterface },
		id: number,
		title: string | undefined,
		receiver: string | undefined,
		phone: string | undefined,
		address: string | undefined,
		district: string | undefined,
		postal: string | undefined,
		metadata: AddressMetadataInterface | undefined,
		location_id: number | undefined,
		setAsDefault: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.UserProfileRepository.updateAddress(id,
			CommonHelper.stripUndefined({
				title,
				receiver,
				phone,
				address,
				district,
				postal,
				metadata,
				location_id,
			}), transaction).then(async isUpdated => {
				if (setAsDefault) {
					user.profile.address_id = id

					await this.UserProfileRepository.updateProfile(user.id, {
						address_id: id,
					}, transaction)
				}

				return isUpdated
			})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(user_address_id: number, transaction: EntityManager): Promise<UserAddressInterface> {
		return this.UserProfileRepository.getAddresses(undefined, [user_address_id], transaction).then(addresses => {
			return addresses[0]
		})
	}

	async getByIds(user_id: number, user_address_ids: number | undefined, transaction: EntityManager): Promise<UserAddressInterface>
	async getByIds(user_id: number, user_address_ids: number[] | undefined, transaction: EntityManager): Promise<UserAddressInterface[]>

	@ManagerModel.bound
	async getByIds(user_id: number, user_address_ids?: number | number[], transaction?: EntityManager): Promise<any> {
		if (Array.isArray(user_address_ids)) {
			return this.UserProfileRepository.getAddresses(user_id, user_address_ids, transaction)
				.then(userAddresses => {
					return user_address_ids.map(id => {
						return userAddresses.find(userAddress => {
							return userAddress.id === id
						}) || null
					})
				})
		} else if (user_address_ids !== undefined) {
			return this.UserProfileRepository.getAddresses(user_id, [user_address_ids], transaction).then(addresses => {
				return addresses[0]
			})
		} else {
			return this.UserProfileRepository.getAddresses(user_id, user_address_ids as undefined, transaction)
		}
	}

	@ManagerModel.bound
	async getDeep(address_id: number, transaction: EntityManager) {
		return this.UserProfileRepository.getAddressDeep(address_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new UserAddressManager()
