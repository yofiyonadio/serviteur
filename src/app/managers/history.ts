import { ManagerModel } from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	HistoryRepository,
} from 'app/repositories'


class HistoryManager extends ManagerModel {

	static __displayName = 'HistoryManager'

	protected HistoryRepository: HistoryRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			HistoryRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		type: 'CLAIM' | 'INVENTORY' | 'ORDER_DETAIL' | 'ORDER' | 'PURCHASE_ORDER' | 'PURCHASE_REQUEST' | 'SHIPMENT' | 'STYLESHEET' | 'VOUCHER',
		id_or_ids: number | number[],
		last_only: boolean = true,
		transaction: EntityManager,
	) {
		const last = last_only ? true : false
		if(Array.isArray(id_or_ids)) {
			if(last) {
				switch (type) {
				case 'CLAIM':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'INVENTORY':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'ORDER_DETAIL':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'ORDER':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'PURCHASE_ORDER':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'PURCHASE_REQUEST':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'SHIPMENT':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'STYLESHEET':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				case 'VOUCHER':
					return this.HistoryRepository.get(type, id_or_ids, true, transaction)
				}
			} else {
				switch (type) {
				case 'CLAIM':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'INVENTORY':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'ORDER_DETAIL':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'ORDER':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'PURCHASE_ORDER':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'PURCHASE_REQUEST':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'SHIPMENT':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'STYLESHEET':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				case 'VOUCHER':
					return this.HistoryRepository.get(type, id_or_ids, false, transaction)
				}
			}
		} else {
			if (last) {
				if (last) {
					switch (type) {
					case 'CLAIM':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'INVENTORY':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'ORDER_DETAIL':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'ORDER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'PURCHASE_ORDER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'PURCHASE_REQUEST':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'SHIPMENT':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'STYLESHEET':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'VOUCHER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					}
				} else {
					switch (type) {
					case 'CLAIM':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'INVENTORY':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'ORDER_DETAIL':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'ORDER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'PURCHASE_ORDER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'PURCHASE_REQUEST':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'SHIPMENT':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'STYLESHEET':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'VOUCHER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					}
				}
			} else {
				if (last) {
					switch (type) {
					case 'CLAIM':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'INVENTORY':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'ORDER_DETAIL':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'ORDER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'PURCHASE_ORDER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'PURCHASE_REQUEST':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'SHIPMENT':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'STYLESHEET':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					case 'VOUCHER':
						return this.HistoryRepository.get(type, id_or_ids, true, transaction)
					}
				} else {
					switch (type) {
					case 'CLAIM':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'INVENTORY':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'ORDER_DETAIL':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'ORDER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'PURCHASE_ORDER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'PURCHASE_REQUEST':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'SHIPMENT':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'STYLESHEET':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					case 'VOUCHER':
						return this.HistoryRepository.get(type, id_or_ids, false, transaction)
					}
				}
			}
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new HistoryManager()

