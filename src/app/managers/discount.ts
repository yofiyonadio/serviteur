import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	ProductRepository,
	VariantRepository,
} from 'app/repositories'
import { ERRORS, ERROR_CODES } from 'app/models/error'

class DiscountManager extends ManagerModel {

	static __displayName = 'DiscountManager'

	protected ProductRepository: ProductRepository
	protected VariantRepository: VariantRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ProductRepository,
			VariantRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================
	async alterVariantDiscount(
		changer_user_id: number,
		variant_id: number,
		discount: {
			amount?: number,
			percentage?: number,
		} = {},
		transaction: EntityManager,
	) {
		const variant = await this.VariantRepository.get(variant_id, {}, transaction)

		if (!!discount.amount) {
			return this.VariantRepository.update(changer_user_id, variant_id, {
				price: discount.amount,
			}, undefined, undefined, undefined, transaction)
		} else if (!!discount.percentage) {
			if (discount.percentage > 100) {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Percentage Discount More Than 100!')
			}

			const amount = Math.floor(variant.retail_price * (discount.percentage / 100))

			return this.VariantRepository.update(changer_user_id, variant_id, {
				price: variant.retail_price - amount,
			}, undefined, undefined, undefined, transaction)
		} else {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Please Provide Discount Amount or Discount Percentage')
		}
	}

	// ============================= GETTER =============================
	get getDiscount() {
		return this.ProductRepository.filter
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new DiscountManager()
