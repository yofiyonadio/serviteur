import { ErrorModel, ManagerModel } from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import OrderManager from '../order'
import ProductManager from '../product'
import ServiceManager from '../service'
import ClusterManager from '../cluster'
import NotificationManager from '../notification'

import StylecardRepository from 'app/repositories/stylecard'

import { StylecardVariantInterface, StylecardInterface } from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import { ERROR_CODES, ERRORS } from 'app/models/error'
import { ORDER_DETAIL_STATUSES, ORDERS, STYLEBOARD_STATUSES, STYLECARD_STATUSES } from 'energie/utils/constants/enum'
import { isEmpty } from 'lodash'

class ServiceStylecardManager extends ManagerModel {

	static __displayName = 'ServiceStylecardManager'

	protected StylecardRepository: StylecardRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			StylecardRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		data: Parameter<StylecardInterface>,
		user_asset_ids: number[] | undefined,
		collection_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.insert(data, user_asset_ids, transaction).then(async res => {
			if (collection_ids) {
				await this.setCollection(res.id, collection_ids, transaction)
			}

			return {
				...res,
				collection_ids,
			}
		})
	}

	@ManagerModel.bound
	async createVariant(
		stylecard_id: number,
		variant_id: number,
		bundle_discount: number | undefined,
		transaction: EntityManager,
		item_card?: string,
	): Promise<StylecardVariantInterface> {
		const variant = await ProductManager.variant.get(variant_id, {}, transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				err.detail = 'Variant not found!'
			}

			throw err
		})

		return this.StylecardRepository.setVariant({
			stylecard_id,
			variant_id,
			price: variant.price,
			retail_price: variant.retail_price,
			bundle_discount,
			item_card,
		}, transaction)
	}

	@ManagerModel.bound
	async setCollection(
		stylecard_id: number,
		collection_ids: number[],
		transaction: EntityManager,
	) {
		// reset collection
		await this.StylecardRepository.detachCollection(stylecard_id, transaction)

		await this.StylecardRepository.setCollection(stylecard_id, collection_ids, transaction)

		return true
	}

	get addAnswer() {
		return this.StylecardRepository.addAnswer
	}

	get addUser() {
		return this.StylecardRepository.addUser
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async publishStylecard(
		changer_user_id: number,
		stylecard_id: number,
		stylist_note: string | undefined,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const stylecard = await this.get({stylecard_id_or_ids: stylecard_id, many: false}, {with_variants: true}, transaction)
		
		// UNUSED BECAUSE NEW STYLECARD UI DOESNT REQUIRE EXACT QTY
		// if (stylecard.count === stylecard.stylecardVariants.length) {
		// } else {
		// 	throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard item count mismatch.')
		// }

		return this.StylecardRepository.update(
			changer_user_id,
			stylecard_id,
			{
				status: STYLECARD_STATUSES.PUBLISHED,
				stylist_note,
			},
			note,
			transaction,
		).then(async isUpdated => {
			if(!!stylecard.cluster_id) {
				const usersInCluster = await ClusterManager.getUsers(stylecard.cluster_id, transaction)

				if (!isEmpty(usersInCluster)) {
					// add stylecard to all user in cluster
					await this.StylecardRepository.addUser(stylecard.id, usersInCluster.map(user => user.id), transaction)
						.catch(error => {
							if (error.code === '23505') {
								throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Users already have this stylecard, please remove them before update')
							}

							throw error
						})

					// send notif
					usersInCluster.map(user => {
						NotificationManager.sendWeeklyRecomendationNotice(
							user.id,
							stylecard.cluster_id,
							user.email,
							user.profile.first_name,
							user.stylist?.asset?.url,
							user.stylist?.profile.stage_name,
							`${process.env.MAIN_WEB_URL}/boards`,
						)
					})
				}
			}

			return isUpdated
		})
		
	}

	@ManagerModel.bound
	async update(
		changer_user_id: number,
		stylecard_id: number,
		update: Partial<Parameter<StylecardInterface>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.update(changer_user_id, stylecard_id, update, note, transaction).then(async isUpdated => {
			if(isUpdated && update.status === STYLECARD_STATUSES.STYLING) {
				const styleboard = await ServiceManager.styleboard.getStyleboardOfStylecard(stylecard_id, { with_order_detail: true }, transaction).catch(err => null)

				if (styleboard) {
					if (styleboard.status === STYLEBOARD_STATUSES.PENDING) {
						await ServiceManager.styleboard.update(changer_user_id, styleboard.id, {
							status: STYLEBOARD_STATUSES.STYLING,
						}, 'Auto update because stylecard is updated into styling.', transaction)
					}

					if (styleboard.orderDetail && (styleboard.orderDetail.status === ORDER_DETAIL_STATUSES.PENDING || styleboard.orderDetail.status === ORDER_DETAIL_STATUSES.EXCEPTION)) {
						await OrderManager.detail.update(changer_user_id, styleboard.orderDetail.id, {
							status: ORDER_DETAIL_STATUSES.PROCESSING,
						}, 'Auto update because stylesheet is updated into styling.', transaction)

						const order = await OrderManager.getShallowWithDetails(styleboard.orderDetail.order_id, transaction)

						if (order.status === ORDERS.PAID && order.orderDetails.findIndex(detail => detail.status === ORDER_DETAIL_STATUSES.PENDING) === -1) {
							await OrderManager.update(changer_user_id, order.id, {
								status: ORDERS.PROCESSING,
							}, 'Auto update because stylecard is updated into styling and all details already processing.', transaction)
						}

						return isUpdated
					}
				}
			}

			return isUpdated
		})
	}

	get updateVariant() {
		return this.StylecardRepository.updateVariant
	}

	@ManagerModel.bound
	async updateStylist(
		changer_user_id: number,
		stylecard_id: number,
		stylist_id: number,
		note: string,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.update(changer_user_id, stylecard_id, {
			stylist_id,
		}, note, transaction)
	}

	get publishBatch() {
		return this.StylecardRepository.publishBatch
	}

	// ============================= GETTER =============================
	get filter() {
		return this.StylecardRepository.filter
	}

	get filterPrestyled() {
		return this.StylecardRepository.filterPrestyled
	}

	get filterByVariantId() {
		return this.StylecardRepository.filterByVariantId
	}

	get variants() {
		return this.StylecardRepository.getVariants
	}

	get getNotes() {
		return this.StylecardRepository.getNotes
	}

	get get() {
		return this.StylecardRepository.get
	}

	get getStylecard() {
		return this.StylecardRepository.getStylecard
	}

	get getPrestyled() {
		return this.StylecardRepository.getPrestyled
	}

	get getStylecardVariants() {
		return this.StylecardRepository.getStylecardVariants
	}

	get getCollections() {
		return this.StylecardRepository.getCollection
	}

	get getLatestAnswer() {
		return this.StylecardRepository.getLatestAnswer
	}

	get getStylecardRecomendationAnswer() {
		return this.StylecardRepository.getStylecardRecomendationAnswer
	}

	get ggetCountViewStylecard() {
		return this.StylecardRepository.getCountViewStylecard
	}

	@ManagerModel.bound
	async getStylistActiveStylecard(
		stylist_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.get({
			stylist_id_or_ids: stylist_id,
			many: true,
		}, {
			with_variants: true,
			with_styleboard: true,
			deep_variants: true,
			status: STYLECARD_STATUSES.STYLING,
		}, transaction)
	}

	@ManagerModel.bound
	async getBundleDiscount(
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.getBundleDiscount(stylecard_id, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async removeVariant(
		stylecard_variant_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository
			.getStylecardVariants([stylecard_variant_id], { with_stylecard: true }, transaction)
			.then(svs => svs.pop())
			.then(async stylecardVariant => {
				if (stylecardVariant.stylecard.status !== STYLECARD_STATUSES.STYLING) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_101, 'Stylecard is not currently in styling')
				}

				return this.StylecardRepository.removeVariant(stylecard_variant_id, transaction)
			})
	}

	@ManagerModel.bound
	async removeAllCollection(
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.detachCollection(stylecard_id, transaction)
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async setStylist(
		changer_user_id: number,
		user_id: number,
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.update(changer_user_id, stylecard_id, {
			stylist_id: user_id,
		}, '', transaction)
	}

	@ManagerModel.bound
	async duplicateStylecard(
		changer_user_id: number,
		stylecard_id: number,
		source_stylecard_id: number,
		consent: boolean,
		transaction: EntityManager,
	) {
		const stylecard = await this.StylecardRepository.get({
			stylecard_id_or_ids: stylecard_id,
			many: false,
		}, {
			with_variants: true,
		}, transaction)

		if (stylecard.stylecardVariants.length && !consent) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_109, 'Stylecard is not empty, are you sure want to replace it\'s content with a new one?')
		} else if (stylecard.status !== STYLECARD_STATUSES.PENDING && stylecard.status !== STYLECARD_STATUSES.STYLING) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Stylecard must be in STYLING or PENDING status.')
		}

		if (stylecard.stylecardVariants.length) {
			await Promise.all(stylecard.stylecardVariants.map(async sV => {
				return this.StylecardRepository.removeVariant(sV.id, transaction)
			}))
		}

		const source = await this.StylecardRepository.get({
			stylecard_id_or_ids: source_stylecard_id,
			many: false,
		}, {
			with_variants: true,
			deep_variants: true,
		}, transaction)

		return Promise.all(source.stylecardVariants.map(async sV => {
			return this.StylecardRepository.setVariant({
				stylecard_id,
				variant_id: sV.variant_id,
				price: sV.variant.price,
				retail_price: sV.variant.retail_price,
			}, transaction)
		})).then(async duplicateds => {
			if (duplicateds.length === source.stylecardVariants.length) {
				await this.StylecardRepository.update(changer_user_id, stylecard_id, {
					title: source.title,
					stylist_note: source.stylist_note,
				}, 'Update because of success duplication.', transaction)

				return duplicateds.map(() => true)
			} else if (duplicateds.length > 0) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Partial duplication success.')
			} else {
				return duplicateds.map(() => true)
			}
		})
	}

	@ManagerModel.bound
	async isVariantInStylecard(
		variant_id: number,
		stylecard_id: number,
		transaction: EntityManager,
	) {
		return this.StylecardRepository.getStylecardsFromVariant(variant_id, transaction).then(stylecards => {
			return stylecards.findIndex(stylecard => {
				return stylecard.id === stylecard_id
			}) > -1
		})
	}

	// ============================ PRIVATES ============================

}

export default new ServiceStylecardManager()
