import {
	ErrorModel,
	ManagerModel,
} from 'app/models'

import {
	Connection,
	EntityManager,
} from 'typeorm'

import StyleboardRepository from 'app/repositories/styleboard'
import { StylesheetInventoryInterface } from 'energie/app/interfaces'

import { CommonHelper, TimeHelper } from 'utils/helpers'
import NotificationManager from '../notification'
import UserManager from '../user'

import {
	STYLEBOARD_STATUSES,
	STYLECARD_STATUSES,
} from 'energie/utils/constants/enum'

import { ERRORS, ERROR_CODES } from 'app/models/error'
import { ServiceManager } from '..'

class ServiceStyleboardManager extends ManagerModel {

	static __displayName = 'ServiceStyleboardManager'

	protected StyleboardRepository: StyleboardRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			StyleboardRepository,
		])
	}

	// ============================= INSERT =============================
	get create() {
		return this.StyleboardRepository.insert
	}

	// ============================= UPDATE =============================
	get update() {
		return this.StyleboardRepository.update
	}

	get updateStylboardUserAsset() {
		return this.StyleboardRepository.updateStylboardUserAsset
	}

	@ManagerModel.bound
	async updateStylist(
		changer_user_id: number,
		styleboard_id: number,
		stylist_id: number,
		note: string,
		transaction: EntityManager,
	) {
		const stylecard_ids = await this.StyleboardRepository.get(styleboard_id, {
			with_stylecard: true,
		}, transaction).then(styleboard => styleboard.stylecards.map(stylecard => stylecard.id))

		return this.update(changer_user_id, styleboard_id, {
			stylist_id,
		}, note, transaction)
			.then(async isUpdated => {
				await Promise.all(stylecard_ids.map(async st_id => {
					return ServiceManager.stylecard.updateStylist(changer_user_id, st_id, stylist_id, note, transaction)
				}))

				return isUpdated
			})
	}

	@ManagerModel.bound
	async startStyling(
		changer_user_id: number,
		styleboard_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.update(changer_user_id, styleboard_id, {
			status: STYLEBOARD_STATUSES.STYLING,
		}, note, transaction).then(async isUpdated => {
			if (isUpdated) {
				const styleboard = await this.StyleboardRepository.get(styleboard_id, { with_request: true }, transaction)
				const stylist = await UserManager.getStylist(styleboard.user_id, transaction)

				// Send notice
				NotificationManager.sendStartStylingNotice(
					styleboard.user_id,
					'STYLEBOARD',
					stylist.profile.stage_name,
				)
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async publishStyleboard(
		changer_user_id: number,
		styleboard_id: number,
		transaction: EntityManager,
	) {
		const styleboard = await this.StyleboardRepository.get(styleboard_id, {
			deep: true,
			with_service: true,
			with_request: true,
			with_user: true,
			with_stylecard: true,
			with_stylecard_variant: true,
		}, transaction)

		// styleboard.title === null
		if (!styleboard.title) {
			// check styleboard title
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Styleboard has no title.')
		} else if (!styleboard.stylist_note) {
			// check styleboard stylist note
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Styleboard stylist note is empty.')
		} else if (styleboard.stylecards.map(st => !st.title).indexOf(true) > -1) {
			// check all stylesheet title
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'One or more of your stylecard has no title.')
		} else if (styleboard.stylecards.map(st => !st.stylist_note).indexOf(true) > -1) {
			// check all stylesheet stylist note
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'One or more of your stylecard stylist note is empty.')
		} else if (styleboard.stylecards.map(st => st.status === STYLECARD_STATUSES.PUBLISHED).indexOf(false) > -1) {
			// check all stylesheet status is publish
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'One or more of your stylecard is not published yet.')
		} else if (styleboard.stylecards.length !== styleboard.service.metadata.stylecard_count) {
			// check all stylesheet count to match service metadata
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Stylecard count mismatch.')
		} else if (styleboard.stylecards.map(st => st.stylecardVariants.length !== styleboard.service.metadata.stylecard_item_count).indexOf(true) > -1) {
			// check all stylesheet item count to match service metadata
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Stylecard item count mismatch.')
		}

		return await this.update(changer_user_id, styleboard_id, {
			status: STYLEBOARD_STATUSES.PUBLISHED,
			expired_at: TimeHelper.moment().add(2, 'day').toDate(),
		}, '', transaction).then(async isUpdated => {
			if (isUpdated) {
				// Final variant price stamping
				await Promise.all(styleboard.stylecards.map(s => s.stylecardVariants).reduce(CommonHelper.flatten, []).map(async sV => {
					await ServiceManager.stylecard.updateVariant(sV.id, {
						price: sV.variant.price,
						retail_price: sV.variant.retail_price,
					}, transaction)
				}))

				const stylist = await UserManager.getStylist(styleboard.user_id, transaction)

				NotificationManager.sendStylingRequestNotice(
					styleboard.user.id,
					styleboard.request.id,
					styleboard.user.email,
					styleboard.user.profile.first_name,
					stylist.profile.asset?.url,
					stylist.profile.stage_name,
				)
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async resolveStyleboard(
		changer_user_id: number,
		styleboard_id: number,
		status: STYLEBOARD_STATUSES.REDEEMED | STYLEBOARD_STATUSES.RESOLVED,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const styleboard = await this.StyleboardRepository.get(styleboard_id, {
			deep: true,
			with_request: true,
		}, transaction)

		return this.update(changer_user_id, styleboard.id, { status }, note, transaction)
	}

	@ManagerModel.bound
	async unresolveStyleboard(
		changer_user_id: number,
		styleboard_id: number,
		transaction: EntityManager,
	) {
		const styleboard = await this.StyleboardRepository.get(styleboard_id, {
			deep: true,
			with_request: true,
		}, transaction)

		return this.update(changer_user_id, styleboard.id, {
			status: STYLEBOARD_STATUSES.PUBLISHED,
		}, 'Reopen styleboard', transaction)

	}

	// ============================= GETTER =============================
	get filter() {
		return this.StyleboardRepository.filter
	}

	get get() {
		return this.StyleboardRepository.get
	}

	get getWithResponse() {
		return this.StyleboardRepository.getWithResponse
	}

	get getFromOrderDetail() {
		return this.StyleboardRepository.getFromOrderDetail
	}

	get getExpired() {
		return this.StyleboardRepository.getExpired
	}

	get getExpiring() {
		return this.StyleboardRepository.getExpiring
	}

	@ManagerModel.bound
	async emailToPublished(styleboard_id: number, transaction: EntityManager) {
		const styleboard = await this.StyleboardRepository.get(styleboard_id, {
			with_user: true,
			with_stylist: true,
			with_request: true,
		}, transaction)

		if(styleboard.status === STYLEBOARD_STATUSES.PUBLISHED) {
			return await NotificationManager.sendStylingRequestNotice(
				styleboard.user.id,
				styleboard.request.id,
				styleboard.user.email,
				styleboard.user.profile.first_name,
				styleboard.stylist.profile.asset?.url,
				styleboard.stylist.profile.stage_name,
			)
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_010, 'Styleboard is not published!')
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	get getStyleboardOfStylecard() {
		return this.StyleboardRepository.getFromStylecard
	}

	@ManagerModel.bound
	async isVariantInStyleboard(
		variant_id: number,
		styleboard_id: number,
		user_id: number | undefined,
		transaction: EntityManager,
	) {
		return this.StyleboardRepository.getStyleboardsFromVariant(variant_id, transaction).then(styleboards => {
			return styleboards.findIndex(styleboard => {
				return styleboard.id === styleboard_id && (user_id ? user_id === styleboard.user_id : true)
			}) > -1
		})
	}

	@ManagerModel.bound
	calculateDiscountedPrice(
		stylesheetInventories: StylesheetInventoryInterface[],
	) {
		const totalPrice = stylesheetInventories.map(sI => sI.price).reduce(CommonHelper.sum, 0)

		let discountedPrice = totalPrice

		if (stylesheetInventories.length > 3) {
			discountedPrice = Math.round(totalPrice * .8 / 100) * 100
		} else if (stylesheetInventories.length > 2) {
			discountedPrice = Math.round(totalPrice * .85 / 100) * 100
		} else if (stylesheetInventories.length > 1) {
			discountedPrice = Math.round(totalPrice * .9 / 100) * 100
		}

		return discountedPrice
	}
	// ============================ PRIVATES ============================

}

export default new ServiceStyleboardManager()
