import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

// import AssetManager from '../asset'
import BrandManager from '../brand'
import PacketManager from '../packet'

import ServiceStyleboardManager from './_styleboard'
import ServiceStylecardManager from './_stylecard'

import ServiceRepository from 'app/repositories/service'

import { ServiceMetadataInterface } from 'energie/app/interfaces/service'

import TimeHelper, { Moment } from 'utils/helpers/time'

import DEFAULTS from 'utils/constants/default'
import { ASSETS } from 'energie/utils/constants/enum'


const RANGE = 0

class ServiceManager extends ManagerModel {

	static __displayName = 'ServiceManager'

	protected ServiceRepository: ServiceRepository

	private LASTORDERDATE: Moment
	private SHIPMENTDATE: Moment

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ServiceRepository,
		], async () => {
			// Initialization function goes here

			await BrandManager.initialize(connection)
			await PacketManager.initialize(connection)
			await ServiceStyleboardManager.initialize(connection)
			await ServiceStylecardManager.initialize(connection)

			await this.sync()
			this.resyncDate()
		})
	}

	get styleboard(): typeof ServiceStyleboardManager {
		return ServiceStyleboardManager
	}

	get stylecard(): typeof ServiceStylecardManager {
		return ServiceStylecardManager
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		id: number,
		with_brand: boolean,
		transaction: EntityManager,
	) {
		return this.ServiceRepository.getBy('id', id, with_brand, transaction)
	}

	@ManagerModel.bound
	async getBrand(
		service_id: number,
		transaction: EntityManager,
	) {
		return this.ServiceRepository.getBrand(service_id, transaction)
	}

	@ManagerModel.bound
	async getBySlug(
		service_slug: string,
		with_brand: boolean,
		transaction: EntityManager,
	) {
		return this.ServiceRepository.getBy('slug', service_slug, with_brand, transaction)
	}

	@ManagerModel.bound
	async getByTitle(
		title: string,
		transaction: EntityManager,
	) {
		return this.ServiceRepository.getByTitle(title, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	get lastOrderDate() {
		this.resyncIfPassed()
		return this.LASTORDERDATE
	}

	get shipmentDate() {
		this.resyncIfPassed()
		return this.SHIPMENTDATE
		// return TimeHelper.moment().add(1, 'd')
	}

	get readableShipmentDate() {
		this.resyncIfPassed()

		if (RANGE > 0) {
			const ENDDATE = TimeHelper.moment(this.SHIPMENTDATE).add(RANGE, 'd')

			return TimeHelper.getRange([
				this.SHIPMENTDATE.toDate(),
				ENDDATE.toDate(),
			])
		} else {
			return this.SHIPMENTDATE.format('DD MMM YYYY')
		}
	}

	// ============================ PRIVATES ============================
	private resyncDate() {
		this.LASTORDERDATE = TimeHelper.moment().endOf('d')
		this.SHIPMENTDATE = TimeHelper.moment().endOf('d').add(3, 'd')
	}

	private resyncIfPassed() {
		if (+this.LASTORDERDATE < Date.now()) {
			this.resyncDate()
		}
	}

	private async sync() {
		if (DEFAULTS.SYNC) {
			await this.transaction(async transaction => {
				await this.ServiceRepository.saveOrUpdate('ServiceRecord', [{
					id: 1,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DIGITAL_PACKET_ID,
					title: 'Custom',
					slug: 'white-glove',
					description: 'Styling service by Yuna & Co.',
					price: 0,
					retail_price: 60000,
					// start_time: TimeHelper.moment(new Date(2015, 7, 8, 9, 0, 0, 0)).format() as any,
					// close_time: TimeHelper.moment(new Date(2015, 7, 8, 18, 0, 0, 0)).format() as any,
					// days: [DAYS.MONDAY, DAYS.TUESDAY, DAYS.WEDNESDAY, DAYS.THURSDAY, DAYS.FRIDAY].join(',') as any,
					metadata: JSON.stringify({
						stylecard_count: 3,
						stylecard_item_count: 5,
					} as ServiceMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
				}], transaction)

				await this.ServiceRepository.saveOrUpdate('ServiceAssetRecord', [{
					id: 1,
					service_id: 1,
					url: 'services/white-glove.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 1,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}], transaction)
			})
		}

		DEFAULTS.setCustomStylingServiceId(1)
	}
}

export default new ServiceManager()
