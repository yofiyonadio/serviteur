import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import BrandManager from '../brand'
import InventoryManager from '../inventory'
import MasterManager from '../master'
import MatchBoxManager from '../matchbox'
import NotificationManager from '../notification'
import OrderManager from '../order'
import PacketManager from '../packet'
import VoucherManager from '../voucher'

import ShipmentTikiManager from './_tiki'
import ShipmentSicepatManager from './_sicepat'
// import ShipmentNinjaManager from './_ninja'

import ShipmentRepository from 'app/repositories/shipment'

import {
	ShipmentAddressInterface,
	ShipmentInterface,
	PacketInterface,
	OrderDetailInterface,
	OrderAddressInterface,
	BrandAddressInterface,
	OrderRequestInterface,
	// OrderRequestInterface,
} from 'energie/app/interfaces'

import InterfaceAddressModel from 'energie/app/models/interface.address'

import CommonHelper from 'utils/helpers/common'
import TimeHelper from 'utils/helpers/time'

import { Parameter } from 'types/common'

import { check } from 'app/interfaces/guard'

import DEFAULTS from 'utils/constants/default'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { SHIPMENTS, SHIPMENT_STATUSES, ORDER_DETAILS, ORDER_REQUESTS, VOUCHERS, ORDERS, STATEMENT_SOURCES } from 'energie/utils/constants/enum'
import { CONNOTE_STATUS, CONNOTE_STATUS_DESCRIPTION } from 'app/interfaces/tiki'

import csvjson from 'csvjson'
import { groupBy, keyBy, padStart, uniqBy, uniq } from 'lodash'
import { UserManager } from '..'

const AVAILABLE_SHIPMENTS: SHIPMENTS[] = Object.values(SHIPMENTS)

interface RateInterface {
	type: SHIPMENTS,
	courier: string,
	service: string,
	rate: number,
	description: string,
	min_day: number,
	max_day: number,
	prices: object,
}

class ShipmentManager extends ManagerModel {

	static __displayName = 'ShipmentManager'

	ShipmentRepository: ShipmentRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ShipmentRepository,
		], async () => {
			await OrderManager.initialize(connection)
			await ShipmentTikiManager.initialize(connection)
			await ShipmentSicepatManager.initialize(connection)
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		method: SHIPMENTS,
		packet: Parameter<PacketInterface>,
		order_details_or_ids: OrderDetailInterface[] | number[] | undefined,
		addresses_or_address_ids: [Parameter<ShipmentAddressInterface>, Parameter<ShipmentAddressInterface>?] | [number, number?],
		is_facade: boolean,
		is_return: boolean = false,
		data: Pick<ShipmentInterface, 'amount' | 'prices' | 'awb' | 'url' | 'note' | 'metadata' | 'created_at'> & {
			courier?: string,
			service?: string,
			status?: SHIPMENT_STATUSES,
		},
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		return Promise.resolve().then(async () => {
			let orderDetails: OrderDetailInterface[]

			if (!!order_details_or_ids) {
				if(typeof order_details_or_ids[0] === 'number') {
					// get the orderdetail first
					orderDetails = await OrderManager.detail.getByIds(order_details_or_ids as number[], false, transaction) as any
				} else {
					orderDetails = order_details_or_ids as OrderDetailInterface[]
				}
			}

			// we do not need to make sure that the order details being sent
			// is having the same pickup point from its reference
			// it might have changed and being overriden

			let origin: Parameter<ShipmentAddressInterface> = null
			let destination: Parameter<ShipmentAddressInterface> = null

			if(typeof addresses_or_address_ids[0] === 'number') {
				// its an id
				origin = await this.ShipmentRepository.getAddressDeep(addresses_or_address_ids[0], transaction)
			} else {
				origin = addresses_or_address_ids[0]
			}

			if(addresses_or_address_ids.length === 1 || addresses_or_address_ids[1] === undefined || addresses_or_address_ids[1] === null) {
				destination = origin
				origin = await BrandManager.getAddresses(DEFAULTS.YUNA_BRAND_ADDRESS_ID, transaction).then(addresses => addresses[0])
			} else if(typeof addresses_or_address_ids[1] === 'number') {
				destination = await this.ShipmentRepository.getAddressDeep(addresses_or_address_ids[1], transaction)
			} else {
				destination = addresses_or_address_ids[1]
			}
			const shipmentAddresses: [Parameter<ShipmentAddressInterface>, Parameter<ShipmentAddressInterface>] = [origin, destination]
			const shipmentPacket = await PacketManager.create(packet, transaction)

			let service: string
			let packetPrice: number
			let rate: RateInterface

			switch(method) {
			case SHIPMENTS.MANUAL:
			default:
				return this.ShipmentRepository.insert(
					SHIPMENTS.MANUAL,
					data.courier,
					data.service,
					data.amount,
					shipmentPacket.id,
					shipmentAddresses,
					orderDetails,
					data.status,
					data.awb,
					data.url,
					data.note,
					data.prices,
					is_facade,
					is_return,
					data.created_at,
					data.metadata,
					transaction,
				)
			case SHIPMENTS.TIKI:
				service = CommonHelper.default(data.service, ShipmentTikiManager.getDefaultService())
				packetPrice = orderDetails.map(oD => oD.retail_price).reduce(CommonHelper.sum, 0)

				rate = (await this.getRates(shipmentAddresses[0], shipmentAddresses[1], shipmentPacket, packetPrice, this.getFacadeShipmentTypeForRate(), undefined, service, transaction))[0]

				if(is_facade) {
					return this.ShipmentRepository.insert(
						SHIPMENTS.TIKI,
						data.courier || 'Tiki',
						service,
						rate.rate,
						shipmentPacket.id,
						shipmentAddresses,
						orderDetails,
						is_facade ? SHIPMENT_STATUSES.DELIVERED : SHIPMENT_STATUSES.PENDING,
						data.awb,
						data.url || 'https://tiki.id/id/tracking',
						data.note,
						data.prices,
						is_facade,
						is_return,
						data.created_at,
						data.metadata,
						transaction,
					)
				} else {
					return ShipmentTikiManager.createShipmentOrder(
						await this.getShipmentNumber(method, transaction),
						uniq(orderDetails.map(oD => oD.title)).join(','),
						packetPrice,
						service,
						shipmentAddresses[0],
						shipmentAddresses[1],
						0,
					).then(res => {
						return this.ShipmentRepository.insert(
							SHIPMENTS.TIKI,
							data.courier || 'Tiki',
							service,
							rate.rate,
							shipmentPacket.id,
							shipmentAddresses,
							orderDetails,
							SHIPMENT_STATUSES.PENDING,
							res.response.paket_id,
							data.url || 'https://tiki.id/id/tracking',
							data.note,
							data.prices,
							is_facade,
							is_return,
							data.created_at,
							data.metadata,
							transaction,
						)
					})
				}
			case SHIPMENTS.SICEPAT:
				service = CommonHelper.default(data.service, ShipmentSicepatManager.getDefaultService())
				packetPrice = orderDetails.map(oD => oD.retail_price).reduce(CommonHelper.sum, 0)

				rate = (await this.getRates(shipmentAddresses[0], shipmentAddresses[1], shipmentPacket, packetPrice, [SHIPMENTS.SICEPAT], undefined, service, transaction))[0]

				if(is_facade) {
					return this.ShipmentRepository.insert(
						SHIPMENTS.SICEPAT,
						rate.courier,
						service,
						rate.rate,
						shipmentPacket.id,
						shipmentAddresses,
						orderDetails,
						is_facade ? SHIPMENT_STATUSES.DELIVERED : SHIPMENT_STATUSES.PENDING,
						data.awb,
						data.url,
						data.note,
						data.prices,
						is_facade,
						is_return,
						data.created_at,
						data.metadata,
						transaction,
					)
				} else {
					return await ShipmentSicepatManager.createShipment(
						shipmentAddresses[0],
						shipmentAddresses[1],
						await this.getShipmentNumber(method, transaction),
						shipmentPacket,
						packetPrice,
						transaction,
					).then(awb => {
						return this.ShipmentRepository.insert(
							SHIPMENTS.SICEPAT,
							rate.courier,
							service,
							rate.rate,
							shipmentPacket.id,
							shipmentAddresses,
							orderDetails,
							SHIPMENT_STATUSES.PENDING,
							awb,
							data.url,
							data.note,
							data.prices,
							is_facade,
							is_return,
							data.created_at,
							data.metadata,
							transaction,
						)
					})
				}
			// case SHIPMENTS.NINJA:
			// 	const price = orderDetails.map(oD => oD.price).reduce(CommonHelper.sum, 0)

			// 	return ShipmentNinjaManager.createShipmentOrder(
			// 		await this.getShipmentNumber(method, transaction),
			// 		SERVICE_TYPE.PARCEL,
			// 		SERVICE_LEVEL.SAMEDAY,
			// 		shipmentAddresses[0],
			// 		shipmentAddresses[1],
			// 		{
			// 			dimensions: shipmentPacket,
			// 		},
			// 	).then(res => {
			// 		return this.ShipmentRepository.insert(
			// 			SHIPMENTS.NINJA,
			// 			'Ninja',
			// 			SERVICE_LEVEL.STANDARD,
			// 			price,
			// 			shipmentPacket.id,
			// 			shipmentAddresses,
			// 			orderDetails,
			// 			SHIPMENT_STATUSES.PENDING,
			// 			res.tracking_number,
			// 			data.url,
			// 			data.note,
			// 			data.prices,
			// 			is_facade,
			// 			is_return,
			// 			data.created_at,
			// 			data.metadata,
			// 			transaction,
			// 		)
			// 	})
			}
		}).then(async shipment => {
			if(shouldSendEmail) {
				if (
					data.status === SHIPMENT_STATUSES.PROCESSING
					|| data.status === SHIPMENT_STATUSES.DELIVERED
				) {
					const orders = await OrderManager.getOrderNumbersAndUserFromOrderDetails(shipment.orderDetails.map(oD => oD.id), transaction)

					orders.forEach(order => {
						if (data.status === SHIPMENT_STATUSES.PROCESSING) {
							NotificationManager.sendShippingNotice(
								shipment.id,
								order.user.id,
								order.user.email,
								order.user.profile.first_name,
								shipment.url || `${process.env.MAIN_WEB_URL}profile/order/${order.id}`,
								order.number,
								shipment.orderDetails,
							)
						} else if (data.status === SHIPMENT_STATUSES.DELIVERED) {
							NotificationManager.sendDeliveryNotice(
								order.id,
								order.user.id,
								order.user.email,
								order.user.profile.first_name,
								order.number,
								shipment.orderDetails,
							)
						}
					})
				}
			}

			return shipment
		})
	}

	@ManagerModel.bound
	async createAddress(
		data: Parameter<InterfaceAddressModel>,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.insertAddress(data, transaction)
	}

	@ManagerModel.bound
	async createAutomatedShipmentsFromOrderDetails(
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>,
		config: {
			type?: SHIPMENTS,
			courier?: string,
			service?: string,
			is_facade?: boolean,
		},
		transaction: EntityManager,
	): Promise<Array<ShipmentInterface & {
		origin: BrandAddressInterface,
		destination: OrderAddressInterface,
		packet: Parameter<PacketInterface>,
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
	}>> {
		// Dedupe ids
		const ids = []

		function makeId(_id: number = Date.now()): number {
			let id = _id

			if (ids.indexOf(id) > -1) {
				id = id + 1000 + Math.round(Math.random() * 1000)

				return makeId(id)
			} else {
				ids.push(id)

				return id
			}
		}
		// first we list all possible destinations
		// cannot just use location_id because the address might differ even tho
		// it is on the same address (e.g: floor)
		const details = await Promise.all(order_details.map(detail => this.getOriginAndDestinationFromOrderDetail(detail, transaction)))
		const destinations: { [key: number]: InterfaceAddressModel } = keyBy(await Promise.all(details.map(orderDetail => orderDetail.destination)), address => address.id)
		const origins: { [key: number]: InterfaceAddressModel } = keyBy(await Promise.all(details.map(orderDetail => orderDetail.origin)), address => address.id)
		const orderDetailsByKey: { [key: string]: typeof details } = groupBy(details, orderDetail => {
			let shippingIn: number

			if (orderDetail.is_digital) {
				shippingIn = 0
			} else {
				shippingIn = TimeHelper.moment(orderDetail.shipment_at).diff(TimeHelper.moment(), 'd')
			}

			return `${ orderDetail.origin.id }|${ orderDetail.destination.id }|${ shippingIn }`
		})

		return Promise.all(Object.keys(orderDetailsByKey).map(async key => {
			const keys = key.split('|')

			const orderDetails: typeof details = orderDetailsByKey[key]
			const packet = await this.getProductPacket(orderDetails, transaction)
			const totalPacketPrice = orderDetails.map(orderDetail => orderDetail.retail_price).reduce(CommonHelper.sum, 0)
			const shipment = config.type === undefined ? SHIPMENTS.MANUAL : config.type

			// ========================================================
			// NOTE:
			// If packet has no width / length / height / weight,
			// It means that packet is digital only, return no shipment
			// ========================================================

			if (packet.length === 0 || packet.height === 0 || packet.width === 0 || packet.weight === 0) {
				return null
			} else {
				const rate = (await this.getRates(
					origins[keys[0]],
					destinations[keys[1]],
					packet,
					totalPacketPrice,
					[shipment],
					config.courier,
					config.service === undefined ? ShipmentTikiManager.getDefaultService() : config.service,
					transaction,
				))[0]

				return {
					origin: origins[keys[0]] as BrandAddressInterface,
					destination: destinations[keys[1]] as OrderAddressInterface,
					packet,
					orderDetails: orderDetails.map(orderDetail => order_details.find(oD => oD.id === orderDetail.id)),
					// 'type' | 'status' | 'courier' | 'service' | 'note' | 'amount' | 'prices' | 'is_facade' | 'metadata'
					type: shipment,
					status: SHIPMENT_STATUSES.PENDING,
					courier: rate.courier,
					service: rate.service,
					note: 'Facade shipment made from automated system.',
					amount: rate.rate,
					prices: rate.prices,
					is_facade: true,
					is_return: false,
					metadata: {
						max_delivery_day: rate.max_day,
						min_delivery_day: rate.min_day,
					},
				}
			}
		})).then(shipments => {
			return shipments.filter(shipment => shipment !== null)
		}).then(async shipments => {
			if (config.is_facade) {
				return shipments.map(shipment => {
					return {
						id: makeId(),
						created_at: new Date(),
						packet_id: shipment.packet.id,
						origin_id: shipment.origin.id,
						destination_id: shipment.destination.id,
						type: shipment.type,
						status: shipment.status,
						courier: shipment.courier,
						service: shipment.service,
						awb: null,
						url: null,
						note: shipment.note,
						amount: shipment.amount,
						prices: shipment.prices,
						is_facade: shipment.is_facade,
						is_return: shipment.is_return,
						metadata: shipment.metadata,
						// ======== ADDITIONALS
						origin: shipment.origin,
						destination: shipment.destination,
						packet: shipment.packet,
						orderDetails: shipment.orderDetails,
					}
				})
			} else {
				return shipments.reduce((promise, shipment) => {
					return promise.then(sum => {
						return this.create(
							shipment.type,
							shipment.packet,
							shipment.orderDetails,
							[shipment.origin, shipment.destination],
							shipment.is_facade,
							shipment.is_return,
							{
								amount: shipment.amount,
								prices: shipment.prices,
								note: shipment.note,
								metadata: shipment.metadata,
								created_at: new Date(),
								courier: shipment.courier,
								service: shipment.service,
								status: shipment.status,
							},
							false,
							transaction,
						).then(_shipment => {
							return [
								...sum,
								{
									..._shipment,
									origin: shipment.origin,
									destination: shipment.destination,
									packet: shipment.packet,
									orderDetails: shipment.orderDetails,
								},
							]
						})
					})
				}, Promise.resolve([]))
			}
		})
	}

	get createCampaign() {
		return this.ShipmentRepository.insertCampaign
	}

	get createCoupon() {
		return this.ShipmentRepository.insertCoupon
	}

	async createShipmentFromCSV(
		csv: string,
		transaction: EntityManager,
	) {
		const datas: Array<{
			awb: string,
			order_detail_ids: number[],
			courier: string,
			service: string,
			url: string,
			status: string,
			type: string,
		}> = csvjson.toObject(csv, {delimiter: ','})
			.reduce((i, data: {order_detail_id: string, courier: string, service: string, awb: string, url: string, status: string, type: string}) => {
				const currentAwbIndex = i.findIndex(_i => _i.awb === data.awb)

				if (currentAwbIndex > -1) {
					i[currentAwbIndex].order_detail_ids.push(parseInt(data.order_detail_id, 10))

					return i
				} else {
					return i.concat({
						awb: data.awb,
						order_detail_ids: [parseInt(data.order_detail_id, 10)],
						courier: data.courier,
						service: data.service,
						url: data.url,
						status: data.status,
						type: data.type,
					})
				}
			}, [])

		const origin = await BrandManager.getMainAddress(DEFAULTS.YUNA_BRAND_ID, transaction)

		return CommonHelper.serialize(datas, async (promise, data) => {
			return promise.then(async () => {

				// Validate data
				const awb = data.awb
				const order_detail_ids = data.order_detail_ids
				const courier = data.courier
				const service = data.service
				const url = data.url
				const status = data.status
				let type = data.type as SHIPMENTS

				if (
					check(awb, 'string') &&
					check(order_detail_ids, ['array', 'number']) &&
					check(courier, 'string') &&
					check(service, 'string') &&
					check(url, 'string') &&
					check(status, ['enum', [
						'PENDING',
						'PROCESSING',
						'ON_COURIER',
						'FAILED',
						'DELIVERED',
						'DELIVERY_CONFIRMED',
						'EXCEPTION',
					]])
				) {
					const detail = await OrderManager.detail.getWithAddressAndPacket(order_detail_ids[0], transaction)

					return this.create(SHIPMENTS.MANUAL, detail.packet, order_detail_ids, [{
						location_id: detail.address.location_id,
						title: detail.address.title,
						receiver: detail.address.receiver,
						phone: detail.address.phone,
						address: detail.address.address,
						district: detail.address.district,
						postal: detail.address.postal,
					}], false, false, {
						created_at: new Date(),
						courier,
						service,
						awb,
						url,
						amount: await this.getRates(origin, {
							location_id: detail.address.location_id,
							title: detail.address.title,
							receiver: detail.address.receiver,
							phone: detail.address.phone,
							address: detail.address.address,
							district: detail.address.district,
							postal: detail.address.postal,
						}, detail.packet, detail.price, [SHIPMENTS.MANUAL], data.courier, data.service, transaction).then(rate => rate[0].rate),
						status: status as SHIPMENT_STATUSES,
					}, true, transaction)
					.then(async shipment => {
						
						if (type in SHIPMENTS) {
							await this.update(DEFAULTS.SYSTEM_USER_ID, shipment.id, {type}, false, undefined, transaction)
						} else {
							await this.update(DEFAULTS.SYSTEM_USER_ID, shipment.id, {}, false, undefined, transaction)
						}

						return shipment
					})
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Parameter not satisfied; CSV must contain: order_detail_id, courier, service, awb, url, status, and type.')
				}
			})
		})
	}

	@ManagerModel.bound
	async createStatus(
		changer_user_id: number,
		shipment_id: number,
		code: SHIPMENT_STATUSES,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.update(changer_user_id, shipment_id, {
			status: code,
			note,
		}, undefined, transaction)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateAddress(
		shipment_address_id: number,
		data: Parameter<InterfaceAddressModel>,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.updateAddress(shipment_address_id, data, transaction)
	}

	@ManagerModel.bound
	async update(
		changer_user_id: number,
		shipment_id: number,
		data: Partial<ShipmentInterface>,
		shouldSendEmail: boolean = false,
		updated_at: Date | undefined,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.update(changer_user_id, shipment_id, data, updated_at, transaction).then(async isUpdated => {
			if(isUpdated && shouldSendEmail) {
				if(
					data.status === SHIPMENT_STATUSES.PROCESSING
					|| data.status === SHIPMENT_STATUSES.DELIVERED
				) {
					const shipment = await this.getByIds([shipment_id], transaction).then(s => s[0])
					const orders = await OrderManager.getOrderNumbersAndUserFromOrderDetails(shipment.orderDetails.map(oD => oD.id), transaction)

					orders.forEach(order => {
						if(data.status === SHIPMENT_STATUSES.PROCESSING) {
							NotificationManager.sendShippingNotice(
								shipment.id,
								order.user.id,
								order.user.email,
								order.user.profile.first_name,
								shipment.url || `${process.env.MAIN_WEB_URL}profile/order/${order.id}`,
								order.number,
								shipment.orderDetails,
							)
						} else if(data.status === SHIPMENT_STATUSES.DELIVERED) {
							NotificationManager.sendDeliveryNotice(
								order.id,
								order.user.id,
								order.user.email,
								order.user.profile.first_name,
								order.number,
								shipment.orderDetails,
							)
						}
					})
				}
			}

			return isUpdated
		})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number | undefined,
		limit: number | undefined,
		filter: {
			type?: SHIPMENTS,
			sort?: 'ASC' | 'DESC',
			status?: SHIPMENT_STATUSES,
			search?: string
			with_order?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.filter(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	async getAddressDeep(
		address_id: number,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getAddressDeep(address_id, transaction)
	}

	@ManagerModel.bound
	async getByIds(
		shipment_ids: number[],
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getByIds(shipment_ids, transaction)
	}

	@ManagerModel.bound
	async getBy(
		shipment: Parameter<Partial<ShipmentInterface>>,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getBy(shipment, transaction)
	}

	@ManagerModel.bound
	async getActive(
		offset: number,
		limit: number,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getActive(offset, limit, transaction)
	}

	@ManagerModel.bound
	async getDelivered(
		date: Date,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getDelivered(date, transaction)
	}

	@ManagerModel.bound
	async getRatesAndPacketFromOrderDetail(
		order_detail_ids: number[],
		destination: Parameter<InterfaceAddressModel>,
		types: SHIPMENTS[] = [],
		courier: string | undefined,
		service: string | undefined,
		transaction: EntityManager,
	): Promise<{
		packet: PacketInterface,
		rates: RateInterface[],
	}> {
		const orderDetails = await OrderManager.detail.getByIds(order_detail_ids, false, transaction).then(oDs => {
			return Promise.all(oDs.map(async oD => {
				const origin = await this.getOriginFromOrderDetail({
					...oD,
					request: oD.orderRequest,
				}, transaction)

				return {
					...oD,
					origin,
				}
			}))
		})

		// Test if origin the same
		const haveSameOrigin = uniqBy(orderDetails, orderDetail => {
			return orderDetail.origin.id
		}).length === 1

		if(haveSameOrigin) {
			const packet = await PacketManager.createFromIds(orderDetails.map(od => od.packet_id), true, transaction)

			return {
				packet,
				rates: await this.getRates(
					orderDetails[0].origin,
					destination,
					packet,
					orderDetails.map(oD => oD.retail_price).reduce(CommonHelper.sum, 0),
					types,
					courier,
					service,
					transaction,
				),
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Order details have many origin. It should have been a single origin instead.')
		}
	}

	@ManagerModel.bound
	async getRates(
		origin: Parameter<InterfaceAddressModel>,
		destination: Parameter<InterfaceAddressModel>,
		packet: Parameter<PacketInterface>,
		price: number,
		types: SHIPMENTS[] = [],
		courier: string | undefined,
		service: string | undefined,
		transaction: EntityManager,
	): Promise<RateInterface[]> {
		if (!origin.location_id || !destination.location_id) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Origin or destination has no location id')
		}

		return CommonHelper.flatten(await Promise.all((types.length ? types : AVAILABLE_SHIPMENTS).map(async type => {
			return await this.getRatesFrom(type, packet, price, origin, destination, courier, service, transaction).catch(err => {
				if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					err.detail = 'Shipment Error: no address with location id found!'
				}

				throw err
			})
		})))
	}

	@ManagerModel.bound
	async getShipmentFromOrderDetail(
		order_detail_id: number,
		is_facade: boolean | undefined,
		is_return: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.getByOrderDetailId(order_detail_id, is_facade, is_return, transaction)
	}

	@ManagerModel.bound
	async getSicepatWaybil(shipment_id: number, transaction: EntityManager) {
		const shipment = await this.getByIds([shipment_id], transaction).then(s => s[0])

		return await ShipmentSicepatManager.getByWaybill(shipment.awb, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async statusToProcessing(
		changer_user_id: number,
		shipment_id: number,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.update(changer_user_id, shipment_id, {
			status: SHIPMENT_STATUSES.PROCESSING,
		}, undefined, transaction).then(async isUpdated => {
			if (isUpdated && shouldSendEmail) {
				const shipment = await this.getByIds([shipment_id], transaction).then(s => s[0])
				const orders = await OrderManager.getOrderNumbersAndUserFromOrderDetails(shipment.orderDetails.map(oD => oD.id), transaction)

				orders.forEach(order => {
					NotificationManager.sendShippingNotice(
						shipment.id,
						order.user.id,
						order.user.email,
						order.user.profile.first_name,
						shipment.url || `${process.env.MAIN_WEB_URL}profile/order/${order.id}`,
						order.number,
						shipment.orderDetails,
					)
				})
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async statusToDelivered(
		changer_user_id: number,
		shipment_id: number,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		return this.ShipmentRepository.update(changer_user_id, shipment_id, {
			status: SHIPMENT_STATUSES.DELIVERED,
		}, undefined, transaction).then(async isUpdated => {
			if (isUpdated) {
				const shipment = await this.getByIds([shipment_id], transaction).then(s => s[0])
				const orders = await OrderManager.getOrderNumbersAndUserFromOrderDetails(shipment.orderDetails.map(oD => oD.id), transaction)

				// resolve if this is not a stylesheet
				await Promise.all(shipment.orderDetails.map(async detail => {
					switch(detail.type) {
					case ORDER_DETAILS.STYLESHEET:
					default:
						return false
					case ORDER_DETAILS.INVENTORY:
					case ORDER_DETAILS.VOUCHER:
							return OrderManager.detail.resolveOrderDetail(changer_user_id, detail.id, false, 'Resolving order detail because of delivered item.', transaction)
					}
				}))

				if(shouldSendEmail) {
					orders.forEach(order => {
						NotificationManager.sendDeliveryNotice(
							order.id,
							order.user.id,
							order.user.email,
							order.user.profile.first_name,
							order.number,
							shipment.orderDetails,
						)
					})
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async confirmDelivery(
		changer_user_id: number,
		user_id: number | null,
		shipment_id: number,
		is_return: boolean,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		if(user_id) {
			await this.ShipmentRepository.getBelong(user_id, shipment_id, transaction)
		}

		return this.ShipmentRepository.update(changer_user_id, shipment_id, {
			status: SHIPMENT_STATUSES.DELIVERY_CONFIRMED,
			confirmed_at: new Date(),
		}, undefined, transaction).then(async isUpdated => {
			if(isUpdated && !is_return) {
				await this.ShipmentRepository.getOrderDetails([shipment_id], false, false, transaction).then(orderDetails => {
					return Promise.all(orderDetails.map(async detail => {
						// resolve order and order detail
						await OrderManager.detail.resolveOrderDetail(changer_user_id, detail.id, false, 'Resolving order detail because of delivery confirmation.', transaction).catch(null)

						return detail
					})).then(async details => {
						const order1 = await OrderManager.get(details[0].order_id, transaction)
						// resolve order
						await details.reduce((order_ids, detail) => {
							if (order_ids.indexOf(detail.order_id) === -1) {
								return order_ids.concat(detail.order_id)
							} else {
								return order_ids
							}
						}, []).reduce((promise, order_id) => {
							return promise.then(async init => {
								await OrderManager.update(
									changer_user_id,
									order_id,
									{
										status: ORDERS.RESOLVED,
									},
									'Resolving order detail because of delivery confirmation.',
									transaction,
								)
								const order = await OrderManager.get(order_id, transaction)
								const from_user = await UserManager.referral.getByUserRef(order.user_id, transaction).catch(async err => {
									const user = await UserManager.get(order.user_id, transaction)
									const refCode = `${user.profile.first_name}${user_id ? user_id : order.user_id}`
									await UserManager.insertRefCode(user.user_profile_id, refCode, transaction)
								})
								// const order = await this.OrderRepository.get(order_id, true, transaction)

								if (from_user) {
									this.log('injecting point')
									await UserManager.point.depositPoint(from_user.user_id, from_user.amount, STATEMENT_SOURCES.USER, from_user.user_id, `Bonus Referral Point from ${ from_user.user_reference }`, transaction)
									await UserManager.referral.updateStatus(from_user.user_id, order.user_id, transaction)
								}

							})
						}, Promise.resolve(null))

						// Send deliveryConfirmationNotice
						if (shouldSendEmail) {
							NotificationManager.sendDeliveryConfirmationNotice(
								user_id ? user_id : order1.user_id,
								details[0].order_id,
							)
						}
					})
				})
			}

			return isUpdated
		})

		// TODO: SEND EMAIL
	}

	@ManagerModel.bound
	getFacadeShipmentTypeForRate() {
		return [SHIPMENTS.TIKI]
	}

	@ManagerModel.bound
	getLatestShipmentHistory(
		type: SHIPMENTS,
		awb: string,
	) {
		return this.getShipmentHistory(type, awb).then(histories => {
			const history = histories[0]

			if (!history) {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'No history found')
			}

			return history
		})
	}

	@ManagerModel.bound
	async trackShipment(
		awb: string,
	) {
		return ShipmentSicepatManager.trackPacket(awb)
	}

	@ManagerModel.bound
	getShipmentHistory(
		type: SHIPMENTS,
		awb: string,
	) {
		switch(type) {
		case SHIPMENTS.TIKI:
			return ShipmentTikiManager.getShipmentHistory(awb).then(histories => {
				return histories.map(history => {
					switch (history.status) {
					case CONNOTE_STATUS['CON 01']:
						return {
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['DEL 01']:
						return {
							status: SHIPMENT_STATUSES.ON_COURIER,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['DEX 01']:
					case CONNOTE_STATUS['DEX 02']:
					case CONNOTE_STATUS['DEX 03']:
					case CONNOTE_STATUS['DEX 04']:
					case CONNOTE_STATUS['DEX 05']:
					case CONNOTE_STATUS['DEX 06']:
					case CONNOTE_STATUS['DEX 07']:
					case CONNOTE_STATUS['DEX 08']:
					case CONNOTE_STATUS['DEX 09']:
					case CONNOTE_STATUS['DEX 10']:
					case CONNOTE_STATUS['DEX 11']:
					case CONNOTE_STATUS['DEX 12']:
					case CONNOTE_STATUS['DEX 13']:
					case CONNOTE_STATUS['DEX 14']:
						return {
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['INC 01']:
					case CONNOTE_STATUS['MDE 01']:
					case CONNOTE_STATUS['MDE 02']:
					case CONNOTE_STATUS['MDE 03']:
					case CONNOTE_STATUS['MDE 04']:
						return {
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['MDE 06']:
					case CONNOTE_STATUS['MDE 07']:
					case CONNOTE_STATUS['MDE 08']:
						return {
							status: SHIPMENT_STATUSES.FAILED,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['MDE 09']:
					case CONNOTE_STATUS['MDE 10']:
					case CONNOTE_STATUS['MDE 11']:
					case CONNOTE_STATUS['MDE 12']:
					case CONNOTE_STATUS['MDE 15']:
					case CONNOTE_STATUS['MDE 18']:
						return {
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['POD 01']:
					case CONNOTE_STATUS['POD 02']:
					case CONNOTE_STATUS['POD 03']:
					case CONNOTE_STATUS['POD 04']:
						return {
							status: SHIPMENT_STATUSES.DELIVERED,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					case CONNOTE_STATUS['PUP 01']:
					case CONNOTE_STATUS['PUP 05']:
					case CONNOTE_STATUS.TRS:
						return {
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.entry_date,
							note: history.noted || CONNOTE_STATUS_DESCRIPTION[history.status],
						}
					}
				})
			})
		case SHIPMENTS.MANUAL:
		case SHIPMENTS.SICEPAT:
			return ShipmentSicepatManager.latestPacketStatus(awb).then(history => {
				// return histories.map(history => {
					switch (history.status as any) {
					case 'IN':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'OUT':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'PICKREQ':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'PICK':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'INP':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'ANT':
						return [{
							status: SHIPMENT_STATUSES.PROCESSING,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'DELIVERED':
						return [{
							status: SHIPMENT_STATUSES.DELIVERED,
							created_at: history.date_time,
							note: history.receiver_name,
						}]
					case 'UNPICK':
						return [{
							status: SHIPMENT_STATUSES.FAILED,
							created_at: history.date_time,
							note: history.city,
						}]
					case '(RTS) Return To Shipper':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'NTH (Not At Home)':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'Cnee Unkown':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'AU (Antar Ulang)':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'MR (Misroute)':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'Criss Cross':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'CODA (Closed Once Delivery Attempt)':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'LOST (Hilang)':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'BROKE':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'Retur Pusat':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'RTA':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'Hold / Pending':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'CODB':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'OTS':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					case 'OSD':
						return [{
							status: SHIPMENT_STATUSES.EXCEPTION,
							created_at: history.date_time,
							note: history.city,
						}]
					}
				// })
			})
		default:
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Getting history of ${type} is not configured yet`)
		}
	}

	@ManagerModel.bound
	async sendShipmentEmail(
		shipment_id: number,
		transaction: EntityManager,
	) {
		const shipment = await this.getByIds([shipment_id], transaction).then(shipments => shipments[0])
		const order = await OrderManager.detail.getOrder(shipment.orderDetails[0].id, transaction)

		switch(shipment.status) {
		case SHIPMENT_STATUSES.DELIVERED:
		case SHIPMENT_STATUSES.DELIVERY_CONFIRMED:
			return NotificationManager.sendDeliveryNotice(
				order.id,
				order.user.id,
				order.user.email,
				order.user.profile.first_name,
				order.number,
				shipment.orderDetails,
			)
		case SHIPMENT_STATUSES.PROCESSING:
			return NotificationManager.sendShippingNotice(
				shipment.id,
				order.user.id,
				order.user.email,
				order.user.profile.first_name,
				shipment.url,
				order.number,
				shipment.orderDetails,
			)
		default:
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Shipment is not eligible for an email update.')
		}
	}

	// ============================ PRIVATES ============================
	private async getProductPacket(
		orderDetails: OrderDetailInterface[],
		transaction: EntityManager,
	) {
		return Promise.all(orderDetails.map(orderDetail => {
			return orderDetail.packet_id
			// return this.getPacketIdFromItem(orderDetail.type, orderDetail)
		})).then(packetIds => {
			return PacketManager.createFromIds(packetIds, true, transaction)
		})
	}

	// private async getPacketIdFromItem<T extends ORDER_DETAILS>(
	// 	type: T,
	// 	item: OrderDetailInterface,
	// 	// item: T extends ORDER_DETAILS.INVENTORY
	// 	// 	? OrderDetailItemInterface<false, ORDER_REQUESTS.INVENTORY>
	// 	// 	: T extends ORDER_DETAILS.STYLESHEET
	// 	// 		? OrderDetailItemInterface<false, ORDER_REQUESTS.MATCHBOX>
	// 	// 		: OrderDetailItemInterface<true>,
	// ) {
	// 	switch(type) {
	// 	case ORDER_DETAILS.STYLESHEET:
	// 		return item.packet_id
	// 		const stylesheet = (item as OrderDetailItemInterface<false, ORDER_REQUESTS.MATCHBOX>).stylesheet
	// 		return stylesheet.packet_id || stylesheet.matchbox.packet_id
	// 	case ORDER_DETAILS.INVENTORY:
	// 		const inventory = (item as OrderDetailItemInterface<false, ORDER_REQUESTS.INVENTORY>).inventory
	// 		return inventory.packet_id || inventory.variant.packet_id || inventory.variant.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID
	// 	case ORDER_DETAILS.VOUCHER:
	// 		const voucher = (item as OrderDetailItemInterface<true>).voucher
	// 		return voucher.is_physical ? DEFAULTS.VOUCHER_PACKET_ID : DEFAULTS.DIGITAL_PACKET_ID
	// 	}
	// }

	private async getRatesFrom(
		type: SHIPMENTS,
		packet: Parameter<PacketInterface>,
		price: number,
		origin: Parameter<InterfaceAddressModel>,
		destination: Parameter<InterfaceAddressModel>,
		courier: string | undefined,
		service: string | undefined,
		transaction: EntityManager,
	): Promise<RateInterface[]> {
		switch(type) {
		default:
			return []
		case SHIPMENTS.TIKI:
			const originRateId: string = await MasterManager.location.getArea(type, origin.location_id, {}, origin.metadata, transaction).then(area => area.tariff_code)
			const destinationRateId: string = await MasterManager.location.getArea(type, destination.location_id, {}, destination.metadata, transaction).then(area => area.tariff_code)

			return ShipmentTikiManager.getServices(originRateId, destinationRateId, packet.weight, service).then(rates => {
				return rates.map(rate => {
					return {
						type: SHIPMENTS.TIKI,
						courier: 'Tiki',
						service: rate.SERVICE,
						rate: parseInt(rate.TARIFF, 10) || 0,
						description: rate.DESCRIPTION,
						min_day: parseInt(rate.EST_DAY, 10) || 0,
						max_day: parseInt(rate.EST_DAY, 10) || 0,
						prices: {},
					}
				})
			})
		case SHIPMENTS.SICEPAT:
			const originRate: string = await MasterManager.location.getArea(type, origin.location_id, {slug: 'origin'}, origin.metadata, transaction).then(sicepat => sicepat.code)
			const destinationRate: string = await MasterManager.location.getArea(type, destination.location_id, {slug: 'destination'}, destination.metadata, transaction).then(sicepat => sicepat.code)

			return ShipmentSicepatManager.getService(originRate, destinationRate, packet.weight).then(rates => {
				return rates.map(rate => {
					return {
						type: SHIPMENTS.SICEPAT,
						courier: 'Sicepat',
						service: rate.service,
						rate: rate.tariff,
						description: rate.description,
						min_day: parseInt(rate.etd.slice(0, 1), 10),
						max_day: parseInt(rate.etd.slice(4, 5), 10),
						prices: {},
					}
				})
			})

		case SHIPMENTS.MANUAL:
			return MasterManager.location.getArea(type, destination.location_id, {}, destination.metadata, transaction).then(area => {
				return [{
					type: SHIPMENTS.MANUAL,
					courier: 'Shipment',
					service: 'Reguler',
					rate: (area.price * Math.max(Math.ceil(packet.weight), 1)),
					description: 'Manual shipment.',
					min_day: area.min_day,
					max_day: area.max_day,
					prices: {},
				}]
			})
		}
	}

	private async getShipmentNumber(type: SHIPMENTS, transaction: EntityManager) {
		const now = TimeHelper.moment()
		const startMinute = TimeHelper.moment(now).startOf('minute')
		const endMinute = TimeHelper.moment(now).endOf('minute')

		return await this.ShipmentRepository.getCountInCurrentMinute(transaction, startMinute.toDate(), endMinute.toDate()).then(result => {
			return `${ this.getPrefix(type) }${ now.format('YYMMDD') }${ padStart((result + 1).toString(), 2, '0') }${ now.format('mmHH') }`
		})
	}

	private getPrefix(type: SHIPMENTS) {
		switch(type) {
		case SHIPMENTS.MANUAL:
		default:
			return 'YCS'
		case SHIPMENTS.TIKI:
			return process.env.TIKI_PREFIX
		}
	}

	private async getOriginAndDestinationFromOrderDetail(detail: OrderDetailInterface & {
		request: OrderRequestInterface & {
			address: OrderAddressInterface,
		},
	}, transaction: EntityManager): Promise<OrderDetailInterface & {
		origin: InterfaceAddressModel,
		destination: InterfaceAddressModel,
		request: OrderRequestInterface,
	}> {
		return {
			...detail,
			origin: await this.getOriginFromOrderDetail(detail, transaction),
			destination: detail.request.address,
		}
	}

	private async getOriginFromOrderDetail(detail: OrderDetailInterface & { request: OrderRequestInterface }, transaction: EntityManager) {
		return Promise.resolve().then(async () => {
			switch (detail.type) {
			case ORDER_DETAILS.INVENTORY: {
				return 'inventory'
			}
			case ORDER_DETAILS.STYLESHEET: {
				switch (detail.request.type) {
				case ORDER_REQUESTS.MATCHBOX: {
					return MatchBoxManager.getBrand(detail.request.ref_id, transaction)
				}
				case ORDER_REQUESTS.STYLEBOARD: {
					// TODO: Change into service manager
					return BrandManager.get(DEFAULTS.YUNA_BRAND_ID, transaction)
				}
				case ORDER_REQUESTS.VOUCHER: {
					const voucher = await VoucherManager.get(detail.request.ref_id, false, transaction)
					switch (voucher.type) {
					case VOUCHERS.MATCHBOX: {
						return MatchBoxManager.getBrand(voucher.ref_id, transaction)
					}
					case VOUCHERS.INVENTORY:
					default:
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Voucher type not well defined')
					}
				}
				default:
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Order request type not well defined')
				}
			}
			case ORDER_DETAILS.WALLET_TRANSACTION:
				return BrandManager.get(DEFAULTS.YUNA_BRAND_ID, transaction)
			case ORDER_DETAILS.STYLEBOARD:
				return BrandManager.get(DEFAULTS.YUNA_BRAND_ID, transaction)
			case ORDER_DETAILS.VOUCHER:
				return BrandManager.get(DEFAULTS.YUNA_BRAND_ID, transaction)
			default:
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Order detail type not well defined')
			}
		}).then(async brand => {
			return Promise.resolve().then(() => {
				if (brand === 'inventory') {
					return InventoryManager.getAddress(detail.ref_id, transaction)
				} else {
					return BrandManager.getMainAddress(brand.id, transaction)
				}
			}).catch(err => {
				if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					// Brand have no main address, set as yuna's main address
					return BrandManager.getMainAddress(DEFAULTS.YUNA_BRAND_ID, transaction)
				}

				throw err
			})
		})
	}

	// private async getDestinationFromOrderRequest(request: OrderRequestInterface, trx?: EntityManager) {
	// 	return this.withTransaction(trx, async transaction => {
	// 		return OrderManager.address.get(request.order_id, request.order_address_id, transaction)
	// 	})
	// }

}

export default new ShipmentManager()
