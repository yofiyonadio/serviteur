import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection } from 'typeorm'

import MasterManager from '../master'

// import { OtherTikiAreaInterface } from 'energie/app/interfaces'
import InterfaceAddressModel from 'energie/app/models/interface.address'
// import { OtherTikiAreaInterface } from 'energie/app/interfaces'

import TikiService from 'app/services/tiki'
import CommonHelper from 'utils/helpers/common'
import Tikis from 'utils/constants/tiki'
import { Parameter } from 'types/common'

import { ERRORS, ERROR_CODES } from 'app/models/error'

// import * as Fuse from 'fuse.js'

// import * as TikiArea from './_tiki_area.json'


class ShipmentTikiManager extends ManagerModel {

	static __displayName = 'ShipmentTikiManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [
		], async () => {
			await MasterManager.initialize(connection)
			// await this.OtherTikiRepository.getLast().then(last => {
			// 	const filtered = (TikiArea as any as OtherTikiAreaInterface[]).filter(t => t.id > last.id)

			// 	if(filtered.length) {
			// 		return this.OtherTikiRepository.insert(filtered)
			// 	}
			// }).catch(async err => {
			// 	if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
			// 		return this.OtherTikiRepository.insert(TikiArea as any)
			// 	}
			// })
			// this.updateNullLocationId(.4)
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createShipmentOrder(
		id: string,
		description: string,							// packet content description
		value: number,									// packet content value, 0 to remove insurance
		service: string | undefined,					// courier service
		consignor: Parameter<InterfaceAddressModel>,	// origin
		consignee: Parameter<InterfaceAddressModel>,	// destination
		is_cod: 0 | 1 = 0,								// 1 means COD
		awb?: string,									// custom awb number
	) {
		return TikiService.createShipment(id, description, 0, CommonHelper.default(service, this.getDefaultService()), consignor, consignee, is_cod, awb)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	getDefaultService() {
		return Tikis.whitelist.service[0]
	}

	@ManagerModel.bound
	async getServices(
		origin_tariff_code: string,
		destination_tariff_code: string,
		weight: number,
		service?: string,
	) {
		return TikiService.getServices(
			origin_tariff_code,
			destination_tariff_code,
			weight,
		).then(services => {
			if(service !== undefined) {
				return [this.findWhitelistedService(services, service)]
			} else {
				return services
			}
		})
	}

	@ManagerModel.bound
	async getShipmentInfo(awb: string) {
		return TikiService.getShipmentInfo(awb)
	}

	@ManagerModel.bound
	async getShipmentHistory(awb: string) {
		return TikiService.getShipmentHistory(awb)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private findWhitelistedService(
		rates?: Array<{
			SERVICE: string,						// PRODUCT SERVICE
			DESCRIPTION: string,					// DESCRIPTION
			TARIFF: string,							// PRICE
			EST_DAY: string,						// ESTIMATED SHIPMENT DAY
		}>,
		service?: string,
	) {
		if (rates === undefined) {
			throw new ErrorModel(ERRORS.NODATA, ERROR_CODES.ERR_105, 'No rates provided')
		}

		if (service !== undefined) {
			return rates.find(rate => {
				return rate.SERVICE === service
			}) || this.findWhitelistedService(rates)
		}

		const _service = Tikis.whitelist.service.find(__service => {
			return rates.map(rate => rate.SERVICE).indexOf(__service) > -1
		})

		if(_service) {
			return rates.find(rate => {
				return rate.SERVICE === _service
			})
		} else {
			return rates[0]
		}
	}

	// private async startMapping(tolerance: number, areas: OtherTikiAreaInterface[], mapped: number = 0, transaction: EntityManager): Promise<number> {
	// 	if(areas.length) {
	// 		const area = areas.shift()
	// 		let _mapped = mapped

	// 		const possibleLocations = await MasterManager.location.getAreasFromPostalCode(area.zip_code, transaction).catch(err => {
	// 			this.warn('Error when getting areas', area.zip_code, err)

	// 			return []
	// 		})

	// 		const fuse = new Fuse(possibleLocations, {
	// 			shouldSort: true,
	// 			threshold: tolerance,
	// 			keys: [
	// 				'area_name',
	// 				'area_alias',
	// 				'suburb_name',
	// 				'suburh_alias',
	// 			],
	// 		})

	// 		const results = fuse.search(area.sub_dist)

	// 		if(results.length === 0) {
	// 			// no match, try to increase tolerance
	// 			this.log(`No match for #${area.id} – ${area.sub_dist}, try to increase tolerance`)
	// 		} else if(results.length === 1) {
	// 			_mapped++
	// 			await this.updateWithLocationId(area.id, results[0].id, transaction)
	// 		} else {
	// 			if(results[0].area_name.match(new RegExp(area.sub_dist.replace(/\(/g, '\\(').replace(/\)/g, '\\)'), 'i'))) {
	// 				_mapped++
	// 				await this.updateWithLocationId(area.id, results[0].id, transaction)
	// 			} else {
	// 				// duplicate matches, try to lower tolerance
	// 				this.log(`Duplicate match for #${area.id} – ${area.sub_dist} (${results.map(r => `${r.area_name}`).join('/')}), try to lower tolerance`)
	// 			}
	// 		}

	// 		return this.startMapping(tolerance, areas, _mapped, transaction)
	// 	} else {
	// 		this.log(`Done with ${mapped} area(s) mapped.`)
	// 		return mapped
	// 	}
	// }

}
export default new ShipmentTikiManager()
