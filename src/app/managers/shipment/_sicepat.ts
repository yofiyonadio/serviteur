import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'

import {
	Connection,
	EntityManager,
} from 'typeorm'

import MasterManager from '../master'

import SicepatService from 'app/services/sicepat'

import Sicepat from 'utils/constants/sicepat'

import { AreaSicepatRepository } from 'app/repositories'

import { PacketInterface } from 'energie/app/interfaces'

import InterfaceAddressModel from 'energie/app/models/interface.address'

import { Parameter } from 'types/common'
import { TimeHelper } from 'utils/helpers'

class ShipmentSicepatManager extends ManagerModel {

	static __displayName = 'ShipmentSicepatManager'

	AreaSicepatRepository: AreaSicepatRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			AreaSicepatRepository,
		], async () => {
			await MasterManager.initialize(connection)

			// this.transaction(async transaction => {
			// 	// await this.uploadAreas(transaction)

			// 	// await this.AreaSicepatRepository.getCompleteAddress(
			// 	// 	{
			// 	// 		location_id: 4798,
			// 	// 		title: 'Yuna Office',
			// 	// 		receiver: 'Yuna Ocnanuy',
			// 	// 		phone: '+628176333222',
			// 	// 		address: 'Jl. Tanjung Duren Raya No. 103, RT.6/RW.5\nTanjung Duren Selatan\nJakarta',
			// 	// 		district: 'Cakung',
			// 	// 		postal: '11470',
			// 	// 	}, transaction)
			// 	// 	.then(res => {this.log(res)})
			// 	// 	.catch(err => {this.warn(err)})
			// })
		})
	}

	async createShipment(
		origin: Parameter<InterfaceAddressModel>,
		destination: Parameter<InterfaceAddressModel>,
		reference_number: string,
		packet: PacketInterface,
		price: number,
		transaction: EntityManager,
	) {

		const origin_code: string = await this.AreaSicepatRepository.getByLocationId(origin.location_id, 'origin', transaction).then(res => res[0].code)
		const origin_detail = await this.AreaSicepatRepository.getCompleteAddress(origin, transaction)
		const destination_detail = await this.AreaSicepatRepository.getCompleteAddress(destination, transaction)

		return await SicepatService.createShipment(
				TimeHelper.format(new Date(), 'YYYY-MM-DD HH:mm'),
				origin_code,
				reference_number,
				await this.AreaSicepatRepository.markWaybill(transaction),
				price,
				Sicepat.whitelist.service[0],
				origin_detail,
				destination_detail,
				packet,
			).then(res => {
				return res.datas[0].receipt_number
			})
	}

	async trackPacket(
		waybill: string,
	) {
		return await SicepatService.trackPacket(waybill).then(packet => {
			return packet
		})
	}

	async latestPacketStatus(
		waybill: string,
	) {
		return await SicepatService.trackPacket(waybill).then(packet => {
			return packet.last_status
		})
	}

	getDefaultService() {
		return Sicepat.whitelist.service[0]
	}

	async getService(
		origin: string,
		destination: string,
		weight: number,
	) {
		return await SicepatService.getService(origin, destination, weight)
	}

	async getByWaybill(waybill: string, transaction: EntityManager) {
		return await this.AreaSicepatRepository.getByWaybill(waybill, transaction)
	}

	// ================================== METHOD ==================================
	// async uploadAreas(transaction: EntityManager) {
	// 	await this.AreaSicepatRepository.insert(SicepatArea as any, transaction)
	// 		.then(() => {this.log('Done, tapi cek dulu dah masuk apa belom')})
	// 		.catch(err => {this.warn(err)})
	// }

	// async getLatestAreas() {
	// 	const areas = []

	// 	await SicepatService.getOriginArea().then(origins => origins.map(origin => {
	// 		areas.push({
	// 			slug: 'origin',
	// 			subdistrict: '',
	// 			city: '',
	// 			province: '',
	// 			origin_name: origin.origin_name,
	// 			code: origin.origin_code,
	// 		})
	// 	}))

	// 	await SicepatService.getDestinationArea().then(destinations => destinations.map(destination => {
	// 		areas.push({
	// 			slug: 'destination',
	// 			subdistrict: destination.subdistrict,
	// 			city: destination.city,
	// 			province: destination.province,
	// 			origin_name: '',
	// 			code: destination.destination_code,
	// 		})
	// 	}))
	// }
}

export default new ShipmentSicepatManager()
