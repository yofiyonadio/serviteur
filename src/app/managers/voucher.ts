import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import PromoManager from './promo'

import VoucherRepository from 'app/repositories/voucher'

import { VoucherInterface, PacketInterface } from 'energie/app/interfaces'
import InterfaceAssetModel from 'energie/app/models/interface.asset'

import TimeHelper, { Moment } from 'utils/helpers/time'
import { Parameter } from 'types/common'

import { VOUCHER_STATUSES } from 'energie/utils/constants/enum'
import { ERROR_CODES, ERRORS } from 'app/models/error'

import { isEmpty } from 'lodash'
import { PacketManager } from '.'


class VoucherManager extends ManagerModel {

	static __displayName = 'VoucherManager'

	protected VoucherRepository: VoucherRepository

	private LASTORDERDATE: Moment
	private SHIPMENTDATE: Moment

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			VoucherRepository,
		], async () => {
			// Initialization function goes here
			this.resyncDate()
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		data: Parameter<VoucherInterface>,
		transaction: EntityManager,
	) {
		return this.VoucherRepository.insert(
			data.code,
			data.type,
			data.ref_id,
			data.status || VOUCHER_STATUSES.UNPUBLISHED,
			// Pengecualian
			data.packet_id,
			data.note,
			data.name,
			data.email,
			data.free_shipping,
			data.is_physical,
			data.expired_at,
			transaction,
		)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		voucher_id: number,
		data: Partial<Parameter<VoucherInterface, 'packet_id'>>,
		note: string | undefined,
		packet: Parameter<PacketInterface> | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.VoucherRepository.update(changer_user_id, voucher_id, data, note, transaction).then(async isUpdated => {
			if(isUpdated && !isEmpty(packet)) {
				await PacketManager.createOrUpdate('VOUCHER', voucher_id, undefined, packet, transaction)
			}
			return isUpdated
		})
	}

	// @ManagerModel.bound
	// async updatePacketId(voucher_id: number, new_packet_id: number, transaction: EntityManager) {
	// 	return this.VoucherRepository.getVoucher(voucher_id, transaction).then(voucher => {
	// 		return OrderManager.detail.getByRefId(ORDER_DETAILS.VOUCHER, voucher.id, transaction).then(async orderDetail => {
	// 			const packetId = voucher.packet_id

	// 			if (packetId === orderDetail.packet_id) {
	// 				await OrderManager.detail.update(orderDetail.id, { packet_id: new_packet_id }, '', transaction)
	// 			}

	// 			return this.update(voucher_id, {
	// 				packet_id: new_packet_id,
	// 			}, undefined, transaction)
	// 		})
	// 	})
	// }

	// ============================= GETTER =============================

	@ManagerModel.bound
	async get(
		voucher_id: number,
		availableOnly: boolean = false,
		transaction: EntityManager,
	) {
		if (availableOnly) {
			return this.VoucherRepository.getUnredeemedVoucher(voucher_id, [VOUCHER_STATUSES.AVAILABLE], transaction)
		} else {
			return this.VoucherRepository.getVoucher(voucher_id, transaction)
		}
	}

	@ManagerModel.bound
	async getWithReference(
		voucher_id: number,
		availableOnly: boolean = false,
		transaction: EntityManager,
	) {
		return this.VoucherRepository.getVoucherWithReference(voucher_id, availableOnly, transaction)
	}

	@ManagerModel.bound
	async getLastHistory(
		voucher_id: number,
		transaction: EntityManager,
	) {
		return this.VoucherRepository.getVoucherLastHistory(voucher_id, transaction)
	}

	@ManagerModel.bound
	async getAllWithStatus(
		offset: number,
		limit: number,
		status: VOUCHER_STATUSES,
		date: Date,
		transaction: EntityManager,
	) {
		return this.VoucherRepository.filter(offset, limit, status, date, transaction)
	}

	@ManagerModel.bound
	async getAsset(
		voucher_id: number,
		transaction: EntityManager,
	): Promise<InterfaceAssetModel> {
		return this.VoucherRepository.getAsset(voucher_id, transaction)
	}

	@ManagerModel.bound
	async getByCode(
		code: string,
		unredeemed: boolean = false,
		transaction: EntityManager,
	) {
		if (unredeemed) {
			return this.VoucherRepository.getUnredeemedVoucher(code, [VOUCHER_STATUSES.AVAILABLE], transaction).then(v => {
				return v
			})
		} else {
			return this.VoucherRepository.getVoucher(code, transaction).then(v => {
				return v
			})
		}
	}

	get lastOrderDate() {
		this.resyncIfPassed()
		return this.LASTORDERDATE
	}

	get shipmentDate() {
		this.resyncIfPassed()
		return this.SHIPMENTDATE
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async checkValidity(
		code: string,
		transaction: EntityManager,
	): Promise<VoucherInterface> {
		return this.VoucherRepository.getUnredeemedVoucher(code, [VOUCHER_STATUSES.AVAILABLE], transaction)
	}

	@ManagerModel.bound
	async generateRandomVoucherCode(prefix: string = 'YV-', length: number = 6) {
		return PromoManager.getRandomVoucherCode(prefix, length)
	}

	@ManagerModel.bound
	async cancel(
		changer_user_id: number,
		voucher_id: number,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.VoucherRepository.update(changer_user_id, voucher_id, {
			status: VOUCHER_STATUSES.EXCEPTION,
		}, note, transaction)
	}

	@ManagerModel.bound
	async publish(
		changer_user_id: number,
		voucher_id: number,
		note: string | undefined,
		transaction: EntityManager,
	): Promise<boolean> {
		return this.VoucherRepository.update(changer_user_id, voucher_id, {
			status: VOUCHER_STATUSES.AVAILABLE,
		}, note, transaction)
	}

	@ManagerModel.bound
	async redeem(
		changer_user_id: number,
		voucher_id: number,
		status: VOUCHER_STATUSES = VOUCHER_STATUSES.REDEEMED,
		note: string | undefined,
		transaction: EntityManager,
	) {
		await this.VoucherRepository.getUnredeemedVoucher(voucher_id, [VOUCHER_STATUSES.AVAILABLE], transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Voucher already redeemed')
			}

			throw err
		})

		return this.VoucherRepository.update(changer_user_id, voucher_id, {
			status,
		}, note, transaction)
	}

	@ManagerModel.bound
	async updateToRedeemed(
		changer_user_id: number,
		voucher_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.VoucherRepository.update(changer_user_id, voucher_id, {
			status: VOUCHER_STATUSES.REDEEMED,
		}, note, transaction)
	}

	// ============================ PRIVATES ============================
	private resyncDate() {
		this.LASTORDERDATE = TimeHelper.moment().hour() >= 13 ? TimeHelper.moment().add(1, 'd').hour(13).minute(0).second(0).millisecond(0) : TimeHelper.moment().hour(13).minute(0).second(0).millisecond(0)
		this.SHIPMENTDATE = TimeHelper.moment(this.LASTORDERDATE).add(1, 'd').startOf('d')
	}

	private resyncIfPassed() {
		if (+this.LASTORDERDATE < Date.now()) {
			this.resyncDate()
		}
	}

	// async createCustom(
	// 	item_id: number,
	// 	item_type: VOUCHERS,
	// 	is_physical?: boolean,
	// 	free_shipping?: boolean,
	// 	code?: string,
	// 	packet?: PacketInterface,
	// 	transaction?: EntityManager,
	// ) {
	// 	if(!transaction) {
	// 		return this.transaction(_transaction => {
	// 			return this.createCustom(
	// 				item_id,
	// 				item_type,
	// 				is_physical,
	// 				free_shipping,
	// 				code,
	// 				packet,
	// 				_transaction,
	// 			)
	// 		})
	// 	}

	// 	let packet_id = is_physical ? DEFAULTS.VOUCHER_PACKET_ID : DEFAULTS.DIGITAL_PACKET_ID

	// 	if (packet) {
	// 		packet_id = (await PacketManager.create(packet)).id
	// 	}

	// 	// const metadata: VoucherMetadataInterface = orderDetail.metadata as VoucherMetadataInterface
	// 	const config: any = {
	// 		transaction,
	// 	}
	// 	let itemPrice = 0

	// 	switch (item_type) {
	// 		case VOUCHERS.MATCHBOX:
	// 			const matchbox = await MatchboxManager.getOne(item_id)
	// 			config.validMatchboxIds = [item_id]
	// 			itemPrice = matchbox.price
	// 			break
	// 		case VOUCHERS.INVENTORY:
	// 			const inventory = await InventoryManager.getOne(item_id, true)
	// 			config.validVariantIds = [inventory.variant_id]
	// 			itemPrice = inventory.variant.price
	// 			break
	// 	}

	// 	return await this.VoucherRepository.save('VoucherRecord', {
	// 		id: null,
	// 		created_at: null,
	// 		packet_id,
	// 		code: code || CouponManager.getRandomVoucherCode('YV', 8),
	// 		// coupon_id: (await CouponManager.createCoupons({
	// 		// 	id: null,
	// 		// 	created_at: null,
	// 		// 	title: ,
	// 		// 	type: COUPONS.TOTAL,
	// 		// 	amount: itemPrice,
	// 		// 	free_shipping: !!free_shipping,
	// 		// 	is_valid_for_matchbox: item_type === VOUCHERS.MATCHBOX,
	// 		// 	is_valid_for_product: item_type === VOUCHERS.INVENTORY,
	// 		// 	usage_limit: 1,
	// 		// 	published_at: new Date(),
	// 		// 	expired_at: TimeHelper.moment().add(3, 'M').toDate(),
	// 		// }, {
	// 		// 	validMatchboxIds,
	// 		// 	validProductIds,
	// 		// 	validVariantIds,
	// 		// 	validUserIds,
	// 		// }, transaction)).id,
	// 		item_type,
	// 		ref_id: item_id,
	// 		type,
	// 		merchant,
	// 	} as VoucherInterface, transaction)
	// }
}

export default new VoucherManager()
