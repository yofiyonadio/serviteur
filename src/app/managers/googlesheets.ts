import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	GoogleSheetsRepository,
} from 'app/repositories'

class GoogleSheetsManager extends ManagerModel {

	protected GoogleSheetsRepository: GoogleSheetsRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			GoogleSheetsRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getData(query: string, transaction: EntityManager) {
		return this.GoogleSheetsRepository.getData(query, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new GoogleSheetsManager()
