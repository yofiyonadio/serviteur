import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import MasterManager from './master'
import MatchboxManager from './matchbox'
import OrderManager from './order'
import ProductManager from './product'
import PromoManager from './promo'
import UserManager from './user'

import {
	FeedbackAnswerInterface,
	FeedbackInterface,
	InventoryInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	OrderDetailInterface,
	ShipmentInterface,
	VariantInterface,
} from 'energie/app/interfaces'

import {
	FeedbackRepository,
	ExchangeRepository,
} from 'app/repositories'

import DEFAULTS from 'utils/constants/default'
import { FEEDBACKS, ORDER_DETAILS, STATEMENT_SOURCES } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import { Parameter } from 'types/common'
import { CommonHelper, TimeHelper } from 'utils/helpers'

class FeedbackManager extends ManagerModel {

	static __displayName = 'FeedbackManager'

	protected FeedbackRepository: FeedbackRepository
	protected ExchangeRepository: ExchangeRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			FeedbackRepository,
			ExchangeRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async createWithExchange(
		changer_user_id: number | null,
		user_id: number | null,
		order_detail_id: number,
		data: {
			type: FEEDBACKS,
			ref_id: number, // variant id or stylesheet id --> order_detail_id
			answers: Array<{
				question_id: number,
				answer: object,
			}>,
		},
		note: string | undefined,
		transaction: EntityManager,
	) {
		// ==============================================================
		// Before all, we should check the ownership
		// ==============================================================
		if(user_id) {
			await OrderManager.detail.getBelong(user_id, order_detail_id, transaction).then(async orderDetail => {
				switch(orderDetail.type) {
				case ORDER_DETAILS.STYLESHEET:
					const stylesheet = await MatchboxManager.stylesheet.getInventoriesFromStylesheet(orderDetail.ref_id, false, transaction)

					switch(data.type) {
					case FEEDBACKS.STYLESHEET:
						// URGENT, MODIFY REF ID OF STYLESHEET, TO BE DELETED ON NEXT RELEASE
						data.ref_id = orderDetail.ref_id

						if(data.ref_id !== stylesheet.id) {
							data.ref_id = stylesheet.id
						}
						return true
					case FEEDBACKS.VARIANT:
						if(stylesheet.stylesheetInventories.findIndex(sI => sI.inventory.variant_id === data.ref_id) === -1) {
							throw new Error()
						}
						return true
					}
				case ORDER_DETAILS.INVENTORY:
					return true
				case ORDER_DETAILS.VOUCHER:
				default:
					throw new Error('Type not configured.')
				}
			}).catch(err => {
				this.warn(err)
				throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, 'You are not authorized to send this feedback.')
			})
		}

		// ==============================================================
		// And check if feedback hasn't been created yet
		// ==============================================================
		const checkFeedback = await this.FeedbackRepository.getDetailByOrderDetailId(user_id, order_detail_id, transaction).catch(err => null)

		const feedbackVariant = checkFeedback ? checkFeedback.findIndex(fv => fv.user_id === user_id && fv.order_detail_id === order_detail_id && fv.type === 'VARIANT' && data.type === 'VARIANT' && data.ref_id === fv.ref_id) : -1
		const feedbackStylesheet = checkFeedback ? checkFeedback.findIndex(fs => fs.user_id === user_id && fs.order_detail_id === order_detail_id && fs.type === 'STYLESHEET' && data.type === 'STYLESHEET' && data.ref_id === fs.ref_id) : -1

		if(feedbackVariant > -1 || feedbackStylesheet > -1) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'User has submitted a feedback on this order detail before.')
		}

		// ==============================================================
		// And check if feedback complete
		// ==============================================================
		const VariantFeedbackQuestions = MasterManager.question.getByGroupId(DEFAULTS.VARIANT_QUESTION_GROUP_ID, transaction).then(_questions => {
			return Promise.all(_questions.map(async question => {
				return CommonHelper.stripUndefined({
					...question,
					questionCategories: undefined,
					category_ids: await Promise.all(question.questionCategories.map(qC => qC.category_id).map(cId => MasterManager.category.getAllChildrenIds(cId, transaction))).then(cIds => cIds.reduce(CommonHelper.sumArray, [])),
					questions: await Promise.all(question.questions.map(async q => {
						return CommonHelper.stripUndefined({
							...q,
							category_ids: await Promise.all(q.questionCategories.map(qC => qC.category_id).map(cId => MasterManager.category.getChildrenIds(cId, transaction))).then(cIds => cIds.reduce(CommonHelper.sumArray, [])),
							questionCategories: undefined,
						})
					})),
				})
			}))
		})
		const StylesheetFeedbackQuestions = MasterManager.question.getByGroupId(DEFAULTS.STYLESHEET_QUESTION_GROUP_ID, transaction)
		const answerQuestionIds = data.answers.map(a => a.question_id)

		switch(data.type) {
		case FEEDBACKS.STYLESHEET:
			StylesheetFeedbackQuestions.then(questions => {
				questions.forEach(q => {
					if(q.is_required && answerQuestionIds.indexOf(q.id) === -1) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'You need to answer all required questions.')
					}
				})
			})
			break
		case FEEDBACKS.VARIANT:
			const product = await ProductManager.variant.getProduct(data.ref_id, transaction)

			VariantFeedbackQuestions.then(questions => {
				questions.forEach(q => {
					if (q.is_required && q.category_ids.indexOf(product.category_id) > -1 && answerQuestionIds.indexOf(q.id) === -1) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'You need to answer all required questions.')
					}
				})
			})
			break
		default:
			throw new Error('Feedback type not configured.')
		}

		// ==============================================================
		// Cashback Handler
		// ==============================================================
		const order_id = await OrderManager.getOrderNumbersAndUserFromOrderDetails([order_detail_id], transaction).then(o => o[0].id)
		const lastFeedbacks = await this.FeedbackRepository.getShallow(user_id, order_id, transaction).catch(() => [])

		await PromoManager.cashback.normalHandler(user_id, order_id, lastFeedbacks, data, transaction)
		await UserManager.point.depositPoint(
			user_id,
			500,
			STATEMENT_SOURCES.CAMPAIGN,
			null,
			'Feedback Loyalty Program Reward',
			transaction,
		)

		// ==============================================================
		// create feedback
		// ==============================================================
		return this.FeedbackRepository.insert({
			user_id,
			order_detail_id,
			type: data.type,
			ref_id: data.ref_id,
		}, data.answers, transaction)
	}

	@ManagerModel.bound
	async create(
		feedback: Parameter<FeedbackInterface>,
		answers: FeedbackAnswerInterface[],
		transaction: EntityManager,
	) {
		return this.FeedbackRepository.insert(feedback, answers, transaction)
	}

	@ManagerModel.bound
	async createAnswers(
		feedback_id: number,
		answers: FeedbackAnswerInterface[],
		transaction: EntityManager,
	) {
		return this.FeedbackRepository.insertAnswer(feedback_id, answers, transaction)
	}

	// ============================= UPDATE =============================

	async updateAnswers(
		feedback_id: number,
		answers: FeedbackAnswerInterface[],
		transaction: EntityManager,
	) {
		return answers.map(async answer => {
			return await this.FeedbackRepository.updateAnswer(feedback_id, answer, transaction)
		})
	}

	// ============================= GETTER =============================
	get getShallow() {
		return this.FeedbackRepository.getShallow
	}

	async getFeedbackByOrderId(
		order_id: number,
		transaction: EntityManager,
	) {
		// const order = await OrderManager.getDetail([order_id], undefined, true, false, false, transaction).then(o => o[0])
		
		// const details = await Promise.all(order.orderDetails.map(od => {
		// 	return this.feedbackDetail(order.user_id, od.id, od.orderRequest?.ref_id, transaction)
		// }))
		// return this.FeedbackRepository.getFeedbackByOrderId(order_id, transaction)
	}

	async getByOrderDetailId(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.FeedbackRepository.getDetailByOrderDetailId(user_id, order_detail_id, transaction)
	}

	async getByTypeAndRefId(
		type: FEEDBACKS,
		ref_id: number,
		transaction: EntityManager,
	) {
		return this.FeedbackRepository.getAnswerBy(ref_id, type, transaction)
	}

	get getRaw() {
		return this.FeedbackRepository.listUserFeedback
	}

	async feedbackList(
		user_id: number,
		offset: number,
		limit: number,
		is_history: boolean,
		transaction: EntityManager,
	) {
		const orders = is_history ? await this.FeedbackRepository.listUserFeedback(
			user_id,
			{
				detailed: false,
				with_feedback: false,
			},
			{
				offset,
				limit,
				history_only: true,
			},
			transaction,
		) : await this.FeedbackRepository.listUserFeedback(
			user_id,
			{
				detailed: false,
				with_feedback: false,
			},
			{
				offset,
				limit,
				empty_feedback: true,
			},
			transaction,
		)

		return orders.reduce(async (promise, order) => {
			return promise.then(async init => {
				const stylesheets = order.details
					.filter(detail => detail.type === ORDER_DETAILS.STYLESHEET)
					.reduce((i, detail) => {
						return i.concat(...detail.variants.map(variant => {
							return {
								variant_id: variant.id,
								order_detail_id: detail.id,
								order_created_at: order.created_at,
								feedback_expired_at: !!detail.shipment?.delivered?.id ?
									TimeHelper.moment(detail.shipment.delivered.created_at).add(7, 'days').toDate() :
									!!detail.shipment?.confirmed_at ? detail.shipment.confirmed_at : new Date(),
								brand: variant.brand.title,
								product: variant.product.title,
								size: variant.size.title,
								colors: variant.colors.map(color => color.title),
								price: variant.price,
								is_refundable: true,
								is_exchangeable: true,
								image: CommonHelper.allowKey(variant.asset, 'id', 'url', 'metadata.version'),
							}
						}))
					}, [])

				const inventories = order.details
					.filter(detail => detail.type === ORDER_DETAILS.INVENTORY)
					.reduce((i, detail) => {
						return i.concat({
							variant_id: detail.variants[0].id,
							order_detail_id: detail.id,
							order_created_at: order.created_at,
							feedback_expired_at: !!detail.shipment?.delivered?.id ?
								TimeHelper.moment(detail.shipment.delivered.created_at).add(7, 'days').toDate() :
								!!detail.shipment?.confirmed_at ? detail.shipment.confirmed_at : new Date(),
							brand: detail.variants[0].brand.title,
							product: detail.variants[0].product.title,
							size: detail.variants[0].size.title,
							colors: detail.variants[0].colors.map(color => color.title),
							price: detail.price,
							is_refundable: detail.variants[0].quantity > 0 || detail.variants[0].have_consignment_stock,
							is_exchangeable: detail.variants[0].quantity > 0 || detail.variants[0].have_consignment_stock,
							image: CommonHelper.allowKey(detail.variants[0].asset, 'id', 'url', 'metadata.version'),
						})
					}, [])

				return [
					...init,
					...stylesheets,
					...inventories,
				]
			})
		}, Promise.resolve([] as Array<{
			order_created_at: Date,
			feedback_expired_at: Date,
			variant_id: number,
			order_detail_id: number,
			brand: string,
			product: string,
			size: string,
			colors: string[],
			is_refundable: boolean,
			is_exchangeable: boolean,
			image: any,
		}>))
	}

	async feedbackDetail(
		user_id: number,
		order_detail_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		return this.FeedbackRepository.listUserFeedback(
			user_id,
			{
				detailed: true,
				with_feedback: true,
			}, {
				order_detail_id,
			},
			transaction,
		).then(async order => {
				// let discountRefundable: any
				const refundValue = await OrderManager.utilities.refundProcessor(user_id, order.id, null, [], false, transaction)
				const discountRefundable = await PromoManager.checkRefundable(order_detail_id, transaction)
				const discountExchangable = await PromoManager.checkExchangeable(order_detail_id, transaction)

				const stylesheetVariants = order.details
				.filter(detail => detail.type === ORDER_DETAILS.STYLESHEET)
				.reduce((i, detail) => {
						return i.concat(...detail.variants.map(variant => {
							return {
								id: variant.id,
								brand: variant.brand.title,
								product: variant.product.title,
								category: {
									id: variant.category.id,
									title: variant.category.title,
								},
								size: variant.size.title,
								colors: variant.colors.map(color => CommonHelper.allowKey(color, 'id', 'title', 'hex', 'image')),
								image: CommonHelper.allowKey(variant.asset, 'id', 'url', 'metadata.version'),
								price: variant.price,
								retail_price: variant.retail_price,
								is_refundable: discountRefundable,
								message: 'Barang tidak bisa dikembalikan',
								is_exchangeable: true,
								feedback_expired_at: !!detail.shipment?.delivered?.id ?
									TimeHelper.moment(detail.shipment.delivered.created_at).add(7, 'days').toDate() :
									!!detail.shipment?.confirmed_at ? detail.shipment.confirmed_at : new Date(),
								refund_value: refundValue[variant.id],
								feedback: !!variant.feedback ? {
									id: variant.feedback.id,
									created_at: variant.feedback.created_at,
									questions: variant.feedback.answers.map(answer => {
										return {
											// ...CommonHelper.allowKey(answer.question, 'id', 'title'),
											...answer.question,
											answer: answer.answer,
										}
									}),
								} : {},
							}
						}))
					}, [])

				const variantInventories = order.details
					.filter(detail => detail.type === ORDER_DETAILS.INVENTORY)
					.reduce((i, detail) => {
						return i.concat({
							id: detail.variants[0].id,
							brand: detail.variants[0].brand.title,
							product: detail.variants[0].product.title,
							category: {
								id: detail.variants[0].category.id,
								title: detail.variants[0].category.title,
							},
							size: detail.variants[0].size.title,
							colors: detail.variants[0].colors.map(color => CommonHelper.allowKey(color, 'id', 'title', 'hex', 'image')),
							image: CommonHelper.allowKey(detail.variants[0].asset, 'id', 'url', 'metadata.version'),
							price: detail.variants[0].price,
							retail_price: detail.variants[0].retail_price,
							is_refundable: !!discountRefundable ? true : (detail.variants[0].quantity > 0 || detail.variants[0].have_consignment_stock),
							message: !discountRefundable ? 'Produk promosi tidak bisa dikembalikan' : 'Produk ini tidak bisa dikembalikan',
							is_exchangeable: !!discountExchangable && (detail.variants[0].quantity > 0 || detail.variants[0].have_consignment_stock),
							feedback_expired_at: !!detail.shipment?.delivered?.id ?
								TimeHelper.moment(detail.shipment.delivered.created_at).add(7, 'days').toDate() :
								!!detail.shipment?.confirmed_at ? detail.shipment.confirmed_at : new Date(),
							refund_value: refundValue[detail.variants[0].id],
							feedback: !!detail.variants[0].feedback ? {
								id: detail.variants[0].feedback.id,
								created_at: detail.variants[0].feedback.created_at,
								questions: detail.variants[0].feedback.answers.map(answer => {
									return {
										...CommonHelper.allowKey(answer.question, 'id', 'title'),
										answer: answer.answer,
									}
								}),
							} : {},
						})
					}, [])

				return {
					order_id: order.id,
					order_created_at: order.created_at,
					number: order.number,
					variant: {
						...stylesheetVariants.find(sv => sv.id === variant_id),
						...variantInventories.find(sv => sv.id === variant_id),
					},
				}
			})
	}
	// ============================= DELETE =============================

	async resetFeedback(
		order_number: string,
		transaction: EntityManager,
	) {
		const order = await OrderManager.get(order_number, transaction)

		return order.orderDetails.filter(f => f.is_exchange === false).map(od => {
			return this.FeedbackRepository.resetFeedback(order.user_id, od.id, transaction)
		})
	}

	// ============================= METHODS ============================
	async getRefundValue(
		details: Array<OrderDetailInterface & {
			variants: Array<VariantInterface & {
				inventory: InventoryInterface,
				quantity: number,
			}>,
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			facade_shipment: ShipmentInterface,
		}>,
		transaction: EntityManager,
	) {
		return details.reduce(async (promise, detail) => {
			return promise.then(async init => {
				let subtotal = 0
				let discount = 0
				let totalSheetItemValues = 0 // only for stylesheet
				let paidValue = 0

				if (detail.type === ORDER_DETAILS.STYLESHEET) {
					subtotal = detail.price === 0
						? await OrderManager.detail.getOrderDetailFromVoucherRedeem(detail.id, transaction).then(orderDetail => orderDetail.price).catch(() => detail.price)
						: detail.price
					discount = detail.orderDetailCampaigns.reduce((i, campaign) => campaign.is_refundable ? i + campaign.discount : i, 0)
						+ detail.orderDetailCoupons.reduce((i, coupon) => i + coupon.discount, 0)
					totalSheetItemValues = detail.variants.reduce((i, variant) => i + variant.inventory.price, 0)
					paidValue = subtotal - discount

					return {
						...init,
						...detail.variants.reduce((i, variant) => {
							return {
								...i,
								[variant.id]: Math.ceil(variant.inventory.price * totalSheetItemValues / paidValue),
							}
						}, {}),
					}
				} else if (detail.type === ORDER_DETAILS.INVENTORY) {
					subtotal = details.filter(d => d.type === ORDER_DETAILS.INVENTORY).reduce((i, d) => {
						if (d.variants[0].quantity > 0 || d.variants[0].have_consignment_stock) {
							return i + d.price
						} else {
							return i
						}
					}, 0)
					discount = detail.orderDetailCampaigns.reduce((i, campaign) => campaign.is_refundable ? i + campaign.discount : i, 0)
						+ detail.orderDetailCoupons.reduce((i, coupon) => i + coupon.discount, 0)
					paidValue = subtotal - discount

					return {
						...init,
						...details.filter(d => d.type === ORDER_DETAILS.INVENTORY).reduce((i, d) => {
							if (d.variants[0].quantity > 0 || d.variants[0].have_consignment_stock) {
								return {
									...i,
									[d.variants[0].id]: Math.ceil(d.price / subtotal * paidValue),
								}
							} else {
								return i
							}
						}, {}),
					}
				}
			})
		}, Promise.resolve({} as {
			[variant_id: number]: number,
		}))
	}

	// ============================ PRIVATES ============================
	// NOTE: DEPRECATED!!
	// private isRequestingRefund(feedback: FeedbackInterface & {
	// 	answers: Array<FeedbackAnswerInterface & {
	// 		type: QUESTIONS,
	// 	}>;
	// }): boolean {
	// 	return feedback.type === FEEDBACKS.VARIANT && feedback.answers.findIndex(answer => {
	// 		// tslint:disable-next-line: no-string-literal
	// 		return answer.question_id === DEFAULTS.EXCHANGE_QUESTION_ID && answer.answer[answer.type] === 'Return'
	// 	}) > -1
	// }

}

export default new FeedbackManager()

