import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
  StorefrontRepository,
} from 'app/repositories'
import { Parameter } from 'types/common'
import { StorefrontInterface } from 'energie/app/interfaces'

import csvjson from 'csvjson'
import { uniqBy } from 'lodash'
import { ERRORS, ERROR_CODES } from 'app/models/error'
class StorefrontManager extends ManagerModel {

  static __displayName = 'StorefrontManager'

  protected StorefrontRepository: StorefrontRepository

  async initialize(connection: Connection) {
		return super.initialize(connection, [
			StorefrontRepository,
		])
  }

	// ============================= INSERT =============================

	@ManagerModel.bound
	async insert(
		storefront: Parameter<StorefrontInterface>,
		transaction: EntityManager,
	) {
		return this.StorefrontRepository.insert(storefront, transaction)
	}

	get createSchedule() {
		return this.StorefrontRepository.insertSchedule
	}

	async createVariant(
		variant_id: number,
		storefront_id: number,
		transaction: EntityManager,
	) {
		const isDuplicate = await this.duplicateVariantChecker(storefront_id, variant_id, transaction)
			if (!isDuplicate) {
				return this.StorefrontRepository.insertVariant(variant_id, storefront_id, transaction)
			} else {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, `variant id ${variant_id} already added`)
			}
	}

	async insertVariantFromCSV(
		csv: string,
		storefront_id: number,
		transaction: EntityManager
	) {
		const data: Array<{
			variant_ids: number,
		}> = csvjson.toObject(csv)
		const dataFiltered = uniqBy(data, 'variant_ids')

		const added = []
		const alreadyAdded = []
		
		await Promise.all(dataFiltered.map( async variant => {
			const isDuplicate = await this.duplicateVariantChecker(storefront_id, variant.variant_ids, transaction)
				if (!isDuplicate) {
					await this.StorefrontRepository.insertVariant(variant.variant_ids, storefront_id, transaction)
					added.push(variant.variant_ids)
				} else {
					alreadyAdded.push(variant.variant_ids)
				}
			// return await this.StorefrontRepository.insertVariant(variant.variant_ids, storefront_id, transaction)
		}))
		return {
			variant_added: added,
			variant_already_in_promotion: alreadyAdded,
		}
	}

	// ============================= UPDATE =============================
	get update() {
		return this.StorefrontRepository.update
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		filter: {
			offset: number,
			limit: number,
		},
		query: {
			include_zero_variant: boolean,
			show_on_page: boolean,
			price: string,
			with_variant?: boolean,
		},
		transaction: EntityManager,
	) {
		return this.StorefrontRepository.filter( filter, query, transaction)
	}

	@ManagerModel.bound
	async get(
		filter: {
			slug?: string,
			id?: number,
		},
		include_zero_variant: boolean,
		with_variant: boolean,
		transaction: EntityManager,
	) {
		return this.StorefrontRepository.get(filter, include_zero_variant, with_variant, transaction)
	}

	getVariants(
		offset: number,
		limit: number,
		filter: {
			id: number,
		},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		transaction: EntityManager,
	) {
		return this.StorefrontRepository.getVariants(offset, limit, filter, sort_by, transaction)
	}

	// ============================= DELETE =============================

	get deleteVariant() {
		return this.StorefrontRepository.deleteVariant
	}

	get deleteSchedule() {
		return this.StorefrontRepository.deleteSchedule
	}

	// ============================= METHODS ============================

	@ManagerModel.bound
	async duplicateVariantChecker(
		storefront_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		const pv = await this.StorefrontRepository.getStorefrontVariant(storefront_id, variant_id, transaction)
		if (pv[0].length) {
			return true
		} else {
			return false
		}
	}
}

export default new StorefrontManager()

