import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
  PromotionRepository, VariantRepository, SchedulerRepository
} from 'app/repositories'

import {
	uniqBy,
} from 'lodash'
import { Parameter } from 'types/common'
import { check } from 'app/interfaces/guard'
import { PromotionInterface, PromotionVariantInterface, VariantInterface } from 'energie/app/interfaces'
import { COUPONS, SCHEDULER_STAT, SCHEDULER_TYPE } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'
// import DiscountManager from './discount'

import csvjson from 'csvjson'
import { isEmpty } from 'lodash'
class PromotionManager extends ManagerModel {

  static __displayName = 'PromotionManager'

  protected PromotionRepository: PromotionRepository
  protected VariantRepository: VariantRepository
  protected SchedulerRepository: SchedulerRepository

  async initialize(connection: Connection) {
		return super.initialize(connection, [
			PromotionRepository,
			VariantRepository,
			SchedulerRepository,
		])
  }

	// ============================= INSERT =============================
	@ManagerModel.bound
	async insert(
		promotion: Parameter<PromotionInterface>,
		transaction: EntityManager,
	) {
		const newPromotion = await this.PromotionRepository.insert(promotion, transaction)
		await this.SchedulerRepository.insert({
			promotion_id: newPromotion.id,
			title: newPromotion.title,
			status: SCHEDULER_STAT.RUN,
			function: 'updateDiscount',
			type: SCHEDULER_TYPE.ONCE,
			start_date: newPromotion.published_at,
			end_date: newPromotion.expired_at,
		}, transaction)

		return newPromotion
	}

	@ManagerModel.bound
	async createPromotionWithCSV(
		buffer: Buffer,
		changer_user_id: number,
		transaction: EntityManager,
	) {
		const file = buffer.toString('utf-8')

		const datas: Array<{
			title: string,
			published_at: string,
			expired_at: string,
			product_ids: string,
		}> = await csvjson.toObject(file, { delimiter: ',', quote: '"'})
			.reduce((promise, csv) => {
				return promise.then(async init => {
					if(check(csv, ['object', {
						title: 'string',
						published_at: 'string',
						expired_at: 'string',
					}])) {
						await this.insert({
							title: csv.title,
							published_at: csv.published_at || null,
							expired_at: csv.expired_at || null,
							creator_id: changer_user_id,
						}, transaction).then(async res => {
							const added = []
							const alreadyAdded = []
							const products = !isEmpty(csv.product_ids) ? csv.product_ids.split(',') : undefined

							if(!isEmpty(products)) {
								await Promise.all(products.map( async pId => {
									const variants = await this.VariantRepository.getByProductId(pId, transaction)
	
									if (variants[0].length) {
										await Promise.all(variants[0].map( async v => {
											const isDuplicate = await this.duplicateVariantChecker(res.id, v.id, transaction).catch(err => ({ message: 'duplicating variant err'}))
											if (!isDuplicate) {
												await this.PromotionRepository.insertVariant(v.id, res.id, true, COUPONS.PERCENT, csv.discount || 0, transaction).catch(err => ({ message: 'inserting variant failed ' + v.id}))
												added.push(v.id)
											} else {
												alreadyAdded.push(v.id)
											}
										}))	
									}
								}))
							}
						})
					} else {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Wrong CSV Format!!')
					}
				})
			}, Promise.resolve([]))

		this.log('Creating promotion ', datas)
		return {
			message: 'done',
		}

	}

	@ManagerModel.bound
	async insertVariantWithCSV(
		promotion_id: number,
		buffer: Buffer,
		transaction: EntityManager,
	) {
		const file = buffer.toString('utf-8')

		const data: Array<{
			product_id: number,
			discount: number,
		}> = csvjson.toObject(file, { delimiter: ','})
			
		const added = []
		const alreadyAdded = []
		await Promise.all(data.map(async ({ product_id, discount }) => {
			const variants = await this.VariantRepository.getByProductId(product_id, transaction)
			if (variants[0].length) {
				await Promise.all(variants[0].map( async v => {
					const isDuplicate = await this.duplicateVariantChecker(promotion_id, v.id, transaction)
					if (!isDuplicate) {
						await this.PromotionRepository.insertVariant(v.id, promotion_id, true, COUPONS.PERCENT, discount || 0, transaction)
						added.push(v.id)
					} else {
						alreadyAdded.push(v.id)
					}
				}))	
			}
		}))

		return {
			variant_added: added,
			variant_already_in_promotion: alreadyAdded,
		}
	}

	@ManagerModel.bound
	async insertProductFromCSV(
		promotion_id: number,
		csv: string,
		transaction: EntityManager
	) {
		const data: Array<{
			product_id: number,
		}> = csvjson.toObject(csv)
		const dataFiltered = uniqBy(data, 'product_id')
		const added = []
		const alreadyAdded = []
		await Promise.all(dataFiltered.map( async d => {
			const variants = await this.VariantRepository.getByProductId(d.product_id, transaction)
			if (variants[0].length) {
				await Promise.all(variants[0].map( async v => {
					const isDuplicate = await this.duplicateVariantChecker(promotion_id, v.id, transaction)
					if (!isDuplicate) {
						await this.PromotionRepository.insertVariant(v.id, promotion_id, true, COUPONS.PERCENT, 0, transaction)
						added.push(v.id)
					} else {
						alreadyAdded.push(v.id)
					}
				}))	
			}
		}))

		return {
			variant_added: added,
			variant_already_in_promotion: alreadyAdded,
		}
	}

	@ManagerModel.bound
	async insertProduct(
		promotion_id: number,
		product_ids: number[],
		transaction: EntityManager,
	) {
		const added = []
		const alreadyAdded = []
		await Promise.all(product_ids.map( async product_id => {
			const variants = await this.VariantRepository.getByProductId(product_id, transaction)
			if (variants[0].length) {
				await Promise.all(variants[0].map( async v => {
					const isDuplicate = await this.duplicateVariantChecker(promotion_id, v.id, transaction)
					if (!isDuplicate) {
						await this.PromotionRepository.insertVariant(v.id, promotion_id, true, COUPONS.PERCENT, 0, transaction)
						added.push(v.id)
					} else {
						alreadyAdded.push(v.id)
					}
				}))
			}
		}))
		return {
			variant_added: added,
			variant_already_in_promotion: alreadyAdded,
		}
	}

	// ============================= UPDATE =============================
	// get update() {
	// 	return this.PromotionRepository.update
	// }

	@ManagerModel.bound
	async update(
		id: number,
		promotion: any,
		transaction: EntityManager,
	) {
		const updated = await this.PromotionRepository.update(id, promotion, transaction)
		await this.SchedulerRepository.update(id, {
			promotion_id: undefined,
			function: undefined,
			status: undefined,
			type: undefined,
			title: promotion.title,
			start_date: promotion.published_at,
			end_date: promotion.expired_at
		}, transaction)
		
		await this.updateDiscountManual(transaction)

		return updated
	}

	@ManagerModel.bound
	async updateVariant(
		changer_user_id: number,
		promotionVariants: any[],
		transaction: EntityManager,
	) {
		await Promise.all(promotionVariants.map( async pv => {
			await this.PromotionRepository.updateVariant(pv.id, pv, transaction)
			.then(async () => {
				if (pv.is_active === false) {
					const pv2 = await this.PromotionRepository.getDetailForPriceUpdate(pv.id, transaction)
					await this.expireDiscount(changer_user_id, pv2.variant, transaction)
				}
				if (!!pv.amount || !!pv.type || (pv.is_active === true || pv.is_active === false)) {
					const pv2 = await this.PromotionRepository.getDetailForPriceUpdate(pv.id, transaction)
					// await this.updateDiscountManual(transaction)
					await this.updateDiscountById(pv2.variant_id, transaction)
				}
			})
			.catch(err => {
				return err
			})
		}))

		return {
			variant_updated: promotionVariants.map(pv => pv.id)
		}
	}

	@ManagerModel.bound
	async updateAll(
		promotion_id: number,
		is_active: boolean,
		type: COUPONS,
		amount: number,
		transaction: EntityManager,
	) {
		const updated = await this.PromotionRepository.updateAll(promotion_id, is_active, type, amount, transaction)
	 	await this.updateDiscountManual(transaction)
		return updated
	}

	@ManagerModel.bound
	async updatePrice(
		changer_user_id: number,
		promotion_variant: PromotionVariantInterface,
		transaction: EntityManager
	) {
		if (promotion_variant.type === 'TOTAL') {
			return this.VariantRepository.update(changer_user_id, promotion_variant.variant_id, {
				price: promotion_variant.amount,
			}, undefined, undefined, undefined, transaction)
		} else if (promotion_variant.type === 'PERCENT') {
			if (promotion_variant.amount > 100) {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Percentage Discount More Than 100!')
			}
			const variant = await this.VariantRepository.get(promotion_variant.variant_id, {}, transaction)

			const amount = Math.floor(variant.retail_price * (promotion_variant.amount / 100))

			return this.VariantRepository.update(changer_user_id, promotion_variant.variant_id, {
				price: variant.retail_price - amount,
			}, undefined, undefined, undefined, transaction)
		} else {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Please Provide Discount Amount or Discount Percentage')
		}
	}

	@ManagerModel.bound
	async updatePrice2(
		transaction: EntityManager
	) {
		const expiredDiscount = await this.filterFlat({ is_active: false }, transaction)
		const activeDiscount = await this.filterFlat({ is_active: true }, transaction)
		let queryExpired = ''
		let queryActive = ''
		let expDisc = []
		let actDisc = []

		if (expiredDiscount.data.length) {
			expiredDiscount.data.map(p => {
				p.promotionVariant.map(pv => {
					expDisc = expDisc.concat(pv)
				})
			})
		}
		expDisc = uniqBy(expDisc, 'variant_id')
		if (expDisc.length) {
			expDisc.map(pv => {
				queryExpired += `(${pv.variant_id}, ${pv.variant.retail_price}, ${pv.variant.product_id}, ${pv.variant.size_id}, ${pv.variant.is_published}, ${pv.variant.is_available}, ${pv.variant.have_consignment_stock}, ${pv.variant.limited_stock}), `
			})
		}
		if (activeDiscount.data.length) {
			activeDiscount.data.map(p => {
				p.promotionVariant.map(pv => {
					actDisc = actDisc.concat(pv)
				})
			})
		}
		actDisc = uniqBy(actDisc, 'variant_id')
		if (actDisc.length) {
			actDisc.map(pv => {
				let price = 0
				if (pv.type === 'TOTAL') {
					price = pv.amount
				} else if (pv.type === 'PERCENT') {
					price = pv.variant.retail_price - (Math.floor(pv.variant.retail_price * (pv.amount / 100)))
				}
				queryActive += `(${pv.variant_id}, ${price}, ${pv.variant.product_id}, ${pv.variant.size_id}, ${pv.variant.is_published}, ${pv.variant.is_available}, ${pv.variant.have_consignment_stock}, ${pv.variant.limited_stock}), `
			})
		}
		queryExpired = queryExpired.substring(0, queryExpired.length - 2)
		queryActive = queryActive.substring(0, queryActive.length - 2)
		return this.VariantRepository.updateDiscount(queryExpired, transaction)
		.then(async () => {
			return this.VariantRepository.updateDiscount(queryActive, transaction)
		})
	}

	@ManagerModel.bound
	async updateDiscountManual(
		transaction: EntityManager
	) {
		return this.VariantRepository.updateDiscountManual(transaction)
	}

	@ManagerModel.bound
	async updateDiscountById(
		variant_id: number,
		transaction: EntityManager
	) {
		return this.VariantRepository.updateDiscountById(variant_id, transaction)
	}

	@ManagerModel.bound
	async expireDiscount(
		changer_user_id: number,
		variant: VariantInterface,
		transaction: EntityManager,
	) {
		return this.VariantRepository.update(changer_user_id, variant.id, {
			price: variant.retail_price,
		}, undefined, undefined, undefined, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			is_active?: boolean,
			published?: boolean,
			expired?: boolean,
		},
		transaction: EntityManager,
	) {
		return this.PromotionRepository.filter(offset, limit, search, filter, transaction)
	}

	get variantForList() {
		return this.PromotionRepository.getVariantForList
	}

	@ManagerModel.bound
	async filterFlat(
		filter: {
			is_active?: boolean,
		},
		transaction: EntityManager,
	) {
		return this.PromotionRepository.filterFlat(filter, transaction)
	}

	@ManagerModel.bound
	async filterVariant(
		promotion_id: number,
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		transaction: EntityManager,
	) {
		return this.PromotionRepository.filterVariant(promotion_id, offset, limit, search, transaction)
	}

	@ManagerModel.bound
	async get(
		id: number,
		transaction: EntityManager,
	) {
		return this.PromotionRepository.get(id, transaction)
	}

	@ManagerModel.bound
	async duplicateVariantChecker(
		promotion_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		const pv = await this.PromotionRepository.getPromotionVariant(promotion_id, variant_id, transaction)
		if (pv[0].length) {
			return true
		} else {
			return false
		}
	}

	// ============================= DELETE =============================

	@ManagerModel.bound
	async deleteVariant(
		changer_user_id: number,
		promotion_id: number,
		product_ids: number[],
		transaction: EntityManager
	) {
		await Promise.all(product_ids.map(async product_id => {
			const variants = await this.VariantRepository.getByProductId(product_id, transaction)
			await this.PromotionRepository.deleteVariant(promotion_id, variants[0].map(v => v.id), transaction)
			.then(variants[0].map(async v => {
				return this.expireDiscount(changer_user_id, v, transaction)
			}))
			.then(async () => {
				return this.updateDiscountManual(transaction)
			})
			.catch(err => {
				return err
			})
		}))
		return {
			product_deleted: product_ids,
		}
	}

	@ManagerModel.bound
	async deleteAll(
		changer_user_id: number,
		promotion_id: number,
		transaction: EntityManager
	) {
		const variants = await this.VariantRepository.getByPromotionId(promotion_id, transaction)
		await this.PromotionRepository.deleteVariant(promotion_id, variants[0].map(v => v.id), transaction)
		.then(variants[0].map(async v => {
			return this.expireDiscount(changer_user_id, v, transaction)
		}))
		.then(async () => {
			return this.updateDiscountManual(transaction)
		})
		.catch(err => {
			return err
		})
		return `All product with promotion_id ${promotion_id} has been deleted`
	}

	// ============================= METHODS ============================
}

export default new PromotionManager()

