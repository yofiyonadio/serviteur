import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
	MigrationRepository,
} from 'app/repositories'

import { SendGridHandler, XenditHandler } from '../../handlers'
import { AreaManualInterface, ExchangeDetailInterface, ExchangeInterface, FeedbackAnswerInterface, OrderInterface, ProductInterface, ShipmentExchangeDetailInterface, StyleboardInterface, UserAnswerInterface, UserProfileInterface, VariantAssetInterface, VariantInterface } from 'energie/app/interfaces'
import { CommonHelper, TimeHelper } from 'utils/helpers'

import csvjson from 'csvjson'
// import * as fs from 'fs'
import { ORDER_DETAILS, PAYMENT_AGENTS, PAYMENT_METHOD, STYLEBOARD_STATUSES, STYLESHEET_STATUSES } from 'energie/utils/constants/enum'
import { check } from 'app/interfaces/guard'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import NotificationManager from '../notification'
import OrderManager from '../order'
import PromotionManager from '../promotion'
// import UserManager from '../user'
// import ProductManager from '../product'

import fs from 'fs'
import { isEmpty } from 'lodash'
class MigrationManager extends ManagerModel {

	static __displayName = 'MigrationManager'

	protected MigrationRepository: MigrationRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			MigrationRepository,
		])
	}

	// async migrateUserPointTransaction(
	// 	transaction: EntityManager,
	// ) {
	// 	return UserManager.point.migrateUserPointTransaction(transaction)
	// }

	async categoryMigration(transaction: EntityManager) {
		const data: { [product_id: number]: number } = await csvjson.toObject(fs.readFileSync('data/inventory_data.csv', 'utf8'), { delimiter: ',' })
			.reduce(async (promise, file: {
				old_category: string,
				old_type: string,
				new_category: string,
				new_type: string,
				note: string,
				product_id: string,
			}) => {
				return promise.then(async (init: { [product_id: number]: number }) => {
					if (file.new_category === '' || file.new_type === '') {
						return init
					}

					if (!isEmpty(init[file.product_id])) {
						return init
					}

					const category = await this.MigrationRepository.getCategoryByTitle(file.new_type.toLocaleLowerCase(), transaction).catch(async err => {
						SendGridHandler.sendCustom('rheza@helloyuna.io', 'Migration Category', JSON.stringify({
							file, init,
						}))
						throw err
					})

					return {
						...init,
						[file.product_id]: category.id,
					}
				})
			}, Promise.resolve({} as { [product_id: number]: number }))


		const product_ids = Object.keys(data).map(key => parseInt(key, 10))

		const newProducts = await this.MigrationRepository.product(product_ids, undefined, transaction)
			.then(products => {
				return products.map(product => {
					return {
						...product,
						category_id: data[product.id],
					}
				})
			})

		await this.MigrationRepository.insertOrUpdate('app.product', newProducts, undefined, transaction)

		return 'done'
	}

	async softDeleteProduct(transaction: EntityManager) {
		const productOnly = await this.MigrationRepository.product((await csvjson.toObject(fs.readFileSync('data/inventory_duplicate.csv', 'utf8'), { delimiter: ',' })).map(data => data.product_id), {}, transaction)
			.then(products => {
				return products.map(product => {
					return {
						...product,
						deleted_at: TimeHelper.moment(),
					}
				})
			})

		await this.MigrationRepository.insertOrUpdate('app.product', productOnly, undefined, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('haidar@helloyuna.io', 'soft Del Product [ERROR!!!]', JSON.stringify(productOnly))
				throw err
			})

		await SendGridHandler.sendCustom('haidar@helloyuna.io', 'soft Del Product', JSON.stringify(productOnly))

		return 'done'
	}

	async prestyleToStylecard(transaction?: EntityManager) {
		const prestyled = await this.MigrationRepository.getAllPrestyled(transaction)

		// update stylecard
		await this.MigrationRepository.batchUpdateStylecard(prestyled, transaction)

		return 'done'
	}

	async migrateFeedbackAnswer(transaction?: EntityManager) {
		const recentFeedbackAnswers = await this.MigrationRepository.getThreeStarFeedback(transaction)
		const newFeedbackAnswers = recentFeedbackAnswers.reduce((init, answer) => {
			const scale = (question_id: number, currentScale: number) => {
				if (question_id === 64) {
					return currentScale + 1
				} else if (question_id === 68) {
					if (currentScale === 0) {
						return 0
					} else if (currentScale === 1) {
						return 2
					} else if (currentScale === 2) {
						return 4
					} else {
						return currentScale
					}
				}

				return currentScale
			}

			return init.concat({
				...answer,
				answer: {
					SCALE: scale(answer.question_id, answer.answer.SCALE),
				},
			})
		}, [] as FeedbackAnswerInterface[])

		this.MigrationRepository.insertOrUpdate('app.feedback_answer', newFeedbackAnswers, undefined, transaction)

		return 'test'
	}

	async migrateAvailableStockToUnavailable(transaction: EntityManager) {
		const variants: VariantInterface[] = (await this.MigrationRepository.getUnavailableInventory(transaction)).map(variant => {
			return {
				...variant,
				is_available: false,
			}
		})

		await this.MigrationRepository.insertOrUpdate('app.variant', variants, undefined, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate available stock to unavailable [ERROR!!!]', JSON.stringify({ err }))
				throw err
			})

		await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate available stock to unavailable', JSON.stringify({ code: 200 }))

		return 'done'
	}

	async migrateVariantAsset(transaction: EntityManager) {
		const var_assets: VariantAssetInterface[] = await csvjson.toObject(fs.readFileSync('data/variant_asset.csv', 'utf8'), { delimiter: ',' })

		await this.MigrationRepository.insertOrUpdate('app.variant_asset', var_assets, undefined, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate variant assets [ERROR!!!]', JSON.stringify({ err }))
				throw err
			})

		await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate migrate variant assets', JSON.stringify({ code: 200 }))

		return 'done'
	}

	async migrateVariantAssets(transaction: EntityManager) {
		const var_assets = await csvjson.toObject(fs.readFileSync('data/backup_variant_asset.csv', 'utf8'), { delimiter: ',' })
		let queryString = ''
		var_assets.map(var_asset => {
			queryString += `(${var_asset.image_id}, ${var_asset.variant_id}, ${var_asset.order}, ${var_asset.index}, '', 'IMAGE'), `
		})
		queryString = queryString.substring(0, queryString.length - 2)
		await this.MigrationRepository.migrateVariantAsset(queryString, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('riva@helloyuna.io', 'migrate variant assets [ERROR!!!]', JSON.stringify({ err }))
				throw err
			})

		await SendGridHandler.sendCustom('riva@helloyuna.io', 'migrate migrate variant assets', JSON.stringify({ code: 200 }))

		return 'done'
	}

	async migrateOldCategory(transaction: EntityManager) {
		const products = await this.MigrationRepository.getOldCategoryProduct(transaction)
		// const products2 = products.slice(0, 3)
		// const ids = products2.map(p => {
		// 	return {
		// 		product_id: p.id,
		// 		category_id: p.category_id
		// 	}
		// })
		products.map(async p => {
			let newCategoryId = 1
			switch (p.category_id) {
				case 114:
					newCategoryId = 211
					break
				case 115:
					newCategoryId = 210
					break
				case 116:
					newCategoryId = 258
					break
				case 117:
					newCategoryId = 257
					break
				case 118:
					newCategoryId = 209
					break
				case 119:
					newCategoryId = 202
					break
				case 120:
					newCategoryId = 203
					break
				case 121:
					newCategoryId = 132
					break
				case 122:
					newCategoryId = 137
					break
				case 123:
					newCategoryId = 213
					break
				case 124:
					newCategoryId = 130
					break
				case 126:
					newCategoryId = 138
					break
				case 127:
					newCategoryId = 135
					break
				default:
					newCategoryId = 1
			}
			// this.log(newCategoryId)
			await this.MigrationRepository.batchUpdateProductCategory(p.id, newCategoryId, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate product with old category [ERROR!!!]', JSON.stringify({err}))
				throw err
			})

		})
		await SendGridHandler.sendCustom('riva@helloyuna.io', 'migrate product with old category', JSON.stringify({code: 200}))

		return 'done'
	}

	async migrateStylingStatusStyleboard(transaction: EntityManager) {
		const styleboards: StyleboardInterface[] = await (await this.MigrationRepository.getStylingStyleboard(transaction)).map(styleboard => {
			return {
				...styleboard,
				status: STYLEBOARD_STATUSES.RESOLVED,
				expired_at: TimeHelper.moment().format() as any,
			}
		})

		await this.MigrationRepository.insertOrUpdate('app.styleboard', styleboards, undefined, transaction)
			.catch(async err => {
				await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate styling status in styleboard [ERROR!!!]', JSON.stringify({ err }))
				throw err
			})

		await SendGridHandler.sendCustom('haidar@helloyuna.io', 'migrate styling status in styleboard', JSON.stringify({ code: 200 }))

		return 'done'
	}

	async batchUpdatePrices(buffer: Buffer, transaction: EntityManager) {
		const file = buffer.toString('utf-8')

		const backupData: Array<{
			product_id: number,
			variant: Array<{
				id: number,
				price: number,
				retail_price: number,
			}>,
		}> = []

		const datas: Array<{
			product_id: number
			price: number,
			retail_price: number,
		}> = await csvjson.toObject(file, { delimiter: ',' })
			.reduce((promise, csv: {
				variant_id: string,
				price: string,
				retail_price: string,
			}) => {
				return promise.then(async init => {
					if (check(csv, ['object', {
						variant_id: 'string',
						price: 'string',
						retail_price: 'string',
					}])) {
						const variant = await this.MigrationRepository.variants([parseInt(csv.variant_id, 10)], transaction).then(variants => variants[0]).catch(() => { throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, `Problem with variant id ${ csv.variant_id }`) })
						const product = await this.MigrationRepository.product([variant.product_id], { with_variant: true }, transaction).then(products => products[0])

						backupData.push({
							product_id: product.id,
							variant: product.variants.map(v => {
								return {
									id: v.id,
									price: v.price,
									retail_price: v.retail_price,
								}
							}),
						})

						return [...init, {
							product_id: product.id,
							price: parseInt(csv.price, 10),
							retail_price: parseInt(csv.retail_price, 10),
						}]
					} else {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Wrong CSV Format!!')
					}
				})
			}, Promise.resolve([]))

		await SendGridHandler.sendCustom('rheza@helloyuna.io', 'Backup Data Price', JSON.stringify(backupData))

		await datas.reduce((promise, data) => {
			return promise.then(() => {
				return this.MigrationRepository.batchUpdateVariantPrices(data.product_id, data.price, data.retail_price, transaction)
			})
		}, Promise.resolve(null))

		return {
			message: 'done',
		}
	}

	async updateOngkir(buffer: Buffer, transaction: EntityManager) {
		const file = buffer.toString('utf-8')

		const recentOngkir = await this.MigrationRepository.getRecentOngkir(transaction)
		const newOngkir: { [city_id: number]: number } = csvjson.toObject(file, { delimiter: ',' })
			.reduce((i, data: { city_id: string, avg_price: string }) => {
				const cityId = parseInt(data.city_id, 10)
				const price = parseInt(data.avg_price, 10)

				return {
					...i,
					[cityId]: price,
				}
			}, {} as { [city_id: number]: number })

		const latestOngkir: AreaManualInterface[] = recentOngkir.map(ongkir => {
			return {
				id: ongkir.id,
				created_at: ongkir.created_at,
				updated_at: new Date(),
				location_id: ongkir.location_id,
				area: ongkir.area,
				price: newOngkir[ongkir.location.city_id],
				min_day: ongkir.min_day,
				max_day: ongkir.max_day,
			}
		})

		await this.MigrationRepository.insertOrUpdate('area.manual', latestOngkir, undefined, transaction)
	}

	async refundProcessor(
		order_id: number,
		transaction: EntityManager,
	) {
		// get order details
		const details = await this.MigrationRepository.getForCountRefund(order_id, transaction)

		// count discount
		const discount = details.reduce((sum, detail) => {
			return sum + detail.orderDetailCampaigns.reduce((i, campaign) => !campaign.is_refundable ? i + campaign.discount : i, 0)
				+ detail.orderDetailCoupons.reduce((i, coupon) => i + coupon.discount, 0)
		}, 0)

		// get variant value
		const variant = await details.reduce(async (promise, detail) => {
			return promise.then(async init => {
				let subtotal = 0
				let totalSheetItemValues = 0 // only for stylesheet
				let realRetailPrice = 0

				if (detail.type === ORDER_DETAILS.STYLESHEET) {
					subtotal = detail.price === 0
						? await OrderManager.detail.getOrderDetailFromVoucherRedeem(detail.id, transaction).then(orderDetail => orderDetail.price).catch(() => detail.price)
						: detail.price
					totalSheetItemValues = detail.variants.reduce((i, v) => i + v.inventory.price, 0)
					realRetailPrice = subtotal - discount

					return {
						...init,
						...detail.variants.reduce((i, v) => {
							return {
								...i,
								[v.id]: Math.ceil(v.inventory.price / totalSheetItemValues * realRetailPrice),
							}
						}, {}),
					}
				} else if (detail.type === ORDER_DETAILS.INVENTORY) {
					subtotal = details.filter(d => d.type === ORDER_DETAILS.INVENTORY).reduce((i, d) => {
						return i + d.price
					}, 0)
					realRetailPrice = subtotal - discount

					return {
						...init,
						...details.reduce((i, d) => {
							return {
								...i,
								[d.variants[0].id]: Math.ceil(d.price / subtotal * realRetailPrice),
							}
						}, {}),
					}
				}
			})
		}, Promise.resolve({} as {
			[id: number]: number,
		}))

		return variant
	}

	async migrateStylist(buffer: Buffer, transaction: EntityManager) {
		//
		// ─── HANDOVER STYLIST WHEN SOMEONE ENTER OR RESIGN ───────────────
		//

		// handle uploaded file
		// const file = buffer.toString('utf-8')

		// parse to number[]
		const ids: number[] = await this.MigrationRepository.getStylistClient(1086, transaction).then(us => us.map(u => u.id))
		// const ids: number[] = csvjson.toObject(file, {delimiter: ','})
		// 	.reduce((i, data: {id: string}) => {
		// 		if (check(data, ['object', {
		// 			id: 'string',
		// 		}])) {
		// 			return i.concat(parseInt(data.id, 10))
		// 		} else {
		// 			return i
		// 		}
		// 	}, [] as number[])

		// stylist_ids
		const stylist_ids = [1092, 3095, 43843, 44021]

		// get users
		const users = await this.MigrationRepository.users(ids, transaction)

		// get stylists
		const stylists = await this.MigrationRepository.users(stylist_ids, transaction)

		// map user profiles with new stylist
		const userProfiles: UserProfileInterface[] = users.map((user, index) => {
			this.log(`sending email.... to ${user.profile.first_name}`)
			NotificationManager.email.sendHandoverStylistMail(
				user.id,
				user.email,
				user.profile.first_name,
				stylists[index % stylist_ids.length].profile.stage_name,
				user.stylist.profile.stage_name,
				stylists[index % stylist_ids.length].image.url,
			)

			return {
				...user.profile,
				stylist_id: stylists[index % stylist_ids.length].id,
			}
		})

		// update stylist user (1092, 3095, 43843, 44021)
		await this.MigrationRepository.updateUserProfile(userProfiles, transaction)

		return 'donee'
	}

	async upsertPaymentChannel(transaction: EntityManager) {
		const xenditChannels = await XenditHandler.getPaymentMethod()

		const channels = xenditChannels.map((channel, index) => {
			return {
				id: (index + 1),
				agent: PAYMENT_AGENTS.XENDIT,
				...channel,
			}
		}).concat({
			id: xenditChannels.length + 1,
			agent: PAYMENT_AGENTS.INDODANA,
			method: PAYMENT_METHOD.PAYLATER,
			code: 'PAYLATER',
			name: 'PAYLATER indodana',
			is_active: true,
		})

		await this.MigrationRepository.insertOrUpdate('other.payment_channel', channels, undefined, transaction)

		return 'donee'
	}

	async migrateExchange(transaction: EntityManager) {
		const exchanges: ExchangeInterface[] = []
		const exchangeDetails: ExchangeDetailInterface[] = []
		const shipmentExchangeDetail: ShipmentExchangeDetailInterface[] = []

		await this.MigrationRepository.getExchange(transaction)
			.then(exes => {
				return exes.forEach(exchange => {
					exchanges.push({
						id: exchange.id,
						created_at: exchange.created_at,
						deleted_at: exchange.deleted_at,
						status: exchange.status,
						user_id: exchange.order.user_id,
						note: exchange.note,
					})

					exchange.details.map(detail => {
						exchangeDetails.push({
							id: detail.id,
							created_at: detail.created_at,
							updated_at: detail.updated_at,
							exchange_id: detail.exchange_id,
							order_detail_id: exchange.order_detail_id,
							replacement_id: exchange.replacement_id,
							inventory_id: !!detail.inventory?.id ? detail.inventory.id : detail.stylesheetInventory.inventory_id,
							note: detail.note,
							is_refund: detail.is_refund,
							status: detail.status,
						})

						if (!!exchange.shipment?.id) {
							shipmentExchangeDetail.push({
								shipment_id: exchange.shipment.id,
								exchange_detail_id: detail.id,
							})
						}
					})
				})
			})

		// upsert exchanges
		await this.MigrationRepository.upsertExchange(exchanges, transaction)

		// upsert details
		await this.MigrationRepository.upsertExchangeDetails(exchangeDetails, transaction)

		// upsert shipmentExchageDetails
		await this.MigrationRepository.upsertShipmentExchangeDetails(shipmentExchangeDetail, transaction)

		return 'done'
	}

	async fixUserPhoneNumber(transaction: EntityManager, id?: number) {
		const userProfile = await this.MigrationRepository.userProfile(transaction)
			.then(profiles => {
				return profiles.reduce((i, profile) => {
					if (!!profile.phone) {
						switch (true) {
							case profile.phone.slice(0, 6) === '+62+62':
								return i.concat({
									...profile,
									phone: profile.phone.substring(6),
								})
							case profile.phone.slice(0, 5) === '+6208':
								return i.concat({
									...profile,
									phone: profile.phone.substring(4),
								})
							case profile.phone.slice(0, 4) === '0628':
							case profile.phone.slice(0, 4) === '+621':
							case profile.phone.slice(0, 4) === '+622':
							case profile.phone.slice(0, 4) === '+623':
							case profile.phone.slice(0, 4) === '+624':
							case profile.phone.slice(0, 4) === '+625':
							case profile.phone.slice(0, 4) === '+626':
							case profile.phone.slice(0, 4) === '+627':
							case profile.phone.slice(0, 4) === '+628':
							case profile.phone.slice(0, 4) === '+629':
								return i.concat({
									...profile,
									phone: profile.phone.substring(3),
								})
							case profile.phone.slice(0, 2) === '08':
								return i.concat({
									...profile,
									phone: profile.phone.substring(1),
								})
							default:
								return i.concat(profile)
						}
					} else {
						return i.concat(profile)
					}
				}, [] as UserProfileInterface[])
			})

		// migrate
		await this.MigrationRepository.updateUserProfile(userProfile, transaction)

		return 'done'
	}

	async fixUserAnswer(transaction: EntityManager) {
		// get wrong format user answer
		const userAnswers = await this.MigrationRepository.userAnswer(transaction)
			.then(user_answers => {
				return user_answers.reduce((i, user_answer) => {
					return i.concat({
						...user_answer,
						answer: {
							CHOICE: [(user_answer.answer.CHOICE as any)],
						},
					})
				}, [] as UserAnswerInterface[])
			})

		// upsert them
		await this.MigrationRepository.upsertUserAnswer(userAnswers, transaction)

		return 'done'
	}

	async migratePrice(type: 'product' | 'variant', buffer: Buffer, transaction: EntityManager) {
		let variants: VariantInterface[] = []
		// get variant price from csv file
		const file = buffer.toString('utf-8')

		// parse to json
		const newPrices: { [id: number]: number } = csvjson.toObject(file, { delimiter: ',' })
			.reduce((i, data: { id: number, price: number }) => {
				if (check(data, ['object', {
					id: 'string',
					price: 'string',
				}])) {
					return {
						...i,
						[data.id]: data.price,
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Wrong CSV Format!!')
				}
			}, {} as { [id: number]: number })

		const ids = Object.keys(newPrices).map(id => parseInt(id, 10))

		if (type === 'variant') {
			// get variants, and map with new prices
			variants = await this.MigrationRepository.variants(ids, transaction)
				.then(async v => {
					// send email backup to rheza@helloyuna.io
					await SendGridHandler.sendCustom('rheza@helloyuna.io', 'Backup Data Price', JSON.stringify(v.map(_v => {
						return {
							id: _v.id,
							price: _v.price,
						}
					})))

					return v
				})
				.then(v => {
					return v.map(variant => {
						return {
							...variant,
							updated_at: !!newPrices[variant.id] ? TimeHelper.moment().toDate() : variant.updated_at,
							price: newPrices[variant.id] ?? variant.price,
						}
					})
				})
		} else if (type === 'product') {
			// get variants from product_ids,and map with new prices
			variants = await this.MigrationRepository.product(ids, { with_variant: true }, transaction)
				.then(async products => {
					// send email backup to rheza@helloyuna.io
					await SendGridHandler.sendCustom('rheza@helloyuna.io', 'Backup Data Price', JSON.stringify(products.map(product => {
						return {
							id: product.id,
							variants: product.variants.map(variant => {
								return {
									id: variant.id,
									price: variant.price,
								}
							}),
						}
					})))

					return products.reduce((init, product) => init.concat(...product.variants), [] as VariantInterface[])
						.map(variant => {
							return {
								...variant,
								updated_at: !!newPrices[variant.product_id] ? TimeHelper.moment().toDate() : variant.updated_at,
								price: newPrices[variant.product_id] ?? variant.price,
							}
						})
				})
		} else {
			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_100, 'Unknown Type')
		}

		// upsert
		await this.MigrationRepository.upsertVariantPrices(variants, transaction)

		return 'donee'
	}

	async migrateRetailPrice(type: 'product' | 'variant', buffer: Buffer, transaction: EntityManager) {
		let variants: VariantInterface[] = []
		// get variant price from csv file
		const file = buffer.toString('utf-8')

		// parse to json
		const newPrices: { [id: number]: number } = csvjson.toObject(file, { delimiter: ',' })
			.reduce((i, data: { id: number, retail_price: number }) => {
				if (check(data, ['object', {
					id: 'string',
					retail_price: 'string',
				}])) {
					return {
						...i,
						[data.id]: data.retail_price,
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Wrong CSV Format!!')
				}

			}, {} as { [id: number]: number })

		const ids = Object.keys(newPrices).map(id => parseInt(id, 10))

		if (type === 'variant') {
			// get variants from db, and map with new prices
			variants = await this.MigrationRepository.variants(Object.keys(newPrices).map(id => parseInt(id, 10)), transaction)
				.then(async v => {
					// send email backup to rheza@helloyuna.io
					await SendGridHandler.sendCustom('rheza@helloyuna.io', 'Backup Data Retail Price', JSON.stringify(v.map(_v => {
						return {
							id: _v.id,
							retail_price: _v.retail_price,
						}
					})))

					return v
				})
				.then(v => {
					return v.map(variant => {
						return {
							...variant,
							updated_at: !!newPrices[variant.id] ? TimeHelper.moment().toDate() : variant.updated_at,
							retail_price: newPrices[variant.id] ?? variant.retail_price,
						}
					})
				})
		} else if (type === 'product') {
			// get variants from product_ids,and map with new prices
			variants = await this.MigrationRepository.product(ids, { with_variant: true }, transaction)
				.then(async products => {
					// send email backup to rheza@helloyuna.io
					await SendGridHandler.sendCustom('rheza@helloyuna.io', 'Backup Data Price', JSON.stringify(products.map(product => {
						return {
							id: product.id,
							variants: product.variants.map(variant => {
								return {
									id: variant.id,
									price: variant.retail_price,
								}
							}),
						}
					})))

					return products.reduce((init, product) => init.concat(...product.variants), [] as VariantInterface[])
						.map(variant => {
							return {
								...variant,
								updated_at: !!newPrices[variant.product_id] ? TimeHelper.moment().toDate() : variant.updated_at,
								retail_price: newPrices[variant.product_id] ?? variant.retail_price,
							}
						})
				})
		} else {
			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_100, 'Unknown Type')
		}

		// upsert
		await this.MigrationRepository.upsertVariantPrices(variants, transaction)

		return 'donee'
	}

	async cleanOldActiveStylesheet(transaction: EntityManager) {
		const stylesheets = await this.MigrationRepository.getActiveStylesheet(transaction)

		// upsert
		await this.MigrationRepository.insertOrUpdate('app.stylesheet', stylesheets.map(stylesheet => {
			return {
				...stylesheet,
				status: STYLESHEET_STATUSES.EXCEPTION,
			}
		}), undefined, transaction)

		return 'donee'
	}

	async migrate(transaction: EntityManager) {
		// get styleboard and order
		const {
			users,
			orders,
		} = await this.MigrationRepository.styleboard(transaction).then(styleboards => {
			return styleboards.reduce((i, styleboard) => {
				return {
					users: [...i.users, {
						user_id: styleboard.user_id,
						outfit: styleboard.outfit,
						attire: styleboard.attire,
						product: styleboard.product,
						styleboard_id: styleboard.id,
						asset_ids: styleboard.assets.map(asset => asset.user_asset_id),
					}],
					orders: [...i.orders, CommonHelper.allowKey(styleboard.order, 'id', 'user_id', 'number', 'status')],
				}
			}, {
				users: [],
				orders: [],
			} as {
				users: Array<{
					user_id: number,
					outfit: string,
					attire: string,
					product: string,
					styleboard_id: number,
					asset_ids: number[],
				}>,
				orders: Array<Pick<OrderInterface, 'id' | 'user_id' | 'number' | 'status'>>,
			})
		})

		// insert user_request
		await this.MigrationRepository.insertUserRequest(users, transaction)

		// update order status
		await this.MigrationRepository.updateOrderStatus(orders, transaction)

		return 'done'
	}

	async getStylistVariant(transaction?: EntityManager) {
		const products = await this.MigrationRepository.getStylistProduct(transaction)
		if (products[0].length) {
			return this.MigrationRepository.getStylistVariant(products[0].map(p => p.product_id), transaction)
		}	
		return [[], 0]
	}

	async batchUpdateVariant(variants, transaction?: EntityManager) {
		return this.MigrationRepository.batchUpdateVariant(variants, transaction)
	}

	async batchUpdateProduct(products: ProductInterface, transaction?: EntityManager) {
		return this.MigrationRepository.batchUpdateProduct(products, transaction)
	}

	async updateDiscountManual(transaction: EntityManager) {
		await PromotionManager.updateDiscountManual(transaction)
		return 'discount updated'
	}

	async revertDeletedStylistProduct(transaction?: EntityManager) {
		const products = await this.MigrationRepository.getDeletedStylistProduct(transaction)

		if (products[0].length) {
			await this.MigrationRepository.revertDeletedStylistProduct(products, transaction)
		}
		return 'done'
	}
}

export default new MigrationManager()

