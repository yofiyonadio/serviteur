import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	Connection,
	EntityManager,
} from 'typeorm'

import {
	ProductManager,
	PromotionManager
} from './index'

import {
	SchedulerRepository,
} from 'app/repositories'

import { SchedulerPreOrderInterface } from 'energie/app/interfaces'
// import { COUPONS } from 'energie/utils/constants/enum'
// import { ERRORS, ERROR_CODES } from 'app/models/error'
import { VARIANT_STAT } from 'energie/utils/constants/enum'

import { TimeHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

class SchedulerManager extends ManagerModel {

	static __displayName = 'SchedulerManager'

	protected SchedulerRepository: SchedulerRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			SchedulerRepository,
		])
	}




	// ============================= GETTER =============================


	@ManagerModel.bound
	async get(
		transaction: EntityManager,
	) {
		return this.SchedulerRepository.get(transaction)
	}

	@ManagerModel.bound
	async getPreOrder(
		transaction: EntityManager,
	) {
		return this.SchedulerRepository.getPreOrder(transaction)
	}

	// ============================= INSERT =============================

	async insertPreOrder(
		order_status: VARIANT_STAT,
		data: Parameter<SchedulerPreOrderInterface>,
		transaction: EntityManager,
	) {
		return this.SchedulerRepository.insertPreOrder(order_status, data, transaction)
	}

	// ============================= DELETE =============================


	// ============================= METHODS ============================

	executor(classes: SchedulerManager, func: string, transaction: EntityManager, params?: any) {
		(classes as any)[func](transaction, params)
	}

	async updateDiscount(transaction) {
		const nows = TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss')
		this.log('updating discount at' + nows)
		return PromotionManager.updateDiscountManual(transaction)
	}

	async preorderScheduler(transaction, params) {
		const nows = TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss')
		const datas: any = {}

		const variant = await ProductManager.variant.getDetail(params.variant_id, transaction)

		if (params.schedule_type === 'START') {
			datas.is_available = true
		}
		if (params.schedule_type === 'END') {
			datas.is_available = false
		}
		if (params.schedule_type === 'SHIPMENT') {
			if (
				(variant.limited_stock && variant.quantity > 0) ||
				(!variant.limited_stock && variant.have_consignment_stock)
			) {
				datas.is_available = true
				datas.order_status = VARIANT_STAT.DEFAULT
			}
		}

		this.log('PO Run at ' + nows)


		ProductManager.variant.update(22, params.variant_id, datas, undefined, undefined, undefined, undefined, transaction)
	}
}

export default new SchedulerManager()

