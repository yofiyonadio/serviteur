
import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection } from 'typeorm'

import NotificationManager from './notification'
import UserManager from './user'

import DEFAULTS from 'utils/constants/default'


class WarningManager extends ManagerModel {

	static __displayName = 'WarningManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [], async () => {
			await NotificationManager.initialize(connection)
			await UserManager.initialize(connection)
		})
	}


	// ============================= INSERT =============================
	@ManagerModel.bound
	async notify(title: string, message: string, err?: Error | ErrorModel) {

		const admin = await this.transaction(transaction => {
			return UserManager.get(DEFAULTS.SYSTEM_USER_ID, transaction)
		})

		return NotificationManager.email.sendNotificationMail(
			admin.email,
			title,
			`${ message }${ err ? `\n\n${ err instanceof ErrorModel
				? `Error: ${ err.type }: ${ err.code }, ${ err.message }\n${ JSON.stringify(err.detail) }`
				: `Error: ${ err.name }: ${ err.message }\n\nStack: ${ JSON.stringify(err.stack) }` }` : '' }`,
		)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new WarningManager()
