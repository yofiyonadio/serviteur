// import {
// 	ErrorModel,
// 	ManagerModel,
// } from 'app/models'
// import { Connection, EntityManager } from 'typeorm'

// import {
// 	VoucherRepository,
// } from 'app/repositories'

// import {
// 	// OrderDetailInterface,
// 	// OrderRequestInterface,
// 	// ShipmentOrderDetailInterface,
// 	// ShipmentInterface,
// 	// UserInterface,
// } from 'energie/app/interfaces'

// import { REDEEM_STATUSES, ORDER_REQUESTS } from 'energie/utils/constants/enum'
// import { ERRORS, ERROR_CODES } from 'app/models/error'


// class OrderRedeemManager extends ManagerModel {

// 	static __displayName = 'OrderRedeemManager'

// 	protected VoucherRepository: VoucherRepository

// 	async initialize(connection: Connection) {
// 		return super.initialize(connection, [
// 			VoucherRepository,
// 		])
// 	}

// 	// ============================= INSERT =============================

// 	// ============================= UPDATE =============================

// 	// ============================= GETTER =============================

// 	// ============================= DELETE =============================

// 	// ============================= METHODS ============================
// 	@ManagerModel.bound
// 	async confirm(
// 		redeem_id: number,
// 		admin_id: number,
// 		type: ORDER_REQUESTS,
// 		ref_id: number,
// 		trx?: EntityManager,
// 	) {
// 		return this.withTransaction(trx, async transaction => {
// 			return this.VoucherRepository.getRedeemDetail(redeem_id, transaction).then(redeem => {
// 				if(redeem.status === REDEEM_STATUSES.PENDING) {
// 					// approve
// 					const now = new Date()
// 					return this.VoucherRepository.updateRedeem(redeem_id, {
// 						status: REDEEM_STATUSES.APPROVED,
// 						type,
// 						ref_id,
// 						processed_at: now,
// 					}, `Approved by ${admin_id}`, transaction).then(isUpdated => {
// 						if(isUpdated) {
// 							redeem.status = REDEEM_STATUSES.APPROVED
// 							redeem.type = type
// 							redeem.ref_id = ref_id
// 							redeem.processed_at = now

// 							return redeem
// 						}

// 						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_099, {
// 							message: 'Something went wrong, cannot update redemption status',
// 							status: false,
// 						})
// 					})
// 				} else if(redeem.status === REDEEM_STATUSES.APPROVED) {
// 					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, {
// 						message: 'Voucher already redeemed',
// 						status: redeem.status,
// 					})
// 				} else if(redeem.status === REDEEM_STATUSES.REJECTED) {
// 					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, {
// 						message: 'Voucher already rejected',
// 						status: redeem.status,
// 					})
// 				} else {
// 					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, {
// 						message: 'There is something error with redemption',
// 						status: redeem.status,
// 					})
// 				}
// 			})
// 		})
// 	}

// 	// ============================ PRIVATES ============================



// }

// export default new OrderRedeemManager()
