import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	BCARepository,
} from 'app/repositories'

import BCAService from 'app/services/bca'

import {
	InquiryListofBillsRequestInterface,
	InquiryListofBillsResponseInterface,
	PaymentFlagRequestInterface,
	PaymentFlagResponseInterface,
} from 'app/interfaces/bca'

import UserManager from '../user'
import OrderManager from '../order'

import crypto from 'crypto'

import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import { TimeHelper } from 'utils/helpers'
import { ORDERS, PAYMENTS, PAYMENT_AGENTS } from 'energie/utils/constants/enum'
import { isNullOrUndefined } from 'util'
import { check } from 'app/interfaces/guard'

class BCAManager extends ManagerModel {
	static __displayName = 'BCAManager'

	protected BCARepository: BCARepository

	private clientId: string = process.env.BCA_CLIENT_ID
	private clientSecret: string = process.env.BCA_CLIENT_SECRET
	private api_key: string = process.env.BCA_API_KEY
	private api_secret: string = process.env.BCA_API_SECRET

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			BCARepository,
		])
	}


	// ============================= INSERT =============================
	@ManagerModel.bound
	async upsert(
		cred: string,
		updated_at: Date,
		expired_at: Date,
		transaction: EntityManager,
	) {
		return this.BCARepository.upsert(cred, updated_at, expired_at, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getToken(authorization: string, transaction: EntityManager) {
		const __authorization: string = `Basic ${Buffer.from(this.clientId + ':' + this.clientSecret).toString('base64')}`

		if(__authorization === authorization) {
			const hashCred = crypto.createHash('sha256').update(`${__authorization}${TimeHelper.moment().toString()}`).digest('hex')
			const updated_at = TimeHelper.moment().toDate()
			const expired_at = TimeHelper.moment(updated_at).add(1, 'hours').toDate()

			const token = await this.upsert(
				hashCred,
				updated_at,
				expired_at,
				transaction,
			)

			return {
				access_token: token,
				token_type: 'bearer',
				expires_in: TimeHelper.moment(expired_at).diff(updated_at) / 1000,
				scope: 'resource.WRITE resource.READ',
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102)
		}
	}

	@ManagerModel.bound
	async getPaymentCode(
		user_id: number,
		transaction: EntityManager,
	) {
		const prefix: number = 12894
		const user = await UserManager.get(user_id, transaction)
		// const phone = user.profile.phone
		let payment_code: string

		if (user.profile.is_phone_verified) {
			// check old order first
			const orders = await UserManager.order.getAll(user_id, {status_in: [ORDERS.PENDING_PAYMENT]}, transaction).catch(() => null)

			if (orders.length > 0) {
				await Promise.all(orders.map(order => {
					return OrderManager.update(user.id, order.id, {status: ORDERS.EXCEPTION}, 'Cancelled', transaction)
				}))
			}

			payment_code = user.profile.phone.substring(3)
		} else {
			payment_code = Math.floor(100000000000 + Math.random() * 900000000000).toString()
		}

		return {
			prefix,
			payment_code: prefix + payment_code,
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async inquiryBill(
		method: string,
		url: string,
		token: string,
		bca_key: string,
		bca_timestamp: string,
		bca_signature: string,
		data: InquiryListofBillsRequestInterface,
		transaction: EntityManager,
	): Promise<InquiryListofBillsResponseInterface> {
		let status: '00' | '01'
		let reason: {
			Indonesian: string,
			English: string,
		}
		let order_id: number = null
		let CustomerName: string = ''
		let total: string = ''

		// authenticate first
		await this.authentication(token, bca_key, bca_signature, {method, url, timestamp: bca_timestamp, body: data}, transaction)

		// body check
		status = this.inquiryBillBodyChecking(data).status
		reason = this.inquiryBillBodyChecking(data).reason

		if (status === '00') {
			// check if customer number is valid
			order_id = await OrderManager.payment.getPaymentCode(`${data.CompanyCode}${data.CustomerNumber}`, transaction).catch(() => null)
			if (order_id === null) {
				status = '01',
				reason = {
					Indonesian: 'VA Number tidak ditemukan',
					English: 'Can\'t find VA number',
				}
			}
		}

		if (status === '00') {
			// get order detail
			const order = await OrderManager.getDetail([order_id], undefined, undefined, undefined, false, transaction).then(o => o[0])
			CustomerName = `${order.user.profile.first_name} ${order.user.profile.last_name}`
			total = Number(OrderManager.utilities.getPrices(order.orderDetails, order.shipments, order.points, order.wallets).total).toFixed(2)

			switch(order.status) {
			case ORDERS.PENDING_PAYMENT:
				status = '00'
				reason = {
					Indonesian: 'Sukses',
					English: 'Success',
				}
				break
			case ORDERS.PAID:
			case ORDERS.PAID_WITH_EXCEPTION:
			case ORDERS.PROCESSING:
			case ORDERS.RESOLVED:
				status = '01'
				reason = {
					Indonesian: 'Bill sudah dibayar',
					English: 'Bill is Already paid',
				}
				break
			case ORDERS.EXPIRED:
				status = '01'
				reason = {
					Indonesian: 'Order sudah expired',
					English: 'Order was expired',
				}
				break
			case ORDERS.EXCEPTION:
				status = '01'
				reason = {
					Indonesian: 'Order error',
					English: 'Order error',
				}
				break
			}
		}

		// make response
		return {
			CompanyCode: '12894',
			CustomerNumber: data.CustomerNumber,
			RequestID: data.RequestID,
			InquiryStatus: status,
			InquiryReason: reason,
			CustomerName,
			CurrencyCode: 'IDR',
			TotalAmount: total,
			SubCompany: '00000',
			DetailBills: [],
			FreeTexts: [],
			AdditionalData: '',
		}
	}

	@ManagerModel.bound
	async flagPayment(
		method: string,
		url: string,
		token: string,
		bca_key: string,
		bca_signature: string,
		bca_timestamp: string,
		data: PaymentFlagRequestInterface,
		transaction: EntityManager,
	): Promise<PaymentFlagResponseInterface> {
		let status: '00' | '01'
		let reason: {
			Indonesian: string,
			English: string,
		}
		let CustomerName: string = ''
		let order_id: number = null
		let total: number = null

		// authenticate first
		await this.authentication(token, bca_key, bca_signature, {method, url, timestamp: bca_timestamp, body: data}, transaction)

		// check body
		status = this.flagPaymentBodyChecking(data).status
		reason = this.flagPaymentBodyChecking(data).reason

		if (status === '00') {
			// check if customer number is valid
			order_id = await OrderManager.payment.getPaymentCode(`${data.CompanyCode}${data.CustomerNumber}`, transaction).catch(() => null)
			if (order_id === null) {
				status = '01',
				reason = {
					Indonesian: 'VA Number tidak ditemukan',
					English: 'Can\'t find VA number',
				}
			}
		}

		if (status === '00') {
			// get order detail
			let order
			order = await OrderManager.getDetail([order_id], undefined, undefined, undefined, false, transaction).then(o => o[0])
			CustomerName = `${order.user.profile.first_name} ${order.user.profile.last_name}`
			total = OrderManager.utilities.getPrices(order.orderDetails, order.shipments, order.points, order.wallets).total

			switch(order.status) {
			case ORDERS.PENDING_PAYMENT:
				status = '00'
				break
			case ORDERS.PAID:
			case ORDERS.PAID_WITH_EXCEPTION:
			case ORDERS.PROCESSING:
			case ORDERS.RESOLVED:
				status = '01'
				reason = {
					Indonesian: 'Bill sudah dibayar',
					English: 'Bill is Already paid',
				}
				break
			case ORDERS.EXPIRED:
				status = '01'
				reason = {
					Indonesian: 'Bill sudah expired',
					English: 'Bill was expired',
				}
				break
			case ORDERS.EXCEPTION:
				status = '01'
				reason = {
					Indonesian: 'Bill tidak valid',
					English: 'Bill is no longer valid',
				}
				break
			}

			if (total !== parseInt(data.TotalAmount, 10)) {
				status = '01'
				reason = {
					Indonesian: 'Total amount tidak sama dengan bill',
					English: 'Input amount is different with bill amount',
				}
			}

			if (status === '00') {
				if (total === parseInt(data.PaidAmount, 10)) {
					await OrderManager.payOrder(
						order.user_id,
						order.id,
						'',
						data,
						PAYMENTS.SUCCESS,
						PAYMENT_AGENTS.BCA,
						true,
						true,
						transaction,
					)

					status = '00'
					reason = {
						Indonesian: 'Sukses',
						English: 'Success',
					}
				} else {
					status = '01',
					reason = {
						Indonesian: 'Paid Amount tidak sama dengan Total Amount',
						English: 'Paid Amount is not match with Total Amount',
					}
				}
			}
		}

		// make response
		return {
			CompanyCode: '12894',
			CustomerNumber: data.CustomerNumber,
			RequestID: data.RequestID,
			PaymentFlagStatus: status,
			PaymentFlagReason: reason,
			CustomerName,
			CurrencyCode: 'IDR',
			PaidAmount: data.PaidAmount,
			TotalAmount: Number(total).toFixed(2),
			TransactionDate: data.TransactionDate,
			DetailBills: [],
			FreeTexts: [],
			AdditionalData: '',
		}
	}

	async inquiryVA(
		request_id?: string,
		customer_number?: string,
	) {
		return BCAService.inquryVA(request_id, customer_number)
	}

	// ============================ PRIVATES ============================
	private timestampFormatCheck(timestamp: string): boolean {
		const regex = RegExp('([0-9]{2}/[0-9]{2}/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2})')
		const test = regex.test(timestamp)

		return !test
	}

	private inquiryBillBodyChecking(body: InquiryListofBillsRequestInterface): {
		status: '00' | '01',
		reason: {
			Indonesian: string,
			English: string,
		},
	} {
		let status: '00' | '01'
		let reason: {
			Indonesian: string,
			English: string,
		}

		if (isNullOrUndefined(body.CompanyCode) || body.CompanyCode === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Company Code kosong',
				English: 'Field Company Code is empty',
			}
		} else if (isNullOrUndefined(body.CustomerNumber) || body.CustomerNumber === '') {
			status =  '01'
			reason = {
				Indonesian: 'Field Customer Number kosong',
				English: 'Field Customer Number is empty',
			}
		} else if (isNullOrUndefined(body.RequestID) || body.RequestID === '') {
			status = '01'
			reason = {
				Indonesian: 'Field RequestID kosong',
				English: 'Field RequestID is empty',
			}
		} else if (isNullOrUndefined(body.ChannelType) || (body.ChannelType as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Channel Type kosong',
				English: 'Field Channel Type is empty',
			}
		} else if (isNullOrUndefined(body.TransactionDate) || (body.TransactionDate as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Transaction Date kosong',
				English: 'Field Transaction Date is empty',
			}
		} else if (this.timestampFormatCheck(body.TransactionDate)) {
			status = '01'
			reason = {
				Indonesian: 'Format Transaction Date tidak sesuai',
				English: 'Transaction Date Format is not match',
			}
		} else {
			status = '00',
			reason = {
				Indonesian: 'Sukses',
				English: 'Success',
			}
		}

		return {
			status,
			reason,
		}
	}

	private flagPaymentBodyChecking(body: PaymentFlagRequestInterface): {
		status: '00' | '01',
		reason: {
			Indonesian: string,
			English: string,
		},
	} {
		let status: '00' | '01'
		let reason: {
			Indonesian: string,
			English: string,
		}

		if (isNullOrUndefined(body.CompanyCode) || body.CompanyCode === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Company Code kosong',
				English: 'Field Company Code is empty',
			}
		} else if (isNullOrUndefined(body.CustomerNumber) || body.CustomerNumber === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Customer Number kosong',
				English: 'Field Customer Number is empty',
			}
		} else if (isNullOrUndefined(body.RequestID) || body.RequestID === '') {
			status = '01'
			reason = {
				Indonesian: 'Field RequestID kosong',
				English: 'Field RequestID is empty',
			}
		} else if (isNullOrUndefined(body.ChannelType) || (body.ChannelType as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Channel Type kosong',
				English: 'Field Channel Type is empty',
			}
		} else if (isNullOrUndefined(body.CustomerName) || (body.CustomerName) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Customer Name kosong',
				English: 'Field Customer Name is empty',
			}
		} else if (isNullOrUndefined(body.CurrencyCode) || (body.CurrencyCode as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Currency Code kosong',
				English: 'Field Currency Code is empty',
			}
		} else if (isNullOrUndefined(body.PaidAmount) || (body.PaidAmount) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Paid Amount kosong',
				English: 'Field Paid Amount is empty',
			}
		} else if (isNullOrUndefined(body.TotalAmount) || (body.TotalAmount) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Total Amount kosong',
				English: 'Field Total Amount is empty',
			}
		} else if (isNullOrUndefined(body.TransactionDate) || (body.TransactionDate as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Transaction Date kosong',
				English: 'Field Transaction Date is empty',
			}
		} else if (this.timestampFormatCheck(body.TransactionDate)) {
			status = '01'
			reason = {
				Indonesian: 'Format Transaction Date tidak sesuai',
				English: 'Transaction Date Format is not match',
			}
		} else if (isNullOrUndefined(body.SubCompany) || (body.SubCompany) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field SubCompany kosong',
				English: 'Field SubCompany is empty',
			}
		} else if (isNullOrUndefined(body.Reference) || (body.Reference) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Reference kosong',
				English: 'Field Reference is empty',
			}
		} else if (isNullOrUndefined(body.FlagAdvice) || (body.FlagAdvice as string) === '') {
			status = '01'
			reason = {
				Indonesian: 'Field Flag Advice kosong',
				English: 'Field Flag Advice is empty',
			}
		} else if (!check(body.FlagAdvice, ['enum', ['Y', 'N']])) {
			status = '01'
			reason = {
				Indonesian: `Field Flag Advice hanya terima 'Y' atau 'N'`,
				English: `Field Flag Advice only accept 'Y' or 'N'`,
			}
		} else {
			status = '00'
			reason = {
				Indonesian: 'Sukses',
				English: 'Success',
			}
		}

		return {
			status,
			reason,
		}
	}

	private async authentication(
		token: string,
		bca_key: string,
		bca_signature: string,
		data: {
			method: string,
			url: string,
			timestamp: string,
			body: object,
		},
		transaction: EntityManager,
	) {
		const bca_token = await this.BCARepository.getToken(transaction)
		const __signature = BCAService.signatureHandler(data.method, data.url, token, this.api_secret, data.timestamp, data.body)

		if (token !== bca_token.token) {
			throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, 'unknown token')
		}

		if (token === bca_token.token && TimeHelper.moment(bca_token.expired_at).diff(TimeHelper.moment()) < 0) {
			throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, 'token has expired')
		}

		if (bca_key !== this.api_key) {
			throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, 'unknown key')
		}

		if (bca_signature !== __signature) {
			throw new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, 'unknown signature')
		}

		return true
	}
}

export default new BCAManager()
