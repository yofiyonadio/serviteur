import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import {
	EntityManager,
} from 'typeorm'

// import { TimeHelper } from 'utils/helpers'

// import { InstagramCatalogManager } from '.'

import { VariantInterface } from 'energie/app/interfaces'
import { InstagramCatalogManager } from '.'

import ListenerInterface from '../../utils/constants/listener'
import { Axios } from '../../utils/helpers'


class ListenerManager extends ManagerModel {

	static __displayName = 'ListenerManager'


	// ============================= GETTER =============================

	// ============================= INSERT =============================

	// ============================= DELETE =============================

	// ============================= EXECUTOR ============================

	executor(payload: any, transaction?: EntityManager) {
		switch (payload.listener_name) {
			case 'instagram_catalog':
				this.instagram_catalog(payload, transaction)
				break;
			case 'cerveau_listener':
				this.cerveau_listener(payload, transaction)
				break;
		}


	}

	// ============================= METHODS ============================

	cerveau_listener(payload: ListenerInterface, transaction?: EntityManager) {
		if (['production'].includes(process.env.ENV)) {
			payload.target_uri.forEach(uri => {
				Axios.get(uri + payload.table).then(res => res).catch(err => err)
			})
		}
	}

	instagram_catalog(payload: ListenerInterface, transaction?: EntityManager) {
		if (['production'].includes(process.env.ENV)) {
			const catalog_urls = payload.target_uri

			if (payload.table === 'variant') {
				let variant: VariantInterface
				if (payload.type === 'INSERT') {
					variant = payload.new as VariantInterface
				} else if (payload.type === 'UPDATE') {
					variant = payload.new as VariantInterface
				} else if (payload.type === 'DELETE') {
					variant = payload.old as VariantInterface
				}

				InstagramCatalogManager.getData(variant.id, transaction)
					.then(catalog => {
						catalog_urls.forEach(catalog_url => {
							Axios.post(catalog_url, {
								method: payload.type,
								datas: catalog[0]
							}).then(res => res).catch(err => err)
						})
					})
					.catch(err => {
						catalog_urls.forEach(catalog_url => {
							Axios.post(catalog_url, {
								method: 'DELETE',
								datas: { id: variant.id }
							}).then(res => res).catch(err => err)
						})
					})
			}
		}
	}



}

export default new ListenerManager()

