import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	TopupRepository,
} from 'app/repositories'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES } from 'app/models/error'


class TopupManager extends ManagerModel {

	static __displayName = 'TopupManager'

	protected TopupRepository: TopupRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			TopupRepository,
		], async () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.TopupRepository.getBy('slug', 'topup-yuna', transaction)
					.catch(async err => {
						if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
							return this.TopupRepository.insert({
								title: 'Yuna Wallet Topup', // Can be used like 'Yuna Wallet Topup – Indomaret' to mark different sources of topups
								slug: 'topup-yuna',
								description: 'Topup your Yuna Wallet.',
								terms: '',
								published_at: new Date(),
							}, transaction)
						}

						throw err
					}).then(topup => {
						DEFAULTS.setWalletTopupId(topup.id)
					})
			})
		})
	}


	// ============================= INSERT =============================
	@ManagerModel.bound
	async createAmount(
		topup_id: number,
		amount: number,
		transaction: EntityManager,
	) {
		return this.TopupRepository.insertAmount({
			topup_id,
			amount,
		}, transaction)
	}

	@ManagerModel.bound
	async getTopupAmount(
		topup_amount_id: number,
		transaction: EntityManager,
	) {
		return this.TopupRepository.getTopupAmount(topup_amount_id, transaction)
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	getBy(
		type: 'id' | 'slug',
		id: string | number,
		transaction: EntityManager,
	) {
		return this.TopupRepository.getBy(type, id, transaction)
	}

	// ============================ PRIVATES ============================

}

export default new TopupManager()
