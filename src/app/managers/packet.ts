import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	PacketRepository,
	InventoryRepository,
	VariantRepository,
	ProductRepository,
	StylesheetRepository,
	MatchboxRepository,
	VoucherRepository,
	OrderDetailRepository,
	ShipmentRepository,
} from 'app/repositories'

import {
	PacketRecord,
} from 'energie/app/records'

import { PacketInterface } from 'energie/app/interfaces'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES, ERRORS } from 'app/models/error'

import { Parameter } from 'types/common'


class PacketManager extends ManagerModel {

	static __displayName = 'PacketManager'

	protected PacketRepository: PacketRepository
	protected InventoryRepository: InventoryRepository
	protected VariantRepository: VariantRepository
	protected ProductRepository: ProductRepository
	protected StylesheetRepository: StylesheetRepository
	protected MatchboxRepository: MatchboxRepository
	protected VoucherRepository: VoucherRepository
	protected OrderDetailRepository: OrderDetailRepository
	protected ShipmentRepository: ShipmentRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			PacketRepository,
			InventoryRepository,
			VariantRepository,
			ProductRepository,
			StylesheetRepository,
			MatchboxRepository,
			VoucherRepository,
			OrderDetailRepository,
			ShipmentRepository,
		], async () => {
			// Initialization function goes here

			return this.withTransaction(undefined, async transaction => {

				const defaultMatchboxPacket = await this.PacketRepository.getPackets({
					weight: 2,
					length: 25,
					width: 25,
					height: 15,
				}, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: 2,
							length: 25,
							width: 25,
							height: 15,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setDefaultMatchboxPacketId(defaultMatchboxPacket.id)

				const miniMatchboxPacket = await this.PacketRepository.getPackets({
					weight: 1,
					length: 30,
					width: 27,
					height: 6,
				}, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: 1,
							length: 30,
							width: 27,
							height: 6,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setMiniMatchboxPacketId(miniMatchboxPacket.id)

				const productPacket = await this.PacketRepository.getPackets({
					weight: .25,
					length: 30,
					width: 30,
					height: 5,
				}, transaction).catch(err => {
					if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: .25,
							length: 30,
							width: 30,
							height: 5,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setProductPacketId(productPacket.id)

				const voucherPacket = await this.PacketRepository.getPackets({
					weight: .01,
					length: 22.9,
					width: 16.2,
					height: .5 ,
				}, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: .01,
							length: 22.9,
							width: 16.2,
							height: .5,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setVoucherPacketId(voucherPacket.id)

				const candlePacket = await this.PacketRepository.getPackets({
					weight: 2,
					length: 11,
					width: 11,
					height: 11,
				}, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: 2,
							length: 11,
							width: 11,
							height: 11,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setCandlePacketId(candlePacket.id)

				const digitalPacket = await this.PacketRepository.getPackets({
					weight: 0,
					length: 0,
					width: 0,
					height: 0,
				}, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						return this.PacketRepository.insert({
							weight: 0,
							length: 0,
							width: 0,
							height: 0,
						}, transaction)
					}

					this.warn(err)
				})

				DEFAULTS.setDigitalPacketId(digitalPacket.id)
			})
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		packet: Parameter<PacketInterface>,
		transaction: EntityManager,
	) {
		return await this.PacketRepository.insert(packet, transaction)
	}

	@ManagerModel.bound
	async createFromIds(
		packetIds: number[],
		is_facade: boolean = false,
		transaction: EntityManager,
	): Promise<PacketInterface> {
		// TODO, use bin packing algorithm
		return this.PacketRepository.getPackets(packetIds, transaction).then(async packets => {
			const finalPacket = packets.reduce((sum, packet) => {
				return {
					...sum,
					weight: sum.weight + packet.weight,
					width: Math.max(sum.width, packet.width),
					height: Math.max(sum.height, packet.height),
					length: sum.length + packet.length,
				}
			}, {
				weight: 0,
				length: 0,
				width: 0,
				height: 0,
				id: null,
				created_at: null,
			})

			if(finalPacket.weight === 0 || finalPacket.length === 0 || finalPacket.width === 0 || finalPacket.height === 0) {
				return new PacketRecord().update(finalPacket)
			} else {
				if(is_facade) {
					return new PacketRecord().update(finalPacket)
				} else {
					return await this.PacketRepository.insert(finalPacket, transaction)
				}
			}
		})
	}

	@ManagerModel.bound
	async createOrUpdate(
		type: 'INVENTORY' | 'VARIANT' | 'PRODUCT' | 'STYLESHEET' | 'MATCHBOX' | 'VOUCHER' | 'ORDER_DETAIL' | 'SHIPMENT',
		type_id: number,
		insertingOrPacketId: number | boolean | undefined,
		data: Parameter<PacketInterface>,
		transaction: EntityManager,
	): Promise<number> {
		if (typeof insertingOrPacketId === 'number') {
			// packet exist
			return this.PacketRepository.update(insertingOrPacketId, data, transaction).then(isUpdated => {
				if (isUpdated) {
					return insertingOrPacketId // number
				}

				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed when updating packet')
			})
		} else {
			// check first, if exist, update, if not exist, create
			return Promise.resolve().then(() => {
				if (insertingOrPacketId === true) {
					return null
				} else {
					switch (type) {
					case 'INVENTORY':
						return this.InventoryRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'VARIANT':
						return this.VariantRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'PRODUCT':
						return this.ProductRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'STYLESHEET':
						return this.StylesheetRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'MATCHBOX':
						return this.MatchboxRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'VOUCHER':
						return this.VoucherRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'ORDER_DETAIL':
						return this.OrderDetailRepository.getPacketId(type_id, transaction).catch(err => null)
					case 'SHIPMENT':
						return this.ShipmentRepository.getPacketId(type_id, transaction).catch(err => null)
					}
				}
			}).then(async packetId => {
				if (packetId === null) {
					// create
					return this.PacketRepository.insert(data, transaction).then(p => p.id).then(async packet_id => {
						switch (type) {
						case 'INVENTORY':
							await this.InventoryRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'VARIANT':
							await this.VariantRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'PRODUCT':
							await this.ProductRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'STYLESHEET':
							await this.StylesheetRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'MATCHBOX':
							await this.MatchboxRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'VOUCHER':
							await this.VoucherRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'ORDER_DETAIL':
							await this.OrderDetailRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						case 'SHIPMENT':
							await this.ShipmentRepository.updatePacketId(type_id, packet_id, transaction).catch(err => null)
							break
						}
						return packet_id
					})
				} else if (typeof packetId === 'number') {
					return this.PacketRepository.update(packetId, data, transaction).then(isUpdated => {
						if (isUpdated) {
							return packetId
						}

						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed when updating packet')
					})
				} else {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Failed when updating packet')
				}
			})
		}
	}

	@ManagerModel.bound
	async createBulk(
		type: 'INVENTORY' | 'VARIANT' | 'PRODUCT' | 'STYLESHEET' | 'MATCHBOX' | 'VOUCHER' | 'ORDER_DETAIL' | 'SHIPMENT',
		type_ids: number[],
		data: Parameter<PacketInterface>,
		transaction: EntityManager,
	): Promise<number> {
		return this.PacketRepository.insert(data, transaction).then(p => p.id).then(async packet_id => {
			switch (type) {
			case 'INVENTORY':
				await this.InventoryRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'VARIANT':
				await this.VariantRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'PRODUCT':
				await this.ProductRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'STYLESHEET':
				await this.StylesheetRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'MATCHBOX':
				await this.MatchboxRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'VOUCHER':
				await this.VoucherRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'ORDER_DETAIL':
				await this.OrderDetailRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			case 'SHIPMENT':
				await this.ShipmentRepository.updatePacketId(type_ids, packet_id, transaction).catch(err => null)
				break
			}

			return packet_id
		})
	}

	@ManagerModel.bound
	async duplicate(
		packet_id: number,
		is_facade: boolean | undefined,
		trx: EntityManager,
	) {
		if(is_facade) {
			return packet_id
		} else {
			return this.withTransaction(trx, async transaction => {
				return this.PacketRepository.duplicate(packet_id, transaction)
			})
		}
	}

	@ManagerModel.bound
	async duplicateAndInsertInto(
		type: 'INVENTORY' | 'VARIANT' | 'PRODUCT' | 'STYLESHEET' | 'MATCHBOX' | 'VOUCHER' | 'ORDER_DETAIL' | 'SHIPMENT',
		type_ids: number[],
		packet_id: number,
		transaction: EntityManager,
	) {
		return this.PacketRepository.duplicate(packet_id, transaction).then(async packetId => {
			switch (type) {
			case 'INVENTORY':
				await this.InventoryRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'VARIANT':
				await this.VariantRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'PRODUCT':
				await this.ProductRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'STYLESHEET':
				await this.StylesheetRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'MATCHBOX':
				await this.MatchboxRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'VOUCHER':
				await this.VoucherRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'ORDER_DETAIL':
				await this.OrderDetailRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			case 'SHIPMENT':
				await this.ShipmentRepository.updatePacketId(type_ids, packetId, transaction).catch(err => null)
				break
			}

			return packetId
		})
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getPacket(
		packet_id: number,
		transaction: EntityManager,
	): Promise<PacketInterface> {
		return this.PacketRepository.getPackets([packet_id], transaction).then(packets => {
			return packets[0]
		})
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async delete(
		packet_id: number,
		transaction: EntityManager,
	) {
		return this.PacketRepository.delete(packet_id, transaction)
	}

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new PacketManager()
