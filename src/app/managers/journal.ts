import {
	ManagerModel, ErrorModel,
} from 'app/models'
import {
	Connection,
} from 'typeorm'

import WordpressService from '../services/wordpress'
import { ErrorInterface } from '../interfaces/wordpress'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import { isEmpty } from 'lodash'

class JournalManager extends ManagerModel {
	static __displayName = 'JournalManager'


	async initialize(connection: Connection) {
		return super.initialize(connection, [])
	}

	async getPosts(
		options: {
			page?: number,
			per_page?: number,
			search?: string,
			order?: string,
	}) {
		return await WordpressService.getPosts(options)
			.catch((error: ErrorInterface) => {
				throw new ErrorModel(ERRORS.NODATA, ERROR_CODES.ERR_101, error.data.params
					? Object.keys(error.data.params).map(key => error.data.params[key]).join(' and ')
					: error.message,
				)
			})
			.then(async posts => {
				if(isEmpty(posts)) {
					throw new ErrorModel(ERRORS.NODATA, ERROR_CODES.ERR_101, 'Post Not Found!')
				}

				return await Promise.all(posts.map(async post => {
					const media = await WordpressService.getMedia(post.featured_media).catch(e => undefined)
					const categories = await Promise.all(post.categories.map(category => WordpressService.getCategory(category)))
						.then(results => results.map(result => result.slug))

					return {
						id: post.id,
						title: post.title.rendered,
						link: post.link,
						categories,
						content: post.content.rendered,
						media_title: media ? media.title : undefined,
						media_details: media ? media.media_details.sizes : undefined,
					}
				}))
			})
	}
}

export default new JournalManager()

