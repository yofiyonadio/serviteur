import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	InstagramCatalogRepository,
} from 'app/repositories'

class InstagramCatalogManager extends ManagerModel {

	protected InstagramCatalogRepository: InstagramCatalogRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			InstagramCatalogRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getData(variant_id: number, transaction: EntityManager) {
		return this.InstagramCatalogRepository.getData(variant_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new InstagramCatalogManager()
