import { ErrorModel, ManagerModel } from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import OrderManager from './index'

import AssetManager from '../asset'
import BrandManager from '../brand'
import InventoryManager from '../inventory'
import MatchboxManager from '../matchbox'
import NotificationManager from '../notification'
import PacketManager from '../packet'
import ProductManager from '../product'
import PromoManager from '../promo'
import ServiceManager from '../service'
import ShipmentManager from '../shipment'
import TopupManager from '../topup'
import UserManager from '../user'
import VoucherManager from '../voucher'

import {
	OrderDetailRepository,
	OrderRepository,
	StylesheetRepository,
} from 'app/repositories'

import {
	OrderDetailInterface,
	OrderRequestInterface,
	VoucherInterface,
	OrderAddressInterface,
	OrderInterface,
	ServiceInterface,
	InventoryInterface,
	MatchboxInterface,
	TopupInterface,
	UserInterface,
	UserProfileInterface,
	StylecardInterface,
} from 'energie/app/interfaces'
import {
	OrderRequestVoucherMetadataInterface,
	OrderRequestServiceMetadataInterface,
	OrderRequestMatchboxMetadataInterface,
	OrderRequestTopupMetadataInterface,
	OrderRequestVoucherTopupMetadataInterface,
	OrderRequestVariantMetadataInterface,
} from 'energie/app/interfaces/order.request'

import { CommonHelper, FormatHelper, TimeHelper, StringHelper } from 'utils/helpers'

import { Parameter } from 'types/common'
import { OrderDetailDynamicInterface } from 'app/interfaces/request/order'

import DEFAULTS from 'utils/constants/default'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { ORDER_DETAILS, ORDER_DETAIL_STATUSES, ORDER_REQUESTS, VOUCHERS, VOUCHER_STATUSES, BOOKING_SOURCES, SHIPMENTS, INVENTORIES, ORDERS, STYLESHEET_STATUSES, STYLEBOARD_STATUSES, STATEMENTS, STATEMENT_SOURCES, STYLESHEETS, WALLET_STATUSES, STYLECARD_STATUSES, VARIANT_STAT } from 'energie/utils/constants/enum'

import { capitalize, padStart, difference, isEmpty } from 'lodash'


class OrderDetailManager extends ManagerModel {

	static __displayName = 'OrderDetailManager'

	protected OrderDetailRepository: OrderDetailRepository
	protected StylesheetRepository: StylesheetRepository
	protected OrderRepository: OrderRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderDetailRepository,
			StylesheetRepository,
			OrderRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		data: Parameter<OrderDetailInterface>,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.insert(data, transaction)
	}

	@ManagerModel.bound
	async createTransaction(
		order_detail_id: number,
		user_wallet_transaction_id: number,
		amount: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.insertTransaction(user_wallet_transaction_id, order_detail_id, amount, note, transaction)
	}

	@ManagerModel.bound
	async createFromRequest(
		changer_user_id: number,
		order: OrderInterface,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		order_date: Date,
		transaction: EntityManager,
	): Promise<Array<OrderDetailInterface & {
		request: OrderRequestInterface & {
			address: OrderAddressInterface,
		},
	}>> {

		let stylingRequestQuota = 1

		return requests.reduce(async (_p, request) => {
			return _p.then(async sum => {
				switch (request.type) {
					case ORDER_REQUESTS.VARIANT: {
						// ========================================================
						// We should respect quantity
						// Steps:
						// 1. Get inventories from variant
						// 2. Create voucher if "as_voucher" is true
						// 3. Set prices
						// 4. Set packet
						// 5. Create order detail
						// 6. Book inventory with newly created detail / voucher
						// ========================================================

						// 1. Get inventories from variant
						const quantity = CommonHelper.default(request.quantity, 1)
						const metadata = (request.metadata as OrderRequestVariantMetadataInterface)
						// If styleboard exist, variant is in styleboard, already checked on request creation
						const styleboard = metadata.styleboard_id ? await ServiceManager.styleboard.get(metadata.styleboard_id, {
							with_stylecard: true,
							with_stylecard_variant: true,
							with_request: true,
							deep: true,
						}, transaction) : null
						const stylecard = metadata.stylecard_id ? await ServiceManager.stylecard.get({
							stylecard_id_or_ids: metadata.stylecard_id,
							many: false,
						}, {
							with_variants: true,
							deep_variants: true,
						}, transaction) : null
						const variant = await ProductManager.variant.get(request.ref_id, {
							detailed: true,
						}, transaction)

						// override price for prestyled
						let price
						let retail_price

						if (!!styleboard) {
							const styleboardUserId = styleboard.user_id || null

							if (!!styleboardUserId && styleboardUserId !== order.user_id) {
								// Validate styleboard ownership
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Styleboard not belong to this user.')
							} else if (styleboard.status === STYLEBOARD_STATUSES.PENDING || styleboard.status === STYLEBOARD_STATUSES.STYLING) {
								// Validate styleboard status
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Styleboard not ready yet to be ordered')
							} else if (styleboard.status === STYLEBOARD_STATUSES.EXCEPTION) {
								// Validate styleboard status
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Exception Styleboard can't be ordered`)
							}
						}

						if (!!stylecard && stylecard.is_bundle) {
							// do error checking
							const stylecardRequestProductIds = await ProductManager.getByVariantIds(requests.filter(r => r.type === ORDER_REQUESTS.VARIANT && (r.metadata as OrderRequestVariantMetadataInterface).stylecard_id === stylecard.id).map(r => r.ref_id), transaction).then(products => {
								return products.map(p => p.id)
							})
							const stylecardProductIds = stylecard.stylecardVariants.map(sv => {
								if(sv.product.id === variant.product_id) {
									price = sv.price
									retail_price = sv.retail_price
								}
								return sv.product.id
							})

							if (difference(stylecardProductIds, stylecardRequestProductIds).length) {
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Miss one or more item from bundle item`)
							}
						} else {
							price = variant.price
							retail_price = variant.retail_price
						}

						const inventories: InventoryInterface[] = await InventoryManager.getFromVariant(request.ref_id, quantity, false, true, transaction).then(async invs => {
							if (variant.is_available) {
								const variantDetail = await ProductManager.variant.getDetail(variant.id, transaction)

								if (variantDetail.quantity === 1 && variant.limited_stock) {
									await ProductManager.variant.updateAvailable(variant.id, false, transaction)
								}
								if (invs.length < quantity) {
									await ProductManager.variant.updateAvailable(variant.id, true, transaction)
									throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, `Product: ${variant.brand.title} – ${variant.product.title} has only ${StringHelper.pluralize(invs.length, 'item', '0 item', 's')} left.`)
								}

								return invs
							} else {
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Out of stock.')
							}
						}).catch(err => {
							if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
								if (!!styleboard && variant.is_available && !request.as_voucher && quantity === 1) {
									// In order for voucher to be created,
									// we must supply inventory id, it needs to be booked
									return Array(quantity).fill(null)
								} else if (!!stylecard && variant.is_available) {
									if (stylecard.inventory_only && variant.have_consignment_stock) {
										return Array(quantity).fill(null)
									}

									throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Out of stock.')
								} else if (!styleboard && !stylecard && variant.is_available) {
									return Array(quantity).fill(null)
								} else {
									throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Out of stock.')
								}
							}

							throw err
						})

						const isPOFixed = variant.order_status === VARIANT_STAT.PO_FIXED

						return [...sum, ...await inventories.reduce(async (p, inventory) => {
							return p.then(async orderDetails => {
								if (!!inventory) {
									// 2. Create voucher if "as_voucher" is true
									const voucher = request.as_voucher ? await this.getVoucherFrom(request, request.metadata as OrderRequestVoucherMetadataInterface, inventory, is_facade, transaction) : null
									this.log('this !!inventory running....')
									const detail = await this.getOrderDetail({
										as_voucher: request.as_voucher,
										is_facade,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.INVENTORY,
										title: `${variant.brand.title} – ${variant.product.title}`,
										description: `Color: ${variant.colors.map(c => capitalize(c.title)).join('/')}, Size: ${variant.size.title}`,

										// 3. Set Prices
										price,
										retail_price,
										handling_fee: 0,

										item: !!voucher ? {
											id: voucher.id,
											// 4. Set packet
											packet_id: voucher.packet_id,
											shipment_at: isPOFixed ? variant.po.shipment_date : voucher.is_physical ? VoucherManager.shipmentDate.toDate() : order.created_at,
											is_physical: voucher.is_physical,
										} : {
											id: inventory.id,
											// 4. Set packet
											packet_id: inventory.packet_id || variant.packet_id || variant.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID,
											// Inventory available
											shipment_at: isPOFixed ? variant.po.shipment_date : InventoryManager.shipmentDate(order_date).toDate(),
											is_physical: true,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction)

									if (is_facade) {
										return [...orderDetails, {
											id: Date.now(),
											created_at: new Date(),
											...detail,
											request,
										}]
									} else {
										// 5. Create order detail
										return this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
											// 6. Book inventory with newly created detail / voucher
											if (!!voucher) {
												await InventoryManager.book(changer_user_id, inventory.id, BOOKING_SOURCES.VOUCHER, voucher.id, `Booking from voucher: #${voucher.code}`, true, transaction)
											} else {
												await InventoryManager.book(changer_user_id, inventory.id, BOOKING_SOURCES.ORDER_DETAIL, orderDetail.id, `Booking from order: #${order.number}`, true, transaction)
											}

											return [...orderDetails, {
												...orderDetail,
												request,
											}]
										})
									}
								} else {

									if (request.as_voucher) {
										throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Out of stock.')
									}

									this.log('this inventory running....')

									// 2. Create voucher if "as_voucher" is true
									const detail = await this.getOrderDetail({
										as_voucher: false,
										is_facade,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.INVENTORY,
										title: `${variant.brand.title} – ${variant.product.title}`,
										description: `Color: ${variant.colors.map(c => capitalize(c.title)).join('/')}, Size: ${variant.size.title}`,

										// 3. Set Prices
										price,
										retail_price,
										handling_fee: 0,

										item: {
											id: null,
											// 4. Set packet
											packet_id: variant.packet_id || variant.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID,
											shipment_at: isPOFixed ? variant.po.shipment_date : (variant.have_consignment_stock ? InventoryManager.shipmentDate(order_date) : (variant.overseas || variant.product.overseas ? InventoryManager.foreignShipmentDate(order_date) : InventoryManager.localShipmentDate(order_date))).toDate(),
											is_physical: true,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction)

									if (is_facade) {
										return [...orderDetails, {
											id: Date.now(),
											created_at: new Date(),
											...detail,
											request,
										}]
									} else {
										// 5. Create order detail
										return this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
											return [...orderDetails, {
												...orderDetail,
												request,
											}]
										})
									}
								}
							})
						}, Promise.resolve([] as Array<OrderDetailInterface & {
							request: OrderRequestInterface & {
								address: OrderAddressInterface,
							},
						}>))]
					} case ORDER_REQUESTS.MATCHBOX: {
						// ========================================================
						// We should respect quantity
						// Steps:
						// 1. Get matchbox
						// 2. Create voucher if "as_voucher" is true
						// 3. Set prices
						// 4. Set packet
						// 5. Create order detail
						// ========================================================

						// 1. Get matchbox
						const quantity = CommonHelper.default(request.quantity, 1)
						const matchbox = await MatchboxManager.get(request.ref_id, true, transaction)
						// const metadata = request.metadata as OrderRequestMatchboxMetadataInterface

						// 2. Create voucher if "as_voucher" is true
						const voucher = request.as_voucher ? await this.getVoucherFrom(request, request.metadata as OrderRequestVoucherMetadataInterface, matchbox, is_facade, transaction) : null
						const detail = await this.getOrderDetail({
							as_voucher: request.as_voucher,
							is_facade,
							order_id: order.id,
							order_request_id: request.id,

							type: ORDER_DETAILS.STYLESHEET,
							title: `${matchbox.title}`,
							description: (request.metadata as OrderRequestMatchboxMetadataInterface).lineup || Array(parseInt(matchbox.count, 10) || 1).fill('Clothing').join(' + '),

							// 3. Set Prices
							price: matchbox.price - matchbox.styling_fee,
							retail_price: matchbox.retail_price,
							handling_fee: matchbox.styling_fee,

							item: !!voucher ? {
								id: voucher.id,
								// 4. Set packet
								packet_id: voucher.packet_id,
								shipment_at: voucher.is_physical ? VoucherManager.shipmentDate.toDate() : order.created_at,
								is_physical: voucher.is_physical,
							} : {
								id: null,
								// 4. Set packet
								packet_id: matchbox.packet_id,
								shipment_at: MatchboxManager.shipmentDate(matchbox.slug).toDate(),
								is_physical: true,
							},

							is_exchange: false,
							status: ORDER_DETAIL_STATUSES.PENDING,
						}, transaction)

						if (is_facade) {
							return [...sum, ...new Array(quantity).fill(undefined).map((u, i) => {
								return {
									id: Date.now() + i,
									created_at: new Date(),
									...detail,
									request,
								}
							})]
						} else {
							// 3. Create order detail
							return [...sum, ...await this.OrderDetailRepository.insert(Array(quantity).fill(detail), transaction).then(orderDetails => {
								return orderDetails.map(orderDetail => {
									return {
										...orderDetail,
										request,
									}
								})
							})]
						}
					} case ORDER_REQUESTS.SERVICE: {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Service request is now deprecated')

						// ========================================================
						// We should respect quantity
						// Steps:
						// 1. Get matchbox
						// 2. Create voucher if "as_voucher" is true
						// 3. Set prices
						// 4. Set packet
						// 5. Create order detail
						// ========================================================

						// 1. Get service
						const haveOrder = await UserManager.order.haveOngoingCustomStyling(order.user_id, transaction)

						if (haveOrder) {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot request another styling since there is ongoing custom styling already.')
						}

						const quantity = request.ref_id === DEFAULTS.CUSTOM_STYLING_SERVICE_ID ? stylingRequestQuota : CommonHelper.default(request.quantity, 1)
						const service = await ServiceManager.get(request.ref_id, false, transaction)
						const metadata = request.metadata as OrderRequestServiceMetadataInterface

						if (request.ref_id === DEFAULTS.CUSTOM_STYLING_SERVICE_ID && stylingRequestQuota > 0) {
							stylingRequestQuota--
						}

						if (quantity === 0) {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Can only request 1 styling at a time.')
						}

						// 2. Create voucher if "as_voucher" is true
						const voucher = request.as_voucher ? await this.getVoucherFrom(request, request.metadata as OrderRequestVoucherMetadataInterface, service, is_facade, transaction) : null
						const detail = await this.getOrderDetail({
							as_voucher: request.as_voucher,
							is_facade,
							order_id: order.id,
							order_request_id: request.id,

							type: ORDER_DETAILS.STYLEBOARD,
							title: metadata.outfit ? `${metadata.outfit} Outfit` : `${service.title}`,
							description: metadata.attire && metadata.outfit ? `A ${metadata.attire.toLowerCase()} outfit for a ${metadata.outfit.toLowerCase()}` : '',

							// 3. Set Prices
							price: service.price,
							retail_price: service.retail_price,
							handling_fee: 0,

							item: !!voucher ? {
								id: voucher.id,
								// 4. Set packet
								packet_id: voucher.packet_id,
								shipment_at: voucher.is_physical ? VoucherManager.shipmentDate.toDate() : order.created_at,
								is_physical: voucher.is_physical,
							} : {
								id: null,
								// 4. Set packet
								packet_id: service.packet_id,
								shipment_at: order.created_at,
								is_physical: false,
							},

							is_exchange: false,
							status: ORDER_DETAIL_STATUSES.PENDING,
						}, transaction)

						if (is_facade) {
							return [...sum, ...new Array(quantity).fill(undefined).map((u, i) => {
								return {
									id: Date.now() + i,
									created_at: new Date(),
									...detail,
									request,
								}
							})]
						} else {
							// 5. Create order detail
							return [...sum, ...await this.OrderDetailRepository.insert(Array(quantity).fill(detail), transaction).then(orderDetails => {
								return orderDetails.map(orderDetail => {
									return {
										...orderDetail,
										request,
									}
								})
							})]
						}
					} case ORDER_REQUESTS.STYLEBOARD: {

						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Styleboard request is now deprecated')

					} case ORDER_REQUESTS.TOPUP: {
						// ========================================================
						// We must respect quantity
						// Steps:
						// 1. Get topup amount
						// 2. Create voucher if "as_voucher" is true
						// 3. Set prices
						// 4. Set packet
						// 5. Create order detail
						// ========================================================

						// 1. Get topup amount
						const quantity = CommonHelper.default(request.quantity, 1)
						const topupAmount = await TopupManager.createAmount(request.ref_id, (request.metadata as OrderRequestTopupMetadataInterface).amount, transaction)

						// 2. Create voucher if "as_voucher" is true
						const voucher = request.as_voucher ? await this.getVoucherFrom(request, request.metadata as OrderRequestVoucherTopupMetadataInterface, topupAmount, is_facade, transaction) : null
						const detail = await this.getOrderDetail({
							as_voucher: request.as_voucher,
							is_facade,
							order_id: order.id,
							order_request_id: request.id,

							type: ORDER_DETAILS.WALLET_TRANSACTION,
							title: topupAmount.title,
							description: `Amount: ${FormatHelper.currency(topupAmount.amount, 'IDR')}`,

							// 3. Set Prices
							price: topupAmount.amount,
							retail_price: topupAmount.amount,
							handling_fee: 0,

							item: !!voucher ? {
								id: voucher.id,
								// 4. Set packet
								packet_id: voucher.packet_id,
								shipment_at: voucher.is_physical ? VoucherManager.shipmentDate.toDate() : order.created_at,
								is_physical: voucher.is_physical,
							} : {
								id: null,
								// 4. Set packet
								packet_id: DEFAULTS.DIGITAL_PACKET_ID,
								shipment_at: order.created_at,
								is_physical: false,
							},

							is_exchange: false,
							status: ORDER_DETAIL_STATUSES.PENDING,
						}, transaction)

						if (is_facade) {
							return [...sum, ...new Array(quantity).fill(undefined).map((u, i) => {
								return {
									id: Date.now() + i,
									created_at: new Date(),
									...detail,
									request,
								}
							})]
						} else {
							// 3. Create order detail
							return [...sum, ...await this.OrderDetailRepository.insert(Array(quantity).fill(detail), transaction).then(orderDetails => {
								return orderDetails.map(orderDetail => {
									return {
										...orderDetail,
										request,
									}
								})
							})]
						}
					} case ORDER_REQUESTS.VOUCHER: {
						// ========================================================
						// It should not have quantity. Quantity is ignored
						// It should not have "as_voucher" attribute
						// It's ID is CODE
						// Steps:
						// 1. Get voucher detail
						// 2. Get item detail
						// 3. Set prices
						// 4. Set packet
						// 5. Create order detail
						// 6. Move booking to order detail if voucher type is "INVENTORY"
						// 7. Update voucher status to used
						// ========================================================

						// 1. Get voucher detail
						const voucher = await VoucherManager.get(request.ref_id, true, transaction)

						switch (voucher.type) {
							case VOUCHERS.INVENTORY: {
								// 2. Get item detail
								const inventory = await InventoryManager.get(voucher.ref_id, transaction)
								const variant = await ProductManager.variant.get(inventory.variant_id, { detailed: true }, transaction)

								const detail = await this.getOrderDetail({
									as_voucher: false, // we do not want to have voucherception
									is_facade,
									order_id: order.id,
									order_request_id: request.id,

									type: ORDER_DETAILS.INVENTORY,
									title: `${variant.brand.title} – ${variant.product.title}`,
									description: `Color: ${variant.colors.map(color => capitalize(color.title)).join('/')}, Size: ${variant.size.title}`,

									// 3. Set prices
									price: 0,
									retail_price: variant.retail_price,
									handling_fee: 0,

									item: {
										id: inventory.id,
										// 4. Set packet
										packet_id: inventory.packet_id || variant.packet_id || variant.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID,
										// Inventory available
										shipment_at: InventoryManager.shipmentDate(order_date).toDate(),
										is_physical: true,
									},

									is_exchange: false,
									status: ORDER_DETAIL_STATUSES.PENDING,
								}, transaction)

								if (is_facade) {
									return [...sum, {
										id: Date.now(),
										created_at: new Date(),
										...detail,
										request,
									}]
								} else {
									// 5. Create order detail
									return [...sum, ...await this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
										// 6. Move booking to order detail if voucher type is "INVENTORY"
										await InventoryManager.updateBookingSource(changer_user_id, 'VOUCHER_TO_ORDER', voucher.id, orderDetail.id, transaction)

										// 7. Update voucher status to used
										// To prevent voucher duplicate usage
										await VoucherManager.redeem(changer_user_id, voucher.id, VOUCHER_STATUSES.REDEEM_PENDING, `Voucher is beeing used by #OD-${orderDetail.id}`, transaction)

										return [{
											...orderDetail,
											request,
										}]
									})]
								}
							} case VOUCHERS.MATCHBOX: {
								// 2. Get item detail
								const matchbox = await MatchboxManager.get(voucher.ref_id, true, transaction)

								const detail = await this.getOrderDetail({
									as_voucher: false,  // we do not want to have voucherception
									is_facade,
									order_id: order.id,
									order_request_id: request.id,

									type: ORDER_DETAILS.STYLESHEET,
									title: `${matchbox.title}`,
									description: (request.metadata as OrderRequestMatchboxMetadataInterface).lineup || Array(parseInt(matchbox.count, 10) || 1).fill('Clothing').join(' + '),

									// 3. Set Prices
									price: 0,
									retail_price: matchbox.retail_price,
									handling_fee: 0,

									item: {
										id: null,
										// 4. Set packet
										packet_id: matchbox.packet_id,
										shipment_at: MatchboxManager.shipmentDate(matchbox.slug).toDate(),
										is_physical: true,
									},

									is_exchange: false,
									status: ORDER_DETAIL_STATUSES.PENDING,
								}, transaction)

								if (is_facade) {
									return [...sum, {
										id: Date.now(),
										created_at: new Date(),
										...detail,
										request,
									}]
								} else {
									// 3. Create order detail
									return [...sum, ...await this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
										// 5. Update voucher status to used
										// To prevent voucher duplicate usage
										await VoucherManager.redeem(changer_user_id, voucher.id, VOUCHER_STATUSES.REDEEM_PENDING, `Voucher is beeing used by #OD-${orderDetail.id}`, transaction)

										return [{
											...orderDetail,
											request,
										}]
									})]
								}
							} case VOUCHERS.SERVICE: {
								// DEPRECATED
								// 2. Get item detail
								const service = await ServiceManager.get(voucher.ref_id, false, transaction)
								const metadata = request.metadata as OrderRequestServiceMetadataInterface

								const detail = await this.getOrderDetail({
									as_voucher: false,  // we do not want to have voucherception
									is_facade,
									order_id: order.id,
									order_request_id: request.id,

									type: ORDER_DETAILS.STYLEBOARD,
									title: metadata.outfit ? `${metadata.outfit} Outfit` : `${service.title}`,
									description: metadata.outfit && metadata.attire ? `A ${metadata.outfit.toLowerCase()} outfit for a ${metadata.attire.toLowerCase()}` : '',

									// 3. Set Prices
									price: 0,
									retail_price: service.retail_price,
									handling_fee: 0,

									item: {
										id: null,
										// 4. Set packet
										packet_id: service.packet_id,
										shipment_at: order.created_at,
										is_physical: false,
									},

									is_exchange: false,
									status: ORDER_DETAIL_STATUSES.PENDING,
								}, transaction)

								if (is_facade) {
									return [...sum, {
										id: Date.now(),
										created_at: new Date(),
										...detail,
										request,
									}]
								} else {
									// 3. Create order detail
									return [...sum, ...await this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
										// 5. Update voucher status to used
										// To prevent voucher duplicate usage
										await VoucherManager.redeem(changer_user_id, voucher.id, VOUCHER_STATUSES.REDEEM_PENDING, `Voucher is beeing used by #OD-${orderDetail.id}`, transaction)

										return [{
											...orderDetail,
											request,
										}]
									})]
								}
							} case VOUCHERS.TOPUP_AMOUNT: {
								// 2. Get item detail
								const topupAmount = await TopupManager.getTopupAmount(voucher.ref_id, transaction)

								const detail = await this.getOrderDetail({
									as_voucher: false,
									is_facade,
									order_id: order.id,
									order_request_id: request.id,

									type: ORDER_DETAILS.WALLET_TRANSACTION,
									title: topupAmount.title,
									description: `Amount: ${FormatHelper.currency(topupAmount.amount, 'IDR')}`,

									// 3. Set Prices
									price: 0,
									retail_price: topupAmount.amount,
									handling_fee: 0,

									item: {
										id: null,
										// 4. Set packet
										packet_id: DEFAULTS.DIGITAL_PACKET_ID,
										shipment_at: order.created_at,
										is_physical: false,
									},

									is_exchange: false,
									status: ORDER_DETAIL_STATUSES.PENDING,
								}, transaction)

								if (is_facade) {
									return [...sum, {
										id: Date.now(),
										created_at: new Date(),
										...detail,
										request,
									}]
								} else {
									// 3. Create order detail
									return [...sum, ...await this.OrderDetailRepository.insert(detail, transaction).then(async orderDetail => {
										// 5. Update voucher status to used
										// To prevent voucher duplicate usage
										await VoucherManager.redeem(changer_user_id, voucher.id, VOUCHER_STATUSES.REDEEM_PENDING, `Voucher is beeing used by #OD-${orderDetail.id}`, transaction)

										return [{
											...orderDetail,
											request,
										}]
									})]
								}
							} default:
								throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_103, 'Voucher type invalid.')
						}

					} default:
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request type invalid.')
				}
			})
		}, Promise.resolve([] as Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>))
	}

	@ManagerModel.bound
	async createReturnShipment(
		user_id: number,
		order_detail_id: number,
		awb: string,
		courier: string,
		transaction: EntityManager,
	) {
		const orderDetail = await this.OrderDetailRepository.getWithAddressAndPacket(user_id, order_detail_id, transaction)
		const destination = await BrandManager.getAddresses(DEFAULTS.YUNA_BRAND_ID, transaction).then(addresses => addresses[0])

		return ShipmentManager.create(
			SHIPMENTS.MANUAL,
			orderDetail.packet,
			[orderDetail],
			[orderDetail.address, destination],
			false,
			true,
			{
				amount: 0,
				prices: {},
				awb,
				url: '',
				note: '',
				metadata: {},
				created_at: new Date(),
				courier,
				service: '',
			},
			false,
			transaction,
		)
	}

	@ManagerModel.bound
	async createStatus(
		changer_user_id: number,
		order_detail_id: number,
		code: ORDER_DETAIL_STATUSES,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.update(changer_user_id, order_detail_id, {
			status: code,
		}, note, transaction)
	}

	@ManagerModel.bound
	async composeBagFromRequest<T extends OrderRequestInterface>(
		user_id: number,
		order: OrderInterface,
		requests: T[],
		{
			with_shipment,
		}: {
			with_shipment: boolean,
		},
		transaction: EntityManager,
	): Promise<Array<OrderDetailDynamicInterface<ORDER_DETAILS, T['type']>>> {

		let stylingRequestQuota = 1
		const order_date = new Date()

		return requests.reduce(async (_p, request) => {
			return _p.then(async sum => {
				switch (request.type) {
					case ORDER_REQUESTS.VARIANT: {
						let isInventoryAvailable = true
						const metadata = (request.metadata as OrderRequestVariantMetadataInterface)
						const variant = await ProductManager.variant.getDetailFlat(request.ref_id, transaction)
						const styleboard = metadata.styleboard_id ? await ServiceManager.styleboard.get(metadata.styleboard_id, {
							with_stylecard: true,
							with_stylecard_variant: true,
							with_request: true,
							deep: true,
						}, transaction) : null
						const stylecard = metadata.stylecard_id ? await ServiceManager.stylecard.get({
							stylecard_id_or_ids: metadata.stylecard_id,
							many: false,
						}, {
							with_variants: true,
							deep_variants: true,
						}, transaction) : null
						const quantity = await ProductManager.variant.getAvailableInventoryCount(variant.id, transaction).then(qty => {
							if (variant.is_available) {
								if (qty === 0) {
									isInventoryAvailable = variant.have_consignment_stock ? true : false
									if (!!styleboard && variant.is_available) {
										// If inventory is 0 but variant is in styleboard,
										// We let it to be bought once
										return 1
									} else if (!!stylecard && variant.is_available) {
										return 1
									} else if (!styleboard && !stylecard && variant.is_available) {
										// for regular product
										return request.quantity
									}
								}

								return qty
							} else {
								return 0
							}
						})
						const physical = !!(request.metadata as OrderRequestVoucherMetadataInterface).is_physical

						// Error Handler
						let error: ErrorModel
						let price
						let retail_price

						if ((quantity < request.quantity) && (quantity === 0)) {
							error = new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Out of stock')
						} else if ((quantity < request.quantity) && (quantity > 0)) {
							error = new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_104, `Only ${StringHelper.pluralize(quantity, 'item', '0 item', 's')} left`)
						} else if (metadata.inventory_id && variant.inventories.findIndex(i => i.id === metadata.inventory_id) === -1) {
							error = new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Inventory is not found in this variant')
						}

						if (!!stylecard && stylecard.is_bundle) {
							// do error checking
							const stylecardRequestProductIds = await ProductManager.getByVariantIds(requests.filter(r => r.type === ORDER_REQUESTS.VARIANT && (r.metadata as OrderRequestVariantMetadataInterface).stylecard_id === stylecard.id).map(r => r.ref_id), transaction).then(products => {
								return products.map(p => p.id)
							})
							const stylecardProductIds = stylecard.stylecardVariants.map(sv => {
								// override price if prestyled
								if(sv.product.id === variant.product_id) {
									price = sv.price
									retail_price = sv.retail_price
								}

								return sv.product.id
							})

							if (difference(stylecardProductIds, stylecardRequestProductIds).length) {
								error = new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Miss one or more item from bundle item`)
							}
						} else {
							price = variant.price
							retail_price = variant.retail_price
						}

						const isPOFixed = variant.order_status === VARIANT_STAT.PO_FIXED
						

						return [...sum, {
							...await this.getOrderDetail({
								as_voucher: request.as_voucher,
								is_facade: true,
								order_id: order.id,
								order_request_id: request.id,

								type: ORDER_DETAILS.INVENTORY,
								title: `${variant.brand.title} – ${variant.product.title}`,
								description: `Color: ${variant.colors.map(color => capitalize(color.title)).join('/')}, Size: ${variant.size.title}`,

								price,
								retail_price,
								handling_fee: 0,

								item: {
									id: null,
									// 4. Set packet
									packet_id: with_shipment
										? this.getPacketId(variant.packet_id || variant.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID, request.as_voucher, physical)
										: DEFAULTS.DIGITAL_PACKET_ID,
									shipment_at: isPOFixed ? variant.po.shipment_date : request.as_voucher
										? physical
											? VoucherManager.shipmentDate.toDate()
											: order.created_at
										: isInventoryAvailable
											? InventoryManager.shipmentDate(order_date).toDate()
											: variant.overseas || variant.product.overseas
												? InventoryManager.foreignShipmentDate(order_date).toDate()
												: InventoryManager.localShipmentDate(order_date).toDate(),
									is_physical: request.as_voucher ? physical : true,
								},

								is_exchange: false,
								status: ORDER_DETAIL_STATUSES.PENDING,
							}, transaction),
							id: Date.now(),
							request,
							quantity,
							error,
							item: {
								...variant,
								product: variant.product,
								brand: variant.brand,
								asset: variant.asset,
								category: variant.category,
								size: variant.size,
								colors: variant.colors,
								po: variant.po,
								styleboard: styleboard || undefined,
								stylecard: stylecard || undefined,
							},
						} as OrderDetailDynamicInterface<ORDER_DETAILS.INVENTORY, ORDER_REQUESTS.VARIANT>]
					} case ORDER_REQUESTS.MATCHBOX: {
						const matchbox = await MatchboxManager.get(request.ref_id, true, transaction)
						const physical = !!(request.metadata as OrderRequestVoucherMetadataInterface).is_physical
						const quantity = Infinity

						return [...sum, {
							...await this.getOrderDetail({
								as_voucher: request.as_voucher,
								is_facade: true,
								order_id: order.id,
								order_request_id: request.id,

								type: ORDER_DETAILS.STYLESHEET,
								title: matchbox.title,
								description: null,

								price: matchbox.price - matchbox.styling_fee,
								retail_price: matchbox.retail_price - matchbox.styling_fee,
								handling_fee: matchbox.styling_fee,

								item: {
									id: null,
									// 4. Set packet
									packet_id: with_shipment
										? this.getPacketId(matchbox.packet_id || DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID, request.as_voucher, physical)
										: DEFAULTS.DIGITAL_PACKET_ID,
									shipment_at: request.as_voucher
										? physical
											? VoucherManager.shipmentDate.toDate()
											: order.created_at
										: MatchboxManager.shipmentDate(matchbox.slug).toDate(),
									is_physical: request.as_voucher ? physical : true,
								},

								is_exchange: false,
								status: ORDER_DETAIL_STATUSES.PENDING,
							}, transaction),
							id: Date.now(),
							request,
							quantity,
							item: {
								...matchbox,
								brand: {
									...matchbox.brand,
									address: undefined,
								},
								asset: await AssetManager.get('matchbox', matchbox.id, false, transaction).catch(error => null),
								variants: [],
							},
						} as OrderDetailDynamicInterface<ORDER_DETAILS.STYLESHEET, ORDER_REQUESTS.MATCHBOX>]
					} case ORDER_REQUESTS.SERVICE: {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Service request is now deprecated')

						let error: ErrorModel
						const service = await ServiceManager.get(request.ref_id, true, transaction)
						const physical = !!(request.metadata as OrderRequestVoucherMetadataInterface).is_physical
						const haveOngoing = await UserManager.order.haveOngoingCustomStyling(user_id, transaction)
						const quantity = haveOngoing
							? 0
							: request.ref_id === DEFAULTS.CUSTOM_STYLING_SERVICE_ID
								? stylingRequestQuota
								: Infinity

						if (request.ref_id === DEFAULTS.CUSTOM_STYLING_SERVICE_ID && stylingRequestQuota > 0) {
							stylingRequestQuota--
						}

						if (quantity < request.quantity) {
							if (haveOngoing) {
								error = new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'You already have an ongoing styling')
							} else {
								error = new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'One custom request at a time')
							}
						}

						return [...sum, {
							...await this.getOrderDetail({
								as_voucher: request.as_voucher,
								is_facade: true,
								order_id: order.id,
								order_request_id: request.id,

								type: ORDER_DETAILS.STYLEBOARD,
								title: service.title,
								description: null,

								price: service.price,
								retail_price: service.retail_price,
								handling_fee: 0,

								item: {
									id: null,
									// 4. Set packet
									packet_id: with_shipment
										? this.getPacketId(service.packet_id || DEFAULTS.DIGITAL_PACKET_ID, request.as_voucher, physical)
										: DEFAULTS.DIGITAL_PACKET_ID,
									shipment_at: request.as_voucher
										? physical
											? VoucherManager.shipmentDate.toDate()
											: order.created_at
										: order.created_at,
									is_physical: request.as_voucher ? physical : false,
								},

								is_exchange: false,
								status: ORDER_DETAIL_STATUSES.PENDING,
							}, transaction),
							id: Date.now(),
							request,
							quantity,
							error,
							item: {
								...service,
								brand: {
									...service.brand,
									address: undefined,
								},
								asset: await AssetManager.get('service', service.id, false, transaction).catch(error => null),
							},
						} as OrderDetailDynamicInterface<ORDER_DETAILS.STYLEBOARD, ORDER_REQUESTS.SERVICE>]
					} case ORDER_REQUESTS.STYLEBOARD: {

						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Styleboard request is now deprecated')

					} case ORDER_REQUESTS.TOPUP: {
						const topupAmount = await TopupManager.createAmount(request.ref_id, (request.metadata as OrderRequestTopupMetadataInterface).amount, transaction)
						const physical = !!(request.metadata as OrderRequestVoucherMetadataInterface).is_physical
						const quantity = Infinity

						return [...sum, {
							...await this.getOrderDetail({
								as_voucher: request.as_voucher,
								is_facade: true,
								order_id: order.id,
								order_request_id: request.id,

								type: ORDER_DETAILS.WALLET_TRANSACTION,
								title: `Amount: ${FormatHelper.currency(topupAmount.amount, 'IDR')}`,
								description: null,

								price: topupAmount.amount,
								retail_price: topupAmount.amount,
								handling_fee: 0,

								item: {
									id: null,
									// 4. Set packet
									packet_id: with_shipment
										? this.getPacketId(DEFAULTS.DIGITAL_PACKET_ID, request.as_voucher, physical)
										: DEFAULTS.DIGITAL_PACKET_ID,
									shipment_at: request.as_voucher
										? physical
											? VoucherManager.shipmentDate.toDate()
											: order.created_at
										: ServiceManager.shipmentDate.toDate(),
									is_physical: request.as_voucher ? physical : false,
								},

								is_exchange: false,
								status: ORDER_DETAIL_STATUSES.PENDING,
							}, transaction),
							id: Date.now(),
							request,
							quantity,
							item: {
								...topupAmount,
								asset: '//app/topup.jpg',
							},
						} as OrderDetailDynamicInterface<ORDER_DETAILS.WALLET_TRANSACTION, ORDER_REQUESTS.TOPUP>]
					} case ORDER_REQUESTS.VOUCHER: {

						let error: ErrorModel
						const voucher = await VoucherManager.get(request.ref_id, true, transaction).catch(err => {
							error = err

							return VoucherManager.get(request.ref_id, false, transaction)
						})

						const quantity = error ? 0 : 1

						switch (voucher.type) {
							case VOUCHERS.INVENTORY: {
								const inventory = await InventoryManager.getDeepFlat(voucher.ref_id, false, transaction)

								return [...sum, {
									...await this.getOrderDetail({
										as_voucher: false,
										is_facade: true,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.INVENTORY,
										title: `${inventory.brand.title} – ${inventory.product.title}`,
										description: `Color: ${inventory.colors.map(color => capitalize(color.title)).join('/')}, Size: ${inventory.size.title}`,

										price: 0,
										retail_price: inventory.variant.retail_price,
										handling_fee: 0,

										item: {
											id: null,
											// 4. Set packet
											packet_id: with_shipment
												? inventory.packet_id || inventory.variant.packet_id || inventory.product.packet_id || DEFAULTS.PRODUCT_PACKET_ID
												: DEFAULTS.DIGITAL_PACKET_ID,
											// Inventory available
											// Id null because of voucher usage. Once paid, it is available
											shipment_at: InventoryManager.shipmentDate(order_date).toDate(),
											is_physical: true,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction),
									id: Date.now(),
									request,
									quantity,
									error,
									item: {
										...inventory.variant,
										product: inventory.product,
										brand: inventory.brand,
										asset: inventory.asset,
										category: inventory.category,
										size: inventory.size,
										colors: inventory.colors,
									},
								} as OrderDetailDynamicInterface<ORDER_DETAILS.INVENTORY, ORDER_REQUESTS.VOUCHER>]
							} case VOUCHERS.MATCHBOX: {
								const matchbox = await MatchboxManager.get(voucher.ref_id, true, transaction)

								return [...sum, {
									...await this.getOrderDetail({
										as_voucher: false,
										is_facade: true,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.STYLESHEET,
										title: matchbox.title,
										description: null,

										price: 0,
										retail_price: matchbox.retail_price,
										handling_fee: 0,

										item: {
											id: null,
											// 4. Set packet
											packet_id: with_shipment
												? matchbox.packet_id || DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID
												: DEFAULTS.DIGITAL_PACKET_ID,
											shipment_at: MatchboxManager.shipmentDate(matchbox.slug).toDate(),
											is_physical: true,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction),
									id: Date.now(),
									request,
									quantity,
									error,
									item: {
										...matchbox,
										brand: {
											...matchbox.brand,
											address: undefined,
										},
										asset: await AssetManager.get('matchbox', matchbox.id, false, transaction).catch(error => null),
										variants: [],
									},
								} as OrderDetailDynamicInterface<ORDER_DETAILS.STYLESHEET, ORDER_REQUESTS.VOUCHER>]
							} case VOUCHERS.SERVICE: {
								const service = await ServiceManager.get(voucher.ref_id, true, transaction)

								return [...sum, {
									...await this.getOrderDetail({
										as_voucher: false,
										is_facade: true,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.STYLEBOARD,
										title: service.title,
										description: null,

										price: 0,
										retail_price: service.retail_price,
										handling_fee: 0,

										item: {
											id: null,
											// 4. Set packet
											packet_id: with_shipment
												? service.packet_id || DEFAULTS.DIGITAL_PACKET_ID
												: DEFAULTS.DIGITAL_PACKET_ID,
											shipment_at: ServiceManager.shipmentDate.toDate(),
											is_physical: false,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction),
									id: Date.now(),
									request,
									quantity,
									item: {
										...service,
										brand: {
											...service.brand,
											address: undefined,
										},
										asset: await AssetManager.get('service', service.id, false, transaction).catch(error => null),
									},
								} as OrderDetailDynamicInterface<ORDER_DETAILS.STYLEBOARD, ORDER_REQUESTS.SERVICE>]
							} case VOUCHERS.TOPUP_AMOUNT: {
								const topupAmount = await TopupManager.getTopupAmount(voucher.ref_id, transaction)

								return [...sum, {
									...await this.getOrderDetail({
										as_voucher: false,
										is_facade: true,
										order_id: order.id,
										order_request_id: request.id,

										type: ORDER_DETAILS.WALLET_TRANSACTION,
										title: `Amount: ${FormatHelper.currency(topupAmount.amount, 'IDR')}`,
										description: null,

										price: 0,
										retail_price: topupAmount.amount,
										handling_fee: 0,

										item: {
											id: null,
											// 4. Set packet
											packet_id: DEFAULTS.DIGITAL_PACKET_ID,
											shipment_at: VoucherManager.shipmentDate.toDate(),
											is_physical: false,
										},

										is_exchange: false,
										status: ORDER_DETAIL_STATUSES.PENDING,
									}, transaction),
									id: Date.now(),
									request,
									quantity,
									error,
									item: {
										...topupAmount,
										asset: '//app/topup.jpg',
									},
								} as OrderDetailDynamicInterface<ORDER_DETAILS.WALLET_TRANSACTION, ORDER_REQUESTS.VOUCHER>]
							} default:
								throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_103, 'Voucher type invalid.')
						}

					} default:
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request type invalid.')
				}
			})
		}, Promise.resolve([] as Array<OrderDetailDynamicInterface<ORDER_DETAILS, T['type']>>)).then(details => {
			return details.map(detail => {
				if (detail.quantity < detail.request.quantity && !detail.error) {
					detail.error = new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, `Only ${StringHelper.pluralize(detail.quantity, 'item', '0 item', 's')} left`)
				}

				return detail
			})
		})
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		order_detail_id: number,
		update: Partial<Parameter<OrderDetailInterface>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const updateOrderDetail = await this.OrderDetailRepository.update(changer_user_id, order_detail_id, update, note, transaction)
		if (update.status === "EXCEPTION") {
			const od = await this.getByIds([order_detail_id], false, transaction)
			const ods = await this.OrderRepository.getShallowWithDetails(od[0].order_id, transaction)
			let allException = true
			ods.orderDetails.map(od => {
				if (od.status !== 'EXCEPTION') {
					allException = false
				}
			})

			if (allException) {
				await OrderManager.exceptOrder(
					changer_user_id,
					ods.id,
					'Order status auto updated to exception because all order detail status is exception',
					false,
					transaction
				)
			}
		}

		return updateOrderDetail
	}

	@ManagerModel.bound
	async systemUpdate(
		order_detail_id: number,
		update: Partial<Parameter<OrderDetailInterface>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.systemUpdate(order_detail_id, update, note, transaction)
	}

	// ============================= GETTER =============================
	get get() {
		return this.OrderDetailRepository.getDetail
	}

	@ManagerModel.bound
	async getByIds(
		ids: number[],
		deep: boolean = true,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getByIds(ids, deep, transaction)
	}

	@ManagerModel.bound
	async getUnshipped(
		offset: number = 0,
		limit: number = 40,
		filter: {
			search?: string,
			status?: ORDER_DETAIL_STATUSES,
		} = {},
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.filterUnshippedOrderDetail(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	async getByRefId(
		type: ORDER_DETAILS,
		refId: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getByRefId(type, refId, transaction)
	}

	@ManagerModel.bound
	async getByStylesheetId(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getByStylesheetId(stylesheet_id, transaction)
	}

	@ManagerModel.bound
	async getShipmentAddress(
		ids: number[],
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getShipmentAddress(ids, transaction)
	}

	// @ManagerModel.bound
	// async getDetailed(
	// 	user_id: number,
	// 	order_detail_id: number,
	// 	transaction: EntityManager,
	// ) {
	// 	return this.OrderDetailRepository.getDetailed(user_id, order_detail_id, transaction)
	// }

	@ManagerModel.bound
	async getBelong(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getBelong(user_id, order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getDetailedHistory(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getDetailedHistory(user_id, order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getTransactions(
		user_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getTransactions(user_id, order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getDetailedShallow(
		order_detail_ids: number[],
		with_discount: boolean,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getDetailedShallow(order_detail_ids, with_discount, transaction)
	}

	@ManagerModel.bound
	async getWithFeedback(
		order_detail_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getWithFeedback(order_detail_id, user_id, transaction)
	}

	@ManagerModel.bound
	async checkFeedback(
		order_detail_id: number,
		user_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.checkFeedback(order_detail_id, user_id, transaction)
	}

	@ManagerModel.bound
	async getDetail(
		id: number,
		user_id: number | undefined,
		is_active: boolean,
		filter: {
			date?: Date,
			status?: ORDER_DETAIL_STATUSES,
			// with_feedback?: boolean,
			// with_exchange?: boolean,
			// with_history?: boolean,
		} = {},
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getMatches(id, user_id, is_active, {
			...filter,
			with_feedback: true,
			with_history: true,
			with_refund: true,
		}, transaction).then(details => details[0])
	}

	@ManagerModel.bound
	async getOrderTransactions(
		limit: number | undefined,
		sort_by: string | undefined,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getOrderTransactions(limit, sort_by, transaction)
	}

	@ManagerModel.bound
	async getMyMatches(
		user_id: number | undefined,
		is_active: boolean,
		filter: {
			date?: Date,
			status?: ORDER_DETAIL_STATUSES,
		} = {},
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getMatches(undefined, user_id, is_active, filter, transaction)
	}

	@ManagerModel.bound
	async getPostponed(
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getPostponed(transaction)
	}

	@ManagerModel.bound
	async getPacket(
		order_detail_ids: number[],
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getDetailPacket(order_detail_ids, transaction)
	}

	@ManagerModel.bound
	async getOrder(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getOrder(order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getWithAddressAndPacket(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.getWithAddressAndPacket(undefined, order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getOrderDetailFromVoucherRedeem(id: number, transaction: EntityManager) {
		return await this.OrderDetailRepository.getOrderDetailFromVoucherRedeem(id, transaction)
	}

	@ManagerModel.bound
	async getRequestShallow(order_detail_id: number, transaction: EntityManager) {
		return this.OrderDetailRepository.getRequestShallow(order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getRequest(order_detail_id: number, transaction: EntityManager) {
		return await this.OrderDetailRepository.getRequest(order_detail_id, transaction).then(request => {
			if (request.as_voucher) {
				if (request.type === ORDER_REQUESTS.VARIANT) {
					return {
						type: ORDER_DETAILS.VOUCHER,
						title: request.variant ? `${request.variant.product.brand.title} – ${request.variant.product.title}` : '',
						metadata: {
							address: CommonHelper.allowKey(request.address, 'title', 'receiver', 'phone', 'address', 'district', 'postal'),
							...request.metadata,
						},
					}
				} else if (request.type === ORDER_REQUESTS.MATCHBOX) {
					return {
						type: ORDER_DETAILS.VOUCHER,
						title: request.matchbox ? request.matchbox.title : '',
						metadata: {
							address: CommonHelper.allowKey(request.address, 'title', 'receiver', 'phone', 'address', 'district', 'postal'),
							...request.metadata,
						},
					}
				} else if (request.type === ORDER_REQUESTS.SERVICE) {
					return {
						type: ORDER_DETAILS.VOUCHER,
						title: request.service ? request.service.title : '',
						metadata: {
							address: CommonHelper.allowKey(request.address, 'title', 'receiver', 'phone', 'address', 'district', 'postal'),
							...request.metadata,
						},
					}
				} else if (request.type === ORDER_REQUESTS.STYLEBOARD) {
					return {
						type: ORDER_DETAILS.VOUCHER,
						title: request.styleboard ? request.styleboard.title : '',
						metadata: {
							address: CommonHelper.allowKey(request.address, 'title', 'receiver', 'phone', 'address', 'district', 'postal'),
							...request.metadata,
						},
					}
				} else if (request.type === ORDER_REQUESTS.TOPUP) {
					return {
						type: ORDER_DETAILS.VOUCHER,
						title: request.topup ? request.topup.title : '',
						metadata: {
							address: CommonHelper.allowKey(request.address, 'title', 'receiver', 'phone', 'address', 'district', 'postal'),
							...request.metadata,
						},
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Type as voucher unrecognized (${request.type}).`)
				}
			} else {
				if (request.type === ORDER_REQUESTS.MATCHBOX) {
					return {
						type: ORDER_DETAILS.STYLESHEET,
						title: '',
						metadata: request.metadata,
					}
				} else if (request.type === ORDER_REQUESTS.SERVICE) {
					return {
						type: ORDER_DETAILS.STYLEBOARD,
						title: '',
						metadata: request.metadata,
					}
				} else if (request.type === ORDER_REQUESTS.STYLEBOARD) {
					return {
						type: ORDER_DETAILS.STYLESHEET,
						title: '',
						metadata: request.metadata,
					}
				} else if (request.type === ORDER_REQUESTS.TOPUP) {
					return {
						type: ORDER_DETAILS.WALLET_TRANSACTION,
						title: '',
						metadata: request.metadata,
					}
				} else if (request.type === ORDER_REQUESTS.VARIANT) {
					return {
						type: ORDER_DETAILS.INVENTORY,
						title: '',
						metadata: request.metadata,
					}
				} else if (request.type === ORDER_REQUESTS.VOUCHER) {
					if (request.voucher) {
						if (request.voucher.type === VOUCHERS.INVENTORY) {
							return {
								type: ORDER_DETAILS.INVENTORY,
								title: '',
								metadata: request.metadata,
							}
						} else if (request.voucher.type === VOUCHERS.MATCHBOX) {
							return {
								type: ORDER_DETAILS.STYLESHEET,
								title: '',
								metadata: request.metadata,
							}
						} else if (request.voucher.type === VOUCHERS.SERVICE) {
							return {
								type: ORDER_DETAILS.STYLEBOARD,
								title: '',
								metadata: request.metadata,
							}
						} else if (request.voucher.type === VOUCHERS.TOPUP_AMOUNT) {
							return {
								type: ORDER_DETAILS.WALLET_TRANSACTION,
								title: '',
								metadata: request.metadata,
							}
						} else {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Voucher type unrecognized (${request.voucher.type}).`)
						}
					} else {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Voucher not found (${request.type}).`)
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Type unrecognized (${request.type}).`)
				}
			}
		})
	}

	@ManagerModel.bound
	async getSummary(order_detail_id: number, transaction: EntityManager) {
		return await this.OrderDetailRepository.getSummary(order_detail_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async processOrderDetail(
		changer_user_id: number,
		order_detail_id: number,
		note: string,
		transaction: EntityManager,
	) {
		const detail = await this.get([order_detail_id], { with_order: true }, transaction).then(od => od[0])

		switch (detail.order.status) {
			case ORDERS.PAID:
			case ORDERS.PAID_WITH_EXCEPTION:
				await this.OrderDetailRepository.update(changer_user_id, order_detail_id, {
					status: ORDER_DETAIL_STATUSES.PROCESSING,
				}, note, transaction).then(async isUpdated => {
					if (isUpdated) {
						await OrderManager.update(changer_user_id, detail.order.id, {
							status: ORDERS.PROCESSING,
						}, note, transaction)
					}
				})
				return true
			case ORDERS.PROCESSING:
				await this.OrderDetailRepository.update(changer_user_id, order_detail_id, {
					status: ORDER_DETAIL_STATUSES.PROCESSING,
				}, note, transaction)
				return true
			default:
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Cannot Process Order Detail')
		}
	}

	@ManagerModel.bound
	async primeOrderDetail(
		changer_user_id: number,
		order_detail_id: number,
		note: string,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.update(changer_user_id, order_detail_id, {
			status: ORDER_DETAIL_STATUSES.PRIMED,
		}, note, transaction).then(async isUpdated => {
			if (isUpdated) {
				const detail = await this.getDetailedShallow([order_detail_id], false, transaction).then(d => d[0])

				// update order status
				await OrderManager.update(changer_user_id, detail.order_id, {
					status: ORDERS.PROCESSING,
				}, note, transaction)

				// need to update its inventories to unavailable
				switch (detail.type) {
					case ORDER_DETAILS.STYLESHEET: {
						// Update only if detail have ref_id, otherwise, ignore
						if (detail.ref_id) {
							await MatchboxManager.stylesheet.packStylesheet(detail.ref_id, transaction)
							// Pack remaining inventories, update to unavailable
							await Promise.all(detail.stylesheet.stylesheetInventories.map(sI => {
								return InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.STYLESHEET, sI.inventory_id, INVENTORIES.UNAVAILABLE, detail.stylesheet.id, 'Inventory out (order detail primed).', true, transaction).catch(err => null)
							}))
						}
					} break
					case ORDER_DETAILS.INVENTORY: {
						if (isEmpty(detail.inventory)) {
							throw new ErrorModel(ERRORS.NODATA, ERROR_CODES.ERR_101, 'Please book inventory first')
						}

						await InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.ORDER_DETAIL, detail.inventory.id, INVENTORIES.UNAVAILABLE, detail.id, 'Inventory out (order detail primed).', true, transaction).catch(err => null)
					} break
					case ORDER_DETAILS.VOUCHER:
					default:
						break
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async resolveOrderDetail(
		changer_user_id: number,
		order_detail_id: number,
		cascade: boolean = false,
		note: string | null,
		transaction: EntityManager,
	) {
		return this.OrderDetailRepository.update(changer_user_id, order_detail_id, {
			status: ORDER_DETAIL_STATUSES.RESOLVED,
		}, note, transaction).then(async isUpdated => {
			if (isUpdated && cascade) {
				const order = await this.OrderDetailRepository.getOrderWithDetails('order_detail', order_detail_id, transaction)

				if (order.status === ORDERS.PROCESSING && order.orderDetails.findIndex(detail => detail.status !== ORDER_DETAIL_STATUSES.RESOLVED) === -1) {
					await OrderManager.update(changer_user_id, order.id, {
						status: ORDERS.RESOLVED,
					}, 'Auto update because order details are resolved.', transaction)
				}
			}


			return isUpdated
		})
	}

	@ManagerModel.bound
	async startOrderDetail(
		changer_user_id: number,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		order: OrderInterface,
		orderDetail: OrderDetailInterface & {
			request: OrderRequestInterface,
		},
		transaction: EntityManager,
	) {
		return Promise.resolve().then(async () => {
			if (orderDetail.type === ORDER_DETAILS.INVENTORY) {
				// ========================================================
				// 1. Check if order request a voucher, REDEEM
				// 2. Check booking status, if already released, try to
				//    capture again
				// 3. If cannot re-capture, set as exception
				// ========================================================
				if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.updateToRedeemed(changer_user_id, voucher.id, `Redeem voucher because of #OD-${orderDetail.id} order success.`, transaction)
				} else if (orderDetail.request.type !== ORDER_REQUESTS.VARIANT) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}

				const inventory = await InventoryManager.get(orderDetail.ref_id, transaction)
				const booking = await InventoryManager.getLastBooking(inventory.id, transaction)

				if (booking.source === BOOKING_SOURCES.ORDER_DETAIL && booking.ref_id === orderDetail.ref_id) {
					// ========================================================
					// Perfect condition, do nothing
					// ========================================================
					return {
						status: ORDER_DETAIL_STATUSES.PENDING,
						ref_id: inventory.id,
						note: null,
						slug: null,
					}
				} else if (booking.source === BOOKING_SOURCES.VOUCHER && orderDetail.request.type === ORDER_REQUESTS.VOUCHER && booking.ref_id === orderDetail.request.ref_id) {
					// ========================================================
					// Need to move booking source
					// ========================================================
					return InventoryManager.updateBookingSource(changer_user_id, 'VOUCHER_TO_ORDER', orderDetail.request.ref_id, orderDetail.id, transaction).then(inventoryId => {
						return {
							status: ORDER_DETAIL_STATUSES.PENDING,
							ref_id: inventoryId,
							note: null,
							slug: null,
						}
					}).catch(() => {
						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Voucher cannot be published. Fail updating booking source.')
					})
				} else {
					// ========================================================
					// Need to find replacement
					// ========================================================
					return this.bookOrFindReplacement(changer_user_id, orderDetail.ref_id, BOOKING_SOURCES.ORDER_DETAIL, orderDetail.id, `Booking from re-opened order detail #${orderDetail.id}.`, transaction).then(async isBooked => {
						if (isBooked) {
							return {
								status: ORDER_DETAIL_STATUSES.PENDING,
								ref_id: await this.getByIds([orderDetail.id], false, transaction).then(ods => ods[0].ref_id),
								note: null,
								slug: null,
							}
						} else {
							throw false
						}
					}).catch(() => {
						return {
							status: ORDER_DETAIL_STATUSES.EXCEPTION,
							ref_id: null,
							note: 'Cannot booking the inventory. Please resolve manually.',
							slug: null,
						}
					})
				}
			} else if (orderDetail.type === ORDER_DETAILS.STYLESHEET) {
				// ========================================================
				// 1. Check request, if voucher, fully REDEEM it
				// 2. Check reference, if exist and EXCEPTION,
				//    make it as PENDING
				// 3. If not exist, make one
				// ========================================================
				let voucher: VoucherInterface = null

				if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.updateToRedeemed(changer_user_id, voucher.id, `Redeem voucher because of #OD-${orderDetail.id} order success.`, transaction)
				} else if (orderDetail.request.type !== ORDER_REQUESTS.MATCHBOX && orderDetail.request.type !== ORDER_REQUESTS.STYLEBOARD) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}

				if (orderDetail.ref_id) {
					const stylesheet = await MatchboxManager.stylesheet.getShallow(orderDetail.ref_id, transaction)
					const matchbox = await MatchboxManager.get(stylesheet.matchbox_id, false, transaction)

					if (stylesheet.status === STYLESHEET_STATUSES.EXCEPTION) {
						await MatchboxManager.stylesheet.update(changer_user_id, orderDetail.ref_id, {
							status: STYLESHEET_STATUSES.PENDING,
						}, `Make stylesheet pending again as #OD-${orderDetail.id} order success`, undefined, transaction)

						stylesheet.status = STYLESHEET_STATUSES.PENDING
					}

					return {
						status: stylesheet.status === STYLESHEET_STATUSES.PENDING ? ORDER_DETAIL_STATUSES.PENDING : ORDER_DETAIL_STATUSES.PROCESSING,
						ref_id: stylesheet.id,
						note: 'Stylesheet already exist. Proceed with available stylesheet.',
						slug: matchbox.slug,
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.STYLEBOARD) {

					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_099, 'Styleboard is already deprecated.')

				} else {
					const matchbox = await MatchboxManager.get(orderDetail.request.type === ORDER_REQUESTS.VOUCHER ? voucher.ref_id : orderDetail.request.ref_id, false, transaction)
					const isUserHaveStylesheet = await MatchboxManager.stylesheet.isUserHaveAny(user.id, transaction)
					const metadata = (orderDetail.request.metadata as OrderRequestMatchboxMetadataInterface)
					// ========================================================
					// Get user's stylist Id or assign a new one
					// ========================================================
					const stylistId = user.profile.stylist_id || await UserManager.setStylist(user.id, false, transaction)
					let lineup = metadata.lineup ?? null

					if (lineup) {
						// ========================================================
						// Check length must match count
						// If not, nullify the lineup
						// ========================================================
						if (parseInt(matchbox.count, 10) !== lineup.split(' + ').length) {
							lineup = null
						}
					}

					return MatchboxManager.stylesheet.create({
						matchbox_id: matchbox.id,
						user_id: user.id,
						stylist_id: stylistId,
						type: isUserHaveStylesheet ? STYLESHEETS.REPEAT : STYLESHEETS.FIRST,
						status: STYLESHEET_STATUSES.PENDING,
						lineup,
						note: metadata.note,
						count: parseInt(matchbox.count, 10),
						real_value: matchbox.real_value,
						min_value: matchbox.min_value,
						value: matchbox.value,
					}, metadata.asset_ids, undefined, transaction).then(stylesheet => {
						return {
							status: ORDER_DETAIL_STATUSES.PENDING,
							ref_id: stylesheet.id,
							note: 'Stylesheet created.',
							slug: matchbox.slug,
						}
					})
				}
			} else if (orderDetail.type === ORDER_DETAILS.STYLEBOARD) {
				// ========================================================
				// 1. Check request, if voucher, fully REDEEM it
				// 2. Check detail type
				// 3. If styleboard, check reference, if exist & EXCEPTION,
				//    make it as PENDING, and check children stylesheets
				// 4. If children stylesheets EXCEPTION, make it PENDING
				// 5. If reference not exist, make one
				// 6. If service, do nothing
				// ========================================================
				let voucher: VoucherInterface = null

				if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.updateToRedeemed(changer_user_id, voucher.id, `Redeem voucher because of #OD-${orderDetail.id} order success.`, transaction)
				} else if (orderDetail.request.type !== ORDER_REQUESTS.SERVICE) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}

				if (orderDetail.ref_id) {
					const styleboard = await ServiceManager.styleboard.get(orderDetail.ref_id, { with_stylecard: true }, transaction)

					if (styleboard.status === STYLEBOARD_STATUSES.EXCEPTION) {
						await ServiceManager.styleboard.update(changer_user_id, styleboard.id, {
							status: STYLEBOARD_STATUSES.PENDING,
						}, `Make styleboard pending again as #OD-${orderDetail.id} order success`, transaction)

						await Promise.all((styleboard.stylecards ?? []).map(async stylecard => {
							if (stylecard.status === STYLECARD_STATUSES.EXCEPTION) {
								await ServiceManager.stylecard.update(changer_user_id, stylecard.id, {
									status: STYLECARD_STATUSES.PENDING,
								}, `Make stylecard pending again as #OD-${orderDetail.id} order success`, transaction)

								stylecard.status = STYLECARD_STATUSES.PENDING
							}
						}))

						styleboard.status = STYLEBOARD_STATUSES.PENDING
					}

					return {
						status: styleboard.status === STYLEBOARD_STATUSES.PENDING ? ORDER_DETAIL_STATUSES.PENDING : ORDER_DETAIL_STATUSES.PROCESSING,
						ref_id: styleboard.id,
						note: 'Styleboard already exist. Proceed with available styleboard.',
						slug: null,
					}
				} else {
					const service = await ServiceManager.get(orderDetail.request.type === ORDER_REQUESTS.VOUCHER ? voucher.ref_id : orderDetail.request.ref_id, false, transaction)
					const matchbox = await MatchboxManager.get(DEFAULTS.STYLING_MATCHBOX_ID, false, transaction)
					const metadata = orderDetail.request.metadata as OrderRequestServiceMetadataInterface
					// await this.createStylesheet(user, orderDetail, transaction)
					if (!user.profile.stylist_id) {
						user.profile.stylist_id = await UserManager.setStylist(user.id, false, transaction)
					}

					const stylecards: StylecardInterface[] = await Array(service.metadata.stylecard_count).fill(undefined).reduce((p, u) => {
						return p.then(async (arr: StylecardInterface[]) => {
							return [...arr, await ServiceManager.stylecard.create({
								matchbox_id: matchbox.id,
								stylist_id: user.profile.stylist_id,
								status: STYLECARD_STATUSES.PENDING,
								count: service.metadata.stylecard_item_count,
								note: metadata.note,
								is_master: false,
								is_bundle: false,
								is_public: false,
								inventory_only: false,
							}, metadata.asset_ids, undefined, transaction)]
						})
					}, Promise.resolve([]))

					const styleboard = await ServiceManager.styleboard.create({
						service_id: service.id,
						title: orderDetail.title,
						outfit: metadata.outfit,
						attire: metadata.attire,
						product: metadata.product,
						note: metadata.note,
						user_id: user.id,
						stylist_id: user.profile.stylist_id,
						status: STYLEBOARD_STATUSES.PENDING,
					}, stylecards.map(s => s.id), metadata.asset_ids, transaction)

					return {
						status: ORDER_DETAIL_STATUSES.PENDING,
						ref_id: styleboard.id,
						note: 'Styleboard created.',
						slug: null,
					}
				}
			} else if (orderDetail.type === ORDER_DETAILS.WALLET_TRANSACTION) {
				// ========================================================
				// 1. Check request, if voucher, fully REDEEM it
				// 2. Check for reference, if exist, check for balance
				//    if balanced, please adjust i.e. user cancel
				//    and then re-open the order
				// 3. If reference not exist, first create voucher
				// 4. REDEEM and create transaction
				// ========================================================
				let voucher: VoucherInterface = null

				if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.updateToRedeemed(changer_user_id, voucher.id, `Redeem voucher because of #OD-${orderDetail.id} order success.`, transaction)
				} else if (orderDetail.request.type !== ORDER_REQUESTS.TOPUP) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}

				if (orderDetail.ref_id) {
					const topupTransaction = await UserManager.wallet.getTransaction(orderDetail.ref_id, transaction)
					const transactionTransactions = await UserManager.wallet.getTransactionsOf(orderDetail.ref_id, transaction)

					const amount = (topupTransaction.type === STATEMENTS.CREDIT ? topupTransaction.amount : -topupTransaction.amount) + transactionTransactions.map(t => t.type === STATEMENTS.CREDIT ? t.amount : -t.amount).reduce(CommonHelper.sum, 0)

					if (amount === 0) {
						if (topupTransaction.type === STATEMENTS.CREDIT) {
							await UserManager.wallet.depositInto(topupTransaction.user_wallet_id, topupTransaction.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topupTransaction.id, 'Re-apply topup wallet transaction.', transaction)
						} else {
							await UserManager.wallet.withdrawFrom(topupTransaction.user_wallet_id, topupTransaction.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topupTransaction.id, 'Re-apply wallet deduction transaction.', transaction)
						}

						return {
							status: ORDER_DETAIL_STATUSES.RESOLVED,
							ref_id: topupTransaction.id,
							note: 'Transaction applied.',
							slug: null,
						}
					} else {
						return {
							status: ORDER_DETAIL_STATUSES.RESOLVED,
							ref_id: topupTransaction.id,
							note: null,
							slug: null,
						}
					}
				} else {
					// ========================================================
					// Create a voucher as a reference for transaction
					// Voucher is redeemed by default
					// ========================================================
					const topupAmount = orderDetail.request.type === ORDER_REQUESTS.VOUCHER
						? await TopupManager.getTopupAmount(voucher.ref_id, transaction)
						: await TopupManager.createAmount(orderDetail.request.ref_id, (orderDetail.request.metadata as OrderRequestTopupMetadataInterface).amount, transaction)

					const transactionVoucher = await VoucherManager.create({
						packet_id: await PacketManager.duplicate(DEFAULTS.DIGITAL_PACKET_ID, false, transaction),
						code: await VoucherManager.generateRandomVoucherCode(`YTWV-${TimeHelper.moment().format('YYMM[-]DDSSmmHH')}-`, 8),
						type: VOUCHERS.TOPUP_AMOUNT,
						ref_id: topupAmount.id,
						status: VOUCHER_STATUSES.REDEEMED,
						note: `Autogenerated voucher from topup request #OD-${orderDetail.id}`,
						is_physical: false,
						free_shipping: true,
						expired_at: new Date(),
					}, transaction)

					const wallet = await UserManager.wallet.getWallet(user.id, transaction)

					const topupTransaction = topupAmount.amount > 0
						? await UserManager.wallet.depositInto(wallet.id, Math.abs(topupAmount.amount), STATEMENT_SOURCES.VOUCHER, transactionVoucher.id, 'Topup transaction.', transaction)
						: await UserManager.wallet.withdrawFrom(wallet.id, Math.abs(topupAmount.amount), STATEMENT_SOURCES.VOUCHER, transactionVoucher.id, 'Topup transaction.', transaction)

					return {
						status: ORDER_DETAIL_STATUSES.RESOLVED,
						ref_id: topupTransaction.id,
						note: 'Transaction applied.',
						slug: null,
					}
				}
			} else if (orderDetail.type === ORDER_DETAILS.VOUCHER) {
				// ========================================================
				// Order detail voucher MUST HAVE REFERENCE
				// If don't, then throw error
				// 1. Check request, if INVENTORY OR VARIANT check validity
				// 2. If valid, publish voucher
				// 3. If not valid, try to recapture inventory / variant
				// 4. If MATCHBOX, publish voucher
				// 5. If SERVICE, publish voucher
				// 5. If TOPUP, publish voucher
				// 6. If Not Physical, send email and resolve order detail
				// ========================================================
				if (!orderDetail.ref_id) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Voucher missing.')
				}

				const voucher = await VoucherManager.get(orderDetail.ref_id, false, transaction)

				if (orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
					const inventory = await InventoryManager.get(voucher.ref_id, transaction)
					const booking = await InventoryManager.getLastBooking(inventory.id, transaction)

					if (booking.source === BOOKING_SOURCES.VOUCHER && booking.ref_id === voucher.id) {
						// ========================================================
						// Perfect condition, publish voucher
						// ========================================================
						await VoucherManager.publish(changer_user_id, voucher.id, 'Voucher published.', transaction)

						if (!voucher.is_physical) {
							// Method of delivery is email,
							// Send voucher to email
							await NotificationManager.sendGiftCardNotice(
								voucher.id,
								voucher.email,
								user.profile.first_name,
								orderDetail.title,
								voucher.code,
								voucher.note,
								`#${padStart(`${voucher.id}`, 5, '0')}`,
								TimeHelper.format(voucher.expired_at, 'DD/MM/YYYY'),
								`${process.env.MAIN_WEB_URL}profile/redeem`,
							)

							return {
								status: ORDER_DETAIL_STATUSES.RESOLVED,
								ref_id: voucher.id,
								note: null,
								slug: null,
							}
						} else {
							return {
								status: ORDER_DETAIL_STATUSES.PENDING,
								ref_id: voucher.id,
								note: null,
								slug: null,
							}
						}
					} else {
						// ========================================================
						// Need to find replacement
						// ========================================================
						return this.bookOrFindReplacement(changer_user_id, inventory.id, BOOKING_SOURCES.VOUCHER, voucher.id, `Booking from re-published voucher #${voucher.id}.`, transaction).then(async isBooked => {
							if (isBooked) {
								await VoucherManager.publish(changer_user_id, voucher.id, 'Voucher published.', transaction)

								if (!voucher.is_physical) {
									// Method of delivery is email,
									// Send voucher to email
									await NotificationManager.sendGiftCardNotice(
										voucher.id,
										voucher.email,
										user.profile.first_name,
										// We can reuse the same inventory to get the detail because the variant and product
										// will still probably the same
										orderDetail.title,
										voucher.code,
										voucher.note,
										`#${padStart(`${voucher.id}`, 5, '0')}`,
										TimeHelper.format(voucher.expired_at, 'DD/MM/YYYY'),
										`${process.env.MAIN_WEB_URL}profile/redeem`,
									)

									return {
										status: ORDER_DETAIL_STATUSES.RESOLVED,
										ref_id: voucher.id,
										note: null,
										slug: null,
									}
								} else {
									return {
										status: ORDER_DETAIL_STATUSES.PENDING,
										ref_id: voucher.id,
										note: null,
										slug: null,
									}
								}
							} else {
								throw false
							}
						}).catch(() => {
							return {
								status: ORDER_DETAIL_STATUSES.EXCEPTION,
								ref_id: voucher.id,
								note: 'Cannot booking the inventory into voucher. Please resolve manually by booking available inventory into voucher.',
								slug: null,
							}
						})
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.MATCHBOX) {
					const matchbox = await MatchboxManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.publish(changer_user_id, voucher.id, 'Voucher published.', transaction)

					if (!voucher.is_physical) {
						// Method of delivery is email,
						// Send voucher to email
						await NotificationManager.sendGiftCardNotice(
							voucher.id,
							voucher.email,
							user.profile.first_name,
							`${matchbox.title} Instant Matchbox`,
							voucher.code,
							voucher.note,
							`#${padStart(`${voucher.id}`, 5, '0')}`,
							TimeHelper.format(voucher.expired_at, 'DD/MM/YYYY'),
							`${process.env.MAIN_WEB_URL}profile/redeem`,
						)

						return {
							status: ORDER_DETAIL_STATUSES.RESOLVED,
							ref_id: voucher.id,
							note: null,
							slug: matchbox.slug,
						}
					} else {
						return {
							status: ORDER_DETAIL_STATUSES.PENDING,
							ref_id: voucher.id,
							note: null,
							slug: matchbox.slug,
						}
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.SERVICE) {
					const service = await ServiceManager.get(orderDetail.request.ref_id, false, transaction)

					await VoucherManager.publish(changer_user_id, voucher.id, 'Voucher published.', transaction)

					if (!voucher.is_physical) {
						// Method of delivery is email,
						// Send voucher to email
						await NotificationManager.sendGiftCardNotice(
							voucher.id,
							voucher.email,
							user.profile.first_name,
							`${service.title} Styling`,
							voucher.code,
							voucher.note,
							`#${padStart(`${voucher.id}`, 5, '0')}`,
							TimeHelper.format(voucher.expired_at, 'DD/MM/YYYY'),
							`${process.env.MAIN_WEB_URL}profile/redeem`,
						)

						return {
							status: ORDER_DETAIL_STATUSES.RESOLVED,
							ref_id: voucher.id,
							note: null,
							slug: null,
						}
					} else {
						return {
							status: ORDER_DETAIL_STATUSES.PENDING,
							ref_id: voucher.id,
							note: null,
							slug: null,
						}
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.TOPUP) {
					const topup = await TopupManager.getBy('id', orderDetail.request.ref_id, transaction)

					await VoucherManager.publish(changer_user_id, voucher.id, 'Voucher published.', transaction)

					if (!voucher.is_physical) {
						// Method of delivery is email,
						// Send voucher to email
						await NotificationManager.sendGiftCardNotice(
							voucher.id,
							voucher.email,
							user.profile.first_name,
							topup.title,
							voucher.code,
							voucher.note,
							`#${padStart(`${voucher.id}`, 5, '0')}`,
							TimeHelper.format(voucher.expired_at, 'DD/MM/YYYY'),
							`${process.env.MAIN_WEB_URL}profile/redeem`,
						)

						return {
							status: ORDER_DETAIL_STATUSES.RESOLVED,
							ref_id: voucher.id,
							note: null,
							slug: null,
						}
					} else {
						return {
							status: ORDER_DETAIL_STATUSES.PENDING,
							ref_id: voucher.id,
							note: null,
							slug: null,
						}
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail type invalid (${orderDetail.type}).`)
			}
		}).then(async ({
			ref_id,
			status,
			note,
			slug,
		}) => {
			return this.update(changer_user_id, orderDetail.id, {
				status,
				ref_id,
				shipment_at: await (async () => {
					switch (orderDetail.type) {
						case ORDER_DETAILS.INVENTORY: {
							// It is available!
							// return InventoryManager.shipmentDate.toDate()
							return orderDetail.shipment_at
						} case ORDER_DETAILS.STYLEBOARD: {
							return ServiceManager.shipmentDate.toDate()
						} case ORDER_DETAILS.STYLESHEET: {
							return MatchboxManager.shipmentDate(slug).toDate()
						} case ORDER_DETAILS.VOUCHER: {
							return VoucherManager.shipmentDate.toDate()
						} case ORDER_DETAILS.WALLET_TRANSACTION: {
							return VoucherManager.shipmentDate.toDate()
						} default:
							throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_103, 'Order detail shipment at not configured!')
					}
				})(),
			}, note, transaction)
		})
	}

	@ManagerModel.bound
	async exceptOrderDetail(
		changer_user_id: number,
		orderDetail: OrderDetailInterface & {
			request: OrderRequestInterface,
		},
		note: string | null,
		transaction: EntityManager,
	) {
		if (orderDetail.status === ORDER_DETAIL_STATUSES.PENDING) {
			if (orderDetail.type === ORDER_DETAILS.INVENTORY) {
				if (orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
					// ========================================================
					// Default: Order request is not a voucher, Cancel the booking, make the inventory available again
					// prestyle or styleboard case: order detail doesn't have ref_id
					// ========================================================
					if (orderDetail.ref_id) {
						const inventory = await InventoryManager.get(orderDetail.ref_id, transaction)
						await this.cancelBookingIfEligible(changer_user_id, inventory, BOOKING_SOURCES.ORDER_DETAIL, orderDetail.id, transaction).then(isReleased => {
							if (!isReleased) {
								throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Inventory cannot be released. Please release manually.')
							}
						})
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					// ========================================================
					// Order request is a voucher
					// 1. Move booking back to voucher
					// 2. Re-publish the voucher if can
					// ========================================================
					const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					await InventoryManager.updateBookingSource(changer_user_id, 'ORDER_TO_VOUCHER', orderDetail.id, voucher.id, transaction).then(isUpdated => {
						if (!isUpdated) {
							throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Voucher cannot be published. Fail updating booking source.')
						}
					})

					await this.republishIfEligible(changer_user_id, orderDetail.id, voucher, transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else if (orderDetail.type === ORDER_DETAILS.STYLESHEET) {
				if (orderDetail.request.type === ORDER_REQUESTS.MATCHBOX || orderDetail.request.type === ORDER_REQUESTS.STYLEBOARD) {
					// ========================================================
					// Order request is not a voucher
					// If have stylesheet, make the stylesheet exception
					// ========================================================
					if (orderDetail.ref_id) {
						await MatchboxManager.stylesheet.update(changer_user_id, orderDetail.ref_id, {
							status: STYLESHEET_STATUSES.EXCEPTION,
						}, `Stylesheet cancelled (#OD-${orderDetail.id} Exception).`, undefined, transaction)
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					// ========================================================
					// Order request is a voucher
					// 1. If have stylesheet, make the stylesheet exception
					// 2. Re-publish the voucher if can
					// ========================================================
					const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

					if (orderDetail.ref_id) {
						await MatchboxManager.stylesheet.update(changer_user_id, orderDetail.ref_id, {
							status: STYLESHEET_STATUSES.EXCEPTION,
						}, `Stylesheet cancelled (#OD-${orderDetail.id} Exception).`, undefined, transaction)
					}

					await this.republishIfEligible(changer_user_id, orderDetail.id, voucher, transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else if (orderDetail.type === ORDER_DETAILS.STYLEBOARD) {
				if (orderDetail.request.type === ORDER_REQUESTS.SERVICE) {
					// ========================================================
					// If have styleboard, make the styleboard exception
					// And all of its stylesheet exception
					// ========================================================
					if (orderDetail.ref_id) {
						const styleboard = await ServiceManager.styleboard.get(orderDetail.ref_id, { with_stylecard: true }, transaction)

						await ServiceManager.styleboard.update(changer_user_id, styleboard.id, {
							status: STYLEBOARD_STATUSES.EXCEPTION,
						}, `Stylesheet cancelled (#OD-${orderDetail.id} Exception).`, transaction)

						await Promise.all(styleboard.stylecards.map(async stylecard => {
							return ServiceManager.stylecard.update(changer_user_id, stylecard.id, {
								status: STYLECARD_STATUSES.EXCEPTION,
							}, `Stylesheet cancelled (#OD-${orderDetail.id} Exception).`, transaction)
						}))
					} else {
						// ========================================================
						// Do nothing since it don't have any reference
						// ========================================================
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else if (orderDetail.type === ORDER_DETAILS.WALLET_TRANSACTION) {
				if (orderDetail.request.type === ORDER_REQUESTS.TOPUP) {
					// ========================================================
					// Order request is topup
					// Do nothing since it won't have any reference
					// Why? Because order detail instantly resolved
					// So it shouldn't be cancellable
					// But just in case, let us handle this
					// ========================================================
					if (orderDetail.ref_id) {
						const topup = await UserManager.wallet.getTransaction(orderDetail.ref_id, transaction)
						const children = await UserManager.wallet.getTransactionsOf(orderDetail.ref_id, transaction).catch(err => [])
						const amount = (topup.type === STATEMENTS.CREDIT ? topup.amount : -topup.amount) + children.filter(c => c.status === WALLET_STATUSES.APPLIED).map(c => c.type === STATEMENTS.CREDIT ? c.amount : -c.amount).reduce(CommonHelper.sum, 0)

						if (amount !== 0) {
							// ========================================================
							// Only if credit or debit has already happened
							// Now we deduct / insert from wallet to make the amount back to zero
							// ========================================================
							if (topup.type === STATEMENTS.CREDIT) {
								// Inversed because we want to make things 0
								await UserManager.wallet.withdrawFrom(topup.user_wallet_id, topup.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topup.id, 'Order cancellation.', transaction)
							} else {
								// Inversed because we want to make things 0
								await UserManager.wallet.depositInto(topup.user_wallet_id, topup.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topup.id, 'Order cancellation.', transaction)
							}
						}
					} else {
						// ========================================================
						// Do nothing since it don't have any reference
						// ========================================================
					}
				} else if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
					// ========================================================
					// Order request is voucher
					// Make voucher available again
					// Do nothing since it won't have any reference
					// Why? Because order detail instantly resolved
					// So it shouldn't be cancellable
					// But just in case, let us handle this
					// ========================================================
					const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)
					await this.republishIfEligible(changer_user_id, orderDetail.id, voucher, transaction)

					if (orderDetail.ref_id) {
						const topup = await UserManager.wallet.getTransaction(orderDetail.ref_id, transaction)
						const children = await UserManager.wallet.getTransactionsOf(orderDetail.ref_id, transaction).catch(err => [])
						const amount = (topup.type === STATEMENTS.CREDIT ? topup.amount : -topup.amount) + children.filter(c => c.status === WALLET_STATUSES.APPLIED).map(c => c.type === STATEMENTS.CREDIT ? c.amount : -c.amount).reduce(CommonHelper.sum, 0)

						if (amount !== 0) {
							// ========================================================
							// Only if credit or debit has already happened
							// Now we deduct / insert from wallet to make the amount back to zero
							// ========================================================
							if (topup.type === STATEMENTS.CREDIT) {
								// Inversed because we want to make things 0
								await UserManager.wallet.withdrawFrom(topup.user_wallet_id, topup.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topup.id, 'Order cancellation.', transaction)
							} else {
								// Inversed because we want to make things 0
								await UserManager.wallet.depositInto(topup.user_wallet_id, topup.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, topup.id, 'Order cancellation.', transaction)
							}
						}
					} else {
						// ========================================================
						// Do nothing since it don't have any reference
						// ========================================================
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else if (orderDetail.type === ORDER_DETAILS.VOUCHER) {
				if (orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
					// ========================================================
					// Order request is inventory or variant
					// 1. Cancel the booking (in voucher) and make the
					//    inventory available again
					// 2. Make voucher exception
					// ========================================================
					const voucher = await VoucherManager.get(orderDetail.ref_id, false, transaction)
					const inventory = await InventoryManager.get(voucher.ref_id, transaction)

					await this.cancelBookingIfEligible(changer_user_id, inventory, BOOKING_SOURCES.VOUCHER, voucher.id, transaction).then(isReleased => {
						if (!isReleased) {
							throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Inventory cannot be released. Please release manually.')
						}
					})

					await VoucherManager.update(changer_user_id, voucher.id, {
						status: VOUCHER_STATUSES.EXCEPTION,
					}, `Invalidate voucher because of #OD-${orderDetail.id} exception.`, undefined, transaction)
				} else if (orderDetail.request.type === ORDER_REQUESTS.MATCHBOX) {
					// ========================================================
					// Order request is matchbox
					// Make voucher exception
					// ========================================================
					await VoucherManager.update(changer_user_id, orderDetail.ref_id, {
						status: VOUCHER_STATUSES.EXCEPTION,
					}, `Invalidate voucher because of #OD-${orderDetail.id} exception.`, undefined, transaction)
				} else if (orderDetail.request.type === ORDER_REQUESTS.SERVICE) {
					// ========================================================
					// Order request is service
					// Make voucher exception
					// ========================================================
					await VoucherManager.update(changer_user_id, orderDetail.ref_id, {
						status: VOUCHER_STATUSES.EXCEPTION,
					}, `Invalidate voucher because of #OD-${orderDetail.id} exception.`, undefined, transaction)
				} else if (orderDetail.request.type === ORDER_REQUESTS.TOPUP) {
					// ========================================================
					// Order request is topup
					// Make voucher exception, it shouldn't have any reference
					// because it is impossible to bypass the order detail
					// status checking in the first place
					// ========================================================
					await VoucherManager.update(changer_user_id, orderDetail.ref_id, {
						status: VOUCHER_STATUSES.EXCEPTION,
					}, `Invalidate voucher because of #OD-${orderDetail.id} exception.`, undefined, transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type} -> ${orderDetail.type}).`)
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail type invalid (${orderDetail.type}).`)
			}
		} else if (orderDetail.status === ORDER_DETAIL_STATUSES.EXCEPTION) {
			// Already exceptioned
			return true
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail status invalid (${orderDetail.status}), you cannot cancel an ongoing order.`)
		}

		return this.update(changer_user_id, orderDetail.id, {
			status: ORDER_DETAIL_STATUSES.EXCEPTION,
		}, note, transaction)
	}

	@ManagerModel.bound
	async refundOrderDetail(
		changer_user_id: number,
		order_detail_id: number,
		note: string,
		transaction: EntityManager,
	) {
		const orderDetail = await this.get([order_detail_id], { with_request: true, with_order: true, with_shipment: true }, transaction).then(od => od[0])

		if (orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
			// except order detail
			await this.exceptOrderDetail(changer_user_id, orderDetail, note, transaction)

			// process refund
			await OrderManager.utilities.refundProcessor(
				orderDetail.order.user_id,
				orderDetail.order.id,
				orderDetail.request.ref_id,
				[],
				true,
				transaction,
			)

			return true
		}

		return false
	}

	async bookOrFindReplacement(
		changer_user_id: number,
		inventory_id: number | null,
		source: BOOKING_SOURCES.ORDER_DETAIL | BOOKING_SOURCES.VOUCHER | BOOKING_SOURCES.STYLESHEET,
		source_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		if (inventory_id === null) {
			return false
		}

		const inventory = await InventoryManager.get(inventory_id, transaction)

		return Promise.resolve().then(async () => {
			if (inventory.status === INVENTORIES.BOOKED) {
				const booking = await InventoryManager.getLastBooking(inventory.id, transaction)

				if (booking.source === source && booking.ref_id === source_id) {
					// We are good to go
					return true
				} else {
					// It's being booked by somebody else, try to book another
					return inventory.variant_id
				}
			} else if (inventory.status === INVENTORIES.AVAILABLE) {
				// Booking already released
				await InventoryManager.book(changer_user_id, inventory.id, source, source_id, note, true, transaction).then(isBooked => {
					return isBooked || inventory.variant_id
				}).catch(() => {
					// Booking failed. Race condition. Try to book another
					return inventory.variant_id
				})
			} else {
				// Inventory not available. Try to book another
				return inventory.variant_id
			}
		}).then(goodToGoOrVariantId => {
			if (goodToGoOrVariantId === true) {
				return true
			}

			// Inventory not available, try to book another
			return InventoryManager.getFromVariant(goodToGoOrVariantId, 1, false, true, transaction).then(inventories => {
				return InventoryManager.book(changer_user_id, inventories[0].id, source, source_id, note, true, transaction).then(isBooked => {
					if (isBooked) {
						// Update reference
						if (source === BOOKING_SOURCES.ORDER_DETAIL) {
							return this.OrderDetailRepository.update(changer_user_id, source_id, {
								ref_id: inventories[0].id,
							}, `Change reference id to #${inventories[0].id}`, transaction)
						} else if (source === BOOKING_SOURCES.VOUCHER) {
							return VoucherManager.update(changer_user_id, source_id, {
								ref_id: inventories[0].id,
							}, `Change reference id to #${inventories[0].id}`, undefined, transaction)
						} else if (source === BOOKING_SOURCES.STYLESHEET) {
							return isBooked
						}
					}
				})
			}).catch(() => {
				return false
			})
		})
	}
	// ============================ PRIVATES ============================
	private async getOrderDetail({
		as_voucher,
		is_facade,
		order_id,
		order_request_id,
		title,
		description,
		price,
		retail_price,
		handling_fee,
		is_exchange,
		type,
		status,
		item,
	}: {
		as_voucher: boolean,
		is_facade: boolean,
		order_id: number,
		order_request_id: number,
		title: string,
		description: string,
		price: number,
		retail_price: number,
		handling_fee: number,
		is_exchange: boolean,
		type: ORDER_DETAILS,
		status: ORDER_DETAIL_STATUSES,
		item: {
			id: number,
			packet_id: number,
			shipment_at: Date,
			is_physical: boolean,
		},
	}, transaction: EntityManager): Promise<Parameter<OrderDetailInterface>> {
		return {
			order_id,
			order_request_id,
			description,
			price,
			retail_price,
			handling_fee,
			is_exchange,
			status,
			metadata: {},
			is_digital: !item.is_physical,
			packet_id: await PacketManager.duplicate(item.packet_id, is_facade, transaction),
			ref_id: item.id,
			shipment_at: item.shipment_at,
			...(as_voucher ? {
				title: `${title} Gift Card`,
				type: ORDER_DETAILS.VOUCHER,
			} : {
				title,
				type,
			}),
		}
	}

	private async getVoucherFrom(
		request: OrderRequestInterface,
		metadata: OrderRequestVoucherMetadataInterface,
		inventoryOrMatchboxOrServiceOrTopup: InventoryInterface | MatchboxInterface | ServiceInterface | TopupInterface,
		is_facade: boolean | undefined,
		transaction: EntityManager,
	): Promise<VoucherInterface> {

		let packetId = metadata.is_physical ? DEFAULTS.VOUCHER_PACKET_ID : DEFAULTS.DIGITAL_PACKET_ID

		if (request.type === ORDER_REQUESTS.MATCHBOX && (request.id === 10 || request.id === 11)) {
			packetId = DEFAULTS.CANDLE_PACKET_ID
		}

		let code: string = null

		if (request.type === ORDER_REQUESTS.TOPUP) {
			code = await PromoManager.getAvailableRandomVoucherCode('', 8, transaction)
			code = `YVWT-${TimeHelper.moment().format('YYMM')}-${code.substring(0, 4)}-${TimeHelper.moment().format('HHDD')}-${code.substring(4)}`
		} else {
			code = await PromoManager.getAvailableRandomVoucherCode('YV', 8, transaction)
		}

		const detail = {
			packet_id: packetId,
			// code: request.type === ORDER_REQUESTS.TOPUP ? await VoucherManager.generateRandomVoucherCode(`YTWV-${ TimeHelper.moment().format('YYMM[-]DDSSmmHH') }-`, 8) : await PromoManager.getAvailableRandomVoucherCode('YV', 8, transaction),
			code,
			type: this.getVoucherTypeFromRequest(request),
			ref_id: inventoryOrMatchboxOrServiceOrTopup.id,
			status: VOUCHER_STATUSES.UNPUBLISHED,
			note: metadata.note,
			name: metadata.name,
			email: metadata.email,
			free_shipping: false,
			is_physical: metadata.is_physical,
			expired_at: TimeHelper.moment().add(3, 'months').add(1, 'd').startOf('day').toDate(),
		}

		if (is_facade) {
			return {
				id: Date.now(),
				created_at: new Date(),
				...detail,
			}
		} else {
			return VoucherManager.create({
				...detail,
				packet_id: await PacketManager.duplicate(detail.packet_id, is_facade, transaction),
			}, transaction)
		}
	}

	private getPacketId(defaultPacketId: number, as_voucher: boolean, is_physical: boolean) {
		return as_voucher
			? is_physical
				? DEFAULTS.VOUCHER_PACKET_ID
				: DEFAULTS.DIGITAL_PACKET_ID
			: defaultPacketId
	}

	private getVoucherTypeFromRequest(request: OrderRequestInterface) {
		switch (request.type) {
			case ORDER_REQUESTS.MATCHBOX:
				return VOUCHERS.MATCHBOX
			case ORDER_REQUESTS.VARIANT:
				return VOUCHERS.INVENTORY
			case ORDER_REQUESTS.SERVICE:
				return VOUCHERS.SERVICE
			case ORDER_REQUESTS.TOPUP:
				return VOUCHERS.TOPUP_AMOUNT
			case ORDER_REQUESTS.VOUCHER:
			default:
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request type invalid.')
		}
	}

	private async cancelBookingIfEligible(
		changer_user_id: number,
		inventory: InventoryInterface,
		source: BOOKING_SOURCES.ORDER_DETAIL | BOOKING_SOURCES.VOUCHER,
		source_id: number,
		transaction: EntityManager,
	) {
		if (inventory.status === INVENTORIES.BOOKED) {
			// masih di book
			const booking = await InventoryManager.getLastBooking(inventory.id, transaction)

			if (booking.source === source && booking.ref_id === source_id) {
				return InventoryManager.cancelBooking(changer_user_id, source, inventory.id, INVENTORIES.AVAILABLE, source_id, 'Booking cancelled.', true, transaction).then(isCancelled => {
					if (isCancelled) {
						return true
					}
					return new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Booking cannot be cancelled.')
				}).catch(err => {
					return new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, `Booking cannot be cancelled.\n${err instanceof ErrorModel ? `${err.message}: ${err.detail}` : `${(err as Error).name}: ${(err as Error).message}`}`)
				})
			} else {
				// being booked by somebody else
				return true
			}
		} else if (inventory.status === INVENTORIES.UNAVAILABLE) {
			// kemungkinan sudah di box
			return new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Booking cannot be cancelled. Inventory already unavailable. (Probably inventory already processed by fulfillment)')
		} else {
			return true
		}
	}

	private async republishIfEligible(
		changer_user_id: number,
		order_detail_id: number,
		voucher: VoucherInterface,
		transaction: EntityManager,
	) {
		if (voucher.status === VOUCHER_STATUSES.REDEEMED || voucher.status === VOUCHER_STATUSES.REDEEM_PENDING) {
			return await VoucherManager.publish(changer_user_id, voucher.id, `Voucher made available again (#OD-${order_detail_id} Exception).`, transaction).then(async isPublished => {
				if (!isPublished) {
					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Voucher cannot be published. Fail publishing voucher.')
				}
			})
		} else {
			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, `Voucher status invalid (${voucher.status}). Voucher cannot be published.`)
		}
	}
}

export default new OrderDetailManager()
