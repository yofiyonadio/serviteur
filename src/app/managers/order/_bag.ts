import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	OrderBagRepository,
} from 'app/repositories'

import VoucherManager from '../voucher'
import ServiceManager from '../service'

import ErrorModel, { ERRORS } from 'app/models/error'
import { ERROR_CODES } from 'app/models/error'
import { ORDER_REQUESTS, VOUCHERS } from 'energie/utils/constants/enum'
import {
	OrderRequestMatchboxMetadataInterface,
	OrderRequestMetadataInterface,
	OrderRequestServiceMetadataInterface,
	OrderRequestStyleboardMetadataInterface,
	OrderRequestTopupMetadataInterface,
	OrderRequestVariantMetadataInterface,
	OrderRequestVoucherMetadataInterface,
	OrderRequestVoucherTopupMetadataInterface,
} from 'energie/app/interfaces/order.request'
import { isEmpty } from 'lodash'
import { BagItemInterface, VoucherInterface } from 'energie/app/interfaces'
import { check } from 'app/interfaces/guard'
import { CommonHelper } from 'utils/helpers'
// import EmailNotificationManager from 'app/managers/notification/_email'


class OrderBagManager extends ManagerModel {
	static __displayName = 'OrderBagManager'

	protected OrderBagRepository: OrderBagRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderBagRepository,
		])
	}


	// ============================= INSERT =============================
	@ManagerModel.bound
	async flushBag(
		user_id: number,
		items: Array<{
			type: ORDER_REQUESTS,
			ref_id: number | string,
			quantity: number,
			as_voucher?: boolean,
			metadata?: OrderRequestMetadataInterface,
		}>,
		transaction: EntityManager,
	) {
		// get user bag with its item
		return this.OrderBagRepository.getUserBag(user_id, { get_many: false, with_items: true }, {}, transaction)
			.then(async bag => {
				return Promise.all(items.map(async ({ type, ref_id, quantity, as_voucher, metadata }) => {
					let bagItem: BagItemInterface

					// special case for voucher
					if (type === ORDER_REQUESTS.VOUCHER && typeof ref_id === 'string') {
						const voucher = await VoucherManager.getByCode(ref_id, true, transaction)
						// find item in bag
						bagItem = bag.items.find(item => item.ref_id === voucher.id && item.type === type && !item.deleted_at && !+item.ordered_at)
					} else {
						// find item in bag
						bagItem = bag.items.find(item => item.ref_id === ref_id && item.type === type && !item.deleted_at && !+item.ordered_at)
					}

					if (!isEmpty(bagItem)) {
						return this.OrderBagRepository.orderItem(bag.id, bagItem.ref_id, type, transaction)
					} else {
						return true
					}
				}))
			})
			.catch(error => {
				if (error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101)) {
					// if not found assume, they order not from bag
					return true
				}

				throw error
			})

	}

	@ManagerModel.bound
	async addItems(
		user_id: number,
		items: Array<{
			type: ORDER_REQUESTS,
			ref_id: number | string,
			quantity: number,
			as_voucher?: boolean,
			metadata?: OrderRequestMetadataInterface,
		}>,
		transaction: EntityManager,
	) {
		// map item first to parser voucher code to id
		const __items__ = await items.reduce(async (promise, item) => {
			return promise.then(async i => {
				if (item.type === ORDER_REQUESTS.VOUCHER && typeof item.ref_id === 'string') {
					const voucher = await VoucherManager.getByCode(item.ref_id, true, transaction)

					return i.concat({
						type: item.type,
						ref_id: voucher.id,
						quantity: item.quantity,
						as_voucher: item.as_voucher,
						metadata: await this.trimMetadata(item.as_voucher, item.type, item.ref_id, item.metadata, transaction),
					})
				} else if (typeof item.ref_id === 'number') {

					return i.concat({
						type: item.type,
						ref_id: item.ref_id,
						quantity: item.quantity,
						as_voucher: item.as_voucher,
						metadata: await this.trimMetadata(item.as_voucher, item.type, item.ref_id, item.metadata, transaction),
					})
				}
			})
		}, Promise.resolve([] as Array<{
			type: ORDER_REQUESTS,
			ref_id: number,
			quantity: number,
			as_voucher?: boolean,
			metadata?: OrderRequestMetadataInterface,
		}>))


		// check if this user has bag, if not exists create one
		const bag = await this.OrderBagRepository.getUserBag(user_id, { get_many: false }, {}, transaction)
			.catch(error => {
				if (error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101)) {
					// creating bag
					return this.OrderBagRepository.insert({
						user_id,
					}, transaction)
				}

				throw error
			})

		// add item to bag
		await this.OrderBagRepository.insertItems(__items__.map(item => {
			return {
				bag_id: bag.id,
				type: item.type,
				ref_id: item.ref_id,
				as_voucher: item.as_voucher,
				quantity: item.quantity,
				metadata: item.metadata,
			}
		}), transaction)

		return true
	}

	// ============================= UPDATE =============================
	async updateQuantityOrMetadata(
		user_id: number,
		bag_item_id: number,
		data: {
			quantity?: number,
			metadata?: OrderRequestMetadataInterface,
		},
		transaction: EntityManager,
	) {
		// get bag if exists, if not throw error
		const bag = await this.OrderBagRepository.getUserBag(user_id, {
			get_many: false,
			with_items: true,
		}, {}, transaction)
			.catch(error => {
				throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You don\'t have any bag')
			})

		const bagItem: BagItemInterface = bag.items.find(item => item.id === bag_item_id && item.bag_id === bag.id && !+item.deleted_at && !+item.ordered_at)

		if (!isEmpty(bagItem)) {
			if (bagItem.type === ORDER_REQUESTS.VOUCHER && !!data.quantity) {
				throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You can\'t add quantity from voucher')
			} else if (bagItem.type === ORDER_REQUESTS.VOUCHER && !isEmpty(data.metadata)) {
				const voucher = await VoucherManager.get(bagItem.ref_id, true, transaction)
				const metadata = await this.trimMetadata(false, bagItem.type, voucher, data.metadata, transaction)

				return this.OrderBagRepository.updateItem(bagItem.id, bagItem.bag_id, { metadata }, transaction)
			} else {
				if (!!data.quantity) {
					return this.OrderBagRepository.updateItem(bagItem.id, bagItem.bag_id, { quantity: data.quantity }, transaction)
				} else if (!isEmpty(data.metadata)) {
					const metadata = await this.trimMetadata(bagItem.as_voucher, bagItem.type, bagItem.ref_id, data.metadata, transaction)

					return this.OrderBagRepository.updateItem(bagItem.id, bagItem.bag_id, { metadata }, transaction)
				} else {
					throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You can\'t update other than quantity or metadata')
				}
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You don\'t have this item in bag')
		}
	}

	// ============================= GETTER =============================
	get getUserBag() {
		return this.OrderBagRepository.getUserBag
	}

	// async getBagAbandonment() {
	// 	return this.transaction(async transaction => {
	// 		return this.OrderBagRepository.getBagAbandonment(transaction)
	// 			.then(res => {
	// 				return EmailNotificationManager.sendBagAbandonment(res, transaction)
	// 			})
	// 	})
	// }

	// ============================= DELETE =============================
	async deleteItem(
		user_id: number,
		bag_item_id: number,
		transaction: EntityManager,
	) {
		// get bag if exists, if not throw error
		const bag = await this.OrderBagRepository.getUserBag(user_id, {
			get_many: false,
			with_items: true,
		}, {}, transaction)
			.catch(error => {
				throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You don\'t have any bag')
			})

		const bagItem = bag.items.find(item => item.id === bag_item_id && item.bag_id === bag.id && !+item.deleted_at && !+item.ordered_at)

		if (!isEmpty(bagItem)) {
			return this.OrderBagRepository.deleteItem(bagItem.id, bagItem.bag_id, transaction)
		} else {
			throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'You don\'t have this item in bag')
		}
	}

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async trimMetadata(as_voucher: boolean, type: ORDER_REQUESTS, voucherOrIdOrSlug: VoucherInterface | number | string, metadata: OrderRequestMetadataInterface, transaction: EntityManager) {
		// Validate metadata first
		if (as_voucher === true) {
			if (type === ORDER_REQUESTS.TOPUP) {
				if (check(metadata, ['object', {
					name: 'string',
					email: 'string',
					note: 'string',
					amount: 'number',
					code: 'string?',
					is_physical: 'boolean?',
					merchant: 'string?',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVoucherTopupMetadataInterface, 'amount', 'code', 'email', 'is_physical', 'merchant', 'name', 'note')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for VOUCHER TOPUP not fulfilled.')
				}
			} else {
				if (check(metadata, ['object', {
					name: 'string',
					email: 'string',
					note: 'string',
					code: 'string?',
					is_physical: 'boolean?',
					merchant: 'string?',
					address: ['object?', {
						location_id: 'number',
						address: 'string',
						postal: 'string',
						title: 'string?',
						receiver: 'string?',
						phone: 'string?',
						district: 'string?',
						metadata: ['object?', {
							tikiAreaId: 'number?',
							manualAreaId: 'number?',
							sicepatAreaId: 'number?',
						}],
					}],
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVoucherMetadataInterface, 'code', 'email', 'is_physical', 'merchant', 'name', 'note', 'address')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for VOUCHER not fulfilled.')
				}
			}
		} else {
			if (type === ORDER_REQUESTS.VARIANT) {
				if (check(metadata, ['object', {
					styleboard_id: 'number?',
					stylecard_id: 'number?',
					note: 'string?',
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					device: 'string?',
					ref: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?',
					created_at: 'number?'
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVariantMetadataInterface, 'styleboard_id', 'stylecard_id', 'note', 'merchant', 'source', 'medium', 'ref', 'utm_source', 'utm_medium', 'utm_campaign', 'device', 'created_at')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for INVENTORY/VARIANT not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.MATCHBOX) {
				if (check(metadata, ['object', {
					lineup: 'string?',
					note: 'string?',
					asset_ids: ['array?', 'number'],
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					ref: 'string?',
					device: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestMatchboxMetadataInterface, 'asset_ids', 'lineup', 'note', 'merchant', 'source', 'medium', 'ref', 'utm_source', 'utm_medium', 'utm_campaign', 'device')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for MATCHBOX not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.SERVICE) {
				if (check(metadata, ['object', {
					outfit: 'string',
					attire: 'string',
					product: 'string?',
					note: 'string?',
					asset_ids: ['array?', 'number'],
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					ref: 'string?',
					device: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestServiceMetadataInterface, 'attire', 'note', 'outfit', 'product', 'asset_ids', 'merchant', 'source', 'medium', 'ref', 'utm_source', 'utm_medium', 'utm_campaign', 'device')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for SERVICE not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.STYLEBOARD) {
				if (check(metadata, ['object', {
					stylecard_variant_ids: ['array', 'number'],
				}])) {
					return CommonHelper.allowKey({
						...metadata,
						...await Promise.resolve().then(() => {
							if (voucherOrIdOrSlug === null) {
								return {}
							} else if (typeof voucherOrIdOrSlug === 'number') {
								// So that there is continuity with its request
								return ServiceManager.styleboard.get(voucherOrIdOrSlug, { with_request: true }, transaction).then(styleboard => {
									return styleboard.request.metadata
								})
							} else {
								return {}
							}
						}),
					} as OrderRequestStyleboardMetadataInterface, 'stylecard_variant_ids', 'attire', 'note', 'outfit', 'product', 'merchant', 'source', 'medium', 'ref', 'utm_source', 'utm_medium', 'utm_campaign', 'device')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for STYLEBOARD not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.TOPUP) {
				if (check(metadata, ['object', {
					amount: 'number',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestTopupMetadataInterface, 'amount')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for TOPUP not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.VOUCHER) {
				if (voucherOrIdOrSlug === null || typeof voucherOrIdOrSlug === 'number' || typeof voucherOrIdOrSlug === 'string') {
					return {}
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.INVENTORY) {
					return this.trimMetadata(false, ORDER_REQUESTS.VARIANT, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.MATCHBOX) {
					return this.trimMetadata(false, ORDER_REQUESTS.MATCHBOX, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.SERVICE) {
					return this.trimMetadata(false, ORDER_REQUESTS.SERVICE, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.TOPUP_AMOUNT) {
					return {}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Voucher type not yet configured.')
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Request type not yet configured.')
			}
		}
	}
}

export default new OrderBagManager()
