import {
	ManagerModel, ErrorModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import OrderUtilitiesManager from './_utilities'

import {
	OrderRepository,
	OrderPaymentRepository,
} from 'app/repositories'

import {
	MidtransHandler,
	FaspayHandler,
} from 'app/handlers'

import BCAManager from '../third.party/bca'
import XenditHandler, { VA_BANK, EWALLET, RETAIL_OUTLET } from 'app/handlers/xendit'
import IndodanaService from '../../services/indodana'
import PaymentInterface from 'energie/app/interfaces/order.payment'

import {
	NotificationParameterInterface,
	// TransactionParameterInterface,
} from 'app/interfaces/midtrans'

import {
	PaymentNotificationInterface, CreditReturnResponseInterface,
} from 'app/interfaces/faspay'

import {
	TimeHelper,
	CommonHelper,
} from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { PAYMENTS, ORDERS, PAYMENT_AGENTS, PAYMENT_METHOD } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { OrderAddressInterface, OrderDetailInterface, OrderPaymentInterface, UserInterface, UserProfileInterface } from 'energie/app/interfaces'
import { Parameter } from 'types/common'
import OrderInterface, { OrderMetadataInterface } from 'energie/app/interfaces/order'
import { BrandManager } from '..'


class OrderPaymentManager extends ManagerModel {

	static __displayName = 'OrderPaymentManager'

	protected OrderRepository: OrderRepository
	protected OrderPaymentRepository: OrderPaymentRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
			OrderPaymentRepository,
		], async () => {
			await OrderUtilitiesManager.initialize(connection)
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		order_id: number,
		code: PAYMENTS,
		agent: PAYMENT_AGENTS,
		method: string | undefined,
		metadata: object | undefined, // why object? because we dont know how 3rd party services will provide data, but can sih
		transaction: EntityManager,
	) {
		return this.OrderPaymentRepository.insert(order_id, code, agent, method, metadata, transaction)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateActivePayment(
		order_id: number,
		data: Partial<Parameter<OrderPaymentInterface>>,
		transaction: EntityManager,
	) {
		return this.OrderPaymentRepository.updateActivePayment(order_id, CommonHelper.stripUndefined(data), transaction)
			.then(() => {
				return this.OrderPaymentRepository.getPaymentWithStatus(order_id, PAYMENTS.SUCCESS, transaction)
			})
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getLatestPendingPayment(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderPaymentRepository.getPaymentWithStatus(order_id, PAYMENTS.PENDING, transaction)
	}

	@ManagerModel.bound
	async getPaymentCode(
		payment_code: string,
		transaction: EntityManager,
	) {
		return this.OrderPaymentRepository.getPaymentCode(payment_code, transaction)
	}

	@ManagerModel.bound
	async getPaymentMethods(transaction: EntityManager) {
		return this.OrderPaymentRepository.getPaymentMethods(transaction)
	}

	@ManagerModel.bound
	async getPaymentDetail(
		order_id: number,
		transaction: EntityManager,
	) {
		const order = await this.OrderRepository.getDetail(order_id, { }, transaction)
		const price = OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

		if(order.status !== ORDERS.PENDING_PAYMENT) {
			const payment = await this.OrderPaymentRepository.getPaymentWithStatus(order.id, PAYMENTS.SUCCESS, transaction).catch(() => null)
			const method = payment ? payment.agent === PAYMENT_AGENTS.FASPAY ? ['FASPAY VIRTUAL ACCOUNT', 'BCA'] : payment.agent === PAYMENT_AGENTS.MANUAL ? ['FREE'] : payment.method.split(' ') : ['EXCEPTION']

			return {
				total: price.total,
				method: method[0],
				order_number: order.number,
				code: method[1],
				expired_at: null,
				status: payment?.code,
			}
		} else if(order.status === ORDERS.PENDING_PAYMENT) {
			const payment = await this.OrderPaymentRepository.getPaymentWithStatus(order.id, PAYMENTS.PENDING, transaction)
				.then(detail => {
					if(detail.method) {
						const payment_detail = detail.method.split(' ')
	
						return {
							method: payment_detail[0],
							code: payment_detail[1],
							prefix: (detail.metadata as any)?.prefix,
						}
					}

					return {
						method: 'Manual',
						code: 'BCA',
						prefix: 'Yuna & Co',
					}
				})
				.catch(error => {
					if(error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101) && order.metadata.faspay_payment_link) {
						return {
							method: 'FASPAY VIRTUAL ACCOUNT',
							code: 'BCA',
							prefix: null,
						}
					}

					throw error
				})

				return {
					total: price.total,
					method: payment.method,
					order_number: order.number,
					code: payment.code,
					prefix: payment.prefix,
					...(order.metadata as Omit<OrderMetadataInterface, 'injector_id' | 'payment_id'>),
					expired_at: TimeHelper.moment(order.created_at).add(6, 'hour').toDate(),
					status: PAYMENTS.PENDING,
				}
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async initiateMidtransTransaction(
		order_number: string,
		transaction: EntityManager,
		// parameter: TransactionParameterInterface,
	) {
		const order = await this.OrderRepository.getDetail(order_number, { shipment: true }, transaction)

		if (order.status !== ORDERS.PENDING_PAYMENT) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, order.status)
		}

		const {
			// subtotal,
			// shipping,
			// discount,
			// wallet,
			roundup,
			handling,
			total,
		} = OrderUtilitiesManager.getPrices(
			order.orderDetails,
			order.shipments,
			order.points,
			order.wallets,
		)

		return MidtransHandler.transaction({
			transaction_details: {
				order_id: order.number,
				gross_amount: total,
			},
			item_details: [
				...order.orderDetails.map(orderDetail => {
					return {
						id: `${ orderDetail.id }`,
						name: orderDetail.title.substring(0, 50),
						price: orderDetail.price,
						quantity: 1,
					}
				}),
			].concat(handling > 0 ? [{
				id: `handling_${order.id}`,
				name: 'Styling Fee',
				price: handling,
				quantity: 1,
			}] : []).concat(roundup > 0 ? [{
				id: `roundup_${order.id}`,
				name: 'Round Up',
				price: roundup,
				quantity: 1,
			}] : []).concat(order.shipments.map(shipment => {
				return {
					id: `shipment_${ shipment.id }`,
					name: `${ shipment.courier } – ${ shipment.service }`,
					price: shipment.amount,
					quantity: 1,
				}
			})).concat(order.campaigns.map(campaign => {
				return {
					id: `campaign_${ campaign.id }`,
					name: campaign.title,
					price: - [].concat(...order.orderDetails.map(oD => {
						return oD.orderDetailCampaigns.filter(oDC => oDC.campaign_id === campaign.id).map(oDC => oDC.discount)
					})).concat(...order.shipments.map(oD => {
						return oD.shipmentCampaigns.filter(oDC => oDC.campaign_id === campaign.id).map(oDC => oDC.discount)
					})).reduce(CommonHelper.sum, 0),
					quantity: 1,
				}
			})).concat(order.coupons.map(coupon => {
				return {
					id: `coupon_${ coupon.id }`,
					name: `Coupon - ${ coupon.title }`,
					price: - [].concat(...order.orderDetails.map(oD => {
						return oD.orderDetailCoupons.filter(oDC => oDC.coupon_id === coupon.id).map(oDC => oDC.discount)
					})).concat(...order.shipments.map(oD => {
						return oD.shipmentCoupons.filter(oDC => oDC.coupon_id === coupon.id).map(oDC => oDC.discount)
					})).reduce(CommonHelper.sum, 0),
					quantity: 1,
				}
			})).concat(order.wallets.map(wallet => {
				return {
					id: `wallet_${ wallet.id }`,
					name: `User Wallet`,
					price: - wallet.amount,
					quantity: 1,
				}
			})),
			customer_details: {
				first_name: order.user.profile.first_name,
				last_name: order.user.profile.last_name,
				email: order.user.email,
			},
			callbacks: {
				finish: process.env.CHECKOUT_URL + '?agent=midtrans',
			},
			expiry: {
				start_time: TimeHelper.moment(order.created_at).format('YYYY-MM-DD HH:mm:ss Z'),
				duration: 6,
				unit: 'hour',
			},
		})
	}

	@ManagerModel.bound
	async getMidtransPaymentLink(
		order_number: string,
	) {
		return `${process.env.CONNECTER_URL}third-party/midtrans/payment/${order_number}`
	}

	@ManagerModel.bound
	async receiveMidtransNotification(
		notification: NotificationParameterInterface,
	) {
		return MidtransHandler.handleNotification(notification)
	}

	@ManagerModel.bound
	async initiateFaspayTransaction(
		order_number: string,
		transaction: EntityManager,
		// parameter: Without<FaspayTransactionParameterInterface, 'signature' | 'merchant_id' | 'merchant_name'>,
	) {
		const order = await this.OrderRepository.getDetail(order_number, {}, transaction)

		// check order_payment with pending status
		// if exists then throw error, means there is one active payment with xendit channel
		// if error with code 101, proceed old method
		// else throw error
		return await this.OrderPaymentRepository.getPaymentWithStatus(order.id, PAYMENTS.PENDING, transaction)
		.then(() => {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'This Payment is Invalid')
		})
		.catch(error => {
			if(error instanceof ErrorModel && error.is(ERROR_CODES.ERR_101)) {
				if(order.status !== ORDERS.PENDING_PAYMENT) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, order.status)
				}

				const {
					// subtotal,
					// shipping,
					// discount,
					roundup,
					handling,
					total,
				} = OrderUtilitiesManager.getPrices(
					order.orderDetails,
					order.shipments,
					order.points,
					order.wallets,
				)

				return FaspayHandler.transaction({
					order_id: order.number,
					bill_date: TimeHelper.moment(order.created_at).format('YYYY-MM-DD HH:mm:ss'),
					bill_expired: TimeHelper.moment(order.created_at).add(6, 'h').format('YYYY-MM-DD HH:mm:ss'),
					bill_desc: `Pembelian ${ order.orderDetails.map(oD => oD.title).join(', ') }`,
					bill_total: total,
					display_cust: true,
					custName: `${ order.user.profile.first_name } ${ order.user.profile.last_name }`,
					custEmail: order.user.email,
					products: [
						...order.orderDetails.map(orderDetail => {
							return {
								product: orderDetail.title.substring(0, 50),
								amount: orderDetail.price,
								qty: 1,
							}
						}),
					].concat(handling > 0 ? [{
						product: 'Styling Fee',
						amount: handling,
						qty: 1,
					}] : []).concat(roundup > 0 ? [{
						product: 'Round Up',
						amount: roundup,
						qty: 1,
					}] : []).concat(order.shipments.map(shipment => {
						return {
							product: `${ shipment.courier } – ${ shipment.service }`,
							amount: shipment.amount,
							qty: 1,
						}
					})).concat(order.campaigns.map(campaign => {
						return {
							product: campaign.title,
							amount: - [].concat(...order.orderDetails.map(oD => {
								return oD.orderDetailCampaigns.filter(oDC => oDC.campaign_id === campaign.id).map(oDC => oDC.discount)
							})).concat(...order.shipments.map(oD => {
								return oD.shipmentCampaigns.filter(oDC => oDC.campaign_id === campaign.id).map(oDC => oDC.discount)
							})).reduce(CommonHelper.sum, 0),
							qty: 1,
						}
					})).concat(order.coupons.map(coupon => {
						return {
							product: `Coupon - ${ coupon.title }`,
							amount: - [].concat(...order.orderDetails.map(oD => {
								return oD.orderDetailCoupons.filter(oDC => oDC.coupon_id === coupon.id).map(oDC => oDC.discount)
							})).concat(...order.shipments.map(oD => {
								return oD.shipmentCoupons.filter(oDC => oDC.coupon_id === coupon.id).map(oDC => oDC.discount)
							})).reduce(CommonHelper.sum, 0),
							qty: 1,
						}
					})).concat(order.wallets.map(wallet => {
						return {
							product: `User Wallet`,
							amount: - wallet.amount,
							qty: 1,
						}
					})),
					return_url: process.env.FASPAY_CREDIT_RETURN_URL,
					term_condition: 1,
				})
			}

			throw error
		})
	}

	@ManagerModel.bound
	async getFaspayPaymentLink(
		order_number: string,
	) {
		return `${ process.env.CONNECTER_URL }third-party/faspay/payment/${ order_number }`
	}

	@ManagerModel.bound
	async receiveFaspayNotification(
		notification: PaymentNotificationInterface,
	) {
		return FaspayHandler.handleNotification(notification)
	}

	@ManagerModel.bound
	async receiveFaspayCreditNotification(
		notification: CreditReturnResponseInterface,
	) {
		return FaspayHandler.handleCreditTransaction(notification)
	}

	@ManagerModel.bound
	async receiveXenditNotification(
		token: string,
		data: any,
	) {
		return XenditHandler.notification(token, data)
	}

	@ManagerModel.bound
	async initiateIndodanaTransaction(
		order: OrderInterface & {
			details: OrderDetailInterface[],
			address: OrderAddressInterface,
			price: {
				subtotal: number;
				handling: number;
				shipping: number;
				discount: number;
				adjustments: number;
				wallet: number;
				roundup: number;
				total: number;
			},
		},
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		transaction: EntityManager,
	) {
		const yunaAddress = await BrandManager.getAddresses(DEFAULTS.YUNA_BRAND_ID, transaction).then(addresses => addresses[0])

		const {
			redirectUrl,
			transactionId,
		} = await IndodanaService.createPayment(order, user, yunaAddress)

		await this.create(
			order.id,
			PAYMENTS.PENDING,
			PAYMENT_AGENTS.INDODANA,
			'PAYLATER INDODANA',
			{
				redirectUrl,
				transactionId,
			},
			transaction,
		)

		return redirectUrl
	}

	@ManagerModel.bound
	async initiateBCATransaction(
		order_id: number,
		order_number: string,
		user_id: number,
		name: string,
		amount: number,
		transaction: EntityManager,
	) {
		const {
			prefix,
			payment_code,
		} = await BCAManager.getPaymentCode(user_id, transaction)

		await this.create(
			order_id,
			PAYMENTS.PENDING,
			PAYMENT_AGENTS.BCA,
			'VIRTUAL_ACCOUNT BCA',
			{
				id: null,
				method: 'VIRTUAL_ACCOUNT',
				bank: 'BCA',
				order_number,
				prefix,
				payment_code,
				name,
				amount,
			},
			transaction,
		)

		return {
			payment_code,
		}
	}

	@ManagerModel.bound
	async initiateXenditTransaction(
		order_id: number,
		order_number: string,
		method: PAYMENT_METHOD,
		token: string | undefined,
		auth_id: string | undefined,
		masked_card: string | undefined,
		va_bank: VA_BANK | undefined,
		ewallet: EWALLET | undefined,
		retail_outlet: RETAIL_OUTLET | undefined,
		name: string,
		phone: string,
		total: number,
		expired_at: Date,
		transaction: EntityManager,
	): Promise<{
		payment_id: string,
		payment_code: string,
		payment_link: string,
		status: string,
	}> {
		return XenditHandler.transaction(
			method,
			token,
			auth_id,
			va_bank,
			ewallet,
			retail_outlet,
			name,
			phone,
			order_number,
			total,
			expired_at,
		).then(async payment => {
			let status: PAYMENTS
			let type: string
			let payment_id: string
			let payment_code: string
			let payment_link: string
			let metadata: object

			switch(payment.method) {
			case PAYMENT_METHOD.CARD:
				status = (payment.status === 'CAPTURED' || payment.status === 'AUTHORIZED') ? PAYMENTS.SUCCESS : PAYMENTS.FAILED
				type = payment.method + ' ' + payment.card_brand
				metadata = {...payment, masked_card}
				break
			case PAYMENT_METHOD.VIRTUAL_ACCOUNT:
				status = PAYMENTS.PENDING
				type = payment.method + ' ' + payment.bank
				payment_id = payment.id
				payment_code = payment.payment_code
				metadata = {...payment}
				break
			case PAYMENT_METHOD.RETAIL_OUTLET:
				status = PAYMENTS.PENDING
				type = payment.method + ' ' + payment.outlet_name
				payment_id = payment.id
				payment_code = payment.payment_code
				metadata = {...payment}
				break
			case PAYMENT_METHOD.EWALLET:
				status = PAYMENTS.PENDING
				type = payment.method + ' ' + payment.ewallet_type
				payment_id = null
				payment_link = payment.checkout_url
				metadata = {...payment}
				break
			}

			await this.create(
				order_id,
				status,
				PAYMENT_AGENTS.XENDIT,
				type,
				metadata,
				transaction,
			)

			return {
				payment_id,
				payment_code,
				payment_link,
				status,
			}
		})
	}

	@ManagerModel.bound
	async createOrchangePayment(
		order_id: number,
		agent: PAYMENT_AGENTS,
		method: PAYMENT_METHOD,
		token: string | undefined,
		auth_id: string | undefined,
		masked_card: string | undefined,
		va_bank: VA_BANK | undefined,
		ewallet: EWALLET | undefined,
		retail_outlet: RETAIL_OUTLET | undefined,
		phone: string,
		transaction: EntityManager,
	) {
		// get order
		const order = await this.OrderRepository.getDetail(order_id, {}, transaction)

		// get price total
		const price = OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

		// condition for xendit, if user want to change payment from xendit to other channel
		if(!!order.metadata?.payment_id) {
			// get active payment with pending status
			const payment = await this.OrderPaymentRepository.getPaymentWithStatus(order.id, PAYMENTS.PENDING, transaction)

			// deactive old payment
			await this.OrderPaymentRepository.updateActivePayment(order.id, {
				code: PAYMENTS.EXCEPTION,
				note: 'Changing Payment Method',
			}, transaction)

			if(payment.agent === PAYMENT_AGENTS.XENDIT) {
				// expire xendit payment invoice only for virtual account or retail outlet
				const oldMethod = payment.method.split(' ')[0]
				if(oldMethod === PAYMENT_METHOD.VIRTUAL_ACCOUNT || oldMethod === PAYMENT_METHOD.RETAIL_OUTLET) {
					await XenditHandler.setExpired(
						oldMethod,
						order.metadata.payment_id,
						price.total,
						TimeHelper.moment(new Date()).subtract(5, 'minute').toDate(),
					).catch(() => null)
				}
			}
		// condition for indodana, if user want to change payment from indodana to other channel
		} else if (!!order.metadata?.indodana_payment_link) {
			// get active payment with pending status
			const payment = await this.OrderPaymentRepository.getPaymentWithStatus(order.id, PAYMENTS.PENDING, transaction)

			// deactive old payment
			await this.OrderPaymentRepository.updateActivePayment(order.id, {
				code: PAYMENTS.EXCEPTION,
				note: 'Changing Payment Method',
			}, transaction)

			// create full cancellation
			await IndodanaService.createRefund(
				{
					...order,
					payment,
				},
				order.user,
				price.total,
				'Others',
			)
		}

		// create new payment
		let xendit
		let bca
		// const faspay: any = null
		let indodana

		// if user want to change payment from xendit to BCA channel
		if(method === PAYMENT_METHOD.VIRTUAL_ACCOUNT && va_bank === VA_BANK.BCA) {
			bca = await this.initiateBCATransaction(
				order_id,
				order.number,
				order.user_id,
				order.user.profile.first_name + ' ' + order.user.profile.last_name,
				price.total,
				transaction,
			)
			// faspay = {faspay_payment_link: await this.getFaspayPaymentLink(order.number)}
		} else if (method === PAYMENT_METHOD.PAYLATER && agent === PAYMENT_AGENTS.INDODANA) {
			indodana = {
				indodana_payment_link: await this.initiateIndodanaTransaction(
					{
						...order,
						details: order.orderDetails,
						address: order.addresses[0],
						price: {
							subtotal: price.subtotal,
							handling: price.handling,
							shipping: price.shipping,
							discount: price.discount,
							adjustments: price.adjustments,
							wallet: price.wallet,
							roundup: price.roundup,
							total: price.total,
						},
					},
					order.user,
					transaction,
				),
			}
		} else {
			xendit = await this.initiateXenditTransaction(
				order_id,
				order.number,
				method,
				token,
				auth_id,
				masked_card,
				va_bank,
				ewallet,
				retail_outlet,
				order.user.profile.first_name + ' ' + order.user.profile.last_name,
				phone,
				price.total,
				TimeHelper.moment(order.created_at).add(6, 'hour').toDate(),
				transaction,
			)
		}

		// update order metadata and status if using card to paid else stay to panding payment
		await this.OrderRepository.update(null, order_id, {
			status: method === PAYMENT_METHOD.CARD ? ORDERS.PAID : ORDERS.PENDING_PAYMENT,
			metadata: {
				...(
					method === PAYMENT_METHOD.VIRTUAL_ACCOUNT && va_bank === VA_BANK.BCA ? bca
					: method === PAYMENT_METHOD.PAYLATER && PAYMENT_AGENTS.INDODANA ? indodana
					: xendit
				),
				injector_id: order.metadata.injector_id,
			},
		}, 'Changing Payment method', transaction)

		return xendit ? xendit : bca ? bca : indodana
	}

	@ManagerModel.bound
	async checkStatus(
		order_number: string,
		transaction: EntityManager,
	) {
		const order = await this.OrderRepository.get(order_number, false, transaction)
		const payments = await this.OrderPaymentRepository.getPaymentsByOrderNumber(order_number, transaction).catch(() => [] as PaymentInterface[])
		let status = payments[0]?.code ?? null

		if(order.metadata.faspay_payment_link) {
			// check faspay
			const payment = payments.find(p => p.agent === PAYMENT_AGENTS.FASPAY)

			if(payment) {
				if((payment as any).metadata.transaction.merchant_id) {
					// debit
					status = await FaspayHandler.getStatus('DEBIT', {
						trx_id: (payment.metadata as any).trx_id,
						merchant_id: FaspayHandler.get('merchantId'),
						bill_no: order_number,
					})
				} else if((payment as any).metadata.transaction.MERCHANTID) {
					// credit
					let amount = (payment.metadata as any).transaction?.AMOUNT || (payment.metadata as any).notification?.AMOUNT

					if (!amount) {
						const _order = await this.OrderRepository.getDetail(order.id, {}, transaction)
						amount = OrderUtilitiesManager.getPrices(_order.orderDetails, _order.shipments, _order.points, _order.wallets).total?.toFixed(2)
					}

					status = await FaspayHandler.getStatus('CREDIT', {
						MERCHANTID: FaspayHandler.get('creditMerchantId'),
						MERCHANT_TRANID: (payment.metadata as any).transaction?.MERCHANT_TRANID,
						TRANSACTIONID: (payment.metadata as any).transaction?.TRANSACTIONID,
						AMOUNT: amount,
						TRANSACTIONTYPE: '4',
						RESPONSE_TYPE: '3',
						PAYMENT_METHOD: '1',
					})
				}

				if(status === null) {
					status = payment.code
				}
			}
		} else if(order.metadata.midtrans_payment_link) {
			// TODO:
			// check via midtrans
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Midtrans check is not configured')
		}

		switch(status) {
		case PAYMENTS.PENDING:
			return {
				status,
				title: 'Terima kasih!',
				description: 'Order kamu sudah diterima dan akan diproses setelah kami menerima pembayaranmu. Periksa email kamu untuk update lebih lanjut tentang status pembayaranmu.',
			}
		case PAYMENTS.SUCCESS:
			return {
				status,
				title: 'Selamat!',
				description: 'Transaksi kamu sudah berhasil diproses.',
			}
		case PAYMENTS.FAILED:
			return {
				status,
				title: 'Pembayaran gagal',
				description: 'Sayang sekali, pembayaran kamu ditolak. Silakan order ulang dan coba metode pembayaran lain.',
			}
		case PAYMENTS.EXCEPTION:
		default:
			return {
				status: PAYMENTS.EXCEPTION,
				title: 'Menunggu konfirmasi',
				description: 'Order kamu telah diterima dan akan diproses setelah kami menerima pembayaranmu. Jika kamu membayar dengan kartu kredit, pembayaranmu sedang menunggu persetujuan dari Payment Processor kami. Periksa email kamu untuk update lebih lanjut tentang status pembayaranmu.',
			}
		}
	}

	// ============================ PRIVATES ============================


}
export default new OrderPaymentManager()
