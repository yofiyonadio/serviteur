import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import BrandManager from '../brand'
import OrderAddressManager from './_address'
import ServiceManager from '../service'
import VoucherManager from '../voucher'

import {
	OrderRepository,
} from 'app/repositories'

import InterfaceAddressModel from 'energie/app/models/interface.address'
import { OrderRequestInterface, OrderAddressInterface, VoucherInterface } from 'energie/app/interfaces'
import { RequestOrderInterface } from 'app/interfaces/request/order'

import DEFAULTS from 'utils/constants/default'
import { ORDER_REQUESTS, VOUCHERS } from 'energie/utils/constants/enum'
import { ERROR_CODES, ERRORS } from 'app/models/error'

import { CommonHelper } from 'utils/helpers'

import { isEmpty } from 'lodash'
import { Parameter } from 'types/common'
import { OrderRequestVoucherMetadataInterface, OrderRequestVariantMetadataInterface, OrderRequestMatchboxMetadataInterface, OrderRequestServiceMetadataInterface, OrderRequestStyleboardMetadataInterface, OrderRequestTopupMetadataInterface, OrderRequestMetadataInterface, OrderRequestVoucherTopupMetadataInterface } from 'energie/app/interfaces/order.request'
import { check } from 'app/interfaces/guard'


class OrderRequestManager extends ManagerModel {

	static __displayName = 'OrderRequestManager'

	protected OrderRepository: OrderRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
		], async () => {
			await OrderAddressManager.initialize(connection)
			await VoucherManager.initialize(connection)
		})
	}

	// ============================= INSERT =============================
	async create(
		order_id: number,
		user_id: number,
		requests: RequestOrderInterface[],
		default_address: InterfaceAddressModel,
		is_facade: boolean | undefined,
		transaction: EntityManager,
	): Promise<Array<OrderRequestInterface & {
		address: OrderAddressInterface,
	}>> {
		const defaultAddress = await OrderAddressManager.create(order_id, default_address, is_facade, transaction)
		const addresses = {}

		const data = await requests.reduce((promise, request) => {
			return promise.then(async sum => {
				const as_voucher = CommonHelper.default(request.as_voucher, false)
				const quantity = CommonHelper.default(request.quantity, 1)
				const address = isEmpty(request.metadata.address) ? defaultAddress : await OrderAddressManager.create(order_id, request.metadata.address, is_facade, transaction)

				addresses[address.id] = address

				if (request.type === ORDER_REQUESTS.VOUCHER) {
					const voucher = await VoucherManager.getByCode(request.id as string, true, transaction).catch(err => {
						if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
							err.message = 'Voucher is invalid or already redeemed.'
						}

						throw err
					})

					return sum.concat({
						order_id,
						order_address_id: address.id,
						address,
						type: request.type,
						// ========================================================
						// Id is voucher's code that is being redeemed by user
						// ========================================================
						ref_id: voucher.id,
						// ========================================================
						// Validated: as_voucher must be false
						// Validated: quantity must be 1
						// ========================================================
						as_voucher: false,
						quantity: 1,
						metadata: await this.trimMetadata(false, ORDER_REQUESTS.VOUCHER, voucher, request.metadata, transaction),
					})
				} else {

					const metadata = request.metadata

					if (request.type === ORDER_REQUESTS.VARIANT) {
						if ((metadata as OrderRequestVariantMetadataInterface).styleboard_id) {
							// check whether this variant is belong to the styleboard or not
							const isVariantInStyleboard = await ServiceManager.styleboard.isVariantInStyleboard(request.id as number, (metadata as OrderRequestVariantMetadataInterface).styleboard_id, user_id, transaction)

							if (!isVariantInStyleboard) {
								delete (metadata as OrderRequestVariantMetadataInterface).styleboard_id
							}
						}

						// Why don't we use else if? To handle injection
						if ((metadata as OrderRequestVariantMetadataInterface).stylecard_id) {
							// check whether this variant is belong to the stylecard or not
							const isVariantInStylecard = await ServiceManager.stylecard.isVariantInStylecard(request.id as number, (metadata as OrderRequestVariantMetadataInterface).stylecard_id, transaction)

							if (!isVariantInStylecard) {
								delete (metadata as OrderRequestVariantMetadataInterface).stylecard_id
							}
						}
					}


					return sum.concat({
						order_id,
						order_address_id: address.id,
						address,
						type: request.type,
						ref_id: request.id as number,
						as_voucher,
						quantity,
						metadata: await this.trimMetadata(as_voucher, request.type, request.id, metadata, transaction),
					})
				}
			})
		}, Promise.resolve([] as Array<Parameter<OrderRequestInterface> & {
			address: OrderAddressInterface,
		}>))

		if(is_facade) {
			return data.map((orderRequest, i) => {
				return {
					id: i,
					...orderRequest,
					address: addresses[orderRequest.order_address_id],
					metadata: orderRequest.metadata,
				}
			})
		} else {
			return this.OrderRepository.insertRequest(data.map(d => {
				return {
					order_id: d.order_id,
					order_address_id: d.order_address_id,
					type: d.type,
					ref_id: d.ref_id,
					as_voucher: d.as_voucher,
					quantity: d.quantity,
					metadata: d.metadata,
				}
			}), transaction).then(orderRequests => {
				return orderRequests.map(orderRequest => {
					return {
						...orderRequest,
						address: addresses[orderRequest.order_address_id],
					}
				})
			})
		}
	}

	async createCustom(
		request: Parameter<OrderRequestInterface> & {
			address: OrderAddressInterface,
		},
		is_facade: boolean,
		transaction: EntityManager,
	): Promise<OrderRequestInterface & {
		address: OrderAddressInterface,
	}> {
		const as_voucher = request.as_voucher ?? false
		const voucher = request.type === ORDER_REQUESTS.VOUCHER ? await VoucherManager.get(request.ref_id, false, transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return request.ref_id
			}

			throw err
		}) : request.ref_id

		if (is_facade) {
			return {
				id: Date.now(),
				order_id: request.order_id,
				order_address_id: request.order_address_id,
				type: request.type,
				ref_id: request.ref_id,
				as_voucher,
				quantity: request.quantity ?? 1,
				address: request.address,
				metadata: await this.trimMetadata(as_voucher, request.type, voucher, request.metadata, transaction),
			}
		} else {
			return this.OrderRepository.insertRequest({
				order_id: request.order_id,
				order_address_id: request.order_address_id,
				type: request.type,
				ref_id: request.ref_id,
				as_voucher,
				quantity: request.quantity ?? 1,
				metadata: await this.trimMetadata(as_voucher, request.type, voucher, request.metadata, transaction),
			}, transaction).then(orderRequest => {
				return {
					...orderRequest,
					address: request.address,
				}
			})
		}
	}

	async createForBag(
		order_id: number,
		requests: RequestOrderInterface[],
		transaction: EntityManager,
	): Promise<Array<OrderRequestInterface & {
		address: OrderAddressInterface,
	}>> {
		// const defaultAddress = await OrderAddressManager.create(order_id, default_address, is_facade, transaction)
		// const addresses = {}
		const address = await BrandManager.getMainAddress(DEFAULTS.YUNA_BRAND_ID, transaction).then(a => {
			return CommonHelper.stripUndefined({
				...a,
				brand_id: undefined,
				order_id,
			})
		})

		return requests.reduce((promise, request) => {
			return promise.then(async sum => {
				const as_voucher = CommonHelper.default(request.as_voucher, false)
				const quantity = CommonHelper.default(request.quantity, 1)

				if (request.type === ORDER_REQUESTS.VOUCHER) {
					const voucher = await VoucherManager.getByCode(request.id as any as string, true, transaction).catch(err => {
						if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
							err.message = 'Voucher is invalid or already redeemed.'
						}

						throw err
					})

					return [
						...sum,
						{
							id: Date.now(),
							order_id,
							order_address_id: address.id,
							address,
							type: request.type,
							// ========================================================
							// Id is voucher's code that is being redeemed by user
							// ========================================================
							ref_id: voucher.id,
							// ========================================================
							// Validated: as_voucher must be false
							// Validated: quantity must be 1
							// ========================================================
							as_voucher: false,
							quantity: 1,
							metadata: await this.trimMetadata(false, ORDER_REQUESTS.VOUCHER, voucher, request.metadata, transaction),
						},
					]
				} else {
					return [
						...sum,
						{
							id: Date.now(),
							order_id,
							order_address_id: address.id,
							address,
							type: request.type,
							ref_id: request.id,
							as_voucher,
							quantity,
							metadata: await this.trimMetadata(as_voucher, request.type, request.id, request.metadata, transaction),
						},
					]
				}
			})
		}, Promise.resolve([]))
	}

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async trimMetadata(as_voucher: boolean, type: ORDER_REQUESTS, voucherOrIdOrSlug: VoucherInterface | number | string, metadata: OrderRequestMetadataInterface, transaction: EntityManager)  {
		// Validate metadata first
		if (as_voucher === true) {
			if (type === ORDER_REQUESTS.TOPUP) {
				if (check(metadata, ['object', {
					name: 'string',
					email: 'string',
					note: 'string',
					amount: 'number',
					code: 'string?',
					is_physical: 'boolean?',
					merchant: 'string?',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVoucherTopupMetadataInterface, 'amount', 'code', 'email', 'is_physical', 'merchant', 'name', 'note')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for VOUCHER TOPUP not fulfilled.')
				}
			} else {
				if(check(metadata, ['object', {
					name: 'string',
					email: 'string',
					note: 'string',
					code: 'string?',
					is_physical: 'boolean?',
					merchant: 'string?',
					address: ['object?', {
						location_id: 'number',
						address: 'string',
						postal: 'string',
						title: 'string?',
						receiver: 'string?',
						phone: 'string?',
						district: 'string?',
						metadata: ['object?', {
							tikiAreaId: 'number?',
							manualAreaId: 'number?',
							sicepatAreaId: 'number?',
						}],
					}],
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVoucherMetadataInterface, 'code', 'email', 'is_physical', 'merchant', 'name', 'note', 'address')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for VOUCHER not fulfilled.')
				}
			}
		} else {
			if (type === ORDER_REQUESTS.VARIANT) {
				if (check(metadata, ['object', {
					styleboard_id: 'number?',
					stylecard_id: 'number?',
					note: 'string?',
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					ref: 'string?',
					device: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?',
					created_at: 'number?'
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestVariantMetadataInterface, 'styleboard_id', 'stylecard_id', 'note', 'merchant', 'source', 'medium', 'ref', 'utm_source', 'utm_medium', 'utm_campaign', 'device', 'created_at')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for INVENTORY/VARIANT not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.MATCHBOX) {
				if (check(metadata, ['object', {
					lineup: 'string?',
					note: 'string?',
					asset_ids: ['array?', 'number'],
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					device: 'string?',
					ref: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?'
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestMatchboxMetadataInterface, 'asset_ids', 'lineup', 'note', 'merchant')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for MATCHBOX not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.SERVICE) {
				if (check(metadata, ['object', {
					outfit: 'string',
					attire: 'string',
					product: 'string?',
					note: 'string?',
					asset_ids: ['array?', 'number'],
					merchant: 'string?',
					source: 'string?',
					medium: 'string?',
					ref: 'string?',
					device: 'string?',
					utm_source: 'string?',
					utm_medium: 'string?',
					utm_campaign: 'string?'
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestServiceMetadataInterface, 'attire', 'note', 'outfit', 'product', 'asset_ids', 'merchant')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for SERVICE not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.STYLEBOARD) {
				if (check(metadata, ['object', {
					stylecard_variant_ids: ['array', 'number'],
				}])) {
					return CommonHelper.allowKey({
						...metadata,
						...await Promise.resolve().then(() => {
							if (voucherOrIdOrSlug === null) {
								return {}
							} else if (typeof voucherOrIdOrSlug === 'number') {
								// So that there is continuity with its request
								return ServiceManager.styleboard.get(voucherOrIdOrSlug, { with_request: true }, transaction).then(styleboard => {
									return styleboard.request.metadata
								})
							} else {
								return {}
							}
						}),
					} as OrderRequestStyleboardMetadataInterface, 'stylecard_variant_ids', 'attire', 'note', 'outfit', 'product', 'merchant')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for STYLEBOARD not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.TOPUP) {
				if (check(metadata, ['object', {
					amount: 'number',
				}])) {
					return CommonHelper.allowKey(metadata as OrderRequestTopupMetadataInterface, 'amount')
				} else {
					throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Metadata object for TOPUP not fulfilled.')
				}
			} else if (type === ORDER_REQUESTS.VOUCHER) {
				if (voucherOrIdOrSlug === null || typeof voucherOrIdOrSlug === 'number' || typeof voucherOrIdOrSlug === 'string') {
					return {}
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.INVENTORY) {
					return this.trimMetadata(false, ORDER_REQUESTS.VARIANT, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.MATCHBOX) {
					return this.trimMetadata(false, ORDER_REQUESTS.MATCHBOX, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.SERVICE) {
					return this.trimMetadata(false, ORDER_REQUESTS.SERVICE, null, metadata, transaction)
				} else if (voucherOrIdOrSlug?.type === VOUCHERS.TOPUP_AMOUNT) {
					return {}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Voucher type not yet configured.')
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Request type not yet configured.')
			}
		}
	}


}
export default new OrderRequestManager()
