import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import NotificationManager from '../notification'
import InventoryManager from '../inventory'

import {
	OrderRepository,
	OrderDetailRepository,
	OrderPaymentRepository,
} from 'app/repositories'

import {
	OrderDetailInterface,
	ShipmentOrderDetailInterface,
	ShipmentInterface,
	UserInterface,
	OrderDetailCouponInterface,
	OrderDetailCampaignInterface,
	UserWalletTransactionInterface,
	OrderDetailTransactionInterface,
	ShipmentCouponInterface,
	ShipmentCampaignInterface,
	UserPointTransactionInterface,
} from 'energie/app/interfaces'
import { RequestOrderInterface } from 'app/interfaces/request/order'

import CommonHelper from 'utils/helpers/common'
import FormatHelper from 'utils/helpers/format'
import TimeHelper from 'utils/helpers/time'

import { ERRORS, ERROR_CODES } from 'app/models/error'
import { ORDER_DETAILS, ORDER_REQUESTS, STATEMENTS, STATEMENT_SOURCES } from 'energie/utils/constants/enum'

import {
	isEmpty,
	padStart,
	uniqBy,
} from 'lodash'
import { OrderManager, UserManager } from '..'


class OrderUtilitiesManager extends ManagerModel {

	static __displayName = 'OrderUtilitiesManager'

	protected OrderRepository: OrderRepository
	protected OrderDetailRepository: OrderDetailRepository
	protected OrderPaymentRepository: OrderPaymentRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
			OrderDetailRepository,
			OrderPaymentRepository,
		])
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getOrderNumber(
		// transaction must be provided
		transaction: EntityManager,
	) {
		const now = TimeHelper.moment()
		const startMinute = TimeHelper.moment(now).startOf('minute')
		const endMinute = TimeHelper.moment(now).endOf('minute')

		return await this.OrderRepository.getCountInCurrentMinute(transaction, startMinute.toDate(), endMinute.toDate()).then(result => {
			return `YCO${now.format('YYMMDD')}${padStart((result + 1).toString(), 3, '0')}${now.format('mmHH')}`
		})
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async sendCheckoutMail(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getDetail(order_id, {}, transaction).then(order => {
			const {
				subtotal,
				handling,
				shipping,
				discount,
				adjustments,
				wallet,
				roundup,
				total,
			} = this.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

			return NotificationManager.sendCheckoutConfirmationNotice(
				order.user.id,
				order.user.email,
				order_id,
				order.number,
				TimeHelper.format(order.created_at, 'DD MMM YYYY hh:mm'),
				TimeHelper.moment(order.created_at).add(6, 'h').format('DD MMM YYYY hh:mm'),
				order.orderDetails,
				FormatHelper.readableStatus(order.status),
				FormatHelper.currency(subtotal, 'IDR'),
				FormatHelper.currency(handling, 'IDR'),
				FormatHelper.currency(-discount, 'IDR'),
				FormatHelper.currency(shipping, 'IDR'),
				FormatHelper.currency(adjustments, 'IDR'),
				FormatHelper.currency(wallet, 'IDR'),
				FormatHelper.currency(roundup, 'IDR'),
				FormatHelper.currency(total, 'IDR'),
			)
		})
	}

	async refundProcessor(
		user_id: number,
		order_id: number,
		variant_id: number,
		inventory_ids: number[],
		autoDeposit: boolean,
		transaction: EntityManager,
	) {
		// get order details
		const details = await this.OrderRepository.getForCountRefund(order_id, transaction)

		// get point used
		const pointSpent = details[0].points.reduce((i, p) => p.type === STATEMENTS.DEBIT ? i + p.amount : i, 0)

		// get already refunded point
		const refundPoint = details[0].points.reduce((i, p) => p.type === STATEMENTS.CREDIT ? i + p.amount : i, 0)

		// count discount
		const discount = details.reduce((sum, detail) => {
			return sum + detail.orderDetailCampaigns.reduce((i, campaign) => !campaign.is_refundable ? i + campaign.discount : i, 0)
				+ detail.orderDetailCoupons.reduce((i, coupon) => i + coupon.discount, 0)
		} , 0)

		// get variant value
		const variant = await details.reduce(async (promise, detail) => {
			return promise.then(async init => {
				let subtotal = 0
				let totalSheetItemValues = 0 // only for stylesheet
				let realRetailPrice = 0

				if (detail.type === ORDER_DETAILS.STYLESHEET) {
					subtotal = detail.price === 0
						? await OrderManager.detail.getOrderDetailFromVoucherRedeem(detail.id, transaction).then(orderDetail => orderDetail.price).catch(() => detail.price)
						: detail.price
					totalSheetItemValues = detail.variants.reduce((i, v) => i + v.inventory.price, 0)
					realRetailPrice = subtotal - discount

					return {
						...init,
						...detail.variants.reduce((i, v) => {
							return {
								...i,
								[v.id]: Math.ceil(v.inventory.price / totalSheetItemValues * realRetailPrice),
							}
						}, {}),
					}
				} else if (detail.type === ORDER_DETAILS.INVENTORY) {
					subtotal = details.filter(d => d.type === ORDER_DETAILS.INVENTORY).reduce((i, d) => {
						return i + d.price
					}, 0)
					realRetailPrice = subtotal - discount

					return {
						...init,
						...details.filter(d => d.type === ORDER_DETAILS.INVENTORY).reduce((i, d) => {
							return {
								...i,
								[d.variants[0].id]: Math.ceil(d.price / subtotal * realRetailPrice),
							}
						}, {}),
					}
				}
			})
		}, Promise.resolve({} as {
			[id: number]: number,
		}))

		// count refund value
		const refundValue = !!variant_id && isEmpty(inventory_ids) ? {
			value: variant[variant_id],
			point: Math.max((pointSpent - refundPoint), 0),
			wallet: Math.max((variant[variant_id] - Math.max((pointSpent - refundPoint), 0)), 0),
		} : await inventory_ids.reduce(async (promise, inventory_id, i) => {
			return promise.then(async rv => {
				let pointRemains = 0
				const inventory = await InventoryManager.get(inventory_id, transaction)

				if (i === 0) {
					pointRemains += Math.max((pointSpent - refundPoint), 0)
				} else {
					pointRemains += Math.max((pointSpent - rv.point), 0)
				}

				return {
					value: rv.value + variant[inventory.variant_id],
					point: pointRemains,
					wallet: rv.wallet + (rv.value + variant[inventory.variant_id] - pointRemains),
				}
			})
		}, Promise.resolve({
			value: 0,
			point: 0,
			wallet: 0,
		}))

		if (autoDeposit && refundValue.point > 0) {
			// create refund point transaction
			await UserManager.point.depositPoint(
				user_id,
				refundValue.point,
				STATEMENT_SOURCES.ORDER,
				order_id,
				'Point from refund',
				transaction,
			)
		}

		if (autoDeposit && refundValue.wallet > 0) {
			// get user wallet
			const userWallet = await UserManager.wallet.getWallet(user_id, transaction)

			// create refund wallet transaction
			await UserManager.wallet.depositInto(
				userWallet.id,
				refundValue.wallet,
				STATEMENT_SOURCES.ORDER,
				order_id,
				'Wallet from refund',
				transaction,
			)
		}

		return variant
	}

	@ManagerModel.bound
	public getPrices(
		orderDetails: Array<OrderDetailInterface & {
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
			orderDetailTransactions?: OrderDetailTransactionInterface[],
		}>,
		shipments: Array<ShipmentInterface & {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		}>,
		points: UserPointTransactionInterface[],
		wallets: UserWalletTransactionInterface[],
	): {
		subtotal: number,
		shipping: number,
		discount: number,
		cashback: number,
		point: number,
		wallet: number,
		handling: number,
		roundup: number,
		adjustments: number,
		total: number,
	} {
		// =========================================
		// Order detail price is not counted if
		// it is an exchange.
		// =========================================
		const subtotal: number = orderDetails.map(orderDetail => orderDetail.price).reduce(CommonHelper.sum, 0)
		const handling: number = orderDetails.map(orderDetail => orderDetail.handling_fee).reduce(CommonHelper.sum, 0)
		const campaign: number = orderDetails.map(orderDetail => orderDetail.orderDetailCampaigns.map(orderDetailCampaign => orderDetailCampaign.discount).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0) + shipments.filter(shipment => shipment.is_facade).map(shipment => shipment.shipmentCampaigns.map(sC => sC.discount).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0)
		const coupon: number = orderDetails.map(orderDetail => orderDetail.orderDetailCoupons.map(orderDetailCoupon => orderDetailCoupon.discount).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0) + shipments.filter(shipment => shipment.is_facade).map(shipment => shipment.shipmentCoupons.map(sC => sC.discount).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0)
		// const adjustments: number = orderDetails.map(orderDetail => orderDetail.orderDetailTransactions.map(orderDetailTransaction => orderDetailTransaction.amount).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0)
		const shipping: number = shipments.filter(shipment => shipment.is_facade).map(shipment => shipment.amount).reduce(CommonHelper.sum, 0)
		const wallet: number = wallets.filter(w => w.type === STATEMENTS.DEBIT).map(w => w.amount).reduce(CommonHelper.sum, 0) ?? 0
		const point: number = points.filter(p => p.type === STATEMENTS.DEBIT).map(p => p.amount).reduce(CommonHelper.sum, 0) ?? 0
		const roundup: number = 0 // wallets.filter(w => w.type === STATEMENTS.CREDIT).map(w => w.amount).reduce(CommonHelper.sum, 0) ?? 0
		const cashback: number = orderDetails.map(orderDetail => orderDetail.orderDetailCoupons.map(orderDetailCoupon => orderDetailCoupon.cashback).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0) +
			orderDetails.map(orderDetail => orderDetail.orderDetailCampaigns.map(orderDetailCampaign => orderDetailCampaign.cashback).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0) +
			shipments.filter(shipment => shipment.is_facade).map(shipment => shipment.shipmentCoupons.map(sC => sC.cashback).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0) +
			shipments.filter(shipment => shipment.is_facade).map(shipment => shipment.shipmentCampaigns.map(sC => sC.cashback).reduce(CommonHelper.sum, 0)).reduce(CommonHelper.sum, 0)
		const total: number = subtotal + handling + shipping + roundup - coupon - campaign - point - wallet
		return {
			subtotal,
			handling,
			shipping,
			roundup,
			discount: coupon + campaign,
			cashback,
			point,
			wallet,
			// For now we do not print this
			adjustments: 0,
			total,
		}
	}

	@ManagerModel.bound
	public getShipments(
		isFacade: boolean = true,
		orderDetails: Array<OrderDetailInterface & {
			orderDetailShipments: Array<ShipmentOrderDetailInterface & {
				shipment: ShipmentInterface,
			}>,
		}>,
	): ShipmentInterface[] {
		return uniqBy(
			orderDetails
				.map(orderDetail => {
					return (orderDetail.orderDetailShipments || [])
						.map(orderDetailShipment => orderDetailShipment.shipment)
						.filter(shipment => isFacade ? shipment.is_facade : !shipment.is_facade)
				})
				.reduce(CommonHelper.sumArray, []),
			shipment => shipment.id,
		)
	}

	// ===================================================================
	// DO:
	// - Validate requests
	// - It may contain double vouchers or double inventory of same id
	// ===================================================================
	@ManagerModel.bound
	public validateRequest(requests: RequestOrderInterface[]) {
		if (!requests.length) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_101, 'Request cannot be empty')
		} else if (
			// Check for duplicate voucher usage or inventory id
			requests.map(request => {
				return `${request.type}|${request.id}`
			}).reduce((sum, id) => {
				sum.filter[id] = sum.filter[id] ? sum.dup.push(id) : true
				return sum
			}, { filter: {}, dup: [] }).dup.findIndex(id => {
				const type = id.split('|')[0]
				return type === ORDER_REQUESTS.VOUCHER
			}) > -1
		) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'You cannot redeem the same voucher nor having multiple order inventory with the same id')
		}

		requests.forEach(request => {
			if (!request.metadata) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request metadata cannot be empty.')
			}

			if (request.metadata.address && !request.metadata.address.location_id) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Request address has no location_id!')
			}

			if (request.type === ORDER_REQUESTS.VOUCHER && request.as_voucher === true) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Voucher cannot be bought as voucher, we don\'t wanna have voucherception here.')
			}

			if ((request.type === ORDER_REQUESTS.VOUCHER) && request.quantity > 1) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Voucher cannot have quantity > 1')
			}

			if (!ORDER_REQUESTS[request.type]) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Product type "${request.type}" is invalid`)
			}
		})

	}

	// ===================================================================
	// DO:
	// - Validate user
	// - User must be verified
	// ===================================================================
	@ManagerModel.bound
	public validateUser(user: UserInterface) {
		// Disable user verification for now
		// if (!user.is_verified) {
		// 	return Promise.reject(new ErrorModel(ERRORS.NOT_VERIFIED, ERROR_CODES.ERR_100, 'User need to be verified first.'))
		// }
	}

	// ============================ PRIVATES ============================



}

export default new OrderUtilitiesManager()
