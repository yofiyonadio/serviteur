import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import SegmentHandler from 'app/handlers/segment'

// import AddressManager from 'app/managers/address'
import AssetManager from '../asset'
import PromoManager from '../promo'
import BrandManager from '../brand'
import InventoryManager from '../inventory'
import MatchboxManager from '../matchbox'
import NotificationManager from '../notification'
import ShipmentManager from '../shipment'
// import StylesheetManager from '../stylesheet'
import UserManager from '../user'
import VoucherManager from '../voucher'
// import WarningManager from '../warning'

import OrderBagManager from './_bag'
import OrderAddressManager from './_address'
import OrderDetailManager from './_detail'
import OrderPaymentManager from './_payment'
import OrderRequestManager from './_request'
import OrderUtilitiesManager from './_utilities'
import ServiceManager from '../service'
import TopupManager from '../topup'


import {
	OrderRepository,
	OrderDetailRepository,
	OrderPaymentRepository,
	ShipmentRepository,
} from 'app/repositories'

import InterfaceAddressModel from 'energie/app/models/interface.address'
import InterfaceAssetModel from 'energie/app/models/interface.asset'
import {
	OrderRequestInterface,
	OrderInterface,
	OrderDetailInterface,
	UserInterface,
	UserProfileInterface,
	UserAddressInterface,
	// OrderAddressInterface,
	UserWalletTransactionInterface,
	ShipmentCampaignInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	ShipmentCouponInterface,
	OrderPaymentInterface,
	CampaignInterface,
	MatchboxInterface,
	ServiceInterface,
	ProductInterface,
	BrandInterface,
	UserPointTransactionInterface,
} from 'energie/app/interfaces'
import {
	RequestOrderInterface,
} from 'app/interfaces/request/order'
import {
	OrderDetailVoucherMetadataInterface,
} from 'energie/app/interfaces/order.detail'
import {
	NotificationParameterInterface,
} from 'app/interfaces/midtrans'
import {
	PaymentNotificationInterface, PaymentNotificationResponseInterface, CreditReturnResponseInterface,
} from 'app/interfaces/faspay'
import { VA_BANK } from 'app/handlers/xendit'
import {
	OrderRequestStyleboardMetadataInterface,
} from 'energie/app/interfaces/order.request'

import {
	CommonHelper,
	FormatHelper,
	TimeHelper,
} from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'

import {
	ORDERS,
	ORDER_DETAILS,
	PAYMENTS,
	ORDER_DETAIL_STATUSES,
	ORDER_REQUESTS,
	PAYMENT_AGENTS,
	STATEMENTS,
	STATEMENT_SOURCES,
	WALLET_STATUSES,
	VOUCHERS,
	PAYMENT_METHOD,
} from 'energie/utils/constants/enum'

import { ERRORS, ERROR_CODES } from 'app/models/error'

import { Parameter, Await, Unarray } from 'types/common'

import { capitalize, isEmpty, findIndex } from 'lodash'
import { checkPayment } from 'app/interfaces/guard'
import { PaymentRequestInterface } from 'app/interfaces/payment'
import { ProductManager } from '..'


class OrderManager extends ManagerModel {

	static __displayName = 'OrderManager'

	protected OrderRepository: OrderRepository
	protected OrderDetailRepository: OrderDetailRepository
	protected OrderPaymentRepository: OrderPaymentRepository
	protected ShipmentRepository: ShipmentRepository

	// tslint:disable-next-line:member-ordering
	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
			OrderDetailRepository,
			OrderPaymentRepository,
			ShipmentRepository,
		], async () => {
			await OrderAddressManager.initialize(connection)
			await OrderDetailManager.initialize(connection)
			await OrderPaymentManager.initialize(connection)
			await OrderRequestManager.initialize(connection)
			await OrderUtilitiesManager.initialize(connection)
			await OrderBagManager.initialize(connection)
		})
	}

	get bag(): typeof OrderBagManager {
		return OrderBagManager
	}

	get address(): typeof OrderAddressManager {
		return OrderAddressManager
	}

	get detail(): typeof OrderDetailManager {
		return OrderDetailManager
	}

	get request(): typeof OrderRequestManager {
		return OrderRequestManager
	}

	get payment(): typeof OrderPaymentManager {
		return OrderPaymentManager
	}

	get utilities(): typeof OrderUtilitiesManager {
		return OrderUtilitiesManager
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async injectOrder(
		changer_user_id: number,
		userId: number,
		userAddressId: number,
		// user: UserInterface & {
		// 	profile: UserProfileInterface,
		// },
		items: RequestOrderInterface[],
		couponCode: string | undefined,
		isMock: boolean = false,
		orderDate: Date | null,
		useWallet: boolean = true,
		transaction: EntityManager,
	) {
		let userAddress: UserAddressInterface
		const user = await UserManager.getCompleteProfile(userId, transaction)

		if (isMock) {
			if (userAddressId) {
				userAddress = await UserManager.address.getByIds(user.id, userAddressId, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not found!')
					}

					throw err
				})
			} else {
				userAddress = {
					id: -1,
					title: 'Facade Address',
					receiver: 'NN',
					address: '–',
					postal: '11470',
					user_id: user.id,
					created_at: null,
				}
			}

			return this.mockOrder(
				changer_user_id,
				userAddress,
				user,
				items,
				couponCode,
				{
					useWallet,
				},
				transaction,
			)
		} else {
			if (userAddressId) {
				userAddress = await UserManager.address.getByIds(user.id, userAddressId, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not found!')
					}

					throw err
				})
			} else {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not supplied!')
			}

			return this.createOrder(
				changer_user_id,
				userAddress,
				user,
				items,
				undefined, // TODO: handle untuk inject order
				couponCode,
				{
					orderDate,
					isInjecting: true,
					sendEmail: true,
					useWallet,
				},
				transaction,
			)
		}
	}

	@ManagerModel.bound
	async composeForBag(
		user: UserInterface,
		requests: RequestOrderInterface[],
		coupon_code: string,
		transaction: EntityManager,
	) {
		// =================================
		// CREATE ORDER
		// =================================

		this.log('Creating mock order.....')

		const order: OrderInterface = {
			id: Date.now(),
			user_id: user.id,
			number: '',
			status: ORDERS.PENDING_PAYMENT,
			created_at: new Date(),
		}

		// =================================
		// CREATE ORDER REQUESTS
		// =================================

		this.log('Creating order requests.')

		return OrderRequestManager.createForBag(order.id, requests, transaction).then(async orderRequests => {
			// =================================
			// CREATE ORDER DETAILS
			// =================================

			this.log('Creating order details..')

			return Promise.all([
				orderRequests,
				OrderDetailManager.composeBagFromRequest(user.id, order, orderRequests, {
					with_shipment: false,
				}, transaction),
			])
		}).then(async ([orderRequests, details]) => {
			// =================================
			// APPLY PRE-CAMPAIGN MODIFICATION
			// =================================

			this.log('Applying pre-campaigns..')

			const preCampaignPromotions = await PromoManager.applyPreCampaignPromotion(
				user.id,
				order,
				user,
				coupon_code,
				orderRequests,
				true,
				transaction,
			)

			// Add newly created order request into orderRequests
			orderRequests.push(...preCampaignPromotions.orderRequests)

			let _id = 1

			return {
				details: details.concat(await OrderDetailManager.composeBagFromRequest(user.id, order, preCampaignPromotions.orderRequests, {
					with_shipment: false,
				}, transaction)),
				orderDetails: ([] as Array<OrderDetailInterface & {
					request: OrderRequestInterface,
				}>).concat(details.reduce((sum, detail) => {
					return [
						...sum,
						...Array(detail.request.quantity).fill(undefined).map((u, i) => {
							return {
								...detail,
								id: _id++,
							}
						}),
					]
				}, [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface,
				}>)).concat(preCampaignPromotions.orderDetails).map(detail => {
					return {
						...detail,
						orderDetailCampaigns: [],
						orderDetailCoupons: [],
					}
				}),
				orderDetailPreCampaigns: preCampaignPromotions.orderDetailCampaigns,
				shipmentPreCampaigns: preCampaignPromotions.shipmentCampaigns,
				precampaigns: preCampaignPromotions.campaigns,
			}
		}).then(async ({
			precampaigns,
			details,
			orderDetails,
			orderDetailPreCampaigns,
			shipmentPreCampaigns,
		}) => {
			// =================================
			// CALCULATE COUPON & CAMPAIGN
			// NOTE: coupon can be NULL
			// =================================

			this.log('Calculating discount....')

			const {
				coupon,
				campaigns,
				orderDetailCampaigns,
				orderDetailCoupons,
				// shipmentCampaigns,
				// shipmentCoupons,
			} = await PromoManager.applyPromotionFromCouponAndCampaign(coupon_code, user, orderDetails, [], {
				orderDetailCampaigns: orderDetailPreCampaigns,
				shipmentCampaigns: shipmentPreCampaigns,
			}, undefined, transaction)

			// Insert to DB and mutate orderdetails & shipments
			orderDetails.forEach(orderDetail => {
				orderDetail.orderDetailCampaigns.push(...orderDetailCampaigns.filter(odC => odC.order_detail_id === orderDetail.id))
				orderDetail.orderDetailCoupons.push(...orderDetailCoupons.filter(odC => odC.order_detail_id === orderDetail.id))
			})

			// =================================
			// Wallet usage count
			// =================================

			this.log('Apply point & wallet....')

			// const userPoint = await UserManager.point.getPoint(user.id, transaction)
			const orderPoints: UserPointTransactionInterface[] = []

			const userWallet = await UserManager.wallet.getWallet(user.id, transaction)
			const orderWallets: UserWalletTransactionInterface[] = []

			// =================================
			// Rounding
			// =================================
			await Promise.resolve().then(async () => {
				const {
					total: _total,
				} = OrderUtilitiesManager.getPrices(orderDetails, [], orderPoints, orderWallets)

				if (_total > 0 && _total < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					// roundup
					orderWallets.push({
						id: Date.now(),
						user_wallet_id: userWallet.id,
						amount: DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - _total,
						type: STATEMENTS.CREDIT,
						source: STATEMENT_SOURCES.ORDER,
						status: WALLET_STATUSES.PENDING,
						note: `Round-up because total order value is less than ${FormatHelper.currency(DEFAULTS.MINIMUM_WITHDRAW_AMOUNT, 'IDR')}`,
						ref_id: order.id,
						created_at: new Date(),
					})
				}
			})

			// =================================
			// Final parsing
			// =================================

			const {
				subtotal,
				shipping,
				discount,
				cashback,
				wallet,
				handling,
				roundup,
				total,
			} = OrderUtilitiesManager.getPrices(orderDetails, [], [], orderWallets)

			return {
				details,
				coupons: Object.values(orderDetailCoupons.reduce((sum, oDC) => {
					return {
						...sum,
						[oDC.coupon_id]: {
							id: oDC.coupon_id,
							code: coupon.title,
							is_refundable: coupon.is_refundable,
							free_shipping: coupon.free_shipping,
							discount: (sum[oDC.coupon_id]?.discount ?? 0) + oDC.discount,
							cashback: (sum[oDC.coupon_id]?.cashback ?? 0) + oDC.cashback,
						},
					}
				}, {})),
				campaigns: Object.values(orderDetailCampaigns.reduce((sum, oDC) => {
					const campaign = campaigns.concat((precampaigns as any)).find(c => c.id === oDC.campaign_id)

					return {
						...sum,
						[oDC.campaign_id]: {
							id: oDC.campaign_id,
							code: campaign?.title,
							is_refundable: campaign?.is_refundable,
							discount: (sum[oDC.campaign_id]?.discount ?? 0) + oDC.discount,
							cashback: (sum[oDC.campaign_id]?.cashback ?? 0) + oDC.cashback,
						},
					}
				}, {})),
				price: {
					subtotal,
					shipping,
					discount,
					cashback,
					wallet,
					handling,
					roundup,
					total,
				},
			}
		})
	}

	@ManagerModel.bound
	async createOrderWithUserAddressId(
		changer_user_id: number,
		userAddressId: number,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		items: RequestOrderInterface[],
		payment: PaymentRequestInterface | undefined,
		couponCode: string | undefined,
		isMock: boolean = false,
		orderDate: Date | null,
		useWallet: boolean = true,
		newFormat: boolean = false,
		transaction: EntityManager,
	) {
		let userAddress: UserAddressInterface

		if (isMock) {
			if (userAddressId) {
				userAddress = await UserManager.address.getByIds(user.id, userAddressId, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not found!')
					}

					throw err
				})
			} else {
				userAddress = {
					id: -1,
					title: 'Facade Address',
					receiver: 'NN',
					address: '–',
					postal: '11470',
					user_id: user.id,
					created_at: null,
				}
			}

			if (newFormat) {
				return this.createFacadeOrder(
					changer_user_id,
					userAddress,
					user,
					items,
					couponCode,
					{
						useWallet,
					},
					payment,
					transaction,
				)
			} else {
				return this.mockOrder(
					changer_user_id,
					userAddress,
					user,
					items,
					couponCode,
					{
						useWallet,
					},
					transaction,
				)
			}

		} else {
			if (userAddressId) {
				userAddress = await UserManager.address.getByIds(user.id, userAddressId, transaction).catch(err => {
					if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not found!')
					}

					throw err
				})
			} else {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'Address not supplied!')
			}

			return this.createOrder(
				changer_user_id,
				userAddress,
				user,
				items,
				payment,
				couponCode,
				{
					orderDate,
					isInjecting: false,
					sendEmail: true,
					useWallet,
				},
				transaction,
			)
		}
	}

	@ManagerModel.bound
	async createOrderWithoutAddress(
		changer_user_id: number,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		items: RequestOrderInterface[],
		payment: PaymentRequestInterface | undefined,
		couponCode: string | undefined,
		isMock: boolean = false,
		orderDate: Date | null,
		useWallet: boolean = true,
		newFormat: boolean = false,
		transaction: EntityManager,
	) {
		const isAllItemHaveAddress = items.findIndex(item => {
			return isEmpty(item.metadata.address) && (item.metadata as OrderDetailVoucherMetadataInterface).is_physical
		}) === -1

		if (isMock) {
			const defaultAddress: UserAddressInterface = {
				id: -1,
				title: 'Facade Address',
				receiver: 'NN',
				address: '–',
				postal: '11470',
				user_id: user.id,
				created_at: null,
			}

			if (newFormat) {
				return this.createFacadeOrder(
					changer_user_id,
					defaultAddress,
					user,
					items,
					couponCode,
					{
						useWallet,
					},
					payment,
					transaction,
				)
			} else {
				return this.mockOrder(
					changer_user_id,
					defaultAddress,
					user,
					items,
					couponCode,
					{
						useWallet,
					},
					transaction,
				)
			}

		} else if (isAllItemHaveAddress) {
			const defaultAddress: UserAddressInterface = await BrandManager.getMainAddress(DEFAULTS.YUNA_BRAND_ID, transaction).then(address => {
				return {
					...address,
					user_id: DEFAULTS.SYSTEM_USER_ID,
				}
			})

			return this.createOrder(
				changer_user_id,
				defaultAddress,
				user,
				items,
				payment,
				couponCode,
				{
					orderDate,
					isInjecting: false,
					sendEmail: true,
					useWallet,
				},
				transaction,
			)
		} else {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100, 'There are items that have no address!')
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		order_id: number,
		data: Partial<Parameter<OrderInterface>>,
		note: string | undefined,
		transaction: EntityManager,
	) {
		return this.OrderRepository.update(changer_user_id, order_id, data, note, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		id: number | string,
		transaction: EntityManager,
	) {
		return this.OrderRepository.get(id, true, transaction)
	}

	async getOrderDetailById(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getOrderDetailById(order_id, transaction)
		.then(async (order: any) => {
			return {
				...order,
				details: order.details.map(detail => {
					return {
						id: detail.id,
						// dunno mapOne but get plural attribute (variants) instead of singular
						variant_id: detail.variants?.id
					}
				})
			}
		}).catch(() => Promise.resolve({}))
	}

	@ManagerModel.bound
	async getOrderFeedback(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getOrderFeedback(order_id, transaction).then(async order => {
			return {
				...CommonHelper.allowKey(order, 'id', 'created_at', 'updated_at', 'user_id', 'number', 'status', 'metadata', 'cancelled_at', 'user'),
				details: await Promise.all(
					order.details.map(async od => {
						let detailVariant = []
						if (od.type === 'INVENTORY') {
							detailVariant = await this.OrderRepository.getOrderFeedbackVariant(od.id, transaction)
						} else if (od.type === 'STYLESHEET') {
							detailVariant = await this.OrderRepository.getOrderFeedbackStylesheet(od.id, transaction)
						}
						return {
							...od,
							variant: (detailVariant as any).variant
						}
					})
				) 
			}
		})
	}

	@ManagerModel.bound
	async getVariantForFeedback(
		order_id: number,
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getVariantForFeedback(order_id, order_detail_id, transaction)
	}

	@ManagerModel.bound
	async getDetailHistory(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getDetailHistory(order_id, transaction)
	}

	@ManagerModel.bound
	async getEmailOrder(
		ref_id: number,
		type: Array<'ORDER_CONFIRMATION_INCOMPLETE' | 'ORDER_CONFIRMATION_COMPLETE' | 'CHECKOUT'>,
		transaction: EntityManager,
	) {
		return NotificationManager.email.getLatestMail(type, ref_id, transaction)
	}

	@ManagerModel.bound
	async getOrderAddresses(
		order_id: number,
		order_address_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getAddressesByIds(order_id, order_address_ids, transaction)
	}

	@ManagerModel.bound
	async getBCAHistory(
		user_id: number | undefined,
		filter: {
			limit: number,
			offset: number,
		},
		transaction: EntityManager,
	) {
		return this.OrderRepository.getOrderByMethods(user_id, 'BCA', filter, transaction)
	}

	@ManagerModel.bound
	async getOrderAddressesByOrderDetailIds(
		order_detail_ids: number[] | undefined,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getAddressesByOrderDetailIds(order_detail_ids, transaction)
	}

	@ManagerModel.bound
	async getOrderNumbersAndUserFromOrderDetails(
		order_detail_ids: number[],
		transaction: EntityManager,
	) {
		return this.OrderRepository.getOrderNumberAndUserFromOrderDetails(order_detail_ids, transaction)
	}

	@ManagerModel.bound
	async getOrderDetailRelatedToPrices(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getDetailRelatedToPrices(order_id, transaction)
	}

	@ManagerModel.bound
	async filter(
		offset: number | undefined,
		limit: number | undefined,
		filter: {
			search?: string,
			status?: ORDERS,
			date?: Date,
			shipment_date?: Date,
			with_coupon?: boolean,
			with_feedback?: boolean,
			campaign_id?: number,
		} = {},
		transaction: EntityManager,
	) {
		return this.OrderRepository.filter(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	async getDetail(
		order_ids: number[],
		user_id: number | undefined,
		shipment: boolean | undefined,
		ascending: boolean | undefined,
		with_item: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getDetail(order_ids, {
			user_id,
			shipment,
			ascending,
			with_item,
		}, transaction)
	}

	@ManagerModel.bound
	async getCompleteDetail(
		order_ids: number[],
		user_id: number | undefined,
		shipment: boolean | undefined,
		ascending: boolean | undefined,
		with_item: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getDetail(order_ids, {
			user_id,
			shipment,
			ascending,
			with_item,
		}, transaction).then(orders => {
			return Promise.all(orders.map(async order => {
				return {
					order,
					prices: OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets),
					details: await Promise.all(order.orderDetails.map(async orderDetail => {
						const {
							id,
							type,
							product_id,
							product_type,
							isVoucher,
						} = await this.getOrderDetailItemType(orderDetail, transaction)

						let status = null

						if (id) {
							switch (type) {
								case 'INVENTORY': {
									status = await InventoryManager.get(id, transaction).then(i => i.status).catch(err => null)
								} break
								case 'STYLEBOARD': {
									status = await ServiceManager.styleboard.get(id, {}, transaction).then(s => s.status).catch(err => null)
								} break
								case 'STYLESHEET': {
									status = await MatchboxManager.stylesheet.getShallow(id, transaction).then(s => s.status).catch(err => null)
								} break
								case 'WALLET_TRANSACTION': {
									status = null
								} break
								default:
									status = null
									break
							}
						}

						switch (product_type) {
							case 'VARIANT':
								if (product_id) {
									return {
										orderDetail,
										image: await AssetManager.get('variant', product_id, false, transaction).catch(() => null).catch(error => null),
										status,
										title: isVoucher ? `${orderDetail.brand.title} – ${orderDetail.product.title} ${orderDetail.voucher.is_physical ? '' : 'E-'}Gift Card` : `${orderDetail.brand.title} – ${orderDetail.product.title}`,
										inventories: [],
									}
								} else {
									return {
										orderDetail,
										image: null,
										status,
										title: null,
										inventories: [],
									}
								}
							case 'MATCHBOX':
								const inventories = []

								if (orderDetail.orderRequest.type === ORDER_REQUESTS.STYLEBOARD && (orderDetail.orderRequest.metadata as OrderRequestStyleboardMetadataInterface).stylecard_variant_ids?.length && !orderDetail.is_exchange) {
									await ServiceManager.stylecard.getStylecardVariants((orderDetail.orderRequest.metadata as OrderRequestStyleboardMetadataInterface).stylecard_variant_ids, {
										deep: true,
										with_assets: false,
									}, transaction).then(sVs => {
										sVs.forEach(sV => {
											let sIStatus: 'PENDING' | 'CANCELLED' | 'PACKED' | 'PROCESSING' = 'PENDING'

											if (orderDetail.status === ORDER_DETAIL_STATUSES.PROCESSING) {
												sIStatus = 'PROCESSING'
											} else if (orderDetail.status === ORDER_DETAIL_STATUSES.PRIMED || orderDetail.status === ORDER_DETAIL_STATUSES.RESOLVED) {
												sIStatus = 'PACKED'
											} else if (orderDetail.status === ORDER_DETAIL_STATUSES.EXCEPTION) {
												sIStatus = 'CANCELLED'
											}

											inventories.push({
												id: sV.id,
												brand: sV.brand.title,
												title: sV.product?.title,
												category: capitalize(sV.category.title),
												retail_price: sV.retail_price,
												price: sV.price,
												color: sV.colors.map(c => capitalize(c.title)).join('/'),
												size: sV.size.title,
												asset: CommonHelper.allowKey(sV.asset, 'url', 'id'),
												status: sIStatus,
											})
										})
									})
								}

								return {
									orderDetail,
									image: await AssetManager.get('matchbox', product_id, false, transaction).catch(() => null).catch(error => null),
									status,
									title: isVoucher ? `${orderDetail.matchbox.title} Matchbox ${orderDetail.voucher.is_physical ? '' : 'E-'}Gift Card` : `${orderDetail.matchbox.title} Matchbox`,
									inventories,
								}
							case 'SERVICE':
								return {
									orderDetail,
									image: await AssetManager.get('service', product_id, false, transaction).catch(() => null).catch(error => null),
									status,
									title: isVoucher ? `${orderDetail.service.title} Styling ${orderDetail.voucher.is_physical ? '' : 'E-'}Gift Card` : `${orderDetail.service.title} Styling`,
									inventories: [],
								}
							case 'TOPUP':
								return {
									orderDetail,
									image: '//app/topup.jpg',
									status,
									title: isVoucher ? `Wallet Topup ${orderDetail.voucher.is_physical ? '' : 'E-'}Gift Card` : `Wallet Topup`,
									inventories: [],
								}
							default:
								return {
									orderDetail,
									image: null,
									status: null,
									title: null,
									inventories: [],
								}
						}
					})),
				}
			}))
		})
	}

	get getPending() {
		return this.OrderRepository.getPending
	}

	@ManagerModel.bound
	async getHistory(
		order_id: number,
		status: ORDERS[],
		transaction: EntityManager,
	) {
		return this.OrderRepository.getLatestHistory(order_id, status, transaction)
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async payOrder(
		changer_user_id: number,
		order_id: number | string,
		note: string | undefined,
		metadata: any | undefined, // 3rd party metadata
		payment_code: PAYMENTS = PAYMENTS.SUCCESS,
		agent: PAYMENT_AGENTS = PAYMENT_AGENTS.MANUAL,
		shouldSendEmail: boolean = true,
		shouldCatchError: boolean = true,
		transaction: EntityManager,
	) {
		const order = await this.OrderRepository.getDetail(order_id, { with_item: true }, transaction)
		let orderPayment: OrderPaymentInterface
		// Whatever the code, payment proof need to be created

		switch (agent) {
			case PAYMENT_AGENTS.XENDIT:
			case PAYMENT_AGENTS.BCA:
			case PAYMENT_AGENTS.INDODANA:
				orderPayment = await OrderPaymentManager.updateActivePayment(order.id, {
					code: payment_code,
					metadata,
				}, transaction)
				break
			default:
				orderPayment = await OrderPaymentManager.create(order.id, payment_code, agent, undefined, metadata, transaction)
				break
		}

		// =================================
		// Run order product creation
		// =================================

		if (payment_code === PAYMENTS.SUCCESS) {
			if (
				order.status === ORDERS.PENDING_PAYMENT
				|| order.status === ORDERS.EXPIRED
				|| order.status === ORDERS.EXCEPTION
			) {
				await this.OrderRepository.update(changer_user_id, order.id, {
					status: ORDERS.PAID,
				}, note || 'Payment success.', transaction)

				// update user has payment
				await UserManager.updateProfile(order.user_id, {
					has_payment: true,
				}, transaction)

				if (order.status === ORDERS.PENDING_PAYMENT || order.status === ORDERS.EXPIRED || order.status === ORDERS.EXCEPTION) {
					await this.onOrderSuccess(changer_user_id, order, order.user, order.orderDetails.map(orderDetail => {
						return CommonHelper.stripKey({
							...orderDetail,
							request: orderDetail.orderRequest,
						}, 'orderRequest')
					}), transaction).catch(err => {
						if (shouldCatchError) {
							this.warn(err)

							return this.OrderRepository.update(changer_user_id, order.id, {
								status: ORDERS.PAID_WITH_EXCEPTION,
							}, `Payment success but something is wrong with order detail creation. Please check order detail. (${err instanceof ErrorModel ? err.detail || err.message || err : err instanceof Error ? err.message || err : JSON.stringify(err)})`, transaction)
						}

						throw err
					})
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order status could not be changed to PAID from ${order.status}.`)
			}
		} else if (payment_code === PAYMENTS.FAILED) {
			await this.OrderRepository.update(changer_user_id, order.id, {
				status: ORDERS.EXCEPTION,
			}, note || 'Payment failed.', transaction)

			await this.onOrderFailed(changer_user_id, order, order.orderDetails.map(orderDetail => {
				return CommonHelper.stripKey({
					...orderDetail,
					request: orderDetail.orderRequest,
				}, 'orderRequest')
			}), `Payment of ${order.number} failed.`, transaction).catch(async err => {
				if (shouldCatchError) {
					this.warn(err)

					return this.OrderRepository.update(changer_user_id, order.id, {
						status: ORDERS.EXCEPTION,
					}, err instanceof ErrorModel ? err.detail || err.message || err : err instanceof Error ? err.message || err : JSON.stringify(err), transaction)
				}

				throw err
			})
		} else {
			await this.OrderRepository.update(changer_user_id, order.id, {
				status: order.status,
			}, note || `Receive ${payment_code} notification.`, transaction)
		}

		// =================================
		// Send mail
		// =================================

		if (shouldSendEmail && (payment_code === PAYMENTS.SUCCESS || payment_code === PAYMENTS.FAILED)) {
			const {
				subtotal,
				handling,
				shipping,
				discount,
				adjustments,
				point,
				wallet,
				roundup,
				total,
			} = OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

			if (payment_code === PAYMENTS.SUCCESS) {
				await NotificationManager.sendPaymentConfirmationNotice(
					order.user.id,
					order.user.email,
					order.user.profile.first_name,
					order.id,
					order.number,
					TimeHelper.format(order.created_at, 'DD MMM YYYY hh:mm'),
					order.orderDetails,
					FormatHelper.currency(subtotal, 'IDR'),
					FormatHelper.currency(handling, 'IDR'),
					FormatHelper.currency(-discount, 'IDR'),
					FormatHelper.currency(shipping, 'IDR'),
					FormatHelper.currency(adjustments, 'IDR'),
					FormatHelper.currency(point + wallet, 'IDR'),
					FormatHelper.currency(roundup, 'IDR'),
					FormatHelper.currency(total, 'IDR'),
				)

				SegmentHandler.track({
					user_id: changer_user_id,
					event: 'Payment Success',
					properties: {
						id: order_id,
						category: 'Core Ordering',
						value: FormatHelper.currency(subtotal, 'IDR'),
						discount: FormatHelper.currency(-discount, 'IDR'),
						revenue: FormatHelper.currency(total, 'IDR'),
						shipping: FormatHelper.currency(shipping, 'IDR'),
						currency: 'IDR',
						products: this.orderDetailSegment(order.orderDetails),
					},
				})

			} else if (payment_code === PAYMENTS.FAILED) {
				await NotificationManager.sendPaymentCancelationNotice(
					order.id,
					order.user.email,
					order.number,
					order.orderDetails,
					note,
					FormatHelper.currency(subtotal, 'IDR'),
					FormatHelper.currency(handling, 'IDR'),
					FormatHelper.currency(-discount, 'IDR'),
					FormatHelper.currency(shipping, 'IDR'),
					FormatHelper.currency(adjustments, 'IDR'),
					FormatHelper.currency(point + wallet, 'IDR'),
					FormatHelper.currency(roundup, 'IDR'),
					FormatHelper.currency(total, 'IDR'),
				)
			}
		}

		return orderPayment
	}

	@ManagerModel.bound
	async processOrder(
		changer_user_id: number,
		order_id: number,
		note: string | undefined,
		transaction: EntityManager,
	) {
		const status: ORDERS = await this.OrderRepository.getStatus(order_id, transaction)

		if (status === ORDERS.PAID || status === ORDERS.PAID_WITH_EXCEPTION || status === ORDERS.RESOLVED) {
			return this.OrderRepository.update(changer_user_id, order_id, {
				status: ORDERS.PROCESSING,
			}, note || `Start processing order.`, transaction)

			// TODO: SEND MAIL
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order status could not be changed to PROCESSING from ${status}.`)
		}
	}

	@ManagerModel.bound
	async resolveOrder(
		changer_user_id: number,
		order_id: number,
		note: string | undefined,
		force: boolean,
		transaction: EntityManager,
	) {
		const status: ORDERS = await this.OrderRepository.getStatus(order_id, transaction)

		if (status === ORDERS.PROCESSING) {
			const order = await this.OrderRepository.get(order_id, true, transaction)

			// Check if order details are primed
			const notResolved = order.orderDetails.findIndex(orderDetail => orderDetail.status !== ORDER_DETAIL_STATUSES.RESOLVED) > -1

			if (notResolved && !force) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, `Order detail(s) need to be RESOLVED before you can resolve this order.`)
			} else if (notResolved && force) {
				// resolve order detail
				await Promise.all(order.orderDetails.map(async orderDetail => {
					return this.detail.resolveOrderDetail(changer_user_id, orderDetail.id, false, 'Force resolving order detail because order resolving', transaction)
				}))
			}

			return await UserManager.referral.getByUserRef(order.user_id, transaction)
				.then(async data => {
					this.log('and then...')
					this.log('injecting point')
					await UserManager.referral.updateStatus(data.user_id, order.user_id, transaction).then(async bool => {
						if (bool) {
							this.log('finished update status')
							return await UserManager.point.depositPoint(data.user_id, data.amount, STATEMENT_SOURCES.USER, null, `Bonus Referral Point from ${data.user_reference}`, transaction)
						}
					})
					return this.OrderRepository.update(changer_user_id, order_id, {
						status: ORDERS.RESOLVED,
					}, note || `Order concluded.`, transaction)
				})
				.catch(async _ => {
					await UserManager.getUserProfile({ user_id: order.user_id }, transaction).then(user => {
						if (!user.profile.referral_code) {
							const refCode = `${user.profile.first_name}${user.id}`
							UserManager.insertRefCode(user.user_profile_id, refCode, transaction)
						}
					})

					this.log('catched error')

					return this.OrderRepository.update(changer_user_id, order_id, {
						status: ORDERS.RESOLVED,
					}, note || `Order concluded.`, transaction)
				})

			// TODO: SEND MAIL
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order status could not be changed to RESOLVED from ${status}.`)
		}
	}

	@ManagerModel.bound
	async expireOrder(
		changer_user_id: number,
		order_id: number,
		note: string | undefined,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		const status: ORDERS = await this.OrderRepository.getStatus(order_id, transaction)

		if (status === ORDERS.PENDING_PAYMENT) {
			const order = await this.OrderRepository.getDetail(order_id, {}, transaction)

			// =================================
			// Run order failed things
			// =================================
			await this.onOrderFailed(changer_user_id, order, order.orderDetails.map(orderDetail => {
				return CommonHelper.stripKey({
					...orderDetail,
					request: orderDetail.orderRequest,
				}, 'orderRequest')
			}), `Order ${order.number} expired.`, transaction)

			// =================================
			// Update order status
			// =================================

			await this.OrderRepository.update(changer_user_id, order_id, {
				status: ORDERS.EXPIRED,
			}, note || 'Order expired.', transaction)

			// =================================
			// Send mail
			// =================================
			if (shouldSendEmail) {
				const user = await UserManager.get(order.user_id, transaction)

				const {
					subtotal,
					handling,
					shipping,
					discount,
					adjustments,
					point,
					wallet,
					roundup,
					total,
				} = OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

				NotificationManager.sendExpiredOrderNotice(
					order.id,
					user.email,
					order.number,
					TimeHelper.format(order.created_at, 'DD MMMM YYYY'),
					order.orderDetails,
					FormatHelper.currency(subtotal, 'IDR'),
					FormatHelper.currency(handling, 'IDR'),
					FormatHelper.currency(-discount, 'IDR'),
					FormatHelper.currency(shipping, 'IDR'),
					FormatHelper.currency(adjustments, 'IDR'),
					FormatHelper.currency(point + wallet, 'IDR'),
					FormatHelper.currency(roundup, 'IDR'),
					FormatHelper.currency(total, 'IDR'),
					'', // TODO
				)
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'This order is not currently in pending payment. You can only expire a pending payment orders')
		}
	}

	@ManagerModel.bound
	async changeVariantUnavailable(
		variant_id: number,
		transaction: EntityManager,
	) {
		const variant = await ProductManager.variant.getDetail(variant_id, transaction)

		if (variant.quantity === 0 && variant.limited_stock) {
			return await ProductManager.variant.updateAvailable(variant_id, false, transaction)
		} else {
			return false
		}
	}

	@ManagerModel.bound
	async exceptOrder(
		changer_user_id: number,
		order_id: number,
		note: string,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		const status: ORDERS = await this.OrderRepository.getStatus(order_id, transaction).catch(() => {
			return null
		})

		if (status === ORDERS.PENDING_PAYMENT || status === ORDERS.PAID || status === ORDERS.PAID_WITH_EXCEPTION || status === ORDERS.PROCESSING) {
			const order = await this.OrderRepository.getDetail(order_id, {}, transaction)

			// =================================
			// Update order status
			// =================================

			await this.OrderRepository.update(changer_user_id, order_id, {
				status: ORDERS.EXCEPTION,
				cancelled_at: new Date(),
			}, note, transaction)

			// =================================
			// Run order failed things
			// =================================

			if (status === ORDERS.PENDING_PAYMENT || status === ORDERS.PAID || status === ORDERS.PAID_WITH_EXCEPTION) {
				await this.onOrderFailed(changer_user_id, order, order.orderDetails.map(orderDetail => {
					return CommonHelper.stripKey({
						...orderDetail,
						request: orderDetail.orderRequest,
					}, 'orderRequest')
				}), `Order ${order.number} status changed to exception.`, transaction)
			}

			// =================================
			// Add referral if there is any
			// =================================

			// await UserManager.referral.getByUserRef(order.user_id, transaction)
			// 	.then(data => {
			// 		this.log('and then...')
			// 		this.log('injecting point')
			// 		return UserManager.referral.updateStatus(data.user_id, order.user_id, transaction).then(async bool => {
			// 			if (bool) {
			// 				this.log('finished update status')
			// 				await UserManager.point.depositPoint(data.user_id, data.amount, STATEMENT_SOURCES.USER, null, `Bonus Referral Point from ${data.user_reference}`, transaction)
			// 			}
			// 		})
			// 	})

			// =================================
			// Send mail
			// =================================
			if (shouldSendEmail) {
				const user = await UserManager.get(order.user_id, transaction)

				const {
					subtotal,
					handling,
					shipping,
					discount,
					adjustments,
					point,
					wallet,
					roundup,
					total,
				} = OrderUtilitiesManager.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)

				NotificationManager.sendPaymentCancelationNotice(
					order.id,
					user.email,
					order.number,
					order.orderDetails,
					note,
					FormatHelper.currency(subtotal, 'IDR'),
					FormatHelper.currency(handling, 'IDR'),
					FormatHelper.currency(-discount, 'IDR'),
					FormatHelper.currency(shipping, 'IDR'),
					FormatHelper.currency(adjustments, 'IDR'),
					FormatHelper.currency(point + wallet, 'IDR'),
					FormatHelper.currency(roundup, 'IDR'),
					FormatHelper.currency(total, 'IDR'),
				)
			}
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order status could not be changed to EXCEPTION from ${status}.`)
		}
	}

	@ManagerModel.bound
	async postponeOrder(
		order_id: number,
		order_detail_id: number,
		note: string,
		shouldSendEmail: boolean = true,
		transaction: EntityManager,
	) {
		const status: ORDERS = await this.OrderRepository.getStatus(order_id, transaction).catch(() => {
			return null
		})

		if (status === ORDERS.PAID || status === ORDERS.PAID_WITH_EXCEPTION) {
			const order = await this.OrderRepository.getDetail(order_id, {}, transaction)

			const orderDetail = order.orderDetails.find(detail => detail.id === order_detail_id)

			if (!orderDetail) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail not found.`)
			}

			// =================================
			// Update shipment at
			// =================================
			const shipment_at = TimeHelper.calculateShipmentDate(2, TimeHelper.moment(orderDetail.shipment_at))
			await OrderDetailManager.systemUpdate(order_detail_id, {
				shipment_at: shipment_at.toDate(),
			}, note, transaction)

			// =================================
			// Send mail
			// =================================
			// if (shouldSendEmail) {
			// 	const user = await UserManager.get(order.user_id, transaction)

			// 	NotificationManager.sendPostponedOrderNotice(
			// 		user.id,
			// 		user.email,
			// 		order.number,
			// 		TimeHelper.format(order.created_at, 'DD MMMM YYYY'),
			// 		note,
			// 	)
			// }
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order could not be postponed if it's already in ${status}.`)
		}
	}

	@ManagerModel.bound
	async getShallowWithDetails(
		order_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getShallowWithDetails(order_id, transaction)
	}

	@ManagerModel.bound
	async sendCheckoutEmail(
		order_id: number,
		trx: EntityManager,
	) {
		return this.withTransaction(trx, async transaction => {
			return OrderUtilitiesManager.sendCheckoutMail(order_id, transaction)
		})
	}

	@ManagerModel.bound
	async receiveMidtransNotification(
		changer_user_id: number,
		notification: NotificationParameterInterface,
	) {
		const {
			order_number,
			code,
			note,
			metadata,
		} = await OrderPaymentManager.receiveMidtransNotification(notification)

		this.transaction(async transaction => {
			return this.payOrder(
				changer_user_id,
				order_number,
				note,
				metadata,
				code,
				PAYMENT_AGENTS.MIDTRANS,
				true,
				true,
				transaction,
			)
		})

		return true
	}

	@ManagerModel.bound
	async receiveXenditNotification(
		changer_user_id: number,
		token: string,
		data: any,
		transaction: EntityManager,
	) {
		const {
			order_number,
			status,
			metadata,
		} = await OrderPaymentManager.receiveXenditNotification(token, data)

		await this.payOrder(
			changer_user_id,
			order_number,
			'',
			metadata,
			status,
			PAYMENT_AGENTS.XENDIT,
			true,
			true,
			transaction,
		)

		return {
			order_number,
			status,
			metadata,
		}
	}

	@ManagerModel.bound
	async receiveIndodanaNotification(
		changer_user_id: number,
		data: {
			amount: number,
			paymentType: string,
			transactionStatus: string,
			merchantOrderId: string,
			transactionTime: Date,
			transactionId: string,
		},
		transaction: EntityManager,
	) {
		const order = await this.OrderRepository.get(data.merchantOrderId, false, transaction)
		const detail = await this.OrderRepository.getDetail(order.id, {}, transaction)
		const price = OrderUtilitiesManager.getPrices(detail.orderDetails, detail.shipments, detail.points, detail.wallets)
		const payment = await OrderPaymentManager.getLatestPendingPayment(order.id, transaction)

		if (data.transactionStatus === 'PAID' && price.total === data.amount) {
			await this.payOrder(
				changer_user_id,
				order.number,
				'',
				{
					...payment.metadata,
					...data,
				},
				PAYMENTS.SUCCESS,
				PAYMENT_AGENTS.INDODANA,
				true,
				true,
				transaction,
			)

			return {
				order_number: order.number,
				status: PAYMENTS.SUCCESS,
				metadata: data,
			}
		} else {
			throw new ErrorModel(ERRORS.NODATA, ERROR_CODES.ERR_101, 'Payment Failed')
		}
	}

	@ManagerModel.bound
	async receiveFaspayNotification(
		changer_user_id: number,
		notification: PaymentNotificationInterface,
	): Promise<PaymentNotificationResponseInterface> {
		const {
			order_number,
			code,
			note,
			metadata,
		} = await OrderPaymentManager.receiveFaspayNotification(notification)

		this.transaction(async transaction => {
			return this.payOrder(
				changer_user_id,
				order_number,
				note,
				metadata,
				code,
				PAYMENT_AGENTS.FASPAY,
				true,
				true,
				transaction,
			)
		})

		return {
			response: notification.request,
			trx_id: notification.trx_id,
			merchant_id: notification.merchant_id,
			merchant: notification.merchant,
			bill_no: notification.bill_no,
			response_code: '00',
			response_desc: 'Sukses',
			response_date: TimeHelper.moment().format('YYYY-MM-DD HH:mm:ss'),
		}
	}

	@ManagerModel.bound
	async receiveFaspayCreditNotification(
		changer_user_id: number,
		notification: CreditReturnResponseInterface,
	): Promise<PAYMENTS> {
		const {
			order_number,
			code,
			note,
			metadata,
		} = await OrderPaymentManager.receiveFaspayCreditNotification(notification)

		await this.transaction(async transaction => {
			return this.payOrder(
				changer_user_id,
				order_number,
				note,
				metadata,
				code,
				PAYMENT_AGENTS.FASPAY,
				true,
				true,
				transaction,
			)
		})

		return code
	}

	// ============================ PRIVATES ============================
	private async createOrder(
		changer_user_id: number,
		address: InterfaceAddressModel,
		user: UserInterface & {
			profile: UserProfileInterface,
		},
		requests: RequestOrderInterface[],
		payment: PaymentRequestInterface,
		couponCode: string | null = null,
		config: {
			orderDate: Date,
			isInjecting: boolean,
			sendEmail: boolean,
			useWallet: boolean,
		} = {
				orderDate: new Date(),
				isInjecting: false,
				sendEmail: true,
				useWallet: true,
			},
		transaction: EntityManager,
	) {

		const orderDate = config.orderDate
		const isInjecting = CommonHelper.default(config.isInjecting, false)
		const sendEmail = CommonHelper.default(config.sendEmail, true)

		// =================================
		// PRE-VALIDATION
		// =================================

		OrderUtilitiesManager.validateUser(user)
		OrderUtilitiesManager.validateRequest(requests)

		// =================================
		// FLUSH BAG
		// =================================
		this.log('Flush bag..........')

		this.bag.flushBag(
			user.id,
			requests.map(request => {
				return {
					type: request.type,
					ref_id: request.id,
					quantity: request.quantity,
					metadata: request.metadata,
					as_voucher: request.as_voucher,
				}
			}),
			transaction)

		// =================================
		// CREATE ORDER
		// =================================

		this.log('Creating order..........')

		const order = await this.OrderRepository.insert({
			user_id: user.id,
			number: await OrderUtilitiesManager.getOrderNumber(transaction),
			status: ORDERS.PENDING_PAYMENT,
			...(orderDate !== undefined ? { created_at: orderDate } : {}),
			metadata: isInjecting === true ? { injector_id: changer_user_id } : {},
		}, transaction)

		// =================================
		// CREATE ORDER REQUESTS
		// =================================

		this.log('Creating order requests.')

		const orderRequests = await OrderRequestManager.create(order.id, user.id, requests, address, false, transaction)

		// =================================
		// CREATE ORDER DETAILS
		// =================================

		this.log('Creating order details..')

		const {
			orderDetails,
			orderDetailPreCampaigns,
			shipmentPreCampaigns,
		}: {
			orderDetails: Array<Unarray<Await<ReturnType<typeof OrderDetailManager['createFromRequest']>>> & {
				orderDetailCampaigns: OrderDetailCampaignInterface[],
				orderDetailCoupons: OrderDetailCouponInterface[],
			}>,
			orderDetailPreCampaigns: OrderDetailCampaignInterface[],
			shipmentPreCampaigns: ShipmentCampaignInterface[],
		} = await OrderDetailManager.createFromRequest(
			changer_user_id,
			order,
			orderRequests,
			false,
			orderDate,
			transaction,
		).then(async details => {

			// =================================
			// APPLY PRE-CAMPAIGN MODIFICATION
			// =================================

			this.log('Applying pre-campaigns..')

			const preCampaignPromotions = await PromoManager.applyPreCampaignPromotion(
				changer_user_id,
				order,
				user,
				couponCode,
				orderRequests,
				false,
				transaction,
			)

			// Add newly created order request into orderRequests
			orderRequests.push(...preCampaignPromotions.orderRequests)

			return {
				orderDetails: details.concat(preCampaignPromotions.orderDetails).map(detail => {
					return {
						...detail,
						orderDetailCampaigns: [],
						orderDetailCoupons: [],
					}
				}),
				orderDetailPreCampaigns: preCampaignPromotions.orderDetailCampaigns,
				shipmentPreCampaigns: preCampaignPromotions.shipmentCampaigns,
			}
		})

		// =================================
		// CALCULATE SHIPMENTS
		// =================================

		this.log('Calculating shipments...')

		const shipments: Array<Unarray<Await<ReturnType<typeof ShipmentManager['createAutomatedShipmentsFromOrderDetails']>>> & {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		}> = address.id === -1 ? [] : await ShipmentManager.createAutomatedShipmentsFromOrderDetails(orderDetails, {
			is_facade: false,
		}, transaction).then(ss => {
			return ss.map(shipment => {
				return {
					...shipment,
					shipmentCampaigns: [],
					shipmentCoupons: [],
				}
			})
		})

		// =================================
		// CALCULATE COUPON & CAMPAIGN
		// NOTE: coupon can be NULL
		// =================================

		this.log('Calculating discount....')

		const {
			// coupon,
			// campaigns,
			orderDetailCampaigns,
			orderDetailCoupons,
			shipmentCampaigns,
			shipmentCoupons,
		} = await PromoManager.applyPromotionFromCouponAndCampaign(couponCode, user, orderDetails, shipments, {
			orderDetailCampaigns: orderDetailPreCampaigns,
			shipmentCampaigns: shipmentPreCampaigns,
		}, payment, transaction)

		// Insert to DB and mutate orderdetails & shipments
		await Promise.all(orderDetails.map(async orderDetail => {
			const filteredOrderDetailCampaigns = orderDetailCampaigns.filter(odC => odC.order_detail_id === orderDetail.id)
			const filteredOrderDetailCoupons = orderDetailCoupons.filter(odC => odC.order_detail_id === orderDetail.id)

			if (filteredOrderDetailCampaigns.length) {
				await this.OrderDetailRepository.insertCampaign(filteredOrderDetailCampaigns, transaction)
				orderDetail.orderDetailCampaigns.push(...filteredOrderDetailCampaigns)
			}

			if (filteredOrderDetailCoupons.length) {
				await this.OrderDetailRepository.insertCoupon(filteredOrderDetailCoupons, transaction)
				orderDetail.orderDetailCoupons.push(...filteredOrderDetailCoupons)
			}
		}))

		await Promise.all(shipments.map(async shipment => {
			const filteredShipmentCampaigns = shipmentCampaigns.filter(sC => sC.shipment_id === shipment.id)
			const filteredShipmentCoupons = shipmentCoupons.filter(sC => sC.shipment_id === shipment.id)

			if (filteredShipmentCampaigns.length) {
				await this.ShipmentRepository.insertCampaign(filteredShipmentCampaigns, transaction)
				shipment.shipmentCampaigns.push(...filteredShipmentCampaigns)
			}

			if (filteredShipmentCoupons.length) {
				await this.ShipmentRepository.insertCoupon(filteredShipmentCoupons, transaction)
				shipment.shipmentCoupons.push(...filteredShipmentCoupons)
			}
		}))

		// =================================
		// Wallet usage count
		// =================================

		this.log('Apply point & wallet....')

		const userPoint = await UserManager.point.getPoint(user.id, transaction)
		const orderPoints: UserPointTransactionInterface[] = []

		const userWallet = await UserManager.wallet.getWallet(user.id, transaction)
		const orderWallets: UserWalletTransactionInterface[] = []

		if (config.useWallet) {
			let _total = OrderUtilitiesManager.getPrices(orderDetails, shipments, [], []).total
			let pointUsage = Math.min(_total, userPoint.balance)

			if (_total - pointUsage > 0 && _total - pointUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
				pointUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - pointUsage))
			}

			if (pointUsage > 0) {
				orderPoints.push(await UserManager.point.createTransaction(user.id, pointUsage, STATEMENT_SOURCES.ORDER, order.id, 'Point usage.', transaction))

				_total = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, []).total

				if (_total > 0) {
					let walletUsage = Math.min(_total, userWallet.balance)

					if (_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
						walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
					}

					if (walletUsage > 0) {
						orderWallets.push(await UserManager.wallet.pendingWithdrawFrom(userWallet.id, walletUsage, STATEMENT_SOURCES.ORDER, order.id, 'Wallet usage.', transaction))
					}
				}
			} else {
				let walletUsage = Math.min(_total, userWallet.balance)

				if (_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
				}

				if (walletUsage > 0) {
					orderWallets.push(await UserManager.wallet.pendingWithdrawFrom(userWallet.id, walletUsage, STATEMENT_SOURCES.ORDER, order.id, 'Wallet usage.', transaction))
				}
			}
		}

		// =================================
		// Rounding
		// =================================
		await Promise.resolve().then(async () => {
			const {
				total: _total,
			} = OrderUtilitiesManager.getPrices(
				orderDetails,
				shipments,
				orderPoints,
				orderWallets,
			)

			if (_total > 0 && _total < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
				// roundup
				orderWallets.push(await UserManager.wallet.pendingDepositInto(userWallet.id, DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - _total, STATEMENT_SOURCES.ORDER, order.id, 'Order round up.', transaction))
			}
		})

		// =================================
		// CONNECT TO PAYMENT
		// =================================

		this.log('Connecting to payment...')

		const {
			subtotal,
			handling,
			shipping,
			discount,
			adjustments,
			point,
			wallet,
			roundup,
			total,
		} = OrderUtilitiesManager.getPrices(
			orderDetails,
			shipments,
			orderPoints,
			orderWallets,
		)

		const isFree = total === 0 || isInjecting === true
		let result: string
		let xendit: { payment_id: string, payment_code: string, payment_link: string, status: string }
		let bca: { payment_code: string }

		if (!isFree) {
			if (checkPayment(payment)) {
				if (payment.method === PAYMENT_METHOD.VIRTUAL_ACCOUNT && payment.va_bank === VA_BANK.BCA) {
					bca = await OrderPaymentManager.initiateBCATransaction(
						order.id,
						order.number,
						order.user_id,
						`${user.profile.first_name} ${user.profile.last_name}`,
						total,
						transaction,
					)
					// result = await OrderPaymentManager.getFaspayPaymentLink(order.number)
				} else if (payment.method === PAYMENT_METHOD.PAYLATER && payment.agent === PAYMENT_AGENTS.INDODANA) {
					result = await OrderPaymentManager.initiateIndodanaTransaction(
						{
							...order,
							details: orderDetails,
							address: orderDetails[0].request.address,
							price: {
								subtotal,
								handling,
								shipping,
								discount,
								adjustments,
								wallet: wallet + point,
								roundup,
								total,
							},
						},
						user,
						transaction,
					)
				} else {
					xendit = await OrderPaymentManager.initiateXenditTransaction(
						order.id,
						order.number,
						payment.method,
						payment.token,
						payment.auth_id,
						payment.masked_card,
						payment.va_bank,
						payment.ewallet,
						payment.retail_outlet,
						`${user.profile.first_name} ${user.profile.last_name}`,
						payment.phone,
						total,
						TimeHelper.moment(order.created_at).add(6, 'hour').toDate(),
						transaction,
					)
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_100)
			}
		}

		// =================================
		// UPDATE ORDER STATUS
		// =================================

		this.log('Updating order status...', isFree ? '(Free)' : '')

		await this.OrderRepository.update(changer_user_id, order.id, {
			status: ORDERS.PENDING_PAYMENT,
			metadata: {
				...order.metadata,
				...(xendit ? Object.keys(xendit).reduce((i, key) => key === 'status' ? i : { ...i, [key]: xendit[key] }, {}) : null),
				...(bca ? bca : null),
				...(!!payment ? payment.method === PAYMENT_METHOD.PAYLATER ? { indodana_payment_link: result } : { faspay_payment_link: result } : null),
			},
		}, null, transaction)

		// =================================
		// AUTO PAY ORDER IF FREE
		// =================================

		if (total === 0) {
			await this.payOrder(
				changer_user_id,
				order.id,
				'Facade payment made because of free order',
				{},
				PAYMENTS.SUCCESS,
				PAYMENT_AGENTS.MANUAL,
				true,
				true,
				transaction,
			)
		} else if (isInjecting) {
			await this.payOrder(
				changer_user_id,
				order.id,
				`Facade payment made because of injecting order by ${user.id}`,
				{},
				PAYMENTS.PENDING,
				PAYMENT_AGENTS.MANUAL,
				false,
				true,
				transaction,
			)
		} else if (payment.method === PAYMENT_METHOD.CARD && xendit.status === PAYMENTS.SUCCESS) {
			await this.payOrder(
				changer_user_id,
				order.id,
				`Payment Using Credit/Debit Card`,
				{},
				PAYMENTS.SUCCESS,
				PAYMENT_AGENTS.XENDIT,
				true,
				true,
				transaction,
			)
		}

		// =================================
		// SEND MAIL
		// =================================

		if (sendEmail && !isFree) {
			this.log('Sending checkout email..')

			NotificationManager.sendCheckoutConfirmationNotice(
				user.id,
				user.email,
				order.id,
				order.number,
				TimeHelper.format(order.created_at, 'DD MMM YYYY hh:mm'),
				TimeHelper.moment(order.created_at).add(6, 'h').format('DD MMM YYYY hh:mm'),
				orderDetails,
				FormatHelper.readableStatus(order.status),
				FormatHelper.currency(subtotal, 'IDR'),
				FormatHelper.currency(handling, 'IDR'),
				FormatHelper.currency(-discount, 'IDR'),
				FormatHelper.currency(shipping, 'IDR'),
				FormatHelper.currency(adjustments, 'IDR'),
				FormatHelper.currency(point + wallet, 'IDR'),
				FormatHelper.currency(roundup, 'IDR'),
				FormatHelper.currency(total, 'IDR'),
			)
		} else {
			this.log('Skipping sending email..')
		}
		// Change variant availability

		const ods = orderDetails as any
		ods.map(async od => {
			if (od.quantity === 1) {
				await this.changeVariantAvailability(od.item.id, transaction)
			}
		})

		return {
			id: order.id,
			number: order.number,
			created_at: order.created_at,
		}
	}

	private async changeVariantAvailability(
		variant_id: number,
		transaction: EntityManager,
	) {
		const variant = await ProductManager.variant.getDetail(variant_id, transaction)
		if (variant.limited_stock === true || variant.have_consignment_stock === false) {
			return await ProductManager.variant.updateAvailable(variant_id, false, transaction)
		}
	}

	// deprecated?
	private async mockOrder(
		changer_user_id: number,
		address: InterfaceAddressModel,
		user: UserInterface,
		requests: RequestOrderInterface[],
		couponCode: string | null = null,
		config: {
			useWallet: boolean,
		} = {
				useWallet: true,
			},
		transaction: EntityManager,
	) {
		// =================================
		// PRE-VALIDATION
		// =================================

		OrderUtilitiesManager.validateUser(user)
		OrderUtilitiesManager.validateRequest(requests)

		// =================================
		// CREATE ORDER
		// =================================

		this.log('Creating order..........')

		const order: OrderInterface = {
			id: Date.now(),
			user_id: user.id,
			number: '',
			status: ORDERS.PENDING_PAYMENT,
			created_at: new Date(),
		}

		// =================================
		// CREATE ORDER REQUESTS
		// =================================

		this.log('Creating order requests.')

		const orderRequests = await OrderRequestManager.create(order.id, user.id, requests, address, true, transaction)

		// =================================
		// CREATE ORDER DETAILS
		// =================================

		this.log('Creating order details..')
		const order_date = new Date()
		const {
			precampaigns,
			orderDetails,
			orderDetailPreCampaigns,
			shipmentPreCampaigns,
		}: {
			precampaigns: CampaignInterface[],
			orderDetails: Array<Unarray<Await<ReturnType<typeof OrderDetailManager['createFromRequest']>>> & {
				orderDetailCampaigns: OrderDetailCampaignInterface[],
				orderDetailCoupons: OrderDetailCouponInterface[],
			}>,
			orderDetailPreCampaigns: OrderDetailCampaignInterface[],
			shipmentPreCampaigns: ShipmentCampaignInterface[],
		} = await OrderDetailManager.createFromRequest(
			changer_user_id,
			order,
			orderRequests,
			true,
			order_date,
			transaction,
		).then(async details => {
			// =================================
			// APPLY PRE-CAMPAIGN MODIFICATION
			// =================================

			this.log('Applying pre-campaigns..')

			const preCampaignPromotions = await PromoManager.applyPreCampaignPromotion(
				changer_user_id,
				order,
				user,
				couponCode,
				orderRequests,
				true,
				transaction,
			)

			// Add newly created order request into orderRequests
			orderRequests.push(...preCampaignPromotions.orderRequests)

			return {
				orderDetails: details.concat(preCampaignPromotions.orderDetails).map(detail => {
					return {
						...detail,
						orderDetailCampaigns: [],
						orderDetailCoupons: [],
					}
				}),
				orderDetailPreCampaigns: preCampaignPromotions.orderDetailCampaigns,
				shipmentPreCampaigns: preCampaignPromotions.shipmentCampaigns,
				precampaigns: preCampaignPromotions.campaigns,
			}
		})

		// =================================
		// CALCULATE SHIPMENTS
		// =================================

		this.log('Calculating shipments...')

		const shipments: Array<Unarray<Await<ReturnType<typeof ShipmentManager['createAutomatedShipmentsFromOrderDetails']>>> & {
			shipmentCampaigns: ShipmentCampaignInterface[],
			shipmentCoupons: ShipmentCouponInterface[],
		}> = address.id === -1 ? [] : await ShipmentManager.createAutomatedShipmentsFromOrderDetails(orderDetails, {
			is_facade: true,
		}, transaction).then(ss => {
			return ss.map(shipment => {
				return {
					...shipment,
					shipmentCampaigns: [],
					shipmentCoupons: [],
				}
			})
		})

		// =================================
		// CALCULATE COUPON & CAMPAIGN
		// NOTE: coupon can be NULL
		// =================================

		this.log('Calculating discount....')

		const {
			coupon,
			campaigns,
			orderDetailCampaigns,
			orderDetailCoupons,
			shipmentCampaigns,
			shipmentCoupons,
		} = await PromoManager.applyPromotionFromCouponAndCampaign(couponCode, user, orderDetails, shipments, {
			orderDetailCampaigns: orderDetailPreCampaigns,
			shipmentCampaigns: shipmentPreCampaigns,
		}, undefined, transaction)

		// Insert to DB and mutate orderdetails & shipments
		orderDetails.forEach(orderDetail => {
			orderDetail.orderDetailCampaigns.push(...orderDetailCampaigns.filter(odC => odC.order_detail_id === orderDetail.id))
			orderDetail.orderDetailCoupons.push(...orderDetailCoupons.filter(odC => odC.order_detail_id === orderDetail.id))
		})

		shipments.forEach(shipment => {
			shipment.shipmentCampaigns.push(...shipmentCampaigns.filter(sC => sC.shipment_id === shipment.id))
			shipment.shipmentCoupons.push(...shipmentCoupons.filter(sC => sC.shipment_id === shipment.id))
		})

		// =================================
		// Wallet usage count
		// =================================

		this.log('Applying wallet.........')

		const userPoint = await UserManager.point.getPoint(user.id, transaction)
		const userWallet = await UserManager.wallet.getWallet(user.id, transaction)

		const orderPoints: UserPointTransactionInterface[] = []
		const orderWallets: UserWalletTransactionInterface[] = []


		if (config.useWallet && userPoint.balance) {

			let _total = OrderUtilitiesManager.getPrices(orderDetails, shipments, [], []).total

			// applying point first
			let pointUsage = Math.min(_total, userPoint.balance)

			if (_total - pointUsage > 0 && _total - pointUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
				pointUsage -= _total - pointUsage
			}

			if (pointUsage > 0) {
				orderPoints.push({
					id: Date.now(),
					user_point_id: userPoint.id,
					amount: pointUsage,
					type: STATEMENTS.DEBIT,
					source: STATEMENT_SOURCES.ORDER,
					status: WALLET_STATUSES.APPLIED,
					ref_id: order.id,
					created_at: new Date(),
				})

				_total = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, []).total

				// then applying wallet
				if (_total > 0) {
					let walletUsage = Math.min(_total, userWallet.balance)

					if (_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
						walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
					}

					if (walletUsage > 0) {
						orderWallets.push({
							id: Date.now(),
							user_wallet_id: userWallet.id,
							amount: walletUsage,
							type: STATEMENTS.DEBIT,
							source: STATEMENT_SOURCES.ORDER,
							status: WALLET_STATUSES.APPLIED,
							ref_id: order.id,
							created_at: new Date(),
						})
					}
				}
			}  else {
				let walletUsage = Math.min(_total, userWallet.balance)

				if(_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
				}

				if(walletUsage > 0) {
					orderWallets.push({
						id: Date.now(),
						user_wallet_id: userWallet.id,
						amount: walletUsage,
						type: STATEMENTS.DEBIT,
						source: STATEMENT_SOURCES.ORDER,
						status: WALLET_STATUSES.APPLIED,
						ref_id: order.id,
						created_at: new Date(),
					})
				}
			}
		}

		// =================================
		// Rounding
		// =================================
		await Promise.resolve().then(async () => {
			const {
				total: _total,
			} = OrderUtilitiesManager.getPrices(orderDetails, shipments, [], orderWallets)

			if (_total > 0 && _total < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
				// roundup
				orderWallets.push({
					id: Date.now(),
					user_wallet_id: userWallet.id,
					amount: DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - _total,
					type: STATEMENTS.CREDIT,
					source: STATEMENT_SOURCES.ORDER,
					status: WALLET_STATUSES.PENDING,
					note: `Round-up because total order value is less than ${FormatHelper.currency(DEFAULTS.MINIMUM_WITHDRAW_AMOUNT, 'IDR')}`,
					ref_id: order.id,
					created_at: new Date(),
				})
			}
		})

		// =================================
		// Final parsing
		// =================================

		const {
			// OLD
			// subtotal,
			// shipping,
			// discount,
			// wallet,
			// handling,
			// roundup,
			// point,
			// total,
			subtotal,
			shipping,
			discount,
			cashback,
			point,
			wallet,
			handling,
			roundup,
			total,
		} = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, orderWallets)

		return {
			details: await Promise.all(orderDetails.map(async orderDetail => {
				return {
					title: orderDetail.title,
					description: orderDetail.description,
					price: orderDetail.price + orderDetail.handling_fee,
					type: orderDetail.type,
					shipment_at: orderDetail.shipment_at,
					is_digital: orderDetail.is_digital,
					is_exchange: orderDetail.is_exchange,
					request: {
						id: orderDetail.request.id,
						type: orderDetail.request.type,
						metadata: orderDetail.request.metadata,
					},
					// Get asset and additional detail
					...(await Promise.resolve().then(async () => {
						// Switch by order request type
						let asset: InterfaceAssetModel | string = null
						if (orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
							asset = await AssetManager.get('variant', orderDetail.request.ref_id, undefined, transaction).catch(() => null)
						} else if (orderDetail.request.type === ORDER_REQUESTS.MATCHBOX) {
							asset = await AssetManager.get('matchbox', orderDetail.request.ref_id, undefined, transaction).catch(() => null)
						} else if (orderDetail.request.type === ORDER_REQUESTS.SERVICE) {
							asset = await AssetManager.get('service', orderDetail.request.ref_id, undefined, transaction).catch(() => null)
						} else if (orderDetail.request.type === ORDER_REQUESTS.STYLEBOARD) {
							const styleboard = await ServiceManager.styleboard.get(orderDetail.request.ref_id, {
								deep: true,
								with_stylecard: true,
								with_stylecard_variant: true,
								with_assets: false,
							}, transaction)

							return {
								asset: await AssetManager.get('service', styleboard.service_id, undefined, transaction).catch(() => null),
								inventories: styleboard.stylecards.map(stylecard => {
									return stylecard.stylecardVariants.filter(sV => (orderDetail.request.metadata as OrderRequestStyleboardMetadataInterface).stylecard_variant_ids.indexOf(sV.id) > -1)
								}).reduce(CommonHelper.sumArray, []).map(sV => {
									return {
										id: sV.id,
										title: `${sV.brand.title} – ${sV.product.title}`,
										description: `Color: ${sV.colors.map(c => capitalize(c.title)).join('/')}, Size: ${sV.size.title}`,
										asset: sV.asset,
										price: sV.price,
										retail_price: sV.retail_price,
									}
								}),
							}
						} else if (orderDetail.request.type === ORDER_REQUESTS.TOPUP) {
							asset = '//app/topup.jpg'
						} else if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
							const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

							if (voucher.type === VOUCHERS.INVENTORY) {
								const inventory = await InventoryManager.get(voucher.ref_id, transaction)
								asset = await AssetManager.get('variant', inventory.variant_id, undefined, transaction).catch(() => null)
							} else if (voucher.type === VOUCHERS.MATCHBOX) {
								asset = await AssetManager.get('matchbox', voucher.ref_id, undefined, transaction).catch(() => null)
							} else if (voucher.type === VOUCHERS.SERVICE) {
								asset = await AssetManager.get('service', voucher.ref_id, undefined, transaction).catch(() => null)
							} else if (voucher.type === VOUCHERS.TOPUP_AMOUNT) {
								asset = '//app/topup.jpg'
							} else {
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Voucher type invalid (${voucher.type}).`)
							}
						} else {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Order detail request type invalid (${orderDetail.request.type}).`)
						}

						return { asset }
					})),
				}
			})),
			coupons: Object.values(orderDetailCoupons.reduce((sum, oDC) => {
				return {
					...sum,
					[oDC.coupon_id]: {
						id: oDC.coupon_id,
						code: coupon.title,
						discount: (sum[oDC.coupon_id]?.discount ?? 0) + oDC.discount,
						cashback: (sum[oDC.coupon_id]?.cashback ?? 0) + oDC.cashback,
					},
				}
			}, {})),
			campaigns: Object.values(orderDetailCampaigns.reduce((sum, oDC) => {
				const campaign = campaigns.concat((precampaigns as any)).find(c => c.id === oDC.campaign_id)

				return {
					...sum,
					[oDC.campaign_id]: {
						id: oDC.campaign_id,
						code: campaign.title,
						discount: (sum[oDC.campaign_id]?.discount ?? 0) + oDC.discount,
						cashback: (sum[oDC.campaign_id]?.cashback ?? 0) + oDC.cashback,
					},
				}
			}, {})),
			price: {
				// OLD
				// subtotal,
				// shipping,
				// discount,
				// wallet,
				// point,
				// handling,
				// roundup,
				// total,
				subtotal,
				shipping,
				discount,
				cashback,
				point,
				wallet,
				handling,
				roundup,
				total,
			},
		}
	}

	private async createFacadeOrder(
		changer_user_id: number,
		address: InterfaceAddressModel,
		user: UserInterface,
		requests: RequestOrderInterface[],
		coupon_code: string | null = null,
		config: {
			useWallet: boolean,
		} = {
				useWallet: true,
			},
		payment: PaymentRequestInterface,
		transaction: EntityManager,
	) {
		// =================================
		// PRE-VALIDATION
		// =================================

		OrderUtilitiesManager.validateUser(user)
		OrderUtilitiesManager.validateRequest(requests)

		// =================================
		// CREATE ORDER
		// =================================

		this.log('Creating mock order.....')

		const order: OrderInterface = {
			id: Date.now(),
			user_id: user.id,
			number: '',
			status: ORDERS.PENDING_PAYMENT,
			created_at: new Date(),
		}

		// =================================
		// CREATE ORDER REQUESTS
		// =================================

		this.log('Creating order requests.')

		return OrderRequestManager.create(order.id, user.id, requests, address, true, transaction).then(async orderRequests => {
			// =================================
			// CREATE ORDER DETAILS
			// =================================

			this.log('Creating order details..')

			return Promise.all([
				orderRequests,
				OrderDetailManager.composeBagFromRequest(user.id, order, orderRequests, {
					with_shipment: true,
				}, transaction),
			])
		}).then(async ([orderRequests, details]) => {
			// =================================
			// APPLY PRE-CAMPAIGN MODIFICATION
			// =================================
			
			this.log('Applying pre-campaigns..')

			const preCampaignPromotions = await PromoManager.applyPreCampaignPromotion(
				changer_user_id,
				order,
				user,
				coupon_code,
				orderRequests,
				true,
				transaction,
			)

			// Add newly created order request into orderRequests
			orderRequests.push(...preCampaignPromotions.orderRequests)

			let _id = 1
			

			return {
				details: details.concat(await OrderDetailManager.composeBagFromRequest(user.id, order, preCampaignPromotions.orderRequests, {
					with_shipment: true,
				}, transaction)),
				orderDetails: ([] as Array<OrderDetailInterface & {
					request: OrderRequestInterface,
				}>).concat(details.reduce((sum, detail) => {
					return [
						...sum,
						...Array(detail.request.quantity).fill(undefined).map((u, i) => {
							return {
								...detail,
								id: _id++,
							}
						}),
					]
				}, [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface,
				}>)).concat(preCampaignPromotions.orderDetails).map(detail => {
					const request = orderRequests.find(req => req.id === detail.order_request_id)

					return {
						...detail,
						request: {
							...detail.request,
							address: request.address,
						},
						orderDetailCampaigns: [],
						orderDetailCoupons: [],
					}
				}),
				orderDetailPreCampaigns: preCampaignPromotions.orderDetailCampaigns,
				shipmentPreCampaigns: preCampaignPromotions.shipmentCampaigns,
				precampaigns: preCampaignPromotions.campaigns,
			}
		}).then(async ({
			precampaigns,
			details,
			orderDetails,
			orderDetailPreCampaigns,
			shipmentPreCampaigns,
		}) => {

			// =================================
			// CALCULATE SHIPMENTS
			// =================================

			this.log('Calculating shipments...')

			const shipments: Array<Unarray<Await<ReturnType<typeof ShipmentManager['createAutomatedShipmentsFromOrderDetails']>>> & {
				shipmentCampaigns: ShipmentCampaignInterface[],
				shipmentCoupons: ShipmentCouponInterface[],
			}> = address.id === -1 ? [] : await ShipmentManager.createAutomatedShipmentsFromOrderDetails(orderDetails, {
				is_facade: true,
			}, transaction).then(ss => {
				return ss.map(shipment => {
					return {
						...shipment,
						shipmentCampaigns: [],
						shipmentCoupons: [],
					}
				})
			})

			// =================================
			// CALCULATE COUPON & CAMPAIGN
			// NOTE: coupon can be NULL
			// =================================

			this.log('Calculating discount....')

			const {
				coupon,
				campaigns,
				orderDetailCampaigns,
				orderDetailCoupons,
				shipmentCampaigns,
				shipmentCoupons,
			} = await PromoManager.applyPromotionFromCouponAndCampaign(coupon_code, user, orderDetails, shipments, {
				orderDetailCampaigns: orderDetailPreCampaigns,
				shipmentCampaigns: shipmentPreCampaigns,
			}, payment, transaction)

			// Insert to DB and mutate orderdetails & shipments
			orderDetails.forEach(orderDetail => {
				orderDetail.orderDetailCampaigns.push(...orderDetailCampaigns.filter(odC => odC.order_detail_id === orderDetail.id))
				orderDetail.orderDetailCoupons.push(...orderDetailCoupons.filter(odC => odC.order_detail_id === orderDetail.id))
			})

			shipments.forEach(shipment => {
				shipment.shipmentCampaigns.push(...shipmentCampaigns.filter(sC => sC.shipment_id === shipment.id))
				shipment.shipmentCoupons.push(...shipmentCoupons.filter(sC => sC.shipment_id === shipment.id))
			})

			// =================================
			// Wallet usage count
			// =================================

			this.log('Applying point & wallet.')

			const userPoint = await UserManager.point.getPoint(user.id, transaction)
			const userWallet = await UserManager.wallet.getWallet(user.id, transaction)

			const orderPoints: UserPointTransactionInterface[] = []
			const orderWallets: UserWalletTransactionInterface[] = []

			if (config.useWallet) {
				this.log('useWallet')
				let _total = OrderUtilitiesManager.getPrices(orderDetails, shipments, [], []).total

				// applying point first
				let pointUsage = Math.min(_total, userPoint.balance)

				if (_total - pointUsage > 0 && _total - pointUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					pointUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - pointUsage))
				}

				if (pointUsage > 0) {
					orderPoints.push({
						id: Date.now(),
						user_point_id: userPoint.id,
						amount: pointUsage,
						type: STATEMENTS.DEBIT,
						source: STATEMENT_SOURCES.ORDER,
						status: WALLET_STATUSES.APPLIED,
						ref_id: order.id,
						created_at: new Date(),
					})

					_total = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, []).total

					// then applying wallet
					if (_total > 0) {
						let walletUsage = Math.min(_total, userWallet.balance)

						if (_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
							walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
						}

						if (walletUsage > 0) {
							orderWallets.push({
								id: Date.now(),
								user_wallet_id: userWallet.id,
								amount: walletUsage,
								type: STATEMENTS.DEBIT,
								source: STATEMENT_SOURCES.ORDER,
								status: WALLET_STATUSES.APPLIED,
								ref_id: order.id,
								created_at: new Date(),
							})
						}
					}
				} else {
					let walletUsage = Math.min(_total, userWallet.balance)

					if (_total - walletUsage > 0 && _total - walletUsage < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
						walletUsage -= (DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - (_total - walletUsage))
					}

					if (walletUsage > 0) {
						orderWallets.push({
							id: Date.now(),
							user_wallet_id: userWallet.id,
							amount: walletUsage,
							type: STATEMENTS.DEBIT,
							source: STATEMENT_SOURCES.ORDER,
							status: WALLET_STATUSES.APPLIED,
							ref_id: order.id,
							created_at: new Date(),
						})
					}
				}
			}

			// =================================
			// Rounding
			// =================================
			await Promise.resolve().then(async () => {
				const {
					total: _total,
				} = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, orderWallets)

				if (_total > 0 && _total < DEFAULTS.MINIMUM_WITHDRAW_AMOUNT) {
					// roundup
					orderWallets.push({
						id: Date.now(),
						user_wallet_id: userWallet.id,
						amount: DEFAULTS.MINIMUM_WITHDRAW_AMOUNT - _total,
						type: STATEMENTS.CREDIT,
						source: STATEMENT_SOURCES.ORDER,
						status: WALLET_STATUSES.PENDING,
						note: `Round-up because total order value is less than ${FormatHelper.currency(DEFAULTS.MINIMUM_WITHDRAW_AMOUNT, 'IDR')}`,
						ref_id: order.id,
						created_at: new Date(),
					})
				}
			})

			// =================================
			// Final parsing
			// =================================

			const {
				subtotal,
				shipping,
				discount,
				cashback,
				point,
				wallet,
				handling,
				roundup,
				total,
			} = OrderUtilitiesManager.getPrices(orderDetails, shipments, orderPoints, orderWallets)

			return {
				details,
				coupons: Object.values([].concat(orderDetailCoupons).concat(shipmentCoupons).reduce((sum, oDC) => {
					return {
						...sum,
						[oDC.coupon_id]: {
							id: oDC.coupon_id,
							code: coupon.title,
							is_refundable: coupon.is_refundable,
							discount: (sum[oDC.coupon_id]?.discount ?? 0) + oDC.discount,
							cashback: (sum[oDC.coupon_id]?.cashback ?? 0) + oDC.cashback,
						},
					}
				}, {})),
				campaigns: Object.values([].concat(orderDetailCampaigns).concat(shipmentCampaigns).reduce((sum, oDC) => {
					const campaign = campaigns.concat((precampaigns as any)).find(c => c.id === oDC.campaign_id)

					return {
						...sum,
						[oDC.campaign_id]: {
							id: oDC.campaign_id,
							code: campaign?.title,
							is_refundable: campaign?.is_refundable,
							discount: (sum[oDC.campaign_id]?.discount ?? 0) + oDC.discount,
							cashback: (sum[oDC.campaign_id]?.cashback ?? 0) + oDC.cashback,
						},
					}
				}, {})),
				price: {
					subtotal,
					shipping,
					discount,
					cashback,
					point,
					wallet,
					handling,
					roundup,
					total,
				},
			}
		})
	}

	// ===================================================================
	// DO:
	// - release inventory booking that previously made
	// ===================================================================
	private async onOrderFailed(
		changer_user_id: number,
		order: OrderInterface & {
			wallets: UserWalletTransactionInterface[],
		},
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		note: string,
		transaction: EntityManager,
	) {
		// if (!isEmpty(order.wallets)) {
		// 	// Cancel pending transactions
		// 	// Or if already cancelled, check for children recursively
		// 	await Promise.all(order.wallets.map(async wallet => {
		// 		if (wallet.status === WALLET_STATUSES.PENDING) {
		// 			return UserManager.wallet.cancelPendingTransaction(changer_user_id, wallet.id, transaction)
		// 		} else {
		// 			// check children recursive
		// 			const children = await UserManager.wallet.getTransactionsOf(wallet.id, transaction).catch(err => [])
		// 			const amount = (wallet.type === STATEMENTS.CREDIT ? wallet.amount : -wallet.amount) + children.filter(c => c.status === WALLET_STATUSES.APPLIED).map(c => c.type === STATEMENTS.CREDIT ? c.amount : -c.amount).reduce(CommonHelper.sum, 0)

		// 			if (amount !== 0) {
		// 				// Only if credit or debit has already happened
		// 				// Now we deduct / insert from wallet to make the amount back to zero
		// 				if (wallet.type === STATEMENTS.CREDIT) {
		// 					// Inversed because we want to make things 0
		// 					await UserManager.wallet.withdrawFrom(wallet.user_wallet_id, wallet.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, wallet.id, 'Order cancellation.', transaction)
		// 				} else {
		// 					// Inversed because we want to make things 0
		// 					await UserManager.wallet.depositInto(wallet.user_wallet_id, wallet.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, wallet.id, 'Order cancellation.', transaction)
		// 				}
		// 			}
		// 		}

		// 		return true
		// 	}))
		// }

		return orderDetails.reduce((p, orderDetail) => {
			return p.then(isUpdated => {
				return OrderDetailManager.exceptOrderDetail(changer_user_id, orderDetail, note, transaction).then(_isUpdated => _isUpdated && isUpdated)
			})
		}, Promise.resolve(true)).then(async isUpdated => {
			if (isUpdated) {
				await this.OrderDetailRepository.getOrderWithDetails('order', order.id, transaction).then(async ({
					orderDetails: oD,
				}) => {
					if (oD.findIndex(detail => detail.status !== ORDER_DETAIL_STATUSES.EXCEPTION) === -1) {
						await this.update(changer_user_id, order.id, {
							status: ORDERS.EXCEPTION,
						}, 'Auto update because all order details are exceptioned.', transaction)
					}
				})

				return true
			} else {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Error when updating order detail.')
			}
		})
	}

	// ===================================================================
	// DO:
	// 	- Create stylesheet
	// 	- Activate voucher -> need to book the inventory or else voucher become exception order detail exception
	// - FROM PENDING:
	// 	- Used voucher need to be fully redeemed
	// 	- Booking inventory need to be moved from voucher to order detail
	// - FROM EXPIRED:
	//  - Available voucher need to be fully redeemed -> if not, order become paid with exception
	//  - Booking inventory -> if can't, order become paid with exception
	// ===================================================================
	private async onOrderSuccess(
		changer_user_id: number,
		order: OrderInterface & {
			wallets: UserWalletTransactionInterface[],
		},
		user: UserInterface & { profile: UserProfileInterface },
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		transaction: EntityManager,
	) {
		if (!isEmpty(order.wallets)) {
			// ========================================================
			// Apply pending transactions
			// Or if already applied, check for children recursively
			// To apply necessary adjustment, ie: if it's cancelled
			// and re-processed, it should be re-adjusted
			// ========================================================
			await Promise.all(order.wallets.map(async wallet => {
				if (wallet.status === WALLET_STATUSES.PENDING) {
					return UserManager.wallet.applyPendingTransaction(changer_user_id, wallet.id, transaction)
				} else {
					const children = await UserManager.wallet.getTransactionsOf(wallet.id, transaction).catch(err => [])
					const amount = (wallet.type === STATEMENTS.CREDIT ? wallet.amount : -wallet.amount) + children.filter(c => c.status === WALLET_STATUSES.APPLIED).map(c => c.type === STATEMENTS.CREDIT ? c.amount : -c.amount).reduce(CommonHelper.sum, 0)

					if (amount === 0) {
						// ========================================================
						// No debit or credit happened
						// Now we deduct / insert from wallet
						// ========================================================
						if (wallet.type === STATEMENTS.CREDIT) {
							await UserManager.wallet.depositInto(wallet.user_wallet_id, wallet.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, wallet.id, 'Re-applying wallet usage.', transaction)
						} else {
							await UserManager.wallet.withdrawFrom(wallet.user_wallet_id, wallet.amount, STATEMENT_SOURCES.WALLET_TRANSACTION, wallet.id, 'Re-applying wallet usage.', transaction)
						}
					}
				}

				return true
			}))
		}

		return orderDetails.reduce((p, orderDetail) => {
			return p.then(isUpdated => {
				return OrderDetailManager.startOrderDetail(changer_user_id, user, order, orderDetail, transaction).then(_isUpdated => isUpdated && _isUpdated)
			})
		}, Promise.resolve(true)).then(async isUpdated => {
			if (isUpdated) {
				await this.OrderDetailRepository.getOrderWithDetails('order', order.id, transaction).then(async ({
					orderDetails: oD,
				}) => {
					const hasException = oD.findIndex(detail => detail.status === ORDER_DETAIL_STATUSES.EXCEPTION) > -1
					const allResolved = oD.findIndex(detail => detail.status !== ORDER_DETAIL_STATUSES.RESOLVED) === -1
					const hasOngoing = oD.findIndex(detail => detail.status === ORDER_DETAIL_STATUSES.PROCESSING || detail.status === ORDER_DETAIL_STATUSES.PRIMED || detail.status === ORDER_DETAIL_STATUSES.RESOLVED) > -1

					if (hasException) {
						await this.OrderRepository.update(changer_user_id, order.id, {
							status: ORDERS.PAID_WITH_EXCEPTION,
						}, `Payment success but something is wrong with order detail creation. Please check order detail.`, transaction)
					} else if (allResolved) {
						await this.OrderRepository.update(changer_user_id, order.id, {
							status: ORDERS.RESOLVED,
						}, `Payment success and all of it's details are resolved.`, transaction)
					} else if (hasOngoing) {
						await this.OrderRepository.update(changer_user_id, order.id, {
							status: ORDERS.PROCESSING,
						}, `Payment success and some / all of it's detail are processing.`, transaction)
					}
				})

				return true
			} else {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_105, 'Error when updating order detail.')
			}
		})
	}

	private async getOrderDetailItemType(orderDetail: OrderDetailInterface & {
		orderRequest: OrderRequestInterface,
	}, transaction: EntityManager): Promise<{
		id: number,
		type: 'INVENTORY' | 'STYLESHEET' | 'STYLEBOARD' | 'WALLET_TRANSACTION',
		product_id: number,
		product_type: 'VARIANT' | 'MATCHBOX' | 'INVENTORY' | 'SERVICE' | 'TOPUP' | 'INVALID',
		isVoucher: boolean,
	}> {
		const voucher = orderDetail.type === ORDER_DETAILS.VOUCHER ? await VoucherManager.get(orderDetail.ref_id, false, transaction) : null
		const id = voucher ? voucher.ref_id : orderDetail.ref_id

		return Promise.resolve().then(() => {
			switch (orderDetail.orderRequest.type) {
				case ORDER_REQUESTS.VARIANT: {
					return {
						id,
						type: 'INVENTORY',
						product_id: orderDetail.orderRequest.ref_id,
						product_type: 'VARIANT',
					}
				} case ORDER_REQUESTS.STYLEBOARD: {
					return {
						id,
						type: 'STYLESHEET',
						product_id: DEFAULTS.STYLING_RESULT_MATCHBOX_ID,
						product_type: 'MATCHBOX',
					}
				} case ORDER_REQUESTS.MATCHBOX: {
					return {
						id,
						type: 'STYLESHEET',
						product_id: orderDetail.orderRequest.ref_id,
						product_type: 'MATCHBOX',
					}
				} case ORDER_REQUESTS.SERVICE: {
					return {
						id,
						type: 'STYLEBOARD',
						product_id: orderDetail.orderRequest.ref_id,
						product_type: 'SERVICE',
					}
				} case ORDER_REQUESTS.TOPUP: {
					return {
						id: null,
						type: 'WALLET_TRANSACTION',
						product_id: orderDetail.orderRequest.ref_id,
						product_type: 'TOPUP',
					}
				} case ORDER_REQUESTS.VOUCHER: {
					return VoucherManager.get(orderDetail.orderRequest.ref_id, false, transaction).then(async v => {
						switch (v.type) {
							case VOUCHERS.INVENTORY:
								return {
									id: v.ref_id,
									type: 'INVENTORY',
									product_id: v.ref_id,
									product_type: 'INVENTORY',
								}
							case VOUCHERS.MATCHBOX:
								return {
									id: null,
									type: 'STYLESHEET',
									product_id: v.ref_id,
									product_type: 'MATCHBOX',
								}
							case VOUCHERS.SERVICE:
								return {
									id: null,
									type: 'STYLEBOARD',
									product_id: v.ref_id,
									product_type: 'SERVICE',
								}
							case VOUCHERS.TOPUP_AMOUNT:
								const topupAmount = await TopupManager.getTopupAmount(v.ref_id, transaction)

								return {
									id: null,
									type: 'WALLET_TRANSACTION',
									product_id: topupAmount.topup_id,
									product_type: 'TOPUP',
								}
							default:
								return {
									id: null,
									type: null,
									product_id: null,
									product_type: 'INVALID',
									isVoucher: false,
								}
						}
					})
				} default:
					return {
						id: null,
						type: null,
						product_id: null,
						product_type: 'INVALID',
						isVoucher: false,
					}
			}
		}).then((result: any) => {
			return {
				isVoucher: orderDetail.type === ORDER_DETAILS.VOUCHER,
				...result,
			}
		})
	}

	private orderDetailSegment(orderDetails: Array<OrderDetailInterface & {
		matchbox?: MatchboxInterface,
		service?: ServiceInterface,
		product?: ProductInterface,
		brand?: BrandInterface,
	}>) {
		return orderDetails.reduce((init, arr) => {
			switch (arr.type) {
				case ORDER_DETAILS.STYLEBOARD:
					return [
						...init,
						{
							product_id: arr.service?.title,
							sku: null,
							product_category: ORDER_DETAILS.STYLEBOARD,
							name: arr.service?.title,
							brand: 'Yuna & Co.',
							variant: null,
							price: arr.price,
							quantity: 1,
							image_url: null,
						},
					]
				case ORDER_DETAILS.STYLESHEET:
					return [
						...init,
						{
							product_id: arr.matchbox?.title,
							sku: null,
							product_category: ORDER_DETAILS.STYLESHEET,
							name: arr.matchbox?.title,
							brand: 'Yuna & Co.',
							variant: null,
							price: arr.price,
							quantity: 1,
							image_url: null,
						},
					]
				case ORDER_DETAILS.INVENTORY:
					const i = findIndex(init, { product_id: arr.product?.id })

					if (i !== -1) {
						init[i] = { ...init[i], quantity: (init[i].quantity + 1) }

						return init
					} else {
						return [
							...init,
							{
								product_id: arr.product?.title,
								sku: null,
								product_category: ORDER_DETAILS.INVENTORY,
								name: arr.product?.title,
								brand: arr.brand?.title,
								variant: arr.description,
								price: arr.price,
								quantity: 1,
								image_url: null,
							},
						]
					}
				case ORDER_DETAILS.VOUCHER:
					return [
						...init,
						{
							product_id: arr.matchbox?.title,
							sku: null,
							product_category: ORDER_DETAILS.VOUCHER,
							name: arr.matchbox?.title,
							brand: 'Voucher Yuna & Co.',
							variant: null,
							price: arr.price,
							quantity: 1,
							image_url: null,
						},
					]
			}
		}, [] as Array<{
			product_id: number,
			sku: string,
			product_category: string,
			name: string,
			brand: string,
			variant: string,
			price: number,
			quantity: number,
			image_url: string,
		}>)
	}
}

export default new OrderManager()
