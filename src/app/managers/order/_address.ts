import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	OrderRepository,
} from 'app/repositories'

import InterfaceAddressModel from 'energie/app/models/interface.address'
import { OrderAddressInterface } from 'energie/app/interfaces'

import { Parameter } from 'types/common'

import bcrypt from 'bcrypt'



class OrderAddressManager extends ManagerModel {

	static __displayName = 'OrderAddressManager'

	protected OrderRepository: OrderRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			OrderRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		order_id: number,
		data: Parameter<InterfaceAddressModel>,
		is_facade: boolean | undefined,
		transaction: EntityManager,
	) {
		if(is_facade) {
			return {
				id: await bcrypt.hash(`${ order_id }${ data.title }${ data.receiver }${ data.address }${ data.phone }${ data.district }${ data.postal }`, 1) as any as number,
				order_id,
				title: data.title,
				receiver: data.receiver,
				phone: data.phone,
				address: data.address,
				district: data.district,
				postal: data.postal,
				location_id: data.location_id,
				metadata: data.metadata,
				created_at: new Date(),
			}
		} else {
			return this.OrderRepository.insertAddress({
				order_id,
				title: data.title,
				receiver: data.receiver,
				phone: data.phone,
				address: data.address,
				district: data.district,
				postal: data.postal,
				location_id: data.location_id,
				metadata: data.metadata,
			}, transaction)
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		order_id: number,
		order_address_id: number,
		update: Partial<Parameter<OrderAddressInterface>>,
		transaction: EntityManager,
	) {
		return this.OrderRepository.updateAddress(order_id, order_address_id, update, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		order_id: number,
		order_address_id: number,
		transaction: EntityManager,
	) {
		return this.OrderRepository.getAddressesByIds(order_id, [order_address_id], transaction).then(orderAddresses => {
			return orderAddresses[0]
		})
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================


}
export default new OrderAddressManager()
