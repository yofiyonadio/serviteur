import { ErrorModel, ManagerModel } from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import FeedbackManager from '../feedback'
import InventoryManager from '../inventory'
import MasterManager from '../master'
import MatchboxManager from '../matchbox'
import NotificationManager from '../notification'
import OrderManager from '../order'
import PacketManager from '../packet'
import ProductManager from '../product'
import UserManager from '../user'

import StylesheetRepository from 'app/repositories/stylesheet'

import { StylesheetInventoryInterface, StylesheetInterface, PacketInterface, CategoryInterface, InventoryInterface, PurchaseRequestInterface, OrderDetailInterface, OrderDetailCampaignInterface, OrderDetailCouponInterface, FeedbackAnswerInterface, PurchaseOrderInterface, OrderDetailTransactionInterface, UserWalletTransactionInterface, ShipmentInterface } from 'energie/app/interfaces'

import { FormatHelper, CommonHelper } from 'utils/helpers'
import { Parameter } from 'types/common'

import { ERROR_CODES, ERRORS } from 'app/models/error'
import { BOOKING_SOURCES, INVENTORIES, ORDER_DETAIL_STATUSES, STYLESHEET_STATUSES, PURCHASES, PURCHASE_REQUESTS, ORDERS, FEEDBACKS, STYLESHEET_INVENTORY_STATUSES, STATEMENT_SOURCES, ORDER_REQUESTS, STATEMENTS, STYLESHEETS } from 'energie/utils/constants/enum'

import { capitalize, isEmpty, memoize } from 'lodash'

class MatchboxStylesheetManager extends ManagerModel {

	static __displayName = 'MatchboxStylesheetManager'

	protected StylesheetRepository: StylesheetRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			StylesheetRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		data: Parameter<StylesheetInterface>,
		user_asset_ids: number[] | undefined,
		packet: Parameter<PacketInterface> | undefined,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.insert({
			...data,
			packet_id: !isEmpty(packet) ? await PacketManager.create(packet, transaction).then(p => p.id) : await PacketManager.duplicate(await MatchboxManager.get(data.matchbox_id, false, transaction).then(matchbox => matchbox.packet_id), false, transaction),
		}, user_asset_ids, transaction)
	}

	async createInventory(changer_user_id: number, stylesheet_id: number, type: 'INVENTORY' | 'VARIANT', inventory_id: number, is_locked: boolean, trx: EntityManager): Promise<StylesheetInventoryInterface>
	async createInventory(changer_user_id: number, stylesheet_id: number, type: 'PURCHASE_REQUEST', data: Parameter<PurchaseRequestInterface>, is_locked: boolean, trx: EntityManager): Promise<StylesheetInventoryInterface>

	@ManagerModel.bound
	async createInventory(
		changer_user_id: number,
		stylesheet_id: number,
		type: 'INVENTORY' | 'VARIANT' | 'PURCHASE_REQUEST',
		idOrData: number | Parameter<PurchaseRequestInterface>,
		is_locked: boolean = false,
		transaction: EntityManager,
	): Promise<any> {
		return Promise.resolve().then(async () => {
			switch (type) {
			case 'INVENTORY':
			default:
				return {
					inventory: await InventoryManager.getWithVariant(idOrData as number, transaction),
				}
			case 'VARIANT':
				return {
					inventory: await InventoryManager.getIdFromVariant(idOrData as number, 1, transaction).then(async inventories => {
						return InventoryManager.getWithVariant(inventories[0], transaction)
					}),
				}
			case 'PURCHASE_REQUEST':
				return {
					// create from metadata interface
					purchase_request: await InventoryManager.purchase.createRequest(changer_user_id, {
						...idOrData as Parameter<PurchaseRequestInterface>,
					}, transaction),
				}
			}
		}).then(async ({
			inventory,
			purchase_request,
		}) => {
			if(inventory) {
				return InventoryManager.book(changer_user_id, inventory.id, BOOKING_SOURCES.STYLESHEET, stylesheet_id, '', true, transaction).then(isBooked => {
					if (isBooked) {
						return this.StylesheetRepository.setInventory({
							stylesheet_id,
							inventory_id: inventory.id,
							purchase_request_id: null,
							retail_price: inventory.variant.retail_price,
							// test
							price: inventory.variant.price,
							is_locked,
							is_refunded: false,
						}, transaction)
					} else {
						throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Inventory already booked.')
					}
				})
			} else if(purchase_request) {
				if(purchase_request.variant_id) {
					const variant = await ProductManager.variant.get(purchase_request.variant_id, {}, transaction)

					return this.StylesheetRepository.setInventory({
						stylesheet_id,
						inventory_id: null,
						purchase_request_id: purchase_request.id,
						retail_price: variant.retail_price,
						price: variant.price,
						is_locked,
						is_refunded: false,
					}, transaction)
				} else {
					return this.StylesheetRepository.setInventory({
						stylesheet_id,
						inventory_id: null,
						purchase_request_id: purchase_request.id,
						retail_price: purchase_request.retail_price,
						price: purchase_request.price,
						is_locked,
						is_refunded: false,
					}, transaction)
				}
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'No inventory nor purchase request found.')
			}
		})
	}

	@ManagerModel.bound
	async createReplacementStylesheetInventory(
		changer_user_id: number,
		stylesheet_id: number,
		type: 'replacement' | 'styleboard',
		stylesheetInventory: StylesheetInventoryInterface & {
			inventory?: InventoryInterface,
			purchaseRequest?: PurchaseRequestInterface,
		},
		transaction: EntityManager,
	) {
		if (stylesheetInventory.inventory) {
			// ==============================================================
			// Make sure that the inventory is not being booked by anything
			// because it should be unavailable!
			// ==============================================================
			if(type === 'replacement' && stylesheetInventory.inventory.status === INVENTORIES.BOOKED) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Inventory #${stylesheetInventory.inventory_id} is being booked by another source.`)
			}

			// ==============================================================
			// Briefly change inventory status to available
			// for later be used in bookings
			// ==============================================================
			await InventoryManager.update(changer_user_id, stylesheetInventory.inventory_id, {
				status: INVENTORIES.AVAILABLE,
				note: 'Temporarily made available to be booked by new stylesheet.',
			}, undefined, transaction)

			// ==============================================================
			// Book the inventory
			// ==============================================================
			await InventoryManager.book(changer_user_id, stylesheetInventory.inventory_id, BOOKING_SOURCES.STYLESHEET, stylesheet_id, 'Inventory moved to new stylesheet.', true, transaction)

			// ==============================================================
			// Put it into stylesheet inventory
			// ==============================================================
			return this.StylesheetRepository.setInventory({
				stylesheet_id,
				inventory_id: stylesheetInventory.inventory_id,
				purchase_request_id: null,
				retail_price: stylesheetInventory.retail_price,
				price: stylesheetInventory.price,
				is_locked: true,
				is_refunded: false,
			}, transaction).then(async sI => {
				// ==============================================================
				// Update parent to have reference to this new stylesheet inv
				// ==============================================================
				await this.StylesheetRepository.updateInventory(stylesheetInventory.id, {
					stylesheet_inventory_id: sI.id,
					is_locked: true,
				}, transaction)

				return sI
			})
		} else if (stylesheetInventory.purchaseRequest) {
			// ==============================================================
			// Purchase Request must be RESOLVED or LOCKED
			// ==============================================================
			if (stylesheetInventory.purchaseRequest.status !== PURCHASE_REQUESTS.RESOLVED && stylesheetInventory.purchaseRequest.status !== PURCHASE_REQUESTS.LOCKED) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Purchase request must be resolved or locked before replaced.')
			}

			// ==============================================================
			// Duplicate the PR
			// ==============================================================
			const purchaseRequest = await InventoryManager.purchase.createRequest(changer_user_id, {
				user_id: stylesheetInventory.purchaseRequest.user_id,
				variant_id: stylesheetInventory.purchaseRequest.variant_id,
				url: stylesheetInventory.purchaseRequest.url,
				brand_id: stylesheetInventory.purchaseRequest.brand_id,
				seller_id: stylesheetInventory.purchaseRequest.seller_id,
				title: stylesheetInventory.purchaseRequest.title,
				category_id: stylesheetInventory.purchaseRequest.category_id,
				color_id: stylesheetInventory.purchaseRequest.color_id,
				size_id: stylesheetInventory.purchaseRequest.size_id,
				retail_price: stylesheetInventory.purchaseRequest.retail_price,
				price: stylesheetInventory.purchaseRequest.price,
				status: PURCHASE_REQUESTS.PENDING,
				quantity: 1,
			}, transaction)

			// ==============================================================
			// Put it into stylesheet inventory
			// ==============================================================
			return this.StylesheetRepository.setInventory({
				stylesheet_id,
				inventory_id: null,
				purchase_request_id: purchaseRequest.id,
				retail_price: stylesheetInventory.retail_price,
				price: stylesheetInventory.price,
				is_locked: true,
				is_refunded: false,
			}, transaction).then(async sI => {
				// ==============================================================
				// Update parent to have reference to this new stylesheet inv
				// ==============================================================
				await this.StylesheetRepository.updateInventory(stylesheetInventory.id, {
					stylesheet_inventory_id: sI.id,
					is_locked: true,
				}, transaction)

				return sI
			})
		} else {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'No inventory nor purchase request specified when creating replacement.')
		}
	}

	@ManagerModel.bound
	async createOrUpdateFeedbackAnswers(
		stylesheet_inventory_id: number,
		user_id: number,
		order_detail_id: number,
		answers: FeedbackAnswerInterface[],
		transaction: EntityManager,
	) {
		const feedbacks = await FeedbackManager.getByOrderDetailId(user_id, order_detail_id, transaction).catch(err => [])
		const variant = await this.StylesheetRepository.getVariantId(stylesheet_inventory_id, transaction)
		const feedback_id = feedbacks.filter(f => f.type === FEEDBACKS.VARIANT && f.ref_id === variant).map(feedback => feedback.id)[0]
		const question_ids = feedbacks.filter(f => f.type === FEEDBACKS.VARIANT && f.ref_id === variant).map(feedback => {
				return feedback.answers.map(answer => {
					return answer.question_id
				})
			})[0]

		if(feedback_id) {
			return answers.map(async answer => {
				if(question_ids.indexOf(answer.question_id) > -1) {
					await FeedbackManager.updateAnswers(feedback_id, [answer], transaction)
				} else {
					await FeedbackManager.createAnswers(feedback_id, [answer], transaction)
				}
			})
		} else {
			await FeedbackManager.create({
				user_id,
				order_detail_id,
				type: FEEDBACKS.VARIANT,
				ref_id: variant,
			}, answers, transaction)
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		stylesheet_id: number,
		update: Partial<Parameter<StylesheetInterface, 'packet_id'>>,
		note: string | undefined,
		packet: Parameter<PacketInterface> | undefined,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.update(changer_user_id, stylesheet_id, update, note, transaction).then(async isUpdated => {
			if(isUpdated && !isEmpty(packet)) {
				await PacketManager.createOrUpdate('STYLESHEET', stylesheet_id, undefined, packet, transaction)
			}

			return isUpdated
		}).then(async isUpdated => {
			if(isUpdated && update.status === STYLESHEET_STATUSES.STYLING) {
				// update order detail to processing
				const orderDetail = await this.getOrderDetailOf(stylesheet_id, transaction).catch(err => null as OrderDetailInterface)

				if (orderDetail && (orderDetail.status === ORDER_DETAIL_STATUSES.PENDING || orderDetail.status === ORDER_DETAIL_STATUSES.EXCEPTION)) {
					await OrderManager.detail.update(changer_user_id, orderDetail.id, {
						status: ORDER_DETAIL_STATUSES.PROCESSING,
					}, 'Auto update because stylesheet is updated into styling.', transaction)

					const order = await OrderManager.getShallowWithDetails(orderDetail.order_id, transaction)
					const stylist = await UserManager.getStylist(order.user_id, transaction)

					// Send notice
					NotificationManager.sendStartStylingNotice(
						order.user_id,
						'STYLESHEET',
						stylist.profile.stage_name,
					)

					if(order.status === ORDERS.PAID && order.orderDetails.findIndex(detail => detail.status === ORDER_DETAIL_STATUSES.PENDING) === -1) {
						await OrderManager.update(changer_user_id, order.id, {
							status: ORDERS.PROCESSING,
						}, 'Auto update because stylesheet is updated into styling and all details already processing.', transaction)
					}

					return isUpdated
				}
			} else if (isUpdated && update.status === STYLESHEET_STATUSES.PUBLISHED) {
				const orderDetail = await this.getOrderDetailOf(stylesheet_id, transaction).catch(err => null as OrderDetailInterface)

				if (orderDetail) {
					const stylesheet = await this.StylesheetRepository.getDetailedInventoriesFromStylesheet(stylesheet_id, transaction)

					if (stylesheet.title) {
						await OrderManager.detail.update(changer_user_id, orderDetail.id, {
							title: stylesheet.title,
						}, 'Updating order detail title to match with stylesheet title', transaction)
					}

					await Promise.all(stylesheet.stylesheetInventories.map(async st_i => {
						if(st_i.inventory?.variant?.price && st_i.inventory?.variant.retail_price) {
							await MatchboxManager.stylesheet.updateInventory(st_i.id, {
								price: st_i.inventory.variant.price,
								retail_price: st_i.inventory.variant.retail_price,
							}, transaction)
						}
					}))
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async updateInventory(
		stylesheet_inventory_id: number,
		update: Partial<Parameter<StylesheetInventoryInterface>>,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.updateInventory(stylesheet_inventory_id, update, transaction)
	}

	@ManagerModel.bound
	async updateStylist(
		changer_user_id: number,
		stylesheet_id: number,
		stylist_id: number,
		note: string,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.update(changer_user_id, stylesheet_id, {
			stylist_id,
		}, note, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number | undefined,
		limit: number | undefined,
		filter: {
			type?: STYLESHEETS,
			search?: string,
			date?: Date,
			status?: STYLESHEET_STATUSES,
			user_id?: number,
			have_order?: boolean,
			with_inventories?: boolean,
			with_history?: boolean,
			no_shipment?: boolean,
			without_po?: boolean,
			newest_first?: boolean,
			purchase_request_status?: PURCHASE_REQUESTS,
			order_detail_status?: ORDER_DETAIL_STATUSES,
		} = {},
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.filter(offset, limit, filter, transaction)
	}

	@ManagerModel.bound
	async getAddress(
		stylesheet_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getAddress(stylesheet_ids, transaction)
	}

	@ManagerModel.bound
	async getNotes(
		stylesheet_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getNotes(stylesheet_ids, transaction)
	}

	@ManagerModel.bound
	async getInvoices(
		stylesheet_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getInvoices(stylesheet_ids, transaction)
	}

	@ManagerModel.bound
	async getShallow(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.get(stylesheet_id, false, false, transaction)
	}

	@ManagerModel.bound
	async getShallowWithInventory(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.get(stylesheet_id, false, true, transaction)
	}

	@ManagerModel.bound
	async getDetailed(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getDetailed(stylesheet_id, transaction)
	}

	@ManagerModel.bound
	async getDeepWithInventory(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.get(stylesheet_id, true, true, transaction)
	}

	@ManagerModel.bound
	async getDetailedInventoriesFromStylesheet(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getDetailedInventoriesFromStylesheet(stylesheet_id, transaction)
	}

	@ManagerModel.bound
	async getDetailedInventories(
		stylesheet_inventory_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getDetailedInventories(stylesheet_inventory_ids, false, transaction)
	}

	@ManagerModel.bound
	async getDetailedInventoriesWithHistory(
		stylesheet_inventory_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getDetailedInventories(stylesheet_inventory_ids, true, transaction)
	}

	@ManagerModel.bound
	async getInventoriesFromStylesheet(
		stylesheet_id: number,
		with_order_detail: boolean = false,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getInventoriesFromStylesheet(stylesheet_id, with_order_detail, transaction)
	}

	@ManagerModel.bound
	async getInventoryOfPurchaseRequest(
		purchase_request_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getInventoryOfPurchaseRequest(purchase_request_id, transaction)
	}

	@ManagerModel.bound
	async getStylesheetInventories(
		stylesheet_inventory_ids: number[],
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getStylesheetInventories(stylesheet_inventory_ids, transaction)
	}

	@ManagerModel.bound
	async getUserStyleHistory(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getStyleHistory(user_id, transaction)
	}

	@ManagerModel.bound
	async getStylistActiveStylesheet(
		stylist_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getStylistStylesheets(stylist_id, transaction)
	}

	@ManagerModel.bound
	async getOrderDetailOf(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.getOrderDetailFrom(stylesheet_id, transaction)
	}

	@ManagerModel.bound
	async getVariantIds(
		stylesheet_id_or_ids: number[],
		transaction: EntityManager,
	) {
		return await Promise.all(stylesheet_id_or_ids.map(id => {
			return this.StylesheetRepository.getVariantId(id, transaction)
		}))
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async removeInventory(
		changer_user_id: number,
		stylesheet_inventory_id: number,
		should_cancel_request: boolean = true,
		force_remove: boolean = false,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository
			.getInventory(stylesheet_inventory_id, transaction)
			.then(async stylesheetInventory => {
				if (stylesheetInventory.is_locked) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_101, 'Cannot remove inventory because the inventory is locked.')
				}

				if (stylesheetInventory.inventory_id) {
					return InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.inventory_id, INVENTORIES.AVAILABLE, stylesheetInventory.stylesheet_id, 'Inventory removed from stylesheet', true, transaction).then(async isCancelled => {
						if(isCancelled) {
							await this.StylesheetRepository.removeInventory(stylesheet_inventory_id, transaction)
							return true
						}

						return isCancelled
					}).catch(async err => {
						if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_104) && force_remove) {
							await this.StylesheetRepository.removeInventory(stylesheet_inventory_id, transaction)
							return true
						}

						throw err
					})
				} else if (stylesheetInventory.purchase_request_id) {
					const purchaseRequest = await InventoryManager.purchase.getRequestDetail([stylesheetInventory.purchase_request_id], transaction).then(pRs => pRs[0])

					if(purchaseRequest.purchase_order_id) {
						// PO already created, get PO
						const purchaseOrder = await InventoryManager.purchase.getOrderDetail([purchaseRequest.purchase_order_id], transaction).then(pOs => pOs[0])

						if((purchaseOrder.status === PURCHASES.RESOLVED || purchaseOrder.status === PURCHASES.PURCHASED) && purchaseRequest.status === PURCHASE_REQUESTS.APPROVED) {
							// SUCCESS OR PENDING, cannot remove
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_101, 'Cannot remove inventory because ongoing purchase order.')
						} else {
							// can cancel
							return this.StylesheetRepository.removeInventory(stylesheet_inventory_id, transaction)
						}
					} else {
						if(purchaseRequest.status === PURCHASE_REQUESTS.APPROVED || purchaseRequest.status === PURCHASE_REQUESTS.RESOLVED) {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_101, 'Cannot remove inventory because purchase request already approved.')
						} else {
							// can cancel
							// cancel the purchase request too!
							if(should_cancel_request) {
								return InventoryManager.purchase.deleteRequest(changer_user_id, purchaseRequest.id, '', transaction).then(isDeleted => {
									if(isDeleted) {
										return this.StylesheetRepository.removeInventory(stylesheet_inventory_id, transaction)
									}

									return isDeleted
								})
							} else {
								return this.StylesheetRepository.removeInventory(stylesheet_inventory_id, transaction)
							}
						}
					}
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_101, 'Cannot remove inventory because it have neither purchase request nor inventory.')
				}
			})
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async isUserHaveAny(
		user_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository
			.getByUserId(user_id, transaction)
			.then(() => {
				return true
			}).catch(err => {
				if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
					return false
				}

				throw err
			})
	}

	// @ManagerModel.bound
	// async getDynamicRefundValue(
	// 	stylesheet_inventory_ids: number[],
	// 	transaction: EntityManager,
	// ) {
	// 	const stylesheet = await this.StylesheetRepository.getInventoriesFromStylesheet
	// }

	// used for old api (2.2, 2.3, 2.4)
	// for new api use in FeedbackManager
	@ManagerModel.bound
	async getRefundValue(detail: OrderDetailInterface & {
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		orderDetailCoupons: OrderDetailCouponInterface[],
		orderDetailTransactions: OrderDetailTransactionInterface[],
		shipments: ShipmentInterface[],
	}, stylesheet: StylesheetInterface & {
		stylesheetInventories: Array<StylesheetInventoryInterface & {
			inventory: InventoryInterface,
		}>,
	}, transaction: EntityManager): Promise<{
		[id: number]: number,
	}> {
		const subtotal: number = detail.price === 0
			? await OrderManager.detail.getOrderDetailFromVoucherRedeem(detail.id, transaction).then(orderDetail => orderDetail.price).catch(() => detail.price)
			: detail.price
		// const handling = 60000
		const totalItemPurchaseValues = stylesheet.stylesheetInventories.map(sI => sI.inventory.price).reduce(CommonHelper.sum, 0)
		const paidValue = Math.max(
			subtotal -
			// handling +
			// detail.shipments.reduce((i, shipment) => shipment.is_facade ? i + shipment.amount : i, 0) -
			// ([].concat(detail.orderDetailTransactions.map(t => t.amount)).reduce(CommonHelper.sum, 0)) -
			(
				[].concat(
					// If refundable, then it is not counted on the refund value subtraction
					((detail.orderDetailCampaigns || []).map(c => c.is_refundable ? 0 : c.discount)),
				).concat(
					// coupons
					((detail.orderDetailCoupons || []).map(c => c.discount)),
				).reduce(CommonHelper.sum, 0)
			)
		, 0)

		return stylesheet.stylesheetInventories.map(sI => {
			return {
				[sI.id]: Math.ceil(sI.inventory.price / totalItemPurchaseValues * paidValue),
			}
		}).reduce(CommonHelper.sumObject, {})
	}

	@ManagerModel.bound
	async setStylist(
		changer_user_id: number,
		user_id: number,
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		return this.StylesheetRepository.update(changer_user_id, stylesheet_id, { stylist_id: user_id }, '', transaction).then(async isUpdated => {
			if(isUpdated) {
				// check user is set
				const stylesheet = await this.getShallow(stylesheet_id, transaction)
				const user = await UserManager.get(stylesheet.user_id, transaction)

				if(!user.profile.stylist_id) {
					await UserManager.updateProfile(stylesheet.user_id, {
						stylist_id: user_id,
					}, transaction)
				}
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async duplicateStylesheet(
		changer_user_id: number,
		stylesheet_id: number,
		source_stylesheet_id: number,
		consent: boolean,
		transaction: EntityManager,
	) {
		const stylesheet = await this.StylesheetRepository.get(stylesheet_id, true, true, transaction)

		if (stylesheet.stylesheetInventories.length && !consent) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_109, 'Stylesheet is not empty, are you sure want to replace it\'s content with a new one?')
		} else if(!(stylesheet.status === STYLESHEET_STATUSES.PENDING || stylesheet.status === STYLESHEET_STATUSES.STYLING)) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Stylesheet must be in STYLING or PENDING status.')
		} else if (stylesheet.stylesheetInventories.length) {
			// unbook and cancel PR if eligible
			await this.StylesheetRepository.getDetailedInventories(stylesheet.stylesheetInventories.map(sI => sI.id), false, transaction).then(async stylesheetInventories => {
				await Promise.all(stylesheetInventories.map(async sI => {
					if (sI.inventory) {
						await InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.STYLESHEET, sI.inventory.id, INVENTORIES.AVAILABLE, sI.stylesheet_id, 'Booking cancelled as stylesheet is being replaced.', false, transaction)

						return this.StylesheetRepository.removeInventory(sI.id, transaction)
					} else if (sI.purchaseRequest) {
						if (sI.purchaseRequest.status === PURCHASE_REQUESTS.PENDING || sI.purchaseRequest.status === PURCHASE_REQUESTS.EXCEPTION || sI.purchaseRequest.status === PURCHASE_REQUESTS.REJECTED) {
							await InventoryManager.purchase.updateRequest(changer_user_id, sI.purchaseRequest.id, {
								status: PURCHASE_REQUESTS.EXCEPTION,
								note: 'Purchase request cancelled because stylesheet is being replaced.',
							}, transaction)

							return this.StylesheetRepository.removeInventory(sI.id, transaction)
						} else {
							throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_104, 'Purchase request status must be in PENDING, EXCEPTION, or REJECTED.')
						}
					}

					throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Stylesheet inventory has neither inventory nor purchase request.')
				}))
			})
		}

		const source = await this.StylesheetRepository.getDetailed(source_stylesheet_id, transaction)

		return Promise.all(source.stylesheetInventories.map(async sI => {
			return Promise.resolve().then(async () => {
				if (sI.inventory) {
					return this.createInventory(changer_user_id, stylesheet_id, 'VARIANT', sI.inventory.variant_id, false, transaction).then(() => true).catch(err => false)
				}

				return false
			}).then(isInventorySet => {
				if (isInventorySet) {
					return true
				}

				if (sI.purchaseRequest) {
					return this.createInventory(changer_user_id, stylesheet_id, 'PURCHASE_REQUEST', {
						user_id: changer_user_id,
						variant_id: sI.purchaseRequest.variant_id,
						url: sI.purchaseRequest.url,
						brand_id: sI.purchaseRequest.brand_id,
						seller_id: sI.purchaseRequest.seller_id,
						title: sI.purchaseRequest.title,
						category_id: sI.purchaseRequest.category_id,
						color_id: sI.purchaseRequest.color_id,
						size_id: sI.purchaseRequest.size_id,
						retail_price: sI.purchaseRequest.retail_price,
						price: sI.purchaseRequest.price,
						quantity: 1,
						status: PURCHASE_REQUESTS.PENDING,
						note: `By stylesheet duplication (#ST-${ source_stylesheet_id })`,
					}, false, transaction).then(() => true).catch(err => false)
				}

				return false
			})
		})).then(async duplicateds => {
			if (duplicateds.indexOf(true) > -1) {
				await this.StylesheetRepository.update(changer_user_id, stylesheet_id, {
					title: source.title,
					stylist_note: source.stylist_note,
				}, 'Update because of success duplication.', transaction)

				return duplicateds
			} else {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Duplication on any of the stylesheet inventory failed.')
			}
		})
	}

	@ManagerModel.bound
	async packStylesheet(
		stylesheet_id: number,
		transaction: EntityManager,
	) {
		// ==============================================================
		// Final price update, check wether all of the SI have inventory.
		// Cannot be approved if inventory not provided
		// ==============================================================
		const stylesheet = await this.StylesheetRepository.getDetailedInventoriesFromStylesheet(stylesheet_id, transaction)

		// ==============================================================
		// Check if inventory length satisfy stylesheet requirements
		// ==============================================================
		if (stylesheet.stylesheetInventories.length !== stylesheet.count) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, 'Stylesheet inventory count & stylesheet item count doesn\'t match.')
		}

		// ==============================================================
		// Check if all inventory are from inventory and booked!
		// Individual inventory might be unavailabled before.
		// Do not ignore locked inventory.
		// Locked Inventory should be booked by this stylesheet too!
		// ==============================================================
		await Promise.all(stylesheet.stylesheetInventories.map(async sI => {
			if (!sI.inventory_id) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, 'Stylesheet inventory have not yet converted into inventory. Check purchase request.')
			} else {
				// check booking status if its really booked by this vendor
				// or unavailable
				if (sI.inventory.status !== INVENTORIES.BOOKED && sI.inventory.status !== INVENTORIES.UNAVAILABLE) {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Inventory #${sI.inventory_id} is not booked nor packed! Please cancel this inventory and book again.`)
				}

				if(sI.inventory.status === INVENTORIES.BOOKED) {
					const booking = await InventoryManager.getLastBooking(sI.inventory_id, transaction)

					if (booking.source !== BOOKING_SOURCES.STYLESHEET || booking.ref_id !== stylesheet_id) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Inventory booking exception. Inventory #${sI.inventory_id} is being booked by another source. (${booking.source}: #${booking.ref_id})`)
					}
				}
			}
		}))

		// ==============================================================
		// Check if lineup satisfy stylesheet requirements
		// ==============================================================
		if (stylesheet.lineup) {
			const lineups = stylesheet.lineup.split(' + ')

			if (lineups.length === stylesheet.count) {
				// valid
				const roots = await MasterManager.category.getRoots(transaction)

				const categoryLineup = this.getLineupCategory(stylesheet.lineup, roots)

				if (categoryLineup.length === stylesheet.count) {
					// success transforming lineup to category
					Promise.all(stylesheet.stylesheetInventories.map(async sI => {
						return MasterManager.category.getRootId(sI.category.id, transaction).then(cId => {
							return {
								stylesheetInventoryId: sI.id,
								rootCategoryId: cId,
							}
						})
					})).then(rootStylesheetCategoryIds => {
						rootStylesheetCategoryIds.forEach(({
							stylesheetInventoryId,
							rootCategoryId,
						}) => {
							const categoryLineupIndex = categoryLineup.findIndex(cL => cL.id === rootCategoryId)

							if (categoryLineupIndex > -1) {
								categoryLineup.splice(categoryLineupIndex, 1)
							} else {
								// Category not found
								throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_105, `Lineup requirement not met. Stylesheet inventory #${stylesheetInventoryId} category is wrong. Please look for the right lineup.`)
							}
						})
					})
				}
			}
		}

		// ==============================================================
		// Check if prices satisfy stylesheet requirements
		// ==============================================================
		const value = stylesheet.stylesheetInventories.map(sI => {
			return {
				price: sI.price,
				retail: sI.retail_price,
			}
		}).reduce((sum, curr) => {
			return {
				price: sum.price + curr.price,
				retail: sum.retail + curr.retail,
			}
		}, {
			price: 0,
			retail: 0,
		})

		if (value.retail < stylesheet.min_value) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, `Total retail value doesn't adequately supply minimum stylesheet value. (${FormatHelper.currency(value.retail, 'IDR')}/${FormatHelper.currency(stylesheet.min_value, '')})`)
		} else if (value.price > stylesheet.real_value) {
			throw new ErrorModel(ERRORS.NOT_SATISFIED, ERROR_CODES.ERR_105, `Total purchase value exceed maximum stylesheet real value. (${FormatHelper.currency(value.price, 'IDR')}/${FormatHelper.currency(stylesheet.real_value, '')})`)
		}

		// Stamping is now on publishing

		return true
	}

	@ManagerModel.bound
	async refundStylesheetInventory(
		changer_user_id: number,
		stylesheet_inventory_id: number,
		status: INVENTORIES.AVAILABLE | INVENTORIES.DEFECT | INVENTORIES.EXCEPTION | null,
		note: string,
		transaction: EntityManager,
	) {
		const stylesheetInventory = await this.getStylesheetInventories([stylesheet_inventory_id], transaction).then(s => s[0])
		let stylesheet = await this.getShallowWithInventory(stylesheetInventory.stylesheet_id, transaction)
		const request = await OrderManager.detail.getRequestShallow(stylesheet.orderDetail.id, transaction)
		const order = await OrderManager.getOrderDetailRelatedToPrices(stylesheet.orderDetail.order_id, transaction)
		const {
			subtotal,
			handling,
			shipping,
			roundup,
			discount,
		} = OrderManager.utilities.getPrices(order.orderDetails, order.shipments, order.points, order.wallets)
		const totalWithoutShipment = subtotal + handling + roundup - discount
		// 412.900 + 0 + 0 - 141.945 (+60.000) = 270.955

		if (request.type !== ORDER_REQUESTS.STYLEBOARD) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'This stylesheet inventory is not belong to a styleboard.')
		}

		if (!stylesheetInventory.is_locked) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylesheet inventory invalid.')
		}

		if (stylesheetInventory.inventory_id && !status) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylesheet inventory have inventory associated to it but have no status set.')
		} else if (stylesheetInventory.inventory_id) {
			await InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.inventory_id, INVENTORIES.AVAILABLE, stylesheetInventory.stylesheet_id, note || 'Booking cancelled because of refund.', false, transaction)
		} else if (stylesheetInventory.purchase_request_id) {
			await InventoryManager.purchase.updateRequest(changer_user_id, stylesheetInventory.purchase_request_id, {
				status: PURCHASE_REQUESTS.EXCEPTION,
				note: `Stlysheet inventory refunded. ${ note }`,
			}, transaction)
		}

		// Refund into wallet
		const wallet = await UserManager.wallet.getWallet(stylesheet.user_id, transaction)

		const trx = await UserManager.wallet.depositInto(wallet.id, Math.ceil(totalWithoutShipment * stylesheetInventory.price / subtotal / 100) * 100, STATEMENT_SOURCES.STYLESHEET_INVENTORY, stylesheetInventory.id, note || 'Purchase refund because item is not available.', transaction)
		// 270.955 * 193.900 / 412.900

		// Insert into order detail transaction
		await OrderManager.detail.createTransaction(stylesheet.orderDetail.id, trx.id, trx.type === STATEMENTS.CREDIT ? -trx.amount : trx.amount, `Item #SI-${ stylesheetInventory.id } unavailable refund.${ note ? ` ${ note }` : '' }`, transaction)

		// Update Stylesheet Inventory status
		await this.updateInventory(stylesheet_inventory_id, {
			is_refunded: true,
			deleted_at: new Date(),
		}, transaction)

		// Update PR status


		// Update stylesheet min value and real value
		stylesheet = await this.getShallowWithInventory(stylesheetInventory.stylesheet_id, transaction)

		const value = stylesheet.stylesheetInventories.map(s => s.retail_price).reduce(CommonHelper.sum, 0)
		const realValue = stylesheet.stylesheetInventories.map(s => s.price).reduce(CommonHelper.sum, 0)

		await this.update(changer_user_id, stylesheet.id, {
			count: stylesheet.stylesheetInventories.length,
			value,
			min_value: Math.ceil(value * .8 / 100) * 100,
			real_value: realValue,
		}, note, undefined, transaction)


		// If empty must auto resolved
		// And refund the shipment
		let shipmentRefund: UserWalletTransactionInterface = null
		if (!stylesheet.stylesheetInventories.length && stylesheet.orderDetail && stylesheet.orderDetail.id) {
			await OrderManager.detail.resolveOrderDetail(changer_user_id, stylesheet.orderDetail.id, false, 'Auto resolve order because all stylesheet inventories are refunded', transaction)
			const shipment = order.shipments.find(s => s.is_facade === true)

			shipmentRefund = await UserManager.wallet.depositInto(wallet.id, shipping, STATEMENT_SOURCES.SHIPMENT, shipment.id, 'Shipment refund because no shipment is being made.', transaction)
		}

		// Send email
		await Promise.all([
			UserManager.get(order.user_id, transaction),
			this.StylesheetRepository.getRefundedInventories(stylesheet.id, transaction),
		]).then(([user, stylesheetInventories]) => {
			const recount = {
				subtotal: stylesheetInventories.map(sI => sI.price).reduce(CommonHelper.sum, 0),
				discount: stylesheetInventories.map(sI => Math.ceil(totalWithoutShipment * sI.price / subtotal / 100) * 100).reduce(CommonHelper.sum, 0),
				shipment: shipmentRefund ? shipmentRefund.amount : 0,
			}

			NotificationManager.sendOutOfStockNotice(
				order.id,
				user.email,
				order.number,
				stylesheetInventories.map(sI => {
					return {
						variantName: `${ sI.brand.title } – ${ sI.product?.title ?? sI.purchaseRequest?.title }`,
						variantDescription: capitalize(sI.colors[0].title) + '/ ' + sI.size.title,
						price: FormatHelper.currency(sI.price, 'IDR'),
						qty: 1,
					}
				}),
				FormatHelper.currency(recount.subtotal, 'IDR'),
				undefined,
				FormatHelper.currency(recount.discount, '-IDR'),
				FormatHelper.currency(recount.shipment, 'IDR'),
				undefined,
				undefined,
				undefined,
				FormatHelper.currency(recount.subtotal + recount.shipment - recount.discount, 'IDR'),
			)
		})

		return true
	}

	@ManagerModel.bound
	public getStylesheetInventoryStatus(isRefunded: boolean, replacementId: number, inventory?: InventoryInterface, purchaseRequest?: PurchaseRequestInterface & { purchaseOrder?: PurchaseOrderInterface }): STYLESHEET_INVENTORY_STATUSES {
		if (replacementId) {
			return STYLESHEET_INVENTORY_STATUSES.REPLACED
		}

		if (isRefunded) {
			return STYLESHEET_INVENTORY_STATUSES.CANCELLED
		}

		if (inventory) {
			switch(inventory.status) {
			case INVENTORIES.BOOKED:
				return STYLESHEET_INVENTORY_STATUSES.BOOKED
			case INVENTORIES.UNAVAILABLE:
				return STYLESHEET_INVENTORY_STATUSES.PACKED
			case INVENTORIES.DEFECT:
			case INVENTORIES.EXCEPTION:
				return STYLESHEET_INVENTORY_STATUSES.INVENTORY_EXCEPTION
			case INVENTORIES.RETURNED:
				return STYLESHEET_INVENTORY_STATUSES.RETURNED
			case INVENTORIES.AVAILABLE:
			default:
				return STYLESHEET_INVENTORY_STATUSES.INVENTORY_INVALID
			}
		} else if (purchaseRequest) {
			switch (purchaseRequest.status) {
			case PURCHASE_REQUESTS.PENDING:
				return STYLESHEET_INVENTORY_STATUSES.REQUESTING
			case PURCHASE_REQUESTS.APPROVED:
				if (purchaseRequest.purchaseOrder) {
					switch (purchaseRequest.purchaseOrder.status) {
					case PURCHASES.PENDING:
						return STYLESHEET_INVENTORY_STATUSES.PURCHASING
					case PURCHASES.PURCHASED:
						return STYLESHEET_INVENTORY_STATUSES.PURCHASED
					case PURCHASES.CANCELLED:
						return STYLESHEET_INVENTORY_STATUSES.PURCHASE_CANCELLED
					case PURCHASES.RESOLVED:
						return STYLESHEET_INVENTORY_STATUSES.PURCHASE_COMPLETED
					case PURCHASES.EXCEPTION:
					default:
						return STYLESHEET_INVENTORY_STATUSES.PURCHASE_EXCEPTION
					}
				} else {
					return STYLESHEET_INVENTORY_STATUSES.REQUEST_APPROVED
				}
			case PURCHASE_REQUESTS.REJECTED:
				return STYLESHEET_INVENTORY_STATUSES.REQUEST_REJECTED
			case PURCHASE_REQUESTS.RECEIVED:
				return STYLESHEET_INVENTORY_STATUSES.INVENTORY_RECEIVED
			case PURCHASE_REQUESTS.RESOLVED:
				return STYLESHEET_INVENTORY_STATUSES.REQUEST_COMPLETED
			case PURCHASE_REQUESTS.LOCKED:
				return STYLESHEET_INVENTORY_STATUSES.LOCKED
			case PURCHASE_REQUESTS.EXCEPTION:
			default:
				return STYLESHEET_INVENTORY_STATUSES.REQUEST_EXCEPTION
			}
		} else {
			return STYLESHEET_INVENTORY_STATUSES.INVALID
		}
	}

	@ManagerModel.bound
	async updateStylesheetInventoryVariant(
		changer_user_id: number,
		stylesheet_inventory_id: number,
		variant_id: number,
		transaction: EntityManager,
	) {
		const stylesheetInventory = await this.StylesheetRepository.getInventory(stylesheet_inventory_id, transaction)
		const variant = await ProductManager.variant.getColorAndSize(variant_id, transaction)

		return Promise.resolve().then(() => {
			if (stylesheetInventory.inventory_id) {
				return InventoryManager.cancelBooking(changer_user_id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.inventory_id, INVENTORIES.AVAILABLE, stylesheetInventory.stylesheet_id, 'Cancel booking because of stylesheet inventory changed by user.', false, transaction).then(isUpdated => {
					if (isUpdated) {
						return InventoryManager.getFromVariant(variant_id, 1, false, true, transaction).then(async inventories => {
							const inventory = inventories.shift()

							await InventoryManager.book(changer_user_id, inventory.id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.stylesheet_id, `Booked by user #${ changer_user_id }`, true, transaction)

							return MatchboxManager.stylesheet.updateInventory(stylesheet_inventory_id, {
								inventory_id: inventory.id,
							}, transaction)
						}).catch(err => {
							return false
						})
					}

					return isUpdated
				})
			} else if (stylesheetInventory.purchase_request_id) {
				return InventoryManager.getFromVariant(variant_id, 1, false, true, transaction).then(async inventories => {
					const inventory = inventories.shift()

					await InventoryManager.book(changer_user_id, inventory.id, BOOKING_SOURCES.STYLESHEET, stylesheetInventory.stylesheet_id, `Booked by user #${changer_user_id}`, true, transaction)

					return MatchboxManager.stylesheet.updateInventory(stylesheet_inventory_id, {
						inventory_id: inventory.id,
					}, transaction)
				}).then(isUpdated => {
					if(isUpdated) {
						return InventoryManager.purchase.updateRequest(changer_user_id, stylesheetInventory.purchase_request_id, {
							status: PURCHASE_REQUESTS.EXCEPTION,
							note: 'Purchase request cancelled. User book from inventory instead.',
						}, transaction)
					}

					return isUpdated
				}).catch(err => {
					return false
				})
			}

			return false
		}).then(isUpdated => {
			if (isUpdated) {
				return MatchboxManager.stylesheet.updateInventory(stylesheet_inventory_id, {
					price: variant.price,
					retail_price: variant.retail_price,
				}, transaction)
			}

			return isUpdated
		}).then(isUpdated => {
			if (!isUpdated) {
				throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Booking failed.')
			}

			return isUpdated
		}).catch(err => {
			if (err instanceof ErrorModel) {
				throw err
			}

			throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, err)
		})
	}

	// ============================ PRIVATES ============================
	// tslint:disable-next-line: member-ordering
	private getLineupCategory = memoize((lineup: string, roots: Array<Pick<CategoryInterface, 'title' | 'id'>>) => {
		return lineup.split(' + ').map(l => {
			return roots.find(root => root.title.toLowerCase() === l.toLowerCase())
		}).filter(c => !!c)
	}, (lineup: string, roots: CategoryInterface[]) => {
		return lineup + roots.map(c => c.title).join('/')
	})

}

export default new MatchboxStylesheetManager()
