import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

// import AssetManager from '../asset'
import BrandManager from '../brand'
import PacketManager from '../packet'

import MatchboxStylesheetManager from './_stylesheet'

import MatchboxRepository from 'app/repositories/matchbox'

import { MatchboxInterface, BrandInterface, BrandAddressInterface } from 'energie/app/interfaces'
import { MatchboxMetadataInterface } from 'energie/app/interfaces/matchbox'

import TimeHelper, { Moment } from 'utils/helpers/time'

import DEFAULTS from 'utils/constants/default'
import { ASSETS, MATCHBOXES } from 'energie/utils/constants/enum'
// import { ASSETS } from 'energie/utils/constants/enum'


const RANGE = 0

class MatchboxManager extends ManagerModel {

	static __displayName = 'MatchboxManager'

	protected MatchboxRepository: MatchboxRepository

	private LASTORDERDATE: Moment
	private SHIPMENTDATE: Moment
	private SHIPMENTDATESHORT: Moment

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			MatchboxRepository,
		], async () => {
			// Initialization function goes here

			await BrandManager.initialize(connection)
			await PacketManager.initialize(connection)
			await MatchboxStylesheetManager.initialize(connection)

			await this.sync()

			await this.transaction(async transaction => {
				DEFAULTS.setCustomMatchboxId(await this.getBySlug('custom', false, transaction).then(m => m.id))
				DEFAULTS.setStylingMatchboxId(await this.getBySlug('session', false, transaction).then(m => m.id))
				DEFAULTS.setStylingResultMatchboxId(await this.getBySlug('session-result', false, transaction).then(m => m.id))
			})

			this.resyncDate()
		})
	}

	// tslint:disable-next-line:member-ordering
	get stylesheet(): typeof MatchboxStylesheetManager {
		return MatchboxStylesheetManager
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	async get(id: number, withAddress: false | undefined, trx: EntityManager): Promise<MatchboxInterface>
	async get(id: number, withAddress: true, trx: EntityManager): Promise<MatchboxInterface & {
		brand: BrandInterface & {
			address: BrandAddressInterface,
		},
	}>

	@ManagerModel.bound
	async get(
		id: number,
		withAddress: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.MatchboxRepository.getBy('id', id, withAddress, transaction)
	}

	@ManagerModel.bound
	async getBrand(
		matchbox_id: number,
		transaction: EntityManager,
	) {
		return this.MatchboxRepository.getBrand(matchbox_id, transaction)
	}

	@ManagerModel.bound
	async getBySlug(
		matchbox_slug: string,
		withAddress: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.MatchboxRepository.getBy('slug', matchbox_slug, withAddress as true, transaction)
	}

	@ManagerModel.bound
	async getByTitle(
		title: string,
		transaction: EntityManager,
	) {
		return this.MatchboxRepository.getByTitle(title, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	lastOrderDate() {
		this.resyncIfPassed()

		return this.LASTORDERDATE
	}

	shipmentDate(slug: string) {
		this.resyncIfPassed()

		if (slug === 'active' || slug === 'comfort') {
			return this.SHIPMENTDATESHORT
		} else {
			return this.SHIPMENTDATE
		}
	}

	readableShipmentDate(slug: string) {
		this.resyncIfPassed()

		if (slug === 'active' || slug === 'comfort') {
			if (RANGE > 0) {
				const ENDDATE = TimeHelper.moment(this.SHIPMENTDATESHORT).add(RANGE, 'd')

				return TimeHelper.getRange([
					this.SHIPMENTDATESHORT.toDate(),
					ENDDATE.toDate(),
				])
			} else {
				return this.SHIPMENTDATESHORT.format('DD MMM YYYY')
			}
		} else {
			if (RANGE > 0) {
				const ENDDATE = TimeHelper.moment(this.SHIPMENTDATE).add(RANGE, 'd')

				return TimeHelper.getRange([
					this.SHIPMENTDATE.toDate(),
					ENDDATE.toDate(),
				])
			} else {
				return this.SHIPMENTDATE.format('DD MMM YYYY')
			}
		}
	}

	// ============================ PRIVATES ============================
	private resyncDate() {
		this.LASTORDERDATE = TimeHelper.moment().endOf('d')
		this.SHIPMENTDATE = TimeHelper.calculateShipmentDate()
		this.SHIPMENTDATESHORT = TimeHelper.calculateShipmentDate(3)
	}

	private resyncIfPassed() {
		if (+this.LASTORDERDATE < Date.now()) {
			this.resyncDate()
		}
	}

	private async sync() {
		if (DEFAULTS.SYNC) {
			await this.transaction(async transaction => {
				await this.MatchboxRepository.saveOrUpdate('MatchboxRecord', [{
					id: 1,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					// Update 2.2.1
					title: 'Basic',
					slug: 'basic',
					description: 'Ingin mengubah gaya? Ingin mencoba tren baru? Atau hanya untuk update isi lemari pakaian? Basic Instant Matchbox jawabannya. Terdiri dari empat produk yang dipersonalisasi untuk melengkapi tampilan harian kamu; dari pakaian, aksesori hingga sepatu — dalam ukuran apa pun.',
					price: 660000,
					retail_price: 660000,
					count: '4',
					value: 900000,
					min_value: 720000,
					real_value: 560000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: true,
						features: [{
							title: '4 items based on lineup you chose worth ~IDR 900,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits items to be replaced—free of charge*',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days',
							is_active: true,
						}],
						brands: [
							'Zalora', 'Blanik', 'Cotton Ink',
							'Berrybenka', 'Hijabenka', 'Button On',
							'Something Borrowed', 'Lovadova', 'Whitemode',
							'Head Over Heels', 'ShopAtAleen', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 2,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.MINI_MATCHBOX_PACKET_ID,
					title: 'Mini',
					slug: 'mini',
					description: 'Sebagai starter pack yang terdiri dari dua produk pakaian, dikurasi khusus untukmu oleh stylist kami sesuai dengan gaya kamu — tanpa repot dan bebas rasa bersalah!',
					price: 360000,
					retail_price: 360000,
					count: '2',
					value: 500000,
					min_value: 400000,
					real_value: 280000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '2 items of clothing worth ~IDR 450,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits items to be replaced—free of charge*',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days',
							is_active: true,
						}],
						brands: [
							'Zalora', 'Cotton On', 'Cotton Ink',
							'Berrybenka', 'ShopAtVelvet', 'Goya',
							'Something Borrowed', 'Blanik', 'Chlorine',
							'Body Big Size', 'Kivee', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 3,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Deluxe',
					slug: 'deluxe',
					description: 'Dengan mengedepankan eksklusivitas, kami menyajikan kombinasi produk pilihan berkualitas dalam satu paket yang menawan. Jika kamu peduli akan kualitas, ini adalah Matchbox yang pas untukmu. Bisa menjadi kado untuk yang tersayang atau menghadiahi diri sendiri.',
					price: 1000000,
					retail_price: 1000000,
					count: '4',
					value: 1300000,
					min_value: 1100000,
					real_value: 920000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items based on lineup you chose worth ~IDR 1,300,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits items to be replaced—free of charge*',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days',
							is_active: true,
						}],
						brands: [
							'Mango', 'Love Bonito', 'Cotton Ink',
							'Marks & Spencer', 'ShopAtVelvet', 'Goya',
							'Pomelo', 'Cotton On', 'Ensemble',
							'Rubi', 'Cloth Inc', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 4,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Workwear',
					slug: 'workwear',
					description: 'Calling all modern ladies to werk it!  Break out of your style routine with our ‘business-edition’ Matchbox, no matter what your job status is. Filled with ideal staples curated for casual to formal workwear, this is the ultimate go-to box specifically made for the #bossbabe in you.',
					price: 660000,
					retail_price: 660000,
					count: '4',
					value: 900000,
					min_value: 720000,
					real_value: 600000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 900,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'The Executive', 'Giordano', 'Morphidae',
							'Berrybenka', 'Office Hour', 'Eprise',
							'(X) S.M.L', 'Korz', 'Cloth Inc',
							'AliveLoveArts', 'Zalora Basics', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 5,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Matchbox X Catwomanizer',
					slug: 'xcat',
					description: '“Every woman needs a little black dress”, period. Even Karl Lagerfeld swears by it. For that never-fail LBD, get ready for our special and limited edition Matchbox. Handpicked by none-other-than @catwomanizer a.k.a. Andrea Gunawan herself, you’ll receive one black dress as the key piece and three other items by our stylists. This Matchbox is going to be your most versatile yet; One you never knew you needed.\n—\nImportant note: LBD is non-exchangeable and is only available in XS, S, M, L and XL',
					price: 960000,
					retail_price: 960000,
					count: '4',
					value: 1200000,
					min_value: 1000000,
					real_value: 840000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 1,200,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'Zalora', 'Cotton On', 'Cotton Ink',
							'Berrybenka', 'ShopAtVelvet', 'Goya',
							'Something Borrowed', 'Blanik', 'Chlorine',
							'Body Big Size', 'Kivee', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 6,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Ramadhan Basic',
					slug: 'basic-ramadhan',
					description: 'Be Ramadhan ready this Eid! Enjoy the holy celebration in style with our Ramadhan edition matchbox. In collaboration with all your most-favorite brands, turn heads and win hearts with our stylist-picked pieces for your most unique and on-trend personal style. No more panic shopping, delayed delivery or bad mix-match, just stylish and happy you!',
					price: 660000,
					retail_price: 660000,
					count: '4',
					value: 900000,
					min_value: 720000,
					real_value: 600000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 900,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'Hijup', 'H Project', 'Hijabenka',
							'Zumara', 'ShopAtVelvet', 'Hazelnut',
							'Cotton Ink', 'ShopAtAleen', 'JV Hasanah',
							'Covering Story', 'Zalia', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 7,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Ramadhan Deluxe',
					slug: 'deluxe-ramadhan',
					description: 'Make it your best Eid yet! It’s time to up your fashion game with elegant pieces from popular local and international brands - all assembled by our stylists for your most fashionable and unique style. Kiss your typical Ramadhan outfits goodbye, let’s get festive with our deluxe Matchbox!',
					price: 1000000,
					retail_price: 1000000,
					count: '4',
					value: 1300000,
					min_value: 1100000,
					real_value: 940000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 1,300,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'Mango', 'Hijup ', 'Cotton Ink Studio',
							'H & M', 'ShopAtVelvet', 'ALLURA',
							'Pomelo', 'Goya', 'Kami',
							'Love and Flair', 'Cloth Inc', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 8,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Matchbox X SweetEscape',
					slug: 'sweetescape',
					description: 'All your bags are packed, are you ready to go? Teaming up with everyone’s favorite traveling photography service — SweetEscape, this is a Matchbox that deserves a spot in your suitcase. Wherever you’re traveling to, let our stylists pack for you. So strike a pose, flash a smile, now you’re camera ready!\nNOTE: Remember to write a note to your stylist to let her know your destination.',
					price: 660000,
					retail_price: 660000,
					count: '4',
					value: 900000,
					min_value: 720000,
					real_value: 600000,
					styling_fee: 60000,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 900,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'Zalora Basics', 'Cotton On', 'Dotdtails',
							'POP U', 'ShopAtVelvet', 'Goya',
							'Something Borrowed', 'Ensemble', 'Chlorine',
							'Supre', 'Mango', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 9,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Replacement',
					slug: 'custom',
					description: 'Special matchbox especially made for someone special – you!',
					price: 1260000,
					retail_price: 1260000,
					count: '1 - 4',
					value: 1500000,
					min_value: 1200000,
					real_value: 1200000,
					styling_fee: 60000,
					type: MATCHBOXES.STYLE,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: '4 items worth ~IDR 1,500,000 at retail value',
							slug: '4-item',
							is_active: true,
						}, {
							title: '7-day try-on period',
							slug: 'period',
							is_active: true,
						}, {
							title: 'Stylist note',
							slug: 'note',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits/unwanted items to be replaced—free of charge*.',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days.\n*Return shipping will be at your own expense',
							is_active: true,
						}],
						brands: [
							'Zalora Basics', 'Cotton On', 'Dotdtails',
							'POP U', 'ShopAtVelvet', 'Goya',
							'Something Borrowed', 'Ensemble', 'Chlorine',
							'Supre', 'Mango', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 10,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Valentine — Basic',
					slug: 'valentine-basic',
					description: '',
					price: 640000,
					retail_price: 760000,
					count: '4',
					value: 900000,
					min_value: 720000,
					real_value: 560000,
					styling_fee: 160000,
					type: MATCHBOXES.GIFT,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: 'Basic Matchbox Gift Card worth IDR 660,000',
							slug: 'card',
							is_active: true,
						}, {
							title: 'Flore No.05 Lavande Scented Candle By Kencana Candle worth IDR 100,000',
							slug: 'candle',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits items to be replaced—free of charge*',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days',
							is_active: true,
						}],
						brands: [
							'Zalora', 'Blanik', 'Cotton Ink',
							'Berrybenka', 'Hijabenka', 'Button On',
							'Something Borrowed', 'Lovadova', 'Whitemode',
							'Head Over Heels', 'ShopAtAleen', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2017, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 11,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Valentine — Deluxe',
					slug: 'valentine-deluxe',
					description: '',
					price: 920000,
					retail_price: 1100000,
					count: '4',
					value: 1300000,
					min_value: 1100000,
					real_value: 760000,
					styling_fee: 160000,
					type: MATCHBOXES.GIFT,
					metadata: JSON.stringify({
						is_popular: false,
						features: [{
							title: 'Deluxe Matchbox Gift Card worth IDR 1,000,000',
							slug: 'card',
							is_active: true,
						}, {
							title: 'Flore No.05 Lavande Scented Candle By Kencana Candle worth IDR 100,000',
							slug: 'candle',
							is_active: true,
						}],
						warranties: [{
							title: 'Return unwanted items',
							slug: 'wallet',
							description: 'Refund will be issued to your Wallet Balance which can be applied towards your next purchase or withdrawn to your bank account anytime',
							is_active: true,
						}, {
							title: 'One-time FREE exchange',
							slug: 'free-exchange',
							description: 'Return the misfits items to be replaced—free of charge*',
							is_active: true,
						}, {
							title: '7-business day replacement',
							slug: 'replacement',
							description: 'Replacements will be shipped within seven business days',
							is_active: true,
						}],
						brands: [
							'Zalora', 'Blanik', 'Cotton Ink',
							'Berrybenka', 'Hijabenka', 'Button On',
							'Something Borrowed', 'Lovadova', 'Whitemode',
							'Head Over Heels', 'ShopAtAleen', 'and many more…'],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2018, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 12,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Custom',
					slug: 'session',
					description: 'Special matchbox for stylist, no limit!',
					price: 2500000,
					retail_price: 2500000,
					count: '5',
					value: 0,
					min_value: 0,
					real_value: 99999999,
					styling_fee: 0,
					type: MATCHBOXES.STYLE,
					metadata: JSON.stringify({
						is_popular: false,
						features: [],
						warranties: [],
						brands: [],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 13,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.DEFAULT_MATCHBOX_PACKET_ID,
					title: 'Custom',
					slug: 'session-result',
					description: 'Special matchbox especially made for someone special – you!',
					price: 2500000,
					retail_price: 2500000,
					count: '5',
					value: 0,
					min_value: 0,
					real_value: 99999999,
					styling_fee: 0,
					type: MATCHBOXES.STYLE,
					metadata: JSON.stringify({
						is_popular: false,
						features: [],
						warranties: [],
						brands: [],
					} as MatchboxMetadataInterface) as any,
					published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 14,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.MINI_MATCHBOX_PACKET_ID,
					title: 'Comfort Home',
					slug: 'comfort',
					description: 'Diam di rumah tidak berarti tidak bisa bergaya dengan nyaman. Dapatkan produk-produk pakaian dari para brand partner dari Yuna & Co. yang sudah diseleksi dan dikurasi oleh stylists kami. Dapatkan set pakaian terdiri dari t-shirt, celana pendek dan pakaian tidur.',
					price: 430000,
					retail_price: 430000,
					count: '3',
					value: 550000,
					min_value: 0,
					real_value: 390000,
					styling_fee: 0,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [],
						warranties: [],
						brands: [],
					} as MatchboxMetadataInterface) as any,
					// published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 15,
					brand_id: DEFAULTS.YUNA_BRAND_ID,
					packet_id: DEFAULTS.MINI_MATCHBOX_PACKET_ID,
					title: 'Active Home',
					slug: 'active',
					description: 'Tidak bisa keluar rumah, bukan menjadi alasan untuk tidak memiliki rutinitas sehat seperti olahraga dari Yoga, Pilates, Lari pagi, dan lain”. Biar Yuna & Co membantumu untuk mencarikan pakaian olahraga untuk menyemangati rutinitas sehat mu.',
					price: 480000,
					retail_price: 480000,
					count: '2',
					value: 620000,
					min_value: 0,
					real_value: 400000,
					styling_fee: 0,
					type: MATCHBOXES.BOTH,
					metadata: JSON.stringify({
						is_popular: false,
						features: [],
						warranties: [],
						brands: [],
					} as MatchboxMetadataInterface) as any,
					// published_at: TimeHelper.moment(new Date(2019, 0, 1, 0, 0, 0, 0)).format() as any,
					expired_at: TimeHelper.moment(new Date(2019, 7, 17, 0, 0, 0, 0)).format() as any,
					updated_at: TimeHelper.moment().format() as any,
				}], transaction)

				await this.MatchboxRepository.saveOrUpdate('MatchboxAssetRecord', [{
					id: 1,
					matchbox_id: 1,
					url: 'matchbox/basic.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 2,
					matchbox_id: 2,
					url: 'matchbox/mini.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 3,
					matchbox_id: 3,
					url: 'matchbox/deluxe.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 4,
					matchbox_id: 4,
					url: 'matchbox/workwear.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 5,
					matchbox_id: 5,
					url: 'matchbox/x-catwomanizer.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 540,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 6,
					matchbox_id: 6,
					url: 'matchbox/basic-ramadhan.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 7,
					matchbox_id: 7,
					url: 'matchbox/deluxe-ramadhan.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 8,
					matchbox_id: 8,
					url: 'matchbox/x-sweetescape.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 9,
					matchbox_id: 9,
					url: 'matchbox/custom.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 10,
					matchbox_id: 10,
					url: 'matchbox/valentine-basic.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 0,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 11,
					matchbox_id: 11,
					url: 'matchbox/valentine-deluxe.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 0,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 12,
					matchbox_id: 12,
					url: 'matchbox/custom.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				},  {
					id: 13,
					matchbox_id: 13,
					url: 'matchbox/custom.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 14,
					matchbox_id: 14,
					url: 'matchbox/comfort-home.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}, {
					id: 15,
					matchbox_id: 15,
					url: 'matchbox/active-home.jpg',
					type: ASSETS.IMAGE,
					width: 960,
					height: 720,
					metadata: JSON.stringify({
						version: 4,
					} as object) as any,
					order: 0,
					updated_at: TimeHelper.moment().format() as any,
				}], transaction)
			})
		}
	}
}

export default new MatchboxManager()
