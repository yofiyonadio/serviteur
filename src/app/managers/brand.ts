import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import AssetManager from './asset'
import MasterManager from './master'

import {
	BrandRepository,
} from 'app/repositories'

import {
	BrandInterface,
	BrandAddressInterface,
} from 'energie/app/interfaces'
import { AssetUploadConfig } from 'app/interfaces/file'

import { CommonHelper } from 'utils/helpers'
import { ObjectOrArray } from 'types/common'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES } from 'app/models/error'


class BrandManager extends ManagerModel {

	static __displayName = 'BrandManager'

	protected BrandRepository: BrandRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			BrandRepository,
		], async () => {
			// Initialization function goes here
			await this.transaction(async transaction => {
				const yuna = await this.BrandRepository
				.getByTitle('Yuna & Co.', transaction)
				.catch(async err => {
					if(err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
						const brand = await this.BrandRepository.insert({
							title: 'Yuna & Co.',
							is_published: true,
						}, transaction)

						const brandAddress = await this.BrandRepository.insertAddress(brand.id, {
							title: 'Yuna Office',
							receiver: 'Yuna Ocnanuy',
							phone: '+628176333222',
							address: 'Jl. Tanjung Duren Raya No. 103, RT.6/RW.5\nTanjung Duren Selatan\nJakarta',
							district: 'Grogol Petamburan/Jakarta Barat',
							postal: '11470',
							metadata: {
								tikiAreaId: 16832,
							},
							location_id: 4799,
						}, transaction)

						brand.brand_address_id = brandAddress.id

						await this.BrandRepository.update(brand.id, {
							brand_address_id: brandAddress.id,
						}, transaction)

						return brand
					}
				})

				DEFAULTS.setYunaBrandId(yuna.id)
				DEFAULTS.setYunaBrandAddressId(yuna.brand_address_id)

				return yuna
			})
		})
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		dataOrDatas: ObjectOrArray<BrandInterface>,
		transaction: EntityManager,
	) {
		if (Array.isArray(dataOrDatas)) {
			return this.BrandRepository.insert(dataOrDatas, transaction)
		} else {
			return this.BrandRepository.insert(dataOrDatas, transaction)
		}
	}

	@ManagerModel.bound
	async createAddress(
		brand_id: number,
		address: BrandAddressInterface,
		transaction: EntityManager,
	) {
		return this.BrandRepository.insertAddress(brand_id, address, transaction)
	}

	@ManagerModel.bound
	async createAsset(
		brand_id: number,
		asset: {
			image: string,
			config?: AssetUploadConfig,
			metadata?: object,
			order?: number,
		},
		transaction: EntityManager,
	) {
		return AssetManager.upload('brand', brand_id, asset.image, undefined, asset.config, asset.metadata, asset.order, transaction)
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async updateAddress(
		brand_address_id: number,
		data: Partial<BrandAddressInterface>,
		transaction: EntityManager,
	) {
		return this.BrandRepository.updateAddress(brand_address_id, data, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		brand_id: number,
		transaction: EntityManager,
	) {
		return this.BrandRepository.get(brand_id, {consignment_stock: undefined},  transaction)

	}

	@ManagerModel.bound
	async getYuna(
		transaction: EntityManager,
	) {
		return this.BrandRepository.get(DEFAULTS.YUNA_BRAND_ID, {consignment_stock: undefined}, transaction)
	}

	@ManagerModel.bound
	async getAddresses(
		brand_address_id_or_ids: number | number[],
		transaction: EntityManager,
	) {
		return this.BrandRepository.getAddresses(brand_address_id_or_ids, transaction)
	}

	@ManagerModel.bound
	async getAddressDeep(address_id: number, trx: EntityManager) {
		return this.withTransaction(trx, async transaction => {
			return this.BrandRepository.getAddressDeep(address_id, transaction)
		})
	}

	@ManagerModel.bound
	async getAll(
		filter: {
			consignment_stock?: boolean,
		},
		transaction: EntityManager,
	) {
		return this.BrandRepository.get(null, {consignment_stock: filter.consignment_stock}, transaction)
	}

	@ManagerModel.bound
	async getByCategoryIds(
		category_ids: number[],
		transaction: EntityManager,
	) {
		const childrens = await Promise.all(category_ids.map(categoryId => {
			return MasterManager.category.getChildrenIds(categoryId, transaction)
		})).then(cc => {
			return CommonHelper.flatten(cc)
		}).catch(err => {
			this.warn(err)

			return []
		})

		return this.BrandRepository.getByCategoryIds([...category_ids, ...childrens], transaction)
	}

	@ManagerModel.bound
	async getByTitle(
		title: string,
		transaction: EntityManager,
	) {
		return this.BrandRepository.getByTitle(title, transaction)
	}

	@ManagerModel.bound
	async getMainAddress(
		brand_id: number,
		transaction: EntityManager,
	) {
		return this.BrandRepository.getMainAddress(brand_id, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	async remove(
		id: number,
		transaction: EntityManager,
	) {
		return this.BrandRepository.update(id, {
			deleted_at: new Date(),
		}, transaction)
	}

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new BrandManager()
