import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	ClusterRepository,
} from 'app/repositories'

import {
	ClusterInterface,
} from 'energie/app/interfaces'
import { Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'
import { STYLECARD_STATUSES } from 'energie/utils/constants/enum'

import ServiceManager from './service'
import NotificationManager from './notification'

import { isEmpty } from 'lodash'
import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'

class ClusterManager extends ManagerModel {
	static __displayName = 'ClusterManager'

	protected ClusterRepository: ClusterRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ClusterRepository,
		])
	}


	// ============================= INSERT =============================
	async insert(
		data: ClusterInterface,
		transaction: EntityManager,
	) {
		return this.ClusterRepository.insert(data, transaction)
	}

	// ============================= UPDATE =============================
	async update(
		cluster_id: number,
		data: Parameter<ClusterInterface>,
		transaction: EntityManager,
	) {
		return this.ClusterRepository.update(cluster_id, CommonHelper.stripUndefined(data), transaction)
	}

	// ============================= GETTER =============================
	async filter(
		offset: number,
		limit: number,
		search: string | undefined,
		transaction: EntityManager,
	) {
		return this.ClusterRepository.filter(offset, limit, search, transaction)
	}

	async getById(
		cluster_id: number,
		filter: {
			offset?: number,
			limit?: number,
		} = {},
		option: {
			type_no?: number
			with_stylecard?: boolean,
			stylecard_status?: STYLECARD_STATUSES,
			limit?: number,
			by_published_date?: boolean,
		},
		transaction: EntityManager,
	) {
		return this.ClusterRepository.get(cluster_id, undefined, filter, option, transaction)
	}

	async getByType(
		cluster_type: string,
		filter: {
			offset?: number,
			limit?: number,
		} = {},
		option: {
			type_no?: number
			with_stylecard?: boolean,
			stylecard_status?: STYLECARD_STATUSES,
		},
		transaction: EntityManager,
	) {
		return this.ClusterRepository.get(undefined, cluster_type, filter, option, transaction)
	}

	get getUsers() {
		return this.ClusterRepository.getUsers
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	async publishBatchCluster(
		changer_user_id: number,
		cluster_id: number,
		stylecard_ids: number[],
		note: string,
		transaction: EntityManager,
	) {
		// TURN IT OFF
		// const stylecards = await ServiceManager.stylecard.get({
		// 	stylecard_id_or_ids: stylecard_ids,
		// 	many: true,
		// }, {
		// 	with_variants: true,
		// }, transaction)

		// before publish check stylecard_item_count first
		// const is_valid = stylecards.findIndex(stylecard => stylecard.count !== stylecard.stylecardVariants.length) === -1
		// if (is_valid) {
		// } else {
		// 	throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Invalid Stylecard Item Count!')
		// }

		return await ServiceManager.stylecard.publishBatch(
			changer_user_id,
			stylecard_ids,
			note,
			transaction,
		).then(async isUpdated => {
			const usersInCluster = await this.getUsers(cluster_id, transaction)

			if (!isEmpty(usersInCluster)) {
				// add stylecard to all user in cluster
				await ServiceManager.stylecard.addUser(stylecard_ids, usersInCluster.map(user => user.id), transaction)
					.catch(error => {
						if (error.code === '23505') {
							throw new ErrorModel(ERRORS.UNKNOWN, ERROR_CODES.ERR_099, 'Users already have this stylecards, please remove them before update')
						}

						throw error
					})

				// send notif
				usersInCluster.map(user => {
					NotificationManager.sendWeeklyRecomendationNotice(
						user.id,
						cluster_id,
						user.email,
						user.profile.first_name,
						user.stylist?.asset?.url,
						user.stylist?.profile.stage_name,
						`${process.env.MAIN_WEB_URL}/boards`,
					)
				})
			}

			return isUpdated
		})
	}

}

export default new ClusterManager()
