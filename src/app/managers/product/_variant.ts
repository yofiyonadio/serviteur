import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import AssetManager from '../asset'
import PacketManager from '../packet'

import VariantRepository from 'app/repositories/variant'

import {
	VariantInterface,
	PacketInterface,
	OrderDetailInterface,
	OrderDetailCampaignInterface,
	OrderDetailCouponInterface,
	ShipmentInterface,
} from 'energie/app/interfaces'


import {
	SchedulerManager,
} from 'app/managers'

import { Parameter } from 'types/common'

import {
	isEmpty,
} from 'lodash'
import { ProductManager } from '..'
import { INVENTORIES, ORDER_DETAILS, SCHEDULER_TYPE, SCHEDULER_STAT, VARIANT_STAT } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'


class ProductVariantManager extends ManagerModel {

	static __displayName = 'ProductVariantManager'

	protected VariantRepository: VariantRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			VariantRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		changer_user_id: number,
		data: Parameter<VariantInterface>,
		colors_ids: number[],
		tag_ids: number[] | undefined,
		measurements: { [key: string]: string } | undefined,
		packet: Parameter<PacketInterface> | undefined,
		assets: Array<{
			image: string,
			order: number,
			metadata?: object,
		}> = [],
		transaction: EntityManager,
	) {
		if (colors_ids.length === 2 && colors_ids[0] === colors_ids[1]) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Secondary color cannot be the same as Primary color')
		}

		if (data.sku === '') {
			delete data.sku
		}

		const variant = await this.VariantRepository.insert(
			changer_user_id,
			{
				...data,
				price: data.price === 0 && data.retail_price !== 0 ? data.retail_price : data.price,
				retail_price: data.retail_price === 0 && data.price !== 0 ? data.price : data.retail_price,
				packet_id: !isEmpty(packet) ? await PacketManager.create(packet, transaction).then(p => p.id) : await PacketManager.duplicate(await ProductManager.getPacket(data.product_id, transaction).then(p => p.id), false, transaction),
			},
			colors_ids,
			tag_ids,
			measurements,
			transaction,
		)

		if (assets.length > 0) {
			await Promise.all(assets.map(asset => {
				return AssetManager.upload('variant', variant.id, asset.image, undefined, undefined, asset.metadata, asset.order, transaction)
			}))
		}

		return variant
	}

	get addHistory() {
		return this.VariantRepository.addHistory
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		variant_id: number,
		variant: Partial<Parameter<VariantInterface, 'packet_id'>>,
		tag_ids: number[] | undefined,
		measurements: { [key: string]: string } | undefined,
		color_ids: number[] | undefined,
		packet: PacketInterface | undefined,
		transaction: EntityManager,
		po_start?: Date,
		po_end?: Date,
		po_shipment?: Date,
		po_interval?: number,
	) {
		return this.VariantRepository.update(
			changer_user_id,
			variant_id,
			variant,
			color_ids,
			tag_ids,
			measurements,
			transaction,
		).then(async isUpdated => {
			if (isUpdated && !isEmpty(packet)) {
				await PacketManager.createOrUpdate('VARIANT', variant_id, undefined, packet, transaction)
			}

			if (variant.order_status === VARIANT_STAT.PO_FIXED) {
				await SchedulerManager.insertPreOrder(variant.order_status, {
					variant_id,
					title: 'po_variant_' + variant_id,
					status: SCHEDULER_STAT.RUN,
					function: 'preorderScheduler',
					type: SCHEDULER_TYPE.ONCE,
					start_date: po_start,
					end_date: po_end,
					shipment_date: po_shipment,
				}, transaction)
				await ProductManager.variant.update(22, variant_id, {
					is_available: false
				}, undefined, undefined, undefined, undefined, transaction)
			}

			if (variant.order_status === VARIANT_STAT.PO_INTERVAL) {
				await SchedulerManager.insertPreOrder(variant.order_status, {
					variant_id,
					title: 'po_variant_' + variant_id,
					status: SCHEDULER_STAT.RUN,
					function: 'preorderScheduler',
					type: SCHEDULER_TYPE.ONCE,
					shipment_interval: po_interval,
				}, transaction)
			}

			if (variant.order_status === VARIANT_STAT.DEFAULT) {
				await SchedulerManager.insertPreOrder(variant.order_status, {
					variant_id,
					title: 'po_variant_' + variant_id,
					status: SCHEDULER_STAT.STOP,
					function: 'preorderScheduler',
					type: SCHEDULER_TYPE.ONCE,
				}, transaction)
				await ProductManager.variant.update(22, variant_id, {
					is_available: true
				}, undefined, undefined, undefined, undefined, transaction)
			}

			return isUpdated
		})
	}//

	@ManagerModel.bound
	async updateAvailable(
		variant_id: number,
		is_available: boolean,
		transaction: EntityManager,
	) {
		return this.VariantRepository.updateAvailable(variant_id, is_available, transaction)
	}

	// ============================= GETTER =============================
	get filter() {
		return this.VariantRepository.filter
	}

	@ManagerModel.bound
	async variantList(
		offset: number = 0,
		limit: number = 20,
		filter: {
			product_ids?: string,
			brand_ids?: string,
			category_ids?: string,
			size_ids?: string,
			color_ids?: string,
			price?: string,
			search?: string,
			merchants?: string[],
			yuna_product?: boolean,
			available_only?: boolean,
			blacklist_man?: boolean,
		} = {},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		internalProductOnly: boolean,
		excludeZeroInventory: boolean | undefined,
		includeZeroVariant: boolean | undefined,
		transaction: EntityManager,
	) {
		return this.VariantRepository.variantList(offset, limit, filter, sort_by, internalProductOnly, excludeZeroInventory, includeZeroVariant, transaction)
	}

	get get() {
		return this.VariantRepository.get
	}

	get getProduct() {
		return this.VariantRepository.getProduct
	}


	get getBrand() {
		return this.VariantRepository.getBrand
	}

	get getColorAndSize() {
		return this.VariantRepository.getColorAndSize
	}

	get getDetail() {
		return this.VariantRepository.getDetail
	}

	get getDetailFlat() {
		return this.VariantRepository.getDetailFlat
	}

	get getInventories() {
		return this.VariantRepository.getInventories
	}

	get getAvailableInventoryCount() {
		return this.VariantRepository.getAvailableInventoryCount
	}

	get getMeasurements() {
		return this.VariantRepository.getMeasurements
	}

	get getDiscount() {
		return this.VariantRepository.getDiscount
	}

	async getTags(variant_id: number, grouped: false | undefined, transaction: EntityManager): Promise<ReturnType<VariantRepository['getTags']>>
	async getTags(variant_id: number, grouped: true | undefined, transaction: EntityManager): Promise<ReturnType<VariantRepository['getGroupedTags']>>

	@ManagerModel.bound
	async getTags(
		variant_id: number,
		grouped: boolean | undefined,
		transaction: EntityManager,
	): Promise<any> {
		if (grouped) {
			return this.VariantRepository.getGroupedTags(variant_id, transaction)
		} else {
			return this.VariantRepository.getTags(variant_id, transaction)
		}
	}

	@ManagerModel.bound
	async getProductDetail(
		user_id: number,
		variant_id: number,
		order_detail_id: number | undefined,
		stylesheet_inventory_id: number | undefined,
		transaction: EntityManager,
	) {
		return this.VariantRepository.getProductDetail(user_id, variant_id, order_detail_id, stylesheet_inventory_id, transaction)
			.then(productDetail => {
				return {
					...productDetail,
					variants: productDetail.variants.map(variant => {
						return {
							...variant,
							availableQuantity: variant.inventories.reduce((a, b) => a + (b.status === INVENTORIES.AVAILABLE ? 1 : 0), 0),
						}
					}),
				}
			})
	}

	@ManagerModel.bound
	async getDetailFlats(
		variant_ids: number[],
		offset: number,
		limit: number,
		filter: {
			price: string,
		},
		transaction: EntityManager,
		operator?: string,
		percent?: number,
	) {
		return this.VariantRepository.getDetailFlats(variant_ids, offset, limit, filter, transaction, operator, percent)
	}

	get getProductDetailFromVariant() {
		return this.VariantRepository.getProductDetailFromVariant
	}

	get getAnswers() {
		return this.VariantRepository.getAnswers
	}

	get getLatestAnswer() {
		return this.VariantRepository.getLatestAnswer
	}

	// used for old api (2.2, 2.3, 2.4)
	// for new api use in FeedbackManager
	getRefundValue(
		details: Array<OrderDetailInterface & {
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			orderDetailCoupons: OrderDetailCouponInterface[],
		}>,
		shipments: ShipmentInterface[],
	): {
		[id: number]: number,
	} {
		const {
			subtotal,
			discount,
		} = details.reduce((init, detail) => {
			if (detail.type === ORDER_DETAILS.INVENTORY) {
				return {
					subtotal: init.subtotal + detail.price,
					discount: init.discount
						+ detail.orderDetailCampaigns.reduce((i, campaign) => campaign.is_refundable ? i + campaign.discount : i, 0)
						+ detail.orderDetailCoupons.reduce((i, coupon) => i + coupon.discount, 0),
				}
			} else {
				return init
			}
		}, {
			subtotal: 0,
			discount: 0,
		} as {
			subtotal: number,
			discount: number,
		})

		const paidValue = subtotal - discount

		return details.reduce((obj, detail) => {
			return {
				...obj,
				[detail.id]: Math.ceil(detail.price / subtotal * paidValue),
			}
		}, {})
	}

	// ============================= DELETE =============================
	get remove() {
		return this.VariantRepository.remove
	}


	async removeAssets(variant_id: number, transaction: EntityManager): Promise<boolean>
	async removeAssets(variant_id: number, variant_asset_id: number, transaction: EntityManager): Promise<boolean>

	@ManagerModel.bound
	async removeAssets(variant_id: number, assetIdOrTransaction: number | EntityManager, transaction?: EntityManager) {
		if (typeof assetIdOrTransaction === 'number') {
			return AssetManager.delete('variant', variant_id, assetIdOrTransaction, transaction)
		} else {
			return AssetManager.batchDelete('variant', variant_id, assetIdOrTransaction)
		}
	}

	// ============================= METHODS ============================
	get addAnswer() {
		return this.VariantRepository.addAnswer
	}

	// ============================ PRIVATES ============================

}

export default new ProductVariantManager()

