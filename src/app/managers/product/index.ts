import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import PacketManager from '../packet'

import ProductVariantManager from './_variant'

import {
	ProductRepository,
} from 'app/repositories'

import {
	ProductInterface,
	PacketInterface,
} from 'energie/app/interfaces'

import { Parameter } from 'types/common'
import { CommonHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'

import {
	isEmpty,
} from 'lodash'

class ProductManager extends ManagerModel {

	static __displayName = 'ProductManager'

	protected ProductRepository: ProductRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ProductRepository,
		], async () => {
			await ProductVariantManager.initialize(connection)
		})
	}

	// tslint:disable-next-line:member-ordering
	get variant(): typeof ProductVariantManager {
		return ProductVariantManager
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		changer_user_id: number,
		data: Parameter<ProductInterface, 'packet_id'>,
		packet: Parameter<PacketInterface> | undefined,
		tag_ids: number[] | undefined,
		transaction: EntityManager,
	): Promise<ProductInterface> {
		if (!isEmpty(packet)) {
			return PacketManager.create(packet, transaction).then(async _packet => {
				return this.ProductRepository.insert(changer_user_id, {
					...data,
					packet_id: _packet.id,
				}, tag_ids, transaction)
			})
		} else {
			return PacketManager.duplicate(DEFAULTS.PRODUCT_PACKET_ID, false, transaction).then(packetId => {
				return this.ProductRepository.insert(changer_user_id, {
					...data,
					packet_id: packetId,
				}, tag_ids, transaction)
			})
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		changer_user_id: number,
		product_id: number,
		data: Partial<Parameter<ProductInterface, 'packet_id'>>,
		packet: Parameter<PacketInterface> | undefined,
		tag_ids: number[] | undefined,
		note: string,
		transaction: EntityManager,
	) {
		return this.ProductRepository.update(
			changer_user_id,
			product_id,
			CommonHelper.stripKey(data as ProductInterface, 'packet_id'),
			tag_ids,
			note,
			transaction,
		).then(async isUpdated => {
			if (isUpdated && !isEmpty(packet)) {
				await PacketManager.createOrUpdate('PRODUCT', product_id, undefined, packet, transaction)
			}

			return isUpdated
		})
	}

	@ManagerModel.bound
	async updateRemarks(
		product_id: number,
		stylist_id: number,
		remarks: string,
		transaction: EntityManager,
	) {
		const stylist_remarks = `${stylist_id} - ${remarks}`
		return this.ProductRepository.updateRemarks(product_id, stylist_remarks, transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number = 0,
		limit: number = 20,
		filter: {
			product_ids?: string,
			brand_ids?: string,
			category_ids?: string,
			size_ids?: string,
			color_ids?: string,
			price?: string,
			search?: string,
			merchants?: string[],
			yuna_product?: boolean,
			available_only?: boolean,
			blacklist_man?: boolean,
			start_discount?: string,
			recent_only?: boolean,
		} = {},
		sort_by: {
			created_at?: 'ASC' | 'DESC',
			updated_at?: 'ASC' | 'DESC',
			price?: 'ASC' | 'DESC',
		} = {},
		internalProductOnly: boolean,
		excludeZeroInventory: boolean | undefined,
		includeZeroVariant: boolean | undefined,
		mustPublished: boolean,
		transaction: EntityManager,
	) {
		return this.ProductRepository.filter(offset, limit, filter, sort_by, internalProductOnly, excludeZeroInventory, includeZeroVariant, mustPublished, transaction)
	}

	@ManagerModel.bound
	async get(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getDetail(product_id, true, transaction)
	}

	@ManagerModel.bound
	async getByTitle(
		title: string,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getByTitle(title, transaction)
	}

	@ManagerModel.bound
	async getPacket(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getPacket(product_id, transaction)
	}

	@ManagerModel.bound
	async getItemSoldCount(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getItemSoldCount(product_id, transaction)
	}

	@ManagerModel.bound
	async getStylesheetItemSoldCount(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getStylesheetItemSoldCount(product_id, transaction)
	}

	@ManagerModel.bound
	async getInventories(
		product_id: number,
		availailable_only: boolean,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getInventories(product_id, availailable_only, transaction)
	}

	@ManagerModel.bound
	async getPromotion(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getPromotion(product_id, transaction)
	}


	@ManagerModel.bound
	async getTags(
		product_id: number,
		transaction: EntityManager,
	) {
		return this.ProductRepository.getTags(product_id, transaction)
	}

	@ManagerModel.bound
	async getByVariantIds(
		variant_ids: number[],
		transaction: EntityManager,
	) {
		return this.ProductRepository.getByVariantIds(variant_ids, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new ProductManager()

