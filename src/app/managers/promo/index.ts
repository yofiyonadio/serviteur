import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import PromoCampaignManager from './_campaign'
import PromoCouponManager from './_coupon'
import PromoPrecampaignManager from './_precampaign'
import PromoCashbackManager from './_cashback'
import VoucherManager from '../voucher'

import {
	UserInterface,
	UserProfileInterface,
	ShipmentInterface,
	OrderDetailInterface,
	OrderRequestInterface,
	BrandAddressInterface,
	OrderAddressInterface,
	OrderDetailCouponInterface,
	OrderInterface,
	OrderDetailCampaignInterface,
	ShipmentCouponInterface,
	ShipmentCampaignInterface,
	PacketInterface,
} from 'energie/app/interfaces'
import { CommonHelper } from 'utils/helpers'
import { Parameter } from 'types/common'
import { PaymentRequestInterface } from 'app/interfaces/payment'
import { isEmpty } from 'lodash'


class PromoManager extends ManagerModel {

	static __displayName = 'PromoManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [
		], async () => {
			await PromoCampaignManager.initialize(connection)
			await PromoCouponManager.initialize(connection)
			await PromoPrecampaignManager.initialize(connection)
			await PromoCashbackManager.initialize(connection)
		})
	}

	get campaign(): typeof PromoCampaignManager {
		return PromoCampaignManager
	}

	get coupon(): typeof PromoCouponManager {
		return PromoCouponManager
	}

	get cashback(): typeof PromoCashbackManager {
		return PromoCashbackManager
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getCouponReferralCode(
		user: UserInterface & { profile: UserProfileInterface },
		transaction: EntityManager,
	) {
		return PromoCouponManager.getReferralCode(user, transaction)
	}

	@ManagerModel.bound
	public getRandomVoucherCode(
		prefix: string,
		length: number,
	) {
		return PromoCouponManager.getRandomVoucherCode(prefix, length)
	}

	@ManagerModel.bound
	async getAvailableRandomVoucherCode(
		prefix: string,
		length: number,
		transaction: EntityManager,
	): Promise<string> {
		const code = PromoCouponManager.getRandomVoucherCode(prefix, length)

		if(await VoucherManager.getByCode(code, false, transaction).then(() => true).catch(() => false)) {
			return this.getAvailableRandomVoucherCode(prefix, length, transaction)
		} else {
			return code
		}
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async applyPromotionFromCouponAndCampaign(
		coupon_code: string,
		user: UserInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_campaigns: {
			orderDetailCampaigns: OrderDetailCampaignInterface[],
			shipmentCampaigns: ShipmentCampaignInterface[],
		},
		payment: PaymentRequestInterface | undefined,
		transaction: EntityManager,
	) {
		const coupon = coupon_code ? await PromoCouponManager.getCouponIfValid(user.id, coupon_code, order_details.map(d => d.price).reduce(CommonHelper.sum, 0), transaction) : null
		const couponApplicableOrderDetails = coupon ? await PromoCouponManager.filterApplicableOrderDetails(coupon, order_details, transaction) : []

		// =================================
		// APPLY COUPON
		// =================================

		this.log('Applying coupon.........')

		const appliedCoupons = await Promise.all([
			PromoCouponManager.applyCouponOnOrderDetails(coupon, couponApplicableOrderDetails, applied_campaigns.orderDetailCampaigns),
			PromoCouponManager.applyCouponOnShipments(coupon, couponApplicableOrderDetails.map(d => d.id), shipments, applied_campaigns.shipmentCampaigns, transaction),
		]).then(([oDCs, sCs]) => {
			// CHECK COUPON MAXIMUM SPEND
			if (coupon && coupon.maximum_discount > -1) {
				const totalDiscount = oDCs.map(oDC => oDC.discount).concat(sCs.map(s => s.discount)).reduce(CommonHelper.sum, 0)
				const totalCashback = oDCs.map(oDC => oDC.cashback).concat(sCs.map(s => s.cashback)).reduce(CommonHelper.sum, 0)

				if (totalDiscount > coupon.maximum_discount || totalCashback > coupon.maximum_discount) {
					// Recursively cut coupon spending
					if(coupon.as_cashback) {
						([] as Array<OrderDetailCouponInterface | ShipmentCouponInterface>).concat(oDCs).concat(sCs).reduceRight((diff, couponCashback) => {
							if(diff <= 0) return diff

							const reduction = Math.min(diff, couponCashback.cashback)

							couponCashback.cashback -= reduction
							
							return diff - reduction
						}, totalCashback - coupon.maximum_discount)
				 	} else {
						 ([] as Array<OrderDetailCouponInterface | ShipmentCouponInterface>).concat(oDCs).concat(sCs).reduceRight((diff, couponDiscount) => {
							 if (diff <= 0) {
								 return diff
							 } else {
								 const reduction = Math.min(diff, couponDiscount.discount)
	 
								 // Mutate the discount
								 couponDiscount.discount -= reduction
	 
								 return diff - reduction
							 }
						 }, totalDiscount - coupon.maximum_discount)
					}
				}
			}

			return {
				orderDetailCoupons: oDCs.filter(oDC => !!oDC.cashback ? (oDC.cashback > 0) : oDC.discount >= 0), // oDC.discount >= 0 utk membaca kupon discount Rp 0 otherwise just put > 0
				shipmentCoupons: sCs.filter(sC => !!sC.cashback ? (sC.cashback > 0) : (sC.discount > 0)),
			}
		})

		// =================================
		// APPLY CAMPAIGN
		// =================================

		this.log('Applying campaign.......')

		const appliedCampaigns = await PromoCampaignManager.applyCampaigns(user, order_details, shipments, {
			orderDetails: [...appliedCoupons.orderDetailCoupons, ...applied_campaigns.orderDetailCampaigns],
			shipments: [...appliedCoupons.shipmentCoupons, ...applied_campaigns.shipmentCampaigns],
		}, payment, transaction).then(result => {
			const campaignDiscounts = result.campaigns.map(campaign => {
				return {
					...campaign,
					odPromo: result.orderDetailCampaigns.reduce((i, odc) => odc.campaign_id === campaign.id ? (i + (odc.cashback > 0 ? odc.cashback : odc.discount)) : i, 0),
					sPromo: result.shipmentCampaigns.reduce((i, sc) => sc.campaign_id === campaign.id ? (i + sc.cashback > 0 ? sc.cashback : sc.discount) : i, 0),
				}
			})

			const largestODCampaignPromo = campaignDiscounts.sort((a, b) => b.odPromo - a.odPromo)[0]
			const largestSCampaignPromo = campaignDiscounts.sort((a, b) => b.sPromo - a.sPromo)[0]

			return {
				orderDetailCampaigns: result.orderDetailCampaigns.filter(f => f.campaign_id === largestODCampaignPromo.id),
				shipmentCampaigns: result.shipmentCampaigns.filter(f => f.campaign_id === largestSCampaignPromo.id),
				campaigns: [largestODCampaignPromo, largestSCampaignPromo],
			}
		}).then(result => {
			// Concat given applied promos
			return {
				orderDetailCampaigns: result.orderDetailCampaigns.concat(applied_campaigns.orderDetailCampaigns),
				shipmentCampaigns: result.shipmentCampaigns.concat(applied_campaigns.shipmentCampaigns),
				campaigns: result.campaigns,
			}
		})

		// ===========================================
		// DO ADJUSTMENTS
		// Maximum coupon + campaign value
		// is order detail price or shipment amount
		// ===========================================
		order_details.forEach(orderDetail => {
			const totalAppliedPromo = appliedCoupons.orderDetailCoupons
				.filter(odC => odC.order_detail_id === orderDetail.id)
				.map(odC => odC.discount)
				.concat(appliedCampaigns.orderDetailCampaigns
					.filter(odC => odC.order_detail_id === orderDetail.id)
					.map(odC => odC.discount))
				.reduce(CommonHelper.sum, 0)

			if (totalAppliedPromo > orderDetail.price) {
				// Recursively cut promo spending
				([] as Array<OrderDetailCouponInterface | OrderDetailCampaignInterface>).concat(appliedCoupons.orderDetailCoupons).concat(appliedCampaigns.orderDetailCampaigns).reduceRight((diff, appliedPromo) => {
					if (diff <= 0) {
						return diff
					} else {
						const reduction = Math.min(diff, appliedPromo.discount)

						// Mutate the discount
						appliedPromo.discount -= reduction

						return diff - reduction
					}
				}, totalAppliedPromo - orderDetail.price)
			}
		})

		shipments.forEach(shipment => {
			const totalAppliedPromo = appliedCoupons.shipmentCoupons
				.filter(sC => sC.shipment_id === shipment.id)
				.map(sC => sC.discount)
				.concat(appliedCampaigns.shipmentCampaigns
					.filter(sC => sC.shipment_id === shipment.id)
					.map(sC => sC.discount))
				.reduce(CommonHelper.sum, 0)

			if (totalAppliedPromo > shipment.amount) {
				// Recursively cut promo spending
				([] as Array<ShipmentCouponInterface | ShipmentCampaignInterface>).concat(appliedCoupons.shipmentCoupons).concat(appliedCampaigns.shipmentCampaigns).reduceRight((diff, appliedPromo) => {
					if (diff <= 0) {
						return diff
					} else {
						const reduction = Math.min(diff, appliedPromo.discount)

						// Mutate the discount
						appliedPromo.discount -= reduction

						return diff - reduction
					}
				}, totalAppliedPromo - shipment.amount)
			}
		})

		return {
			coupon,
			campaigns: appliedCampaigns.campaigns,
			orderDetailCampaigns: appliedCampaigns.orderDetailCampaigns.filter(odC => !!odC.cashback ? (odC.cashback > 0) : (odC.discount > 0)),
			orderDetailCoupons: appliedCoupons.orderDetailCoupons.filter(odC => !!odC.cashback ? (odC.cashback > 0) : (odC.discount >= 0)),
			shipmentCampaigns: appliedCampaigns.shipmentCampaigns.filter(sC => sC.discount > 0),
			shipmentCoupons: appliedCoupons.shipmentCoupons.filter(sC => sC.discount > 0),
		}
	}

	@ManagerModel.bound
	async checkCouponValidity(
		user_id: number,
		couponCode: string,
		transaction: EntityManager,
	) {
		return PromoCouponManager.checkCouponValidity(
			user_id,
			couponCode,
			transaction,
		)
	}

	@ManagerModel.bound
	async checkRefundable(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		const coupon = await PromoCouponManager.getCouponFromOrderDetailInput(order_detail_id, transaction).catch(() => {})
		const campaign = await PromoCampaignManager.getCampaignFromOrderDetailInput(order_detail_id, transaction).catch(() => {})

		const isCouponRefundable = isEmpty(coupon) || coupon.is_refundable
		const isCampaignRefundable = isEmpty(campaign) || campaign.is_refundable
		
		return isCouponRefundable && isCampaignRefundable
	}
	
	@ManagerModel.bound
	async checkExchangeable(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		const coupon = await PromoCouponManager.getCouponFromOrderDetailInput(order_detail_id, transaction).catch(() => {})
		const campaign = await PromoCampaignManager.getCampaignFromOrderDetailInput(order_detail_id, transaction).catch(() => {})

		const isCouponExchangeable = isEmpty(coupon) // && coupon.is_exchangeable
		const isCampaignExchangeable = isEmpty(campaign) // && campaign.is_exchangeable
		
		return isCouponExchangeable && isCampaignExchangeable
	}

	@ManagerModel.bound
	async applyPreCampaignPromotion(
		changer_user_id: number,
		order: OrderInterface,
		user: UserInterface,
		couponCode: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	) {
		return PromoPrecampaignManager.applyPreCampaignFromRequest(
			changer_user_id,
			order,
			user,
			couponCode,
			requests,
			is_facade,
			transaction,
		)
	}

	// ============================ PRIVATES ============================

}

export default new PromoManager()
