import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

// import VoucherManager from '../voucher'
// import ShipmentManager from '../shipment'
import ServiceManager from '../service'
import OrderManager from '../order'
import PromoCouponManager from './_coupon'

import {
	CampaignRepository,
} from 'app/repositories'

import {
	CampaignInterface,
	OrderRequestInterface,
	OrderAddressInterface,
	UserInterface,
	OrderDetailInterface,
	OrderDetailCampaignInterface,
	OrderInterface,
	ShipmentCampaignInterface,
	StyleboardInterface,
	ServiceInterface,
} from 'energie/app/interfaces'
import { OrderRequestMatchboxMetadataInterface, OrderRequestVariantMetadataInterface } from 'energie/app/interfaces/order.request'
import { OrderDetailTopupMetadataInterface } from 'energie/app/interfaces/order.detail'

import CommonHelper from 'utils/helpers/common'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES } from 'app/models/error'
import { ORDER_REQUESTS } from 'energie/utils/constants/enum'


class PromoPrecampaignManager extends ManagerModel {

	static __displayName = 'PromoPrecampaignManager'

	protected CampaignRepository: CampaignRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CampaignRepository,
		])
	}


	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async applyPreCampaignFromRequest(
		changer_user_id: number,
		order: OrderInterface,
		user: UserInterface,
		coupon: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	) {
		const campaigns: CampaignInterface[] = await this.CampaignRepository.getAll(transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return []
			}

			throw err
		})

		return campaigns.reduce(async (p, campaign) => {
			return p.then(async sum => {
				return this.applyPreCampaign(
					changer_user_id,
					order,
					campaign,
					user,
					coupon,
					requests,
					is_facade,
					transaction,
				).then(result => {
					return {
						campaigns: [...sum.campaigns, ...result.campaigns],
						orderRequests: [...sum.orderRequests, ...result.orderDetails.reduce((s, oD) => {
							if (s.findIndex(od => od.id === oD.order_request_id) > -1) {
								return s
							} else {
								return [...s, oD.request]
							}
						}, [] as Array<OrderRequestInterface & {
							address: OrderAddressInterface,
						}>)],
						orderDetails: [...sum.orderDetails, ...result.orderDetails],
						orderDetailCampaigns: [...sum.orderDetailCampaigns, ...result.orderDetailCampaigns],
						shipmentCampaigns: [...sum.shipmentCampaigns, ...result.shipmentCampaigns],
					}
				})
			})
		}, Promise.resolve({
			campaigns: [] as CampaignInterface[],
			orderRequests: [] as Array<OrderRequestInterface & {
				address: OrderAddressInterface,
			}>,
			orderDetails: [] as Array<OrderDetailInterface & {
				request: OrderRequestInterface & {
					address: OrderAddressInterface,
				},
			}>,
			orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
			shipmentCampaigns: [] as ShipmentCampaignInterface[],
		}))
	}

	// ============================ PRIVATES ============================
	private applyPreCampaign = async (
		changer_user_id: number,
		order: OrderInterface,
		campaign: CampaignInterface,
		user: UserInterface,
		coupon: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	) => {
		switch (campaign.slug) {

		// ======================================
		// APPLY CAMPAIGN MANUALLY
		// NOTE: COUPON CAN BE NULL
		// ======================================

		case 'bogo-mini':
			return this.bogo(campaign, changer_user_id, order, user, coupon, requests, is_facade, transaction)
		// Disable for now
		// case 'styleboard-styling-fee':
		// 	return this.styleboardStylingFeeRefund(campaign, changer_user_id, order, requests, is_facade, transaction)
		case 'active-home-bogo':
			return this.activebogo(campaign, changer_user_id, order, user, coupon, requests, is_facade, transaction)
		case 'payday-1-day-basic-mini':
			return this.paydayOneDayBasicMini(campaign, changer_user_id, order, user, coupon, requests, is_facade, transaction)
		default:
			return {
				campaigns: [] as CampaignInterface[],
				orderDetails: [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface & {
						address: OrderAddressInterface,
					},
				}>,
				orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
				shipmentCampaigns: [] as ShipmentCampaignInterface[],
			}
		}
	}

	private async bogo(
		campaign: CampaignInterface,
		changer_user_id: number,
		order: OrderInterface,
		user: UserInterface,
		coupon: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	): Promise<{
		campaigns: CampaignInterface[],
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		shipmentCampaigns: ShipmentCampaignInterface[],
	}> {
		// ==============================================
		// DESCRIPTION:
		// Buy one anything get one mini
		// ==============================================
		// check from order detail, not order request
		// - for all matchbox only
		// - not valid for any coupon application
		// - not valid for voucher request
		// - max. 1 free per purchase

		const hasCoupon = await PromoCouponManager.checkCouponValidity(user.id, coupon, transaction).catch(() => null)
		const order_date = new Date()

		if(hasCoupon) {
			return {
				campaigns: [],
				orderDetails: [],
				orderDetailCampaigns: [],
				shipmentCampaigns: [],
			}
		} else {
			return requests.reduce(async (p, request) => {
				return p.then(async sum => {

					// max. 1 free per puchase
					if (sum.orderDetailCampaigns.findIndex(odC => odC.campaign_id === campaign.id) > -1) {
						return sum
					}

					const requestMatchboxId = request.as_voucher === false
						? request.type === ORDER_REQUESTS.MATCHBOX
							? request.ref_id
							: null
						: null

					if (requestMatchboxId !== 15 && requestMatchboxId !== 1) {
						const orderDetails = await OrderManager.detail.createFromRequest(
							changer_user_id,
							order,
							// CREATE NEW REQUEST FOR MINI
							[await OrderManager.request.createCustom({
								order_id: request.order_id,
								order_address_id: request.order_address_id,
								type: ORDER_REQUESTS.MATCHBOX,
								ref_id: 2, // Mini ID
								quantity: 1,
								as_voucher: false,
								address: request.address,
								metadata: CommonHelper.stripUndefined({
									...request.metadata as OrderRequestMatchboxMetadataInterface,
									lineup: undefined,
								}),
							}, is_facade, transaction)],
							is_facade,
							order_date,
							transaction,
						)

						return {
							campaigns: sum.campaigns.findIndex(c => c.id === campaign.id) > -1 ? sum.campaigns : [...sum.campaigns, campaign],
							orderDetails: [...sum.orderDetails, ...orderDetails],
							orderDetailCampaigns: [...sum.orderDetailCampaigns, ...orderDetails.map(orderDetail => {
								return {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									discount: orderDetail.price + orderDetail.handling_fee,
									is_refundable: campaign.is_refundable,
									cashback: 0,
								}
							})],
							shipmentCampaigns: sum.shipmentCampaigns,
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve({
				campaigns: [] as CampaignInterface[],
				orderDetails: [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface & {
						address: OrderAddressInterface,
					},
				}>,
				orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
				shipmentCampaigns: [] as ShipmentCampaignInterface[],
			}))
		}
	}

	private async activebogo(
		campaign: CampaignInterface,
		changer_user_id: number,
		order: OrderInterface,
		user: UserInterface,
		coupon: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	): Promise<{
		campaigns: CampaignInterface[],
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		shipmentCampaigns: ShipmentCampaignInterface[],
	}> {
		// ==============================================
		// DESCRIPTION:
		// Buy one anything get one mini
		// ==============================================
		// check from order detail, not order request
		// - for active matchbox only
		// - not valid for any coupon application
		// - not valid for voucher request
		// - max. 1 free per purchase

		const hasCoupon = await PromoCouponManager.checkCouponValidity(user.id, coupon, transaction).catch(() => null)
		const order_date = new Date()

		if(hasCoupon) {
			return {
				campaigns: [],
				orderDetails: [],
				orderDetailCampaigns: [],
				shipmentCampaigns: [],
			}
		} else {
			return requests.reduce(async (p, request) => {
				return p.then(async sum => {

					// max. 1 free per puchase
					if (sum.orderDetailCampaigns.length) {
						return sum
					}

					const requestMatchboxId = request.as_voucher === false
						? request.type === ORDER_REQUESTS.MATCHBOX
							? request.ref_id
							: null
						: null

					if (requestMatchboxId === 15) {
						const orderDetails = await OrderManager.detail.createFromRequest(
							changer_user_id,
							order,
							// CREATE NEW REQUEST FOR HOME
							[await OrderManager.request.createCustom({
								order_id: request.order_id,
								order_address_id: request.order_address_id,
								type: ORDER_REQUESTS.MATCHBOX,
								ref_id: 14, // HOME ID
								quantity: 1,
								as_voucher: false,
								address: request.address,
								metadata: CommonHelper.stripUndefined({
									...request.metadata as OrderRequestMatchboxMetadataInterface,
									lineup: undefined,
								}),
							}, is_facade, transaction)],
							is_facade,
							order_date,
							transaction,
						)

						return {
							campaigns: sum.campaigns.findIndex(c => c.id === campaign.id) > -1 ? sum.campaigns : [...sum.campaigns, campaign],
							orderDetails: [...sum.orderDetails, ...orderDetails],
							orderDetailCampaigns: [...sum.orderDetailCampaigns, ...orderDetails.map(orderDetail => {
								return {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									discount: orderDetail.price + orderDetail.handling_fee,
									cashback: 0,
									is_refundable: campaign.is_refundable,
								}
							})],
							shipmentCampaigns: sum.shipmentCampaigns,
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve({
				campaigns: [] as CampaignInterface[],
				orderDetails: [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface & {
						address: OrderAddressInterface,
					},
				}>,
				orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
				shipmentCampaigns: [] as ShipmentCampaignInterface[],
			}))
		}
	}

	// This is not used at the moment
	// tslint:disable-next-line: member-ordering
	async styleboardStylingFeeRefund(
		campaign: CampaignInterface,
		changer_user_id: number,
		order: OrderInterface,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	): Promise<{
		campaigns: CampaignInterface[],
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		shipmentCampaigns: ShipmentCampaignInterface[],
	}> {
		// ==============================================
		// DESCRIPTION:
		// Free styling fee credit for purchase of 1++ item from
		// Styleboard request type
		// ==============================================
		// check from order detail, not order request
		// - for session-result stylesheet only
		// - must take into account applied promo
		const styleboards: Array<StyleboardInterface & {
			service: ServiceInterface,
		}> = []

		return requests.reduce(async (p, request) => {
			return p.then(async sum => {

				// max. 1 credit
				if (sum.orderDetailCampaigns.findIndex(odC => odC.campaign_id === campaign.id) > -1) {
					return sum
				}

				const metadata = request.metadata as OrderRequestVariantMetadataInterface
				const order_date = new Date()

				if (request.type === ORDER_REQUESTS.VARIANT && metadata.styleboard_id) {
					const styleboard = styleboards.find(s => s.id === metadata.styleboard_id) || await ServiceManager.styleboard.get(metadata.styleboard_id, {
						with_service: true,
					}, transaction).then(s => {
						styleboards.push(s)

						return s
					})

					if (styleboard.service.price) {
						const orderDetails = await OrderManager.detail.createFromRequest(
							changer_user_id,
							order,
							// CREATE NEW REQUEST FOR MINI
							[await OrderManager.request.createCustom({
								order_id: request.order_id,
								order_address_id: request.order_address_id,
								type: ORDER_REQUESTS.TOPUP,
								ref_id: DEFAULTS.WALLET_TOPUP_ID,
								quantity: 1,
								as_voucher: false,
								address: request.address,
								metadata: CommonHelper.stripUndefined({
									amount: styleboard.service.price,
								}) as OrderDetailTopupMetadataInterface,
							}, is_facade, transaction)],
							is_facade,
							order_date,
							transaction,
						)

						return {
							campaigns: sum.campaigns.findIndex(c => c.id === campaign.id) > -1 ? sum.campaigns : [...sum.campaigns, campaign],
							orderDetails: [...sum.orderDetails, ...orderDetails],
							orderDetailCampaigns: [...sum.orderDetailCampaigns, ...orderDetails.map(orderDetail => {
								return {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									discount: orderDetail.price + orderDetail.handling_fee,
									cashback: 0,
									is_refundable: campaign.is_refundable,
								}
							})],
							shipmentCampaigns: sum.shipmentCampaigns,
						}
					} else {
						return sum
					}
				} else {
					return sum
				}
			})
		}, Promise.resolve({
			campaigns: [] as CampaignInterface[],
			orderDetails: [] as Array<OrderDetailInterface & {
				request: OrderRequestInterface & {
					address: OrderAddressInterface,
				},
			}>,
			orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
			shipmentCampaigns: [] as ShipmentCampaignInterface[],
		}))

		// return {
		// 	orderDetails: await order_details.reduce(async (p, orderDetail) => {
		// 		return p.then(async sum => {
		// 			// max discount is order detail price
		// 			// subtracted by campaigns already applied (pre-campaign?)
		// 			let discount = 0
		// 			const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
		// 			const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

		// 			if (orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT && metadata.styleboard_id) {
		// 				const count = order_details.filter(oD => {
		// 					return orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT && (oD.request.metadata as OrderRequestVariantMetadataInterface).styleboard_id === metadata.styleboard_id
		// 				}).length

		// 				if (count > 0 && sum.findIndex(s => s.campaign_id === campaign.id) === -1) {
		// 					const styleboard = await ServiceManager.styleboard.get(orderDetail.request.ref_id, { with_service: true }, transaction)
		// 					discount = styleboard.service.price
		// 				}
		// 			}

		// 			// We must check if full discount is applicable or not, since this discount is
		// 			// clinging to a variant instead of groups, so the variant price might be lower than
		// 			// the actual discount amount. We do not allow that.
		// 			if (discount > 0 &&) {
		// 				return [...sum, {
		// 					order_detail_id: orderDetail.id,
		// 					campaign_id: campaign.id,
		// 					is_refundable: campaign.is_refundable,
		// 					discount: Math.min(discount, maxDiscount),
		// 				}]
		// 			} else {
		// 				return sum
		// 			}
		// 		})
		// 	}, Promise.resolve([] as OrderDetailCampaignInterface[])),
		// 	shipments: [],
		// }
	}

	private async paydayOneDayBasicMini(
		campaign: CampaignInterface,
		changer_user_id: number,
		order: OrderInterface,
		user: UserInterface,
		coupon: string,
		requests: Array<OrderRequestInterface & {
			address: OrderAddressInterface,
		}>,
		is_facade: boolean,
		transaction: EntityManager,
	): Promise<{
		campaigns: CampaignInterface[],
		orderDetails: Array<OrderDetailInterface & {
			request: OrderRequestInterface & {
				address: OrderAddressInterface,
			},
		}>,
		orderDetailCampaigns: OrderDetailCampaignInterface[],
		shipmentCampaigns: ShipmentCampaignInterface[],
	}> {
		// ==============================================
		// DESCRIPTION:
		// Buy one anything get one mini
		// ==============================================
		// check from order detail, not order request
		// - for active matchbox only
		// - not valid for any coupon application
		// - not valid for voucher request
		// - max. 1 free per purchase

		const hasCoupon = await PromoCouponManager.checkCouponValidity(user.id, coupon, transaction).catch(() => null)

		if(hasCoupon) {
			return {
				campaigns: [],
				orderDetails: [],
				orderDetailCampaigns: [],
				shipmentCampaigns: [],
			}
		} else {
			return requests.reduce(async (p, request) => {
				return p.then(async sum => {

					// max. 1 free per puchase
					if (sum.orderDetailCampaigns.length) {
						return sum
					}

					const requestMatchboxId = request.as_voucher === false
						? request.type === ORDER_REQUESTS.MATCHBOX
							? request.ref_id
							: null
						: null

					if (requestMatchboxId === 1) {
						const order_date = new Date()
						const orderDetails = await OrderManager.detail.createFromRequest(
							changer_user_id,
							order,
							// CREATE NEW REQUEST FOR HOME
							[await OrderManager.request.createCustom({
								order_id: request.order_id,
								order_address_id: request.order_address_id,
								type: ORDER_REQUESTS.MATCHBOX,
								ref_id: 2, // Mini ID
								quantity: 1,
								as_voucher: false,
								address: request.address,
								metadata: CommonHelper.stripUndefined({
									...request.metadata as OrderRequestMatchboxMetadataInterface,
									lineup: undefined,
								}),
							}, is_facade, transaction)],
							is_facade,
							order_date,
							transaction,
						)

						return {
							campaigns: sum.campaigns.findIndex(c => c.id === campaign.id) > -1 ? sum.campaigns : [...sum.campaigns, campaign],
							orderDetails: [...sum.orderDetails, ...orderDetails],
							orderDetailCampaigns: [...sum.orderDetailCampaigns, ...orderDetails.map(orderDetail => {
								return {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									discount: orderDetail.price + orderDetail.handling_fee,
									cashback: 0,
									is_refundable: campaign.is_refundable,
								}
							})],
							shipmentCampaigns: sum.shipmentCampaigns,
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve({
				campaigns: [] as CampaignInterface[],
				orderDetails: [] as Array<OrderDetailInterface & {
					request: OrderRequestInterface & {
						address: OrderAddressInterface,
					},
				}>,
				orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
				shipmentCampaigns: [] as ShipmentCampaignInterface[],
			}))
		}
	}
}

export default new PromoPrecampaignManager()
