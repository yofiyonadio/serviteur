import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import PacketManager from '../packet'
// import ShipmentManager from '../shipment'
import UserManager from '../user'

import {
	CouponRepository,
} from 'app/repositories'

import {
	CouponInterface,
	UserInterface,
	OrderDetailInterface,
	ShipmentInterface,
	MatchboxCouponInterface,
	ProductCouponInterface,
	VariantCouponInterface,
	UserCouponInterface,
	UserProfileInterface,
	OrderRequestInterface,
	BrandAddressInterface,
	OrderAddressInterface,
	ServiceCouponInterface,
	OrderDetailCouponInterface,
	OrderDetailCampaignInterface,
	ShipmentCouponInterface,
	ShipmentCampaignInterface,
	PacketInterface,
	StylecardCouponInterface,
} from 'energie/app/interfaces'

import CommonHelper from 'utils/helpers/common'
import FormatHelper from 'utils/helpers/format'
import { ObjectOrArray, Parameter, Await, Unarray } from 'types/common'

import DEFAULTS from 'utils/constants/default'
import { ERRORS, ERROR_CODES } from 'app/models/error'
import { COUPONS, ORDER_REQUESTS, COUPON_VALIDITY } from 'energie/utils/constants/enum'

import {
	padEnd,
	isEmpty,
} from 'lodash'

const POSSIBLE_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
const POSSIBLE_CHARS_LENGTH = POSSIBLE_CHARS.length

import TimeHelper from 'utils/helpers/time'
import { MatchboxManager } from '..'


class PromoCouponManager extends ManagerModel {

	static __displayName = 'PromoCouponManager'

	protected CouponRepository: CouponRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CouponRepository,
		])
	}

	@ManagerModel.bound
	async checkCouponValidity(
		user_id: number,
		couponCode: string,
		transaction: EntityManager,
	) {
		return this.getCouponIfValid(
			user_id,
			couponCode,
			undefined,
			transaction,
		)
	}

	// ============================= INSERT =============================
	async create(data: CouponInterface, config: {
		validMatchboxIds?: number[],
		validProductIds?: number[],
		validServiceIds?: number[],
		validVariantIds?: number[],
		validUserIds?: number[],
		validStylecardIds?: number[],
	}, transaction: EntityManager): Promise<CouponInterface & {
		couponMatchboxes?: MatchboxCouponInterface[],
		couponProducts?: ProductCouponInterface[],
		couponServices?: ServiceCouponInterface[],
		couponVariants?: VariantCouponInterface[],
		couponUsers?: UserCouponInterface[],
		couponStylecards?: StylecardCouponInterface[],
	}>
	async create(data: CouponInterface[], config: {
		validMatchboxIds?: number[],
		validProductIds?: number[],
		validServiceIds?: number[],
		validVariantIds?: number[],
		validUserIds?: number[],
		validStylecardIds?: number[],
	}, transaction: EntityManager): Promise<Array<CouponInterface & {
		couponMatchboxes?: MatchboxCouponInterface[],
		couponProducts?: ProductCouponInterface[],
		couponServices?: ServiceCouponInterface[],
		couponVariants?: VariantCouponInterface[],
		couponUsers?: UserCouponInterface[],
		couponStylecards?: StylecardCouponInterface[],
	}>>

	@ManagerModel.bound
	async create(data: CouponInterface | CouponInterface[], config: {
		validMatchboxIds?: number[],
		validProductIds?: number[],
		validServiceIds?: number[],
		validVariantIds?: number[],
		validUserIds?: number[],
		validStylecardIds?: number[],
	} = {}, transaction: EntityManager): Promise<ObjectOrArray<CouponInterface & {
		couponMatchboxes?: MatchboxCouponInterface[],
		couponProducts?: ProductCouponInterface[],
		couponServices?: ServiceCouponInterface[],
		couponVariants?: VariantCouponInterface[],
		couponUsers?: UserCouponInterface[],
		couponStylecards?: StylecardCouponInterface[],
	}>> {
		let coupons: CouponInterface[]

		if (Array.isArray(data)) {
			// guard if create stylecard coupon with total type
			const validfor = data.reduce((i, d) => i.concat(...d.valid_for), [])

			if (data.findIndex(f => f.type !== 'PERCENT') > -1 && validfor.findIndex(f => f === 'STYLECARD') > -1) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard Coupon only accept percent discount')
			}

			coupons = await this.CouponRepository.insert(data, transaction)
		} else {
			// guard if create stylecard coupon with total type
			if (data.valid_for.findIndex(f => f === 'STYLECARD') > -1 && data.type !== 'PERCENT') {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard Coupon only accept percent discount')
			}

			coupons = [await this.CouponRepository.insert(data, transaction)]
		}

		coupons = await Promise.all(coupons.map(async coupon => {
			return this.CouponRepository.update(
				coupon.id,
				null,
				config.validMatchboxIds,
				config.validProductIds,
				config.validServiceIds,
				config.validVariantIds,
				config.validUserIds,
				config.validStylecardIds,
				transaction,
			).then(([couponMatchboxes, couponProducts, couponServices, couponVariants, couponUsers]) => {
				return {
					...coupon,
					couponMatchboxes,
					couponProducts,
					couponServices,
					couponVariants,
					couponUsers,
				}
			})
		}))

		if (Array.isArray(coupons)) {
			return coupons
		} else {
			return coupons[0]
		}
	}

	@ManagerModel.bound
	async addProduct(
		coupon_id: number,
		type: 'MATCHBOX' | 'PRODUCT' | 'SERVICE' | 'USER' | 'VARIANT' | 'STYLECARD',
		type_id: number,
		amount: number | null,
		transaction: EntityManager,
	) {
		switch (type) {
		case 'MATCHBOX':
			return this.CouponRepository.addCouponMatchbox(coupon_id, type_id, transaction)
		case 'PRODUCT':
			return this.CouponRepository.addCouponProduct(coupon_id, type_id, amount, transaction)
		case 'SERVICE':
			return this.CouponRepository.addCouponService(coupon_id, type_id, transaction)
		case 'USER':
			return this.CouponRepository.addCouponUser(coupon_id, type_id, transaction)
		case 'VARIANT':
			return this.CouponRepository.addCouponVariant(coupon_id, type_id, transaction)
		case 'STYLECARD':
			const coupon = await this.CouponRepository.getCoupon(coupon_id, transaction)

			if (coupon.type !== COUPONS.PERCENT) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard Coupon only accept percent discount')
			}

			return this.CouponRepository.addCouponStylecard(coupon_id, type_id, transaction)
		}
	}

	// ============================= UPDATE =============================
	@ManagerModel.bound
	async update(
		coupon_id: number,
		data: Partial<Parameter<CouponInterface>>,
		transaction: EntityManager,
	) {
		const coupon = await this.CouponRepository.getCoupon(coupon_id, transaction)

		if (data.valid_for) {
			if (data.valid_for.findIndex(f => f === 'STYLECARD') > -1 && coupon.type !== 'PERCENT') {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard Coupon only accept percent discount')
			}
		}

		if (data.type) {
			if (coupon.valid_for.findIndex(f => f === 'STYLECARD') > -1 && data.type !== 'PERCENT') {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Stylecard Coupon only accept percent discount')
			}
		}

		return this.CouponRepository.update(
			coupon_id,
			data,
			undefined,
			undefined,
			undefined,
			undefined,
			undefined,
			undefined,
			transaction,
		)
	}

	get updateCouponProduct() {
		return this.CouponRepository.updateCouponProduct
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async filter(
		offset: number = 0,
		limit: number = 40,
		search: string | undefined,
		filter: {
			validity?: COUPON_VALIDITY,
			date?: '1 days' | '7 days' | '1 months' | '2 months' | '3 months' | '1 years',
			expired?: boolean,
			published: boolean,
			is_public: boolean,
			order_detail_id?: number,
		},
		transaction: EntityManager,
	) {
		return this.CouponRepository.filter(offset, limit, search, filter, transaction)
	}
	
	@ManagerModel.bound
	getCouponFromOrderDetailInput(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponFromOrderDetailInput(order_detail_id, transaction)
	}

	@ManagerModel.bound
	async get(
		id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponWithCount(id, transaction)
	}

	@ManagerModel.bound
	async getAvailable(
		user_id: number | undefined,
		type: COUPON_VALIDITY | undefined,
		for_all_recommendation: boolean | undefined,
		is_public: boolean,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getAvailableCoupon(user_id, type, for_all_recommendation, is_public, transaction)
	}

	@ManagerModel.bound
	async getDetailed(
		id: number,
		transaction: EntityManager,
	) {
		return Promise.all([
			this.CouponRepository.getCouponWithCount(id, transaction),
			this.CouponRepository.getCouponMatchbox(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponMatchbox']>>),
			this.CouponRepository.getCouponService(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponService']>>),
			this.CouponRepository.getCouponUser(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponUser']>>),
			this.CouponRepository.getCouponProduct(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponProduct']>>),
			this.CouponRepository.getCouponVariant(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponVariant']>>),
			this.CouponRepository.getCouponStylecard(id, transaction).catch(err => [] as Await<ReturnType<CouponRepository['getCouponStylecard']>>),
		]).then(([coupon, matchboxes, services, users, products, variants, stylecards]) => {
			return {
				...coupon,
				matchboxes,
				services,
				users,
				products,
				variants,
				stylecards,
			}
		})
	}

	@ManagerModel.bound
	async getStylecards(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponStylecard(coupon_id, transaction)
	}

	@ManagerModel.bound
	async getMatchboxes(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponMatchbox(coupon_id, transaction)
	}

	@ManagerModel.bound
	async getProducts(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponProduct(coupon_id, transaction)
	}

	@ManagerModel.bound
	async getServices(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponService(coupon_id, transaction)
	}

	@ManagerModel.bound
	async getUsers(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponUser(coupon_id, transaction)
	}

	@ManagerModel.bound
	async getVariants(
		coupon_id: number,
		transaction: EntityManager,
	) {
		return this.CouponRepository.getCouponVariant(coupon_id, transaction)
	}

	// ============================= DELETE =============================
	@ManagerModel.bound
	deleteProduct(
		coupon_id: number,
		type: 'MATCHBOX' | 'PRODUCT' | 'SERVICE' | 'USER' | 'VARIANT' | 'STYLECARD',
		type_id: number,
		transaction: EntityManager,
	) {
		switch (type) {
		case 'MATCHBOX':
			return this.CouponRepository.deleteCouponMatchbox(coupon_id, type_id, transaction)
		case 'PRODUCT':
			return this.CouponRepository.deleteCouponProduct(coupon_id, type_id, transaction)
		case 'SERVICE':
			return this.CouponRepository.deleteCouponService(coupon_id, type_id, transaction)
		case 'USER':
			return this.CouponRepository.deleteCouponUser(coupon_id, type_id, transaction)
		case 'VARIANT':
			return this.CouponRepository.deleteCouponVariant(coupon_id, type_id, transaction)
		case 'STYLECARD':
			return this.CouponRepository.deleteCouponStylecard(coupon_id, type_id, transaction)
		}
	}

	// ============================= METHODS ============================
	@ManagerModel.bound
	async applyCouponOnOrderDetails(
		coupon: CouponInterface,
		applicable_order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_campaigns: OrderDetailCampaignInterface[],
	) {
		if (!coupon) {
			return []
		}

		return applicable_order_details.reduce(async (p, orderDetail) => {
			return p.then(async sum => {
				// max discount is order detail price
				// subtracted by campaigns already applied (pre-campaign?)
				let discount = 0
				let cashback = 0

				let maxDiscount = 0
				let maxCashback = 0

				if (coupon.as_cashback) {
					maxCashback = Math.min(Math.max(0, orderDetail.price - applied_campaigns.filter(c => c.order_detail_id === orderDetail.id).map(c => c.cashback).reduce(CommonHelper.sum, 0)), orderDetail.price)
				} else {
					maxDiscount = Math.min(Math.max(0, orderDetail.price - applied_campaigns.filter(c => c.order_detail_id === orderDetail.id).map(c => c.discount).reduce(CommonHelper.sum, 0)), orderDetail.price)
				}

				switch (coupon.type) {
				case COUPONS.PERCENT:
					if (coupon.as_cashback) {
						cashback = Math.min(maxCashback, Math.floor(orderDetail.price * coupon.amount / 100))
					} else {
						discount = Math.min(maxDiscount, Math.floor(orderDetail.price * coupon.amount / 100))
					}

					break
				case COUPONS.TOTAL:
					if (coupon.as_cashback) {
						cashback = Math.min(maxCashback, coupon.amount)
					} else {
						discount = Math.min(maxDiscount, coupon.amount)
					}
					
					break
				}

				if (discount >= 0 || cashback > 0) {
					return [...sum, {
						order_detail_id: orderDetail.id,
						coupon_id: coupon.id,
						source: (orderDetail.request.metadata as any).source,
						medium: (orderDetail.request.metadata as any).medium,
						ref: (orderDetail.request.metadata as any).ref,
						device: (orderDetail.request.metadata as any).device,
						utm_source: (orderDetail.request.metadata as any).utm_source,
						utm_medium: (orderDetail.request.metadata as any).utm_medium,
						utm_campaign: (orderDetail.request.metadata as any).utm_campaign,
						cashback,
						free_shipping: coupon.free_shipping,
						metadata: coupon.metadata,
						discount: (coupon.metadata as any).for_all_recommendation ? (
							!isEmpty((orderDetail.request.metadata as any).source) ? (
								((orderDetail.request.metadata as any).source === 'recommendation' ||
								(orderDetail.request.metadata as any).source.slice(0, 6) === 'wareco') ?
								discount : 0
							) : 0
						) : discount,
					}]
				} else {
					return sum
				}
			})
		}, Promise.resolve([] as OrderDetailCouponInterface[]))
	}


	// //------------------------------ Function Validasi Copun For All Recommendation
	// validCouponForAll(
	// 	datas: object
	// ) {
	// 	for (let i in datas) {
	// 		let for_all_rec = datas[i]["metadata"]["for_all_recommendation"];
	// 		(datas[i]["source"] !== "recommendation") && (for_all_rec) ? datas[i]["discount"] = 0 : null;
	// 	}
	// 	return datas
	// }
	// //------------------------------




	@ManagerModel.bound
	async applyCouponOnShipments(
		coupon: CouponInterface,
		applicable_order_detail_ids: number[],
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			destination: OrderAddressInterface,
			packet: Parameter<PacketInterface>,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_campaigns: ShipmentCampaignInterface[],
		transaction: EntityManager,
	) {
		if (!coupon || !coupon.free_shipping) {
			return []
		}

		return shipments.reduce(async (p, shipment) => {
			return p.then(async sum => {
				const shipmentPacket = shipment.packet
				const discount = await shipment.orderDetails.filter(d => {
					return applicable_order_detail_ids.indexOf(d.id) > -1
				}).reduce(async (_p, orderDetail) => {
					const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
					return _p.then(weight => weight + packet.weight)
				}, Promise.resolve(0)).then(weight => {
					// ================================================
					// If shipment contain multiple order detail, then
					// the free shipping discount will be counted
					// proportional on the weight percentage.
					// ================================================
					const discountByPacketWeight = Math.floor(shipment.amount * (weight / shipmentPacket.weight))

					return Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), shipment.amount - applied_campaigns.filter(s => s.shipment_id === shipment.id).map(s => s.discount).reduce(CommonHelper.sum, 0))
				})

				if (discount > 0) {
					return [...sum, {
						shipment_id: shipment.id,
						coupon_id: coupon.id,
						discount,
						cashback: 0,
					}]
				} else {
					return sum
				}
			})
		}, Promise.resolve([] as ShipmentCouponInterface[]))
	}

	@ManagerModel.bound
	async filterApplicableOrderDetails<T extends OrderDetailInterface & {
		request: OrderRequestInterface,
	}>(
		coupon: CouponInterface,
		order_details: T[],
		transaction: EntityManager,
	) {
		let item_used = 0

		return Promise.all(order_details.map(async orderDetail => {

			// ======================================
			// FILTER INCLUDED PRODUCTS
			// ======================================

			switch (orderDetail.request.type) {
			case ORDER_REQUESTS.STYLEBOARD:
				if (item_used < coupon.order_limit || coupon.order_limit === -1) {
					if (coupon.valid_for.indexOf(COUPON_VALIDITY.MATCHBOX) > -1) {

						if (coupon.valid_for_all) {
							item_used += 1
							return orderDetail
						}

						return this.CouponRepository.getValidityForMatchbox(DEFAULTS.STYLING_RESULT_MATCHBOX_ID, coupon.id, transaction).then(isValid => {
							item_used = isValid ? item_used += 1 : item_used
							return isValid ? orderDetail : false
						})
					} else {
						return false
					}
				} else {
					return false
				}
			case ORDER_REQUESTS.MATCHBOX:
				if (item_used < coupon.order_limit || coupon.order_limit === -1) {
					if (coupon.valid_for.indexOf(COUPON_VALIDITY.MATCHBOX) > -1) {

						if (coupon.valid_for_all) {
							item_used += 1
							return orderDetail
						}

						return this.CouponRepository.getValidityForMatchbox(orderDetail.request.ref_id, coupon.id, transaction).then(isValid => {
							item_used = isValid ? item_used += 1 : item_used
							return isValid ? orderDetail : false
						})
					} else {
						return false
					}
				} else {
					return false
				}
			case ORDER_REQUESTS.SERVICE:
				if (item_used < coupon.order_limit || coupon.order_limit === -1) {
					if (coupon.valid_for.indexOf(COUPON_VALIDITY.SERVICE) > -1) {

						if (coupon.valid_for_all) {
							item_used += 1
							return orderDetail
						}

						return this.CouponRepository.getValidityForService(orderDetail.request.ref_id, coupon.id, transaction).then(isValid => {
							item_used = isValid ? item_used += 1 : item_used
							return isValid ? orderDetail : false
						})
					} else {
						return false
					}
				} else {
					return false
				}
			case ORDER_REQUESTS.VARIANT:
				if (item_used < coupon.order_limit || coupon.order_limit === -1) {
					if (coupon.valid_for.indexOf(COUPON_VALIDITY.PRODUCT) > -1) {

						if (coupon.valid_for_all) {
							if ((coupon.metadata as any).internal_product_only) {
								return this.CouponRepository.getValidityForVariant(orderDetail.request.ref_id, coupon.id, true, (coupon.metadata as any).internal_product_only, transaction).then(isValid => {
									item_used = isValid ? item_used += 1 : item_used
									return isValid ? orderDetail : false
								})
							} else {
								item_used += 1

								return orderDetail
							}
						}

						return this.CouponRepository.getValidityForVariant(orderDetail.request.ref_id, coupon.id, false, (coupon.metadata as any).internal_product_only, transaction).then(isValid => {
							item_used = isValid ? item_used += 1 : item_used
							return isValid ? orderDetail : false
						})
					} else if (coupon.valid_for.indexOf(COUPON_VALIDITY.STYLECARD) > -1) {
						if (!!(orderDetail.request.metadata as any).stylecard_id) {
							if (coupon.valid_for_all) {

								if (!!(coupon.metadata as any)?.for_all_recomendation && (coupon.metadata as any).for_all_recomendation) {
									return this.CouponRepository.getValidityForStylecardCluster((orderDetail.request.metadata as any).stylecard_id, (coupon.metadata as any).for_all_recomendation, undefined, transaction).then(isValid => {
										item_used = isValid ? item_used += 1 : item_used
										return isValid ? orderDetail : false
									})
								} else if (!!(coupon.metadata as any)?.cluster_ids && (coupon.metadata as any)?.cluster_ids.length > 0) {
									return this.CouponRepository.getValidityForStylecardCluster((orderDetail.request.metadata as any).stylecard_id, undefined, (coupon.metadata as any)?.cluster_ids, transaction).then(isValid => {
										item_used = isValid ? item_used += 1 : item_used
										return isValid ? orderDetail : false
									})
								}

								item_used += 1
								return orderDetail
							}

							return this.CouponRepository.getValidityForStylecard((orderDetail.request.metadata as any).stylecard_id, coupon.id, transaction).then(isValid => {
								return isValid ? orderDetail : false
							})
						} else {
							return false
						}
					} else {
						return false
					}
				} else {
					return false
				}
			case ORDER_REQUESTS.VOUCHER:
			default:
				return false
			}
		})).then(values => {
			return values.filter(val => (val !== false)) as Array<Exclude<Unarray<typeof values>, false>>
		})
	}

	@ManagerModel.bound
	public getRandomVoucherCode(prefix = '', length: number = 4) {
		return this.makeid(prefix, length)
	}

	@ManagerModel.bound
	async getReferralCode(
		user: UserInterface & { profile: UserProfileInterface },
		transaction: EntityManager,
	) {
		if (user.profile.coupon_id) {
			return this.CouponRepository.getCoupon(user.profile.coupon_id, transaction)
		} else {
			return this.CouponRepository.insert({
				title: this.makeid(padEnd(user.profile.first_name, 4, user.profile.first_name).substring(0, 4).toUpperCase()),
				description: `Referral code of user[${user.id}] ${user.profile.first_name} ${user.profile.last_name}.`,
				type: COUPONS.PERCENT,
				amount: 30,
				usage_limit_per_user: 1,
				valid_for: [COUPON_VALIDITY.MATCHBOX],
				valid_for_all: false,
				is_public: false,
				is_refundable: true,
			}, transaction).then(async coupon => {

				await Promise.all([
					await this.CouponRepository.addCouponMatchbox(coupon.id, await MatchboxManager.getBySlug('basic', false, transaction).then(m => m.id), transaction),
					await this.CouponRepository.addCouponMatchbox(coupon.id, await MatchboxManager.getBySlug('mini', false, transaction).then(m => m.id), transaction),
					await this.CouponRepository.addCouponMatchbox(coupon.id, await MatchboxManager.getBySlug('deluxe', false, transaction).then(m => m.id), transaction),
				])

				user.profile.coupon_id = coupon.id

				await UserManager.updateProfile(user.id, { coupon_id: coupon.id }, transaction)

				return coupon
			}).catch(err => {
				this.warn(err)

				return this.getReferralCode(user, transaction)
			})
		}
	}

	// ============================ PRIVATES ============================
	// TODO: CHECK
	async getCouponIfValid(
		user_id: number,
		couponCode: string,
		totalAmount: number = 0,
		transaction: EntityManager,
	) {
		const coupon = await this.CouponRepository.getByCode(couponCode, transaction)

		// CHECK PUBLISHED AND EXPIRED
		if (coupon.expired_at ? TimeHelper.moment(coupon.expired_at).diff(TimeHelper.moment(), 'm', true) < 0 : false) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Coupon already expired.')
		}

		if (coupon.published_at ? TimeHelper.moment(coupon.published_at).diff(TimeHelper.moment(), 'm', true) > 0 : false) {
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Coupon is not published yet.')
		}

		// CHECK IF COUPON IS OWNED BY CERTAIN USER OR NOT
		if (coupon.valid_for.indexOf(COUPON_VALIDITY.USER) > -1) {
			await this.CouponRepository.getValidityForUser(user_id, coupon.id, transaction).then(isValid => {
				if (isValid) {
					return true
				} else {
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'You are not eligible to use this coupon')
				}
			})
		}

		// CHECK COUPON USAGE
		if (coupon.usage_limit !== -1) {
			const couponUsage = await this.CouponRepository.getUsage(coupon.id, true, transaction)

			if (couponUsage >= coupon.usage_limit) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'Coupon usage limit exceeded')
			}
		}

		// CHECK COUPON USER USAGE
		if (coupon.usage_limit_per_user !== -1) {
			const couponUserUsage = await this.CouponRepository.getUserUsage(user_id, coupon.id, true, transaction)

			if (couponUserUsage >= coupon.usage_limit_per_user) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'You have reach this coupon usage limit')
			}
		}

		// CHECK COUPON FIRST PURCHASE
		if (coupon.first_purchase_only) {
			const userPurchaseCount = await UserManager.order.getPaidCount(user_id, transaction)

			if (userPurchaseCount > 0) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, 'This coupon can only be used for first purchase')
			}
		}

		// CHECK COUPON MINIMUM SPEND
		if (coupon.minimum_spend > 0) {
			if (coupon.minimum_spend > totalAmount) {
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Promo code is applicable with a minimum purchase of ${FormatHelper.currency(coupon.minimum_spend, 'IDR')}`)
			}
		}

		// CHECK IF PRODUCT COUPON HAS IT OWN DISCOUNT
		const productCoupon = await this.CouponRepository.getCouponProduct(coupon.id, transaction).then(products => products[0]).catch(() => null)
		if (!!productCoupon?.amount && !coupon.valid_for_all) {
			coupon.amount = productCoupon.amount
		}

		return coupon
	}

	// ------------------------------------------------------------------
	// NOTE: DISCOUNT COUNTER
	// coupon is ---> coupon object retrieved from DB
	// orders is array of {
	// 			amount (total of item final price)
	// 			shipping (fee from courier service)
	// 			insurance (fee from courier service)
	// 			products ---> see below
	// 		}
	// products is array of {
	// 			type: order product type: 'matchbox', 'outfit', 'variant'
	// 			detail: product detail object retrieved from DB
	// 		}
	// ------------------------------------------------------------------

	// private getItemDiscount(
	// 	coupon: CouponInterface,
	// 	orderDetails: OrderDetailInterface[],
	// ) {
	// 	let couponUsageCount = 0
	// 	const couponUsageModifier = {
	// 		get() {
	// 			return couponUsageCount
	// 		},
	// 		add() {
	// 			couponUsageCount++
	// 		},
	// 	}

	// 	return orderDetails.map(({
	// 		price,
	// 		// orderDetail
	// 	}) => {
	// 		// NOTE:
	// 		// Price is the finalprice

	// 		if (
	// 			(coupon.usage_limit_item !== -1 ? couponUsageModifier.get() < coupon.usage_limit_item : true)
	// 		) {
	// 			couponUsageModifier.add()
	// 			return Math.min(coupon.amount, price)
	// 		} else {
	// 			return 0
	// 		}
	// 	}).reduce(CommonHelper.sum, 0)
	// }

	// private getAmountDiscount(
	// 	coupon: CouponInterface,
	// 	totalFilteredAmount: number,
	// ) {
	// 	switch (coupon.type) {
	// 		case COUPONS.PERCENT:
	// 			return Math.min(totalFilteredAmount, Math.floor(totalFilteredAmount * coupon.amount / 100))
	// 		case COUPONS.TOTAL:
	// 			return Math.min(totalFilteredAmount, coupon.amount)
	// 		default:
	// 			return 0
	// 	}
	// }

	private makeid(prefix: string = '', length: number = 4, sum: string = ''): string {
		if (sum.length < length) {
			return this.makeid(prefix, length, sum + POSSIBLE_CHARS.charAt(Math.floor(Math.random() * POSSIBLE_CHARS_LENGTH)))
		}

		return prefix + sum
	}

}

export default new PromoCouponManager()
