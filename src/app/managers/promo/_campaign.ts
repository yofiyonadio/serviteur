import {
	ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import VoucherManager from '../voucher'
// import ShipmentManager from '../shipment'
import PacketManager from '../packet'
import ServiceManager from '../service'
import UserManager from '../user'
import PromoManager from '../promo'
// import OrderManager from '../order'

import {
	CampaignRepository,
} from 'app/repositories'

import {
	CampaignInterface,
	OrderDetailInterface,
	OrderRequestInterface,
	ShipmentInterface,
	BrandAddressInterface,
	OrderAddressInterface,
	OrderDetailCouponInterface,
	OrderDetailCampaignInterface,
	ShipmentCampaignInterface,
	ShipmentCouponInterface,
	PacketInterface,
	UserInterface,
} from 'energie/app/interfaces'
import { OrderRequestVariantMetadataInterface } from 'energie/app/interfaces/order.request'

import { TimeHelper, CommonHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
import { ERROR_CODES } from 'app/models/error'
import { ORDER_DETAILS, ORDER_REQUESTS, STYLEBOARD_STATUSES, STATEMENTS, STATEMENT_SOURCES, ORDERS } from 'energie/utils/constants/enum'

import { Parameter } from 'types/common'
import { PaymentRequestInterface } from 'app/interfaces/payment'
import { isEmpty } from 'lodash'


class PromoCampaignManager extends ManagerModel {

	static __displayName = 'PromoCampaignManager'

	protected CampaignRepository: CampaignRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CampaignRepository,
		], async () => {
			// Initialization function goes here
			if (DEFAULTS.SYNC) {
				return this.transaction(async transaction => {
					await this.CampaignRepository.insert([
						{
							id: 1,
							title: 'Free Shipping',
							slug: 'freeshipping',
							is_refundable: true,
							description: 'Get free shipping for up to IDR 20.000 for every purchase of a matchbox',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 2,
							title: 'Voucher - Free Shipping',
							slug: 'voucher-freeshipping',
							is_refundable: true,
							description: 'Free shipping for vouchers tagged as \'free-shipping\'',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 3,
							title: 'Buy One Get One Mini',
							slug: 'bogo-mini',
							is_refundable: false,
							description: 'Get one mini matchbox for every purchase of a matchbox',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 4,
							title: '10% Off',
							slug: 'styleboard-2-item',
							is_refundable: false,
							description: 'Get 10% discount by keeping 2 items from your custom session selection',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 5,
							title: '15% Off',
							slug: 'styleboard-3-item',
							is_refundable: false,
							description: 'Get 15% discount by keeping 3 items from your custom session selection',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 6,
							title: '20% Off',
							slug: 'styleboard-4-item',
							is_refundable: false,
							description: 'Get 20% discount by keeping 4 or more items from your custom session selection',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 7,
							title: 'Styling Fee Credit',
							slug: 'styleboard-styling-fee',
							is_refundable: true,
							description: 'Get styling fee credited back to you by buying at least one product.',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment().format() as any,
						}, {
							id: 8,
							title: 'Buy Active Get Home',
							slug: 'active-home-bogo',
							is_refundable: false,
							description: 'Get one home matchbox for every purchase of active matchbox',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 9,
							title: 'Payday One Day Basic Get Mini',
							slug: 'payday-1-day-basic-mini',
							is_refundable: false,
							description: '',
							terms: '',
							published_at: TimeHelper.moment('2020-08-26 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2020-08-26 23:59:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 10,
							title: 'Bundle Discount',
							slug: 'bundle-discount',
							is_refundable: false,
							description: '',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 11,
							title: 'First Register Get 50k',
							slug: 'get-50k',
							is_refundable: false,
							description: '',
							terms: '',
							published_at: TimeHelper.moment().format() as any,
							created_at: TimeHelper.moment().format() as any,
						}, {
							id: 12,
							title: '25% off for first user',
							slug: 'singlesday',
							is_refundable: false,
							description: '',
							terms: `- Penawaran tidak bisa digabungkan dengan penawaran lain
						- Kode promo SINGLESDAY berlaku untuk mendapatkan potongan 35% OFF untuk semua tipe Instant Matchbox dalam periode promo yang ditentukan
						- Diskon tambahan 25% OFF untuk semua tipe Instant Matchbox hanya berlaku untuk first user
						- Untuk menerapkan kode promo, kamu harus memasukkannya di halaman checkout
						- Promo hanya berlaku pada tanggal 11th Nov 2020 at 00:00 AM sampai 12th Nov 2020 at 11:59 PM`,
							created_at: TimeHelper.moment().format() as any,
							published_at: TimeHelper.moment('2020-11-11 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							expired_at: TimeHelper.moment('2020-11-12 23:59:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 13,
							title: 'SEASONGIVING',
							slug: 'season-giving',
							is_refundable: false,
							description: '',
							terms: ``,
							published_at: TimeHelper.moment('2020-12-19 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-02-28 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						},
						{
							id: 14,
							title: 'Diskon 50k',
							slug: 'myfirst',
							is_refundable: false,
							description: '',
							terms: ``,
							created_at: TimeHelper.moment().format() as any,
							published_at: TimeHelper.moment('2020-11-20 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							expired_at: TimeHelper.moment('2020-12-31 23:59:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						},
						{
							id: 15,
							title: 'OCBC 25% Off',
							slug: 'ocbc-25',
							is_refundable: false,
							description: 'Program Regular ini berlaku sejak tanggal 16 November 2020 sampai dengan 30 April 2021',
							terms: `Syarat dan ketentuan :
						- Promo berlaku menggunakan Kartu Debit & Kartu Kredit OCBC NISP
						- Minimum transkasi Rp 350.000,-
						- Maksimum diskon Rp 200.000,-
						- Berlaku setiap hari (termasuk hari libur nasional)
						- Promo berlaku untuk 1 kartu /1 nama / 1x transaksi perhari
						- Promo tidak berlaku kelipatan atau split bill
						- Tidak dapat digabungan dengan promo lainnya`,
							published_at: TimeHelper.moment('2020-11-16 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-04-30 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 16,
							title: 'STYLISTLOVE',
							slug: 'stylist-love',
							is_refundable: false,
							description: `30% Disc All Items for personal styling service`,
							terms: '',
							published_at: TimeHelper.moment('2021-01-18 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-02-10 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 17,
							title: 'Spark Your Own Joy 30% For new users',
							slug: 'newuser',
							is_refundable: false,
							description: `Spark Your Own Joy 30% OFF untuk semua items di layanan personal styling Yuna & Co untuk user baru.`,
							terms: `
						S&K :
						* Penawaran tidak bisa digabungkan dengan penawaran lain
						* Promo potongan 30% untuk semua produk di Yuna & Co. kecuali gift cards berlaku untuk order pertama dalam periode promo yang ditentukan
						* Maksimal Free Ongkir IDR 20,000
						* Minimal transaksi IDR 100,000
						* Maksimal Diskon IDR 300,000
						* Kode Promo berlaku sampai tanggal 31 Maret 2021
						`,
							published_at: TimeHelper.moment('2021-02-09 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-04-26 15:14:45', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 18,
							title: 'Ramadan Campaign 2021 Yuna & Co. ',
							slug: 'ramadanjoy',
							is_refundable: false,
							description: 'Get 30% OFF for All Gift Cards on Ramadan Season',
							terms: `
						T&C :
						- Penawaran tidak bisa digabungkan dengan penawaran lain
						- Kode promo RAMADANJOY berlaku untuk mendapatkan potongan 30% untuk semua tipe Gift Cards di Yuna & Co. dalam periode promo yang ditentukan
						- Untuk menerapkan kode promo, kamu harus memasukkannya di halaman Shopping Bag
						- Periode promo berlaku hingga 30 April 2021 pukul 23:59 WIB
						`,
							published_at: TimeHelper.moment('2021-04-16 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-04-30 23:59:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 19,
							title: '30% OFF for First Purchase',
							slug: 'firstpurchase30',
							as_cashback: true,
							is_refundable: false,
							description: ' Diskon 30% ( potongan hingga Rp. 100.000) pembelian pertama di Yuna & Co. ',
							published_at: TimeHelper.moment('2021-07-01 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-07-29 03:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 20,
							title: '7% Off',
							slug: '77-1-item',
							is_refundable: false,
							description: 'Checkout with 1 item Enjoy additional +7% Discount per item',
							terms: '',
							published_at: TimeHelper.moment('2021-07-07 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-07-08 23:59:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 21,
							title: '14% Off',
							slug: '77-2-item',
							is_refundable: false,
							description: 'Checkout with 2 items Enjoy additional +14% Discount per item',
							terms: '',
							published_at: TimeHelper.moment('2021-07-07 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-07-08 23:59:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 22,
							title: '21% Off',
							slug: '77-3-item',
							is_refundable: false,
							description: 'Checkout with 3 items or more Enjoy additional +21% Discount per item',
							terms: '',
							published_at: TimeHelper.moment('2021-07-07 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-07-08 23:59:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 23,
							title: 'Loyalty Program Launch',
							slug: 'loyaltyprogram',
							as_cashback: true,
							is_refundable: false,
							description: 'Register 100.000 Yuna Points + purchase checkout cashback 10% up to 200.000 Points',
							published_at: TimeHelper.moment('2021-08-20 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2030-08-31 03:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 24,
							title: 'Loyalty Program',
							slug: 'loyaltyprogramV2',
							as_cashback: true,
							is_refundable: false,
							description: 'Purchase checkout cashback 2% up to 20.000 Points',
							published_at: TimeHelper.moment('2021-08-20 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2030-09-30 03:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 25,
							title: 'Campaign First Purchase New Member',
							slug: 'campaign-first-purchase-new-member',
							as_cashback: true,
							is_refundable: false,
							description: 'Purchase checkout cashback 2% up to 20.000 Points',
							published_at: TimeHelper.moment('2021-08-20 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2030-09-30 03:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}, {
							id: 26,
							title: 'Campaign FYP',
							slug: 'campaign-fyp',
							is_refundable: false,
							description: 'Campaign FYP',
							published_at: TimeHelper.moment('2021-08-20 00:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2030-09-30 03:00:00', 'YYYY-MM-DD hh:mm:ss').format() as any,
						},{
							id: 27,
							title: 'Holiday Set',
							slug: 'holiday-set',
							is_refundable: false,
							description: 'Checkout with 2 items or more pay only Rp. 199000',
							terms: '',
							published_at: TimeHelper.moment('2021-12-08 20:00:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
							created_at: TimeHelper.moment().format() as any,
							expired_at: TimeHelper.moment('2021-12-12 23:59:59', 'YYYY-MM-DD hh:mm:ss').format() as any,
						}
					], transaction)
				})
			}
		})
	}


	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getCampaignFromOrderDetailInput(
		order_detail_id: number,
		transaction: EntityManager,
	) {
		return this.CampaignRepository.getCampaignFromOrderDetailInput(order_detail_id, transaction)
	}

	@ManagerModel.bound
	async get(
		campaign_id: number,
		transaction: EntityManager,
	) {
		return this.CampaignRepository.getBy('id', campaign_id, transaction)
	}

	async filter(
		transaction: EntityManager,
	) {
		return this.CampaignRepository.filter(transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async applyCampaigns(
		user: UserInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		payment: PaymentRequestInterface | undefined,
		transaction: EntityManager,
	) {
		return this.CampaignRepository.getAll(transaction).catch(err => {
			if (err instanceof ErrorModel && err.is(ERROR_CODES.ERR_101)) {
				return [] as CampaignInterface[]
			}

			throw err
		}).then(campaigns => {
			return campaigns.reduce(async (p, campaign) => {
				return p.then(async sum => {
					const res = await this.applyCampaign(user, campaign, order_details, shipments, {
						orderDetails: [...applied_promos.orderDetails, ...sum.orderDetailCampaigns],
						shipments: [...applied_promos.shipments, ...sum.shipmentCampaigns],
					}, payment, transaction)

					return {
						orderDetailCampaigns: [...sum.orderDetailCampaigns, ...res.orderDetails],
						shipmentCampaigns: [...sum.shipmentCampaigns, ...res.shipments],
					}

				})
			}, Promise.resolve({
				orderDetailCampaigns: [] as OrderDetailCampaignInterface[],
				shipmentCampaigns: [] as ShipmentCampaignInterface[],
			})).then(result => {
				const appliedCampaignIds = result.orderDetailCampaigns.map(odC => odC.campaign_id).concat(result.shipmentCampaigns.map(sC => sC.campaign_id))


				return {
					...result,
					campaigns: campaigns.filter(c => {
						return appliedCampaignIds.indexOf(c.id) > -1
					}),
				}
			})
		})
	}

	async firstRegisterCampaign(transaction: EntityManager) {
		const wallets = await UserManager.wallet.getWalletAndPromoTransaction(transaction)

		await wallets.reduce(async (promise, wallet) => {
			return promise.then(async () => {
				if (wallet.transactions.length === 1 &&
					wallet.transactions[0].type === STATEMENTS.CREDIT &&
					wallet.transactions[0].source === STATEMENT_SOURCES.CAMPAIGN &&
					TimeHelper.moment(TimeHelper.moment(wallet.transactions[0].created_at).add(7, 'day')).diff(TimeHelper.moment(), 'm', true) < 0
				) {
					await UserManager.wallet.withdrawFrom(wallet.id, 50000, STATEMENT_SOURCES.CAMPAIGN, 11, 'Expired Wallet Campaign', transaction)
				}
			})
		}, Promise.resolve())
	}

	// ============================ PRIVATES ============================
	private applyCampaign = async (
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		payment: PaymentRequestInterface | undefined,
		transaction: EntityManager,
	) => {
		switch (campaign.slug) {

			// ======================================
			// APPLY CAMPAIGN MANUALLY
			// NOTE: COUPON CAN BE NULL
			// ======================================

			case 'freeshipping':
				return this.freeshipping(campaign, order_details, shipments, applied_promos, transaction)
			case 'voucher-freeshipping':
				return this.voucherFreeshipping(campaign, order_details, shipments, applied_promos, transaction)
			case 'styleboard-2-item':
			case 'styleboard-3-item':
			case 'styleboard-4-item':
				return this.styleboardBulkBuy(campaign, order_details, applied_promos, transaction)
			case 'holiday-set':
				return this.packageDeal(campaign, order_details, applied_promos, transaction)
			case 'bogo-mini':
				return this.bogo(campaign, order_details, shipments, applied_promos, transaction)
			case 'active-home-bogo':
				return this.activebogo(campaign, order_details, shipments, applied_promos, transaction)
			case 'payday-1-day-basic-mini':
				return this.paydayOneDayBasicMini(campaign, order_details, shipments, applied_promos, transaction)
			case 'bundle-discount':
				return this.bundleDiscount(campaign, order_details, applied_promos, transaction)
			case 'season-giving':
				return this.seasongiving(user, campaign, order_details, applied_promos, transaction)
			case 'ocbc-25':
				return this.ocbc(user, campaign, order_details, applied_promos, payment, transaction)
			case 'singlesday':
				return this.singlesday(user, campaign, order_details, applied_promos, payment, transaction)
			case 'myfirst':
				return this.myfirst(user, campaign, order_details, applied_promos, transaction)
			case 'stylist-love':
				return this.stylistLove(campaign, order_details, applied_promos, transaction)
			case 'newuser':
				return this.newuser(user, campaign, order_details, applied_promos, transaction)
			case 'ramadanjoy':
				return this.ramadanJoy(campaign, order_details, applied_promos, transaction)
			case 'firstpurchase30':
				return this.firstPurchase30k(user, campaign, order_details, applied_promos, transaction)
			case 'loyaltyprogram':
				return this.loyaltyprogram(user, campaign, order_details, applied_promos, transaction)
			case 'loyaltyprogramV2':
				return this.loyaltyprogramV2(user, campaign, order_details, applied_promos, transaction)
			case 'campaign-first-purchase-new-member':
				return this.campaignFirstPurchaseNewMember(user, campaign, order_details, applied_promos, transaction)
			case 'campaign-fyp':
				return this.campaignFyp(user, campaign, order_details, applied_promos, transaction)
			case 'package-deal-x-item':
				return this.packageDeal(campaign, order_details, applied_promos, transaction)
			case '77-1-item':
			case '77-2-item':
			case '77-3-item':
				return this.sevenBulkBuy(campaign, order_details, applied_promos, transaction)
			default:
				return {
					orderDetails: [],
					shipments: [],
					combinable: true,
				}
		}
	}

	private async freeshipping(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Free shipping up to IDR 20.000
		// ==============================================
		// check from order detail, not order request
		// - for all matchbox only
		// - max value is shipping value
		// - must take into account applied promo

		return {
			orderDetails: [],
			shipments: await shipments.reduce(async (p, shipment) => {
				return p.then(async sum => {
					const shipmentPacket = shipment.packet
					const discount = await shipment.orderDetails.filter(d => {
						return order_details.map(oD => oD.id).indexOf(d.id) > -1
					}).reduce(async (_p, orderDetail) => {
						// ================================================
						// If shipment contain multiple order detail, then
						// the free shipping discount will be counted
						// proportional on the weight percentage.
						// ================================================
						switch (orderDetail.type) {
							case ORDER_DETAILS.STYLESHEET:
							case ORDER_DETAILS.INVENTORY:
								const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
								return _p.then(data => {
									return {
										count: data.count + 1,
										weight: data.weight + packet.weight,
									}
								})
							default:
								return _p
						}
					}, Promise.resolve({
						count: 0,
						weight: 0,
					})).then(data => {
						const maxDiscount = this.getMaxApplicableDiscount(shipment.amount, applied_promos.shipments.filter(s => s.shipment_id === shipment.id))
						const discountByPacketWeight = Math.floor(shipment.amount * (data.weight / shipmentPacket.weight))

						return Math.min(Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), maxDiscount), data.count * 20000)
					})

					let finalDiscount = sum.reduce((i, d) => i + d.discount, 0)

					switch (true) {
						case ((finalDiscount === 0) && (finalDiscount + discount) <= 20000):
							finalDiscount = (finalDiscount + discount)
							break
						case ((20000 - finalDiscount) >= 0):
							finalDiscount = (20000 - finalDiscount)
							break
						case (20000 - finalDiscount < 0):
							finalDiscount = 0
							break
					}

					if (finalDiscount > 0) {
						return [...sum, {
							shipment_id: shipment.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount: finalDiscount,
							cashback: 0,
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as ShipmentCampaignInterface[])),
			combinable: true,
		}
	}

	private async voucherFreeshipping(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Free shipping cost for vouchers that are
		// marked as free_shipping
		// ==============================================
		// - max value is shipping value
		// - must take into account applied promo

		return {
			orderDetails: [],
			shipments: await shipments.reduce(async (p, shipment) => {
				return p.then(async sum => {
					const shipmentPacket = shipment.packet
					const discount = await shipment.orderDetails.filter(d => {
						return order_details.map(oD => oD.id).indexOf(d.id) > -1
					}).map(od => {
						return order_details.find(d => d.id === od.id)
					}).reduce(async (_p, orderDetail) => {
						// ================================================
						// If shipment contain multiple order detail, then
						// the free shipping discount will be counted
						// proportional on the weight percentage.
						// ================================================
						if (orderDetail.request.type === ORDER_REQUESTS.VOUCHER) {
							const voucher = await VoucherManager.get(orderDetail.request.ref_id, false, transaction)

							if (voucher.free_shipping) {
								const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
								return _p.then(weight => weight + packet.weight)
							}

							return _p
						} else {
							return _p
						}
					}, Promise.resolve(0)).then(weight => {
						const maxDiscount = this.getMaxApplicableDiscount(shipment.amount, applied_promos.shipments.filter(s => s.shipment_id === shipment.id))
						const discountByPacketWeight = Math.floor(shipment.amount * (weight / shipmentPacket.weight))
						return Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), maxDiscount)
					})

					if (discount > 0) {
						return [...sum, {
							shipment_id: shipment.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount,
							cashback: 0,
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as ShipmentCampaignInterface[])),
			combinable: true,
		}
	}

	private async styleboardBulkBuy(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Discount for purchase of 2,3,4++ item from
		// Styleboard request type
		// ==============================================
		// check from order detail, not order request
		// - for session-result stylesheet only
		// must take into account applied promo

		return {
			orderDetails: await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					// max discount is order detail price
					// subtracted by campaigns already applied (pre-campaign?)
					let discount = 0
					const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

					if (orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT && metadata.styleboard_id) {
						const styleboard = await ServiceManager.styleboard.get(metadata.styleboard_id, { with_stylecard: true, with_stylecard_variant: true, deep: true }, transaction)

						const getDiscountedItems = styleboard.stylecards.reduce((init, stylecard) => init.concat(...stylecard.stylecardVariants.reduce((i, sv) => i.concat(sv), [])), []).filter(f => f.inventories.length > 0 || f.variant.have_consignment_stock)

						const count = order_details.filter(oD => {
							const requestRefId = typeof oD.request.ref_id === 'string' ? parseInt(oD.request.ref_id, 10) : oD.request.ref_id

							return orderDetail.type === ORDER_DETAILS.INVENTORY
								&& orderDetail.request.type === ORDER_REQUESTS.VARIANT
								&& (oD.request.metadata as OrderRequestVariantMetadataInterface).styleboard_id === metadata.styleboard_id
								&& getDiscountedItems.length > 0
								&& getDiscountedItems.findIndex(i => i.variant_id === requestRefId) > -1
						}).length

						switch (styleboard.status) {
							case STYLEBOARD_STATUSES.REDEEMED:
							case STYLEBOARD_STATUSES.RESOLVED:
							case STYLEBOARD_STATUSES.EXCEPTION:
								discount = 0
								break
							default:
								if (count === 2 && campaign.slug === 'styleboard-2-item') {
									discount = Math.ceil(orderDetail.price * .1 / 100) * 100
								} else if (count === 3 && campaign.slug === 'styleboard-3-item') {
									discount = Math.ceil(orderDetail.price * .15 / 100) * 100
								} else if (count >= 4 && campaign.slug === 'styleboard-4-item') {
									discount = Math.ceil(orderDetail.price * .2 / 100) * 100
								} else {
									discount = 0
								}
								break
						}
					}

					if (discount > 0) {
						return [...sum, {
							order_detail_id: orderDetail.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount: 0,
							cashback: Math.min(discount, maxDiscount),
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[])),
			shipments: [],
			combinable: true,
		}
	}

	private async sevenBulkBuy(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Discount for purchase of 1,2,3++ item from shop page

		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]
		if (!coupon_id) {
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail) => {
					return p.then(async sum => {

						let discount = 0
						const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
						const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

						const count = order_details.filter(oD => {
							return orderDetail.type === ORDER_DETAILS.INVENTORY
								&& orderDetail.request.type === ORDER_REQUESTS.VARIANT
								&& (oD.request.metadata as OrderRequestVariantMetadataInterface).source === 'shop'
						}).length

						if ((metadata as OrderRequestVariantMetadataInterface).source === 'shop') {
							if (count === 1 && campaign.slug === '77-1-item') {
								discount = Math.ceil(orderDetail.price * .07 / 100) * 100
							} else if (count === 2 && campaign.slug === '77-2-item') {
								discount = Math.ceil(orderDetail.price * .14 / 100) * 100
							} else if (count >= 3 && campaign.slug === '77-3-item') {
								discount = Math.ceil(orderDetail.price * .21 / 100) * 100
							} else {
								discount = 0
							}
						}

						if (discount > 0) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: Math.min(discount, maxDiscount),
								cashback: 0,
							}]
						} else {
							return sum
						}
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async packageDeal(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Discount for purchase of 1,2,3++ item from shop page

		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]
		if (!coupon_id) {
			let leftToBePaid = 0
			let fullDiscount = false
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail, i) => {
					return p.then(async sum => {

						let discount = 0
						// const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
						// const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

						const count = order_details.filter(oD => {
							return orderDetail.type === ORDER_DETAILS.INVENTORY
								&& orderDetail.request.type === ORDER_REQUESTS.VARIANT
								&& (oD.request.metadata as OrderRequestVariantMetadataInterface).source === 'holiday-set'
								&& this.isBundle(2, (oD.request.metadata as any).created_at, order_details)
						}).length

						if ((orderDetail.request.metadata as OrderRequestVariantMetadataInterface).source === 'holiday-set'
							&& this.isBundle(2, (orderDetail.request.metadata as any).created_at, order_details)
							&& campaign.slug === 'holiday-set'
						) {
							if (fullDiscount) {
								discount = orderDetail.price
							} else {
								leftToBePaid = leftToBePaid === 0 ? (199000 * count / 2) : leftToBePaid
								// this.log('left', orderDetail.id, leftToBePaid)
									if (orderDetail.price <= leftToBePaid) {
										discount = 0
										leftToBePaid = leftToBePaid - orderDetail.price
									} else {
										discount = orderDetail.price - leftToBePaid
										fullDiscount = true
									}
							}
						} else {
							discount = 0
						}

						// if ((metadata as OrderRequestVariantMetadataInterface).source === 'shop') {
						// 	if (count % 2 === 0 && campaign.slug !== 'holiday-set') {
						// 		if (fullDiscount) {
						// 			discount = orderDetail.price
						// 		} else {
						// 			leftToBePaid = leftToBePaid === 0 ? (300000 * count / 2) : leftToBePaid
						// 			this.log('left', orderDetail.id, leftToBePaid)
						// 			// if (leftToBePaid > 0 && orderDetail.price >= leftToBePaid) {
						// 			// 	discount = orderDetail.price - leftToBePaid
						// 			// 	fullDiscount = true
						// 			// } else {
						// 				if (orderDetail.price <= leftToBePaid) {
						// 					discount = 0
						// 					leftToBePaid = leftToBePaid - orderDetail.price
						// 				} else {
						// 					discount = orderDetail.price - leftToBePaid
						// 					fullDiscount = true
						// 				}
						// 			// }
						// 		}
						// 		this.log('disc', orderDetail.id, discount)
						// 	} 
							
						// 	else {
						// 		discount = 0
						// 	}
						// }
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						// } else {
						// 	return sum
						// }
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async ramadanJoy(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]

		if (!coupon_id) {
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail) => {
					return p.then(async sum => {
						let discount: number = 0

						if (orderDetail.request.as_voucher) {
							discount = Math.ceil(orderDetail.price * 0.3)

							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						}
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async bundleDiscount(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		return {
			orderDetails: await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					let discount = 0
					const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

					if (orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT && metadata.stylecard_id) {
						const bundle_items = await ServiceManager.stylecard.getBundleDiscount(metadata.stylecard_id, transaction)
						const bundle_items_count = bundle_items.variants.reduce((i, v) => i.find(_i => _i === v.product_id) ? i : i.concat(v.product_id), []).length
						const request_bundle_item_count = order_details.filter(f => (f.request.metadata as any).stylecard_id === metadata.stylecard_id).length
						const variant_id = typeof orderDetail.request.ref_id === 'string' ? parseInt(orderDetail.request.ref_id, 10) : orderDetail.request.ref_id

						if ((bundle_items_count === request_bundle_item_count) && (bundle_items.variants.findIndex(v => v.id === variant_id) !== -1)) {
							discount = bundle_items.variants.find(v => v.id === variant_id).discount
						}
					}

					return [...sum, {
						order_detail_id: orderDetail.id,
						campaign_id: campaign.id,
						is_refundable: campaign.is_refundable,
						discount: Math.min(discount, maxDiscount),
						cashback: 0,
					}]
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))
				.then(campaigns => {
					// check if there is 0 discount
					const check = campaigns.findIndex(c => c.campaign_id === 9 && c.discount === 0) > -1 ? true : false

					if (check) {
						return campaigns.filter(f => f.campaign_id !== 9)
					} else {
						return campaigns
					}
				}),
			shipments: [],
			combinable: true,
		}
	}

	private async stylistLove(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])

		return {
			orderDetails: await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					// max discount is order detail price
					// subtracted by campaigns already applied (pre-campaign?)
					let discount = 0
					const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					const metadata = orderDetail.request.metadata as OrderRequestVariantMetadataInterface

					if (orderDetail.type === ORDER_DETAILS.INVENTORY && orderDetail.request.type === ORDER_REQUESTS.VARIANT && metadata.styleboard_id) {
						const styleboard = await ServiceManager.styleboard.get(metadata.styleboard_id, { with_stylecard: true, with_stylecard_variant: true, deep: true }, transaction)

						// const getDiscountedItems = styleboard.stylecards.reduce((init, stylecard) => init.concat(...stylecard.stylecardVariants.reduce((i, sv) => i.concat(sv), [])), []).filter(f => f.inventories.length > 0)

						const count = order_details.filter(oD => {
							// const requestRefId = typeof oD.request.ref_id === 'string' ? parseInt(oD.request.ref_id, 10) : oD.request.ref_id

							return orderDetail.type === ORDER_DETAILS.INVENTORY
								&& orderDetail.request.type === ORDER_REQUESTS.VARIANT
								&& (oD.request.metadata as OrderRequestVariantMetadataInterface).styleboard_id === metadata.styleboard_id
							// && getDiscountedItems.length > 0
							// && getDiscountedItems.findIndex(i => i.variant_id === requestRefId) > -1
						}).length

						const total = order_details.reduce((i, od) => i + od.price, 0)

						switch (styleboard.status) {
							case STYLEBOARD_STATUSES.REDEEMED:
							case STYLEBOARD_STATUSES.EXCEPTION:
								discount = 0
								break
							default:
								if (
									count > 0 &&
									campaign.slug === 'stylist-love' &&
									total > 100000
								) {
									discount = Math.ceil(orderDetail.price * .3 / 100) * 100
								}
								break
						}
					}

					if (isEmpty(have_coupon)) {
						if (discount > 0) {
							if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > 300000) {
								return [...sum, {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									is_refundable: campaign.is_refundable,
									discount: 300000 - sum.reduce((i, d) => i + d.discount, 0),
									cashback: 0,
								}]
							} else {
								return [...sum, {
									order_detail_id: orderDetail.id,
									campaign_id: campaign.id,
									is_refundable: campaign.is_refundable,
									discount: Math.min(discount, maxDiscount),
									cashback: 0,
								}]
							}
						} else {
							return sum
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[])),
			shipments: [],
			combinable: false,
		}
	}

	private async ocbc(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		payment: PaymentRequestInterface | undefined,
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// T&C
		// - Promo berlaku menggunakan Kartu Debit & Kartu Kredit OCBC NISP
		// - Minimum transkasi Rp 350.000,-
		// - Maksimum diskon Rp 200.000,-
		// - Berlaku setiap hari (termasuk hari libur nasional)
		// - Promo berlaku untuk 1 kartu /1 nama / 1x transaksi perhari
		// - Promo tidak berlaku kelipatan atau split bill
		// - Tidak dapat digabungan dengan promo lainnya,

		const ocbc_bin_numbers: string[] = [
			'421561',
			'464583',
			'524169',
			'464585',
			'486399',
			'464584',
			'603400',
			'537940',
		]

		const currentCard = payment?.masked_card ? [payment.masked_card.slice(0, 6), payment.masked_card.slice(12)] : ['0', '1']
		const total = order_details.reduce((i, od) => i + od.price, 0)
		const orderHistory = await this.CampaignRepository.getOrderHistory(campaign.id, user.id, transaction)
		const cardHistory = !!orderHistory ? (orderHistory.payment.metadata as any).masked_card.slice(12) : '2'
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const transactionHistory = cardHistory === currentCard[1] ? TimeHelper.moment(orderHistory.created_at).diff(TimeHelper.moment(), 'day') < 0 : true

		if (
			ocbc_bin_numbers.findIndex(ocbc_bin_number => ocbc_bin_number === currentCard[0]) > -1 &&
			total >= 350000 &&
			transactionHistory &&
			isEmpty(have_coupon)
		) {
			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(200000, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (orderDetail.request.type === ORDER_REQUESTS.MATCHBOX) {
						discount = Math.ceil(orderDetail.price * .25 / 100) * 100
					} else if (orderDetail.request.type === ORDER_REQUESTS.VARIANT) {
						const stylecard_id = (orderDetail.request.metadata as any).stylecard_id
						const isPrestyle = await ServiceManager.stylecard.get({ stylecard_id_or_ids: stylecard_id, many: false }, {}, transaction)
							.then(stylecard => (stylecard.inventory_only && stylecard.is_bundle) ? true : false)

						if (isPrestyle) {
							discount = Math.ceil(orderDetail.price * .25 / 100) * 100
						}
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > 200000) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 200000 - sum.reduce((i, d) => i + d.discount, 0),
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: Math.min(discount, maxDiscount),
								cashback: 0,
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async newuser(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		/** T&C
		 * Penawaran tidak bisa digabungkan dengan penawaran lain
		 * Promo potongan 30% untuk semua produk di Yuna & Co.kecuali gift cards
		 * berlaku untuk order pertama dalam periode promo yang ditentukan
		 * Maksimal Free Ongkir IDR 20,000
		 * Minimal transaksi IDR 100,000
		 * Maksimal Diskon IDR 300,000
		 */

		const total = order_details.reduce((i, od) => i + od.price, 0)
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (
				o.status === ORDERS.EXPIRED ||
				o.status === ORDERS.EXCEPTION
			) {
				return false
			} else {
				return true
			}
		})).catch(() => [])
		const max_discount = 300000
		const minimum_trx = 100000

		if (
			total >= minimum_trx &&
			isEmpty(have_coupon) &&
			isEmpty(orderHistories)
		) {
			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = Math.ceil(orderDetail.price * .30 / 100) * 100
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: max_discount - sum.reduce((i, d) => i + d.discount, 0),
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: Math.min(discount, maxDiscount),
								cashback: 0,
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async myfirst(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]
		const total = order_details.reduce((i, od) => i + od.price, 0)

		// don't use this transaction
		const orderHistory = await UserManager.order.getAll(user.id, {}, undefined).catch(() => [])

		if (
			!coupon_id &&
			total >= 300000 &&
			orderHistory.length === 0 &&
			TimeHelper.moment(user.created_at).isBetween(campaign.published_at, campaign.expired_at)
		) {
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail) => {
					return p.then(async sum => {
						// const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
						const discount = 50000

						if ((discount + sum.reduce((i, s) => i + s.discount, 0)) > 50000) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						}
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: false,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async loyaltyprogram(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const max_discount = 200000
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (o.status === ORDERS.EXPIRED || o.status === ORDERS.EXCEPTION) return false
			return true
		})).catch(() => [])

		if (isEmpty(have_coupon && isEmpty(orderHistories))) {
			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = Math.ceil(orderDetail.price * .02 / 100) * 100
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: max_discount - sum.reduce((i, d) => i + d.discount, 0)
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: Math.min(discount, maxDiscount),
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async loyaltyprogramV2(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const max_discount = 20000
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (o.status === ORDERS.EXPIRED || o.status === ORDERS.EXCEPTION) return false
			return true
		})).catch(() => [])

		if (isEmpty(have_coupon) && isEmpty(orderHistories)) {
			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = Math.ceil(orderDetail.price * .02 / 100) * 100
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: max_discount - sum.reduce((i, d) => i + d.discount, 0)
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: Math.min(discount, maxDiscount),
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async campaignFirstPurchaseNewMember(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const max_discount = 20000
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (o.status === ORDERS.EXPIRED || o.status === ORDERS.EXCEPTION) return false
			return true
		})).catch(() => [])
		const priceTotal = order_details.reduce((first, item) => {
			return first + item.price
		}, 0)

		if (
			isEmpty(have_coupon)
			&& isEmpty(orderHistories)
			&& (user.created_at >= campaign.published_at)
			&& (priceTotal >= 200000)
		) {

			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = (Math.ceil(orderDetail.price * .05 / 100) * 100)
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: max_discount - sum.reduce((i, d) => i + d.discount, 0)
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: 0,
								cashback: Math.min(discount, maxDiscount),
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {

			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async campaignFyp(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const max_discount = 30000
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (o.status === ORDERS.EXPIRED || o.status === ORDERS.EXCEPTION) return false
			return true
		})).catch(() => [])

		const countFyp = order_details.reduce((first, order) => {
			if ((order.request.metadata as any).source === 'fyp') return first + 1
			return first + 0
		}, 0)

		if (
			isEmpty(have_coupon)
			&& isEmpty(orderHistories)
			&& countFyp >= 2
		) {

			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = (Math.ceil(orderDetail.price * .30  / 100) * 100)
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: max_discount - sum.reduce((i, d) => i + d.discount, 0),
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: Math.min(discount, maxDiscount),
								cashback: 0,
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {

			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async firstPurchase30k(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const have_coupon = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])
		const orderHistories = await UserManager.order.getAll(user.id, {}, undefined).then(orders => orders.filter(o => {
			if (
				o.status === ORDERS.EXPIRED ||
				o.status === ORDERS.EXCEPTION
			) {
				return false
			} else {
				return true
			}
		})).catch(() => [])
		const max_discount = 100000

		if (
			isEmpty(have_coupon) &&
			isEmpty(orderHistories)
		) {
			const orderDetails = await order_details.reduce(async (p, orderDetail) => {
				return p.then(async sum => {
					const maxDiscount = this.getMaxApplicableDiscount(max_discount, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
					let discount = 0

					if (!orderDetail.request.as_voucher) {
						discount = Math.ceil(orderDetail.price * .30 / 100) * 100
					}

					if (discount > 0) {
						if ((discount + sum.reduce((i, d) => i + d.discount, 0)) > max_discount) {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: max_discount - sum.reduce((i, d) => i + d.discount, 0),
								cashback: 0,
								// cashback: max_discount - sum.reduce((i, d) => i + d.discount, 0),
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount: Math.min(discount, maxDiscount),
								cashback: 0,
								// cashback: Math.min(discount, maxDiscount),
							}]
						}
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as OrderDetailCampaignInterface[]))

			return {
				orderDetails,
				shipments: [],
				combinable: orderDetails.reduce((i, od) => i + od.discount, 0) > 0 ? false : true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async seasongiving(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]

		if (!coupon_id) {
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail) => {
					return p.then(async sum => {
						let discount: number = 0

						if (orderDetail.request.as_voucher) {
							discount = Math.ceil(orderDetail.price * 0.3)

							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						} else {
							return [...sum, {
								order_detail_id: orderDetail.id,
								campaign_id: campaign.id,
								is_refundable: campaign.is_refundable,
								discount,
								cashback: 0,
							}]
						}
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: true,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async singlesday(
		user: UserInterface,
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		payment: PaymentRequestInterface | undefined,
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// T&C
		// - Penawaran tidak bisa digabungkan dengan penawaran lain
		// - Kode promo SINGLESDAY berlaku untuk mendapatkan potongan 35% OFF untuk semua tipe Instant
		// 		Matchbox dalam periode promo yang ditentukan
		// - Diskon tambahan 25% OFF untuk semua tipe Instant Matchbox hanya berlaku untuk first user
		// - Untuk menerapkan kode promo, kamu harus memasukkannya di halaman checkout
		// - Promo hanya berlaku pada tanggal 11th Nov 2020 at 00:00 AM sampai 12th Nov 2020 at 11:59 PM

		const coupon_id = applied_promos.orderDetails.reduce((i, od) => (od as any).coupon_id ? i.concat((od as any).coupon_id) : i, [])[0]
		const coupon = await PromoManager.coupon.get(coupon_id, transaction).catch(() => null)

		if (
			!!coupon &&
			coupon.title === 'SINGLESDAY' &&
			TimeHelper.moment(user.created_at).isBetween('2020-11-11T00:00:00', '2020-11-12T23:59:00')
		) {
			return {
				orderDetails: await order_details.reduce(async (p, orderDetail) => {
					return p.then(async sum => {
						const couponDiscount = Math.ceil(orderDetail.price * .35 / 100) * 100
						const maxDiscount = this.getMaxApplicableDiscount(orderDetail.price, applied_promos.orderDetails.filter(od => od.order_detail_id === orderDetail.id))
						const discount = Math.ceil(couponDiscount * .25 / 100) * 100

						return [...sum, {
							order_detail_id: orderDetail.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount: Math.min(discount, maxDiscount),
							cashback: 0,
						}]
					})
				}, Promise.resolve([] as OrderDetailCampaignInterface[])),
				shipments: [],
				combinable: false,
			}
		} else {
			return {
				orderDetails: [],
				shipments: [],
				combinable: true,
			}
		}
	}

	private async bogo(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Apply free shipping on BOGO item
		// ==============================================
		// - filter only order detail that has
		//   bogo campaign applied to it

		return {
			orderDetails: [],
			shipments: await shipments.reduce(async (p, shipment) => {
				return p.then(async sum => {
					const shipmentPacket = shipment.packet
					const discount = await shipment.orderDetails.filter(d => {
						const orderDetail = order_details.find(detail => detail.id === d.id)
						const campaigns = applied_promos.orderDetails.filter(odC => odC.order_detail_id === d.id)

						return orderDetail && campaigns.map((c: OrderDetailCampaignInterface) => c.campaign_id).indexOf(campaign.id) > -1
					}).map(od => {
						return order_details.find(d => d.id === od.id)
					}).reduce(async (_p, orderDetail) => {
						// ================================================
						// If shipment contain multiple order detail, then
						// the free shipping discount will be counted
						// proportional on the weight percentage.
						// ================================================
						const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
						return _p.then(weight => weight + packet.weight)
					}, Promise.resolve(0)).then(weight => {
						const maxDiscount = this.getMaxApplicableDiscount(shipment.amount, applied_promos.shipments.filter(s => s.shipment_id === shipment.id))
						const discountByPacketWeight = Math.floor(shipment.amount * (weight / shipmentPacket.weight))
						return Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), maxDiscount)
					})

					if (discount > 0) {
						return [...sum, {
							shipment_id: shipment.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount,
							cashback: 0,
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as ShipmentCampaignInterface[])),
			combinable: true,
		}
	}

	private async activebogo(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Apply free shipping on BOGO item
		// ==============================================
		// - filter only order detail that has
		//   active bogo campaign applied to it

		return {
			orderDetails: [],
			shipments: await shipments.reduce(async (p, shipment) => {
				return p.then(async sum => {
					const shipmentPacket = shipment.packet
					const discount = await shipment.orderDetails.filter(d => {
						const orderDetail = order_details.find(detail => detail.id === d.id)
						const campaigns = applied_promos.orderDetails.filter(odC => odC.order_detail_id === d.id)

						return orderDetail && campaigns.map((c: OrderDetailCampaignInterface) => c.campaign_id).indexOf(campaign.id) > -1
					}).map(od => {
						return order_details.find(d => d.id === od.id)
					}).reduce(async (_p, orderDetail) => {
						// ================================================
						// If shipment contain multiple order detail, then
						// the free shipping discount will be counted
						// proportional on the weight percentage.
						// ================================================
						const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
						return _p.then(weight => weight + packet.weight)
					}, Promise.resolve(0)).then(weight => {
						const maxDiscount = this.getMaxApplicableDiscount(shipment.amount, applied_promos.shipments.filter(s => s.shipment_id === shipment.id))
						const discountByPacketWeight = Math.floor(shipment.amount * (weight / shipmentPacket.weight))
						return Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), maxDiscount)
					})

					if (discount > 0) {
						return [...sum, {
							shipment_id: shipment.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount,
							cashback: 0,
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as ShipmentCampaignInterface[])),
			combinable: true,
		}
	}

	private async paydayOneDayBasicMini(
		campaign: CampaignInterface,
		order_details: Array<OrderDetailInterface & {
			request: OrderRequestInterface,
		}>,
		shipments: Array<ShipmentInterface & {
			origin: BrandAddressInterface,
			packet: Parameter<PacketInterface>,
			destination: OrderAddressInterface,
			orderDetails: OrderDetailInterface[],
		}>,
		applied_promos: {
			orderDetails: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface>,
			shipments: Array<ShipmentCampaignInterface | ShipmentCouponInterface>,
		},
		transaction: EntityManager,
	): Promise<{
		orderDetails: OrderDetailCampaignInterface[],
		shipments: ShipmentCampaignInterface[],
		combinable: boolean,
	}> {
		// ==============================================
		// DESCRIPTION:
		// Apply free shipping on BOGO item
		// ==============================================
		// - filter only order detail that has
		//   active bogo campaign applied to it

		return {
			orderDetails: [],
			shipments: await shipments.reduce(async (p, shipment) => {
				return p.then(async sum => {
					const shipmentPacket = shipment.packet
					const discount = await shipment.orderDetails.filter(d => {
						const orderDetail = order_details.find(detail => detail.id === d.id)
						const campaigns = applied_promos.orderDetails.filter(odC => odC.order_detail_id === d.id)

						return orderDetail && campaigns.map((c: OrderDetailCampaignInterface) => c.campaign_id).indexOf(campaign.id) > -1
					}).map(od => {
						return order_details.find(d => d.id === od.id)
					}).reduce(async (_p, orderDetail) => {
						// ================================================
						// If shipment contain multiple order detail, then
						// the free shipping discount will be counted
						// proportional on the weight percentage.
						// ================================================
						const packet = await PacketManager.getPacket(orderDetail.packet_id, transaction)
						return _p.then(weight => weight + packet.weight)
					}, Promise.resolve(0)).then(weight => {
						const maxDiscount = this.getMaxApplicableDiscount(shipment.amount, applied_promos.shipments.filter(s => s.shipment_id === shipment.id))
						const discountByPacketWeight = Math.floor(shipment.amount * (weight / shipmentPacket.weight))
						return Math.min((isNaN(discountByPacketWeight) ? 0 : discountByPacketWeight), maxDiscount)
					})

					if (discount > 0) {
						return [...sum, {
							shipment_id: shipment.id,
							campaign_id: campaign.id,
							is_refundable: campaign.is_refundable,
							discount,
							cashback: 0,
						}]
					} else {
						return sum
					}
				})
			}, Promise.resolve([] as ShipmentCampaignInterface[])),
			combinable: true,
		}
	}

	private getMaxApplicableDiscount(
		amount: number,
		applied_promos: Array<OrderDetailCampaignInterface | OrderDetailCouponInterface | ShipmentCampaignInterface | ShipmentCouponInterface>,
	) {
		return Math.max(0, amount - applied_promos.map(p => p.discount).reduce(CommonHelper.sum, 0))
	}

	private isBundle(
		itemCount: number,
		timeId: any,
		orderDetails: any
	) {
		const filtered = orderDetails.filter(od => od.request.metadata.created_at === timeId)
		if (filtered.length === itemCount) {
			return true
		} else {
			return false
		}
	}
}

export default new PromoCampaignManager()
