import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	CampaignRepository,
} from 'app/repositories'

import OrderManager from '../order'
import UserManager from '../user'
import FeedbackManager from '../feedback'

import { FEEDBACKS, ORDER_DETAILS } from 'energie/utils/constants/enum'
import { FeedbackAnswerInterface, FeedbackInterface } from 'energie/app/interfaces'
import { STATEMENT_SOURCES } from 'energie/utils/constants/enum'
import { TimeHelper } from 'utils/helpers'

class PromoCashbackManager extends ManagerModel {

	static __displayName = 'PromoCashbackManager'

	protected CampaignRepository: CampaignRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CampaignRepository,
		])
	}


	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	async normalHandler(
		user_id: number,
		order_id: number,
		lastFeedbacks: Array<FeedbackInterface & {answers: FeedbackAnswerInterface[]}>,
		currentFeedback: {
			type: FEEDBACKS,
			ref_id: number,
			answers: Array<{
				question_id: number,
				answer: object,
			}>,
		},
		transaction: EntityManager,
	) {
		// ==============================================================
		// Cashback Handler
		// 	=> if all the answer were "keep", deposit point to user
		// 	=> else hold until next transaction is completed
		// ==============================================================

		const order = await OrderManager.getDetail([order_id], user_id, false, true, false, transaction).then(o => o[0])
		const lastFeedbackProgress = lastFeedbacks.reduce((init, feedback) => {
			if (feedback.answers.find(f => f.question_id === 69).answer.TAB === 'Keep') {
				init.keepedItems = init.keepedItems + 1
			}

			return {
				...init,
				done: lastFeedbacks.length,
				total: order.orderDetails.length,
			}
		}, {
			done: 0,
			keepedItems: 0,
			total: order.orderDetails.length || 1,
		})

		const currentFeedbackProgress = {
			done: lastFeedbackProgress.done + 1,
			total: lastFeedbackProgress.total,
			keepedItems: lastFeedbackProgress.keepedItems + (((currentFeedback.answers.find(answer => answer.question_id === 69).answer as any).TAB === 'Keep') ? 1 : 0),
		}

		if (
			currentFeedbackProgress.done === currentFeedbackProgress.total &&
			currentFeedbackProgress.keepedItems === currentFeedbackProgress.total
		) {
			const cashbackFromCampaign = order.orderDetails.reduce((sum, od) => sum + od.orderDetailCampaigns.reduce((s, c) => s + c.cashback, 0), 0)
			const cashbackFromCoupon = order.orderDetails.reduce((sum, od) => sum + od.orderDetailCoupons.reduce((s, c) => s + c.cashback, 0), 0)
			const totalCashbackPoint = cashbackFromCampaign + cashbackFromCoupon

			if (totalCashbackPoint > 0) {
				await UserManager.point.depositPoint(
					user_id,
					totalCashbackPoint,
					STATEMENT_SOURCES.ORDER,
					order_id,
					'Cashback Point from order',
					transaction,
				)
			}
		}
	}

	async cronHandler(transaction: EntityManager) {
		return FeedbackManager.getRaw(undefined, {
			detailed: false,
			with_feedback: false,
		}, {
			empty_feedback: true,
			order_detail_with_cashback: true,
			confirmed_date: TimeHelper.moment().subtract(7, 'd').toDate(),
		}, transaction).then(orders => {
			return Promise.all(orders.map(async order => {
				const pointTransactions = await UserManager.point.getDetailTransaction(order.user_id, transaction).then(user => user.point.transactions)
				const cashbackPoint = order.details.reduce((sum, detail) => sum + detail.orderDetailCampaigns.reduce((s, c) => s + c.cashback, 0) + detail.orderDetailCoupons.reduce((s, c) => s + c.cashback, 0), 0)

				if (pointTransactions.findIndex(pt => pt.source === STATEMENT_SOURCES.ORDER && pt.ref_id === order.id) === -1) {
					await UserManager.point.depositPoint(
						order.user_id,
						cashbackPoint,
						STATEMENT_SOURCES.ORDER,
						order.id,
						'Cashback Point from order',
						transaction,
					)
				}
			}))
		}).catch(() => [])
	}

	async disputeHandler(
		user_id: number,
		order_id: number,
		transaction: EntityManager,
	) {
		const feedbacks = await FeedbackManager.getShallow(user_id, order_id, transaction)

		const cashbackTransaction = await feedbacks.reduce(async (promise, feedback) => {
			return promise.then(async init => {
				const orderDetail = await OrderManager.detail.get([feedback.order_detail_id], {with_discount: true, with_detailed_item: true}, transaction).then(od => od[0])
				const isKeepedItem = feedback.answers.findIndex(answer => answer.answer.TAB === 'Keep') > -1
				let cashbackPoint = 0

				if (!!init[orderDetail.order_id]) {
					if (
						isKeepedItem &&
						(orderDetail.coupons.findIndex(c => c.as_cashback) > -1 || orderDetail.campaigns.findIndex(c => c.as_cashback) > -1)
					) {
						switch (orderDetail.type) {
						case ORDER_DETAILS.INVENTORY:
							cashbackPoint = orderDetail.orderDetailCampaigns.reduce((s, c) => s + c.cashback, 0) + orderDetail.orderDetailCoupons.reduce((s, c) => s + c.cashback, 0)

							return {
								...init,
								cashback: init[orderDetail.order_id].cashback + cashbackPoint,
								// below is basically useless for inventory type,
								// but just keep it there for consistency
								totalKeepedItemPrice: init[orderDetail.order_id].totalKeepedItemPrice + orderDetail.variants[0].price,
								totalItemPrice: init[orderDetail.order_id].totalKeepedItemPrice + orderDetail.variants[0].price,
								price: init[orderDetail.order_id].price + orderDetail.price,
							}
						case ORDER_DETAILS.STYLESHEET:
							const item = orderDetail.variants.find(v => v.id === feedback.ref_id && feedback.type === FEEDBACKS.VARIANT)
							const totalKeepedItemPrice = init[orderDetail.order_id].totalKeepedItemPrice + item.inventory.price
							const totalItemPrice = orderDetail.variants.reduce((sum, variant) => sum + variant.inventory.price, 0)
							const price = orderDetail.price

							cashbackPoint = 0 +
								// coupons
								orderDetail.coupons.reduce((sum, coupon) => {
									const cashback = orderDetail.orderDetailCoupons.find(odc => odc.coupon_id === coupon.id).cashback * (totalKeepedItemPrice / totalItemPrice)

									return sum + cashback
								}, 0) +
								// campaigns
								orderDetail.campaigns.reduce((sum, campaign) => {
									const cashback = orderDetail.orderDetailCampaigns.find(odc => odc.campaign_id === campaign.id).cashback * (totalKeepedItemPrice / totalItemPrice)

									return sum + cashback
								}, 0)


							return {
								...init,
								cashback: init[orderDetail.order_id].cashback + cashbackPoint,
								totalKeepedItemPrice,
								totalItemPrice,
								price,
							}
						default:
							return init
						}
					} else {
						return init
					}
				} else {
					return {
						...init,
						[orderDetail.order_id]: {
							cashback: 0,
							totalKeepedItemPrice: 0,
							totalItemPrice: 0,
							price: 0,
						},
					}
				}
			})
		}, Promise.resolve({} as {
			[order_id: number]: {
				cashback: number,
				totalKeepedItemPrice: number,
				totalItemPrice: number,
				price: number,
			},
		}))

		return Promise.all(Object.keys(cashbackTransaction).map(async key => {
			const id = parseInt(key, 10)

			if (cashbackTransaction[id].cashback > 0) {
				return UserManager.point.depositPoint(
					user_id,
					cashbackTransaction[key].cashback,
					STATEMENT_SOURCES.ORDER,
					id,
					'Cashback Point from order',
					transaction,
				)
			}
		}))
	}

	// ============================ PRIVATES ============================
}

export default new PromoCashbackManager()
