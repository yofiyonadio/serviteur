import {
	ManagerModel,
} from 'app/models'

import {
   Connection,
   EntityManager,
} from 'typeorm'

import CollectionRepository from 'app/repositories/collection'

import {
   CollectionInterface,
} from 'energie/app/interfaces'

import DEFAULTS from 'utils/constants/default'


class CollectionManager extends ManagerModel {

	static __displayName = 'CollectionManager'

	protected CollectionRepository: CollectionRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CollectionRepository,
		], () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.make(transaction)
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getByIdOrIds(
		ids: number[] | number,
		transaction: EntityManager,
	) {
		if (Array.isArray(ids)) {
			return this.CollectionRepository.getByIds(ids, transaction)
		} else {
			return this.CollectionRepository.getByIds([ids], transaction)[0]
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async make(transaction: EntityManager) {
		if(DEFAULTS.SYNC) {

			const collections = [
				{
					id: 1,
					title: 'Comfort',
					slug: 'comfort',
				}, {
					id: 2,
					title: 'Active',
					slug: 'active',
				}, {
					id: 3,
					title: 'WFH',
					slug: 'wfh',
				}, {
					id: 4,
					title: 'Halyu Essential',
					slug: 'halyu-essential',
				},
			] as CollectionInterface[]

			await this.CollectionRepository.upsertCollection(collections, transaction)

			return this
		}
	}

}

export default new CollectionManager()
