import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import MeasurementRepository from 'app/repositories/measurement'

import { MeasurementInterface } from 'energie/app/interfaces'

import DEFAULTS from 'utils/constants/default'
// import { ERROR_CODES } from 'app/models/error'


class MeasurementManager extends ManagerModel {

	static __displayName = 'MeasurementManager'

	protected MeasurementRepository: MeasurementRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			MeasurementRepository,
		], () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.make(transaction)
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async make(transaction: EntityManager) {
		if(DEFAULTS.SYNC) {
			await this.MeasurementRepository.insert(1, null, 'Top Measurement', '', true, transaction)
			await this.MeasurementRepository.insert(2, null, 'Shoe Measurement', '', true, transaction)
			await this.MeasurementRepository.insert(3, null, 'Dimension', '', true, transaction)
			await this.MeasurementRepository.insert(4, null, 'Other Measurement', '', false, transaction)
			await this.MeasurementRepository.insert(21, null, 'Belt', '', false, transaction)
			await this.MeasurementRepository.insert(24, null, 'Bottom Measurement', '', false, transaction)
			await this.MeasurementRepository.insert(25, null, 'Dress/One-Piece Measurement', '', false, transaction)

			const measurements = [
				{
					id: 5,
					measurement_id: 1,
					title: 'Bust',
				},
				{
					id: 6,
					measurement_id: 1,
					title: 'Waist',
				},
				// {
				// 	id: 7,
				// 	measurement_id: 1,
				// 	title: 'Bodice Length',
				// },
				{
					id: 8,
					measurement_id: 1,
					title: 'Length',
				},
				{
					id: 9,
					measurement_id: 2,
					title: 'Insole Length',
				},
				{
					id: 10,
					measurement_id: 3,
					title: 'Width',
				},
				{
					id: 11,
					measurement_id: 3,
					title: 'Depth',
				},
				{
					id: 12,
					measurement_id: 3,
					title: 'Height',
				},
				{
					id: 13,
					measurement_id: 4,
					title: 'Sleeve Length',
				},
				{
					id: 14,
					measurement_id: 4,
					title: 'Shoulder Width',
				},
				{
					id: 15,
					measurement_id: 4,
					title: 'Front Length',
				},
				{
					id: 16,
					measurement_id: 4,
					title: 'Back Length',
				},
				{
					id: 17,
					measurement_id: 4,
					title: 'Head Circumference Ø',
				},
				{
					id: 18,
					measurement_id: 4,
					title: 'Sleeve Length',
				},
				{
					id: 19,
					measurement_id: 4,
					title: 'Sleeve Opening Ø',
				},
				{
					id: 20,
					measurement_id: 4,
					title: 'Armhole Ø',
				},
				{
					id: 22,
					measurement_id: 21,
					title: 'Waist',
				},
				{
					id: 23,
					measurement_id: 21,
					title: 'Hips',
				},
				{
					id: 26,
					measurement_id: 3,
					title: 'Depth',
				},
				{
					id: 27,
					measurement_id: 2,
					title: 'Insole Width',
				},
				{
					id: 28,
					measurement_id: 2,
					title: 'Heel Height',
				},
				{
					id: 29,
					measurement_id: 2,
					title: 'Platform Height',
				},
				// {
				// 	id: 30,
				// 	measurement_id: 4,
				// 	title: 'Head Circumference Ø',
				// },
				// {
				// 	id: 31,
				// 	measurement_id: 1,
				// 	title: 'Bodice Length',
				// },
				{
					id: 32,
					measurement_id: 24,
					title: 'Bottom Waist',
				},
				{
					id: 33,
					measurement_id: 24,
					title: 'Hips Ø',
				},
				{
					id: 34,
					measurement_id: 24,
					title: 'Front Rise',
				},
				{
					id: 35,
					measurement_id: 24,
					title: 'Outseam',
				},
				{
					id: 36,
					measurement_id: 24,
					title: 'Inseam',
				},
				{
					id: 37,
					measurement_id: 24,
					title: 'Hem',
				},
				{
					id: 38,
					measurement_id: 25,
					title: 'Bust',
				},
				{
					id: 39,
					measurement_id: 25,
					title: 'Waist',
				},
				{
					id: 40,
					measurement_id: 25,
					title: 'Hips',
				},
				// {
				// 	id: 41,
				// 	measurement_id: 25,
				// 	title: 'Bodice Length',
				// },
				{
					id: 42,
					measurement_id: 25,
					title: 'Length',
				},
				{
					id: 43,
					measurement_id: 25,
					title: 'Front Rise',
				},
				{
					id: 44,
					measurement_id: 25,
					title: 'Outseam',
				},
				{
					id: 45,
					measurement_id: 25,
					title: 'Inseam',
				},
				{
					id: 46,
					measurement_id: 25,
					title: 'Hem',
				},
				{
					id: 47,
					measurement_id: 1,
					title: 'Hips',
				},
			] as MeasurementInterface[]

			measurements && measurements.map(async measurement => {
				await this.MeasurementRepository.insert(
					measurement.id,
					measurement.measurement_id,
					measurement.title,
					null,
					false,
					transaction)
			})
			return this
		}
	}
}

export default new MeasurementManager()
