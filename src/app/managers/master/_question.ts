import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import QuestionRepository from 'app/repositories/question'

import { QuestionInterface, UserAnswerInterface, FeedbackAnswerInterface } from 'energie/app/interfaces'
import { QuestionScaleMetadataInterface, QuestionChoiceMetadataInterface, QuestionCommonMetadataInterface, QuestionTabMetadataInterface, QuestionRequirementInterface, QuestionDropdownMetadataInterface, QuestionTextMetadataInterface, QuestionFileMetadataInterface, QuestionDateMetadataInterface } from 'energie/app/interfaces/question'

 import { CommonHelper, TimeHelper } from 'utils/helpers'

 import DEFAULTS from 'utils/constants/default'
 import { QUESTIONS, REQUIREMENT_OPERATORS } from 'energie/utils/constants/enum'
// import { ERROR_CODES } from 'app/models/error'


class QuestionManager extends ManagerModel {

	static __displayName = 'QuestionManager'

	protected QuestionRepository: QuestionRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			QuestionRepository,
		], async () => {
			// Initialization function goes here
			await this.make()

			DEFAULTS.setExchangeQuestionId(69)
			DEFAULTS.setExchangeNoteQuestionId(70)
			DEFAULTS.setExchangeCommentQuestionId(73)
			DEFAULTS.setVariantFeedbackQuestionId(74)

			DEFAULTS.setStyleProfileQuestionGroupId([1, 2, 3, 4])
			DEFAULTS.setVariantQuestionGroupId(5)
			DEFAULTS.setStylesheetQuestionGroupId(6)
		})
	}


	// tslint:disable-next-line: member-ordering
	choices = {
		size: {
			alpha: [{ title: 'XXS' }, { title: 'XS' }, { title: 'S' }, { title: 'M' }, { title: 'L' }, { title: 'XL' }, { title: 'XXL' }],
			eu: [{ title: '34' }, { title: '36' }, { title: '38' }, { title: '40' }, { title: '42' }, { title: '44' }, { title: '46' }],
			uk: [{ title: '4' }, { title: '6' }, { title: '8' }, { title: '10' }, { title: '12' }, { title: '14' }, { title: '16' }],
			us: [{ title: '0' }, { title: '2' }, { title: '4' }, { title: '6' }, { title: '8' }, { title: '10' }, { title: '12' }, { title: '14' }, { title: '16' }, { title: '18' }, { title: '20' }, { title: '22' }],
			jeans: [{ title: '24' }, { title: '25' }, { title: '26' }, { title: '27' }, { title: '28' }, { title: '29' }, { title: '30' }, { title: '31' }, { title: '32' }, { title: '33' }, { title: '34' }, { title: '35' }, { title: '36' }],
			bra: {
				band: [{ title: '30' }, { title: '32' }, { title: '34' }, { title: '36' }, { title: '38' }, { title: '40' }, { title: '42' }, { title: '44' }, { title: '46' }],
				cup: [{ title: 'A' }, { title: 'B' }, { title: 'C' }, { title: 'D' }, { title: 'DD' }, { title: 'E/DDD' }],
			},
			shoes: {
				eu: [{ title: '35.5' }, { title: '36' }, { title: '36.5' }, { title: '37' }, { title: '37.5' }, { title: '38' }, { title: '38.5' }, { title: '39' }, { title: '39.5' }, { title: '40' }, { title: '40.5' }, { title: '41' }, { title: '41.5' }, { title: '42' }, { title: '42.5' }, { title: '43' }],
				uk: [{ title: '3' }, { title: '3.5' }, { title: '4' }, { title: '4.5' }, { title: '5' }, { title: '5.5' }, { title: '6' }, { title: '6.5' }, { title: '7' }, { title: '7.5' }, { title: '8' }, { title: '8.5' }, { title: '9' }, { title: '9.5' }, { title: '10' }, { title: '10.5' }],
				us: [{ title: '4.5' }, { title: '5' }, { title: '5.5' }, { title: '6' }, { title: '6.5' }, { title: '7' }, { title: '7.5' }, { title: '8' }, { title: '8.5' }, { title: '9' }, { title: '9.5' }, { title: '10' }, { title: '10.5' }, { title: '11' }, { title: '11.5' }, { title: '12' }, { title: '12.5' }],
			},
		},
		price: [{ title: '≤ IDR 200.000' }, { title: '≤ IDR 300.000' }, { title: '≤ IDR 400.000' }, { title: '≤ IDR 500.000' }, { title: '≤ IDR 600.000' }, { title: '≤ IDR 700.000' }, { title: '≤ IDR 800.000' }, { title: '≤ IDR 900.000' }, { title: '≤ IDR 1.000.000' }, { title: '≤ IDR 1.100.000' }, { title: '≤ IDR 1.200.000' }, { title: '≤ IDR 1.300.000' }, { title: '≤ IDR 1.400.000' }, { title: '≤ IDR 1.500.000' }, { title: '≤ IDR 1.600.000' }, { title: '≤ IDR 1.700.000' }, { title: '≤ IDR 1.800.000' }, { title: '≤ IDR 1.900.000' }, { title: '≤ IDR 2.0000.000' }, { title: '> IDR 2.000.000' }],
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	get getQuestionPerGroup() {
		return this.QuestionRepository.getQuestionsPerGroup
	}

	@ManagerModel.bound
	async getAll(transaction: EntityManager) {
		return this.QuestionRepository.getQuestions(undefined, transaction)
	}

	@ManagerModel.bound
	async getAllStyleProfileQuestions(transaction: EntityManager) {
		return Promise.all(DEFAULTS.STYLE_PROFILE_QUESTION_GROUP_ID.map(id => {
			return this.getByGroupId(id, transaction)
		})).then(ids => {
			return ids.reduce(CommonHelper.sumArray, [])
		})
	}

	@ManagerModel.bound
	async getAllGroup(transaction: EntityManager) {
		return this.QuestionRepository.getGroup(undefined, transaction) as any
	}

	@ManagerModel.bound
	async getByGroupId(
		id: number | number[],
		transaction: EntityManager,
	) {
		return this.QuestionRepository.getQuestionsByGroupId(id, transaction)
	}

	@ManagerModel.bound
	async getByIds(
		ids: number[],
		transaction: EntityManager,
	) {
		return this.QuestionRepository.getQuestions(ids, transaction)
	}

	@ManagerModel.bound
	async getIdsByGroupId(
		groupId: number,
		transaction: EntityManager,
	) {
		return this.QuestionRepository
			.getQuestionsByGroupId(groupId, transaction)
			.then((questions: Array<QuestionInterface & {
				questions: QuestionInterface[],
			}>) => {
				return questions.map(q => [q.id, ...q.questions.map(qq => qq.id)]).reduce(CommonHelper.sumArray, [])
			})
	}

	@ManagerModel.bound
	async getQuestionType(
		question_id: number,
		transaction: EntityManager,
	) {
		return this.QuestionRepository.getQuestionType(question_id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	parseAnswer(
		answer: UserAnswerInterface | FeedbackAnswerInterface,
		flattened_questions: QuestionInterface[],
	) {
		const question = flattened_questions.find(q => q.id === answer.question_id)
		const parent = question.question_id ? flattened_questions.find(q => q.id === question.question_id) : null

		if(parent) {
			return {
				title: `${ parent.title } – ${ question.title }`,
				// image: question.image || parent.image || (question.metadata as any).image  || (parent.metadata as any).image,
				// description: question.description || parent.description,
				type: question.type,
				answer: this.getAnswer(answer, question),
			}
		} else {
			return {
				title: question.title,
				// image: question.image || (question.metadata as any).image,
				// description: question.description,
				type: question.type,
				answer: this.getAnswer(answer, question),
			}
		}
	}

	// ============================ PRIVATES ============================

	private async make() {
		return this.withTransaction(undefined, async transaction => {
			if(DEFAULTS.SYNC) {
				await this.QuestionRepository.insert(
					1,
					'Body',
					'Body related questions',
					[
						{
							id: 1,
							question_group_id: 1,
							question_id: null,
							title: 'What is your body shape?',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								choices: [{
									title: 'Pear',
									image: '//question/v2/body-shape-pear.jpg',
								}, {
									title: 'Hourglass',
									image: '//question/v2/body-shape-hourglass.jpg',
								}, {
									title: 'Rectangle',
									image: '//question/v2/body-shape-rectangle.jpg',
								}, {
									title: 'Inverted triangle',
									image: '//question/v2/body-shape-inverted-triangle.jpg',
								}, {
									title: 'Apple',
									image: '//question/v2/body-shape-apple.jpg',
								}, {
									title: 'Don\'t know',
									image: '//question/v2/body-shape-dont-know.jpg',
								}],
								modal: {
									small: true,
									caption: 'What does it means?',
									title: 'Body Shapes',
									content: [{
										title: 'Pear',
										image: '//question/v2/body-shape-pear.jpg',
										description: 'Hips wider than shoulders.',
									}, {
										title: 'Hourglass',
										image: '//question/v2/body-shape-hourglass.jpg',
										description: 'Hips same as shoulders with defined waist.',
									}, {
										title: 'Rectangle',
										image: '//question/v2/body-shape-rectangle.jpg',
										description: 'Hips same as shoulders with undefined waist.',
									}, {
										title: 'Inverted Triangle',
										image: '//question/v2/body-shape-inverted-triangle.jpg',
										description: 'Shoulders wider than hips.',
									}, {
										title: 'Apple',
										image: '//question/v2/body-shape-apple.jpg',
										description: 'Heavier around midsection.',
									}],
								},
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 2,
							question_group_id: 1,
							question_id: null,
							title: 'Your body measurements',
							description: 'If you’d like to be more accurate, please enter your body measurements.',
							type: QUESTIONS.DEFINITION,
							order: 1,
							metadata: JSON.stringify({
								grouping: true,
								modal: {
									caption: 'How to measure',
									title: 'How To Measure',
									content: [{
										description: 'When taking measurements, use a cloth tape measure, not a metal one. Make sure that, when you circle your chest, waist, or hips, the tape is level and neither too tight nor too loose. Measure yourself over tight clothes.',
									}, {
										title: 'Bust',
										description: 'Measure under your arms and around the fullest part of your chest. Note: Bust is not your bra size.',
										image: '//question/how-to-measure-bust.jpg',
									}, {
										title: 'Waist',
										description: 'Measure around your natural waistline, between your navel and rib cage.',
										image: '//question/how-to-measure-waist.jpg',
									}, {
										title: 'Hips',
										description: 'Stand with your feet together and measure around the fullest part of your hips.',
										image: '//question/how-to-measure-hips.jpg',
									}],
								},
							} as QuestionCommonMetadataInterface) as any,
						}, {
							id: 3,
							question_group_id: null,
							question_id: 2,
							title: 'BUST',
							description: 'cm',
							type: QUESTIONS.NUMBER,
							order: 0,
						}, {
							id: 4,
							question_group_id: null,
							question_id: 2,
							title: 'WAIST',
							description: 'cm',
							type: QUESTIONS.NUMBER,
							order: 1,
						}, {
							id: 5,
							question_group_id: null,
							question_id: 2,
							title: 'HIPS',
							description: 'cm',
							type: QUESTIONS.NUMBER,
							order: 2,
						}, {
							id: 6,
							question_group_id: 1,
							question_id: null,
							title: 'Tell us more about your proportions',
							description: '',
							is_required: true,
							type: QUESTIONS.DEFINITION,
							order: 2,
						}, {
							id: 7,
							question_group_id: null,
							question_id: 6,
							title: 'SHOULDERS',
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								choices: [{
									title: 'Narrow',
								}, {
									title: 'Average',
								}, {
									title: 'Broad',
								}],
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 8,
							question_group_id: null,
							question_id: 6,
							title: 'TORSO',
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								choices: [{
									title: 'Short',
								}, {
									title: 'Average',
								}, {
									title: 'Long',
								}],
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 9,
							question_group_id: null,
							question_id: 6,
							title: 'HIPS',
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								choices: [{
									title: 'Narrow',
								}, {
									title: 'Average',
								}, {
									title: 'Broad',
								}],
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 10,
							question_group_id: null,
							question_id: 6,
							title: 'LEGS',
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 3,
							metadata: JSON.stringify({
								choices: [{
									title: 'Short',
								}, {
									title: 'Average',
								}, {
									title: 'Long',
								}],
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 11,
							question_group_id: 1,
							question_id: null,
							title: 'Which part of your body is considered curvy?',
							type: QUESTIONS.CHOICE,
							is_required: false,
							order: 3,
							metadata: JSON.stringify({
								choices: [{
									title: 'Upper half',
								}, {
									title: 'Bottom half',
								}, {
									title: 'Both',
								}, {
									title: 'None',
								}],
							} as QuestionChoiceMetadataInterface) as any,
							deleted_at: TimeHelper.moment().format() as any,
						}, {
							id: 12,
							question_group_id: 1,
							question_id: null,
							title: 'What is your height?',
							description: 'cm',
							type: QUESTIONS.NUMBER,
							is_required: true,
							order: 4,
						}, {
							id: 13,
							question_group_id: 1,
							question_id: null,
							title: 'What is your weight?',
							description: 'kg',
							type: QUESTIONS.NUMBER,
							is_required: true,
							order: 5,
						}, {
							id: 14,
							question_group_id: 1,
							question_id: null,
							title: 'Which size category do you fit into?',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 6,
							metadata: JSON.stringify({
								modal: {
									caption: 'What does this mean?',
									title: 'Size Category',
									content: [{
										title: 'Petite',
										description: 'Designed to fit women of shorter height, typically 155 cm and shorter.',
									}, {
										title: 'Plus',
										description: 'Designed specifically for body that are larger than the average person.',
									}, {
										title: 'Tall',
										description: 'Designed to fit women of 169 cm height or taller.',
									}],
								},
								choices: [{
									title: 'Regular',
								}, {
									title: 'Petite',
								}, {
									title: 'Plus',
								}, {
									title: 'Tall',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 15,
							question_group_id: 1,
							question_id: null,
							title: 'What size do you typically wear?',
							type: QUESTIONS.DEFINITION,
							is_required: true,
							order: 7,
							metadata: JSON.stringify({
								modal: {
									caption: 'See size chart',
									title: 'Size Chart',
									content: [],
								},
							} as QuestionCommonMetadataInterface) as any,
						}, {
							id: 16,
							question_group_id: null,
							question_id: 15,
							title: 'DRESS',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								choices: this.choices.size.alpha,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 17,
							question_group_id: null,
							question_id: 15,
							title: 'SHIRT / BLOUSE',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								choices: this.choices.size.alpha,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 18,
							question_group_id: null,
							question_id: 15,
							title: 'SKIRT',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								choices: this.choices.size.alpha,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 19,
							question_group_id: null,
							question_id: 15,
							title: 'PANTS',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 3,
							metadata: JSON.stringify({
								choices: this.choices.size.alpha,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 20,
							question_group_id: null,
							question_id: 15,
							title: 'JEANS WAIST',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 4,
							metadata: JSON.stringify({
								choices: this.choices.size.jeans,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 21,
							question_group_id: null,
							question_id: 15,
							title: 'BRA BAND',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 5,
							metadata: JSON.stringify({
								choices: this.choices.size.bra.band,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 22,
							question_group_id: null,
							question_id: 15,
							title: 'BRA CUP',
							type: QUESTIONS.SLIDER,
							is_required: true,
							order: 6,
							metadata: JSON.stringify({
								choices: this.choices.size.bra.cup,
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 23,
							question_group_id: null,
							question_id: 15,
							title: 'SHOE SIZE',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 7,
							metadata: JSON.stringify({
								placeholder: 'Select size',
								choices: [{
									title: 'UK',
									choices: this.choices.size.shoes.uk,
								}, {
									title: 'EU',
									choices: this.choices.size.shoes.eu,
								}, {
									title: 'US',
									choices: this.choices.size.shoes.us,
								}],
							} as QuestionTabMetadataInterface) as any,
						},
					],
					transaction,
				)

				await this.QuestionRepository.insert(
					2,
					'Fit',
					// QUESTION_GROUPS.STYLE_PROFILE,
					'Fit related questions',
					[
						{
							id: 24,
							question_group_id: 2,
							question_id: null,
							title: 'How do you prefer clothes to fit you?',
							description: 'You may select more than one',
							type: QUESTIONS.DEFINITION,
							is_required: true,
							order: 0,
						}, {
							id: 25,
							question_group_id: null,
							question_id: 24,
							title: 'Top Half',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								multiple: true,
								choices: [{
									title: 'Fitted',
									// image: '',
								}, {
									title: 'Straight',
									// image: '',
								}, {
									title: 'Loose',
									// image: '',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 26,
							question_group_id: null,
							question_id: 24,
							title: 'Bottom Half',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								multiple: true,
								choices: [{
									title: 'Fitted',
									// image: '',
								}, {
									title: 'Straight',
									// image: '',
								}, {
									title: 'Loose',
									// image: '',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 27,
							question_group_id: 2,
							question_id: null,
							title: 'How do you like your jeans?',
							description: 'You may select more than one',
							type: QUESTIONS.DEFINITION,
							is_required: false,
							deleted_at: TimeHelper.moment().format() as any,
							order: 1,
						}, {
							id: 28,
							question_group_id: null,
							question_id: 27,
							title: 'STYLE',
							type: QUESTIONS.CHOICE,
							is_required: true,
							deleted_at: TimeHelper.moment().format() as any,
							order: 0,
							metadata: JSON.stringify({
								multiple: true,
								choices: [{
									title: 'Skinny',
								}, {
									title: 'Straight',
								}, {
									title: 'Bootcut',
								}, {
									title: 'Wide',
								}, {
									title: 'Cropped',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 29,
							question_group_id: null,
							question_id: 24,
							title: 'Jeans Rise',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								multiple: true,
								choices: [{
									title: 'Low',
									// image: '',
								}, {
									title: 'Mid',
									// image: '',
								}, {
									title: 'High',
									// image: '',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						},
					],
					transaction,
				)

				await this.QuestionRepository.insert(
					3,
					'Style',
					// QUESTION_GROUPS.STYLE_PROFILE,
					'Style related questions',
					[
						{
							id: 30,
							question_group_id: 3,
							question_id: null,
							title: 'What do you think of the styles below?',
							description: 'Please rate all of the styles below',
							type: QUESTIONS.DEFINITION,
							is_required: true,
							order: 0,
						}, {
							id: 31,
							question_group_id: null,
							question_id: 30,
							title: 'BOHEMIAN',
							type: QUESTIONS.TAB,
							image: '//question/style-1-bohemian.jpg',
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								info: {
									title: 'Bohemian',
									description: 'This hippie-influenced persona is the mixture of exotic and romantic styles. It requires a serious layering technique with free-moving garments. It is usually suitable for summer and perfect to wear on music festivals! Fashion icons: Rachel Zoe and Sienna Miller.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 32,
							question_group_id: null,
							question_id: 30,
							title: 'BOMBSHELL',
							image: '//question/style-2-bombshell.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								info: {
									title: 'Bombshell',
									description: 'Looking literally sexy is the aim for this Bombshell persona. Its belief is to accentuate and show every curve of the body. This princess-like style is sometimes confused with Classic persona or considered similar, but NO! This one very much focuses on sexiness. Fashion icons: The Kardashians and Angelina Jolie.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 33,
							question_group_id: null,
							question_id: 30,
							title: 'CASUAL',
							image: '//question/style-3-casual.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								info: {
									title: 'Casual',
									description: 'This persona revolves around comfort, simplicity and flexibility. Playing with colors is Casual’s favorite thing to do. Being comfortable and free to move around are the highest priorities. Fashion icons: Kristen Stewart and Lily Collins.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 34,
							question_group_id: null,
							question_id: 30,
							title: 'CHIC',
							image: '//question/style-4-chic.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 3,
							metadata: JSON.stringify({
								info: {
									title: 'Chic',
									description: 'A perfect balance between classic trendy, and simple sleek shape in a monochrome, and toned down color palette are the mantras for Chic style owner. Fashion icons: Victoria Beckham and Olivia Palermo.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 35,
							question_group_id: null,
							question_id: 30,
							title: 'CLASSIC',
							image: '//question/style-5-classic.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 4,
							metadata: JSON.stringify({
								info: {
									title: 'Classic',
									description: 'Classic personas prim-and-polished style would never go out of style. They strive for looking effortless and neat, this kind loves to spend on staple pieces which usually come in high price. Fashion icons: Tilda Swinton and Grace Kelly.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 36,
							question_group_id: null,
							question_id: 30,
							title: 'DEMURE',
							image: '//question/style-6-demure.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 5,
							metadata: JSON.stringify({
								info: {
									title: 'Demure',
									description: 'Inspired by the school-girl uniform, Demure look is a sexy look for geeks! Representing smart, tidy, slightly luxurious, and romantic at heart, this combination of girly and preppy style is easily adored by feminine stuff in the world. Fashion icons: Alexa Chung and Kate Middleton.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 37,
							question_group_id: null,
							question_id: 30,
							title: 'ECLECTIC',
							image: '//question/style-7-eclectic.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 6,
							metadata: JSON.stringify({
								info: {
									title: 'Eclectic',
									description: 'Futuristic, avant-garde cut, bold looks, and special finished fabrics are things that eclectic favors. Clashing loud patterns, blocking bright colors, and compiling statement accessories together in one look are the idea of being an Eclectic. Fashion icons: Daphne Guinness and Sussie Bubble.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 38,
							question_group_id: null,
							question_id: 30,
							title: 'FLAMBOYANT',
							image: '//question/style-8-flamboyant.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 7,
							metadata: JSON.stringify({
								info: {
									title: 'Flamboyant',
									description: 'Most of the times, Flamboyants wear something flouncy, asymmetrical, tiered pleats or ruffles. Influenced by glam rock fashion era, this glamorous style is defined by the loudness and absurdities. Some call it exaggerated, but that’s the air that drama queens breathe in. Fashion icons: Anna Dello Russo and Katy Perry.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 39,
							question_group_id: null,
							question_id: 30,
							title: 'REBEL',
							image: '//question/style-9-rebel.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 8,
							metadata: JSON.stringify({
								info: {
									title: 'Rebel',
									description: 'Inspired by gothic, punk, and rock music, Rebel is all about screaming edges and attitude. With urban or street style grassroots, this style is generally associated with young subculture. It is also most often seen in major urban centers. Fashion icons: Taylor Momsen and Rihanna.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 40,
							question_group_id: null,
							question_id: 30,
							title: 'SPORTY',
							image: '//question/style-10-sporty.jpg',
							type: QUESTIONS.TAB,
							is_required: true,
							order: 9,
							metadata: JSON.stringify({
								info: {
									title: 'Sporty',
									description: 'Generally adopted by sports lover, this style is only suitable for casual occasion or outdoor occasion. The Sporty sometimes manipulates sports item as a fashion style. Focusing on comfort, people who own this style understand their need of hi-tech fabric especially when making extreme movements. Fashion icons: Gigi Hadid and Zendaya.',
								},
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Love it',
								}],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 41,
							question_group_id: 3,
							question_id: null,
							title: 'How far out of your comfort zone are you willing to go?',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								supersize: true,
								choices: [{
									title: 'Close to my style',
								}, {
									title: 'Maybe a little bit',
								}, {
									title: 'Open to new style',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 42,
							question_group_id: 3,
							question_id: null,
							title: 'What style set are you most comfortable in?',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								supersize: true,
								choices: [{
									title: 'Mostly dresses and skirts',
								}, {
									title: 'Mostly pants and jeans',
								}, {
									title: 'Mix of both',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 43,
							question_group_id: 3,
							question_id: null,
							title: 'What colour palettes are you most comfortable in?',
							description: 'You may select more than one or select "I like all palettes"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 3,
							metadata: JSON.stringify({
								select_none_text: 'I like all palettes',
								multiple: true,
								choices: [{
									title: 'Bright',
									image: '//question/palette-1-bright.jpg',
								}, {
									title: 'Neutral',
									image: '//question/palette-2-neutral.jpg',
								}, {
									title: 'Understated',
									image: '//question/palette-3-understated.jpg',
								}, {
									title: 'Cool',
									image: '//question/palette-4-cool.jpg',
								}, {
									title: 'Warm',
									image: '//question/palette-5-warm.jpg',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 44,
							question_group_id: 3,
							question_id: null,
							title: 'Are there any colours you dislike wearing?',
							description: 'You may select more than one or select "I wear any colour"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 4,
							metadata: JSON.stringify({
								highlights: ['dislike'],
								select_none_text: 'I wear any colour',
								multiple: true,
								inverted: true,
								subsize: true,
								choices: [{
									title: 'Purple',
									image: '//question/v2/color-purple.jpg',
								}, {
									title: 'Pink',
									image: '//question/v2/color-pink.jpg',
								}, {
									title: 'Red',
									image: '//question/v2/color-red.jpg',
								}, {
									title: 'Orange',
									image: '//question/v2/color-orange.jpg',
								}, {
									title: 'Yellow',
									image: '//question/v2/color-yellow.jpg',
								}, {
									title: 'Green',
									image: '//question/v2/color-green.jpg',
								}, {
									title: 'Turquoise',
									image: '//question/v2/color-turquoise.jpg',
								}, {
									title: 'Blue',
									image: '//question/v2/color-blue.jpg',
								}, {
									title: 'Brown',
									image: '//question/v2/color-brown.jpg',
								}, {
									title: 'Beige',
									image: '//question/v2/color-beige.jpg',
								}, {
									title: 'White',
									image: '//question/v2/color-white.jpg',
								}, {
									title: 'Grey',
									image: '//question/v2/color-grey.jpg',
								}, {
									title: 'Black',
									image: '//question/v2/color-black.jpg',
								}, {
									title: 'Silver',
									image: '//question/v2/color-silver.jpg',
								}, {
									title: 'Gold',
									image: '//question/v2/color-gold.jpg',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 45,
							question_group_id: 3,
							question_id: null,
							title: 'Which clothes detail you\'d like to avoid?',
							description: 'You may select more than one or select "I like any detail"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 5,
							metadata: JSON.stringify({
								select_none_text: 'I like any detail',
								highlights: ['avoid'],
								multiple: true,
								inverted: true,
								choices: [{
									title: 'Pleat',
									image: '//question/detail-1-pleat.jpg',
								}, {
									title: 'Ruffle',
									image: '//question/detail-2-ruffle.jpg',
								}, {
									title: 'Fringe',
									image: '//question/detail-3-fringe.jpg',
								}, {
									title: 'Embroidery',
									image: '//question/detail-4-embroidery.jpg',
								}, {
									title: 'Patch',
									image: '//question/detail-5-patch.jpg',
								}, {
									title: 'Screen Print',
									image: '//question/detail-6-screen-print.jpg',
								}, {
									title: 'Lace',
									image: '//question/detail-7-lace.jpg',
								}, {
									title: 'Embellishment',
									image: '//question/detail-8-embellishment.jpg',
								}, {
									title: 'Sequin',
									image: '//question/detail-9-sequin.jpg',
								}, {
									title: 'Tassel',
									image: '//question/detail-10-tassel.jpg',
								}, {
									title: 'Transparency',
									image: '//question/detail-11-transparency.jpg',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 46,
							question_group_id: 3,
							question_id: null,
							title: 'Which prints you\'d like to avoid?',
							description: 'You may select more than one or select "I wear any print"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 6,
							metadata: JSON.stringify({
								select_none_text: 'I wear any print',
								highlights: ['avoid'],
								multiple: true,
								inverted: true,
								choices: [{
									title: 'Animal print',
									image: '//question/pattern-1-animal-print.jpg',
								}, {
									title: 'Paisley',
									image: '//question/pattern-2-paisley.jpg',
								}, {
									title: 'Floral',
									image: '//question/pattern-3-floral.jpg',
								}, {
									title: 'Plaid / Tartan',
									image: '//question/pattern-4-plaid-tartan.jpg',
								}, {
									title: 'Polka dots',
									image: '//question/pattern-5-polka-dots.jpg',
								}, {
									title: 'Stripes',
									image: '//question/pattern-6-stripes.jpg',
								}, {
									title: 'Novelty print',
									image: '//question/pattern-7-novelty-print.jpg',
								}, {
									title: 'Check',
									image: '//question/pattern-8-check.jpg',
								}, {
									title: 'Traditional print',
									image: '//question/pattern-9-traditional.jpg',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 47,
							question_group_id: 3,
							question_id: null,
							title: 'Which fabrics you\'d like to avoid?',
							description: 'You may select more than one or select "I wear any fabric"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 7,
							metadata: JSON.stringify({
								select_none_text: 'I wear any fabric',
								highlights: ['avoid'],
								multiple: true,
								inverted: true,
								subsize: true,
								choices: [{
									title: 'Faux Fur',
								}, {
									title: 'Wool',
								}, {
									title: 'Linen',
								}, {
									title: 'Polyester',
								}, {
									title: 'Faux Leather',
								}, {
									title: 'Leather',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 48,
							question_group_id: 3,
							question_id: null,
							title: 'Do you wear hijab?',
							type: QUESTIONS.BOOLEAN,
							is_required: true,
							order: 8,
						}, {
							id: 49,
							question_group_id: 3,
							question_id: null,
							title: 'Are there any body areas you\'d like to cover up?',
							description: 'You may select more than one or select "I love showing them off"',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 9,
							metadata: JSON.stringify({
								size_ratio: [102, 102],
								select_none_text: 'I love showing them off',
								multiple: true,
								subsize: true,
								choices: [{
									title: 'Shoulders',
								}, {
									title: 'Arms',
								}, {
									title: 'Cleavage',
								}, {
									title: 'Back',
								}, {
									title: 'Tummy',
								}, {
									title: 'Buttocks',
								}, {
									title: 'Legs',
								}],
							} as QuestionChoiceMetadataInterface) as any,
							requirement: JSON.stringify({
								answers: [{
									question_id: 48,
									operator: REQUIREMENT_OPERATORS.IS,
									value: { BOOLEAN: false },
								}],
							} as QuestionRequirementInterface) as any,
						}, {
							id: 50,
							question_group_id: 3,
							question_id: null,
							title: 'How often do you wear heels?',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 10,
							metadata: JSON.stringify({
								choices: [{
									title: 'Always',
								}, {
									title: 'Occasionally',
								}, {
									title: 'Never',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 51,
							question_group_id: 3,
							question_id: null,
							title: 'How high are your heels?',
							description: 'You may select more than one',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 11,
							metadata: JSON.stringify({
								multiple: true,
								subsize: true,
								choices: [{
									title: 'Low',
									description: '3 - 5 cm',
									// image: '',
								}, {
									title: 'Mid',
									description: '5 - 10 cm',
									// image: '',
								}, {
									title: 'High',
									description: '10 - 15 cm',
									// image: '',
								}],
							} as QuestionChoiceMetadataInterface) as any,
							requirement: JSON.stringify({
								answers: [{
									question_id: 50,
									operator: REQUIREMENT_OPERATORS.NOT,
									value: { CHOICE: ['Never'] },
								}],
							} as QuestionRequirementInterface) as any,
						}, {
							id: 52,
							question_group_id: 3,
							question_id: null,
							title: 'Are your ears pierced?',
							type: QUESTIONS.BOOLEAN,
							is_required: true,
							order: 12,
						}, {
							id: 53,
							question_group_id: 3,
							question_id: null,
							title: 'What are the characteristics of your jewelleries/accessories?',
							description: 'You may select more than one',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 13,
							metadata: JSON.stringify({
								multiple: true,
								choices: [{
									title: 'Classic',
								}, {
									title: 'Statement',
								}, {
									title: 'Minimalist/Simple',
								}, {
									title: 'Mix of all',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 54,
							question_group_id: 3,
							question_id: null,
							title: 'Which jewellery tones do you prefer to wear?',
							description: 'You may select more than one',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 14,
							metadata: JSON.stringify({
								multiple: true,
								subsize: true,
								choices: [{
									title: 'Silver',
									// image: '',
								}, {
									title: 'Gold',
									// image: '',
								}, {
									title: 'Rose Gold',
									// image: '',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 55,
							question_group_id: 3,
							question_id: null,
							title: 'Which accessory materials are you allergic to?',
							description: 'You may select more than one',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 15,
							metadata: JSON.stringify({
								multiple: true,
								supersize: true,
								select_none_text: 'I can wear any material',
								choices: [{
									title: 'Silver',
								}, {
									title: 'Gold',
								}, {
									title: 'Platinum',
								}, {
									title: 'Base Metal',
									description: 'Iron, nickel, copper, titanium',
								}, {
									title: 'Natural',
									description: 'Leather, bone, silk, wood',
								}, {
									title: 'Synthetic',
									description: 'Enamel, plastic, silicone',
								}, {
									title: 'Regenerated',
									description: 'Resin, rubbber',
								}],
								other_text: 'Other accessory materials',
								other_placeholder: 'Enter accessory materials you\'re allergic to. Use commas to separate multiple answers.',
							} as QuestionChoiceMetadataInterface) as any,
						},
					],
					transaction,
				)

				await this.QuestionRepository.insert(
					4,
					'You',
					// QUESTION_GROUPS.STYLE_PROFILE,
					'You related questions',
					[
						{
							id: 56,
							question_group_id: 4,
							question_id: null,
							title: 'In which year you were born?',
							description: 'This helps your Stylist to find the right clothes for you.',
							type: QUESTIONS.DROPDOWN,
							is_required: false,
							order: 0,
							metadata: JSON.stringify({
								placeholder: 'Select a year',
								choices: [{
									title: '1960',
								}, {
									title: '1961',
								}, {
									title: '1962',
								}, {
									title: '1963',
								}, {
									title: '1964',
								}, {
									title: '1965',
								}, {
									title: '1966',
								}, {
									title: '1967',
								}, {
									title: '1968',
								}, {
									title: '1969',
								}, {
									title: '1970',
								}, {
									title: '1971',
								}, {
									title: '1972',
								}, {
									title: '1973',
								}, {
									title: '1974',
								}, {
									title: '1975',
								}, {
									title: '1976',
								}, {
									title: '1977',
								}, {
									title: '1978',
								}, {
									title: '1979',
								}, {
									title: '1980',
								}, {
									title: '1981',
								}, {
									title: '1982',
								}, {
									title: '1983',
								}, {
									title: '1984',
								}, {
									title: '1985',
								}, {
									title: '1986',
								}, {
									title: '1987',
								}, {
									title: '1988',
								}, {
									title: '1989',
								}, {
									title: '1990',
								}, {
									title: '1991',
								}, {
									title: '1992',
								}, {
									title: '1993',
								}, {
									title: '1994',
								}, {
									title: '1995',
								}, {
									title: '1996',
								}, {
									title: '1997',
								}, {
									title: '1998',
								}, {
									title: '1999',
								}, {
									title: '2000',
								}, {
									title: '2001',
								}, {
									title: '2002',
								}, {
									title: '2003',
								}],
							} as QuestionDropdownMetadataInterface) as any,
						}, {
							id: 57,
							question_group_id: 4,
							question_id: null,
							title: 'How do you dress on a typical weekday?',
							description: 'Your Stylist use this to find clothes that fit your lifestyle.',
							type: QUESTIONS.CHOICE,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								choices: [{
									title: 'Casual',
									image: '//question/daily-1-casual.jpg',
								}, {
									title: 'Smart Casual',
									image: '//question/daily-2-smart-casual.jpg',
								}, {
									title: 'Semi-formal',
									image: '//question/daily-3-semi-formal.jpg',
								}, {
									title: 'Formal',
									image: '//question/daily-4-formal.jpg',
								}],
							} as QuestionChoiceMetadataInterface) as any,
						}, {
							id: 58,
							question_group_id: 4,
							question_id: null,
							title: 'Are you a parent?',
							type: QUESTIONS.BOOLEAN,
							is_required: true,
							order: 3,
						}, {
							id: 59,
							question_group_id: 4,
							question_id: null,
							title: 'Your social network...',
							description: 'We love learning more about you. If you’d like to share any public social media usernames so that your Stylist can get a better sense of who you are when she styles for you, please include them here.',
							type: QUESTIONS.DEFINITION,
							order: 4,
						}, {
							id: 60,
							question_group_id: null,
							question_id: 59,
							title: 'Instagram Handle',
							description: 'Your Instagram username (e.g. @yunaandco)',
							type: QUESTIONS.TEXT_SHORT,
							order: 0,
							metadata: JSON.stringify({
								placeholder: 'Enter your @',
							} as QuestionTextMetadataInterface) as any,
						}, {
							id: 61,
							question_group_id: null,
							question_id: 59,
							title: 'Pinterest Board',
							description: 'Paste the full URL of your style board (e.g. https://www.pinterest.com/username/your-board)',
							type: QUESTIONS.URL,
							order: 1,
						}, {
							id: 62,
							question_group_id: 4,
							question_id: null,
							title: 'Anything else we should know before we style for you?',
							description: 'e.g. My workplace is pretty casual. I can wear jeans but I would like to dress it up with cute tops or outer-wears. Also, please stick to understated palette—I look best in black and navy. I prefer short sleeves and V-neck blouses.',
							type: QUESTIONS.TEXT_LONG,
							order: 5,
							metadata: JSON.stringify({
								placeholder: 'Write your note here in the language you’re most comfortable with—English or Bahasa Indonesia',
								limit: [0, 500],
							} as QuestionTabMetadataInterface) as any,
						}, {
							id: 63,
							question_group_id: 4,
							question_id: null,
							title: 'Upload your full body photo',
							description: 'Full body photo is preferable, so that your stylist can get a better picture of you. Share whatever you are comfortable with; we care a lot about privacy.',
							type: QUESTIONS.FILE,
							order: 6,
							metadata: JSON.stringify({
								count: 2,
							} as QuestionFileMetadataInterface) as any,
						},
					],
					transaction,
				)

				await this.QuestionRepository.insert(
					5,
					'Variant Feedback',
					// QUESTION_GROUPS.FEEDBACK,
					'Variant related questions',
					[
						{
							id: 64,
							question_group_id: 5,
							question_id: null,
							title: 'Style',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								choices: [{
									title: 'Hate it',
								}, {
									title: 'Just ok',
								}, {
									title: 'Like it',
								}, {
									title: 'Love it',
								}],
							} as QuestionScaleMetadataInterface) as any,
							category_ids: [1, 2, 3, 4],
						}, {
							id: 65,
							question_group_id: 5,
							question_id: null,
							title: 'How\'\s the Size?',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: false,
							order: 1,
							metadata: JSON.stringify({
							} as QuestionScaleMetadataInterface) as any,
							category_ids: [1, 2],
							deleted_at: TimeHelper.moment().format() as any,
						}, {
							id: 66,
							question_group_id: 5,
							question_id: null,
							title: 'Fit',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 1,
							metadata: JSON.stringify({
								choices: [{
									title: 'Too small',
								}, {
									title: 'Just right',
								}, {
									title: 'Too big',
								}],
							} as QuestionScaleMetadataInterface) as any,
							category_ids: [1, 2],
						}, {
							id: 67,
							question_group_id: 5,
							question_id: null,
							title: 'Length',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 2,
							metadata: JSON.stringify({
								choices: [{
									title: 'Too short',
								}, {
									title: 'Just right',
								}, {
									title: 'Too long',
								}],
							} as QuestionScaleMetadataInterface) as any,
							category_ids: [1],
						}, {
							id: 68,
							question_group_id: 5,
							question_id: null,
							title: 'Quality',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 3,
							metadata: JSON.stringify({
								choices: [{
									title: 'Bad',
								}, {
									title: 'Just Ok',
								}, {
									title: 'Great',
								}],
							} as QuestionScaleMetadataInterface) as any,
							category_ids: [1, 2, 3, 4],
						}, {
							id: 69,
							question_group_id: 5,
							question_id: null,
							title: 'What would you like to do with this piece?',
							description: null,
							type: QUESTIONS.TAB,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								info: 'Item with free / one sizing cannot be exchanged to another size',
								choices: [{
									title: 'Return',
								}, {
									title: 'Exchange',
									choices: [{
										title: 'Exchange to a different size',
									}, {
										title: 'This item was damaged or defective',
									}],
								}, {
									title: 'Keep',
								}],
							} as QuestionTabMetadataInterface) as any,
							category_ids: [1, 2, 3, 4],
						}, {
							id: 70,
							question_group_id: 5,
							question_id: null,
							title: 'Comment',
							description: null,
							type: QUESTIONS.TEXT_LONG,
							is_required: false,
							order: 4,
							metadata: JSON.stringify({
								note: 'Leave specific feedback about why you love or dislike this piece.',
								placeholder: 'Write your specific feedback here',
								limit: [0, 500],
							} as QuestionTextMetadataInterface) as any,
							category_ids: [1, 2, 3, 4],
						},
					],
					transaction,
				)

				await this.QuestionRepository.insert(
					6,
					'Feedback',
					// QUESTION_GROUPS.FEEDBACK,
					'Matchbox / Stylesheet related questions',
					[
						{
							id: 71,
							question_group_id: 6,
							question_id: null,
							title: 'How was your experience?',
							description: null,
							type: QUESTIONS.SCALE,
							is_required: true,
							order: 0,
							metadata: JSON.stringify({
								choices: [{
									title: 'Terrible',
								}, {
									title: '',
								}, {
									title: '',
								}, {
									title: '',
								}, {
									title: 'Great',
								}],
							} as QuestionScaleMetadataInterface) as any,
						}, {
							id: 72,
							question_group_id: 6,
							question_id: null,
							title: 'Are you looking forward to getting another matchbox?',
							description: null,
							type: QUESTIONS.BOOLEAN,
							is_required: true,
							order: 1,
						}, {
							id: 73,
							question_group_id: 6,
							question_id: null,
							title: 'Other thoughts?',
							description: 'Tell us your experience and what could be improved.',
							type: QUESTIONS.TEXT_LONG,
							is_required: false,
							order: 3,
							metadata: JSON.stringify({
								placeholder: 'Write your specific feedback here',
								limit: [0, 500],
							} as QuestionTextMetadataInterface) as any,
						}],
					transaction,
				)

				// 2.2.1 Update =======================================================================
				await this.QuestionRepository.insert(
					7,
					'Custom Service Feedback',
					// QUESTION_GROUPS.FEEDBACK,
					'Service / Custom matchbox feedback related questions',
					[{
						id: 74,
						order: 0,
						question_group_id: 7,
						question_id: null,
						title: 'Do you like it?',
						description: null,
						type: QUESTIONS.BOOLEAN,
						is_required: false,
						metadata: JSON.stringify({} as QuestionCommonMetadataInterface) as any,
					}],
					transaction,
				)

				await this.QuestionRepository.insertQuestion([{
					id: 75,
					order: 0,
					question_group_id: 4,
					question_id: null,
					title: 'How much do you typically spend on these items?',
					description: 'We want to send you clothes at prices you’re comfortable with',
					is_required: true,
					type: QUESTIONS.DEFINITION,
				}, {
					id: 76,
					question_group_id: null,
					question_id: 75,
					title: 'Tops',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-tops.jpg',
					order: 0,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 77,
					question_group_id: null,
					question_id: 75,
					title: 'Bottoms',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-bottoms.jpg',
					order: 1,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 78,
					question_group_id: null,
					question_id: 75,
					title: 'Dresses',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-dress.jpg',
					order: 2,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 79,
					question_group_id: null,
					question_id: 75,
					title: 'Outerwear',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-outerwear.jpg',
					order: 3,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 80,
					question_group_id: null,
					question_id: 75,
					title: 'Shoes',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-shoes.jpg',
					order: 4,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 81,
					question_group_id: null,
					question_id: 75,
					title: 'Bags',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-bags.jpg',
					order: 5,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}, {
					id: 82,
					question_group_id: null,
					question_id: 75,
					title: 'Accessories',
					type: QUESTIONS.SCALE,
					is_required: true,
					image: '//question/budget-accessories.jpg',
					order: 6,
					metadata: JSON.stringify({
						choices: this.choices.price,
						simple: true,
					} as QuestionScaleMetadataInterface) as any,
				}], transaction)

				// 2.2.1 Update =======================================================================
				await this.QuestionRepository.insertQuestion([{
					// Section "YOU" Update
					id: 56,
					question_group_id: 4,
					question_id: null,
					title: 'What\'s your date of birth?',
					description: 'This helps your Stylist to find the right clothes for you.',
					type: QUESTIONS.DATE,
					is_required: true,
					order: 1,
					metadata: JSON.stringify({
						format: 'DD MMM YYYY',
					}) as any,
				}], transaction)

				// 2.2.2 Update =======================================================================
				await this.QuestionRepository.insertQuestion([{
					id: 2,
					order: 2,
				}, {
					id: 6,
					order: 3,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 7,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 8,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 9,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 10,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 83,
					question_group_id: 1,
					question_id: null,
					title: 'What is your height and weight?',
					type: QUESTIONS.DEFINITION,
					is_required: true,
					order: 1,
					metadata: JSON.stringify({
						grouping: true,
					} as QuestionCommonMetadataInterface) as any,
				}, {
					id: 12,
					question_group_id: null,
					question_id: 83,
					title: 'Height?',
					description: 'cm',
					type: QUESTIONS.NUMBER,
					is_required: true,
					order: 0,
				}, {
					id: 13,
					question_group_id: null,
					question_id: 83,
					title: 'Weight?',
					description: 'kg',
					type: QUESTIONS.NUMBER,
					is_required: true,
					order: 1,
				}, {
					id: 14,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 84,
					question_group_id: 2,
					question_id: null,
					title: 'What brand do you usually wear?',
					type: QUESTIONS.DROPDOWN,
					is_required: true,
					order: 0,
					metadata: JSON.stringify({
						choices: [{
							title: 'Zara',
						}, {
							title: 'H&M',
						}, {
							title: 'Pull&Bear',
						}, {
							title: 'Uniqlo',
						}, {
							title: 'Other',
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 15,
					question_group_id: 2,
					title: 'What size do you typically wear for these items?',
					order: 1,
					metadata: JSON.stringify({}) as any,
				}, {
					id: 16,
					title: 'Dress',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'Alpha',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 17,
					title: 'Shirt/Blouse',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'Alpha',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 18,
					title: 'Skirt',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'Alpha',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 19,
					title: 'Pants',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'Alpha',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 20,
					title: 'Jeans Waist',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: this.choices.size.jeans,
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 21,
					title: 'Bra Band',
					type: QUESTIONS.DROPDOWN,
				}, {
					id: 22,
					title: 'Bra Cup',
					type: QUESTIONS.DROPDOWN,
				}, {
					id: 23,
					title: 'Shoe',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						placeholder: 'Select size',
						choices: [{
							title: 'EU',
							choices: this.choices.size.shoes.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.shoes.uk,
						}, {
							title: 'US',
							choices: this.choices.size.shoes.us,
						}],
					} as QuestionTabMetadataInterface) as any,
				}, {
					id: 24,
					order: 2,
				}, {
					id: 25,
					metadata: JSON.stringify({
						multiple: true,
						size_ratio: [102, 136],
						subsize: true,
						choices: [{
							title: 'Fitted',
							image: '//question/v2/fit-top-fitted.jpg',
						}, {
							title: 'Straight',
							image: '//question/v2/fit-top-straight.jpg',
						}, {
							title: 'Loose',
							image: '//question/v2/fit-top-loose.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 26,
					metadata: JSON.stringify({
						multiple: true,
						size_ratio: [102, 136],
						subsize: true,
						choices: [{
							title: 'Fitted',
							image: '//question/v2/fit-bottom-fitted.jpg',
						}, {
							title: 'Straight',
							image: '//question/v2/fit-bottom-straight.jpg',
						}, {
							title: 'Loose',
							image: '//question/v2/fit-bottom-loose.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 29,
					order: 2,
					metadata: JSON.stringify({
						multiple: true,
						size_ratio: [102, 136],
						subsize: true,
						choices: [{
							title: 'Low',
							image: '//question/v2/fit-jeans-lowrise.jpg',
						}, {
							title: 'Mid',
							image: '//question/v2/fit-jeans-midrise.jpg',
						}, {
							title: 'High',
							image: '//question/v2/fit-jeans-highrise.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 30,
					title: 'Looks that best represents you everyday',
					description: 'You may select more than one',
					type: QUESTIONS.CHOICE,
					metadata: JSON.stringify({
						multiple: true,
						size_ratio: [158, 212],
						choices: [{
							title: 'Artistic & Eclectic',
							image: '//question/v2/style-artistic.png',
						}, {
							title: 'Bohemian & Earthy',
							image: '//question/v2/style-bohemian.png',
						}, {
							title: 'Bold & Daring',
							image: '//question/v2/style-bold.png',
						}, {
							title: 'Chic & On Trend',
							image: '//question/v2/style-chic.png',
						}, {
							title: 'Classic & Preppy',
							image: '//question/v2/style-classic.png',
						}, {
							title: 'Cool & Edgy',
							image: '//question/v2/style-cool.png',
						}, {
							title: 'Glam & Feminine',
							image: '//question/v2/style-glam.png',
						}, {
							title: 'Minimal & Understated',
							image: '//question/v2/style-minimal.png',
						}, {
							title: 'Sharp & Polished',
							image: '//question/v2/style-sharp.png',
						}, {
							title: 'Urban & Effortless',
							image: '//question/v2/style-urban.png',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 31,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 32,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 33,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 34,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 35,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 36,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 37,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 38,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 39,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 40,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 49,
					metadata: JSON.stringify({
						select_none_text: 'I love showing them off',
						multiple: true,
						subsize: true,
						choices: [{
							title: 'Shoulders',
							image: '//question/v2/body-cover-shoulder.jpg',
						}, {
							title: 'Arms',
							image: '//question/v2/body-cover-arms.jpg',
						}, {
							title: 'Cleavage',
							image: '//question/v2/body-cover-cleavage.jpg',
						}, {
							title: 'Back',
							image: '//question/v2/body-cover-back.jpg',
						}, {
							title: 'Tummy',
							image: '//question/v2/body-cover-belly.jpg',
						}, {
							title: 'Buttocks',
							image: '//question/v2/body-cover-buttocks.jpg',
						}, {
							title: 'Legs',
							image: '//question/v2/body-cover-thighs.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 50,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 51,
					description: 'You may select more than one or select "I don\'t wear heels"',
					metadata: JSON.stringify({
						select_none_text: 'I don\'t wear heels',
						multiple: true,
						subsize: true,
						choices: [{
							title: 'Low',
							description: '3-5 cm',
							image: '//question/v2/heels-low.jpg',
						}, {
							title: 'Mid',
							description: '5-10 cm',
							image: '//question/v2/heels-mid.jpg',
						}, {
							title: 'High',
							description: '10-15 cm',
							image: '//question/v2/heels-high.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
					requirement: JSON.stringify({} as QuestionRequirementInterface) as any,
				}, {
					id: 53,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 54,
					metadata: JSON.stringify({
						multiple: true,
						subsize: true,
						choices: [{
							title: 'Silver',
							image: '//question/v2/jewelry-tone-silver.jpg',
						}, {
							title: 'Gold',
							image: '//question/v2/jewelry-tone-gold.jpg',
						}, {
							title: 'Rose Gold',
							image: '//question/v2/jewelry-tone-rose.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 55,
						description: 'You may select more than one or select "I can wear any material"',
					metadata: JSON.stringify({
						multiple: true,
						supersize: true,
						select_none_text: 'I can wear any material',
						choices: [{
							title: 'Silver',
						}, {
							title: 'Gold',
						}, {
							title: 'Platinum',
						}, {
							title: 'Base Metal',
							description: 'Iron, nickel, copper, titanium',
						}, {
							title: 'Natural',
							description: 'Leather, bone, silk, wood',
						}, {
							title: 'Synthetic',
							description: 'Enamel, plastic, silicone',
						}, {
							title: 'Regenerated',
							description: 'Resin, rubbber',
						}],
						other_text: 'Other accessory materials',
						other_placeholder: 'Enter accessory materials you\'re allergic to.',
						other_description: 'Use commas to separate multiple answers.',
					} as QuestionChoiceMetadataInterface) as any,
				}, {
					id: 56,
					title: 'Date of Birth',
					order: 2,
				}, {
					id: 57,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 58,
					title: 'Are you a mom?',
				}, {
					id: 85,
					question_group_id: 4,
					question_id: null,
					title: 'What brand do you typically wear?',
					description: 'We want to make sure that our picks meet your need.',
					type: QUESTIONS.CHOICE,
					is_required: true,
					order: 1,
					metadata: JSON.stringify({
						select_none_text: 'I cannot find my brands below',
						multiple: true,
						subsize: true,
						choices: [{
							title: 'H&M',
							image: '//question/v2/brand-h_m.png',
						}, {
							title: 'Zara',
							image: '//question/v2/brand-zara.png',
						}, {
							title: 'Uniqlo',
							image: '//question/v2/brand-uniqlo.png',
						}, {
							title: 'Mango',
							image: '//question/v2/brand-mango.png',
						}, {
							title: 'Cotton Ink',
							image: '//question/v2/brand-cottonink.png',
						}, {
							title: 'The Executive',
							image: '//question/v2/brand-the-executive.png',
						}, {
							title: 'Stradivarius',
							image: '//question/v2/brand-stradivarius.png',
						}, {
							title: 'Pull&Bear',
							image: '//question/v2/brand-pull_bear.png',
						}, {
							title: 'M&S',
							image: '//question/v2/brand-m_s.png',
						}, {
							title: 'Zalora',
							image: '//question/v2/brand-zalora.png',
						}, {
							title: 'Pomelo',
							image: '//question/v2/brand-pomelo.png',
						}, {
							title: 'This Is April',
							image: '//question/v2/brand-thisisapril.png',
						}, {
							title: 'HijUp',
							image: '//question/v2/brand-hijup.png',
						}, {
							title: 'Berrybenka',
							image: '//question/v2/brand-berrybenka.png',
						}, {
							title: 'Hijabenka',
							image: '//question/v2/brand-hijabenka.png',
						}, {
							title: 'shop at velvet',
							image: '//question/v2/brand-shopatvelvet.png',
						}, {
							title: 'Cotton On',
							image: '//question/v2/brand-cottonon.png',
						}, {
							title: 'Topshop',
							image: '//question/v2/brand-topshop.png',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				}], transaction)

				// 2.4 Update =======================================================================
				await this.QuestionRepository.insertQuestion([
				{
					id: 1,
					metadata: JSON.stringify({
						img_detail: true,
						choices: [{
							title: 'Pear',
							image: '//question/v2/body-shape-pear.jpg',
							description: 'Pinggul lebih lebar dari bahu',
						}, {
							title: 'Hourglass',
							image: '//question/v2/body-shape-hourglass.jpg',
							description: 'Pinggul sama seperti bahu dengan lekuk pinggang yang jelas',
						}, {
							title: 'Rectangle',
							image: '//question/v2/body-shape-rectangle.jpg',
							description: 'Pinggul sama seperti bahu dengan lekuk pinggang tak terlalu jelas',
						}, {
							title: 'Inverted triangle',
							image: '//question/v2/body-shape-inverted-triangle.jpg',
							description: 'Bahu lebih lebar dari pinggul',
						}, {
							title: 'Apple',
							image: '//question/v2/body-shape-apple.jpg',
							description: 'Lebih besar di sekitar bagian tengah tubuh',
						}, {
							title: 'Don\'t know',
							image: '//question/v2/body-shape-dont-know.jpg',
							description: 'Tidak pasti dengan bentuk tubuhmu',
						}],
					}),
				},
				{
					id: 2,
					metadata: JSON.stringify({
						grouping: false,
						modal: {
							caption: 'How to measure',
							title: 'How To Measure',
							content: [{
								description: 'When taking measurements, use a cloth tape measure, not a metal one. Make sure that, when you circle your chest, waist, or hips, the tape is level and neither too tight nor too loose. Measure yourself over tight clothes.',
							}, {
								title: 'Bust',
								description: 'Measure under your arms and around the fullest part of your chest. Note: Bust is not your bra size.',
								image: '//question/how-to-measure-bust.jpg',
							}, {
								title: 'Waist',
								description: 'Measure around your natural waistline, between your navel and rib cage.',
								image: '//question/how-to-measure-waist.jpg',
							}, {
								title: 'Hips',
								description: 'Stand with your feet together and measure around the fullest part of your hips.',
								image: '//question/how-to-measure-hips.jpg',
							}],
						},
					} as QuestionCommonMetadataInterface) as any,
				},
				{
					id: 3,
					metadata: JSON.stringify({
						min: 50,
						default: 80,
						max: 200,
					}),
				},
				{
					id: 4,
					metadata: JSON.stringify({
						min: 50,
						default: 80,
						max: 200,
					}),
				},
				{
					id: 5,
					metadata: JSON.stringify({
						min: 50,
						default: 80,
						max: 200,
					}),
				},
				{
					id: 12,
					metadata: JSON.stringify({
						min: 130,
						default: 150,
						max: 200,
					}),
				},
				{
					id: 13,
					metadata: JSON.stringify({
						min : 30,
						default: 50,
						max: 110,
					}),
				}, {
					id: 16,
					title: 'Dress',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'International',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 17,
					title: 'Shirt/Blouse',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'International',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 18,
					title: 'Skirt',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'International',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 19,
					title: 'Pants',
					type: QUESTIONS.DROPDOWN,
					metadata: JSON.stringify({
						choices: [{
							title: 'International',
							choices: this.choices.size.alpha,
						}, {
							title: 'EU',
							choices: this.choices.size.eu,
						}, {
							title: 'UK',
							choices: this.choices.size.uk,
						}, {
							title: 'US',
							choices: this.choices.size.us,
						}],
					} as QuestionDropdownMetadataInterface) as any,
				}, {
					id: 44,
					metadata: JSON.stringify({
						small: true,
						img_detail: true,
						size_ratio: [56, 56],
						highlights: ['dislike'],
						select_none_text: 'I wear any colour',
						multiple: true,
						inverted: true,
						choices: [{
							title: 'Purple',
							image: '//question/v2/color-purple.jpg',
						}, {
							title: 'Pink',
							image: '//question/v2/color-pink.jpg',
						}, {
							title: 'Red',
							image: '//question/v2/color-red.jpg',
						}, {
							title: 'Orange',
							image: '//question/v2/color-orange.jpg',
						}, {
							title: 'Yellow',
							image: '//question/v2/color-yellow.jpg',
						}, {
							title: 'Green',
							image: '//question/v2/color-green.jpg',
						}, {
							title: 'Turquoise',
							image: '//question/v2/color-turquoise.jpg',
						}, {
							title: 'Blue',
							image: '//question/v2/color-blue.jpg',
						}, {
							title: 'Brown',
							image: '//question/v2/color-brown.jpg',
						}, {
							title: 'Beige',
							image: '//question/v2/color-beige.jpg',
						}, {
							title: 'White',
							image: '//question/v2/color-white.jpg',
						}, {
							title: 'Grey',
							image: '//question/v2/color-grey.jpg',
						}, {
							title: 'Black',
							image: '//question/v2/color-black.jpg',
						}, {
							title: 'Silver',
							image: '//question/v2/color-silver.jpg',
						}, {
							title: 'Gold',
							image: '//question/v2/color-gold.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				},
				{
					id: 45,
					metadata: JSON.stringify({
						size_ratio: [102, 102],
						select_none_text: 'I like any detail',
						highlights: ['avoid'],
						multiple: true,
						inverted: true,
						choices: [{
							title: 'Pleat',
							image: '//question/detail-1-pleat.jpg',
						}, {
							title: 'Ruffle',
							image: '//question/detail-2-ruffle.jpg',
						}, {
							title: 'Fringe',
							image: '//question/detail-3-fringe.jpg',
						}, {
							title: 'Embroidery',
							image: '//question/detail-4-embroidery.jpg',
						}, {
							title: 'Patch',
							image: '//question/detail-5-patch.jpg',
						}, {
							title: 'Screen Print',
							image: '//question/detail-6-screen-print.jpg',
						}, {
							title: 'Lace',
							image: '//question/detail-7-lace.jpg',
						}, {
							title: 'Embellishment',
							image: '//question/detail-8-embellishment.jpg',
						}, {
							title: 'Sequin',
							image: '//question/detail-9-sequin.jpg',
						}, {
							title: 'Tassel',
							image: '//question/detail-10-tassel.jpg',
						}, {
							title: 'Transparency',
							image: '//question/detail-11-transparency.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				},
				{
					id: 46,
					metadata: JSON.stringify({
						// size_ratio: [102, 102],
						square_row_3: true,
						select_none_text: 'I wear any print',
						highlights: ['avoid'],
						multiple: true,
						inverted: true,
						choices: [{
							title: 'Animal print',
							image: '//question/pattern-1-animal-print.jpg',
						}, {
							title: 'Paisley',
							image: '//question/pattern-2-paisley.jpg',
						}, {
							title: 'Floral',
							image: '//question/pattern-3-floral.jpg',
						}, {
							title: 'Plaid / Tartan',
							image: '//question/pattern-4-plaid-tartan.jpg',
						}, {
							title: 'Polka dots',
							image: '//question/pattern-5-polka-dots.jpg',
						}, {
							title: 'Stripes',
							image: '//question/pattern-6-stripes.jpg',
						}, {
							title: 'Novelty print',
							image: '//question/pattern-7-novelty-print.jpg',
						}, {
							title: 'Check',
							image: '//question/pattern-8-check.jpg',
						}, {
							title: 'Traditional print',
							image: '//question/pattern-9-traditional.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				},
				{
					id: 48,
					deleted_at: TimeHelper.moment().format() as any,
				}, {
					id: 49,
					question_group_id: 3,
					question_id: null,
					title: 'Are there any body areas you\'d like to cover up?',
					// Boleh pilih lebih dari satu atau pilih "Tidak ada” jika kamu tidak keberatan memperlihatkan bagian tubuh berikut ini. Jika kamu memakai hijab, aktifkan toggle “Saya memakai hijab".
					description: 'You may select more than one or select "I love showing them off". If you wear hijab, activate the "I wear hijab" toggle.',
					metadata: JSON.stringify({
						size_ratio: [102, 102],
						select_none_text: 'I wear hijab',
						select_none_switch: true,
						multiple_other_button_text: 'I love showing them off',
						multiple: true,
						choices: [{
							title: 'Shoulders',
							image: '//question/v2/body-cover-shoulder.jpg',
						}, {
							title: 'Arms',
							image: '//question/v2/body-cover-arms.jpg',
						}, {
							title: 'Cleavage',
							image: '//question/v2/body-cover-cleavage.jpg',
						}, {
							title: 'Back',
							image: '//question/v2/body-cover-back.jpg',
						}, {
							title: 'Tummy',
							image: '//question/v2/body-cover-belly.jpg',
						}, {
							title: 'Buttocks',
							image: '//question/v2/body-cover-buttocks.jpg',
						}, {
							title: 'Legs',
							image: '//question/v2/body-cover-thighs.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
					requirement: JSON.stringify({} as QuestionRequirementInterface) as any,
				},
				{
					id: 51,
					metadata: JSON.stringify({
						img_detail: true,
						select_none_text: 'I don\'t wear heels',
						multiple: true,
						choices: [{
							title: 'Low',
							description: '3-5 cm',
							image: '//question/v2/heels-low.jpg',
						}, {
							title: 'Mid',
							description: '5-10 cm',
							image: '//question/v2/heels-mid.jpg',
						}, {
							title: 'High',
							description: '10-15 cm',
							image: '//question/v2/heels-high.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				},
				{
					id: 54,
					metadata: JSON.stringify({
						size_ratio: [102, 102],
						square_row_3: true,
						multiple: true,
						choices: [{
							title: 'Silver',
							image: '//question/v2/jewelry-tone-silver.jpg',
						}, {
							title: 'Gold',
							image: '//question/v2/jewelry-tone-gold.jpg',
						}, {
							title: 'Rose Gold',
							image: '//question/v2/jewelry-tone-rose.jpg',
						}],
					} as QuestionChoiceMetadataInterface) as any,
				},
				{
					id: 84,
					question_group_id: null,
					question_id: 15,
					order: 0,
				}, {
					id: 16,
					order: 1,
				}, {
					id: 17,
					order: 2,
				}, {
					id: 18,
					order: 3,
				}, {
					id: 19,
					order: 4,
				}, {
					id: 20,
					order: 5,
				}, {
					id: 21,
					order: 6,
				}, {
					id: 22,
					order: 7,
				}, {
					id: 23,
					order: 8,
				}, {
					id: 83,
					metadata: JSON.stringify({
						grouping: false,
					} as QuestionCommonMetadataInterface) as any,
				}, {
					id: 85,
					metadata: JSON.stringify({
						align: 'center',
						select_none_text: 'I cannot find my brands below',
						multiple: true,
						subsize: true,
						choices: [{
							title: 'H&M',
						}, {
							title: 'Zara',
						}, {
							title: 'Uniqlo',
						}, {
							title: 'Mango',
						}, {
							title: 'Cotton Ink',
						}, {
							title: 'The Executive',
						}, {
							title: 'Stradivarius',
						}, {
							title: 'Pull&Bear',
						}, {
							title: 'M&S',
						}, {
							title: 'Zalora',
						}, {
							title: 'Pomelo',
						}, {
							title: 'This Is April',
						}, {
							title: 'HijUp',
						}, {
							title: 'Berrybenka',
						}, {
							title: 'Hijabenka',
						}, {
							title: 'shop at velvet',
						}, {
							title: 'Cotton On',
						}, {
							title: 'Topshop',
						}],
						other_text: 'Brand lain',
						other_placeholder: 'Tulis brand favorit lain yang biasanya kamu belanja',
						other_description: 'Gunakan koma untuk memisahkan jawaban',
					} as QuestionChoiceMetadataInterface) as any,
				}], transaction)

				// 2.6.1 Update =======================================================================
				await this.QuestionRepository.insertQuestion([
					{
						id: 64,
						metadata: JSON.stringify({
							choices: [{
								title: 'Disappointed',
							}, {
								title: 'Hate It',
							}, {
								title: 'Not Bad',
							}, {
								title: 'Like It',
							}, {
								title: 'Love It!',
							}],
						} as QuestionScaleMetadataInterface) as any,
					}, {
						id: 68,
						metadata: JSON.stringify({
							choices: [{
								title: 'Bad',
							}, {
								title: 'Not Good',
							}, {
								title: 'Just Ok',
							}, {
								title: 'Good',
							}, {
								title: 'The Best!',
							}],
						} as QuestionScaleMetadataInterface) as any,
					},
				], transaction)

				// 2.7 Update =======================================================================
				// await this.QuestionRepository.insertQuestion([
				// 	{
				// 		id: 67,
				// 		is_required: false,
				// 	},
				// 	{
				// 		id: 84,
				// 		question_group_id: null,
				// 		question_id: null,
				// 		order: 0,
				// 	},
				// 	{
				// 		id: 30,
				// 		metadata: JSON.stringify({
				// 			multiple: true,
				// 			rectangle_row_2: true,
				// 			choices: [{
				// 				title: 'Artistic & Eclectic',
				// 				image: '//question/v2/style-artistic.png',
				// 			}, {
				// 				title: 'Bohemian & Earthy',
				// 				image: '//question/v2/style-bohemian.png',
				// 			}, {
				// 				title: 'Bold & Daring',
				// 				image: '//question/v2/style-bold.png',
				// 			}, {
				// 				title: 'Chic & On Trend',
				// 				image: '//question/v2/style-chic.png',
				// 			}, {
				// 				title: 'Classic & Preppy',
				// 				image: '//question/v2/style-classic.png',
				// 			}, {
				// 				title: 'Cool & Edgy',
				// 				image: '//question/v2/style-cool.png',
				// 			}, {
				// 				title: 'Glam & Feminine',
				// 				image: '//question/v2/style-glam.png',
				// 			}, {
				// 				title: 'Minimal & Understated',
				// 				image: '//question/v2/style-minimal.png',
				// 			}, {
				// 				title: 'Sharp & Polished',
				// 				image: '//question/v2/style-sharp.png',
				// 			}, {
				// 				title: 'Urban & Effortless',
				// 				image: '//question/v2/style-urban.png',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 	}, {
				// 		id: 45,
				// 		metadata: JSON.stringify({
				// 			square_row_3: true,
				// 			select_none_text: 'I like any detail',
				// 			highlights: ['avoid'],
				// 			multiple: true,
				// 			inverted: true,
				// 			choices: [{
				// 				title: 'Pleat',
				// 				image: '//question/detail-1-pleat.jpg',
				// 			}, {
				// 				title: 'Ruffle',
				// 				image: '//question/detail-2-ruffle.jpg',
				// 			}, {
				// 				title: 'Fringe',
				// 				image: '//question/detail-3-fringe.jpg',
				// 			}, {
				// 				title: 'Embroidery',
				// 				image: '//question/detail-4-embroidery.jpg',
				// 			}, {
				// 				title: 'Patch',
				// 				image: '//question/detail-5-patch.jpg',
				// 			}, {
				// 				title: 'Screen Print',
				// 				image: '//question/detail-6-screen-print.jpg',
				// 			}, {
				// 				title: 'Lace',
				// 				image: '//question/detail-7-lace.jpg',
				// 			}, {
				// 				title: 'Embellishment',
				// 				image: '//question/detail-8-embellishment.jpg',
				// 			}, {
				// 				title: 'Sequin',
				// 				image: '//question/detail-9-sequin.jpg',
				// 			}, {
				// 				title: 'Tassel',
				// 				image: '//question/detail-10-tassel.jpg',
				// 			}, {
				// 				title: 'Transparency',
				// 				image: '//question/detail-11-transparency.jpg',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 	}, {
				// 		id: 46,
				// 		metadata: JSON.stringify({
				// 			square_row_3: true,
				// 			select_none_text: 'I wear any print',
				// 			highlights: ['avoid'],
				// 			multiple: true,
				// 			inverted: true,
				// 			choices: [{
				// 				title: 'Animal print',
				// 				image: '//question/pattern-1-animal-print.jpg',
				// 			}, {
				// 				title: 'Paisley',
				// 				image: '//question/pattern-2-paisley.jpg',
				// 			}, {
				// 				title: 'Floral',
				// 				image: '//question/pattern-3-floral.jpg',
				// 			}, {
				// 				title: 'Plaid / Tartan',
				// 				image: '//question/pattern-4-plaid-tartan.jpg',
				// 			}, {
				// 				title: 'Polka dots',
				// 				image: '//question/pattern-5-polka-dots.jpg',
				// 			}, {
				// 				title: 'Stripes',
				// 				image: '//question/pattern-6-stripes.jpg',
				// 			}, {
				// 				title: 'Novelty print',
				// 				image: '//question/pattern-7-novelty-print.jpg',
				// 			}, {
				// 				title: 'Check',
				// 				image: '//question/pattern-8-check.jpg',
				// 			}, {
				// 				title: 'Traditional print',
				// 				image: '//question/pattern-9-traditional.jpg',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 	}, {
				// 		id: 54,
				// 		metadata: JSON.stringify({
				// 			square_row_3: true,
				// 			multiple: true,
				// 			choices: [{
				// 				title: 'Silver',
				// 				image: '//question/v2/jewelry-tone-silver.jpg',
				// 			}, {
				// 				title: 'Gold',
				// 				image: '//question/v2/jewelry-tone-gold.jpg',
				// 			}, {
				// 				title: 'Rose Gold',
				// 				image: '//question/v2/jewelry-tone-rose.jpg',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 	}, {
				// 		id: 12,
				// 		metadata: JSON.stringify({
				// 			placeholder: 'Tinggi',
				// 			min: 130,
				// 			default: 150,
				// 			max: 200,
				// 		}),
				// 	},
				// 	{
				// 		id: 13,
				// 		metadata: JSON.stringify({
				// 			placeholder: 'Berat',
				// 			min : 30,
				// 			default: 50,
				// 			max: 110,
				// 		}),
				// 	},
				// 	{
				// 		id: 24,
				// 		metadata: JSON.stringify({
				// 			rectangle_row_3: true,
				// 			multiple: true,
				// 				choices: [{
				// 					title: 'Fitted',
				// 					// image: '',
				// 				}, {
				// 					title: 'Straight',
				// 					// image: '',
				// 				}, {
				// 					title: 'Loose',
				// 					// image: '',
				// 				}],
				// 		}),
				// 	},
				// 	{
				// 		id: 2,
				// 		metadata: JSON.stringify({
				// 			row_3: true,
				// 			grouping: true,
				// 				modal: {
				// 					caption: 'How to measure',
				// 					title: 'How To Measure',
				// 					content: [{
				// 						description: 'When taking measurements, use a cloth tape measure, not a metal one. Make sure that, when you circle your chest, waist, or hips, the tape is level and neither too tight nor too loose. Measure yourself over tight clothes.',
				// 					}, {
				// 						title: 'Bust',
				// 						description: 'Measure under your arms and around the fullest part of your chest. Note: Bust is not your bra size.',
				// 						image: '//question/how-to-measure-bust.jpg',
				// 					}, {
				// 						title: 'Waist',
				// 						description: 'Measure around your natural waistline, between your navel and rib cage.',
				// 						image: '//question/how-to-measure-waist.jpg',
				// 					}, {
				// 						title: 'Hips',
				// 						description: 'Stand with your feet together and measure around the fullest part of your hips.',
				// 						image: '//question/how-to-measure-hips.jpg',
				// 					}],
				// 				},
				// 		}),
				// 	},
				// 	{
				// 		id: 83,
				// 		metadata: JSON.stringify({
				// 			row_2: true,
				// 			grouping: false,
				// 		}),
				// 	}, {
				// 		id: 49,
				// 		metadata: JSON.stringify({
				// 			size_ratio: [102, 102],
				// 			select_none_text: 'Tidak ada yang ingin ditutupi',
				// 			multiple: true,
				// 			subsize: true,
				// 			choices: [{
				// 				title: 'Shoulders',
				// 			}, {
				// 				title: 'Arms',
				// 			}, {
				// 				title: 'Cleavage',
				// 			}, {
				// 				title: 'Back',
				// 			}, {
				// 				title: 'Tummy',
				// 			}, {
				// 				title: 'Buttocks',
				// 			}, {
				// 				title: 'Legs',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 	},{
				// 		id: 49,
				// 		// Boleh pilih lebih dari satu atau pilih "Tidak ada” jika kamu tidak keberatan memperlihatkan bagian tubuh berikut ini. Jika kamu memakai hijab, aktifkan toggle “Saya memakai hijab".
				// 		metadata: JSON.stringify({
				// 			square_row_3: true,
				// 			select_none_text: 'I wear hijab',
				// 			select_none_switch: true,
				// 			multiple_other_button_text: 'I love showing them off',
				// 			multiple: true,
				// 			choices: [{
				// 				title: 'Shoulders',
				// 				image: '//question/v2/body-cover-shoulder.jpg',
				// 			}, {
				// 				title: 'Arms',
				// 				image: '//question/v2/body-cover-arms.jpg',
				// 			}, {
				// 				title: 'Cleavage',
				// 				image: '//question/v2/body-cover-cleavage.jpg',
				// 			}, {
				// 				title: 'Back',
				// 				image: '//question/v2/body-cover-back.jpg',
				// 			}, {
				// 				title: 'Tummy',
				// 				image: '//question/v2/body-cover-belly.jpg',
				// 			}, {
				// 				title: 'Buttocks',
				// 				image: '//question/v2/body-cover-buttocks.jpg',
				// 			}, {
				// 				title: 'Legs',
				// 				image: '//question/v2/body-cover-thighs.jpg',
				// 			}],
				// 		} as QuestionChoiceMetadataInterface) as any,
				// 		requirement: JSON.stringify({} as QuestionRequirementInterface) as any,
				// 	}, {
				// 		id: 84,
				// 		metadata: JSON.stringify({
				// 			question_group_id: 2,
				// 		})
				// 	}
				// ], transaction)

				// 2.7.1 Update =======================================================================
				await this.QuestionRepository.insertQuestion([
					{
						id: 84,
						question_id: null,
					},
				], transaction)
			}
		})
	}

	private getAnswer(answer: UserAnswerInterface | FeedbackAnswerInterface, question: QuestionInterface, useNumberScale: boolean = false) {
		switch(question.type) {
		case QUESTIONS.BOOLEAN:
		case QUESTIONS.CHECKBOX:
			return answer.answer[question.type]
				? 'Yes'
				: answer.answer[question.type] === null
					? null
					: 'No'
		case QUESTIONS.CHOICE:
			return answer.answer[question.type] ? answer.answer[question.type].join(', ') : null
		case QUESTIONS.DATE:
		case QUESTIONS.DEFINITION:
		case QUESTIONS.DROPDOWN:
		case QUESTIONS.EMAIL:
			return answer.answer[question.type]
		case QUESTIONS.DATE:
			return answer.answer[question.type] ? TimeHelper.format(answer.answer[question.type], (question.metadata as QuestionDateMetadataInterface).format) : answer.answer[QUESTIONS.DROPDOWN]
		case QUESTIONS.FILE:
			return answer.answer[question.type].filter(a => a !== null)
		case QUESTIONS.NUMBER:
			return answer.answer[question.type] + (question.description ? ' ' + question.description : '')
		case QUESTIONS.SCALE:
			return (question.metadata as QuestionScaleMetadataInterface).choices[answer.answer[question.type]]?.title ?? answer.answer[question.type]
		case QUESTIONS.SLIDER:
		case QUESTIONS.STATEMENT:
			return answer.answer[question.type]
		case QUESTIONS.TAB:
			return answer.answer[question.type] + ( answer.answer.CHOICES ? ' — ' + answer.answer.CHOICES.join(', ') : '' )
		case QUESTIONS.TEXT_LONG:
		case QUESTIONS.TEXT_SHORT:
		case QUESTIONS.URL:
			return answer.answer[question.type]
		}
	}

}

export default new QuestionManager()
