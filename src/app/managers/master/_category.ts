import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import CategoryRepository from 'app/repositories/category'

import MeasurementManager from './_measurement'
import SizeManager from './_size'
import TagManager from './_tag'

import { CategoryInterface } from 'energie/app/interfaces'
import { CacheHelper, CommonHelper } from 'utils/helpers'

import DEFAULTS from 'utils/constants/default'
// import { ERROR_CODES } from 'app/models/error'


class CategoryManager extends ManagerModel {

	static __displayName = 'CategoryManager'

	protected CategoryRepository: CategoryRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			CategoryRepository,
		], async () => {
			// Initialization function goes here

			await Promise.all([
				MeasurementManager.initialize(connection),
				SizeManager.initialize(connection),
				TagManager.initialize(connection),
			])

			await this.make()

			this.withTransaction(undefined, async transaction => {
				if (!CacheHelper.get('categoryIdsByParentId') || !CacheHelper.get('categoriesById')) {
					await this.getAllIds(true, transaction)
				}
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPSERT =============================
	@ManagerModel.bound
	async upsert(categories: Array<Partial<CategoryInterface>>, transaction: EntityManager) {
		return this.CategoryRepository.saveOrUpdate('CategoryRecord', categories.map(category => CommonHelper.stripUndefined(category)), transaction)
	}

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getAllIds(complete: boolean | undefined, transaction: EntityManager) {
		return this.CategoryRepository.getAllIds(complete, transaction).then(categories => {
			if (
				complete === true
				&& (!CacheHelper.get('categoryIdsByParentId') || !CacheHelper.get('categoriesById'))
			) {
				const categoriesById = {}
				const categoryIdsByParentId = {}

				categories.forEach(category => {
					categoriesById[category.id] = {
						id: category.id,
						title: category.title,
						category_id: category.category_id,
					}

					if (categoryIdsByParentId[category.id] === undefined) {
						// insert default []
						categoryIdsByParentId[category.id] = []
					}

					if (categoryIdsByParentId[category.category_id] === undefined) {
						categoryIdsByParentId[category.category_id] = []
					}

					categoryIdsByParentId[category.category_id].push(category.id)
				})

				CacheHelper.set('categoryIdsByParentId', categoryIdsByParentId, -1)
				CacheHelper.set('categoriesById', categoriesById, -1)
			}

			return categories
		})
	}

	@ManagerModel.bound
	async getById(
		category_id: number,
		transaction: EntityManager,
	): Promise<Pick<CategoryInterface, 'id' | 'title' | 'category_id'>> {

		const categoriesById = CacheHelper.get('categoriesById')

		if (categoriesById && categoriesById[category_id]) {
			return categoriesById[category_id]
		} else {
			return this.CategoryRepository.getById(category_id, transaction).then(category => {
				const _cat = {
					id: category.id,
					title: category.title,
					category_id: category.category_id,
					categories: category.categories,
				}

				CacheHelper.set('categoryIdsByParentId', {
					...(CacheHelper.get('categoryIdsByParentId') || {}),
					[category.id]: category.categories.map(c => c.id),
				}, -1)

				CacheHelper.set('categoriesById', {
					...(CacheHelper.get('categoriesById') || {}),
					[category.id]: _cat,
				}, -1)

				return _cat
			})
		}
	}

	@ManagerModel.bound
	async getChildrenIds(category_id: number, transaction: EntityManager): Promise<number[]> {
		const categoryIdsByParentId = CacheHelper.get('categoryIdsByParentId')
		if(categoryIdsByParentId && categoryIdsByParentId[category_id]) {
			return categoryIdsByParentId[category_id]
		} else {
			return this.getById(category_id, transaction).then(() => {
				// because cache already set by getById
				return CacheHelper.get('categoryIdsByParentId')[category_id]
			})
		}
	}

	@ManagerModel.bound
	async getAllChildrenIds(category_id: number, transaction: EntityManager): Promise<number[]> {
		const categoryIdsByParentId = CacheHelper.get('categoryIdsByParentId')

		if (categoryIdsByParentId && categoryIdsByParentId[category_id]) {
			if(categoryIdsByParentId[category_id].length) {
				// have children
				return await Promise.all((categoryIdsByParentId[category_id] as number[]).map(async categoryId => {
					return await this.getAllChildrenIds(categoryId, transaction)
				})).then(numbers => [category_id, ...numbers.reduce(CommonHelper.sumArray, [])])
			} else {
				return [category_id]
			}
			// return categoryIdsByParentId[category_id]
		} else {
			return this.getById(category_id, transaction).then(() => {
				return CacheHelper.get('categoryIdsByParentId')[category_id]
			})
		}
	}

	@ManagerModel.bound
	async getParentId(category_id: number, transaction: EntityManager): Promise<number> {
		const categoriesById = CacheHelper.get('categoriesById')

		if (categoriesById && categoriesById[category_id]) {
			return categoriesById[category_id].category_id
		} else {
			return this.getById(category_id, transaction).then(category => {
				return category.category_id
			})
		}
	}

	@ManagerModel.bound
	async getRoot(category_id: number, transaction: EntityManager): Promise<Pick<CategoryInterface, 'id' | 'title' | 'category_id'>> {
		const categoriesById = CacheHelper.get('categoriesById')

		if (categoriesById && categoriesById[category_id]) {
			if (categoriesById[category_id].category_id === null) {
				return categoriesById[category_id]
			} else {
				return this.getRoot(categoriesById[category_id].category_id, transaction)
			}
		} else {
			return this.getById(category_id, transaction).then(category => {
				return category
			})
		}
	}

	@ManagerModel.bound
	async getRoots(transaction: EntityManager): Promise<Array<Pick<CategoryInterface, 'id' | 'title' | 'category_id'>>> {
		const categoryIdsByParentId = CacheHelper.get('categoryIdsByParentId')

		// tslint:disable-next-line: no-string-literal
		if (categoryIdsByParentId['null'] && categoryIdsByParentId['null'].length) {
			// tslint:disable-next-line: no-string-literal
			return Promise.all(categoryIdsByParentId['null'].map(async (cId: number) => {
				return CacheHelper.get('categoriesById')[cId] || this.getById(cId, transaction)
			}))
		} else {
			return this.CategoryRepository.getRoots(transaction)
		}
	}

	@ManagerModel.bound
	async getRootId(category_id: number, transaction: EntityManager): Promise<number> {
		const categoriesById = CacheHelper.get('categoriesById')

		if (categoriesById && categoriesById[category_id]) {
			if(categoriesById[category_id].category_id === null) {
				return category_id
			} else {
				return this.getRootId(categoriesById[category_id].category_id, transaction)
			}
		} else {
			return this.getById(category_id, transaction).then(category => {
				return category.category_id
			})
		}
	}
	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async make() {
		if(DEFAULTS.SYNC) {
			return this.transaction(async transaction => {

				await this.CategoryRepository.insert(1, null, 'clothing', '', [], [], [], transaction)
				await this.CategoryRepository.insert(2, null, 'footwear', '', [], [], [], transaction)
				await this.CategoryRepository.insert(3, null, 'bag', '', [], [], [], transaction)
				await this.CategoryRepository.insert(4, null, 'accessory', '', [], [], [], transaction)
				await this.CategoryRepository.insert(5, 1, 'dress', '', [25], [1], [1, 2, 3, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(6, 1, 'one-piece', '', [25], [1], [1, 2, 3, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(7, 1, 'top', '', [1, 4], [1], [1, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(8, 1, 'outerwear', '', [1, 4], [1], [1, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(9, 1, 'pants', '', [24], [1], [2, 7, 8, 102, 4], transaction)
				await this.CategoryRepository.insert(10, 1, 'short', '', [24], [1], [2, 4, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(11, 1, 'jeans', '', [24], [1, 2], [2, 5, 4, 7, 102], transaction)
				await this.CategoryRepository.insert(12, 1, 'skirt', '', [24], [1], [2, 3, 4, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(13, 2, 'shoe', '', [2], [3], [6, 102], transaction)
				await this.CategoryRepository.insert(14, 3, 'bag', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(15, 3, 'bag', '', [3], [4], [102], transaction)
				// await this.CategoryRepository.insert(15, 4, 'accessory', '', [3], [4], [10, 11, 12, 98, 111], transaction)

				const categories = [
					{
						id: 16,
						category_id: 5,
						title: 'cold shoulder dress',
						description: null,
					},
					{
						id: 17,
						category_id: 5,
						title: 'fit & flare dress',
						description: null,
					},
					{
						id: 18,
						category_id: 5,
						title: 'halter dress',
						description: null,
					},
					{
						id: 19,
						category_id: 5,
						title: 'off-shoulder dress',
						description: null,
					},
					{
						id: 20,
						category_id: 5,
						title: 'pinafore dress',
						description: null,
					},
					{
						id: 21,
						category_id: 5,
						title: 'qipao',
						description: null,
					},
					{
						id: 22,
						category_id: 5,
						title: 'sheath Dress',
						description: null,
					},
					{
						id: 23,
						category_id: 5,
						title: 'shift dress',
						description: null,
					},
					{
						id: 24,
						category_id: 5,
						title: 'shirt dress',
						description: null,
					},
					{
						id: 25,
						category_id: 5,
						title: 'strap dress',
						description: null,
					},
					{
						id: 26,
						category_id: 5,
						title: 'strapless dress',
						description: null,
					},
					{
						id: 27,
						category_id: 5,
						title: 'trapeze dress',
						description: null,
					},
					{
						id: 28,
						category_id: 5,
						title: 'tunic dress',
						description: null,
					},
					{
						id: 29,
						category_id: 5,
						title: 'wrap dress',
						description: null,
					},
					{
						id: 30,
						category_id: 6,
						title: 'bodysuit',
					},
					{
						id: 31,
						category_id: 6,
						title: 'jumpsuit',
					},
					{
						id: 32,
						category_id: 6,
						title: 'overalls',
					},
					{
						id: 33,
						category_id: 6,
						title: 'playsuit',
					},
					{
						id: 34,
						category_id: 6,
						title: 'romper',
					},
					{
						id: 35,
						category_id: 7,
						title: 'asymmetric top',
					},
					{
						id: 36,
						category_id: 7,
						title: 'blouse',
					},
					{
						id: 37,
						category_id: 7,
						title: 'bralette',
					},
					{
						id: 38,
						category_id: 7,
						title: 'camisole',
					},
					{
						id: 39,
						category_id: 7,
						title: 'crop top',
					},
					{
						id: 40,
						category_id: 7,
						title: 'halter top',
					},
					{
						id: 41,
						category_id: 7,
						title: 'jumper',
					},
					{
						id: 42,
						category_id: 7,
						title: 'off-shoulder top',
					},
					{
						id: 43,
						category_id: 7,
						title: 'peplum',
					},
					{
						id: 44,
						category_id: 7,
						title: 'shirt top',
					},
					{
						id: 45,
						category_id: 7,
						title: 't-shirt',
					},
					{
						id: 46,
						category_id: 7,
						title: 'tank-top',
					},
					{
						id: 47,
						category_id: 7,
						title: 'tube top',
					},
					{
						id: 48,
						category_id: 7,
						title: 'tunic top',
					},
					{
						id: 49,
						category_id: 8,
						title: 'blazer',
					},
					{
						id: 50,
						category_id: 8,
						title: 'bolero',
					},
					{
						id: 51,
						category_id: 8,
						title: 'cape',
					},
					{
						id: 52,
						category_id: 8,
						title: 'cardigan',
					},
					{
						id: 53,
						category_id: 8,
						title: 'coat',
					},
					{
						id: 54,
						category_id: 8,
						title: 'hoodie',
					},
					{
						id: 55,
						category_id: 8,
						title: 'jacket',
					},
					{
						id: 56,
						category_id: 8,
						title: 'kimono',
					},
					{
						id: 57,
						category_id: 8,
						title: 'parka',
					},
					{
						id: 58,
						category_id: 8,
						title: 'poncho',
					},
					{
						id: 59,
						category_id: 8,
						title: 'puffer jacket',
					},
					{
						id: 60,
						category_id: 8,
						title: 'vest',
					},
					{
						id: 61,
						category_id: 9,
						title: 'bootcut pants',
					},
					{
						id: 62,
						category_id: 9,
						title: 'capri pants',
					},
					{
						id: 63,
						category_id: 9,
						title: 'cargo pants',
					},
					{
						id: 64,
						category_id: 9,
						title: 'cigarette pants',
					},
					{
						id: 65,
						category_id: 9,
						title: 'culottes',
					},
					{
						id: 66,
						category_id: 9,
						title: 'flared pants',
					},
					{
						id: 67,
						category_id: 9,
						title: 'joggers',
					},
					{
						id: 68,
						category_id: 9,
						title: 'khaki pants',
					},
					{
						id: 69,
						category_id: 9,
						title: 'leggings',
					},
					{
						id: 70,
						category_id: 9,
						title: 'palazzo pants',
					},
					{
						id: 71,
						category_id: 9,
						title: 'skinny pants',
					},
					{
						id: 72,
						category_id: 9,
						title: 'straight pants',
					},
					{
						id: 73,
						category_id: 10,
						title: 'bermuda shorts',
					},
					{
						id: 74,
						category_id: 10,
						title: 'denim shorts',
					},
					{
						id: 75,
						category_id: 10,
						title: 'runner shorts',
					},
					{
						id: 76,
						category_id: 10,
						title: 'skort',
					},
					{
						id: 77,
						category_id: 11,
						title: 'ankle jeans',
					},
					{
						id: 78,
						category_id: 11,
						title: 'bootcut jeans',
					},
					{
						id: 79,
						category_id: 11,
						title: 'boyfriend jeans',
					},
					{
						id: 80,
						category_id: 11,
						title: 'cropped jeans',
					},
					{
						id: 81,
						category_id: 11,
						title: 'flared jeans',
					},
					{
						id: 82,
						category_id: 11,
						title: 'ripped jeans',
					},
					{
						id: 83,
						category_id: 11,
						title: 'skinny jeans',
					},
					{
						id: 84,
						category_id: 11,
						title: 'straight jeans',
					},
					{
						id: 85,
						category_id: 12,
						title: 'a-line skirt',
					},
					{
						id: 86,
						category_id: 12,
						title: 'asymmetric skirt',
					},
					{
						id: 87,
						category_id: 12,
						title: 'bubble skirt',
					},
					{
						id: 88,
						category_id: 12,
						title: 'denim skirt',
					},
					{
						id: 89,
						category_id: 12,
						title: 'flared skirt',
					},
					{
						id: 90,
						category_id: 12,
						title: 'full skirt',
					},
					{
						id: 91,
						category_id: 12,
						title: 'pencil skirt',
					},
					{
						id: 92,
						category_id: 12,
						title: 'straight skirt',
					},
					{
						id: 93,
						category_id: 12,
						title: 'tiered skirt',
					},
					{
						id: 94,
						category_id: 12,
						title: 'tube skirt',
					},
					{
						id: 95,
						category_id: 12,
						title: 'tulip skirt',
					},
					{
						id: 96,
						category_id: 12,
						title: 'tutu skirt',
					},
					{
						id: 97,
						category_id: 12,
						title: 'wrap skirt',
					},
					// {
					// 	id: 98,
					// 	category_id: 13,
					// 	title: 'ankle boots',
					// },
					// {
					// 	id: 99,
					// 	category_id: 13,
					// 	title: 'boots',
					// },
					{
						id: 100,
						category_id: 13,
						title: 'flats',
					},
					// {
					// 	id: 101,
					// 	category_id: 13,
					// 	title: 'heeled sandals',
					// },
					{
						id: 102,
						category_id: 13,
						title: 'mules',
					},
					{
						id: 103,
						category_id: 13,
						title: 'pumps',
					},
					// {
					// 	id: 104,
					// 	category_id: 13,
					// 	title: 'sandals',
					// },
					{
						id: 105,
						category_id: 13,
						title: 'sneakers',
					},
					{
						id: 106,
						category_id: 13,
						title: 'wedges',
					},
					{
						id: 107,
						category_id: 14,
						title: 'backpack',
					},
					{
						id: 108,
						category_id: 14,
						title: 'clutch',
					},
					{
						id: 109,
						category_id: 14,
						title: 'crossbody',
					},
					{
						id: 110,
						category_id: 14,
						title: 'handbag',
					},
					{
						id: 111,
						category_id: 14,
						title: 'tote',
					},
					{
						id: 112,
						category_id: 14,
						title: 'waist bag',
					},
					// {
					// 	id: 113,
					// 	category_id: 14,
					// 	title: 'wallet',
					// },
					// {
					// 	id: 114,
					// 	category_id: 15,
					// 	title: 'bracelet',
					// },
					// {
					// 	id: 115,
					// 	category_id: 15,
					// 	title: 'earrings',
					// },
					// {
					// 	id: 116,
					// 	category_id: 15,
					// 	title: 'hair accessory',
					// },
					// {
					// 	id: 117,
					// 	category_id: 15,
					// 	title: 'hats',
					// },
					// {
					// 	id: 118,
					// 	category_id: 15,
					// 	title: 'necklace',
					// },
					// {
					// 	id: 119,
					// 	category_id: 15,
					// 	title: 'scarf',
					// },
					// {
					// 	id: 120,
					// 	category_id: 15,
					// 	title: 'shawl',
					// },
					// {
					// 	id: 121,
					// 	category_id: 15,
					// 	title: 'eyewear',
					// },
					// {
					// 	id: 122,
					// 	category_id: 15,
					// 	title: 'watch',
					// },
					// {
					// 	id: 123,
					// 	category_id: 15,
					// 	title: 'ring',
					// },
					// {
					// 	id: 124,
					// 	category_id: 15,
					// 	title: 'socks',
					// },
					// {
					// 	id: 125,
					// 	category_id: 15,
					// 	title: 'belt',
					// },
					// {
					// 	id: 126,
					// 	category_id: 15,
					// 	title: 'Mask',
					// },
					// {
					// 	id: 127,
					// 	category_id: 15,
					// 	title: 'Hijab',
					// },
				] as CategoryInterface[]
				categories && categories.map(async category => {
					await this.CategoryRepository.insert(
						category.id,
						category.category_id,
						category.title,
						category.description,
						[],
						[],
						[],
						transaction)
					return this
				})

				// 2.5 Update =======================================================================
				await this.CategoryRepository.insert(125, 4, 'Belt', '', [3], [48], [102], transaction)

				await this.upsert([
					{
						id: 16,
						icon_title: 'product-dress-cold-shoulder-dress',
					},
					{
						id: 17,
						icon_title: 'product-dress-fit-and-flare-dress',
					},
					{
						id: 18,
						icon_title: 'product-tops-halter',
					},
					{
						id: 19,
						icon_title: 'product-dress-off-shoulder-dress',
					},
					{
						id: 20,
						icon_title: 'product-dress-pinafore-dress',
					},
					{
						id: 21,
						icon_title: 'product-dress-qipao',
					},
					{
						id: 22,
						icon_title: 'product-dress-sheath-dress',
					},
					{
						id: 23,
						icon_title: 'product-dress-shift-dress',
					},
					{
						id: 24,
						icon_title: 'product-dress-shirt-dress',
					},
					{
						id: 25,
						icon_title: 'product-dress-strap-dress',
					},
					{
						id: 26,
						icon_title: 'product-dress-strapless-dress',
					},
					{
						id: 27,
						icon_title: 'product-dress-trapeze-dress',
					},
					{
						id: 28,
						icon_title: 'product-tops-tunic-top',
					},
					{
						id: 29,
						icon_title: 'product-dress-wrap-dress',
					},
					{
						id: 30,
						icon_title: 'product-one-pieces-bodysuit',
					},
					{
						id: 31,
						icon_title: 'product-one-pieces-jumpsuit',
					},
					{
						id: 32,
						icon_title: 'product-one-pieces-overalls',
					},
					{
						id: 33,
						icon_title: 'product-one-pieces-playsuit',
					},
					{
						id: 34,
						icon_title: 'product-one-pieces-romper',
					},
					{
						id: 35,
						icon_title: 'product-tops-asymmetric-top',
					},
					{
						id: 36,
						icon_title: 'product-tops-blouse',
					},
					{
						id: 37,
						icon_title: 'product-tops-bralette',
					},
					{
						id: 38,
						icon_title: 'product-tops-camisole',
					},
					{
						id: 39,
						icon_title: 'product-tops-crop-top',
					},
					{
						id: 40,
						icon_title: 'product-tops-halter',
					},
					{
						id: 41,
						icon_title: 'product-tops-jumper',
					},
					{
						id: 42,
						icon_title: 'product-tops-off-shoulder-top',
					},
					{
						id: 43,
						icon_title: 'product-tops-peplum',
					},
					{
						id: 44,
						icon_title: 'product-tops-shirt-top',
					},
					{
						id: 45,
						icon_title: 'product-tops-t-shirt',
					},
					{
						id: 46,
						icon_title: 'product-tops-tank-top',
					},
					{
						id: 47,
						icon_title: 'product-tops-tube-top',
					},
					{
						id: 48,
						icon_title: 'product-tops-tunic-top',
					},
					{
						id: 49,
						icon_title: 'product-outerwears-blazer',
					},
					{
						id: 50,
						icon_title: 'product-outerwears-bolero',
					},
					{
						id: 51,
						icon_title: 'product-outerwears-cape',
					},
					{
						id: 52,
						icon_title: 'product-outerwears-cardigan',
					},
					{
						id: 53,
						icon_title: 'product-outerwears-coat',
					},
					{
						id: 54,
						icon_title: 'product-outerwears-hoodie',
					},
					{
						id: 55,
						icon_title: 'product-outerwears-jacket',
					},
					{
						id: 56,
						icon_title: 'product-outerwears-kimono',
					},
					{
						id: 57,
						icon_title: 'product-outerwears-parka',
					},
					{
						id: 58,
						icon_title: 'product-outerwears-poncho',
					},
					{
						id: 59,
						icon_title: 'product-outerwears-puffer-jacket',
					},
					{
						id: 60,
						icon_title: 'product-outerwears-vest',
					},
					{
						id: 61,
						icon_title: 'product-pants-bootcut-pants',
					},
					{
						id: 62,
						icon_title: 'product-pants-capri-pants',
					},
					{
						id: 63,
						icon_title: 'product-pants-cargo-pants',
					},
					{
						id: 64,
						icon_title: 'product-pants-cigarette-pants',
					},
					{
						id: 65,
						icon_title: 'product-pants-culottes',
					},
					{
						id: 66,
						icon_title: 'product-pants-flared-pants',
					},
					{
						id: 67,
						icon_title: 'product-pants-joggers',
					},
					{
						id: 68,
						icon_title: 'product-pants-khaki-pants',
					},
					{
						id: 69,
						icon_title: 'product-pants-leggings',
					},
					{
						id: 70,
						icon_title: 'product-pants-palazzo-pants',
					},
					{
						id: 71,
						icon_title: 'product-pants-skinny-pants',
					},
					{
						id: 72,
						icon_title: 'product-pants-straight-pants',
					},
					{
						id: 73,
						icon_title: 'product-shorts-bermuda-shorts',
					},
					{
						id: 74,
						icon_title: 'product-shorts-denim-shorts',
					},
					{
						id: 75,
						icon_title: 'product-shorts-runner-shorts',
					},
					{
						id: 76,
						icon_title: 'product-shots-skort',
					},
					{
						id: 77,
						icon_title: 'product-jeans-ankle-jeans',
					},
					{
						id: 78,
						icon_title: 'product-jeans-bootcut-jeans',
					},
					{
						id: 79,
						icon_title: 'product-jeans-boyfriend-jeans',
					},
					{
						id: 80,
						icon_title: 'product-jeans-cropped-jeans',
					},
					{
						id: 81,
						icon_title: 'product-jeans-flared-jeans',
					},
					{
						id: 82,
						icon_title: 'product-jeans-ripped-jeans',
					},
					{
						id: 83,
						icon_title: 'product-jeans-skinny-jeans',
					},
					{
						id: 84,
						icon_title: 'product-jeans-straight-jeans',
					},
					{
						id: 85,
						icon_title: 'product-skirts-a-line-skirt',
					},
					{
						id: 86,
						icon_title: 'product-skirts-asymmetric-skirt',
					},
					{
						id: 87,
						icon_title: 'product-skirts-bubble-skirt',
					},
					{
						id: 88,
						icon_title: 'product-skirts-denim-skirt',
					},
					{
						id: 89,
						icon_title: 'product-skirts-flared-skirt',
					},
					{
						id: 90,
						icon_title: 'product-skirt-full-skirt',
					},
					{
						id: 91,
						icon_title: 'product-skirt-pencil-skirt',
					},
					{
						id: 92,
						icon_title: 'product-skirt-straight-skirt',
					},
					{
						id: 93,
						icon_title: 'product-skirt-tiered-skirt',
					},
					{
						id: 94,
						icon_title: 'product-skirt-tube-skirt',
					},
					{
						id: 95,
						icon_title: 'product-skirt-tulip-skirt',
					},
					{
						id: 96,
						icon_title: 'product-skirt-tutu-skirt',
					},
					{
						id: 97,
						icon_title: 'product-skirt-wrap-skirt',
					},
					// {
					// 	id: 98,
					// 	icon_title: 'product-shoes-ankle-boots',
					// },
					// {
					// 	id: 99,
					// 	icon_title: 'product-shoes-boots',
					// },
					{
						id: 100,
						icon_title: 'product-shoes-flats',
					},
					// {
					// 	id: 101,
					// 	icon_title: 'product-shoes-heeled-sandals',
					// },
					{
						id: 102,
						icon_title: 'product-shoes-mules',
					},
					{
						id: 103,
						icon_title: 'product-shoes-pumps',
					},
					// {
					// 	id: 104,
					// 	icon_title: 'product-shoes-sandals',
					// },
					{
						id: 105,
						icon_title: 'product-shoes-sneakers',
					},
					{
						id: 106,
						icon_title: 'product-shoes-wedges',
					},
					{
						id: 107,
						icon_title: 'product-bags-backpack',
					},
					{
						id: 108,
						icon_title: 'product-bags-clutch',
					},
					{
						id: 109,
						icon_title: 'product-bags-crossbody',
					},
					{
						id: 110,
						icon_title: 'product-bags-handbag',
					},
					{
						id: 111,
						icon_title: 'product-bags-tote',
					},
					{
						id: 112,
						icon_title: 'product-bags-waist-bag',
					},
					// {
					// 	id: 113,
					// 	icon_title: 'product-bags-wallet',
					// },
					// {
					// 	id: 114,
					// 	icon_title: 'product-accessories-bracelet',
					// },
					// {
					// 	id: 115,
					// 	icon_title: 'product-accessories-earrings',
					// },
					// {
					// 	id: 116,
					// 	icon_title: 'product-accessories-hair-accessory',
					// },
					// {
					// 	id: 117,
					// 	icon_title: 'product-accessories-hats',
					// },
					// {
					// 	id: 118,
					// 	icon_title: 'product-accessories-necklace',
					// },
					// {
					// 	id: 119,
					// 	icon_title: 'product-accessories-scarf',
					// },
					// {
					// 	id: 120,
					// 	icon_title: 'product-accessories-shawl',
					// },
					// {
					// 	id: 121,
					// 	icon_title: 'product-accessories-eyewear',
					// },
					// {
					// 	id: 122,
					// 	icon_title: 'product-accessories-watch',
					// },
					// {
					// 	id: 123,
					// 	icon_title: 'product-accessories-ring',
					// },
					// {
					// 	id: 124,
					// 	icon_title: 'product-accessories-socks',
					// },
					// {
					// 	id: 125,
					// 	icon_title: 'product-accessories-belt',
					// },
					// {
					// 	id: 126,
					// 	icon_title: 'product-accessories-mask',
					// },
				], transaction)

				// 2.7 Update =======================================================================
				await this.CategoryRepository.insert(128, 2, 'Sandals', '', [2], [3], [6, 102], transaction)
				await this.CategoryRepository.insert(129, 2, 'Boots', '', [2], [3], [6, 102], transaction)
				await this.CategoryRepository.insert(130, 2, 'Socks', '', [2], [3], [102], transaction)
				await this.CategoryRepository.insert(131, 3, 'Wallet', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(132, 4, 'Eyewear', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(133, 4, 'Headwear', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(134, 4, 'Neckwear', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(135, 4, 'Hijab', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(136, 4, 'Jewelry', '', [3], [4], [10, 11, 12], transaction)
				await this.CategoryRepository.insert(137, 4, 'Watch', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(138, 4, 'Mask', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(139, 4, 'Brooch', '', [3], [4], [102], transaction)
				await this.CategoryRepository.insert(260, 4, 'Makeup', '', [3], [4], [133], transaction)
				await this.CategoryRepository.insert(228, 1, 'Set', '', [1, 4, 24], [1], [1, 2, 3, 7, 8, 102], transaction)
				await this.CategoryRepository.insert(247, 1, 'Man', '', [1, 4], [1], [1, 2, 3, 7, 8, 102], transaction)

				await this.upsert([
					{
						id: 140,
						category_id: 5,
						title: 'Bodycon',
					},
					{
						id: 141,
						category_id: 5,
						title: 'Sun Dress',
					},
					{
						id: 142,
						category_id: 5,
						title: 'Drapery Dress',
					},
					{
						id: 143,
						category_id: 6,
						title: 'Pyjamas',
					},
					{
						id: 144,
						category_id: 7,
						title: 'Sweatshirt',
					},
					{
						id: 145,
						category_id: 8,
						title: 'Utility Jacket',
					},
					{
						id: 146,
						category_id: 11,
						title: 'Mom Jeans',
					},
					// {
					// 	id: 147,
					// 	category_id: 11,
					// 	title: 'Mom Jeans',
					// },
					{
						id: 148,
						category_id: 11,
						title: 'Cullote Jeans',
					},
					{
						id: 149,
						category_id: 12,
						title: 'Mermaid Skirt',
					},
					{
						id: 150,
						category_id: 14,
						title: 'Baguette',
					},
					{
						id: 151,
						category_id: 14,
						title: 'Shoulder',
					},
					{
						id: 152,
						category_id: 14,
						title: 'Sling',
					},
					{
						id: 153,
						category_id: 14,
						title: 'Drawstring',
					},
					{
						id: 154,
						category_id: 14,
						title: 'Messenger',
					},
					{
						id: 155,
						category_id: 14,
						title: 'Saddle',
					},
					{
						id: 156,
						category_id: 14,
						title: 'Duffle',
					},
					{
						id: 157,
						category_id: 14,
						title: 'Bucket',
					},
					{
						id: 158,
						category_id: 14,
						title: 'Envelope',
					},
					{
						id: 159,
						category_id: 14,
						title: 'Hobo',
					},
					{
						id: 160,
						category_id: 14,
						title: 'Satchel',
					},
					{
						id: 161,
						category_id: 128,
						title: 'Flip Flops',
					},
					{
						id: 162,
						category_id: 128,
						title: 'Heeled Sandals',
					},
					{
						id: 163,
						category_id: 128,
						title: 'Wedge',
					},
					// {
					// 	id: 164,
					// 	category_id: 128,
					// 	title: 'Sliders',
					// },
					// {
					// 	id: 165,
					// 	category_id: 128,
					// 	title: 'Gladiator',
					// },
					{
						id: 166,
						category_id: 128,
						title: 'Sport',
					},
					{
						id: 167,
						category_id: 128,
						title: 'Strap',
					},
					{
						id: 168,
						category_id: 128,
						title: 'Lace Up',
					},
					{
						id: 169,
						category_id: 128,
						title: 'Platform',
					},
					{
						id: 170,
						category_id: 128,
						title: 'Slides',
					},
					{
						id: 171,
						category_id: 128,
						title: 'Slippers',
					},
					{
						id: 172,
						category_id: 129,
						title: 'Ankle',
					},
					{
						id: 173,
						category_id: 129,
						title: 'Chelsea',
					},
					{
						id: 174,
						category_id: 129,
						title: 'Biker',
					},
					{
						id: 175,
						category_id: 129,
						title: 'Cowboy',
					},
					// {
					// 	id: 176,
					// 	category_id: 129,
					// 	title: 'Bootie',
					// },
					{
						id: 177,
						category_id: 129,
						title: 'High',
					},
					{
						id: 178,
						category_id: 13,
						title: 'Slip Ons',
					},
					{
						id: 179,
						category_id: 13,
						title: 'Oxfords',
					},
					{
						id: 180,
						category_id: 13,
						title: 'Brogues',
					},
					{
						id: 181,
						category_id: 13,
						title: 'Boats',
					},
					{
						id: 182,
						category_id: 13,
						title: 'Mary Janes',
					},
					{
						id: 183,
						category_id: 13,
						title: 'Heeled Mules',
					},
					{
						id: 184,
						category_id: 13,
						title: 'Espadrilles',
					},
					{
						id: 185,
						category_id: 13,
						title: 'Platform',
					},
					{
						id: 186,
						category_id: 13,
						title: 'Loafers',
					},
					{
						id: 187,
						category_id: 130,
						title: 'Low',
					},
					{
						id: 188,
						category_id: 130,
						title: 'Medium',
					},
					{
						id: 189,
						category_id: 130,
						title: 'High',
					},
					{
						id: 190,
						category_id: 132,
						title: 'Sunglasses',
					},
					{
						id: 191,
						category_id: 132,
						title: 'Clear-glasses',
					},
					{
						id: 192,
						category_id: 133,
						title: 'Cap',
					},
					{
						id: 193,
						category_id: 133,
						title: 'Fedora',
					},
					{
						id: 194,
						category_id: 133,
						title: 'Bandana',
					},
					{
						id: 195,
						category_id: 133,
						title: 'Turban',
					},
					{
						id: 196,
						category_id: 133,
						title: 'Head Scarf',
					},
					{
						id: 197,
						category_id: 133,
						title: 'Straw',
					},
					{
						id: 198,
						category_id: 133,
						title: 'Boater',
					},
					{
						id: 199,
						category_id: 133,
						title: 'Beret',
					},
					{
						id: 200,
						category_id: 133,
						title: 'Beanie',
					},
					{
						id: 201,
						category_id: 133,
						title: 'Bucket',
					},
					{
						id: 202,
						category_id: 134,
						title: 'Scarf',
					},
					{
						id: 203,
						category_id: 134,
						title: 'Shawl',
					},
					{
						id: 204,
						category_id: 134,
						title: 'Pashmina',
					},
					{
						id: 205,
						category_id: 135,
						title: 'Pashmina Hijab',
					},
					{
						id: 206,
						category_id: 135,
						title: 'Sports',
					},
					{
						id: 207,
						category_id: 135,
						title: 'Square',
					},
					{
						id: 208,
						category_id: 133,
						title: 'Headband',
					},
					{
						id: 209,
						category_id: 136,
						title: 'Necklace',
					},
					{
						id: 210,
						category_id: 136,
						title: 'Earrings',
					},
					{
						id: 211,
						category_id: 136,
						title: 'Bracelet',
					},
					{
						id: 212,
						category_id: 136,
						title: 'Bangle',
					},
					{
						id: 213,
						category_id: 136,
						title: 'Ring',
					},
					{
						id: 214,
						category_id: 137,
						title: 'Analogue',
					},
					{
						id: 215,
						category_id: 137,
						title: 'Digital',
					},
					{
						id: 216,
						category_id: 138,
						title: 'Face Mask',
					},
					{
						id: 217,
						category_id: 138,
						title: '2-Ply',
					},
					{
						id: 218,
						category_id: 138,
						title: '3-Ply',
					},
					{
						id: 219,
						category_id: 139,
						title: 'Pin',
					},
					{
						id: 220,
						category_id: 131,
						title: 'Bi-fold',
					},
					{
						id: 221,
						category_id: 131,
						title: 'Tri-fold',
					},
					{
						id: 222,
						category_id: 131,
						title: 'Slim',
					},
					{
						id: 223,
						category_id: 131,
						title: 'Zippered',
					},
					{
						id: 224,
						category_id: 131,
						title: 'Large',
					},
					{
						id: 225,
						category_id: 131,
						title: 'Wristlet',
					},
					{
						id: 226,
						category_id: 131,
						title: 'Continental',
					},
					{
						id: 227,
						category_id: 131,
						title: 'Tall',
					},
					{
						id: 229,
						category_id: 228,
						title: 'Lounge Set',
					},
					{
						id: 230,
						category_id: 228,
						title: 'Pajama Set',
					},
					{
						id: 231,
						category_id: 228,
						title: 'Suit Set',
					},
					{
						id: 232,
						category_id: 125,
						title: 'Wide',
					},
					{
						id: 233,
						category_id: 125,
						title: 'Narrow',
					},
					{
						id: 234,
						category_id: 125,
						title: 'Braided',
					},
					{
						id: 235,
						category_id: 125,
						title: 'Buckle',
					},
					{
						id: 236,
						category_id: 125,
						title: 'D Ring',
					},
					{
						id: 237,
						category_id: 125,
						title: 'Knot',
					},
					{
						id: 238,
						category_id: 125,
						title: 'Corset',
					},
					{
						id: 239,
						category_id: 125,
						title: 'Obi',
					},
					{
						id: 240,
						category_id: 125,
						title: 'Suspender',
					},
					{
						id: 241,
						category_id: 125,
						title: 'Chain',
					},
					{
						id: 242,
						category_id: 125,
						title: 'Cinch',
					},
					{
						id: 243,
						category_id: 125,
						title: 'Twist',
					},
					{
						id: 244,
						category_id: 125,
						title: 'Sash',
					},
					// update May 20, 2021
					// Ini ke dress, harus nya kategori sepatu
					// {
					// 	id: 245,
					// 	category_id: 5,
					// 	title: 'Espadrilles',
					// },
					{
						id: 246,
						category_id: 13,
						title: 'Heels',
					},
					{
						id: 248,
						category_id: 247,
						title: 'T-Shirt',
					},
					{
						id: 249,
						category_id: 247,
						title: 'Long Sleeves T-Shirt',
					},
					{
						id: 250,
						category_id: 247,
						title: 'Shirt',
					},
					{
						id: 251,
						category_id: 247,
						title: 'Hoodie',
					},
					{
						id: 252,
						category_id: 247,
						title: 'Pants',
					},
					{
						id: 253,
						category_id: 247,
						title: 'Shorts',
					},
					{
						id: 254,
						category_id: 133,
						title: 'Hairclip',
					},
					{
						id: 255,
						category_id: 7,
						title: 'Sport Bra',
					},
					{
						id: 256,
						category_id: 134,
						title: 'Collar',
					},
					{
						id: 257,
						category_id: 133,
						title: 'Hat',
					},
					{
						id: 258,
						category_id: 133,
						title: 'Hair Accessory',
					},
					{
						id: 259,
						category_id: 138,
						title: 'Maskchain',
					},
					{
						id: 261,
						category_id: 260,
						title:  'Lips',
					},
				], transaction)
			})
		}
	}
}

export default new CategoryManager()
