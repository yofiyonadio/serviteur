import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import TagRepository from 'app/repositories/tag'

// import IntegrationService from 'app/services/integration'
import { TagInterface } from 'energie/app/interfaces'

import DEFAULTS from 'utils/constants/default'
// import { ERROR_CODES } from 'app/models/error'


class TagManager extends ManagerModel {

	static __displayName = 'TagManager'

	protected TagRepository: TagRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			TagRepository,
		], async () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.make(transaction)
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async make(transaction: EntityManager) {
		if(DEFAULTS.SYNC) {
			await this.TagRepository.insert(1, null, 'Fit/Cut (Top)', '', transaction)
			await this.TagRepository.insert(2, null, 'Fit/Cut (Bottom)', '', transaction)
			await this.TagRepository.insert(3, null, 'Length', '', transaction)
			await this.TagRepository.insert(4, null, 'Rise', '', transaction)
			await this.TagRepository.insert(5, null, 'Jeans Style', '', transaction)
			await this.TagRepository.insert(6, null, 'Heels', '', transaction)
			await this.TagRepository.insert(7, null, 'Detail', '', transaction)
			await this.TagRepository.insert(8, null, 'Pattern', '', transaction)
			await this.TagRepository.insert(9, null, 'Fabric', '', transaction)
			await this.TagRepository.insert(10, null, 'Jewelry Style', '', transaction)
			await this.TagRepository.insert(11, null, 'Jewelry Tone', '', transaction)
			await this.TagRepository.insert(12, null, 'Accessory Material', '', transaction)
			await this.TagRepository.insert(13, null, 'Style', '', transaction)
			await this.TagRepository.insert(98, null, 'Belt Thickness', '', transaction)
			await this.TagRepository.insert(102, null, 'Material', '', transaction)
			await this.TagRepository.insert(111, null, 'Belt Type', '', transaction)
			await this.TagRepository.insert(133, null, 'Skin Tone', '', transaction)

			const tags =  [{
				id: 14,
				tag_id: 1,
				title: 'Tight',
			},
			{
				id: 15,
				tag_id: 1,
				title: 'Fitted',
			},
			{
				id: 16,
				tag_id: 1,
				title: 'Straight',
			},
			{
				id: 17,
				tag_id: 1,
				title: 'Loose',
			},
			{
				id: 18,
				tag_id: 1,
				title: 'Oversized',
			},
			{
				id: 19,
				tag_id: 2,
				title: 'Tight',
			},
			{
				id: 20,
				tag_id: 2,
				title: 'Fitted',
			},
			{
				id: 21,
				tag_id: 2,
				title: 'Straight',
			},
			{
				id: 22,
				tag_id: 2,
				title: 'Loose',
			},
			{
				id: 23,
				tag_id: 2,
				title: 'Oversized',
			},
			{
				id: 24,
				tag_id: 3,
				title: 'Mini',
			},
			{
				id: 25,
				tag_id: 3,
				title: 'Knee Length',
			},
			{
				id: 26,
				tag_id: 3,
				title: 'Midi',
			},
			{
				id: 27,
				tag_id: 3,
				title: 'Maxi',
			},
			{
				id: 28,
				tag_id: 4,
				title: 'Low',
			},
			{
				id: 29,
				tag_id: 4,
				title: 'Mid',
			},
			{
				id: 30,
				tag_id: 4,
				title: 'High',
			},
			{
				id: 31,
				tag_id: 5,
				title: 'Skinny',
			},
			{
				id: 32,
				tag_id: 5,
				title: 'Straight',
			},
			{
				id: 33,
				tag_id: 5,
				title: 'Bootcut',
			},
			{
				id: 34,
				tag_id: 5,
				title: 'Cropped',
			},
			{
				id: 35,
				tag_id: 6,
				title: 'Low(3-5 cm)',
			},
			{
				id: 36,
				tag_id: 6,
				title: 'Mid(5-10 cm)',
			},
			{
				id: 37,
				tag_id: 6,
				title: 'High(10-15 cm)',
			},
			{
				id: 38,
				tag_id: 6,
				title: 'None',
			},
			{
				id: 39,
				tag_id: 7,
				title: 'Fringe',
			},
			{
				id: 40,
				tag_id: 7,
				title: 'Pleat',
			},
			{
				id: 41,
				tag_id: 7,
				title: 'Embroidery',
			},
			{
				id: 42,
				tag_id: 7,
				title: 'Ruffle',
			},
			{
				id: 43,
				tag_id: 7,
				title: 'Patch',
			},
			{
				id: 44,
				tag_id: 7,
				title: 'Screen Print',
			},
			{
				id: 45,
				tag_id: 7,
				title: 'Lace',
			},
			{
				id: 46,
				tag_id: 7,
				title: 'Embellishment',
			},
			{
				id: 47,
				tag_id: 7,
				title: 'Tassel',
			},
			{
				id: 48,
				tag_id: 7,
				title: 'Sequin',
			},
			{
				id: 49,
				tag_id: 7,
				title: 'Transparency',
			},
			{
				id: 50,
				tag_id: 7,
				title: 'Simple',
			},
			{
				id: 51,
				tag_id: 8,
				title: 'Animal Print',
			},
			{
				id: 52,
				tag_id: 8,
				title: 'Paisley',
			},
			{
				id: 53,
				tag_id: 8,
				title: 'Plaid/Tartan',
			},
			{
				id: 54,
				tag_id: 8,
				title: 'Polkadots',
			},
			{
				id: 55,
				tag_id: 8,
				title: 'Stripes',
			},
			{
				id: 56,
				tag_id: 8,
				title: 'Novelty Print',
			},
			{
				id: 57,
				tag_id: 8,
				title: 'Checked',
			},
			{
				id: 58,
				tag_id: 8,
				title: 'Traditional Print',
			},
			{
				id: 59,
				tag_id: 8,
				title: 'Solid',
			},
			{
				id: 60,
				tag_id: 102,
				title: 'Faux Fur',
			},
			{
				id: 61,
				tag_id: 102,
				title: 'Wool',
			},
			{
				id: 62,
				tag_id: 102,
				title: 'Linen',
			},
			{
				id: 63,
				tag_id: 102,
				title: 'Polyester',
			},
			{
				id: 64,
				tag_id: 102,
				title: 'Faux Leather',
			},
			{
				id: 65,
				tag_id: 102,
				title: 'Leather',
			},
			{
				id: 66,
				tag_id: 102,
				title: 'Cotton',
			},
			{
				id: 67,
				tag_id: 102,
				title: 'Velvet',
			},
			{
				id: 68,
				tag_id: 102,
				title: 'Nylon',
			},
			{
				id: 69,
				tag_id: 102,
				title: 'Rayon',
			},
			{
				id: 70,
				tag_id: 102,
				title: 'Viscose',
			},
			{
				id: 71,
				tag_id: 102,
				title: 'Satin',
			},
			{
				id: 72,
				tag_id: 102,
				title: 'Denim',
			},
			{
				id: 73,
				tag_id: 102,
				title: 'Scuba',
			},
			{
				id: 74,
				tag_id: 10,
				title: 'Minimalist',
			},
			{
				id: 75,
				tag_id: 10,
				title: 'Classic',
			},
			{
				id: 76,
				tag_id: 10,
				title: 'Statement',
			},
			{
				id: 77,
				tag_id: 11,
				title: 'Gold',
			},
			{
				id: 78,
				tag_id: 11,
				title: 'Silver',
			},
			{
				id: 79,
				tag_id: 11,
				title: 'Rose Gold',
			},
			{
				id: 80,
				tag_id: 12,
				title: 'Silver',
			},
			{
				id: 81,
				tag_id: 12,
				title: 'Gold',
			},
			{
				id: 82,
				tag_id: 12,
				title: 'Platinum',
			},
			{
				id: 83,
				tag_id: 12,
				title: 'Base Metal',
			},
			{
				id: 84,
				tag_id: 12,
				title: 'Natural',
			},
			{
				id: 85,
				tag_id: 12,
				title: 'Synthetic',
			},
			{
				id: 86,
				tag_id: 12,
				title: 'Regenerated',
			},
			{
				id: 87,
				tag_id: 12,
				title: 'Other',
			},
			{
				id: 88,
				tag_id: 13,
				title: 'Bohemian',
			},
			{
				id: 89,
				tag_id: 13,
				title: 'Bombshell',
			},
			{
				id: 90,
				tag_id: 13,
				title: 'Casual',
			},
			{
				id: 91,
				tag_id: 13,
				title: 'Chic',
			},
			{
				id: 92,
				tag_id: 13,
				title: 'Classic',
			},
			{
				id: 93,
				tag_id: 13,
				title: 'Demure',
			},
			{
				id: 94,
				tag_id: 13,
				title: 'Eclectic',
			},
			{
				id: 95,
				tag_id: 13,
				title: 'Flamboyant',
			},
			{
				id: 96,
				tag_id: 13,
				title: 'Rebel',
			},
			{
				id: 97,
				tag_id: 13,
				title: 'Sporty',
			},
			{
				id: 99,
				tag_id: 98,
				title: 'Skinny',
			},
			{
				id: 100,
				tag_id: 98,
				title: 'Medium',
			},
			{
				id: 101,
				tag_id: 98,
				title: 'Big',
			},
			{
				id: 103,
				tag_id: 102,
				title: 'Corduroy',
			},
			{
				id: 104,
				tag_id: 102,
				title: 'Organza',
			},
			{
				id: 105,
				tag_id: 102,
				title: 'Twill',
			},
			{
				id: 106,
				tag_id: 102,
				title: 'Strech',
			},
			{
				id: 107,
				tag_id: 102,
				title: 'Sanwosh',
			},
			{
				id: 108,
				tag_id: 102,
				title: 'Chino',
			},
			{
				id: 109,
				tag_id: 102,
				title: 'Crepe',
			},
			{
				id: 110,
				tag_id: 102,
				title: 'Spandex',
			},
			{
				id: 112,
				tag_id: 111,
				title: 'Buckle belt',
			},
			{
				id: 113,
				tag_id: 111,
				title: 'Horseshoe buckle belt',
			},
			{
				id: 114,
				tag_id: 111,
				title: 'D Ring belt',
			},
			{
				id: 115,
				tag_id: 111,
				title: 'Skinny Knot belt',
			},
			{
				id: 116,
				tag_id: 111,
				title: 'Suspender belt',
			},
			{
				id: 117,
				tag_id: 111,
				title: 'Cummerbund',
			},
			{
				id: 118,
				tag_id: 111,
				title: 'Military belt',
			},
			{
				id: 119,
				tag_id: 111,
				title: 'Metal belt / Chain belt',
			},
			{
				id: 120,
				tag_id: 111,
				title: 'Hip belt',
			},
			{
				id: 121,
				tag_id: 111,
				title: 'Yoke belt',
			},
			{
				id: 122,
				tag_id: 111,
				title: 'Cinch belt',
			},
			{
				id: 123,
				tag_id: 111,
				title: 'Lace-up belt',
			},
			{
				id: 124,
				tag_id: 111,
				title: 'Corset Style belt',
			},
			{
				id: 125,
				tag_id: 111,
				title: 'Sash belt',
			},
			{
				id: 126,
				tag_id: 111,
				title: 'Obi belt',
			},
			{
				id: 127,
				tag_id: 111,
				title: 'Braided belt',
			},
			{
				id: 128,
				tag_id: 111,
				title: 'Twist belt',
			},
			{
				id: 129,
				tag_id: 111,
				title: 'Bow belt',
			},
			{
				id: 130,
				tag_id: 111,
				title: 'Garter belt',
			},
			{
				id: 131,
				tag_id: 111,
				title: 'Peplum belt',
			},
			{
				id: 132,
				tag_id: 8,
				title: 'Floral',
			},
			{
				id: 134,
				tag_id: 133,
				title: 'Light',
			},
			{
				id: 135,
				tag_id: 133,
				title: 'Medium',
			},
			{
				id: 136,
				tag_id: 133,
				title: 'Olive',
			},
			{
				id: 137,
				tag_id: 133,
				title: 'Dark',
			}] as TagInterface[]

			tags && tags.map(async tag => {
				await this.TagRepository.insert(tag.id, tag.tag_id, tag.title, null, transaction)
			})

			return this
		}
	}

}

export default new TagManager()
