import {
	ManagerModel,
} from 'app/models'
import { Connection } from 'typeorm'

import MasterCategoryManager from './_category'
import MasterColorManager from './_color'
import MasterCollectionManager from './_collection'
import MasterLocationManager from './_location'
import MasterMeasurementManager from './_measurement'
import MasterQuestionManager from './_question'
import MasterSizeManager from './_size'
import MasterTagManager from './_tag'


class MasterManager extends ManagerModel {

	static __displayName = 'MasterManager'

	async initialize(connection: Connection) {
		return super.initialize(connection, [
		], async () => {
			await MasterCategoryManager.initialize(connection)
			await MasterColorManager.initialize(connection)
			await MasterLocationManager.initialize(connection)
			await MasterMeasurementManager.initialize(connection)
			await MasterQuestionManager.initialize(connection)
			await MasterSizeManager.initialize(connection)
			await MasterTagManager.initialize(connection)
			await MasterCollectionManager.initialize(connection)
		})
	}

	// tslint:disable-next-line:member-ordering
	get category(): typeof MasterCategoryManager {
		return MasterCategoryManager
	}

	// tslint:disable-next-line:member-ordering
	get color(): typeof MasterColorManager {
		return MasterColorManager
	}

	get collection(): typeof MasterCollectionManager {
		return MasterCollectionManager
	}

	// tslint:disable-next-line:member-ordering
	get location(): typeof MasterLocationManager {
		return MasterLocationManager
	}

	// tslint:disable-next-line:member-ordering
	get measurement(): typeof MasterMeasurementManager {
		return MasterMeasurementManager
	}

	// tslint:disable-next-line:member-ordering
	get question(): typeof MasterQuestionManager {
		return MasterQuestionManager
	}

	// tslint:disable-next-line:member-ordering
	get size(): typeof MasterSizeManager {
		return MasterSizeManager
	}

	// tslint:disable-next-line:member-ordering
	get tag(): typeof MasterTagManager {
		return MasterTagManager
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================

}

export default new MasterManager()
