import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import SizeRepository from 'app/repositories/size'

// import IntegrationService from 'app/services/integration'
import { SizeInterface } from 'energie/app/interfaces'

import DEFAULTS from 'utils/constants/default'
// import { ERROR_CODES } from 'app/models/error'


class SizeManager extends ManagerModel {

	static __displayName = 'SizeManager'

	protected SizeRepository: SizeRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			SizeRepository,
		], () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.make(transaction)
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async getByIdOrIds(
		ids: number[] | number,
		transaction: EntityManager,
	) {
		if (Array.isArray(ids)) {
			return this.SizeRepository.getByIds(ids, transaction)
		} else {
			return this.SizeRepository.getByIds([ids], transaction)[0]
		}
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ============================
	private async make(transaction: EntityManager) {
		if(DEFAULTS.SYNC) {
			await this.SizeRepository.insert(1, null, 'Alphabet', '', transaction) // clothing
			await this.SizeRepository.insert(2, null, 'Jeans Waist', '', transaction) // clothing
			await this.SizeRepository.insert(3, null, 'Footwear', '', transaction)
			await this.SizeRepository.insert(4, null, 'General', '', transaction)
			await this.SizeRepository.insert(48, null, 'Belt', '', transaction)

			const sizes = [
				{
					id: 5,
					size_id: 1,
					title: 'XXS',
				},
				{
					id: 6,
					size_id: 1,
					title: 'XS',
				},
				{
					id: 7,
					size_id: 1,
					title: 'S',
				},
				{
					id: 8,
					size_id: 1,
					title: 'M',
				},
				{
					id: 9,
					size_id: 1,
					title: 'L',
				},
				{
					id: 10,
					size_id: 1,
					title: 'XL',
				},
				{
					id: 11,
					size_id: 1,
					title: 'XXL',
				},
				{
					id: 12,
					size_id: 1,
					title: '3L',
				},
				{
					id: 13,
					size_id: 1,
					title: '4L',
				},
				{
					id: 14,
					size_id: 1,
					title: 'All Size',
				},
				{
					id: 15,
					size_id: 2,
					title: '24',
				},
				{
					id: 16,
					size_id: 2,
					title: '25',
				},
				{
					id: 17,
					size_id: 2,
					title: '26',
				},
				{
					id: 18,
					size_id: 2,
					title: '27',
				},
				{
					id: 19,
					size_id: 2,
					title: '28',
				},
				{
					id: 20,
					size_id: 2,
					title: '29',
				},
				{
					id: 21,
					size_id: 2,
					title: '30',
				},
				{
					id: 22,
					size_id: 2,
					title: '31',
				},
				{
					id: 23,
					size_id: 2,
					title: '32',
				},
				{
					id: 24,
					size_id: 2,
					title: '33',
				},
				{
					id: 25,
					size_id: 2,
					title: '34',
				},
				{
					id: 26,
					size_id: 2,
					title: '35',
				},
				{
					id: 27,
					size_id: 2,
					title: '36',
				},
				{
					id: 28,
					size_id: 3,
					title: 'EU 35.5',
					description: 'EU 35.5 / US 4.5 / UK 3',
				},
				{
					id: 29,
					size_id: 3,
					title: 'EU 36',
					description: 'EU 36 / US 5 / UK 3.5',
				},
				{
					id: 30,
					size_id: 3,
					title: 'EU 36.5',
					description: 'EU 36.5 / US 5.5 / UK 4',
				},
				{
					id: 31,
					size_id: 3,
					title: 'EU 37',
					description: 'EU 37 / US 6 / UK 4.5',
				},
				{
					id: 32,
					size_id: 3,
					title: 'EU 37.5',
					description: 'EU 37.5 / US 6.5 / UK 5',
				},
				{
					id: 33,
					size_id: 3,
					title: 'EU 38',
					description: 'EU 38 / US 7 / UK 5.5',
				},
				{
					id: 34,
					size_id: 3,
					title: 'EU 38.5',
					description: 'EU 38.5 / US 7.5 / UK 6',
				},
				{
					id: 35,
					size_id: 3,
					title: 'EU 39',
					description: 'EU 39 / US 8 / UK 6.5',
				},
				{
					id: 36,
					size_id: 3,
					title: 'EU 39.5',
					description: 'EU 39.5 / US 8.5 / UK 7',
				},
				{
					id: 37,
					size_id: 3,
					title: 'EU 40',
					description: 'EU 40 / US 9 / UK 7.5',
				},
				{
					id: 38,
					size_id: 3,
					title: 'EU 40.5',
					description: 'EU 40.5 / US 9.5 / UK 8',
				},
				{
					id: 39,
					size_id: 3,
					title: 'EU 41',
					description: 'EU 41 / US 10 / UK 8.5',
				},
				{
					id: 40,
					size_id: 3,
					title: 'EU 41.5',
					description: 'EU 41.5 / US 10.5 / UK 9',
				},
				{
					id: 41,
					size_id: 3,
					title: 'EU 42',
					description: 'EU 42 / US 11 / UK 9.5',
				},
				{
					id: 42,
					size_id: 3,
					title: 'EU 42.5',
					description: 'EU 42.5 / US 11.5 / UK 10',
				},
				{
					id: 43,
					size_id: 3,
					title: 'EU 43',
					description: 'EU 43 / US 12 / UK 10.5',
				},
				{
					id: 44,
					size_id: 4,
					title: 'All Size',
				},
				{
					id: 45,
					size_id: 1,
					title: '5L',
				},
				{
					id: 46,
					size_id: 2,
					title: '37',
				},
				{
					id: 47,
					size_id: 2,
					title: '38',
				},
				{
					id: 49,
					size_id: 48,
					title: 'S',
				},
				{
					id: 50,
					size_id: 48,
					title: 'M',
				},
				{
					id: 51,
					size_id: 48,
					title: 'L',
				},
				{
					id: 52,
					size_id: 48,
					title: 'S/M',
				},
				{
					id: 53,
					size_id: 48,
					title: 'M/L',
				},
				{
					id: 54,
					size_id: 2,
					title: 'All Size',
				},
				{
					id: 55,
					size_id: 4,
					title: 'One Size',
				},
				{
					id: 56,
					size_id: 48,
					title: 'One Size',
				},
				{
					id: 57,
					size_id: 3,
					title: 'EU 35',
					description: 'EU 35',
				},
			] as SizeInterface[]

			sizes && sizes.map(async size => {
				await this.SizeRepository.insert(
					size.id,
					size.size_id,
					size.title,
					null,
					transaction)
			})
			return this
		}
	}

}

export default new SizeManager()
