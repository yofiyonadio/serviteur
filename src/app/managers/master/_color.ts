import {
	// ErrorModel,
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import ColorRepository from 'app/repositories/color'

import DEFAULTS from 'utils/constants/default'
// import { ERROR_CODES } from 'app/models/error'


class ColorManager extends ManagerModel {

	static __displayName = 'ColorManager'

	protected ColorRepository: ColorRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			ColorRepository,
		], () => {
			// Initialization function goes here
			this.withTransaction(undefined, async transaction => {
				return this.make(transaction)
			})
		})
	}
	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(
		id: number,
		transaction: EntityManager,
	) {
		return this.ColorRepository.get(id, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================

	// ============================ PRIVATES ===========================
	private async make(transaction: EntityManager) {
		if(DEFAULTS.SYNC) {
			await this.ColorRepository.insert([{
				id: 1,
				title: 'Lilac',
				hex: '#CC99CC',
			}, {
				id: 2,
				title: 'Light Yellow',
				hex: '#FFF3B2',
			}, {
				id: 3,
				title: 'Mint',
				hex: '#99CC99',
			}, {
				id: 4,
				title: 'Light Blue',
				hex: '#B5D3E7',
			}, {
				id: 5,
				title: 'Camel',
				hex: '#C19A6B',
			}, {
				id: 6,
				title: 'Ecru',
				hex: '#FDFBF8',
			}, {
				id: 7,
				title: 'Fuschia',
				hex: '#C7537E',
			}, {
				id: 8,
				title: 'Maroon',
				hex: '#7F0000',
			}, {
				id: 9,
				title: 'Khaki',
				hex: '#BDB76B',
			}, {
				id: 10,
				title: 'Emerald',
				hex: '#005900',
			}, {
				id: 11,
				title: 'Teal',
				hex: '#0F786F',
			}, {
				id: 12,
				title: 'Navy',
				hex: '#004099',
			}, {
				id: 13,
				title: 'Nude',
				hex: '#E3BC9A',
			}, {
				id: 14,
				title: 'white',
				image: '//question/colour-11-white.jpg',
				hex: '#F8F8F8',
			}, {
				id: 15,
				title: 'black',
				image: '//question/colour-13-black.jpg',
				hex: '#000000',
			}, {
				id: 16,
				title: 'red',
				image: '//question/colour-3-red.jpg',
				hex: '#B4151C',
			}, {
				id: 17,
				title: 'pink',
				image: '//question/colour-2-pink.jpg',
				hex: '#FFB2B2',
			}, {
				id: 18,
				title: 'orange',
				image: '//question/colour-4-orange.jpg',
				hex: '#FFA500',
			}, {
				id: 19,
				title: 'purple',
				image: '//question/colour-1-purple.jpg',
				hex: '#800080',
			}, {
				id: 20,
				title: 'yellow',
				image: '//question/colour-5-yellow.jpg',
				hex: '#FFD700',
			}, {
				id: 21,
				title: 'green',
				image: '//question/colour-6-green.jpg',
				hex: '#008000',
			}, {
				id: 22,
				title: 'turquoise',
				image: '//question/colour-7-turquoise.jpg',
				hex: '#83BFBD',
			}, {
				id: 23,
				title: 'blue',
				image: '//question/colour-8-blue.jpg',
				hex: '#257BCD',
			}, {
				id: 24,
				title: 'brown',
				image: '//question/colour-9-brown.jpg',
				hex: '#A5622A',
			}, {
				id: 25,
				title: 'beige',
				image: '//question/colour-10-beige.jpg',
				hex: '#F5EEDC',
			}, {
				id: 26,
				title: 'silver',
				hex: '#C0C0C0',
				image: '//question/colour-14-silver.jpg',
			}, {
				id: 27,
				title: 'gold',
				hex: '#DDAA0F',
				image: '//question/colour-15-gold.jpg',
			}, {
				id: 28,
				title: 'grey',
				hex: '#999999',
				image: '//question/colour-12-grey.jpg',
			}, {
				id: 29,
				title: 'rose gold',
				hex: '#F3CDC8',
				image: '//question/colour-16-rose-gold.jpg',
			}, {
				id: 30,
				title: 'multi color',
				hex: '#00000000',
				image: '//question/colour-17-multi.jpg',
			}, {
				id: 31,
				title: 'no color',
				hex: '#FFFFFF00',
				image: '//question/colour-18-none.jpg',
			}, {
				id: 32,
				title: 'olive',
				hex: '#708238',
			}, {
				id: 33,
				title: 'dark grey',
				hex: '#666666',
			}, {
				id: 34,
				title: 'dark brown',
				hex: '#654321',
			}, {
				id: 35,
				title: 'mustard',
				hex: '#E1AD01',
			}, {
				id: 36,
				title: 'cream',
				hex: '#FFF3B2',
			}, {
				id: 37,
				title: 'Dusty Pink',
				hex: '#DBBEBB',
			}, {
				id: 38,
				title: 'Sand',
				hex: '#C2B280',
			}, {
				id: 39,
				title: 'Off White',
				hex: '#FFFFF0',
			}], transaction)
		}
	}
}

export default new ColorManager()
