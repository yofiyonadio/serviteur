import {
	// ErrorModel,
	ManagerModel, ErrorModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	LocationRepository,
	AreaManualRepository,
	AreaTikiRepository,
	AreaSicepatRepository,
} from 'app/repositories'

import { AddressMetadataInterface } from 'energie/app/models/interface.address'
import { AreaTikiInterface, AreaManualInterface, LocationInterface, AreaSicepatInterface } from 'energie/app/interfaces'

import { StringHelper } from 'utils/helpers'

import { SHIPMENTS } from 'energie/utils/constants/enum'
import { ERRORS, ERROR_CODES } from 'app/models/error'

import { groupBy, isEmpty, uniqBy } from 'lodash'
import fs from 'fs'


type AreaByShipmentType<T extends SHIPMENTS> = T extends SHIPMENTS.MANUAL
	? AreaManualInterface
	: T extends SHIPMENTS.TIKI
		? AreaTikiInterface
			: T extends SHIPMENTS.SICEPAT
			? AreaSicepatInterface
				: never


class LocationManager extends ManagerModel {

	static __displayName = 'LocationManager'

	protected LocationRepository: LocationRepository
	protected AreaManualRepository: AreaManualRepository
	protected AreaTikiRepository: AreaTikiRepository
	protected AreaSicepatRepository: AreaSicepatRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			LocationRepository,
			AreaManualRepository,
			AreaTikiRepository,
			AreaSicepatRepository,
		], async () => {
			await this.withTransaction(undefined, async transaction => {
				if(process.env.SYNC_LOCATION === 'true') {
					const locations = StringHelper.stripBOM(fs.readFileSync('data/location.json', 'utf8'))
					await this.LocationRepository.insert(JSON.parse(locations), transaction)

					const manuals = StringHelper.stripBOM(fs.readFileSync('data/location.manual.json', 'utf8'))
					await this.AreaManualRepository.saveOrUpdate('AreaManualRecord', JSON.parse(manuals), transaction)

					const tikis = StringHelper.stripBOM(fs.readFileSync('data/location.tiki.json', 'utf8'))
					await this.AreaTikiRepository.saveOrUpdate('AreaTikiRecord', JSON.parse(tikis), transaction)

					const sicepats = StringHelper.stripBOM(fs.readFileSync('data/location.sicepat.json', 'utf8'))
					await this.AreaSicepatRepository.saveOrUpdate('AreaSicepatRecord', JSON.parse(sicepats), transaction)

					const sicepat_locations = StringHelper.stripBOM(fs.readFileSync('data/location.sicepat.location.json', 'utf8'))
					await this.AreaSicepatRepository.combine(JSON.parse(sicepat_locations), transaction)
				}
			})
		})
	}

	// ============================= INSERT =============================

	// ============================= UPDATE =============================

	// ============================= GETTER =============================
	@ManagerModel.bound
	async get(id: number, transaction: EntityManager) {
		return this.LocationRepository.get(id, transaction)
	}

	@ManagerModel.bound
	async getProvinces(transaction: EntityManager) {
		return this.LocationRepository.getProvinces(transaction)
	}

	@ManagerModel.bound
	async getCities(province_id: number | undefined, transaction: EntityManager) {
		return this.LocationRepository.getCities(province_id, transaction)
	}

	@ManagerModel.bound
	async getSuburbs(city_id: number | undefined, transaction: EntityManager) {
		return this.LocationRepository.getSuburbs(city_id, transaction)
	}

	@ManagerModel.bound
	async getPriceLikeShipper(
		targetId: number,
		weight: number,
		transaction: EntityManager,
	) {
		return this.AreaManualRepository.getByLocationId([targetId], transaction).then(area => {
			const price = area.pop().price
			const _weight = Math.max(Math.ceil(weight), 1)
			return {
				status: 'success',
				data: {
					title: 'OK',
					content: 'Successfully retrieving rates',
					rule: '-',
					originArea: '—',
					destinationArea: targetId,
					rates: {
						logistic: {
							regular: [{
								name: 'J&T',
								logo_url: 'http://cdn.shipper.cloud/logistic/small/JNT.png',
								rate_id: 57,
								show_id: 1,
								rate_name: 'Custom',
								stop_origin: 51,
								stop_destination: 6926,
								weight: _weight,
								volumeWeight: _weight,
								finalWeight: _weight,
								itemPrice: 599000,
								item_price: 599000,
								finalRate: price * _weight,
								insuranceRate: 0,
								compulsory_insurance: 0,
								liability: 599000,
								discount: 0,
								min_day: 1,
								max_day: 3,
							}],
						},
					},
				},
			}

		})
	}

	@ManagerModel.bound
	async getAreasFromPostalCode(
		postal_code: string,
		transaction: EntityManager,
	) {
		return this.LocationRepository.getAreas('POSTAL', postal_code, transaction)
	}

	@ManagerModel.bound
	async getAreasFromSuburbId(
		suburb_id: number,
		transaction: EntityManager,
	) {
		return this.LocationRepository.getAreas('SUBURB', suburb_id, transaction)
	}

	@ManagerModel.bound
	async getArea<T extends SHIPMENTS>(
		type: T,
		location_id: number,
		config: {
			slug?: string,
		},
		metadata: AddressMetadataInterface | undefined,
		transaction: EntityManager,
	) {
		return !isEmpty(metadata) && metadata.manualAreaId ? this.getAreaById(type, metadata.manualAreaId, transaction) : this.getAreaByLocationId(type, location_id, config, transaction)
	}

	// ============================= DELETE =============================

	// ============================= METHODS ============================
	@ManagerModel.bound
	async searchAreas(
		search: string,
		transaction: EntityManager,
	) {
		return this.LocationRepository.getAreas('SEARCH', search, transaction)
	}

	// ============================ PRIVATES ============================
	private async getAreaByLocationId<T extends SHIPMENTS>(type: T, location_id: number, config: { slug?: string }, transaction: EntityManager): Promise<AreaByShipmentType<T>>
	private async getAreaByLocationId<T extends SHIPMENTS>(type: T, location_ids: number[], config: { slug?: string }, transaction: EntityManager): Promise<Array<AreaByShipmentType<T>>>
	private async getAreaByLocationId(
		type: SHIPMENTS,
		location_id_or_ids: number | number[],
		config: {
			slug?: string,
		},
		transaction: EntityManager,
	): Promise<any> {
		const isArray = Array.isArray(location_id_or_ids)

		return Promise.resolve().then(() => {
			switch(type) {
			case SHIPMENTS.MANUAL:
				return this.AreaManualRepository.getByLocationId(isArray ? location_id_or_ids as number[] : [location_id_or_ids as number], transaction) as any
			case SHIPMENTS.TIKI:
				return this.AreaTikiRepository.getByLocationId(location_id_or_ids, transaction) as any
			case SHIPMENTS.SICEPAT:
				return this.AreaSicepatRepository.getByLocationId(location_id_or_ids, config.slug, transaction) as any
			default:
				throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Cannot get area of type ${ type }.`)
			}
		}).then((areas: any) => {
			if (isArray) {
				return groupBy(areas, 'location_id')
			} else {
				return {
					[location_id_or_ids as number]: areas,
				}
			}
		}).then(areasByLocationId => {
			const locationIds: number[] = isArray ? location_id_or_ids as number[] : [location_id_or_ids as number]

			return Promise.all(locationIds.map(async locationId => {
				let areas: any[]
				let keyword: string

				switch(type) {
				case SHIPMENTS.MANUAL:
					areas = uniqBy(areasByLocationId[locationId], 'price')
					keyword = 'manualAreaId'
					break
				case SHIPMENTS.TIKI:
					areas = uniqBy(areasByLocationId[locationId], 'tariff_code')
					keyword = 'tikiAreaId'
					break
				case SHIPMENTS.SICEPAT:
					areas = uniqBy(areasByLocationId[locationId], 'code')
					keyword = 'sicepatAreaId'
					break
				default:
					throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Cannot get area of type ${type}.`)
				}

				if (areas.length > 1) {
					// Multiple area found
					const parsedAreas = this.parseAreas(type, await this.LocationRepository.get(locationId, transaction), areas)

					if (uniqBy(parsedAreas, a => {
						return `${ a.title }|${ a.description }`
					}).length > 1) {
						throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_107, {
							type,
							keyword,
							location_id: locationId,
							areas: parsedAreas,
						})
					} else {
						return areas.pop()
					}
				} else if (areas.length === 0) {
					throw new ErrorModel(ERRORS.NOT_FOUND, ERROR_CODES.ERR_101, 'Location not found on Tiki table')
				} else {
					return areas.pop()
				}
			}))
		}).then(areas => {
			if (isArray) {
				return areas
			} else {
				return areas.pop()
			}
		})
	}

	private async getAreaById<T extends SHIPMENTS>(type: T, area_id: number, transaction: EntityManager): Promise<AreaByShipmentType<T>>
	private async getAreaById<T extends SHIPMENTS>(type: T, area_ids: number[], transaction: EntityManager): Promise<Array<AreaByShipmentType<T>>>
	private async getAreaById(
		type: SHIPMENTS,
		area_id_or_ids: number | number[],
		transaction: EntityManager,
	): Promise<any> {
		const isArray = Array.isArray(area_id_or_ids)

		switch(type) {
		case SHIPMENTS.MANUAL:
			return this.AreaManualRepository.getById(isArray ? area_id_or_ids as number[] : [area_id_or_ids as number], transaction).then(manualAreas => {
				if (!isArray) {
					return manualAreas.pop()
				} else {
					return manualAreas
				}
			})
		case SHIPMENTS.TIKI:
			return this.AreaTikiRepository.getById(area_id_or_ids, transaction).then(tikiAreas => {
				if (!isArray) {
					return tikiAreas.pop()
				} else {
					return tikiAreas
				}
			})
		case SHIPMENTS.SICEPAT:
			return this.AreaTikiRepository.getById(area_id_or_ids, transaction).then(sicepatAreas => {
				if(!isArray) {
					return sicepatAreas.pop()
				} else {
					return sicepatAreas
				}
			})
		default:
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Cannot get area of type ${ type }.`)
		}
	}

	private parseAreas<T extends SHIPMENTS>(
		type: T,
		location: LocationInterface,
		areas: Array<AreaByShipmentType<T>>,
	): Array<{
		id: number,
		title: string,
		description: string,
	}> {
		switch(type) {
		case SHIPMENTS.MANUAL:
			return (areas as Array<AreaByShipmentType<SHIPMENTS.MANUAL>>).map(area => {
				return {
					id: area.id,
					title: area.area,
					description: `${ location.city_name }/${ this.parseSuburbWithAlias(location) }/${ this.parseAreaWithAlias(location) }`,
				}
			})
		case SHIPMENTS.TIKI:
			return (areas as Array<AreaByShipmentType<SHIPMENTS.TIKI>>).map(area => {
				return {
					id: area.id,
					title: `${ area.area } – ${ area.district }`,
					description: `${ location.city_name }/${ this.parseSuburbWithAlias(location) }/${ this.parseAreaWithAlias(location) }`,
				}
			})
		case SHIPMENTS.SICEPAT:
			return (areas as Array<AreaByShipmentType<SHIPMENTS.SICEPAT>>).map(area => {
				return {
					id: area.id,
					title: area.subdistrict,
					description: `${ location.city_name }/${ this.parseSuburbWithAlias(location) }/${ this.parseAreaWithAlias(location) }`,
				}
			})
		default:
			throw new ErrorModel(ERRORS.NOT_VALID, ERROR_CODES.ERR_103, `Cannot parse areas of type ${ type }.`)
		}
	}

	private parseAreaWithAlias(location: LocationInterface): string {
		return `${ location.area_name }${ location.area_alias ? `(${ location.area_alias })` : '' }`
	}

	private parseSuburbWithAlias(location: LocationInterface): string {
		return `${ location.suburb_name }${ location.suburb_alias ? `(${ location.suburb_alias })` : '' }`
	}

}

export default new LocationManager()
