import {
	ManagerModel,
} from 'app/models'
import { Connection, EntityManager } from 'typeorm'

import {
	AssetRepository,
} from 'app/repositories'

import AssetService from 'app/services/asset'
import { AssetUploadConfig } from 'app/interfaces/file'

import InterfaceAssetModel from 'energie/app/models/interface.asset'

import { ObjectOrArray, Without, Parameter } from 'types/common'

import { ASSET_TYPES } from 'app/repositories/asset'
import DEFAULTS from 'utils/constants/default'
import { UserManager } from '.'
import { UserAssetInterface } from 'energie/app/interfaces'
import { ORDER_REQUESTS } from 'energie/utils/constants/enum'

export enum NONCES {
	SPQ_1 = 'STYLE_PROFILE_ATTACHMENT_1',
	SPQ_2 = 'STYLE_PROFILE_ATTACHMENT_2',
	PROFILE = 'PROFILE_PICTURE',
	COVER = 'COVER_PICTURE',
	SIGNATURE = 'SIGNATURE',
}

class AssetManager extends ManagerModel {

	static __displayName = 'AssetManager'

	protected AssetRepository: AssetRepository

	async initialize(connection: Connection) {
		return super.initialize(connection, [
			AssetRepository,
		])
	}

	// ============================= INSERT =============================
	@ManagerModel.bound
	async create(
		type: ASSET_TYPES,
		type_id: number,
		file: Parameter<InterfaceAssetModel>,
		transaction: EntityManager,
	) {
		return this.AssetRepository.insert(type, type_id, file, transaction)
	}

	@ManagerModel.bound
	async createMany(
		type: ASSET_TYPES,
		type_id: number,
		files: Array<Parameter<InterfaceAssetModel>>,
		transaction: EntityManager,
	) {
		return this.AssetRepository.insert(type, type_id, files, transaction)
	}

	@ManagerModel.bound
	async reorderAsset(
		type: ASSET_TYPES,
		ids: number[],
		transaction: EntityManager,
	) {
		return this.AssetRepository.reorderAssets(type, ids, transaction)
	}

	@ManagerModel.bound
	async upload(
		type: ASSET_TYPES,
		type_id: number,
		image: string,
		nonce: NONCES | undefined,
		options: Without<AssetUploadConfig, 'public_id' | 'folder'> | undefined,
		metadata: object | undefined,
		order: number | undefined,
		transaction: EntityManager,
	) {
		const file = await AssetService.upload({
			image,
			options: {
				...options,
				folder: `${process.env.ENV}/${DEFAULTS.AGENT}/${type}/${type_id}`,
				...(nonce !== undefined ? {
					public_id: nonce,
					overwrite: true,
				} : {}),
			},
			metadata,
			nonce,
		})

		if (nonce !== undefined) {
			// insert or update
			return this.AssetRepository.update(type, type_id, nonce, {
				...file,
				// ...(order === undefined ? {} : { order }),
				deleted_at: null,
			}, transaction).then(isUpdated => {
				if(!isUpdated) {
					// asset does not exist, insert
					return this.AssetRepository.insert(type, type_id, {
						...file,
						// ...(order === undefined ? {} : { order }),
						deleted_at: null,
					}, transaction)
				}

				return this.AssetRepository.get(type, type_id as number, nonce, transaction)
			}).then(async asset => {
				if(type === 'user' && nonce === NONCES.PROFILE) {
					await UserManager.updateProfile((asset as UserAssetInterface).user_id, {
						asset_id: asset.id,
					}, transaction)
				} else if(type === 'user' && nonce === NONCES.COVER) {
					await UserManager.updateProfile((asset as UserAssetInterface).user_id, {
						cover_id: asset.id,
					}, transaction)
				} else if(type === 'user' && nonce === NONCES.SIGNATURE) {
					await UserManager.updateProfile((asset as UserAssetInterface).user_id, {
						signature_id: asset.id,
					}, transaction)
				}

				return asset
			})
		} else {
			return this.AssetRepository.insert(type, type_id, {
				...file,
				// ...(order === undefined ? {} : { order }),
			}, transaction)
		}
	}

	@ManagerModel.bound
	async uploadBatch(
		type: ASSET_TYPES,
		type_id: number,
		datas: Array<{
			image: string,
			options: AssetUploadConfig,
			metadata: object,
			nonce: string,
			order: number,
		}>,
		transaction: EntityManager,
	) {
		const files = await Promise.all(datas.map(async (data, i) => {
			const file = await AssetService.upload({
				image: data.image,
				options: {
					...data.options,
					folder: `${process.env.ENV}/${DEFAULTS.AGENT}/${type}/${type_id}`,
					...(data.nonce !== undefined ? {
						public_id: data.nonce,
						overwrite: true,
					} : {}),
				},
				// metadata: data.metadata,
				metadata: {
					...data.metadata,
					order: i,
					index: i,
				},
				nonce: data.nonce,
			})

			return {
				...file,
				order: data.order,
			}
		}))

		return this.AssetRepository.insert(type, type_id, files, transaction)
	}

	// ============================= UPDATE =============================
	// We dont support update yet.
	// We should provide mechanism to update metadata later tho

	// ============================= GETTER =============================
	// async get(type: ASSET_TYPES, type_id: number, idOrNonce: ObjectOrArray<number | string>)
	// async get(type: ASSET_TYPES, type_id: number | number[], many?: boolean)
	@ManagerModel.bound
	async get(
		type: ASSET_TYPES,
		type_id: number | number[],
		idOrNonceOrMany: ObjectOrArray<number | string> | boolean,
		transaction: EntityManager,
	) {
		if(typeof idOrNonceOrMany === 'boolean' || idOrNonceOrMany === undefined) {
			return this.AssetRepository.get(type, type_id, idOrNonceOrMany as boolean, transaction)
		} else {
			return this.AssetRepository.get(type, type_id as number, idOrNonceOrMany, transaction)
		}
	}

	@ManagerModel.bound
	async getByOrderRequestType(
		type: ORDER_REQUESTS,
		reference_id: number,
		transaction: EntityManager,
	) {
		return this.AssetRepository.getByOrderRequestType(type, reference_id, transaction)
	}

	// ============================= DELETE =============================
	get batchDelete() {
		return this.AssetRepository.batchDelete
	}

	@ManagerModel.bound
	async delete(
		type: ASSET_TYPES,
		type_id: number,
		idOrNonce: number | string | undefined,
		transaction: EntityManager,
	) {
		// const trxx = idOrNonce instanceof EntityManager ? idOrNonce : trx
		if (typeof idOrNonce === 'string' || typeof idOrNonce === 'number') {
			return this.AssetRepository.delete(type, type_id, idOrNonce, transaction)
		} else {
			return this.AssetRepository.delete(type, type_id, transaction)
		}
	}

	// ============================= METHODS ============================

}

export default new AssetManager()
