import {
	ControllerModel,
} from 'app/models'
import { Response, Request } from 'express'

class AdminDefaultsController extends ControllerModel {

	route() {
		return {
			'/': {
				get: this.default,
			},
		}
	}

	private default = async (req: Request, res: Response) => {
		res.json('Success')
	}
}

export default new AdminDefaultsController()
