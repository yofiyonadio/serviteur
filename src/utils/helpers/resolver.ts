export default {
	resolve(initialValue?: any, onlyIfUndefined: boolean = true) {
		let val: any = initialValue

		const value = {
			get value() {
				return val
			},
			setter: (_value: any) => {
				if (!onlyIfUndefined || onlyIfUndefined && value.value === undefined) {
					val = _value
				}
			},
		}

		return value
	},
}
