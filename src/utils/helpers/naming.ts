import {
	DefaultNamingStrategy,
	// Table,
} from 'typeorm'


class NamingHelper extends DefaultNamingStrategy {
	// foreignKeyName(tableOrName: Table | string, columnNames: string[]): string {
	// 	// sort incoming column names to avoid issue when ["id", "name"] and ["name", "id"] arrays
	// 	const clonedColumnNames = [...columnNames]
	// 	clonedColumnNames.sort()

	// 	const tableName = tableOrName instanceof Table ? tableOrName.name : tableOrName
	// 	const replacedTableName = tableName.replace('.', '_')
	// 	// const key = `${replacedTableName}_${clonedColumnNames.join('_')}`;

	// 	return `FK_${replacedTableName}`
	// }
}

export default new NamingHelper()
