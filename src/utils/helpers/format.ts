import BaseModel from 'energie/app/models/_base'
import { ORDERS } from 'energie/utils/constants/enum'
import {
	capitalize,
} from 'lodash'


const phones = {
	'+62': {
		country: 'IDN',
		tester: /^\+62/,
	},
	'0': {
		country: 'IDN',
		tester: /^0/,
	},
}

const phoneCodes = Object.keys(phones)
const phoneCodeRegex = phoneCodes.map(code => {
	return phones[code].tester
})

function phoneCodeIndex(input: string) {
	return phoneCodeRegex.findIndex(regex => {
		return regex.test(input)
	})
}

function phonePrefix(input: string, index = phoneCodeIndex(input + '')) {
	return input && index > -1 && phoneCodes[index] || ''
}


class FormatHelper extends BaseModel {
	static __displayName = 'FormatHelper'

	// USE THESE INSTEAD ==========================================

	readableStatus(status: ORDERS) {
		return capitalize(status.replace(/_/gi, ' '))
	}

	currency(number: number, prefix = '') {
		return number !== undefined ? this.prefix(number.toString().replace(/,/g, '').replace(/(\d)(?=(\d{3})+$)/g, '$1,'), prefix) : ''
	}

	phone(phoneNumber: string) {
		return phoneNumber ? phoneNumber.replace(/-/g, '').replace(/(\d{3})(\d{1,4})(\d{1,4})/, '$1-$2-$3') : ''
	}

	phoneCode(phoneNumber: string) {
		const pCIndex = phoneCodeIndex(phoneNumber + '')
		return pCIndex > -1 ? `${phonePrefix(phoneNumber, pCIndex)} ${this.phone(phoneNumber.replace(phoneCodes[pCIndex], ''))}` : this.phone(phoneNumber)
	}

	date(birthdate: string) {
		if (birthdate) {
			const stripped = birthdate.replace(/ |\//g, '')

			if (stripped.length <= 4) {
				return stripped.replace(/(\d{2})(\d{1,2})/, '$1 / $2')
			} else {
				return stripped.replace(/(\d{2})(\d{2})(\d{1,4})/, '$1 / $2 / $3')
			}
		}

		return ''
	}

	birthdate(birthdate: string) {
		if (birthdate) {
			const stripped = birthdate.replace(/ |\//g, '')

			if (stripped.length <= 4) {
				return stripped.replace(/(\d{2})(\d{1,2})/, '$1 / $2')
			} else {
				return stripped.replace(/(\d{2})(\d{2})(\d{1,4})/, '$1 / $2 / $3')
			}
		}

		return ''
	}

	private prefix(value: string | number, prefix: string) {
		if(value || value === 0) {
			return prefix ? prefix + ' ' + value : value + ''
		} else {
			return value + ''
		}
	}
}

export default new FormatHelper()
