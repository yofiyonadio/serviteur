import Times from 'utils/constants/time'

let MemCache = {}
const defaultExpiricy = Times.day()
const remove = function(key: string) {
	if(MemCache[key]) {
		delete MemCache[key]
	}

	return this
}
const safeRemove = function() {
		const now = Date.now()

		Object.keys(MemCache).forEach(key => {
			const value = MemCache[key]
			if(value.expiricy < now) {
				remove(key)
			}
		})

		return this
	}

setInterval(safeRemove, Times.minute() * 5)

export default {
	set(key: string, value: any, expiricy = defaultExpiricy) {
		MemCache[key] = {
			value,
			expiricy: expiricy === -1 ? Infinity : expiricy + Date.now(),
		}

		return value
	},
	get(key: string) {
		return MemCache[key] && MemCache[key].value
	},
	remove,
	safeRemove,
	removeAll() {
		MemCache = {}

		return this
	},
}
