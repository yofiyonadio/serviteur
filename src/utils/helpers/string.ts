import { capitalize } from 'lodash'

function splitCamel(string: string) {
	return string && string.split(/(?<=[a-z])(?=[A-Z])/) || []
}

function replaceStringToDoubleQuote(string: string) {
	return string.replace(/"/g, '\\"')
}

function stripBOM(string: string) {
	if (string.charCodeAt(0) === 0xFEFF) {
		return string.slice(1)
	}

	return string
}

function statusToReadable(string: string) {
	return string.split('_').map(s => capitalize(s)).join(' ')
}

function pluralize(num: number, word: string = 'ITEM', emptyString: string = '', plural: string = 'S') {
	return emptyString && !num ? emptyString : `${num} ${word}${num > 1 ? plural : ''}`
}

function objectToNote(obj: object) {
	const re: string[] = []

	for(const i in obj) {
		if(obj[i]) {
			re.push(`${ String(i).split('_').map(s => capitalize(s)).join(' ') }: ${ obj[i] }`)
		}
	}

	return re.join('\n')
}

function objectToArrayNote(obj: object) {
	const re: Array<[string, string]> = []

	for (const i in obj) {
		if (obj[i]) {
			re.push([`${ String(i).split('_').map(s => s.toUpperCase()).join(' ') }`, obj[i]])
		}
	}

	return re
}

export default {
	splitCamel,
	replaceStringToDoubleQuote,
	stripBOM,
	statusToReadable,
	pluralize,
	objectToNote,
	objectToArrayNote,
}
