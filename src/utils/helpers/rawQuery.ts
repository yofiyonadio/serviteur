// import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'


class RawQuery {

	select({
		record,
		from,
		prefix_as = false,
		agg = false,
		doubleQuote = false,
		prefix_column = false
	}: {
		record: any,
		from: string,
		prefix_as?: boolean | 'FROM_AS',
		agg?: 'MAX' | 'MIN' | 'AVG' | 'DISTINCT' | boolean,
		doubleQuote?: boolean,
		prefix_column?: boolean | string
	}) {
		let prefixs = ''
		if (prefix_as === true) {
			prefixs = record['table']['name'] + '_'
		}
		if (prefix_as === 'FROM_AS') {
			prefixs = from + '_'
		}
		if (doubleQuote) {
			from = '"' + from + '"'
		}
		return record['columns'].map(column => {
			let select = ''
			if (prefix_column) {
				select = `${from}."${record['table']['name'] + '_' + column}"`
			} else {
				select = `${from}."${column}"`
			}
			if (agg) {
				if (agg === 'DISTINCT') {
					select = `(array_agg(${select}))[1]`
				} else {
					select = `${agg}(${select})`
				}
			}
			return `${select} "${prefixs}${column}"`
		}).join(', ')
	}

	mapper(formats: object, nested: boolean = false) {
		let results: any = []
		Object.keys(formats).forEach(format => {
			const formatVal = (formats as any)[format]
			if (formatVal instanceof Object) {
				if (formatVal instanceof Array) {
					const tempArr: string[] = []
					formatVal[0].record['columns'].forEach((col: string) => {
						tempArr.push(`'${col.split(format + '_').join('')}'`)
						tempArr.push(`"${formatVal[0].from}"."${format + '_' + col}"`)
					})
					if (formatVal[0].child) {
						tempArr.push.apply(tempArr, this.mapper(formatVal[0].child, true))
					}
					let queryBuild = `array_to_json(array_agg(json_build_object(${tempArr.join(', ')})))`
					if (!nested) {
						queryBuild = queryBuild + ` "${format}"`
					}
					if (nested) {
						results.push(format)
					}
					results.push(queryBuild)
				} else {
					const tempArr: string[] = []
					formatVal.record['columns'].forEach((col: string) => {
						tempArr.push(`'${col.split(format + '_').join('')}'`)
						tempArr.push(`(array_agg("${formatVal.from}"."${format + '_' + col}"))[1]`)
					})
					if (formatVal.child) {
						tempArr.push.apply(tempArr, this.mapper(formatVal.child, true))
					}
					let queryBuild = `json_strip_nulls(json_build_object(${tempArr.join(', ')}))`
					if (!nested) {
						queryBuild = queryBuild + ` "${format}"`
					}
					if (nested) {
						results.push(format)
					}
					results.push(queryBuild)
				}
			}
		})
		if (!nested) {
			results = results.join(', ')
		}
		return results
	}

}



export default new RawQuery()
