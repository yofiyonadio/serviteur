function getValueFromLabels(answer: string, labels: string[], optionalLabels ?: string[]) {
	let val = labels.indexOf(answer)
	val = val > -1 ? val : labels.map(label => {
		return label && label.toLowerCase && label.toLowerCase() || label
	}).indexOf(answer && answer.toLowerCase && answer.toLowerCase() || answer)

	if (val === -1 && optionalLabels) {
		val = getValueFromLabels(answer, optionalLabels)
	}

	return val
}

function getNormalizedValue(answer: string, normals: string[], mapping: {}) {
	if (normals.indexOf(answer) > -1) {
		return answer
	} else {
		return mapping[answer] || mapping[answer && answer.toLowerCase && answer.toLowerCase().replace(/ /g, '') || answer]
	}
}

function getNormalizedStyleValue(answer: string) {
	return getNormalizedValue(answer, ['Hate it', 'Just ok', 'Love it'], {
		'Just OK': 'Just ok',
		'Love It': 'Love it',
		'Tidak suka': 'Hate it',
		'Biasa saja': 'Just ok',
		'Sangat suka ♥': 'Love it',
		// FOOL PROOF
		'justok': 'Just ok',
		'loveit': 'Love it',
		'hateit': 'Hate it',
		'tidaksuka': 'Hate it',
		'biasasaja': 'Just ok',
		'sangatsuka♥': 'Love it',
	})
}


export default {
	parseAnswers(answer: any) {
		switch (answer.field.id) {
		// bentuk tubuh
		case 'WulUiuA25RCi': // ok
		case 'I2yHv2moC0mt': // ok
		case 'GWgo0Ln3Lojt': // ok
		case 'lxMQhcKQG5Cr': // ok
			return [1, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Pear', 'Hourglass', 'Rectangle', 'Inverted triangle', 'Apple', 'Don\'t know'], {
					'Pear/Triangle': 'Pear',
					'Pear/ Triangle': 'Pear',
					'Pear / Triangle': 'Pear',
					'Rectangle/Slender': 'Rectangle',
					'Rectangle / Slender': 'Rectangle',
					'Inverted Triangle': 'Inverted triangle',
					'I don\'t know': 'Don\'t know',
					// FOOL PROOF
					'pear/triangle': 'Pear',
					'hourglass': 'Hourglass',
					'rectangle/slender': 'Rectangle',
					'invertedtriangle': 'Inverted triangle',
					'idon\'tknow': 'Don\'t know',
				})],
			}]

		// bust
		case 'VTl7wxKzf1Vp': // ok
		case 'YRW1y6dVuDt5': // ok
		case 'jZz3GWLXzCrn': // ok
		case 'ptUBPvD5rzgC': // ok
			return [3, {
				NUMBER: answer[answer.type],
			}]

		// waist
		case 'Kf2VQGcIrhvI': // ok
		case 'YRGow6bbJ8aQ': // ok
		case 'ozxmPDHQubTn': // ok
		case 'KKmBLcRSM4b2': // ok
			return [4, {
				NUMBER: answer[answer.type],
			}]

		// hips measurement
		case 'E7tzsSQ3aNHo': // ok
		case 'aK58yRWUb9HV': // ok
		case 'K1Q2XIrfa2A9': // ok
		case 'wzZ6AZaWZ4S9': // ok
			return[5, {
				NUMBER: answer[answer.type],
			}]

		// shoulders proportion
		case 'EbrwJOUXtED3': // ok
		case 'XoHolilibY1K': // ok
		case 'EDjEeOg9MukH': // ok
		case 'ul8AEsyJ5lg9': // ok
			return [7, {
				SCALE: getValueFromLabels(answer[answer.type].label, ['Narrow', 'Average', 'Broad'], ['Kecil', 'Sedang', 'Lebar']),
			}]

		// torso proportion
		case 'rehssBEUJ0tL': // ok
		case 'rEK4gZsRgFt5': // ok
		case 'TZyl133OisEr': // ok
		case 'jTraObhg04vD': // ok
			return[8, {
				SCALE: getValueFromLabels(answer[answer.type].label, ['Short', 'Average', 'Long'], ['Pendek', 'Sedang', 'Panjang']),
			}]

		// hips proportion
		case 'SL2OhZBWI7up': // ok
		case 'wfYM6Csts2hN': // ok
		case 'SDk6wNaUAB85': // ok
		case 'tNtdVMBQooYm': // ok
			return[9, {
				SCALE: getValueFromLabels(answer[answer.type].label, ['Narrow', 'Average', 'Broad'], ['Kecil', 'Sedang', 'Lebar']),
			}]

		// legs proportion
		case 'LPim9qd30ZUX': // ok
		case 'WMb1o69xG2uV': // ok
		case 'ThnstXFrTfuk': // ok
		case 'MYsmBjwkiovo': // ok
			return [10, {
				SCALE: getValueFromLabels(answer[answer.type].label, ['Short', 'Average', 'Long'], ['Pendek', 'Sedang', 'Panjang']),
			}]

		// considered curvy
		case 'wZEKcWbJxdjd': // ok
		case 'M3AZ9y1tgYNo': // ok
		case 'COoJbPc0hQbk': // ok
		case 'M1Qy3Xbj4ixh': // ok
			return [11, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Upper half', 'Bottom half', 'Both', 'None'], {
					'Tubuh bagian atas': 'Upper half',
					'Tubuh bagian bawah': 'Bottom half',
					'Keduanya': 'Both',
					'Tidak ada': 'None',
				})],
			}]

		// height
		case 'LgEq7AYLpeDj': // ok
		case 'CJWam5APNPIz': // ok
		case 'nWsGWDJx6sAO': // ok
		case 'vapWgqihiK3p': // ok
			return [12, {
				NUMBER: answer[answer.type],
			}]

		// weight
		case 'foFZggJIBhtP': // ok
		case 'hc1VXdbIAwpD': // ok
		case 'b9Mjc82Zlvrc': // ok
		case 'cXJquE8tpXPu': // ok
			return [13, {
				NUMBER: answer[answer.type],
			}]

		// size category
		case 'FDJdw3NBp5Od': // ok
		case 'o7mrxy72keYi': // ok
		case 'TQCBXY77IKt8': // ok
		case 'ytCeBLoOK4aT': // ok
			return [14, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Regular', 'Petite', 'Plus', 'Tall'], {
					'Petite (mungil)': 'Petite',
					'Plus (besar)': 'Plus',
					'Tall (tinggi)': 'Tall',
					// FOOL PROOF
					'regular': 'Regular',
					'petite(mungil)': 'Petite',
					'plus(besar)': 'Plus',
					'tall(tinggi)': 'Tall',
				})],
			}]

		// dress size
		case 'xXFU38d1JBNN': // ok
		case 'MiLEZbNspduY': // ok
		case 'dP4ERab1YBzv': // ok
		case 'Yu1QiCQdV9Ei': // ok
			return [16, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// shirt / blouse size
		case 'PIACkpmvJ7AN': // ok
		case 'x1GrjtQyhKlj': // ok
		case 't5qBwIFOemzh': // ok
		case 'QyZh4UIbJCdD': // ok
			return [17, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// skirt size
		case 'IHyoEHgsVJF3': // ok
		case 'UfM50exjs4uB': // ok
		case 'o7vQ2epOi9fZ': // ok
		case 'feyj9Znr1l8M': // ok
			return [18, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// pants size
		case 'sRg3kqhpBLuZ': // ok
		case 'UjOXfb4KrO44': // ok
		case 'jmqtBJ3sPl8B': // ok
		case 'f9mlmHcnXph3': // ok
			return [19, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// jeans waist size
		case 'bmgbu8GjWcel': // ok
		case 'GExT6LLrMUZs': // ok
		case 'YUynpznhdQmM': // ok
		case 'h34Ni0sF57R1': // ok
			return [20, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// bra band
		case 'Y8OpZUolZtWb': // ok
		case 'lBuYBnIlMAqq': // ok
		case 'wf3kA5Zd2VYb': // ok
		case 'L0v6GXvH0v1n': // ok
			return [21, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// bra cup
		case 'XcJYaBX3y7Wg': // ok
		case 'jv7HLvnwO9Xy': // ok
		case 'KAFayhpcjwBH': // ok
		case 'b1jJpixxe1AH': // ok
			return [22, {
				SLIDER: answer[answer.type].label || answer[answer.type],
			}]

		// shoes size
		case 'IsEFOyUpbVOk': // ok
		case 'Vl7s86bfhkCi': // ok
		case 'QWSk3uSBCYjK': // ok
		case 'A0DTzqnDpOXJ': // ok
			return [23, getNormalizedValue(answer[answer.type].label || answer[answer.type], [], {
				'EU 35.5 / US 5': {
					TAB: 'EU',
					CHOICES: ['35.5'],
				},
				'EU 36 / US 5.5': {
					TAB: 'EU',
					CHOICES: ['36'],
				},
				'EU 36.5 / US 6': {
					TAB: 'EU',
					CHOICES: ['36.5'],
				},
				'EU 37 / US 6.5': {
					TAB: 'EU',
					CHOICES: ['37'],
				},
				'EU 37.5 / US 7': {
					TAB: 'EU',
					CHOICES: ['37.5'],
				},
				'EU 38 / US 7.5': {
					TAB: 'EU',
					CHOICES: ['38'],
				},
				'EU 38.5 / US 8': {
					TAB: 'EU',
					CHOICES: ['38.5'],
				},
				'EU 39 / US 8.5': {
					TAB: 'EU',
					CHOICES: ['39'],
				},
				'EU 39.5 / US 9': {
					TAB: 'EU',
					CHOICES: ['39.5'],
				},
				'EU 40 / US 9.5': {
					TAB: 'EU',
					CHOICES: ['40'],
				},
				'EU 40.5 / US 10': {
					TAB: 'EU',
					CHOICES: ['40.5'],
				},
				'EU 41 / US 10.5': {
					TAB: 'EU',
					CHOICES: ['41'],
				},
				'EU 41.5 / US 11': {
					TAB: 'EU',
					CHOICES: ['41.5'],
				},
				'EU 42 / US 11.5': {
					TAB: 'EU',
					CHOICES: ['42'],
				},
				'EU 42.5 / US 12': {
					TAB: 'EU',
					CHOICES: ['42.5'],
				},
				'EU 43 / US 12.5': {
					TAB: 'EU',
					CHOICES: ['43'],
				},
			})]

		// FIT & CUT

		// top-half fit
		case 'H7P1UHiT6PPA': // ok
		case 'BFproocQetnE': // ok
		case 'eW6WjRt2J2AV': // ok
		case 'WNZjsWt7pJiQ': // ok
			return [25, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Tight', 'Fitted', 'Straight', 'Loose', 'Oversized'], {
						'Ketat': 'Tight',
						'Pas': 'Fitted',
						'Lurus': 'Straight',
						'Longgar': 'Loose',
						'Sangat longgar': 'Oversized',
						// FOOL PROOF
						'tight': 'Tight',
						'fitted': 'Fitted',
						'straight': 'Straight',
						'loose': 'Loose',
						'oversized': 'Oversized',
						'ketat': 'Tight',
						'pas': 'Fitted',
						'lurus': 'Straight',
						'longgar': 'Loose',
						'sangatlonggar': 'Oversized',
					})
				}),
			}]

		// bottom-half fit
		case 'h4vcVrcv9so6': // ok
		case 'tk0tyg4lTfya': // ok
		case 'hp4BJQST1zwp': // ok
		case 'RhynZBnM7rcv': // ok
			return [26, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Tight', 'Fitted', 'Straight', 'Loose', 'Oversized'], {
						'Ketat': 'Tight',
						'Pas': 'Fitted',
						'Lurus': 'Straight',
						'Longgar': 'Loose',
						'Sangat longgar': 'Oversized',
						// FOOL PROOF
						'tight': 'Tight',
						'fitted': 'Fitted',
						'straight': 'Straight',
						'loose': 'Loose',
						'oversized': 'Oversized',
						'ketat': 'Tight',
						'pas': 'Fitted',
						'lurus': 'Straight',
						'longgar': 'Loose',
						'sangatlonggar': 'Oversized',
					})
				}),
			}]

		// jeans style
		case 'wwd99hKCa2C0': // ok
		case 'XzTBOXuNIj3t': // ok
		case 'fx68AtdO9qJz': // ok
		case 'iQXhO0WJfzey': // ok
			return [28, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Skinny', 'Straight', 'Bootcut', 'Wide', 'Cropped'], {
						// FOOL PROOF
						skinny: 'Skinny',
						straight: 'Straight',
						bootcut: 'Bootcut',
						wide: 'Wide',
						cropped: 'Cropped',
					})
				}),
			}]

		// jeans rise
		case 'aDg228BD3p3g': // ok
		case 'uOEGRbKimOnK': // ok
		case 'V3sKU7ra15Pn': // ok
		case 'ZqFancWm1ynG': // ok
			return [29, {
				CHOICE: answer[answer.type].labels,
			}]

		// STYLE & PREFERENCE

		// bohemian
		case 'eCwfxgOSvs26': // ok
		case 'hkeyHIjmSImV': // ok
		case 'gzwguHrDGIXa': // ok
		case 'RoYE7SOncju0': // ok
			return [31, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]
		// bombshell
		case 'KIOfWcpuuPfw': // ok
		case 'OE6AWvtqAcKS': // ok
		case 'V9idaD0TCSkU': // ok
		case 'IIwyEoRPX61f': // ok
			return [32, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// casual
		case 'xql9UUAdPSta': // ok
		case 'xa8NLWo7gnFP': // ok
		case 'vmyiUxYEuewC': // ok
		case 'WFr22w89jXC6': // ok
			return [33, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// chic
		case 'nyTw2Acpm28i': // ok
		case 'lyWDsr64usd4': // ok
		case 'JoHTpmBhqD5E': // ok
		case 'rEe12FGqMhcY': // ok
			return [34, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// classic
		case 's0kh7rwzWHn4': // ok
		case 'B7YzllENfX0o': // ok
		case 'PWqisA0fV3f4': // ok
		case 'pNP6ztKjtQ4r': // ok
			return [35, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// eclectic
		case 'CACEQ8EL9rK8': // ok
		case 'Png826eMgKNM': // ok
		case 'BHWjJkhtQeIo': // ok
		case 'HPSpyzf82YHB': // ok
			return [36, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// demure
		case 'K2l4ESqa1ZvG': // ok
		case 'iKhWgtzU1iMk': // ok
		case 'bmiO50kaO0l1': // ok
		case 'Z7HnpXlKOuil': // ok
			return [37, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// flamboyant
		case 'To08hLBQhrm9': // ok
		case 'b71jtLcJK368': // ok
		case 'ipiXaQ043CfY': // ok
		case 'niZOmVumwRRA': // ok
			return [38, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// rebel
		case 'UTvlClUcw6ze': // ok
		case 'TQfmH86RvxOm': // ok
		case 'cinm1ZRWdNRJ': // ok
		case 'LNGgIhAwZWeU': // ok
			return [39, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// sporty
		case 'avTpfFdgjeSk': // ok
		case 'Xzyi7y1cQtOy': // ok
		case 'b9urNCDUzEYT': // ok
		case 'cokhQXYCBQ2q': // ok
			return [40, {
				TAB: getNormalizedStyleValue(answer[answer.type].label),
			}]

		// comfort zone
		case 'mFAIC84Hl6dn': // ok
		case 'bzE8TwRj0ZHc': // ok
		case 'RGtcnB2zvYuN': // ok
		case 'DhtJYbRCTR39': // ok
			return [41, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Close to my style', 'Maybe a little bit', 'Open to new style'], {
					'Dekat dengan gaya saya': 'Close to my style',
					'Mungkin sedikit saja': 'Maybe a little bit',
					'Terbuka untuk mencoba gaya baru': 'Open to new style',
					// FOOL PROOF
					'closetomystyle': 'Close to my style',
					'maybealittlebit': 'Maybe a little bit',
					'opentonewstyle': 'Open to new style',
					'dekatdengangayasaya': 'Close to my style',
					'mungkinsedikitsaja': 'Maybe a little bit',
					'terbukauntukmencobagayabaru': 'Open to new style',
				})],
			}]

		// style set
		case 'qGAdTantWrNY': // ok
		case 'VgNrLdnFefyh': // ok
		case 'HQ5HTKMqmZmY': // ok
		case 'jgNzxXJqWkic': // ok
			return [42, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Mostly dresses and skirts', 'Mostly pants and jeans', 'Mix of both'], {
					'Biasanya dress dan rok': 'Mostly dresses and skirts',
					'Biasanya jeans dan celana': 'Mostly pants and jeans',
					'Campuran keduanya': 'Mix of both',
					// FOOL PROOF
					'mostlydressesandskirts': 'Mostly dresses and skirts',
					'mostlypantsandjeans': 'Mostly pants and jeans',
					'mixofboth': 'Mix of both',
					'biasanyadressdanrok': 'Mostly dresses and skirts',
					'biasanyajeansdancelana': 'Mostly pants and jeans',
					'campurankeduanya': 'Mix of both',
				})],
			}]

		// color palette
		case 'ovhyVm7Xsys0': // ok
		case 'Ukr7yfDkhcRS': // ok
		case 'njxXoaqF7mwl': // ok
		case 'KgwojT8MnDau': // ok
			return [43, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Bright', 'Neutral', 'Understated', 'Cool', 'Warm', 'I like all palettes'], {
						'Cerah': 'Bright',
						'Netral': 'Neutral',
						'Redup': 'Understated',
						'Sejuk': 'Cool',
						'Hangat': 'Warm',
						'Suka semua warna!': 'I like all palettes',
						'I like all colours!': 'I like all palettes',
						// FOOL PROOF
						'bright': 'Bright',
						'neutral': 'Neutral',
						'understated': 'Understated',
						'cool': 'Cool',
						'warm': 'Warm',
						'ilikeallpalettes': 'I like all palettes',
						'cerah': 'Bright',
						'netral': 'Neutral',
						'redup': 'Understated',
						'sejuk': 'Cool',
						'hangat': 'Warm',
						'sukasemuawarna!': 'I like all palettes',
						'ilikeallcolours!': 'I like all palettes',
					})
				}),
			}]

		// disliked color
		case 'oiqRFfdkvzfi': // ok
		case 'TrfbBBPDpAif': // ok
		case 'cL8TpDDpVYsq': // ok
		case 'rsu1RtnuMBPN': // ok
			return [44, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Purple', 'Pink', 'Red', 'Orange', 'Yellow', 'Green', 'Turquoise', 'Blue', 'Brown', 'Beige', 'White', 'Grey', 'Black', 'Gold', 'Silver'], {
						'Ungu': 'Purple',
						'Merah': 'Red',
						'Kuning': 'Yellow',
						'Hijau': 'Green',
						'Tosca': 'Turquoise',
						'Biru': 'Blue',
						'Coklat': 'Brown',
						'Putih': 'White',
						'Abu-abu': 'Grey',
						'Hitam': 'Black',
						// FOOL PROOF
						'purple': 'Purple',
						'pink': 'Pink',
						'red': 'Red',
						'orange': 'Orange',
						'yellow': 'Yellow',
						'green': 'Green',
						'turquoise': 'Turquoise',
						'blue': 'Blue',
						'brown': 'Brown',
						'beige': 'Beige',
						'white': 'White',
						'grey': 'Grey',
						'black': 'Black',
						'gold': 'Gold',
						'silver': 'Silver',
						'ungu': 'Purple',
						'merah': 'Red',
						'kuning': 'Yellow',
						'hijau': 'Green',
						'tosca': 'Turquoise',
						'biru': 'Blue',
						'coklat': 'Brown',
						'putih': 'White',
						'abu-abu': 'Grey',
						'hitam': 'Black',
					})
				}),
			}]

		// disliked pattern
		case 'pf9SohUeMJ4N': // ok
		case 'UFjndEJ7UsBy': // ok
		case 'qc3HJyCVtIua': // ok
		case 'WrCXJbH7zl0t': // ok
			return [46, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Animal print', 'Paisley', 'Floral', 'Plaid / Tartan', 'Polka dots', 'Stripes', 'Novelty print', 'Check', 'Traditional print'], {
						'Novelty print (e.g. birds)': 'Novelty print',
						// FOOL PROOF
						'animalprint': 'Animal print',
						'paisley': 'Paisley',
						'floral': 'Floral',
						'plaid/tartan': 'Plaid / Tartan',
						'polkadots': 'Polka dots',
						'stripes': 'Stripes',
						'noveltyprint': 'Novelty print',
						'check': 'Check',
						'traditionalprint': 'Traditional print',
						'noveltyprint(e.g.birds)': 'Novelty print',
					})
				}),
			}]

		// disliked fabric
		case 'uQHuSnrHOXfS': // ok
		case 'cxf6eOic1xyf': // ok
		case 'O996KH1eH7cr': // ok
		case 'oOfnuVvU9i5A': // ok
			return [47, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Faux Fur', 'Wool', 'Faux Leather', 'Polyester', 'Leather', 'Linen'], {
						'Bulu palsu': 'Faux Fur',
						'Kulit palsu': 'Faux Leather',
						'Kulit': 'Leather',
						// FOOL PROOF
						'fauxfur': 'Faux Fur',
						'wool': 'Wool',
						'fauxleather': 'Faux Leather',
						'polyester': 'Polyester',
						'leather': 'Leather',
						'linen': 'Linen',
						'bulupalsu': 'Faux Fur',
						'kulitpalsu': 'Faux Leather',
						'kulit': 'Leather',
					})
				}),
			}]

		// wear hijab
		case 'TFu3t8HRf9L1': // ok
		case 'wfxhrlM93OSQ': // ok
		case 'MLccv2QjxkuJ': // ok
		case 'rggHHa8gDZ6s': // ok
			return [48, {
				BOOLEAN: answer[answer.type],
			}]

		// cover up
		case 'eVjrQ2nJ6dg6': // ok
		case 'bvfyGMo6dJfv': // ok
		case 'qWezk1oOflAm': // ok
		case 'iMA6dnHNiGDJ': // ok
			return [49, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Shoulders', 'Arms', 'Cleavage', 'Back', 'Tummy', 'Buttocks', 'Legs'], {
						'Bahu': 'Shoulders',
						'Lengan': 'Arms',
						'Belahan Dada': 'Cleavage',
						'Bust / Cleavage': 'Cleavage',
						'Punggung': 'Back',
						'Perut': 'Tummy',
						'Tummy / Midsection': 'Tummy',
						'Pantat / bawah belakang': 'Buttocks',
						'Rear / Bottom': 'Buttocks',
						'Kaki': 'Legs',
						// FOOL PROOF
						'shoulders': 'Shoulders',
						'arms': 'Arms',
						'cleavage': 'Cleavage',
						'back': 'Back',
						'tummy': 'Tummy',
						'buttocks': 'Buttocks',
						'legs': 'Legs',
						'bahu': 'Shoulders',
						'lengan': 'Arms',
						'belahandada': 'Cleavage',
						'bust/cleavage': 'Cleavage',
						'punggung': 'Back',
						'perut': 'Tummy',
						'tummy/midsection': 'Tummy',
						'pantat/bawahbelakang': 'Buttocks',
						'rear/bottom': 'Buttocks',
						'kaki': 'Legs',
					})
				}),
			}]

		// wear heels
		case 'SIRxVdun1dUc': // ok
		case 'WZav23hnhDLC': // ok
		case 'mGBB6Uee286F': // ok
		case 'zusdzHZTMiiO': // ok
			return [50, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Always', 'Occasionally', 'Never'], {
					'Selalu': 'Always',
					'Kadang-kadang': 'Occasionally',
					'Tidak pernah': 'Never',
					// FOOL PROOF
					'always': 'Always',
					'occasionally': 'Occasionally',
					'never': 'Never',
					'selalu': 'Always',
					'kadang-kadang': 'Occasionally',
					'tidakpernah': 'Never',
				})],
			}]

		// heels height
		case 'N9kIKA6jDSWH': // ok
		case 't7Yl4w1rIatb': // ok
		case 'Dv7Z7zht6BrR': // ok
		case 'qNXkGJQCvlc2': // ok
			return [51, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Low', 'Mid', 'High'], {
						'Pendek (3 cm - 5 cm)': 'Low',
						'Sedang (7 cm - 10 cm)': 'Mid',
						'Tinggi (12 cm - 17 cm)': 'High',
						'Low (3 cm - 5 cm)': 'Low',
						'Mid (7 cm - 10 cm)': 'Mid',
						'High (12 cm - 17 cm)': 'High',
						// FOOL PROOF
						'low': 'Low',
						'mid': 'Mid',
						'high': 'High',
						'pendek(3cm-5cm)': 'Low',
						'sedang(7cm-10cm)': 'Mid',
						'tinggi(12cm-17cm)': 'High',
						'low(3cm-5cm)': 'Low',
						'mid(7cm-10cm)': 'Mid',
						'high(12cm-17cm)': 'High',
					})
				}),
			}]

		// ear pierced
		case 'XssMR61gce0u': // ok
		case 'axk0G6ROaW8X': // ok
		case 'Jc6cjeeXDxLc': // ok
		case 'uP7CAN599e9K': // ok
			return [52, {
				BOOLEAN: answer[answer.type],
			}]

		// jewelry characteristic
		case 'Vt4ggNVtX6FI': // ok
		case 'uDUGzVgN5MkN': // ok
		case 'IUY4uDdTNQL9': // ok
		case 'j0gVU7ipHp0x': // ok
			return [53, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Classic', 'Minimalist/Simple', 'Statement', 'Mix of all'], {
					'Klasik': 'Classic',
					'Minimalis / Simple': 'Minimalist/Simple',
					'Minimalist / Simple': 'Minimalist/Simple',
					'Statement (unik & berani)': 'Statement',
					'Perpaduan seimbang semuanya': 'Mix of all',
					'Healthy mix of all': 'Mix of all',
					// FOOL PROOF
					'classic': 'Classic',
					'minimalist/simple': 'Minimalist/Simple',
					'statement': 'Statement',
					'mixofall': 'Mix of all',
					'klasik': 'Classic',
					'minimalis/simple': 'Minimalist/Simple',
					'statement(unik&berani)': 'Statement',
					'perpaduanseimbangsemuanya': 'Mix of all',
					'healthymixofall': 'Mix of all',
				})],
			}]

		// jewelry tones
		case 'R9r6YCLtY6jh': // ok
		case 'TV1tmwBsVvZw': // ok
		case 'tT8rE7HBVo4T': // ok
		case 'IXllZmJf3HKs': // ok
			return [54, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Silver', 'Gold', 'Rose gold'], {
						// FOOL PROOF
						silver: 'Silver',
						gold: 'Gold',
						rosegold: 'Rose gold',
					})
				}),
			}]

		// disliked accessory material
		case 'llFqJedq5hiy': // ok
		case 'o19MxFmurJeR': // ok
		case 't3lbfMLxVJFr': // ok
		case 'jbRqYe3pHhFP': // ok
			return [55, {
				CHOICE: answer[answer.type].labels.map(label => {
					return getNormalizedValue(label, ['Silver', 'Gold', 'Platinum', 'Base Metal', 'Natural', 'Synthetic', 'Regenerated'], {
						'Logam dasar (e.g. besi, nikel, tembaga, titanium)': 'Base Metal',
						'Alami (e.g. kulit, tulang, sutra, kayu)': 'Natural',
						'Sintetis (e.g. enamel, plastik, silikon)': 'Synthetic',
						'Regenerated (e.g. resin, rubber)': 'Regenerated',
						'Base Metal (e.g. iron, nickel, copper, titanium)': 'Base Metal',
						'Natural (e.g. leather, bone, silk, wood)': 'Natural',
						'Synthetic (e.g. enamel, plastic, silicone)': 'Synthetic',
						// FOOL PROOF
						'silver': 'Silver',
						'gold': 'Gold',
						'platinum': 'Platinum',
						'basemetal': 'Base Metal',
						'natural': 'Natural',
						'synthetic': 'Synthetic',
						'regenerated': 'Regenerated',
						'logamdasar(e.g.besi,nikel,tembaga,titanium)': 'Base Metal',
						'alami(e.g.kulit,tulang,sutra,kayu)': 'Natural',
						'sintetis(e.g.enamel,plastik,silikon)': 'Synthetic',
						'regenerated(e.g.resin,rubber)': 'Regenerated',
						'basemetal(e.g.iron,nickel,copper,titanium)': 'Base Metal',
						'satural(e.g.leather,bone,silk,wood)': 'Natural',
						'synthetic(e.g.enamel,plastic,silicone)': 'Synthetic',
					})
				}),
			}]

		// ABOUT YOU

		// year of birth
		case 'tI12DtwqfmpV': // ok
		case 'y7coG2kNEI3D': // ok
		case 'pr920bVbsjkn': // ok
		case 'hXal9UdC5cQl': // ok
			return [56, {
				DROPDOWN: answer[answer.type].label || answer[answer.type],
			}]

		// dress in typical day
		case 'nZUmbt4aKyN8': // ok
		case 's18zoWga2P8U': // ok
		case 'uYdb7ZiYIYdd': // ok
		case 'oiLgUNJIKCAL': // ok
			return [57, {
				CHOICE: [getNormalizedValue(answer[answer.type].label, ['Casual', 'Smart Casual', 'Semi-formal', 'Formal'], {
					'Casual (jeans, atasan)': 'Casual',
					'Smart Casual (jeans, atasan, blazer)': 'Smart Casual',
					'Semi- formal (celana panjang, rok, blus)': 'Semi-formal',
					'Formal (setelan jas)': 'Formal',
					'Casual (jeans, tops)': 'Casual',
					'Smart Casual (jeans, tops, blazers)': 'Smart Casual',
					'Semi - formal (slacks, skirts, blouses)': 'Semi-formal',
					'Formal (suits, suit dresses)': 'Formal',
					// FOOL PROOF
					'casual': 'Casual',
					'smartcasual': 'Smart Casual',
					'semi-formal': 'Semi-formal',
					'formal': 'Formal',
					'casual(jeans,atasan)': 'Casual',
					'smartcasual(jeans,atasan,blazer)': 'Smart Casual',
					'semi-formal(celanapanjang,rok,blus)': 'Semi-formal',
					'formal(setelanjas)': 'Formal',
					'casual(jeans,tops)': 'Casual',
					'smartcasual(jeans,tops,blazers)': 'Smart Casual',
					'semi-formal(slacks,skirts,blouses)': 'Semi-formal',
					'formal(suits,suitdresses)': 'Formal',
				})],
			}]

		// a parent
		case 'KXWhdLCphgJO': // ok
		case 'BSv0Pvw6lWAa': // ok
		case 'vvbamOhnw1mp': // ok
		case 'Eo4pFBCTp0PK': // ok
			return [58, {
				BOOLEAN: answer[answer.type],
			}]

		// instagram
		case 'kUD5oIIxEiXM': // ok
		case 'bJRTIogggZwQ': // ok
		case 'zTr4iWhI9ArH': // ok
		case 'GLieCLA25kf3': // ok
			return [60, {
				TEXT_SHORT: answer[answer.type],
			}]

		// pinterest
		case 'vo4Ugczma0ww': // ok
		case 'FWikD9FrL2OK': // ok
		case 'Evvywb6rvIep': // ok
		case 'W4D6DRvT6kdr': // ok
			return [61, {
				URL: answer[answer.type],
			}]

		// notes
		case 'AHH633W80xDb': // ok
		case 'y5NSLPWqkW04': // ok
		case 'vpm5iG3BM1Xe': // ok
		case 'mxLk19zLBdcH': // ok
			return [62, {
				TEXT_LONG: answer[answer.type],
			}]

		// photo
		case 'iry1hUcXi7gy': // ok
		case 'beHpxDenLEtM': // ok
		case 'X4RBMgFoeUtF': // ok
		case 'kJL9YkScwMXN': // ok
			return [63, {
				FILE: [answer[answer.type]],
			}]

		default:
			return null
		}
	},
}
