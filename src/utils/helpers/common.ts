import { flatten } from 'lodash'
import { Without } from 'types/common'
import { UserProfileInterface } from 'energie/app/interfaces'

type NoUndefined<T extends {}> = Pick<T, { [t in keyof T]: T[t] extends undefined ? never : t }[keyof T]>


export default {
	sum(sum: number = 0, value: number) {
		return sum + value
	},
	sumObject(sum: {} = {}, value: object) {
		return {
			...sum,
			...value,
		}
	},
	sumArray<T extends any>(sum: T[] = [], value: T[]) {
		return [
			...sum,
			...value,
		]
	},
	flatten,
	sortNumber(a: number, b: number) {
		return a - b
	},
	stripUndefined<T>(obj: T): NoUndefined<T> {
		for (const key in obj) {
			if (obj[key] === undefined) {
				delete obj[key]
			}
		}

		return obj
	},
	stripKey<T, K extends keyof T>(obj: T, ...keys: K[]): Without<T, K> {
		const newObj = { ...obj }
		keys.forEach(key => {
			newObj[key] = undefined
			delete newObj[key]
		})

		return newObj
	},
	allowKey<T, K extends keyof T>(obj: T, ...keys: K[] | string[] | [undefined]): Pick<T, K> {
		const newObj = {}

		if(keys[0] !== undefined && !!obj) {
			keys.forEach((key: string) => {
				if (key.indexOf('.') > -1) {
					const newKeys = key.split('.')
					const lastKey = newKeys.pop()
					let data = obj

					newKeys.reduce((sum, k) => {
						if (sum[k] === undefined) {
							sum[k] = {}
						}

						data = data[k]

						return sum[k]
					}, newObj)[lastKey] = data[lastKey]

				} else {
					newObj[key as any] = obj[key]
				}
			})

			return newObj as any
		} else {
			return obj
		}
	},
	numberRange(numbers: number[]): [number, number] {
		const n = numbers.sort(this.sortNumber)
		const lowest = n.shift()
		const highest = numbers.pop()

		if(highest) {
			return [lowest, highest]
		}

		return [lowest, lowest]
	},
	removeFalsyObject(data: object) {
		const newObj = {}
		Object.keys(data).forEach(prop => {
			if (data[prop]) { newObj[prop] = data[prop] }
		})
		return newObj
	},
	default<T extends any, U extends NonNullable<T>, R extends T extends undefined ? U : T>(property: T, defaultValue: U): R {
		if (property === undefined || property === null) {
			return defaultValue as R
		}

		return property as R
	},
	serialize<T extends any, U extends any>(datas: T[], repeater: (promise: Promise<U>, data: T) => Promise<U>): Promise<U> {
		return datas.reduce(repeater, Promise.resolve('' as any))
	},
	fullName(profile: UserProfileInterface) {
		return profile.last_name ? `${ profile.first_name } ${ profile.last_name }` : profile.first_name
	},
}
