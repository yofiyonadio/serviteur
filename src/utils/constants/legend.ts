export enum ASSETS {
	IMAGE = 'Image file types, e.g. JPG, PNG, GIF, TIFF, WEBP.',
	VIDEO = 'Video file types, e.g. MP4, MOV.',
	FILE = 'Document file types, e.g. PDF, XLSX, CSV.',
	RAW = 'Raw file types, e.g. RAW.',
}

export enum BOOKING_SOURCES {
	ORDER_DETAIL = 'Booking came from order detail. This means that the order is holding the item temporarily until payment.',
	VOUCHER = 'Booking came from voucher. This means that the voucher is active and holding the item.',
	STYLESHEET = 'Booking came from stylesheet. This means that stylist is currently styling using this item.',
	USER = 'Booked by user.',
}

export enum BOOKING_STATUSES {
	BOOKED = 'Booking in progress.',
	CLAIMED = 'Booking done and item has already claimed.',
	CANCELLED = 'Booking cancelled.',
}

export enum CODES {
	PENDING = 'Progress pending, waiting someone to take an action.',
	SUCCESS = 'Action approved / success.',
	FAILED = 'Action cancelled / failed.',
	EXCEPTION = 'Action exceptioned. Please refer to note or change log.',
}

export enum COUPONS {
	PERCENT = 'Coupon amount is in percent. The amount is fluid based on total item price.',
	TOTAL = 'Coupon amount is fixed in Rupiah.',
	SHIPPING = 'Coupon will deduct shipping instead of product price.',
}

export enum COUPON_VALIDITY {
	MATCHBOX = 'Coupon valid only for certain matchboxes.',
	PRODUCT = 'Coupon valid only for certain products / variants.',
	USER = 'Coupon valid only for certain user.',
	SERVICE = 'Coupon valid only for certain services.',
}

export enum DAYS {
	SUNDAY = 'Sunday',
	MONDAY = 'Monday',
	TUESDAY = 'Tuesday',
	WEDNESDAY = 'Wednesday',
	THURSDAY = 'Thursday',
	FRIDAY = 'Friday',
	SATURDAY = 'Saturday',
}

export enum EXCHANGES {
	STYLESHEET_INVENTORY = 'Stylesheet inventory is being exchanged.',
	INVENTORY = 'Inventory is being exchanged.',
}

export enum FEEDBACKS {
	STYLESHEET = 'This feedback is valid for the referenced stylesheet.',
	VARIANT = 'This feedback is valid for a certain variant.',
}

export enum INVENTORIES {
	AVAILABLE = 'Inventory is currently in a warehouse and available for booking.',
	UNAVAILABLE = 'Inventory is currently unavailable. It could mean that the inventory is already packed inside a stylesheet.',
	BOOKED = 'Inventory is currently booked. Please refer to booking for more detail.',
	DEFECT = 'Inventory marked as defect. It has flaw that render this inventory useless.',
	EXCEPTION = 'Inventory marked as exception. Please refer to note or change log.',
}

export enum NOTIFICATIONS {
	PROMO = 'Promotional notification. E.g. new coupons or vouchers.',
	ORDER = 'Order related notification. E.g. shipment update or payment update.',
}

export enum ORDERS {
	PENDING_PAYMENT = 'Order unpaid. Waiting till user made a payment.',
	PAID = 'Order paid. Waiting for fulfillment team to start the progress.',
	PAID_WITH_EXCEPTION = 'Order paid but with exception on its detail. Please check your order details for anomaly.',
	PROCESSING = 'Order is being completed.',
	RESOLVED = 'Order is completed and closed.',
	EXPIRED = 'Order unpaid and closed.',
	EXCEPTION = 'Order exceptioned. Please check it\'s detail for anomaly. This could mean that the order has been cancelled.',
}

export enum ORDER_DETAILS {
	INVENTORY = 'This order detail should contain an inventory item.',
	SERVICE = 'This order detail should contain a service.',
	STYLESHEET = 'This order detail should contain a stylesheet.',
	VOUCHER = 'This order detail should contain a voucher.',
}

export enum ORDER_REQUESTS {
	INVENTORY = 'Request came from an inventory purchase request with specific inventory id.',
	MATCHBOX = 'Request came from a matchbox purchase.',
	SERVICE = 'Request came from a service purchase.',
	VARIANT = 'Request came from a variant purchase request.',
	VOUCHER = 'Request came from a voucher redemption.',
}

export enum ORDER_DETAIL_STATUSES {
	PENDING = 'Progress pending. Waiting for an action to start the progress.',
	PROCESSING = 'Order detail is currently being completed.',
	PRIMED = 'Order detail has been prepared. Waiting for shipment or actions from user.',
	RESOLVED = 'Order detail has been completed and closed.',
	EXCEPTION = 'Order detail invalid. Please refer to note or changelog for anomaly.',
}

export enum PAYMENTS {
	SUCCESS = 'Payment success.',
	FAILED = 'Payment failed.',
	PENDING = 'Waiting for payment.',
	EXCEPTION = 'Payment is exceptioned. Please refer to note or change log for detail.',
}

export enum PAYMENT_AGENTS {
	MANUAL = 'Manual payment approval / rejection by admin.',
	FASPAY = 'Third party: Faspay.',
	MIDTRANS = 'Third party: Midtrans.',
}

export enum PURCHASES {
	PENDING = 'Purchase order made but not yet paid.',
	PURCHASED = 'Purchase has been made and paid.',
	RESOLVED = 'Purchase completed.',
	CANCELLED = 'Purchase cancelled.',
	EXCEPTION = 'Purchase is exceptioned. Please refer to note or change log for anomaly.',
}

export enum PURCHASE_REQUESTS {
	PENDING = 'Purchase request is being reviewed.',
	APPROVED = 'Purchase request is approved by reviewer.',
	REJECTED = 'Purchase request is rejected by reviever.',
	RESOLVED = 'Purchase request is resolved and closed.',
	EXCEPTION = 'Purchase request is exceptioned. Please refer to note or change log.',
}

export enum QUESTIONS {
	DEFINITION = 'Question consist of definition only. This is not a real question, shouldn\'t be marked as required.',
	CHOICE = 'Multiple choice questions.',
	TEXT_SHORT = 'Question requires a concise text answer.',
	TEXT_LONG = 'Question requires a long text answer.',
	STATEMENT = 'Question consist of statement only. This is not a real question, shouldn\'t be marked as required .',
	BOOLEAN = 'Yes or no question.',
	EMAIL = 'Question requires an email answer.',
	SCALE = 'Multiple selection question with a scale UI.',
	// RATING = 'Rating question',
	SLIDER = 'Multiple selection question with a slider UI.',
	TAB = 'Multiple selection question with a tab UI',
	DATE = 'Question requires a date answer.',
	// DATETIME = 'Question requires a date and time answer.',
	NUMBER = 'Question requires a number answer.',
	DROPDOWN = 'Multiple selection question with dropdown UI.',
	CHECKBOX = 'Yes or no question with checkbox UI.',
	FILE = 'Question requires a file upload answer.',
	URL = 'Question requires an URL answer.',
}

export enum REQUIREMENT_OPERATORS {
	IS = 'Requirement met by supplying an equal answer.',
	NOT = 'Requirement met by supplying a not equal answer.',
	GREATER_THAN = 'Requirement met by a greater number answer.',
	LESS_THAN = 'Requirement met by a lesser number answer.',
}

export enum SHIPMENTS {
	TIKI = 'Third party: Tiki.',
	MANUAL = 'Manual shipment creation by admin.',
	SICEPAT = 'Third party: SiCepat.',
}

export enum SHIPMENT_STATUSES {
	PENDING = 'Shipment initiated but not yet started.',
	PROCESSING = 'Package picked up by respected agent.',
	ON_COURIER = 'Package is on the way to destination.',
	FAILED = 'Package delivery failed. Please refer to note or change log.',
	DELIVERED = 'Package is successfully delivered on destination.',
	DELIVERY_CONFIRMED = 'Package delivery confirmed on destination.',
	EXCEPTION = 'Package exception. Please refer to note or change log for anomaly.',
}

export enum SOCIALS {
	FACEBOOK = 'Facebook',
	INSTAGRAM = 'Instagram',
}

export enum STATEMENTS {
	DEBIT = 'Negative cash flow (Money is deducted).',
	CREDIT = 'Positive cash flow (Money is deposited).',
}

export enum STATEMENT_SOURCES {
	REQUEST = 'Transaction source is a request made by admin / user.',
	ORDER = 'Transaction came from an order, either a refund or a wallet balance usage.',
	// For refund on stylesheet inventory
	STYLESHEET_INVENTORY = 'Transaction came from a stylesheet inventory, usually a refund from certain inventory return.',
	// For refund on shipment, based on exchange id
	EXCHANGE = 'Transaction came from refund / deposit of an exchange. Usually means a shipment refund cost.',
	// For administrative injection
	USER = 'Transaction initiated by a user.',
}

export enum STYLESHEETS {
	FIRST = 'First ever stylesheet for this certain user.',
	REPEAT = 'Repeated stylesheet for this certain user.',
	EXCHANGE = 'An exchange stylesheet.',
}

export enum STYLESHEET_STATUSES {
	PENDING = 'Stylesheet in on queue and not yet styled.',
	STYLING = 'Stylesheet is being completed.',
	PUBLISHED = 'Stylesheet is done and published.',
	APPROVED = 'Stylesheet is approved by head stylist.',
	EXCEPTION = 'Stylesheet is exceptioned. Please refer to note or change log.',
}

export enum STYLESHEET_INVENTORY_STATUSES {
	BOOKED = 'Inventory successfully booked.',
	PACKED = 'Inventory packed into stylesheet.',
	INVENTORY_EXCEPTION = 'Inventory is defect or exceptioned. Please refer to note or change log.',
	INVENTORY_INVALID = 'Inventory status is invalid. Either is overriden or something happened to it. Please refer to note or change log.',
	INVENTORY_LOCKED = 'A returned inventory and is locked. You cannot change or remove this inventory.',
	REQUESTING = 'Request has been made and currently waiting for approval.',
	PURCHASING = 'Request has been added into purchase list.',
	PURCHASED = 'Request has been ordered and bought.',
	PURCHASE_CANCELLED = 'Purchase cancelled. Please refer to note or change log for reason.',
	PURCHASE_COMPLETED = 'Purchase completed and closed.',
	PURCHASE_EXCEPTION = 'Purchase exceptioned. Please refer to note or change log for anomaly.',
	REQUEST_APPROVED = 'Request has been approved.',
	REQUEST_REJECTED = 'Request rejected. Please refer to note or change log.',
	REQUEST_COMPLETED = 'Request resolved and closed.',
	REQUEST_EXCEPTION = 'Request exceptioned. Please refer to note or change log.',
	INVALID = 'Stylesheet inventory status invalid. Please refer to note or change log.',
}

export enum TOKENS {
	RESET = 'Reset password token.',
	VERIFICATION = 'User verification token. It is used for email verification.',
}

export enum USER_REQUESTS {
	CHANGE_STYLIST = 'Stylist change request.',
	WITHDRAW = 'Wallet money withdrawal request.',
}

export enum VOUCHERS {
	MATCHBOX = 'This voucher would be redeemed into a matchbox.',
	INVENTORY = 'This voucher would be redeemed into a certain inventory.',
	SERVICE = 'This voucher would be redeemed into a service.',
}

export enum VOUCHER_STATUSES {
	UNPUBLISHED = 'Voucher is unpublished.',
	AVAILABLE = 'Voucher is currently published, active and available for redeem.',
	REDEEM_PENDING = 'Voucher is redeemed but currently waiting for payment to be fulfilled.',
	REDEEMED = 'Voucher has been redeemed.',
	EXCEPTION = 'Voucher exceptioned. Please refer to note or change log.',
}
