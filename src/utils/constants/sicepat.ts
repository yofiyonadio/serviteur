export default {
	whitelist: {
		service: [
			'REG',
			'BEST',
			'CARGO',
		],
		category: [
			'Organic',
			'Normal',
			'Fragile',
			'Electronic',
		],
	},
}
