const _RETURN_SHIPMENT_REFUND = 10000
const _MINIMUM_WITHDRAW_AMOUNT = 10000

let _AGENT = ''
let _DEBUG = false

let _DEFAULT_MATCHBOX_PACKET_ID = -1
let _MINI_MATCHBOX_PACKET_ID = -1
let _DIGITAL_PACKET_ID = -1
let _PRODUCT_PACKET_ID = -1
let _VOUCHER_PACKET_ID = -1
let _CANDLE_PACKET_ID = -1

let _CUSTOM_MATCHBOX_ID = -1 // Which is a replacement
let _STYLING_MATCHBOX_ID = -1
let _STYLING_RESULT_MATCHBOX_ID = -1
let _CUSTOM_STYLING_SERVICE_ID = -1

let _YUNA_BRAND_ID = -1
let _YUNA_BRAND_ADDRESS_ID = -1
const _YUNA_BRAND_NAME = 'Yuna & Co.'

let _EXCHANGE_QUESTION_ID = -1
let _EXCHANGE_NOTE_QUESTION_ID = -1
let _EXCHANGE_COMMENT_QUESTION_ID = -1
let _VARIANT_FEEDBACK_QUESTION_ID = -1

let _VARIANT_QUESTION_GROUP_ID = -1
let _STYLE_PROFILE_QUESTION_GROUP_ID = [-1]
let _STYLESHEET_QUESTION_GROUP_ID = -1

let _ROUNDUP_TOPUP_ID = -1
let _WALLET_TOPUP_ID = -1

let _SYSTEM_USER_ID = 1

let _SHIPMENT_PERIOD = 2
let _SYNC = false

let _IS_MAIN = false


export default {
	get DEBUG() {
		return _DEBUG
	},

	get AGENT() {
		return _AGENT
	},

	get SYNC() {
		return _SYNC
	},

	get CUSTOM_MATCHBOX_ID() {
		return _CUSTOM_MATCHBOX_ID
	},

	get STYLING_MATCHBOX_ID() {
		return _STYLING_MATCHBOX_ID
	},

	get STYLING_RESULT_MATCHBOX_ID() {
		return _STYLING_RESULT_MATCHBOX_ID
	},

	get CUSTOM_STYLING_SERVICE_ID() {
		return _CUSTOM_STYLING_SERVICE_ID
	},

	get DEFAULT_MATCHBOX_PACKET_ID() {
		return _DEFAULT_MATCHBOX_PACKET_ID
	},

	get MINI_MATCHBOX_PACKET_ID() {
		return _MINI_MATCHBOX_PACKET_ID
	},

	get DIGITAL_PACKET_ID() {
		return _DIGITAL_PACKET_ID
	},

	get PRODUCT_PACKET_ID() {
		return _PRODUCT_PACKET_ID
	},

	get VOUCHER_PACKET_ID() {
		return _VOUCHER_PACKET_ID
	},

	get CANDLE_PACKET_ID() {
		return _CANDLE_PACKET_ID
	},

	get YUNA_BRAND_ID() {
		return _YUNA_BRAND_ID
	},

	get YUNA_BRAND_ADDRESS_ID() {
		return _YUNA_BRAND_ADDRESS_ID
	},

	get YUNA_BRAND_NAME() {
		return _YUNA_BRAND_NAME
	},

	get EXCHANGE_QUESTION_ID() {
		return _EXCHANGE_QUESTION_ID
	},

	get EXCHANGE_NOTE_QUESTION_ID() {
		return _EXCHANGE_NOTE_QUESTION_ID
	},

	get EXCHANGE_COMMENT_QUESTION_ID() {
		return _EXCHANGE_COMMENT_QUESTION_ID
	},

	get VARIANT_FEEDBACK_QUESTION_ID() {
		return _VARIANT_FEEDBACK_QUESTION_ID
	},

	get VARIANT_QUESTION_GROUP_ID() {
		return _VARIANT_QUESTION_GROUP_ID
	},

	get STYLE_PROFILE_QUESTION_GROUP_ID() {
		return _STYLE_PROFILE_QUESTION_GROUP_ID
	},

	get STYLESHEET_QUESTION_GROUP_ID() {
		return _STYLESHEET_QUESTION_GROUP_ID
	},

	get ROUNDUP_TOPUP_ID() {
		return _ROUNDUP_TOPUP_ID
	},

	get WALLET_TOPUP_ID() {
		return _WALLET_TOPUP_ID
	},

	get SHIPMENT_PERIOD() {
		return _SHIPMENT_PERIOD
	},

	get SYSTEM_USER_ID() {
		return _SYSTEM_USER_ID
	},

	get RETURN_SHIPMENT_REFUND() {
		return _RETURN_SHIPMENT_REFUND
	},

	get MINIMUM_WITHDRAW_AMOUNT() {
		return _MINIMUM_WITHDRAW_AMOUNT
	},

	get IS_MAIN() {
		return _IS_MAIN
	},

	setAgent(agent: string) {
		_AGENT = agent
	},

	setDebugLevel(level: boolean) {
		_DEBUG = level
	},

	setCustomStylingServiceId(id: number) {
		_CUSTOM_STYLING_SERVICE_ID = id
	},

	setCustomMatchboxId(id: number) {
		_CUSTOM_MATCHBOX_ID = id
	},

	setStylingMatchboxId(id: number) {
		_STYLING_MATCHBOX_ID = id
	},

	setStylingResultMatchboxId(id: number) {
		_STYLING_RESULT_MATCHBOX_ID = id
	},

	setDefaultMatchboxPacketId(id: number) {
		_DEFAULT_MATCHBOX_PACKET_ID = id
	},

	setMiniMatchboxPacketId(id: number) {
		_MINI_MATCHBOX_PACKET_ID = id
	},

	setDigitalPacketId(id: number) {
		_DIGITAL_PACKET_ID = id
	},

	setProductPacketId(id: number) {
		_PRODUCT_PACKET_ID = id
	},

	setVoucherPacketId(id: number) {
		_VOUCHER_PACKET_ID = id
	},

	setCandlePacketId(id: number) {
		_CANDLE_PACKET_ID = id
	},

	setYunaBrandId(id: number) {
		_YUNA_BRAND_ID = id
	},

	setYunaBrandAddressId(id: number) {
		_YUNA_BRAND_ADDRESS_ID = id
	},

	setExchangeQuestionId(id: number) {
		_EXCHANGE_QUESTION_ID = id
	},

	setExchangeNoteQuestionId(id: number) {
		_EXCHANGE_NOTE_QUESTION_ID = id
	},

	setVariantFeedbackQuestionId(id: number) {
		_VARIANT_FEEDBACK_QUESTION_ID = id
	},

	setExchangeCommentQuestionId(id: number) {
		_EXCHANGE_COMMENT_QUESTION_ID = id
	},

	setVariantQuestionGroupId(id: number) {
		_VARIANT_QUESTION_GROUP_ID = id
	},

	setStyleProfileQuestionGroupId(id: number[]) {
		_STYLE_PROFILE_QUESTION_GROUP_ID = id
	},

	setStylesheetQuestionGroupId(id: number) {
		_STYLESHEET_QUESTION_GROUP_ID = id
	},

	setRoundupTopupId(id: number) {
		_ROUNDUP_TOPUP_ID = id
	},

	setWalletTopupId(id: number) {
		_WALLET_TOPUP_ID = id
	},

	setShipmentPeriod(num: number) {
		_SHIPMENT_PERIOD = num
	},

	setSync(sync: boolean) {
		_SYNC = sync
	},

	setSystemUserId(system_user_id: number) {
		_SYSTEM_USER_ID = system_user_id
	},

	setIsMain(is_main: boolean) {
		_IS_MAIN = is_main
	},
}
