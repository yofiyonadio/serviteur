import {
	// ORDERS,
	// ORDER_DETAIL_STATUSES,
} from 'energie/utils/constants/enum'

export enum READABLE_ORDERS {
	'PENDING_PAYMENT' = 'Awaiting Payment',
	'PAID' = 'Payment Confirmed',
	'PROCESSING' = 'Processing',
	'PRIMED' = 'Processing',
	'SHIPPING' = 'Shipping',
	'DELIVERED' = 'Delivered',
	'EXPIRED' = 'Expired',
	'EXCEPTION' = 'Exception',
}

export enum READABLE_ORDER_DETAIL_STATUSES {
	'PENDING' = 'Pending',
	'PROCESSING' = 'Processing',
	'PRIMED' = 'Packed',
	'EXCEPTION' = 'Exception',
}

export enum READABLE_SHIPMENT_STATUSES {
	'PROCESSING' = 'Processing',
	'ON_COURIER' = 'On Courier',
	'FAILED' = 'Failed',
	'DELIVERED' = 'Delivered',
	'DELIVERY_CONFIRMED' = 'Delivery Confirmed',
	'EXCEPTION' = 'Exception',
}

export enum READABLE_STYLESHEET_STATUSES {
	'PENDING' = 'Pending',
	'STYLING' = 'Styling',
	'PUBLISHED' = 'Published',
	'APPROVED' = 'Approved',
	'EXCEPTION' = 'Exception',
}
