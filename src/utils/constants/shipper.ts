export default {
	whitelist: {
		name: [
			'J&T',
			'SiCepat',
			'Tiki',
			'JNE',
		],
		rateId: [
			57,
			58,
			54,
			1,
		],
	},
	blacklist: {
		name: [
			'Ninja Xpress',
			'Lion Parcel',
			'Wahana',
			'POS Indonesia',
		],
		rateId: [
			228,
			44,
			15,
			49,
		],
	},
}
