export default interface ListenerInterface {
    old: object,
    new: object,
    schema: string,
    table: string,
    type: string,
    method: 'POST' | 'GET' | 'PUT' | 'DELETE'
    exec_type: 'internal' | 'external',
    listener_name: string,
    target_uri: string[]
}