export type Parameter<T, U = never> = Pick<T, Exclude<keyof T, 'id' | 'created_at' | 'updated_at' | U>>

export type ObjectOrArray<T> = T | T[]

export type Without<T, K> = Pick<T, Exclude<keyof T, K>>

export type Unarray<T> = T extends Array<infer U> ? U : never

export type ObjectOrArrayParameter<T> = ObjectOrArray<Parameter<T>>

export type Await<T> = T extends {
	then(onfulfilled ?: (value: infer U) => unknown): unknown;
} ? U: T
