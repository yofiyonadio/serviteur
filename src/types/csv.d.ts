export function generate(...args: any[]): any;
export namespace generate {
	class Generator {
		static ascii(gen: any): any;
		static bool(gen: any): any;
		static camelize(str: any): any;
		static int(gen: any): any;
		constructor(options: any);
		options: any;
		addListener(ev: any, fn: any): any;
		destroy(err: any, cb: any): any;
		emit(type: any, args: any): any;
		end(): any;
		eventNames(): any;
		getMaxListeners(): any;
		isPaused(): any;
		listenerCount(type: any): any;
		listeners(type: any): any;
		off(type: any, listener: any): any;
		on(ev: any, fn: any): any;
		once(type: any, listener: any): any;
		pause(): any;
		pipe(dest: any, pipeOpts: any): any;
		prependListener(type: any, listener: any): any;
		prependOnceListener(type: any, listener: any): any;
		push(chunk: any, encoding: any): any;
		random(): any;
		rawListeners(type: any): any;
		read(n: any): any;
		removeAllListeners(ev: any): any;
		removeListener(ev: any, fn: any): any;
		resume(): any;
		setEncoding(enc: any): any;
		setMaxListeners(n: any): any;
		unpipe(dest: any): any;
		unshift(chunk: any): any;
		wrap(stream: any): any;
	}
}
export function parse(...args: any[]): any;
export namespace parse {
	class Parser {
		constructor(opts: any);
		info: any;
		options: any;
		state: any;
		addListener(ev: any, fn: any): any;
		cork(): void;
		destroy(err: any, cb: any): any;
		emit(type: any, args: any): any;
		end(chunk: any, encoding: any, cb: any): any;
		eventNames(): any;
		getMaxListeners(): any;
		isPaused(): any;
		listenerCount(type: any): any;
		listeners(type: any): any;
		off(type: any, listener: any): any;
		on(ev: any, fn: any): any;
		once(type: any, listener: any): any;
		pause(): any;
		pipe(dest: any, pipeOpts: any): any;
		prependListener(type: any, listener: any): any;
		prependOnceListener(type: any, listener: any): any;
		push(chunk: any, encoding: any): any;
		rawListeners(type: any): any;
		read(n: any): any;
		removeAllListeners(ev: any): any;
		removeListener(ev: any, fn: any): any;
		resume(): any;
		setDefaultEncoding(encoding: any): any;
		setEncoding(enc: any): any;
		setMaxListeners(n: any): any;
		uncork(): void;
		unpipe(dest: any): any;
		unshift(chunk: any): any;
		wrap(stream: any): any;
		write(chunk: any, encoding: any, cb: any): any;
	}
}
export function stringify(...args: any[]): any;
export namespace stringify {
	class Stringifier {
		constructor(opts: any);
		options: any;
		state: any;
		info: any;
		addListener(ev: any, fn: any): any;
		cork(): void;
		destroy(err: any, cb: any): any;
		emit(type: any, args: any): any;
		end(chunk: any, encoding: any, cb: any): any;
		eventNames(): any;
		getMaxListeners(): any;
		headers(): any;
		isPaused(): any;
		listenerCount(type: any): any;
		listeners(type: any): any;
		normalize_columns(columns: any): any;
		off(type: any, listener: any): any;
		on(ev: any, fn: any): any;
		once(type: any, listener: any): any;
		pause(): any;
		pipe(dest: any, pipeOpts: any): any;
		prependListener(type: any, listener: any): any;
		prependOnceListener(type: any, listener: any): any;
		push(chunk: any, encoding: any): any;
		rawListeners(type: any): any;
		read(n: any): any;
		removeAllListeners(ev: any): any;
		removeListener(ev: any, fn: any): any;
		resume(): any;
		setDefaultEncoding(encoding: any): any;
		setEncoding(enc: any): any;
		setMaxListeners(n: any): any;
		stringify(chunk: any): any;
		uncork(): void;
		unpipe(dest: any): any;
		unshift(chunk: any): any;
		wrap(stream: any): any;
		write(chunk: any, encoding: any, cb: any): any;
	}
}
export function transform(...args: any[]): any;
export namespace transform {
	class Transformer {
		constructor(options1: any, transform1: any);
		options: any;
		transform: any;
		running: any;
		started: any;
		finished: any;
		addListener(ev: any, fn: any): any;
		cork(): void;
		destroy(err: any, cb: any): any;
		emit(type: any, args: any): any;
		end(chunk: any, encoding: any, cb: any): any;
		eventNames(): any;
		getMaxListeners(): any;
		isPaused(): any;
		listenerCount(type: any): any;
		listeners(type: any): any;
		off(type: any, listener: any): any;
		on(ev: any, fn: any): any;
		once(type: any, listener: any): any;
		pause(): any;
		pipe(dest: any, pipeOpts: any): any;
		prependListener(type: any, listener: any): any;
		prependOnceListener(type: any, listener: any): any;
		push(chunk: any, encoding: any): any;
		rawListeners(type: any): any;
		read(n: any): any;
		removeAllListeners(ev: any): any;
		removeListener(ev: any, fn: any): any;
		resume(): any;
		setDefaultEncoding(encoding: any): any;
		setEncoding(enc: any): any;
		setMaxListeners(n: any): any;
		uncork(): void;
		unpipe(dest: any): any;
		unshift(chunk: any): any;
		wrap(stream: any): any;
		write(chunk: any, encoding: any, cb: any): any;
	}
}
