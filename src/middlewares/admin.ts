import Codes from 'utils/constants/code'
// import Errors from 'utils/constants/error'

import {
	intersection,
} from 'lodash'
import {
	NextFunction,
	Request,
	Response,
} from 'express'
import ErrorModel, { ERRORS, ERROR_CODES } from 'app/models/error'
import { UserManager } from 'app/managers'
import { Connection } from 'typeorm'


export default function(roles: string[] | string, connection: Connection) {
	if(Array.isArray(roles)) {
		return (req: Request, res: Response, next: NextFunction) => {
			connection.transaction(async transaction => {
				return UserManager.getRoles(res.locals.data.user.id, transaction)
					.then(userRoles => {
						if(userRoles && userRoles.length && intersection(userRoles, roles).length) {
							next()
						} else {
							// tslint:disable-next-line: no-string-throw
							throw `Need ${roles.join(', ')} privilege.`
						}
					})
			}).catch(err => {
				res.status(Codes.httpstatus.unauthorized).json(new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, err))
			})
		}
	} else {
		return (req: Request, res: Response, next: NextFunction) => {
			connection.transaction(async transaction => {
				return UserManager.getRoles(res.locals.data.user.id, transaction)
					.then(userRoles => {
						if (userRoles && userRoles.length && userRoles.indexOf(roles) > -1) {
							next()
						} else {
							// tslint:disable-next-line: no-string-throw
							throw `Need ${roles} privilege.`
						}
					})
			}).catch(err => {
				res.status(Codes.httpstatus.unauthorized).json(new ErrorModel(ERRORS.NOT_AUTHORIZED, ERROR_CODES.ERR_102, err))
			})
		}
	}
}
