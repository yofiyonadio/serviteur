import {
	NextFunction,
	Request,
	Response,
} from 'express'
import { Connection } from 'typeorm'

export default function(connection: Connection) {
	return (req: Request, res: Response, next: NextFunction) => {
		res.locals.data = {
			user: {},
		}

		// Default to always throw a json
		res.type('json')

		next()
	}
}
