import UserManager from 'app/managers/user'

import {
	NextFunction,
	Request,
	Response,
} from 'express'
import { Connection } from 'typeorm'

export default function(connection: Connection) {
	return (req: Request, res: Response, next: NextFunction) => {
		if(req.header('Authorization')) {
			const token = req.header('Authorization').split(' ')[1]

			if(token) {
				connection.transaction(async transaction => {
					return UserManager
						.authenticate(token, transaction)
						.then(data => {
							res.locals.data.token = token
							res.locals.data.user = data

							next()
						})
				}).catch((err: any) => {
					next()
				})
			} else {
				next()
			}
		} else {
			next()
		}
	}
}
