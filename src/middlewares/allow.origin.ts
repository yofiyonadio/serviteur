import Codes from 'utils/constants/code'
import {
	NextFunction,
	Request,
	Response,
} from 'express'
import { Connection } from 'typeorm'

export default function(connection: Connection) {
	return (req: Request, res: Response, next: NextFunction) => {
		res.header('Access-Control-Allow-Origin', '*')
		res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Cache-Control, Authorization')
		res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT')

		if(req.method === 'OPTIONS') {
			res.status(Codes.httpstatus.ok).send('OK')
		} else {
			next()
		}
	}
}
