module.exports = {
	apps: [{
		name: 'Connecter',
		script: 'index.js',
		env: {
			NODE_PATH: './',
		},
		instances: 'max',
		exec_mode : 'cluster',
	}],
	deploy: {
		development: {
			user: 'arch',
			host: '13.229.88.25',
			ref: 'origin/release-2.10',
			repo: 'git@bitbucket.org:yuna-frontend/connecter.git',
			path: '/home/arch/connecter',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv development && yarn add typescript pg@8.5.1 tsc-silent graphile-build-pg && npm i energie && npm run compile && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
		staging: {
			user: 'arch',
			host: '13.212.50.159',
			ref: 'origin/release-2.10',
			repo: 'git@bitbucket.org:yuna-frontend/connecter.git',
			path: '/home/arch/connecter',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv staging && yarn add typescript pg@8.5.1 tsc-silent graphile-build-pg && npm i energie && npm run compile && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
		production: {
			user: 'arch',
			host: '13.212.37.193',
			ref: 'origin/release-2.10',
			repo: 'git@bitbucket.org:yuna-frontend/connecter.git',
			path: '/home/arch/connecter',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv production && yarn add typescript pg@8.5.1 tsc-silent graphile-build-pg && npm i energie && npm run compile && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
	},
}
