const fs = require('fs');
const gulp = require('gulp');
const readDir = require('fs-readdir-recursive');
const _ = require('lodash');

const INDEXES = [{
	key: 'Repository',
	path: '../src/app/repositories',
}, {
	key: 'Manager',
	path: '../src/app/managers',
}, {
	key: 'Record',
	path: '../src/app/records',
}, {
	key: 'Handler',
	path: '../src/app/handlers',
}, {
	key: 'Model',
	path: '../src/app/models',
}, {
	key: 'Controller',
	path: '../src/controllers',
}, {
	key: 'Middleware',
	path: '../src/middlewares',
}, {
	key: 'Helper',
	path: '../src/utils/helpers',
}]

const ignoreRegex = /^[._]|^(style|config|view)\./
	, indexJsRegex = /^index\.(js|jsx|ts)$/i
	, pathIndexOrExtRegex = /(\/index\.[a-zA-Z]*$|.[a-zA-Z]*$)/
	, fileToRemoveRegex = /\.DS_Store/i
	, imageSetRegex = /@[0-9]x\./
	, ignoreExtension = ['jpg', 'png', 'gif', 'js', 'jsx', 'ts', 'mp4']
	, ignoreFilter = function(indexDir, ignore = ignoreRegex, file, i, dir) {
		return !file.match(ignore) && (indexDir === dir ? !file.match(indexJsRegex) : true)
	}
	, capitalizeFirstLetter = function(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	, removeMaster = function (string) {
		return string !== 'Master'
	}

gulp.task('index', () => {
	INDEXES.map(i => {
		const files = readDir(i.path, ignoreFilter.bind(this, i.path, i.ignore))
		let imports = ''
		let exports = ''

		_.uniqBy(files.map(f => {
			const folders = f.split('/').map(capitalizeFirstLetter).filter(removeMaster)
				, rawFileName = folders.pop()
				, moduleName = folders.join('').split('.').map(capitalizeFirstLetter).join('').split('-').map(capitalizeFirstLetter).join('')
				, fileName = !!rawFileName.match(indexJsRegex) ? '' : rawFileName.split('.').filter(fN => ignoreExtension.indexOf(fN) === -1).map(capitalizeFirstLetter).join('').split('@')[0].split('-').map(capitalizeFirstLetter).join('')
				, pathName = !!f.match(imageSetRegex) ? f.replace(imageSetRegex, '.') : f
				, pathWithoutIndexOrExt = pathName.replace(pathIndexOrExtRegex, '')

			return {
				module: moduleName,
				file: fileName,
				key: moduleName + fileName + i.key,
				path: pathName,
				pathfile: pathWithoutIndexOrExt,
			}
		}), 'key').forEach(fC => {
			imports += `import ${fC.key} from './${fC.pathfile}'\n`
			exports += `	${fC.key},\n`
		})

		fs.writeFile(`${i.path}/index.ts`, `${imports}\n\nexport {\n${exports}}\n`, (err) => {
			if(err) throw err;
		});
	})

	console.log('::gulp:index:: Done updating index.js');
})

gulp.task('remove:dsstore', () => {
	const toBeRemoved = readDir('./', () => true)

	!toBeRemoved.filter(f => {
		return f.match(fileToRemoveRegex)
	}).map(f => {
		fs.unlink(f, (err) => {
			if(err) throw err;
			console.log(`::gulp:remove:dsstore:: ${f} was deleted`);
		});

		return true
	}).length && console.log('::gulp:remove:dsstore:: no .DS_Store to delete');
})
